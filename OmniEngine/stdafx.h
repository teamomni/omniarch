/******************************************************************************
Filename: stdafx.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers



// TODO: reference additional headers your program requires here
#include "DirectXIncludes.h"
#include "AntTweakBar.h"
#include "FMODIncludes.h"

#include <iostream>
#include <functional>

#include "json/json.h"
#include "Console.h"

#include <algorithm>
#include <chrono>
#include <ratio>
#include <thread>
#include <mutex>
#include <queue>
#include <condition_variable>

#include "v8Utilities.h"

