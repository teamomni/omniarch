/******************************************************************************
Filename: Property.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "Vec2.h"
#include "DirectXIncludes.h"
#include "v8.h"

namespace Omni
{

  class Entity;
  class Space;
  class Physics;
  class Component;

  //
  // Property - component data container that can be any type forseeably needed
  //
  struct Property
  {
    typedef bool Bool;
    typedef unsigned long Unsigned;
    typedef long Int;
    typedef float Float;
    typedef std::string String;
    typedef Omni::Vec2 Vec2;
    typedef D3DXCOLOR Color;
    typedef std::vector<Omni::Vec2> Vec2List;

    enum class Type
    {
      Undefined = 0,
      Bool = 1,
      Unsigned = 2,
      Int = 3,
      Float = 4,
      String = 5,
      Vec2 = 6,
      Vec2List = 7,
      Color = 8,
      Entity = 9
    };

    Type type;
    size_t offset;

    bool showInEditor;
    bool readOnly = false;

    Property() : type(Type::Undefined), offset(0), showInEditor(true) {}
    Property(Type type_) : type(type_), offset(0), showInEditor(true) {}
    Property(Type type_, size_t offset_, bool showInEditor_) : type(type_), offset(offset_), showInEditor(showInEditor_) {}

    Property::Bool *GetBool(const Component *owner) const
    {
      return reinterpret_cast<Property::Bool *>(const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(owner)+offset));
    }
    Property::Unsigned *GetUnsigned(const Component *owner) const
    {
      return reinterpret_cast<Property::Unsigned *>(const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(owner)+offset));
    }
    Property::Int *GetInt(const Component *owner) const
    {
      return reinterpret_cast<Property::Int *>(const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(owner)+offset));
    }
    Property::Float *GetFloat(const Component *owner) const
    {
      return reinterpret_cast<Property::Float *>(const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(owner)+offset));
    }
    Property::String *GetString(const Component *owner) const
    {
      return reinterpret_cast<Property::String *>(const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(owner)+offset));
    }
    Property::Vec2 *GetVec2(const Component *owner) const
    {
      return reinterpret_cast<Property::Vec2 *>(const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(owner)+offset));
    }
    Property::Vec2List *GetVec2List(const Component *owner) const
    {
      return reinterpret_cast<Property::Vec2List *>(const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(owner)+offset));
    }
    Property::Color *GetColor(const Component *owner) const
    {
      return reinterpret_cast<Property::Color *>(const_cast<unsigned char *>(reinterpret_cast<const unsigned char *>(owner)+offset));
    }
  };
  typedef std::unordered_map<std::string, Property> PropertyMap;

  struct PropertyDefinition
  {
    Property::Type type;
    size_t offset;
    bool showInEditor;

    union
    {
      Property::Bool v_bool;
      Property::Unsigned v_unsigned;
      Property::Int v_int;
      Property::Float v_float;
    };
    Property::String v_string;
    Property::Color v_color;
    Property::Vec2 v_vec2;
    Property::Vec2List v_vec2list;

    PropertyDefinition() {}
    PropertyDefinition(Property::Type type_) : type(type_), offset(0), showInEditor(true) {}
    //PropertyDefinition(Property::Type type_, size_t offset_, bool showInEditor_) : type(type_), offset(offset_), showInEditor(showInEditor_) {}

    static PropertyDefinition Bool(Property::Bool v, size_t offset_, bool showInEditor_) { PropertyDefinition p; p.v_bool = v; p.type = Property::Type::Bool; p.offset = offset_; p.showInEditor = showInEditor_; return p; }
    static PropertyDefinition Unsigned(Property::Unsigned v, size_t offset_, bool showInEditor_) { PropertyDefinition p; p.v_unsigned = v; p.type = Property::Type::Unsigned; p.offset = offset_; p.showInEditor = showInEditor_; return p; }
    static PropertyDefinition Int(Property::Int v, size_t offset_, bool showInEditor_) { PropertyDefinition p; p.v_int = v; p.type = Property::Type::Int; p.offset = offset_; p.showInEditor = showInEditor_; return p; }
    static PropertyDefinition Float(Property::Float v, size_t offset_, bool showInEditor_) { PropertyDefinition p; p.v_float = v; p.type = Property::Type::Float; p.offset = offset_; p.showInEditor = showInEditor_; return p; }
    static PropertyDefinition String(Property::String v, size_t offset_, bool showInEditor_) { PropertyDefinition p; p.v_string = v; p.type = Property::Type::String; p.offset = offset_; p.showInEditor = showInEditor_; return p; }
    static PropertyDefinition Color(size_t offset_, bool showInEditor_) { PropertyDefinition p; p.v_color = Property::Color(1.0f, 1.0f, 1.0f, 1.0f); p.type = Property::Type::Color; p.offset = offset_; p.showInEditor = showInEditor_; return p; }
    static PropertyDefinition Vec2(Property::Vec2 v, size_t offset_, bool showInEditor_) { PropertyDefinition p; p.v_vec2 = v; p.type = Property::Type::Vec2; p.offset = offset_; p.showInEditor = showInEditor_; return p; }
    static PropertyDefinition Vec2List(size_t offset_, bool showInEditor_) { PropertyDefinition p; p.v_vec2list = {};/*{ Property::Vec2(0.5f, 0.5f), Property::Vec2(-0.5f, 0.5f), Property::Vec2(-0.5f, -0.5f), Property::Vec2(0.5f, -0.5f) };*/ p.type = Property::Type::Vec2List; p.offset = offset_; p.showInEditor = showInEditor_; return p; }

    static PropertyDefinition Bool(Property::Bool v) { PropertyDefinition p; p.v_bool = v; p.type = Property::Type::Bool; return p; }
    static PropertyDefinition Unsigned(Property::Unsigned v) { PropertyDefinition p; p.v_unsigned = v; p.type = Property::Type::Unsigned; return p; }
    static PropertyDefinition Int(Property::Int v) { PropertyDefinition p; p.v_int = v; p.type = Property::Type::Int; return p; }
    static PropertyDefinition Float(Property::Float v) { PropertyDefinition p; p.v_float = v; p.type = Property::Type::Float; return p; }
    static PropertyDefinition String(Property::String v) { PropertyDefinition p; p.v_string = v; p.type = Property::Type::String; return p; }
    static PropertyDefinition Color(Property::Color v) { PropertyDefinition p; p.v_color = v; p.type = Property::Type::Color; return p; }
    static PropertyDefinition Vec2(Property::Vec2 v) { PropertyDefinition p; p.v_vec2 = v; p.type = Property::Type::Vec2; return p; }
    static PropertyDefinition Vec2List(Property::Vec2List v) { PropertyDefinition p; p.v_vec2list = v; p.type = Property::Type::Vec2List; return p; }

    GettorFunctionPointer CreateGettor();
    SettorFunctionPointer CreateSettor();

  };
  typedef std::unordered_map<std::string, PropertyDefinition> PropertyDefinitionMap;
}
