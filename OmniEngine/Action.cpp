/******************************************************************************
Filename: Action.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Action.h"


void ActionList::Update(float dt)
{
  timeElapsed += dt;
  for (auto const &action : actions)
  {
    action->Update(dt);

    if (action->isBlocking)
      break;

    if (action->isFinished)
    {
      action->OnEnd();
      Remove(action);
    }
  }
}

void ActionList::PushFront(Action *action)
{
  actions.push_front(action);
}

void ActionList::PushBack(Action *action)
{
  actions.push_back(action);
}

void ActionList::InsertBefore(Action *existing, Action *action)
{
  auto found = std::find(actions.begin(), actions.end(), existing);
  if (found != actions.end())
    actions.insert(found, action);
}

void ActionList::InsertAfter(Action *existing, Action *action)
{
  auto found = std::find(actions.begin(), actions.end(), existing);
  if (found != actions.end())
    actions.insert(++found, action);
}

Action *ActionList::Remove(Action *action)
{
  Action *retAction = nullptr;
  auto found = std::find(actions.begin(), actions.end(), action);
  if (found != actions.end())
  {
    retAction = *found;
    actions.erase(found);
  }
  return retAction;
}


