/******************************************************************************
Filename: Scriptable.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "v8.h"

namespace Omni
{
  extern v8::Isolate *g_Isolate;

#define v8_RegisterComponentWithEntity(classname) \
  void Entity::v8_Get##classname(v8::Local<v8::String> prop, const v8::FunctionCallbackInfo<v8::Value> &info) \
  { \
  using namespace v8; \
  Locker locker(info.GetIsolate()); \
  Isolate::Scope isolate_scope(info.GetIsolate()); \
  HandleScope handleScope(info.GetIsolate()); \
  Entity *entity = Take<Entity>::FromHolder(info); \
  if (entity == nullptr) { info.GetReturnValue().Set(Undefined(info.GetIsolate())); return; } \
  classname *comp = static_cast<classname *>(*entity->GetComponent(#classname)); \
  if (comp == nullptr) { info.GetReturnValue().Set(Undefined(info.GetIsolate())); return; } \
  Handle<Object> result = comp->v8_GetObject(); \
  Handle<External> comp_ptr = External::New(info.GetIsolate(), comp->selfPtr); \
  result->SetInternalField(0, comp_ptr); \
  info.GetReturnValue().Set(result); \
  }

  // all scriptables MUST have a v8_Register function
  // and be called in Game::UpdateLoop()
  template <typename T>
  class Scriptable
  {
  public:

    v8::Persistent<v8::Object, v8::CopyablePersistentTraits<v8::Object>> persObj;
    static v8::Persistent<v8::ObjectTemplate, v8::CopyablePersistentTraits<v8::ObjectTemplate>> objTemplate;
    
    v8::Handle<v8::Object> v8_GetObject()
    {
      using namespace v8;

      Locker locker(g_Isolate);
      Isolate::Scope isolate_scope(g_Isolate);
      EscapableHandleScope handleScope(g_Isolate);

      // if we don't have a persistent object yet, make it
      if (persObj.IsEmpty())
      {
        Local<ObjectTemplate> newTemplate = Local<ObjectTemplate>::New(g_Isolate, objTemplate);
        Local<Object> newObject = newTemplate->NewInstance();

        persObj.Reset(g_Isolate, newObject);
      }

      Local<Object> returned = Local<Object>::New(g_Isolate, persObj);

      return handleScope.Escape<Object>(returned);
    }

    void ScriptableDestroy() 
    { 
      using namespace v8;
      Locker locker(g_Isolate);
      Isolate::Scope isolate_scope(g_Isolate);
      HandleScope handleScope(g_Isolate);

      persObj.Reset(); 
    }

    static void ScriptableShutDown() 
    { 
      using namespace v8;
      Locker locker(g_Isolate);
      Isolate::Scope isolate_scope(g_Isolate);
      HandleScope handleScope(g_Isolate);

      objTemplate.Reset(); 
    }
  };

  // instantiating static member variables
  template <typename T>
  v8::Persistent<v8::ObjectTemplate, v8::CopyablePersistentTraits<v8::ObjectTemplate>> Scriptable<T>::objTemplate;
}

