/******************************************************************************
Filename: PropertyHandle.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once
#include "Handlet.h"
#define PropertyHandle ModulePropertyHandle
// the above line makes me sad

namespace Omni
{

  class Component;

  struct ModulePropertyHandle
  {
    std::string property;
    Handlet<Component> component;

    ModulePropertyHandle(Handlet<Component> component_, std::string property_) : component(component_)
    {
      property = property_;
    }

    bool operator==(const ModulePropertyHandle &rhs) const;
    Handlet<Component> GetComponent();
  };
}

