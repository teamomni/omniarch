/******************************************************************************
Filename: BloomVShader.hlsl

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//
// Vertex Shader for Creating Bloom Effect
//

//-----------------------------------//
// Struct Passed from CPU            //
//-----------------------------------//
struct vInput
{
  float3 position  : POSITION;
  float2 texcoord  : TEXCOORD;

  float  threshold : THRESHOLD;
};

//-----------------------------------//
// Pass info onto pixel shader       //
//-----------------------------------//
struct vOutput
{
  float4 position  : SV_POSITION;
  float2 texcoord  : TEXCOORD;
  float  threshold : THRESHOLD;
};

//-----------------------------------//
// Main Function of Vertex Shader    //
//-----------------------------------//
vOutput VShader(vInput input)
{
  vOutput output;

  output.position = float4(input.position.xy * 2, 0, 1);
  output.texcoord = input.texcoord;
  output.threshold = input.threshold;

  return output;
}