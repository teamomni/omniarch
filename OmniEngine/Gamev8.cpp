/******************************************************************************
Filename: Gamev8.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Game.h"
#include <sstream>
#include "Input.h"
#include "Console.h"
#include "Entity.h"
#include "Space.h"
#include "RigidBody.h"
#include "v8Utilities.h"
#include "SoundEmitter.h"

namespace Omni
{
  using namespace v8;

  static void PrintMessage(const FunctionCallbackInfo<Value>& info)
  {
    if (info.Length() >= 1)
    {
      Locker locker(info.GetIsolate());
      Isolate::Scope isolate_scope(info.GetIsolate());
      HandleScope handleScope(info.GetIsolate());

      auto &game = Game::Instance();
      v8::Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

      Local<Value> message(info[0]);
      String::Utf8Value value(message);
      //Print the message to stdout
      printf("JAVASCRIPT SAYS : %s", *value);
    }
  }

  static void v8_CheckInput(const FunctionCallbackInfo<Value>& info)
  {
    if (info.Length() >= 1)
    {
      // get the string input
      Locker locker(info.GetIsolate());
      Isolate::Scope isolate_scope(info.GetIsolate());
      HandleScope handleScope(info.GetIsolate());

      auto &game = Game::Instance();
      Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

      Local<Value> check(info[0]);
      String::Utf8Value value(check);

      // check the input
      bool input = Input::Check(*value);

      // return the result
      info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), input));
    }
  }

  static void v8_CheckSystemInput(const FunctionCallbackInfo<Value>& info)
  {
    if (info.Length() >= 1)
    {
      // get the string input
      Locker locker(info.GetIsolate());
      Isolate::Scope isolate_scope(info.GetIsolate());
      HandleScope handleScope(info.GetIsolate());

      auto &game = Game::Instance();
      Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

      Local<Value> check(info[0]);
      String::Utf8Value value(check);

      // check the input
      bool input = Input::SystemCheck(*value);

      // return the result
      info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), input));
    }
  }

  static void v8_GetMousePosition(const FunctionCallbackInfo<Value>& info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    // get the mouse position
    Vec2 mousePos = game.GetSpaceByName("main")->GetMousePosition();

    // put the mouse pos into the vec2
    Handle<Object> vec2Instance = Object::New(info.GetIsolate());
    vec2Instance->Set(String::NewFromUtf8(info.GetIsolate(), "x"), Number::New(info.GetIsolate(), mousePos.x));
    vec2Instance->Set(String::NewFromUtf8(info.GetIsolate(), "y"), Number::New(info.GetIsolate(), mousePos.y));

    // return the result
    info.GetReturnValue().Set(vec2Instance);
  }

  void Game::v8_RegisterPrintMessage(Handle<ObjectTemplate> globalTemplate)
  {
    Locker locker(isolate);
    Isolate::Scope isolate_scope(isolate);
    HandleScope handleScope(isolate);

    globalTemplate->Set(String::NewFromUtf8(isolate, "print"), FunctionTemplate::New(isolate, FunctionCallback(PrintMessage)));
  }

  void Game::v8_RegisterCheckInput(Handle<ObjectTemplate> globalTemplate)
  {
    Locker locker(isolate);
    Isolate::Scope isolate_scope(isolate);
    HandleScope handleScope(isolate);

    globalTemplate->Set(String::NewFromUtf8(isolate, "CheckInput"), FunctionTemplate::New(isolate, FunctionCallback(v8_CheckInput)));
    globalTemplate->Set(String::NewFromUtf8(isolate, "CheckSystemInput"), FunctionTemplate::New(isolate, FunctionCallback(v8_CheckSystemInput)));
  }

  static void v8_Write(const FunctionCallbackInfo<Value> &info)
  {
    // get the string input
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    Local<Value> check(info[0]);
    String::Utf8Value value(check);

    Console::Write(*value);
  }

  static void v8_WriteLine(const FunctionCallbackInfo<Value> &info)
  {
    // get the string input
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    // if passed in a color, take it
    if (info.Length() > 1)
    {
      Console::WriteLine(Take<std::string>::FromV8(info[0]), static_cast<Console::Color>(Take<int>::FromV8(info[1])));
    }
    
    // if no color passed, just use default
    else
      Console::WriteLine(Take<std::string>::FromV8(info[0]));
  }

  static void v8_Info(const FunctionCallbackInfo<Value> &info)
  {
    // get the string input
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    Local<Value> check(info[0]);
    String::Utf8Value value(check);

    // get second info if it's there
    if (info[1]->IsString())
    {
      Local<Value> check2(info[1]);
      String::Utf8Value value2(check2);
      Console::Info(*value, *value2);

      return;
    }

    Console::Info(*value);
  }

  static void v8_Warning(const FunctionCallbackInfo<Value> &info)
  {
    // get the string input
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    Local<Value> check(info[0]);
    String::Utf8Value value(check);

    // get second info if it's there
    if (info[1]->IsString())
    {
      Local<Value> check2(info[1]);
      String::Utf8Value value2(check2);
      Console::Warning(*value, *value2);

      return;
    }

    Console::Warning(*value);
  }

  static void v8_Error(const FunctionCallbackInfo<Value> &info)
  {
    // get the string input
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    Local<Value> check(info[0]);
    String::Utf8Value value(check);

    // get second info if it's there
    if (info[1]->IsString())
    {
      Local<Value> check2(info[1]);
      String::Utf8Value value2(check2);
      Console::Error(*value, *value2);

      return;
    }

    Console::Error(*value);
  }

  void Game::v8_CreateConsole()
  {
    Locker locker(isolate);
    Isolate::Scope isolate_scope(isolate);
    HandleScope handleScope(isolate);

    Context::Scope contextScope(Local<Context>::New(isolate, context));

    Local<Object> console = Object::New(isolate);

    console->Set(String::NewFromUtf8(isolate, "Write"), Function::New(isolate, v8_Write));
    console->Set(String::NewFromUtf8(isolate, "WriteLine"), Function::New(isolate, v8_WriteLine));
    console->Set(String::NewFromUtf8(isolate, "Info"), Function::New(isolate, v8_Info));
    console->Set(String::NewFromUtf8(isolate, "Error"), Function::New(isolate, v8_Error));
    console->Set(String::NewFromUtf8(isolate, "Warning"), Function::New(isolate, v8_Warning));

    isolate->GetEnteredContext()->Global()->Set(String::NewFromUtf8(isolate, "Console"), console);
  }

  static void v8_Connect(const FunctionCallbackInfo<Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    // get the type we're connecting to
    Local<Value> check(info[1]);
    String::Utf8Value value(check);

    // get the name of the event
    Local<Value> eventName_raw(info[2]);
    String::Utf8Value eventName(eventName_raw);

    // if connecting to game NOT TESTED
    if (std::string(*value) == std::string("game"))
    {
      auto &game = Game::Instance();

      // unwrapping and getting the entity pointer
      Local<Object> self = Local<Object>::Cast(info[0]);
      Local<External> wrap = Local<External>::Cast(Local<Object>::Cast(info[3])->GetInternalField(0));
      Entity **ptr = static_cast<Entity **>(wrap->Value());
      if (ptr)
      {
        Entity *entity = *ptr;

        Local<Function> callback = Local<Function>::Cast(info[4]);

        game.dispatcher->ConnectEvent(*eventName, callback, self, entity);
      }
    }

    // if connecting to space
    else if (std::string(*value) == std::string("space"))
    {
      // getting the v8 object
      Local<Object> self = Local<Object>::Cast(info[0]);

      // unwrapping and getting the owner pointer
      Local<External> wrap = Local<External>::Cast(Local<Object>::Cast(info[3])->GetInternalField(0));
      Entity *entity = *static_cast<Entity **>(wrap->Value());

      // getting the space pointer
      Space *space;
      if (info[4]->IsString())
      {
        Local<Value> spaceName_raw(info[4]);
        String::Utf8Value spaceName(spaceName_raw);

        auto &game = Game::Instance();
        space = game.GetSpaceByName(*spaceName);
      }
      else
      {
        space = entity->space;
      }

      Local<Function> callback = Local<Function>::Cast(info[5]);

      if (space)
        space->dispatcher->ConnectEvent(*eventName, callback, self, entity);

      int x = 10;
      if (x) {}
    }

    // if connecting to entity
    else if (std::string(*value) == std::string("entity"))
    {
      // unwrapping and getting the entity pointer
      Local<Object> self = Local<Object>::Cast(info[0]);
      Local<External> wrap = Local<External>::Cast(Local<Object>::Cast(info[3])->GetInternalField(0));
      Entity **ptr = static_cast<Entity **>(wrap->Value());
      if (ptr)
      {
        Entity *entity = *ptr;

        Local<Function> callback = Local<Function>::Cast(info[4]);

        entity->dispatcher->ConnectEvent(*eventName, callback, self, entity);
      }

    }
  }

  static void v8_FindEntityByID(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    size_t id = size_t(Take<int>::FromV8(info[0]));

    Handlet<Entity> found = game.GetEntityByID(id);

    // only return if it actually exists
    if (found.isValid())
    {
      LocalObject foundObj = found->v8_GetObject();

      foundObj->SetInternalField(0, External::New(info.GetIsolate(), found->selfPtr));

      info.GetReturnValue().Set(foundObj);
    }

    // else return invalid
    else
    {
      info.GetReturnValue().Set(Undefined(info.GetIsolate()));
    }
  }

  static void v8_GetCollisionTableState(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    RigidBody::Group group1 = RigidBody::Group(Take<int>::FromV8(info[0]));
    RigidBody::Group group2 = RigidBody::Group(Take<int>::FromV8(info[1]));

    info.GetReturnValue().Set(Integer::New(info.GetIsolate(), int(RigidBody::GetCollisionTableState(group1, group2))));
  }

  static void v8_SetCollisionTableState(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    RigidBody::Group group1 = RigidBody::Group(Take<int>::FromV8(info[0]));
    RigidBody::Group group2 = RigidBody::Group(Take<int>::FromV8(info[1]));
    RigidBody::CollisionState state = RigidBody::CollisionState(Take<int>::FromV8(info[2]));

    RigidBody::SetCollisionTableState(group1, group2, state);
  }

  static void v8_FindEntityByLinkID(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    size_t linkID = size_t(Take<int>::FromV8(info[0]));

    Handlet<Entity> found = game.GetEntityByLinkID(linkID);
    if (found)
    {
      LocalObject foundObj = found->v8_GetObject();

      foundObj->SetInternalField(0, External::New(info.GetIsolate(), found->selfPtr));

      info.GetReturnValue().Set(foundObj);
    }
    else
      info.GetReturnValue().Set(Undefined(info.GetIsolate()));
  }

  static void v8_CastRay(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    Vec2 point = Take<Vec2>::FromV8(info[0]);
    Vec2 direction = Take<Vec2>::FromV8(info[1]);
    unsigned count = unsigned(Take<int>::FromV8(info[2]));

    // actually cast the ray
    auto results = RigidBody::CastRay(point, direction, &count);

    // now return the results as an array
    if (count > 0)
    {
      Handle<Array> ret = Array::New(info.GetIsolate(), int(count));

      // copy the data over to the array
      for (unsigned i = 0; i < count; ++i)
      {
        LocalObject obj = results[i]->v8_GetObject();
        obj->SetInternalField(0, External::New(info.GetIsolate(), results[i]->selfPtr));

        ret->Set(i, obj);
      }

      // return the array
      info.GetReturnValue().Set(ret);
    }
    else
    {
      // return empty array
      Handle<Array> ret = Array::New(info.GetIsolate(), 0);
      ret->Set(0, Integer::New(info.GetIsolate(), 0));
      info.GetReturnValue().Set(ret);
    }
  }

  static void v8_GetUniqueIDFromLinkID(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    size_t linkID = size_t(Take<int>::FromV8(info[0]));

    info.GetReturnValue().Set(Integer::New(info.GetIsolate(), game.GetUniqueIDFromLinkID(linkID)));
  }

  static void v8_QuitGame(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Game::Instance().Stop();
  }

  static void v8_PauseAllExcept(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));
    
    // if no arguments passed in, pause owner space
    if (info.Length() < 1)
    {
      size_t ownerID = size_t(info.Holder()->Get(String::NewFromUtf8(info.GetIsolate(), "owner_id"))->IntegerValue());
      Handlet<Entity> owner = Game::Instance().GetEntityByID(ownerID);
      if (owner.isValid())
        Game::Instance().PauseAllExcept(owner->space->name);
    }

    else
    {
      std::string except = Take<std::string>::FromV8(info[0]);

      Game::Instance().PauseAllExcept(except);
    }
  }

  static void v8_UnpauseAll(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Game::Instance().UnpauseAll();
  }

  static void v8_IsFullScreen(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), Game::Instance().GetGraphicsManager()->GetFullScreen()));
  }

  static void v8_SetFullScreen(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Game::Instance().GetGraphicsManager()->SetFullScreen(Take<bool>::FromV8(info[0]));
  }

  static void v8_CheckInputMethod(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    info.GetReturnValue().Set(Integer::New(info.GetIsolate(), int(Input::mode)));
  }

  static void v8_PauseAllSounds(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Game::Instance().GetSoundSystem()->PauseAll();
  }

  static void v8_UnpauseAllSounds(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Game::Instance().GetSoundSystem()->UnPauseAll();
  }

  static void v8_FindSpaceByName(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    std::string name = Take<std::string>::FromV8(info[0]);

    Space *space = Game::Instance().GetSpaceByName(name);

    if (space)
    {
      LocalObject spaceObj = space->v8_GetObject();
      spaceObj->SetInternalField(0, External::New(info.GetIsolate(), space));

      info.GetReturnValue().Set(spaceObj);
    }
    else
      info.GetReturnValue().Set(Undefined(info.GetIsolate()));
  }

  static void v8_SetDebugMessageSetting(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    game.debugMessageSetting = static_cast<DebugMessageSetting>(Take<int>::FromV8(info[0]));
  }

  static void v8_GetTimeScale(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    info.GetReturnValue().Set(Number::New(info.GetIsolate(), game.timescale));
  };

  static void v8_GetPlayMusic(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), SoundEmitter::playMusic));
  };

  static void v8_SetPlayMusic(v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    SoundEmitter::playMusic = Take<bool>::FromV8(value);
  };

  static void v8_GetPlaySoundEffects(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), SoundEmitter::playSoundEffects));
  };

  static void v8_SetPlaySoundEffects(v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    SoundEmitter::playSoundEffects = Take<bool>::FromV8(value);
  };

  static void v8_SetTimeScale(v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();

    // assign the actual value
    game.timescale = static_cast<float>(value->NumberValue());
  };

  static void v8_LoadLevel(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();

    // assign the actual value
    game.nextLevel = Take<std::string>::FromV8(info[0]);
    game.loadNextLevel = true;
    game.isEditing = false;
  };

  static void v8_GameDispatchEvent(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    std::string eventName = Take<std::string>::FromV8(info[0]);

    Game::Instance().dispatcher->DispatchEvent(eventName, LocalObject::Cast(info[1]));
  };

  static void v8_GetClientWidth(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    info.GetReturnValue().Set(Integer::New(info.GetIsolate(), game.GetWindowSystem()->GetClientWidth()));
  };

  static void v8_GetClientHeight(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    info.GetReturnValue().Set(Integer::New(info.GetIsolate(), game.GetWindowSystem()->GetClientHeight()));
  };

  static void v8_GetCurrentLevel(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    info.GetReturnValue().Set(String::NewFromUtf8(info.GetIsolate(), game.level.c_str()));
  };

  void Game::v8_ExposeGame()
  {
    Locker locker(isolate);
    Isolate::Scope isolate_scope(isolate);
    HandleScope handleScope(isolate);

    Context::Scope contextScope(Local<Context>::New(isolate, context));

    Local<Object> game = Object::New(isolate);

    game->Set(String::NewFromUtf8(isolate, "Connect"), Function::New(isolate, v8_Connect));
    game->Set(String::NewFromUtf8(isolate, "FindEntityByID"), Function::New(isolate, v8_FindEntityByID));
    game->Set(String::NewFromUtf8(isolate, "GetCollisionTableState"), Function::New(isolate, v8_GetCollisionTableState));
    game->Set(String::NewFromUtf8(isolate, "SetCollisionTableState"), Function::New(isolate, v8_SetCollisionTableState));
    game->Set(String::NewFromUtf8(isolate, "FindEntityByLinkID"), Function::New(isolate, v8_FindEntityByLinkID));
    game->Set(String::NewFromUtf8(isolate, "GetUniqueIDFromLinkID"), Function::New(isolate, v8_GetUniqueIDFromLinkID));
    game->Set(String::NewFromUtf8(isolate, "CastRay"), Function::New(isolate, v8_CastRay));
    game->Set(String::NewFromUtf8(isolate, "QuitGame"), Function::New(isolate, v8_QuitGame));
    game->Set(String::NewFromUtf8(isolate, "PauseAllExcept"), Function::New(isolate, v8_PauseAllExcept));
    game->Set(String::NewFromUtf8(isolate, "UnpauseAll"), Function::New(isolate, v8_UnpauseAll));
    game->Set(String::NewFromUtf8(isolate, "IsFullScreen"), Function::New(isolate, v8_IsFullScreen));
    game->Set(String::NewFromUtf8(isolate, "SetFullScreen"), Function::New(isolate, v8_SetFullScreen));
    game->Set(String::NewFromUtf8(isolate, "CheckInputMethod"), Function::New(isolate, v8_CheckInputMethod));
    game->Set(String::NewFromUtf8(isolate, "PauseAllSounds"), Function::New(isolate, v8_PauseAllSounds));
    game->Set(String::NewFromUtf8(isolate, "UnpauseAllSounds"), Function::New(isolate, v8_UnpauseAllSounds));
    game->Set(String::NewFromUtf8(isolate, "FindSpaceByName"), Function::New(isolate, v8_FindSpaceByName));
    game->Set(String::NewFromUtf8(isolate, "GetMousePosition"), Function::New(isolate, v8_GetMousePosition));
    game->Set(String::NewFromUtf8(isolate, "SetDebugMessageSetting"), Function::New(isolate, v8_SetDebugMessageSetting));
    game->Set(String::NewFromUtf8(isolate, "LoadLevel"), Function::New(isolate, v8_LoadLevel));
    game->Set(String::NewFromUtf8(isolate, "DispatchEvent"), Function::New(isolate, v8_GameDispatchEvent));
    game->SetAccessor(v8::String::NewFromUtf8(isolate, "timeScale"), AccessorGetterCallback(v8_GetTimeScale), AccessorSetterCallback(v8_SetTimeScale));
    game->SetAccessor(v8::String::NewFromUtf8(isolate, "clientWidth"), AccessorGetterCallback(v8_GetClientWidth), 0);
    game->SetAccessor(v8::String::NewFromUtf8(isolate, "clientHeight"), AccessorGetterCallback(v8_GetClientHeight), 0);
    game->SetAccessor(v8::String::NewFromUtf8(isolate, "currentLevel"), AccessorGetterCallback(v8_GetCurrentLevel), 0);
    game->SetAccessor(v8::String::NewFromUtf8(isolate, "playMusic"), AccessorGetterCallback(v8_GetPlayMusic), AccessorSetterCallback(v8_SetPlayMusic));
    game->SetAccessor(v8::String::NewFromUtf8(isolate, "playSoundEffects"), AccessorGetterCallback(v8_GetPlaySoundEffects), AccessorSetterCallback(v8_SetPlaySoundEffects));

//#if defined(_DEBUG) || defined(_RELEASE)
    game->Set(String::NewFromUtf8(isolate, "Debugging"), Boolean::New(isolate, true));
//#endif

    isolate->GetEnteredContext()->Global()->Set(String::NewFromUtf8(isolate, "Omni"), game);
  }
}
