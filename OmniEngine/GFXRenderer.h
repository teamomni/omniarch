/******************************************************************************
Filename: GFXRenderer.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// Juli Gregg
// GFXRenderer.h

#pragma once

#include <assert.h>



namespace Omni
{
  namespace Graphics
  {
    class Handle;
  }
}

#include "GFXTools.h"
#include "WindowSystem.h"
#include <string>
#include <vector>
#include <list>
#include "FW1FontWrapper.h"

namespace Omni
{

  namespace Graphics
  {

    class GFXRenderer
    {
    public:
      GFXRenderer(void){};
      ~GFXRenderer(void);

      //Function to check if already initialized
      bool IsInitialized(void) const;

      //Function that Initializes the Render stuff
      //Should return true if succesful
      bool Initialized(WindowSystem &Window);

      //Uninitializes the RenderContext
      //Releases stuff
      void UnInitialize(void);

      bool ErrorCheck(HRESULT, bool assert = true, std::string message = "GRAPHICS ERROR");
      void ErrorCheck(std::string message = "GRAPHICS ERROR", bool assert = true);

      //Wrapper Around DirectX Draw Calls
      //---------------------------------

      void Draw(unsigned vertCount, unsigned vertStart = 0);
      void DrawIndexed(unsigned indexCount, unsigned indexStart = 0, unsigned vertStart = 0);
      void DrawInstanced(unsigned vertCount, unsigned instCount, unsigned vertStart = 0, unsigned instStart = 0);
      void DrawIndexedInstanced(unsigned indexCountPerInst, unsigned instCount, unsigned indexStart = 0, unsigned vertStart = 0, unsigned instStart = 0);
      void Present(void);


      //Wrapper Around DirectX Create Calls
      //--------------------------------

      bool CreateVertexShader(Handle& handle, const std::string& filename, const std::string& entryFunc = "VShader");
      bool CreateVertexShader(Handle& handle, const std::string& filename, InputLayout&, const std::string& entryFunc = "VShader");
      bool CreatePixelShader(Handle& handle, const std::string& filename, const std::string& entryFunc = "PShader");
      bool CreateTexture(Handle& handle, const std::string& filename, std::mutex* mtx);
      bool CreateVertexBuffer(Handle& handle);
      bool CreateIndexBuffer(Handle& handle);
      bool CreateIndexBuffer(Handle& handle, DWORD* indexes, unsigned size);
      bool CreateConstantBuffer(Handle& handle, unsigned size);
      bool CreateRenderTarget(Handle& handle, const float downsamplePercentager = 1.0f);
      bool CreateInstanceBuffer(Handle& handle, void * data, size_t size);
      bool CreateRasterizerStateObject(Handle& handle, bool WireFrame = false, bool Antialiased = false, bool Multisampled = false);
      void CreateBlendMode(Handle& handle, BlendModes* mode);
      void CreateBlendMode(Handle& handle, BlendModes);

      //Wrapper Around DirectX Bind Calls
      //--------------------------------s

      void BindVertexShader(const Handle& vsHandle);
      void BindPixelShader(const Handle& psHandle);
      void BindTexture(unsigned slot, const Handle& texHandle);
      void BindTextures(unsigned count, const Handle texHandles[], unsigned startSlot = 0);
      void UnBindTexture(unsigned slot);
      void BindRenderTargetAsTexture(Handle& rTarget, unsigned slot = 0);
      void BindVertexBuffer(const Handle& vbHandle, size_t stride, unsigned slot = 0, size_t offset = 0);
      void BindVertexBuffers(unsigned count, const Handle vertexBuffers[], size_t strides[], size_t offsets[]);
      void BindIndexBuffer(const Handle& ibHandle);
      void BindConstantBuffer(unsigned slot, const Handle& cbHandle);
      void BindBackBuffer(void);
      void BindRenderTarget(const Handle& rtHandle);
      void BindRenderTargets(unsigned count, const Handle* rtHandles);
      void UnBindRenderTargets(void);

      void BindRasterizerStateObject(Handle& handle);


      //Setters
      //--------------------------------

      void SetClearColor(const float r, const float g, const float b, const float a);
      void SetClearColor(const D3DXCOLOR& color);
      void SetFullScreen(const bool fullscreen);
      void SetPrimitiveTopology(const PrimitiveTopology primitiveTopology);
      void SetViewport(int xOffset, int yOffset, float dimensionX, float dimensionY);
      void SetViewport(const Viewport& viewport);
      void SetBlendMode(Handle& bHandle);
      void SetVSync(bool vsync);

      //Getters
      //--------------------------------

      Handle GetBackBuffer(void) const;
      bool GetFullScreen(void) const;
      ID3D11Device* GetDevice(void) const;
      ID3D11DeviceContext* GetDeviceContext(void) const;
      D3DXCOLOR GetClearColor(void) const;
      inline unsigned GetRasterizerStateObjCount(void) { return m_rasterizerStateObjRes.size(); }

      //Utilities
      //--------------------------------

      void ClearBackBuffer(void);
      void ClearRenderTarget(Handle& rTarget);
      void Resize(LPARAM);
      void UpdateConstantBuffer(const Handle& cHandle, D3DXMATRIX& cameraMatrix);
      void UpdateConstantBuffer(const Handle& cHandle, D3DXMATRIX* cameraMatrix);
      inline unsigned GetClientWidth() { return m_resX; };
      inline unsigned GetClientHeight() { return m_resY; };
      void SetWindowActive(WPARAM wparam, LPARAM lparam);

      //Public Release Function
      //--------------------------------
      void Release(Handle &handle);

    private:


      //PRIVATE INITIALIZE FUNCTIONS
      //--------------------------------

      void InitializeDXGI(void);
      void InitializeDeviceAndSwapChain(void);
      void InitializeBackBuffer(void);
      void InitializeViewport(void);
      void InitializeBlendModes(void);
      void InitializeSamplerState(void);


      // INTERNAL RELEASE FUNCTIONS
      //--------------------------------

      void ReleaseTextureIntern(const Handle& texture);
      void ReleaseVertexShaderIntern(const Handle& vertexShader);
      void ReleasePixelShaderIntern(const Handle& pixelShader);
      void ReleaseVertexBufferIntern(const Handle& vertexBuffer);
      void ReleaseIndexBufferIntern(const Handle& indexBuffer);
      void ReleaseConstantBufferIntern(const Handle& constantBuffer);
      bool ReleaseRenderTargetIntern(const Handle& renderTarget);
      void ReleaseBlendStateIntern(const Handle& blendMode);
      void ReleaseRasterizerObjectIntern(const Handle& rasterizerObject);

      //Utilities
      //--------------------------------
      bool FindEmptySlot(Handle&, OBJT);


      //Structs
      //--------------------------------

      struct Texture
      {
        ID3D11ShaderResourceView  *sResourceView = nullptr;
        //ID3D11Texture2D           *texture;
        //unsigned                   height;
        //unsigned                   width;
      };

      struct VertexShader
      {
        ID3D11VertexShader  *vShader = nullptr;
        ID3D11InputLayout   *inputLayout = nullptr;
      };

      struct RenderTarget
      {
        ID3D11RenderTargetView    *renderTargetView = nullptr;
        ID3D11ShaderResourceView  *sResourceView = nullptr;
        ID3D11Texture2D           *texture2D = nullptr;
        DXGI_FORMAT                format;
        float                      downssamplePercentage = -1.f;
        //unsigned                   width;
        //unsigned                   height;
      };

      //System
      //--------------------------------

      bool        m_initialized = false;
      HWND        m_hwnd;
      unsigned    m_resX = 0;
      unsigned    m_resY = 0;
      Vec2        m_resFullScreen;
      Vec2        m_nativeRes;
      bool        m_fullscreen = false;
      bool        m_vsync = false;
      BlendModes  m_blendMode = BlendModes::Alpha;
      bool        m_changingFSMode = false;


      //DirectX
      //--------------------------------

      IDXGISwapChain      *m_swapChain = nullptr;
      ID3D11Device        *m_device = nullptr;
      ID3D11DeviceContext *m_deviceContext = nullptr;
      ID3D11BlendState    *m_blendState = nullptr;
      //IDXGIFactory        *m_factory;
      //IDXGIAdapter        *m_adapter;
      //IDXGIOutput         *m_output;
     

      D3DXCOLOR            m_clearColor;

      ID3D11RenderTargetView      *m_backBuffer = nullptr;

      ID3D11SamplerState          *m_sampleStates = nullptr;

      //Resources
      //--------------------------------
      std::vector<VertexShader>           m_vertexShaderRes;
      std::vector<ID3D11PixelShader*>     m_pixelShaderRes;
      std::vector<Texture>                m_textureRes;
      std::vector<ID3D11Buffer*>          m_vertexBufferRes;
      std::vector<ID3D11Buffer*>          m_indexBufferRes;
      std::vector<ID3D11Buffer*>          m_constBufferRes;
      std::vector<RenderTarget>           m_renderTargetRes;
      std::vector<ID3D11RasterizerState*> m_rasterizerStateObjRes;
      std::vector<ID3D11BlendState*>      m_blendStateRes;

      //Resource Vacancy Management
      //--------------------------------
      std::list<Handle> m_vacancies;

      //Pointer to Current Window System
      //--------------------------------
      WindowSystem *m_windowSystem = nullptr;
    };
  }
}
