/******************************************************************************
Filename: Game.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Game.h"
#include "Sprite.h"
#include "WindowSystem.h"
#include "GFXManager.h"
#include "SoundSystem.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include "RigidBody.h"
#include "Shape.h"
#include "GFXDebugDraw.h"
#include "Input.h"
#include "HitboxManager.h"
#include "Animator.h"
#include <sstream>
#include "libplatform/libplatform.h"
#include "Console.h"
#include "SoundEmitter.h"
#include "Event.h"
#include "Component.h"
#include "Clickable.h"
#include "Editor.h"
#include "SpriteText.h"
#include "BloomEffect.h"
#include "BlurEffect.h"
#include "ParticleEmitter.h"


#include <shlobj.h>
#pragma comment(lib, "shell32.lib")

#if _CHECK_LEAKS
#pragma comment (lib, "C:\\Program Files (x86)\\Visual Leak Detector\\lib\\Win32\\vld.lib")
#include <vld.h>
#endif

//TwBar *myBar = NULL;

namespace Omni
{
  const float RigidBody::physicsFrameRate = 60;
  const float RigidBody::physicsTimestep = 1 / RigidBody::physicsFrameRate;
  const float Game::s_frameTime = 1 / Game::s_tickRate;

  v8::Isolate *g_Isolate;

  std::mutex Game::mtx;
  std::mutex Game::mtx_input;
  std::queue<MSG *> Game::s_messages;
  std::condition_variable Game::s_messageCondition;

  static void TW_CALL CopyStdStringToClient(std::string& destinationClientString, const std::string& sourceLibraryString)
  {
    // Copy the content of souceString handled by the AntTweakBar library to destinationClientString handled by your application
    destinationClientString = sourceLibraryString;
  }

  void Game::Init()
  {
    HMODULE hModule = GetModuleHandleW(NULL);
    TCHAR nPath[MAX_PATH];

#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    TwCopyStdStringToClientFunc(CopyStdStringToClient);
#endif

    // This gets the current directory of the executable and strips the executable filename itself
    GetModuleFileNameW(hModule, nPath, MAX_PATH);
    size_t len = wcslen(nPath);
    if (len == 0) { return; };
    size_t idx = len - 1;
    while (TRUE)
    {
      TCHAR chr = nPath[idx];
      if (chr == TEXT('\\') || chr == TEXT('/'))
      {
        if (idx == 0 || nPath[idx - 1] == ':') { idx++; }
        break;
      }
      else if (chr == TEXT(':'))
      {
        idx++; break;
      }
      else
      {
        if (idx == 0) { break; }
        else { idx--; };
      }
    }
    nPath[idx] = TEXT('\0');

    fileLocation = nPath;

    // Show the console window if we're in debug mode
//#if defined _DEBUG || defined _RELEASE
#define _CONSOLE_ENABLED
//#endif
    Console::Init(!isFinal);

    std::cout << "*** HOOK: Game:GameStarted ***" << std::endl;


    // create the dispatcher
    dispatcher = new EventDispatcher;
    dispatcher->type = EventDispatcher::Type::Game;

    // start the game in "First"
    if (!isRestarting)
      nextLevel = "SplashScreens";

    // REGISTER SYSTEMS HERE
    RigidBody::Register();
    Sprite::Register();
    HitBoxManager::Register();
    Animator::Register();
    SoundEmitter::Register();
    SpriteText::Register();
    BloomEffect::Register();
    BlurEffect::Register();
    ParticleEmitter::Register();

  }

  void Game::InitTest()
  {
    isTesting = true;
    LoadDataAssets();
  }

  void Game::Init(HINSTANCE hInstance_, unsigned iconID_)
  {
    isTesting = false;
    hInstance = hInstance_;
    iconID = iconID_;
    serializeCamera = false;

    Init();

    // make the editor
    editor = new Editor;

    // if shutting down
    if (!isRestarting)
    {
      lastCreatedArchetype = "";

      // Create the window
      windowSystem = new WindowSystem(hInstance, iconID);

#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
      //Activate Window (do this before graphics is initialized when not in publish mode)
      //windowSystem->ActivateWindow();
#endif

      //Create the Graphics Manager
      graphicsManager = new Graphics::GFXManager();

      //Initialize the Graphics & Error Check
      if (!graphicsManager->Initialize(*windowSystem))
      {
        std::cout << "FAILED TO INITIALIZE GRAPHICS MANAGER" << std::endl;
        ShutdownDueToGraphicsCard();
        throw(true);
        assert(0);
      }

//#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
//#else
      graphicsManager->m_renderer->SetFullScreen(true);
      graphicsManager->SetFullScreen(true);
	    //windowSystem->ActivateWindow();
//#endif

    }
    
//#if _DEBUG
    //DebugDraw
    debugDraw = new Graphics::GFXDebugDraw();
//#endif


    // if restarting or startup
    if (!loadNextLevel)
    {
      soundSystem = new SoundSystem;
      soundSystem->Initialize();

      UINT32 color = 255 << 24 | 255 << 16 | 255 << 8 | 255;
      RenderTextObject rTO = RenderTextObject("Loading...", "Ariel", 50, color, Vec2(float(windowSystem->GetClientWidth() / 2 - 100), float(windowSystem->GetClientHeight() / 2)), 0, true);
      renderStates[0].textObjects.push_back(rTO);
      graphicsManager->Present(renderStates[0], true);

      LoadDataAssets();
    }

    // if loading next level
    else
    {
      //soundSystem->LoadBank("Master Bank.bank");
      //soundSystem->LoadBank("Master Bank.strings.bank");
    }

    // do this all the time!
    Input::Initialize();
  }

  void Game::Update(float dt)
  {
    // real dt (unscaled)
    float unscaledDt = dt;
    dt *= timescale;

    Cleanup();
//#if _DEBUG
    debugDraw->ClearVectors();
//#endif
    Input::Update();

    if (Input::modeChanged)
      std::cout << "INPUT MODE IS NOW: " << (Input::mode == Input::Mode::Keyboard ? "keyboard" : "gamepad") << std::endl;

    //std::cout << "#";

    if (!isEditing)
    {
      // PHYSICS FUNCTIONS
      // add the current frametime to the accumulator
      RigidBody::accumulator += unscaledDt;
      RigidBody::loopsPerFrame = 0;

      // clamp the accumulator to only thrice per frame
      if (RigidBody::accumulator > 3 * RigidBody::physicsTimestep)
        RigidBody::accumulator = 3 * RigidBody::physicsTimestep;


      // keep updating physics until all dt is 'gone'
      while (RigidBody::accumulator > RigidBody::physicsTimestep)
      {
        RigidBody::manifolds.clear();
        RigidBody::Run(RigidBody::physicsTimestep);
        RigidBody::Step(RigidBody::physicsTimestep);
        RigidBody::accumulator -= RigidBody::physicsTimestep;
        ++(RigidBody::loopsPerFrame);
        RigidBody::manifolds.clear();
      }

      // clear all forces
      RigidBody::EndFrame();


      Animator::Run(dt);


      HitBoxManager::HitBoxStep(dt);

      // RUN SYSTEMS HERE
      HitBoxManager::Run(dt);


      SoundEmitter::Run(dt);
    }

    Loop(unscaledDt);
    for (auto &space : spaces)
    {
      using namespace v8;
      Locker locker(isolate);
      Isolate::Scope isolate_scope(isolate);
      HandleScope handleScope(isolate);

      Context::Scope contextScope(Local<Context>::New(isolate, context));

      space.SpawnCreatedEntities();
    }

    if (!isTesting)
      soundSystem->Update();

    Clickable::Step();
    editor->Update();

    // SPRITE ALWAYS GOES LAST
    Sprite::Run(dt);
    ParticleEmitter::Run(dt);
    SpriteText::Run(dt);
    BloomEffect::Run(dt);
    BlurEffect::Run(dt);
    
  }

  void Game::Render(RenderState *renderState, float dt)
  {
    // RUN DRAW SYSTEM HERE
    std::sort(renderState->objects.begin(), renderState->objects.end(), [](const RenderObject &a, const RenderObject &b) -> bool
    {
      return a.depth > b.depth;
    });
    graphicsManager->Present(*renderState);

  }

  void Game::Shutdown()
  {
    //spaces.clear();`
    v8::Locker locker(isolate);
    v8::Isolate::Scope isolate_scope(isolate);
    v8::HandleScope handleScope(isolate);

    for (auto it = spaces.begin(); it != spaces.end(); ++it)
    {
      if (!it->destroying)
      {
        it->destroying = true;
        destroyingSpaces.push_back(&*it);
      }
    }
    Cleanup();

    // SHUT DOWN SYSTEMS HERE
    RigidBody::Shutdown();
    Sprite::Shutdown();
    BloomEffect::Shutdown();
    BlurEffect::Shutdown();
    HitBoxManager::Shutdown();
    Animator::Shutdown();
    SoundEmitter::Shutdown();
    ParticleEmitter::Shutdown();

    Transform::ScriptableShutDown();
    Space::ScriptableShutDown();
    Camera::ScriptableShutDown();
    Entity::ScriptableShutDown();
    Clickable::ScriptableShutDown();

    // destroy events
    Events::UpdateEvent::ScriptableShutDown();
    Events::HitEvent::ScriptableShutDown();
    Events::CollisionEvent::ScriptableShutDown();

    dispatcher->Destroy();

    // if restarting or shutting down
    if (!loadNextLevel)
    {
      for (auto &texture : assets.textures)
      {
        graphicsManager->m_renderer->Release(texture.second);
      }
      assets.textures.clear();
      assets.animations.clear();
      assets.images.clear();
      soundSystem->ShutDown();
      delete soundSystem;
    }

    // if loading another level
    if (loadNextLevel)
    {
      soundSystem->ReleaseAllSoundCues();
    }

    linkIDtoUniqueID.clear();
    assets.archetypes.clear();
    entityHandles.clear();
    systemContainers.clear();
    spaces.clear();
    fileLocation.clear();
    namedSpaces.clear();
    destroyingSpaces.clear();
    for (int i = 0; i < 3; ++i)
      renderStates[i].Clear();
    state_rendering = 0;
    state_updating = 1;
    state_lastUpdated = 2;
    context.Reset();

    //TwDeleteAllBars();

    tick = 0;

    while (s_messages.size() > 0)
      s_messages.pop();

    // if shutting down
    if (!isRestarting)
    {
      graphicsManager->Shutdown();
    }

    // clear all the creation functions
    for (auto &handle : UserComponent::creationFunctions)
    {
      handle.second.Reset();
    }

    Entity::ResetIDCounting();
    UserComponent::Shutdown();
    UserComponent::creationFunctions.clear();

    // clear all the obj templates
    Space::ScriptableShutDown();
    Transform::ScriptableShutDown();
    Camera::ScriptableShutDown();


//#if _DEBUG
    delete debugDraw;
//#endif
    delete editor;
    if (!isRestarting)
    {
      //delete debugDraw;
      delete graphicsManager;
      delete windowSystem;
    }

    loaded = false;
  }
  
  void Game::ShutdownDueToGraphicsCard(void)
  {

    for (auto &texture : assets.textures)
    {
      graphicsManager->m_renderer->Release(texture.second);
    }
    assets.textures.clear();
    assets.animations.clear();
    assets.images.clear();


    linkIDtoUniqueID.clear();
    assets.archetypes.clear();
    entityHandles.clear();
    systemContainers.clear();
    spaces.clear();
    fileLocation.clear();
    namedSpaces.clear();
    destroyingSpaces.clear();
    for (int i = 0; i < 3; ++i)
      renderStates[i].Clear();
    state_rendering = 0;
    state_updating = 1;
    state_lastUpdated = 2;
    context.Reset();

    //TwDeleteAllBars();

    tick = 0;

    while (s_messages.size() > 0)
      s_messages.pop();


    // clear all the creation functions
    for (auto &handle : UserComponent::creationFunctions)
    {
      handle.second.Reset();
    }

    Entity::ResetIDCounting();
    UserComponent::Shutdown();
    UserComponent::creationFunctions.clear();

    delete editor;
    delete graphicsManager;
    delete windowSystem;

    isRunning = false;
  }

  void Game::Loop(float dt)
  {
    float unscaledDt = dt;
    dt *= timescale;

    std::for_each(spaces.begin(), spaces.end(), [&](Space &space)
    {
      space.Cleanup();
      if (!space.destroying)
      {
        using namespace v8; 
        Locker locker(isolate);
        Isolate::Scope isolate_scope(isolate);
        HandleScope handleScope(isolate);

        Context::Scope contextScope(Local<Context>::New(isolate, context));

        // only dispatch the event if not editing
        if (!isEditing)
        {
          // dispatch logic update event only if timescale is bigger than 0
          if (dt > 0 && !space.paused)
          {
            Events::UpdateEvent updateEvent(dt);
            LocalObject updateEventObj = updateEvent.v8_GetObject();
            updateEventObj->SetInternalField(0, External::New(isolate, &updateEvent));
            space.dispatcher->DispatchEvent("onLogicUpdate", updateEventObj);
          }
          
          // dispatch frame update event
          Events::UpdateEvent frameEvent(unscaledDt);
          LocalObject frameEventObj = frameEvent.v8_GetObject();
          frameEventObj->SetInternalField(0, External::New(isolate, &frameEvent));
          space.dispatcher->DispatchEvent("onFrameUpdate", frameEventObj);
        }

        auto entities = space.GetAllEntities();
        std::for_each(entities.begin(), entities.end(), [&](Entity &entity)
        {
          if (entity.active)
          {
            // entity handling here
            entity.clickable.Update();

          }
        });

        space.camera.Update(dt);
      }
    });
  }

  void Game::TestLoop()
  {
    Loop(0.016667f);
  }

  void Game::Start(HINSTANCE hInstance_, unsigned iconID_, const char *dataLocation_, bool isFinal_)
  {
    // This happens ONLY ONCE
    dataLocation = std::string(dataLocation_);
    dataLocation_w.assign(dataLocation.begin(), dataLocation.end());
    isRunning = true;
    loadNextLevel = false;
    isFinal = isFinal_;

    // Anything from here down will happen MULTIPLE TIMES
    while (isRunning || isRestarting)
    {
      try
      {
        Game::Init(hInstance_, iconID_);
      }
      catch (...)
      {
        break;
      }

      isRunning = true;
      isRestarting = false;

      if (!ranOnce)
      {
        v8::V8::InitializeICU();
        v8::Platform *platform = v8::platform::CreateDefaultPlatform();
        v8::V8::InitializePlatform(platform);
        v8::V8::Initialize();
      }
      isolate = v8::Isolate::New();
      g_Isolate = isolate;

      if (isEditing)
      {
        windowText = "OmniArch - EDIT MODE";
      }
      else
      {
        windowText = "OmniArch";
      }

      UpdateLoop();

      std::cout << "UPDATE JOINED" << std::endl;

      Shutdown();
      g_Isolate->Dispose();
      ranOnce = true;
    }
  }

  void Game::StartTest()
  {
    Game::InitTest();
    isRunning = true;
    isRestarting = false;
    if (!ranOnce)
    {
      v8::V8::InitializeICU();
      v8::Platform *platform = v8::platform::CreateDefaultPlatform();
      v8::V8::InitializePlatform(platform);
      v8::V8::Initialize();
    }
    isolate = v8::Isolate::New();
    g_Isolate = isolate;
    //UpdateLoop();

    std::cout << "UPDATE JOINED" << std::endl;

    //Shutdown();
    ranOnce = true;
  }

  void Game::UpdateLoop()
  {
    bool initialized = false;


    //auto test = Game::Instance().GetSpaceByName("main")->CreateEntity("Test");
    //test->AddComponent("SoundEmitter");
    //auto sEmit = test->GetComponent("SoundEmitter");
    //sEmit->SetProperty_String("soundCue", "event:/player_jump_grunt");
    //test->transform.translation = Vec2(0,0);
    //test->SpawnTweakBar();

    using namespace std::chrono;
    auto lastSecond = high_resolution_clock::now();
    auto nextSecond = lastSecond + seconds(1);
    auto currentTime = high_resolution_clock::now();
    unsigned frames = 0;

    while (isRunning)
    {
#pragma region V8 initialization stuff

      if (!initialized)
      {
        LoadJavaScriptAssets();
      }

      if (!initialized)
      {
        // REGISTER SCRIPTABLE OBJECTS
        Entity::v8_Register();
        RigidBody::v8_Register();
        SoundEmitter::v8_Register();
        ParticleEmitter::v8_Register();
        Animator::v8_Register();
        Sprite::v8_Register();
        Space::v8_Register();
        Camera::v8_Register();
        Transform::v8_Register();
        Clickable::v8_Register();
        UserComponent::v8_Register();
        BloomEffect::v8_Register();
        BlurEffect::v8_Register();

        // REGISTER EVENTS
        Events::UpdateEvent::v8_Register();
        Events::HitEvent::v8_Register();
        Events::CollisionEvent::v8_Register();

#pragma endregion

        LoadLevel(nextLevel);        

        lastSecond = high_resolution_clock::now();
        nextSecond = lastSecond + seconds(1);
        currentTime = high_resolution_clock::now();

        clock.Start();

        initialized = true;
      }

      //int numHandles = handleScope.NumberOfHandles(g_Isolate);
      //printf("NUMBER OF HANDLES : %d\n", numHandles);

      // <HACKY HACKY HACK HACK>
      /*for (auto &space : spaces)
      {
        space.PadEntityVector();
      }*/
      // </HACKY HACKY HACK HACK>

      /**************************
      * Update stuff goes here  *
      **************************/

      // get the ELAPSED time
      float dt = clock.Elapsed();

      // START the clock for this frame
      clock.Start();

      // ENGIINE LEVEL INPUT STUFF //
      if (Input::SystemCheck("console") && !isFinal)
      {
        editor->m_active ^= true;
      }

      // EDITOR INPUT STUFF //
      if (editor->m_active)
      {
        if (Input::SystemCheck("create_terrain_at_mouse"))
          editor->CreateFloorAtMousePosition(GetSpaceByName("main"));
        if (Input::SystemCheck("create_entity_at_mouse"))
          editor->CreateEntityAtMousePosition(GetSpaceByName("main"));
        if (Input::SystemCheck("create_last_archetype"))
          editor->CreateEntityAtMousePosition(GetSpaceByName("main"), lastCreatedArchetype);
        // zoom in / out the main space
        if (Input::Check("zoom_in"))
          GetSpaceByName("main")->camera.size -= 0.2f;

        if (Input::Check("zoom_out"))
          GetSpaceByName("main")->camera.size += 0.2f;
      }

      // autoplay is = (not numpad)
      if (Input::SystemCheck("autoplay"))
        Input::autoplay = true;

      // if we are in autoplay, escape cancels it
      if (Input::SystemCheck("deactivate_autoplay"))
        Input::autoplay = false;


      //mtx.lock();
      loaded = true;

      //auto frameDiff = high_resolution_clock::now() - currentTime;
      //currentTime = high_resolution_clock::now();
      //auto nextFrameStart = currentTime + frameLength{ 1 };

      RenderState &renderState = renderStates[state_updating];
      renderState.BeginUpdate(tick);

      Update(dt);

      for (auto const &space : spaces)
      {
        renderState.cameras.push_back(space.camera.GetScaledTransform());
      }

      renderState.EndUpdate(tick);

      ++tick;
      state_lastUpdated = state_updating;

      short prev = state_updating;

      // Get the right state (SUPER INEFFICIENT PROBABLY)
      if (state_rendering == 0)
        state_updating = renderStates[1].endTick <= renderStates[2].endTick ? 1 : 2;
      if (state_rendering == 1)
        state_updating = renderStates[0].endTick <= renderStates[2].endTick ? 0 : 2;
      if (state_rendering == 2)
        state_updating = renderStates[0].endTick <= renderStates[1].endTick ? 0 : 1;
      //mtx.unlock();
      //std::cout << "UPDATE" << std::endl;

      if (!isFinal)
      {
        if (Input::SystemCheck("load_level"))
        {
          loadNextLevel = true;
          isEditing = false;
          Sprite::isEditor = false;
        }
        if (Input::SystemCheck("restart"))
        {
          windowText = "OmniArch - LOADING";
          isEditing = false;
          Sprite::isEditor = false;
          Restart();
        }
        if (Input::SystemCheck("editor_activate"))
        {
          windowText = "OmniArch - LOADING";
          isEditing = true;
          loadNextLevel = true;
          Sprite::isEditor = true;
          Restart();
        }

        if (Input::SystemCheck("editor_save") && isEditing)
        {
          Serialize();
        }
      }


      if (!isTesting)
      {
        MSG msg = { 0 };
        //Check for Esc key to be pressed then break out of the loop
        if (PeekMessage(&msg, GetActiveWindow(), 0, 0, PM_REMOVE))
        {
          TranslateMessage(&msg);
          DispatchMessage(&msg);

          if (msg.message == WM_QUIT)
            Stop();
        }
      }

      //mtx.unlock();

      /**************************
      * Render stuff goes here  *
      **************************/
      state_rendering = state_lastUpdated;

      RenderState toRender = renderStates[state_rendering];


      //std::cout << "            RENDER" << std::endl;

      // pass renderState to Renderer
      Render(&toRender, dt);

      if (!isTesting)
      {
        MSG msg = { 0 };
        //Check for Esc key to be pressed then break out of the loop
        if (PeekMessage(&msg, GetActiveWindow(), 0, 0, PM_REMOVE))
        {
          TranslateMessage(&msg);
          DispatchMessage(&msg);

          if (msg.message == WM_QUIT)
            Stop();
        }
      }
      if (windowText != windowTextLast)
      {
        SetWindowText(windowSystem->GetWindowHandle(), std::wstring(windowText.begin(), windowText.end()).c_str());
        windowTextLast = windowText;
      }

      /**************************
      *  Framerate Controller   *
      **************************/
      ++frames;
      if (clock.Current() - clock.secondStart.QuadPart >= clock.TimeInTicks(1.0f))
      {
        //char buffer[30];
        //sprintf(buffer, "Frames: %d", frames);
        //Console::WriteLine(buffer);
        framesPerSecond_update = (float)frames;
        frames = 0;
        QueryPerformanceCounter(&clock.secondStart);
      }
      
      
      int ms = int((s_frameTime - clock.Elapsed()) * 1000);
      if (ms > 0)
      {
        isolate->IdleNotification(ms);
		ms = int((s_frameTime - clock.Elapsed()) * 1000);
        milliseconds sleepTime(ms);
        std::this_thread::sleep_for(sleepTime);
      }

      if (loadNextLevel)
      {
        Game::Restart();
      }
    }
  }

  void Game::RenderLoop()
  {
    using namespace std::chrono;
    auto lastSecond = high_resolution_clock::now();
    auto nextSecond = lastSecond + seconds(1);
    auto currentTime = high_resolution_clock::now();
    unsigned frames = 0;
    while (isRunning)
    {
      mtx.lock();
      auto frameDiff = high_resolution_clock::now() - currentTime;
      currentTime = high_resolution_clock::now();


      //mtx.lock();
      state_rendering = state_lastUpdated;

      RenderState renderState = renderStates[state_rendering];
      

      //std::cout << "            RENDER" << std::endl;

      // pass renderState to Renderer
      Render(&renderState, (float)duration_cast<milliseconds>(frameDiff).count() / 1000.0f);

      if (!isTesting)
      {
        MSG msg = { 0 };
        //Check for Esc key to be pressed then break out of the loop
        if (PeekMessage(&msg, GetActiveWindow(), 0, 0, PM_REMOVE))
        {
          TranslateMessage(&msg);
          DispatchMessage(&msg);

          if (msg.message == WM_QUIT)
            Stop();
        }
      }

      //std::this_thread::sleep_for(milliseconds(10));
      ++frames;
      if (currentTime >= nextSecond)
      {
        framesPerSecond_render = (float)frames / (float)duration_cast<milliseconds>(currentTime - lastSecond).count() * 1000.0f;
        frames = 0;
        lastSecond = currentTime;
        nextSecond = lastSecond + seconds(1);
        //std::cout << "----" << std::endl << "Update: " << framesPerSecond_update << std::endl << "Render: " << framesPerSecond_render << std::endl;
      }
      if (windowText != windowTextLast)
      {
        SetWindowText(windowSystem->GetWindowHandle(), std::wstring(windowText.begin(), windowText.end()).c_str());
        windowTextLast = windowText;
      }
      mtx.unlock();
    }
  }

  bool Game::GetInput(MSG msg)
  {
    if (!isTesting)
    {
      //Check for Esc key to be pressed then break out of the loop
      if (PeekMessage(&msg, GetActiveWindow(), 0, 0, PM_REMOVE))
      {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    }
    return true;
  }

  void Game::Cleanup()
  {
    //mtx.lock();
    bool reorganize = !destroyingSpaces.empty();
    while (!destroyingSpaces.empty())
    {
      Space *sp = *destroyingSpaces.begin();
      auto result = namedSpaces.find(sp->name);
      if (result != namedSpaces.end())
        namedSpaces.erase(result);
      auto it = spaces.begin();
      for (; it != spaces.end(); ++it)
        if (&*it == sp)
          break;
      it->dispatcher->Destroy();
      delete it->dispatcher;
      it->ScriptableDestroy();
      it->camera.transform.ScriptableDestroy();
      it->camera.ScriptableDestroy();
      spaces.erase(it);
      destroyingSpaces.pop_front();
    }
    if (reorganize)
    {
      size_t i = 0;
      for (auto &space : spaces)
      {
        space.index = i++;
      }
    }
    //mtx.unlock();
  }

  void Game::Restart()
  {
    isRunning = false;
    isRestarting = true;
  }

  void Game::PauseAllExcept(std::string except)
  {
    for (auto &space : spaces)
      if (space.name != except)
        space.paused = true;
  }

  void Game::UnpauseAll()
  {
    for (auto &space : spaces)
      space.paused = false;
  }

  Space *Game::CreateSpace_Top(std::string name)
  {
    if (namedSpaces.find(name) != namedSpaces.end())
      throw std::exception("Tried to create a space with the name of an existing space");

    spaces.push_back(Space(name));
    namedSpaces.insert({ name, &spaces.back() });

    size_t i = 0;
    for (auto &space : spaces)
    {
      space.index = i++;
    }

    return &spaces.back();
  }

  void Game::DestroySpace(std::string name)
  {
    auto result = namedSpaces.find(name);
    if (result == namedSpaces.end())
      throw std::exception("Tried to destroy a space that doesn't exist");
    auto it = spaces.begin();
    for (; it != spaces.end(); ++it)
    {
      if (&*it == result->second) // what
        break;
    }
    it->destroying = true;
    destroyingSpaces.push_back(&*it);
  }

  Space *Game::GetSpaceByName(std::string name)
  {
    auto result = namedSpaces.find(name);
    if (result == namedSpaces.end())
      return nullptr;
    auto it = spaces.begin();
    for (; it != spaces.end(); ++it)
    {
      if (&*it == result->second) // what
        break;
    }
    return &*it;
  }

  void Game::UpdateLoadingScreen()
  {
    
    //if (r > 86)
    //  r--;
    //if (r == 86 && g > 213)
    //  g--;
    //if (g == 213 && b > 213)
    //  b--;

    if (renderStates[0].textObjects.size() == 0)
      return;

    UINT32 color = 255 << 24 | b << 16 | g << 8 | r;
    renderStates[0].textObjects[0].color = color;

    unsigned loadingState = renderStates[0].textObjects[0].text.size() - 7;
    switch (loadingState)
    {
    case 0:
      renderStates[0].textObjects[0].text = "Loading.";
      break;
    case 1:
      renderStates[0].textObjects[0].text = "Loading..";
      break;
    case 2:
      renderStates[0].textObjects[0].text = "Loading...";
      break;
    default:
      renderStates[0].textObjects[0].text = "Loading";
    }

    graphicsManager->Present(renderStates[0], true);
  }

  void Game::LoadAssets(std::string asset, std::function<void(std::string, std::string, void *)> callback, void *arbitrary, std::string preExtension)
  {
    // LOAD TEXTURES
    std::vector<std::wstring> files;
    TCHAR nPath[MAX_PATH];
#if defined(_EDITOR)
    wcscpy(nPath, dataLocation_w.c_str());
#else
    wcscpy(nPath, fileLocation.c_str());
    wcscat(nPath, dataLocation_w.c_str());
#endif

    std::wstring asset_w;
    asset_w.assign(asset.begin(), asset.end());

    wcscat(nPath, asset_w.c_str());
    wcscat(nPath, L"\\*.*");

    //std::wcout << nPath << std::endl;

    WIN32_FIND_DATA fd;
    HANDLE hFind = FindFirstFile(nPath, &fd);

    if (hFind != INVALID_HANDLE_VALUE)
    {
      do
      {
        if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
          files.push_back(fd.cFileName);
      } while (FindNextFile(hFind, &fd));
      FindClose(hFind);
    }

    std::string directory(fileLocation.begin(), fileLocation.end());

    // For threading texture loading
    int count = 0;
    std::vector<std::thread> threads;
    

    Console::Info("Loading " + asset);
    for (auto const &file : files)
    {
      std::string filename(file.begin(), file.end());
#if defined(_EDITOR)
      filename = dataLocation + asset + "\\" + filename;
#else
      filename = directory + dataLocation + asset + "\\" + filename;
#endif
#if defined(_EDITOR)
      size_t lastSlash = dataLocation.length() + asset.length() + 1; // the + 1 is for "\\"
#else
      size_t lastSlash = fileLocation.length() + dataLocation.length() + asset.length() + 1; // the + 1 is for "\\"
#endif
      std::string name(filename.substr(lastSlash, filename.find_last_of(".") - lastSlash));
      if (preExtension != "")
      {
        auto preExtLocation = name.find("." + preExtension);
        name.erase(preExtLocation, preExtension.size() + 1);
      }
      if (name == "Thumbs")
        continue;
      std::replace(name.begin(), name.end(), '\\', '/');
      //Console::Info("", name);

      if (asset == "Textures")
      {
        
        // Create up to five threads
        if (count < 5)
        {
          threads.push_back(std::thread(callback, name, filename, arbitrary));
          count++;
        }
        // Join threads after 5 have been made
        else
        {
          for (unsigned i = 0; i < threads.size(); i++)
          {
            threads[i].join();
          }
          count = 0;
          threads.clear();

          // make sure to add current object were on
          threads.push_back(std::thread(callback, name, filename, arbitrary));

        }
      }
      // All other assets don't load with threading
      else
        callback(name, filename, arbitrary);


      UpdateLoadingScreen();
    }

    // If we didn't make 5 threads, but there's still some left
    if (!threads.empty())
      // join them all
      for (unsigned i = 0; i < threads.size(); i++)
        threads[i].join();
  }

  void Game::LoadJavaScriptAssets()
  {
    v8::Isolate::Scope isoScope(isolate);
    v8::Locker locker(isolate);
    v8::HandleScope handleScope(isolate);

    // REGISTER GLOBAL FUNCTIONS HERE
    v8::Handle<v8::ObjectTemplate> globalTemplate = v8::ObjectTemplate::New();

    v8_RegisterPrintMessage(globalTemplate);
    v8_RegisterCheckInput(globalTemplate);
    v8_RegisterVec2(globalTemplate);
    v8_RegisterVec4(globalTemplate);

    v8::Local<v8::Context> context_temp = v8::Context::New(isolate, NULL, globalTemplate);
    context.Reset(isolate, context_temp);
    v8::Context::Scope contextScope(context_temp);

    v8_CreateConsole();
    v8_ExposeGame();

    LoadAssets("Libraries", [&](std::string name, std::string filename, void *arbitrary)
    {
      std::ifstream inputStream;
      inputStream.open(filename.c_str(), std::ifstream::in);

      if (!inputStream.is_open())
        return;

      std::stringstream buffer;
      buffer << inputStream.rdbuf();

      std::string str = buffer.str();

      v8::Handle<v8::String> source = v8::String::NewFromUtf8(isolate, buffer.str().c_str());

      v8::TryCatch trycatch;

      // this function is to be deprecated. supposed to use v8::ScriptOrigin, but that needs dynamic memory
      v8::Local<v8::Script> script = v8::Script::Compile(source, v8::String::NewFromUtf8(isolate, name.c_str()));

      // if compilation error
      if (trycatch.HasCaught())
      {
        auto e = trycatch.Message();
        if (!e.IsEmpty())
        {
          using namespace v8;
          Local<Value> exception = trycatch.Exception();
          String::Utf8Value exception_str(exception);
          int errorLineNumber = e->GetLineNumber();
          String::Utf8Value errorLine(e->GetSourceLine());
          String::Utf8Value errorFile(e->GetScriptResourceName());
          char buffer[25];
          Console::Error("JavaScript Compilation Error : ", *exception_str);
          std::sprintf(buffer, ", Line Number %d : ", errorLineNumber);
          Console::Write(*errorFile, Omni::Console::Color::Red);
          Console::Write(buffer, Omni::Console::Color::Red);
          Console::WriteLine(*errorLine, Omni::Console::Color::Red);
        }
      }

      // if runtime error
      else
      {
        auto v = script->Run();
        if (trycatch.HasCaught())
        {
          auto e = trycatch.Message();
          if (!e.IsEmpty())
          {
            using namespace v8;
            Local<Value> exception = trycatch.Exception();
            String::Utf8Value exception_str(exception);
            int errorLineNumber = e->GetLineNumber();
            String::Utf8Value errorLine(e->GetSourceLine());
            String::Utf8Value errorFile(e->GetScriptResourceName());
            char buffer[25];
            Console::Error("JavaScript Run Error : ", *exception_str);
            std::sprintf(buffer, ", Line Number %d : ", errorLineNumber);
            Console::Write(*errorFile, Omni::Console::Color::Red);
            Console::Write(buffer, Omni::Console::Color::Red);
            Console::WriteLine(*errorLine, Omni::Console::Color::Red);
          }
        }
      }
    }, nullptr, "lib");
    LoadAssets("Components", [&](std::string name, std::string filename, void *arbitrary)
    {
      using namespace v8;
      std::ifstream inputStream;
      inputStream.open(filename.c_str(), std::ifstream::in);

      if (!inputStream.is_open())
        return;

      std::stringstream buffer;
      buffer << inputStream.rdbuf();

      std::string str = buffer.str();

      Local<String> source = String::NewFromUtf8(isolate, buffer.str().c_str());

      TryCatch trycatch;

      // this function is to be deprecated. supposed to use v8::ScriptOrigin, but that needs dynamic memory
      Local<Script> script = Script::Compile(source, String::NewFromUtf8(isolate, name.c_str()));

      // if compilation error
      if (trycatch.HasCaught())
      {
        auto e = trycatch.Message();
        if (!e.IsEmpty())
        {
          Local<Value> exception = trycatch.Exception();
          String::Utf8Value exception_str(exception);
          int errorLineNumber = e->GetLineNumber();
          String::Utf8Value errorLine(e->GetSourceLine());
          String::Utf8Value errorFile(e->GetScriptResourceName());
          char buffer[25];
          Console::Error("JavaScript Compilation Error : ", *exception_str);
          std::sprintf(buffer, ", Line Number %d : ", errorLineNumber);
          Console::Write(*errorFile, Omni::Console::Color::Red);
          Console::Write(buffer, Omni::Console::Color::Red);
          Console::WriteLine(*errorLine, Omni::Console::Color::Red);
        }
      }

      // if runtime error
      else
      {
        auto v = script->Run();
        if (trycatch.HasCaught())
        {
          auto e = trycatch.Message();
          if (!e.IsEmpty())
          {
            Local<Value> exception = trycatch.Exception();
            String::Utf8Value exception_str(exception);
            int errorLineNumber = e->GetLineNumber();
            String::Utf8Value errorLine(e->GetSourceLine());
            String::Utf8Value errorFile(e->GetScriptResourceName());
            char buffer[25];
            Console::Error("JavaScript Run Error : ", *exception_str);
            std::sprintf(buffer, ", Line Number %d : ", errorLineNumber);
            Console::Write(*errorFile, Omni::Console::Color::Red);
            Console::Write(buffer, Omni::Console::Color::Red);
            Console::WriteLine(*errorLine, Omni::Console::Color::Red);
          }
        }
      }

      // get the global object from the context
      auto global = isolate->GetEnteredContext()->Global();

      Local<Value> moduleRaw = global->Get(String::NewFromUtf8(isolate, name.c_str()));
      Local<Object> moduleConstructor = Local<Object>::Cast(moduleRaw);

      PropertyDefinitionMap propertyMap;

      // if there are properties defined, get them
      if (moduleConstructor->Has(String::NewFromUtf8(isolate, "properties")))
      {
        Local<Object> properties = Local<Object>::Cast(moduleConstructor->Get((String::NewFromUtf8(isolate, "properties"))));
        Local<Array> propertyNames = properties->GetOwnPropertyNames();
        int numProperties = propertyNames->Length();
        for (int i = 0; i < numProperties; ++i)
        {
          LocalObject propDef = LocalObject::Cast(properties->GetRealNamedProperty(Local<String>::Cast(propertyNames->Get(i))));
          Property::Type type = static_cast<Property::Type>(propDef->Get(String::NewFromUtf8(isolate, "type"))->IntegerValue()); 
          PropertyDefinition def;
          std::string propertyName = *String::Utf8Value(propertyNames->Get(i));
          switch (type)
          {
          case Property::Type::Bool:
            def = PropertyDefinition::Bool(propDef->Get(String::NewFromUtf8(isolate, "defaultValue"))->BooleanValue());
            break;
          case Property::Type::Unsigned:
            def = PropertyDefinition::Unsigned(static_cast<Property::Unsigned>(propDef->Get(String::NewFromUtf8(isolate, "defaultValue"))->IntegerValue()));
            break;
          case Property::Type::Int:
            def = PropertyDefinition::Int(static_cast<Property::Int>(propDef->Get(String::NewFromUtf8(isolate, "defaultValue"))->IntegerValue()));
            break;
          case Property::Type::Float:
            def = PropertyDefinition::Float(static_cast<float>(propDef->Get(String::NewFromUtf8(isolate, "defaultValue"))->NumberValue()));
            break;
          case Property::Type::String:
            def = PropertyDefinition::String(Take<std::string>::FromV8(propDef->Get(String::NewFromUtf8(isolate, "defaultValue"))));
            break;
          case Property::Type::Color:
            // nope
            break;
          case Property::Type::Vec2:
            //def = PropertyDefinition::Vec2(Take<Vec2>::FromV8(propDef->Get(String::NewFromUtf8(isolate, "defaultValue"))));
            // WHY NOT LOL
            def = PropertyDefinition::Vec2(Vec2());
            break;
          case Property::Type::Vec2List:
            // you can't do this from JavaScript screw you that's stupid
            break;
          }
          propertyMap.insert({ propertyName, def });
        }
      }

      RegisterComponent(name, ComponentDefinition(name, propertyMap));
    }, nullptr);
    LoadAssets("Archetypes", [&](std::string name, std::string filename, void *arbitrary)
    {
      Json::Value root;
      Json::Reader reader;

      std::ifstream stream;
      stream.open(filename.c_str(), std::ifstream::in);


      bool success = reader.parse(stream, root);
      stream.close();
      if (!success)
        return;

      Archetype arch(name);

      if (!!root["name"])
        arch.name = root["name"].asString();
      
      if (!!root["transform"])
      {
        if (!!root["transform"]["translation"])
        {
          arch.transform.translation.Set(root["transform"]["translation"]["x"].asFloat(), root["transform"]["translation"]["y"].asFloat());
          arch.hasTranslation = true;
        }
        if (!!root["transform"]["scale"])
        {
          arch.transform.scale.Set(root["transform"]["scale"]["x"].asFloat(), root["transform"]["scale"]["y"].asFloat());
          arch.hasScale = true;
        }
        if (!!root["transform"]["rotation"])
        {
          arch.transform.rotation = root["transform"]["rotation"].asFloat();
          arch.hasRotation = true;
        }
      }

      if (!!root["components"])
      {
        for (auto const &compData : root["components"])
        {
          std::string compName = compData["name"].asString();
          
          arch.AddComponent(compName);
          if (componentDefinitions.count(compName) == 0)
          {
            Console::Error("Critical Archetype load error", "Archetype file \"" + filename + "\" contains nonexistent component \"" + compName + "\".");
            continue;
          }
          if (!!compData["properties"])
          {
            PropertyDefinitionMap &propDefs = componentDefinitions[compData["name"].asString()].properties;
            for (auto const &propDef : propDefs)
            {
              if (!!compData["properties"][propDef.first.c_str()])
              {
                switch (propDef.second.type)
                {
                case Property::Type::Bool:
                  arch.AddProperty(compName, propDef.first.c_str(), PropertyContainer::CreateBool(propDef.first.c_str(), compData["properties"][propDef.first.c_str()].asBool()));
                  break;
                case Property::Type::Color:
                  arch.AddProperty(compName, propDef.first.c_str(), PropertyContainer::CreateColor(propDef.first.c_str(), Property::Color(compData["properties"][propDef.first.c_str()]["r"].asFloat(), compData["properties"][propDef.first.c_str()]["g"].asFloat(), compData["properties"][propDef.first.c_str()]["b"].asFloat(), compData["properties"][propDef.first.c_str()]["a"].asFloat())));
                  break;
                case Property::Type::Float:
                  arch.AddProperty(compName, propDef.first.c_str(), PropertyContainer::CreateFloat(propDef.first.c_str(), compData["properties"][propDef.first.c_str()].asFloat()));
                  break;
                case Property::Type::Int:
                  arch.AddProperty(compName, propDef.first.c_str(), PropertyContainer::CreateInt(propDef.first.c_str(), compData["properties"][propDef.first.c_str()].asInt()));
                  break;
                case Property::Type::String:
                  arch.AddProperty(compName, propDef.first.c_str(), PropertyContainer::CreateString(propDef.first.c_str(), compData["properties"][propDef.first.c_str()].asString()));
                  break;
                case Property::Type::Undefined:
                  // TODO: ERROR?!?!?!
                  break;
                case Property::Type::Unsigned:
                  arch.AddProperty(compName, propDef.first.c_str(), PropertyContainer::CreateUnsigned(propDef.first.c_str(), compData["properties"][propDef.first.c_str()].asUInt()));
                  break;
                case Property::Type::Vec2:
                  arch.AddProperty(compName, propDef.first.c_str(), PropertyContainer::CreateVec2(propDef.first.c_str(), Property::Vec2(compData["properties"][propDef.first.c_str()]["x"].asFloat(), compData["properties"][propDef.first.c_str()]["y"].asFloat())));
                  break;
                case Property::Type::Vec2List:
                  Property::Vec2List points;
                  points.reserve(8);

                  for (auto const &point : compData["properties"][propDef.first.c_str()])
                  {
                    points.push_back(Vec2(point["x"].asFloat(), point["y"].asFloat()));
                  }

                  arch.AddProperty(compName, propDef.first.c_str(), PropertyContainer::CreateVec2List(propDef.first.c_str(), points));
                  break;
                }
              }
              else
              {
                // SET IT TO THE DEFAULT FROM THE COMPONENT
              }
              // TODO: ERROR CHECK? WARNING?
            }
          }
        }
      }
      assets.archetypes.insert({ name, arch });
    }, nullptr, "archetype");
    Console::WriteLine("Done loading archetypes.");
  }

  void Game::LoadDataAssets()
  {

    LoadAssets("Textures", [&](std::string name, std::string filename, void *arbitrary)
    {
      std::string fullName(std::string("_") + name + std::string("_full"));
  
      Graphics::Handle handle;

      graphicsManager->m_renderer->CreateTexture(handle, filename, &mtx);

      mtx.lock();
      assets.textures.insert({ name, handle });
      assets.images.insert({ fullName, Image(fullName, assets.textures[name]) });
      mtx.unlock();

    }, nullptr);
    LoadAssets("Materials", [&](std::string name, std::string filename, void *arbitrary)
    {
      Json::Value root;
      Json::Reader reader;

      std::ifstream stream;
      stream.open(filename.c_str(), std::ifstream::in);

      if (!stream.is_open())
        return;
      bool success = reader.parse(stream, root);
      stream.close();
      if (!success)
        return;

      assets.materials.insert({ name, Material(root["density"].asFloat(), root["restitution"].asFloat(), root["friction"].asFloat()) });
      
    }, nullptr, "material");
    LoadAssets("Images", [&](std::string name, std::string filename, void *arbitrary)
    {
      Json::Value root;
      Json::Reader reader;

      std::ifstream stream;
      stream.open(filename.c_str(), std::ifstream::in);

      if (!stream.is_open())
        return;
      bool success = reader.parse(stream, root);
      stream.close();
      if (!success)
        return;

      // ACTUAL **ACTUAL** JSON PARSING STUFF HERE

      for (auto const &imageData : root["images"])
      {
        if (!imageData["name"])
        {
          // ERROR CHECK
          continue;
        }

        std::string imageName = imageData["name"].asString();
        //std::cout << "LOADING IMAGE DEFINITION: " << imageName << std::endl;
        Console::Info("", "  " + imageName);

        // ERROR CHECK: MAKE SURE IMAGE DOES NOT EXIST
        assets.images.insert({ imageName, Image() });

        Image *image = &assets.images[imageName];

        // ERROR CHECK: MAKE SURE TEXTURE EXISTS
        image->name = imageData["name"].asString();
        image->texture = assets.textures[imageData["texture"].asString()];
        image->dimensions.x = imageData["dimensions"]["x"].asFloat();
        image->dimensions.y = imageData["dimensions"]["y"].asFloat();
        image->pixelsPerUnit = imageData["pixelsPerUnit"].asUInt();
        image->uv[0].x = imageData["uv"][0]["x"].asFloat();
        image->uv[0].y = imageData["uv"][0]["y"].asFloat();
        image->uv[1].x = imageData["uv"][1]["x"].asFloat();
        image->uv[1].y = imageData["uv"][1]["y"].asFloat();
        if (!!imageData["hasGlow"] && imageData["hasGlow"].asBool())
        {
          std::string glowName = imageName + "_glow";
          assets.images.insert({ glowName, Image() });
          image = &assets.images[glowName];

          image->name = imageData["name"].asString();
          image->texture = assets.textures[imageData["texture"].asString() + "_glow"];
          image->dimensions.x = imageData["dimensions"]["x"].asFloat();
          image->dimensions.y = imageData["dimensions"]["y"].asFloat();
          image->pixelsPerUnit = imageData["pixelsPerUnit"].asUInt();
          image->uv[0].x = imageData["uv"][0]["x"].asFloat();
          image->uv[0].y = imageData["uv"][0]["y"].asFloat();
          image->uv[1].x = imageData["uv"][1]["x"].asFloat();
          image->uv[1].y = imageData["uv"][1]["y"].asFloat();

          Console::Info("", "    " + imageName + "_glow");
        }
      }
    }, nullptr);
    LoadAssets("Animations", [&](std::string name, std::string filename, void *arbitrary)
    {
      Json::Value root;
      Json::Reader reader;

      std::ifstream stream;
      stream.open(filename.c_str(), std::ifstream::in);

      if (!stream.is_open())
        return;
      bool success = reader.parse(stream, root);
      stream.close();
      if (!success)
        return;

      std::string animName = root["name"].asString();
      assets.animations.insert({ animName, Animation() });
      Animation *anim = &assets.animations[animName];

      anim->name = animName;
      if (!!root["looping"])
        anim->looping = root["looping"].asBool();
      if (!!root["ignoreMirrorX"])
        anim->ignoreMirrorX = root["ignoreMirrorX"].asBool();
      if (!!root["ignoreMirrorY"])
        anim->ignoreMirrorY = root["ignoreMirrorY"].asBool();

      for (auto const &frameData : root["frames"])
      {
        anim->frames.push_back(Frame());
        Frame *frame = &anim->frames.back();

        frame->image = frameData["image"].asString();
        frame->duration = frameData["duration"].asFloat();

        if (!!frameData["points"])
          for (auto const &pointData : frameData["points"])
            frame->points.push_back(Vec2(pointData["x"].asFloat(), pointData["y"].asFloat()));
        if (!!frameData["flags"])
          for (auto const &flagData : frameData["flags"])
            frame->flags.push_back(flagData.asString());
        if (!!frameData["damage"])
          frame->damage = frameData["damage"].asUInt();
        if (!!frameData["inheritHitboxes"])
          frame->inheritHitboxes = true;
        if (!!frameData["hitboxes"])
          for (auto const &hitboxData : frameData["hitboxes"])
          {
            frame->hitboxes.push_back(HitBoxData());
            HitBoxData *hbd = &frame->hitboxes.back();
            hbd->center.x = hitboxData["center"]["x"].asFloat();
            hbd->center.y = hitboxData["center"]["y"].asFloat();
            if (hitboxData["type"].asString() == "damage")
              hbd->type = HitBox::Type::Damage;
            if (hitboxData["type"].asString() == "body")
              hbd->type = HitBox::Type::Body;
            if (!!hitboxData["radius"])
              hbd->radius = hitboxData["radius"].asFloat();
            if (!!hitboxData["scale"])
            {
              hbd->scale.x = hitboxData["scale"]["x"].asFloat();
              hbd->scale.y = hitboxData["scale"]["y"].asFloat();
            }
         }
        if (!!frameData["sound"])
        {
          frame->soundCue = frameData["sound"].asString();
        }
      }
    }, nullptr, "anim");
    soundSystem->LoadBank("Master Bank.bank");
    soundSystem->LoadBank("Master Bank.strings.bank");
  }

  void Game::LoadLevel(std::string levelToLoad)
  {
    bool found = false;
    LoadAssets("Levels", [&](std::string name, std::string filename, void *arbitrary)
    {
      if (found || name != levelToLoad)
        return;

      found = true;
      level = levelToLoad;

      // ACTUAL JSON PARSING STUFF HERE

      Json::Value root;
      Json::Reader reader;

      std::ifstream stream;
      stream.open(filename.c_str(), std::ifstream::in);


      bool success = reader.parse(stream, root);
      stream.close();
      if (!success)
      {
        Console::Error("Critical error loading Level \"" + name + "\"", "JSON file was invalid.");
        return;
      }

      // ACTUAL **ACTUAL** JSON PARSING STUFF HERE

      for (auto const &spaceData : root["spaces"])
      {
        if (!spaceData["name"])
        {
          // ERROR CHECK
          continue;
        }

        Space *space = CreateSpace_Top(spaceData["name"].asString());

        if (!!spaceData["parallax"])
        {
          for (auto const &layerData : spaceData["parallax"]["layers"])
          {
            space->parallaxLayers.push_back(std::tuple<std::string, float, bool>(layerData[0].asString(), layerData[1].asFloat(), layerData[2].asBool()));
          }
          space->parallaxOffset = spaceData["parallax"]["offset"].asFloat();
        }

        if (!!spaceData["camera"])
        {
          space->camera.transform.translation.x = spaceData["camera"]["translation"]["x"].asFloat();
          space->camera.transform.translation.y = spaceData["camera"]["translation"]["y"].asFloat();
          space->camera.transform.rotation = spaceData["camera"]["rotation"].asFloat();
          space->camera.size = spaceData["camera"]["size"].asFloat();

          // also write the camera data to the initial camera
          space->initialCamera = space->camera;
        }

#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
        space->SpawnTweakBar();
#endif

        for (auto const &entityData : spaceData["entities"])
        {
          Handlet<Entity> entity = nullptr;

          if (!entityData["name"])
            if (!!entityData["archetype"])
              entity = space->CreateEntity(entityData["archetype"].asString());
            else
              entity = space->CreateEntity("");
          else
            entity = space->CreateEntity(entityData["name"].asString());

          if (!!entityData["archetype"])
          {
            entity->ApplyArchetype(entityData["archetype"].asString());
          }

          // Transform
          if (!!entityData["transform"])
          {
            if (!!entityData["transform"]["translation"])
            {
              entity->transform.translation.x = entityData["transform"]["translation"]["x"].asFloat();
              entity->transform.translation.y = entityData["transform"]["translation"]["y"].asFloat();
            }
            if (!!entityData["transform"]["scale"])
            {
              entity->transform.scale.x = entityData["transform"]["scale"]["x"].asFloat();
              entity->transform.scale.y = entityData["transform"]["scale"]["y"].asFloat();
            }
            if (!!entityData["transform"]["rotation"])
            {
              entity->transform.rotation = entityData["transform"]["rotation"].asFloat();
            }
          }

          // read in link ID
          if (!!entityData["linkID"])
          {
            size_t linkID = entityData["linkID"].asUInt();
            entity->SetLinkID(linkID);
            linkIDtoUniqueID.insert({ linkID, entity->id });
          }

          if (!!entityData["components"])
          {
            for (auto const &compData : entityData["components"])
            {
              std::string compName = compData["name"].asString();

              if (componentDefinitions.count(compName) == 0)
              {
                Console::Error("Critical Entity loading error", "Entity #" + std::to_string(entity->id) + " contains data for the \"" + compName + "\" component, which does not exist.");
                continue;
              }

              Handlet<Component> comp = entity->GetComponent(compName);
              if (comp == nullptr)
                comp = entity->AddComponent(compName);

              if (!!compData["properties"])
              {
                PropertyDefinitionMap &propDefs = componentDefinitions[compName].properties;
                for (auto const &propDef : propDefs)
                {
                  // if the property is defined in the level file, apply it
                  if (!!compData["properties"][propDef.first.c_str()])
                  {
                    switch (propDef.second.type)
                    {
                    case Property::Type::Bool:
                      comp->SetProperty_Bool(propDef.first.c_str(), compData["properties"][propDef.first.c_str()].asBool());
                      break;
                    case Property::Type::Color:
                      comp->SetProperty_Color(propDef.first.c_str(), D3DXCOLOR(compData["properties"][propDef.first.c_str()]["r"].asFloat(), compData["properties"][propDef.first.c_str()]["g"].asFloat(), compData["properties"][propDef.first.c_str()]["b"].asFloat(), compData["properties"][propDef.first.c_str()]["a"].asFloat()));
                      break;
                    case Property::Type::Float:
                      comp->SetProperty_Float(propDef.first.c_str(), compData["properties"][propDef.first.c_str()].asFloat());
                      break;
                    case Property::Type::Int:
                      comp->SetProperty_Int(propDef.first.c_str(), compData["properties"][propDef.first.c_str()].asInt());
                      break;
                    case Property::Type::String:
                      comp->SetProperty_String(propDef.first.c_str(), compData["properties"][propDef.first.c_str()].asString());
                      break;
                    case Property::Type::Undefined:
                      // TODO: ERROR?!?!?!
                      break;
                    case Property::Type::Unsigned:
                      comp->SetProperty_Unsigned(propDef.first.c_str(), compData["properties"][propDef.first.c_str()].asUInt());
                      break;
                    case Property::Type::Vec2:
                      comp->SetProperty_Vec2(propDef.first.c_str(), Vec2(compData["properties"][propDef.first.c_str()]["x"].asFloat(), compData["properties"][propDef.first.c_str()]["y"].asFloat()));
                      break;
                    case Property::Type::Vec2List:
                      Property::Vec2List points;
                      points.reserve(8);

                      for (auto const &point : compData["properties"][propDef.first.c_str()])
                      {
                        points.push_back(Vec2(point["x"].asFloat(), point["y"].asFloat()));
                      }

                      comp->SetProperty_Vec2List(propDef.first.c_str(), points);
                      break;
                    }
                  }
                  // TODO: ERROR CHECK? WARNING?
                }
              }
            }
          }

          entity->Initialize();
          //entity->SpawnTweakBar();
        }
        }
    }, nullptr, "level");

    loadNextLevel = false;
    nextLevel = levelToLoad;
  }

  Handlet<Entity> Game::GetEntityByLinkID(size_t linkID)
  {
    if (linkIDtoUniqueID.count(linkID))
      return GetEntityByID(linkIDtoUniqueID[linkID]);
    return nullptr;
  }

  size_t Game::GetUniqueIDFromLinkID(size_t linkID)
  {

    if (linkIDtoUniqueID.count(linkID))
      return linkIDtoUniqueID[linkID];
    return 0;
  }

  Handlet<Entity> Game::GetEntityByID(size_t id)
  {
    if (entityHandles.count(id))
      return entityHandles[id];
    return Handlet<Entity>(nullptr);
    //EntityHandle *eh = GetEntityHandleByID(id);
    //return eh ? eh->space->GetEntityByHandle(eh->entity) : nullptr;
  }

  Handlet<Entity> Game::GetEntityByName(std::string name)
  {
    Handlet<Entity> result;
    for (auto &space : spaces)
    {
      result = space.GetEntityByName(name);
      if (result.isValid())
        return result;
    }
    return result;
  }

  ModulePropertyHandle *Game::GetPropertyHandle(Handlet<Component> component, std::string property)
  {
    auto it = modulePropertyHandles.begin();
    for (; it != modulePropertyHandles.end(); ++it)
      if (it->component == component && it->property == property)
        return &*it;
    modulePropertyHandles.push_back(ModulePropertyHandle(component, property));
    return &modulePropertyHandles.back();
  }

  void Game::DiscardModulePropertyHandles(Handlet<Component> module)
  {
    std::remove_if(modulePropertyHandles.begin(), modulePropertyHandles.end(), [&](ModulePropertyHandle &handle)
    {
      return handle.component == module;
    });
  }

  SoundUtilities::ParameterHandle *Game::GetParameterHandle(size_t instance, size_t parameter)
  {
    auto it = parameterHandles.begin();
    for (; it != parameterHandles.end(); ++it)
      if (it->instance == instance && it->parameter == parameter)
        return &*it;
    parameterHandles.push_back(SoundUtilities::ParameterHandle(instance, parameter));
    return &parameterHandles.back();
  }

  Game::~Game()
  {

  }

  void Game::Serialize() const
  {
#if defined(_EDITOR) || defined(_RELEASE) || defined(_DEBUG)
    // Dont serialize in Publish
#else
    return;
#endif

    Json::Value root;
    Json::Value spacesValue(Json::arrayValue);
    root["spaces"] = spacesValue;
    Serialize(root["spaces"]);
    std::ofstream outFile;
#if defined(_EDITOR)
    std::string location(dataLocation.begin(), dataLocation.end()); 
#else
    std::string location(fileLocation.begin(), fileLocation.end());
    location.append(dataLocation);
#endif
    outFile.open(location + "Levels\\" + level + ".level.json");
    if (outFile.is_open())
    {
      outFile << root;
      outFile.close();
    }
    else
    {
      Console::Error("Error opening file level file for writing.", "\"" + location + dataLocation + "Levels\\" + level + ".level.json\" could not be opened for writing.");
    }
    //std::cout << root << std::endl;
    
  }

  void Game::Serialize(Json::Value &root) const
  {
    Console::Info("Serializing level...");
    for (auto const &space : spaces)
    {
      root.append(Json::objectValue);
      space.Serialize(root[space.index]);
    }
    Console::Info("", "...done.");
  }

  bool Game::EditorIsActive() const
  {
    return editor->m_active || isEditing;
  }

  std::string Game::GetMyDocumentsDirectory()
  {
    wchar_t my_documents[MAX_PATH];
    HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, my_documents);

    char ch[MAX_PATH] = { 0 };
    wcstombs(ch, my_documents, MAX_PATH);
    std::string str(ch);
    str.append("\\DigiPen\\OmniEditor");
    return str;
  }

  Game Game::s_instance;
}
