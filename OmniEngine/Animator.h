/******************************************************************************
Filename: Animator.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "Component.h"
#include "Animation.h"
#include <queue>

namespace Omni
{

  class Animator : public SystemComponent<Animator>
  {
  public:
    ~Animator();

    virtual void Initialize();
    virtual void Update(float dt);
    virtual void Destroy();

    static void Register();

    static void v8_Register();

    void Set(std::string animation);
    void Queue(std::string animation);
    void Clear();
    inline bool AnimationDone() { return done; }
    inline size_t AnimationCount() { return animations.size(); }

  private:
    std::queue<std::string> animations;

  public:
    Frame *CurrentFrame();
    unsigned currentFrame = 0;
    std::string defaultAnimation;

  private:
    float frameRemaining = 0.0f;
    bool done = false;
  };

}

