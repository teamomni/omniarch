/******************************************************************************
Filename: ComponentDefinition.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Component.h"

namespace Omni
{
  Handlet<Component> ComponentDefinition::Create(Entity *entity)
  {
    Handlet<Component>comp = nullptr;
    switch (type)
    {
    case Type::System:
      comp = systemComponentCreateFunction(entity);
      break;
    case Type::User:
      UserComponent *userComp = UserComponent::Create(entity, name);
      comp.object = userComp->selfPtr;
      break;
    }
    return comp;
  }

  void ComponentDefinition::AddProperty(std::string name_, Property::Type type_)
  {
    properties.insert({ name_, PropertyDefinition(type_) });
  }
}