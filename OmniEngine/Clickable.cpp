/******************************************************************************
Filename: Clickable.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Clickable.h"
#include "Input.h"
#include "Sprite.h"
#include "Editor.h"

namespace Omni
{
  size_t Clickable::currentlySelectedID[unsigned(Mouse::Button::_Last)] = { 0 };
  bool Clickable::currentlyDown[unsigned(Mouse::Button::_Last)] = { 0 };
  std::vector<std::pair < float, size_t>> Clickable::clickedThisFrame[unsigned(Mouse::Button::_Last)];
  Vec2 Clickable::relativePositionAtPressed;

  Clickable::Clickable() : ownerHandle(0), space(nullptr)
  {
    for (int i = 0; i < (int)Mouse::Button::_Last; ++i)
    {
      mouseStates[i] = ButtonState::Up;
    }
  }


  Clickable::~Clickable()
  {
  }

  void Clickable::Update()
  {
    Handlet<Entity> owner = GetOwner();

    if (!owner)
      return;

    // if it is inactive, skip
    if (!owner->active)
      return;

    // if not in main space, skip
    if (owner->space->name != "main")
        return;

    for (int i = 0; i < int(Mouse::Button::_Last); ++i)
    {
      // if last state was released or released out, then set to up
      if (mouseStates[i] == ButtonState::Released || mouseStates[i] == ButtonState::ReleasedOut)
        mouseStates[i] = ButtonState::Up;

      // get current mouse state
      ButtonState state = Input::mouse.mouseStates[i];

      // if the current state is down, only look for the entity that is clicked on
      if (state == ButtonState::Down)
      {
        if (currentlyDown[i] && owner->id == Clickable::currentlySelectedID[i])
          mouseStates[i] = state;
      }

      // if released, check for currently selected thing
      else if (state == ButtonState::Released)
      {
        if (currentlyDown[i] && owner->id == Clickable::currentlySelectedID[i])
        {
          // get relative mouse position
          Space *mySpace = owner->space;
          Vec2 mousePosition = mySpace->GetMousePosition();

          // get transform data
          Transform &trans = owner->transform;
          Vec2 scale = trans.scale;
          Vec2 position = trans.translation;
          

          // hw, hh
          float hw = scale.x / 2;
          float hh = scale.y / 2;

          // transform the mouse to the world space of the clickable object
          mousePosition = mousePosition - position;

          // if mouse is over this object make it released
          if (mousePosition.x > -hw && mousePosition.x < hw && mousePosition.y > -hh && mousePosition.y < hh)
          {
            mouseStates[i] = ButtonState::Released;
          }

          // else make it released out
          else
          {
            mouseStates[i] = ButtonState::ReleasedOut;
          }

          currentlyDown[i] = false;
        }
      }

      // if currently no selection, look for a selection
      else
      {
        // get relative mouse position
        Space *mySpace = owner->space;
        Handlet<Component> spriteHandlet = GetOwner()->GetComponent("Sprite");
        Sprite *sprite = nullptr;

        if (spriteHandlet.isValid())
          sprite = reinterpret_cast<Sprite *>(*spriteHandlet);

        Vec2 mousePosition = mySpace->GetMousePosition();

        // get transform data
        Transform &trans = owner->transform;
        //Matrix3 inverseTransform = trans.GetMatrix3().Inverse();
        Vec2 scale = trans.scale;
        Vec2 position = trans.translation;
        float zdepth = 1; // things without sprites will always be on top
        if (sprite)
          zdepth = sprite->p_Depth;

        // transform the mouse to the world space of the clickable object
        mousePosition = mousePosition - position;

        // hw, hh
        float hw = scale.x / 2;
        float hh = scale.y / 2;

        // if mouse is over this object, push it into the clicked this frame vector
        if (mousePosition.x > -hw && mousePosition.x < hw && mousePosition.y > -hh && mousePosition.y < hh)
        {
          clickedThisFrame[i].push_back({ zdepth, owner->id });
        }
      }
    }
  }

  void Clickable::Step()
  {
    // loop through each mouse thing
    for (unsigned i = 0; i < unsigned(Mouse::Button::_Last); ++i)
    {
      // check to see if empty first
      if (!clickedThisFrame[i].empty())
      {
        auto &game = Game::Instance();

        // first sort the entities clicked by their z-depth (ascending)
        std::sort(clickedThisFrame[i].begin(), clickedThisFrame[i].end());

        // get the entity that mouse is hovering over
        Handlet<Entity> currentlyClicked = game.GetEntityByID(clickedThisFrame[i][0].second);

        // if current state is clicked, make it the current selection
        if (Input::mouse.mouseStates[i] == ButtonState::Pressed)
        {
          currentlyDown[i] = true;
          currentlySelectedID[i] = clickedThisFrame[i][0].second;

          Handlet<Entity> object = game.GetEntityByID(currentlySelectedID[i]);
          Vec2 objectPosition = object->transform.translation;
          Vec2 mousePosition = object->space->GetMousePosition();

          relativePositionAtPressed = objectPosition - mousePosition;
        }

        for (int j = 0; j < (unsigned)Mouse::Button::_Last; ++j)
        {
          currentlyClicked->clickable.mouseStates[j] = Input::mouse.mouseStates[j];
        }

        // clear the vector of clicked things this frame
        clickedThisFrame[i].clear();  
      }
    }

    // if currently in editor mode
    if (Game::Instance().editor->m_active)
    {
      // if left mouse button has a current target
      if (currentlyDown[unsigned(Mouse::Button::Left)])
      {
        // get relative mouse position
        Handlet<Entity> owner = Game::Instance().GetEntityByID(currentlySelectedID[unsigned(Mouse::Button::Left)]);

        // only do the stuff if the owner is alive
        if (owner)
        {
          Space *mySpace = owner->space;
          Vec2 mousePosition = mySpace->GetMousePosition();

          if (owner->clickable.mouseStates[unsigned(Mouse::Button::Left)] == ButtonState::Down)
          {
              owner->transform.translation = mousePosition + relativePositionAtPressed;
          }
        }
      }
    }
  }

  // call for now is Clickable.GetMouseState("onPress");
  static void v8_GetMouseState(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Clickable *clickable = Take<Clickable>::FromHolder(info);
    std::string whichState = Take<std::string>::FromV8(info[1]);

    Mouse::Button whichButton = Mouse::Button(Take<int>::FromV8(info[0]));
    ButtonState buttonStateQuery = ButtonState(Take<int>::FromV8(info[1]));
    bool state = (clickable->mouseStates[unsigned(whichButton)] == buttonStateQuery);

    info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), state));

  }

  Handlet<Entity> Clickable::GetOwner()
  {
    Handlet<Entity> ent = ownerHandle;
    return ent;
  }

  void Clickable::v8_Register()
  {
    using namespace v8;
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    Local<ObjectTemplate> objTemplate_temp = ObjectTemplate::New(game.isolate);

    objTemplate_temp->SetInternalFieldCount(1);

    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "GetMouseState"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_GetMouseState)));

    objTemplate.Reset(game.isolate, objTemplate_temp);
  }

  // DH PLEASE FIX
  void Entity::v8_GetClickable(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the entity pointer
    Entity *entity = Take<Entity>::FromHolder(info);

    // getting the clickable pointer through the c++ function
    Clickable *clickable = reinterpret_cast<Clickable *>(*entity->GetComponent("Clickable"));

    // create an instance of the js object
    Handle<Object> result = clickable->v8_GetObject();

    // create a wrapper for the actual pointer
    Handle<External> clickable_ptr = External::New(info.GetIsolate(), clickable);

    // store the pointer in the js object
    result->SetInternalField(0, clickable_ptr);

    info.GetReturnValue().Set(result);
  }

  void Clickable::Initialize()
  {

  }
}