/******************************************************************************
Filename: Event.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include <string>
#include "Component.h"
#include "HitBox.h"

namespace Omni
{
  namespace Events
  {
    class UpdateEvent : public Scriptable<UpdateEvent>
    {
    public:
      UpdateEvent() {}
      UpdateEvent(float dt_) : dt(dt_) {}
      ~UpdateEvent() { ScriptableDestroy(); }

      static void v8_Register();

      float dt;
    };

    class HitEvent : public Scriptable<HitEvent>
    {
    public:
      HitEvent() {}
      HitEvent(size_t otherID_, HitBox::Type myType_, HitBox::Type otherType_) :
        otherID(otherID_), myType(myType_), otherType(otherType_) {}
      ~HitEvent() { ScriptableDestroy(); }

      bool operator==(const HitEvent &rhs) 
      {
        return otherID == rhs.otherID && myType == rhs.myType && otherType == rhs.otherType;
      }

      static void v8_Register();

      size_t otherID;
      HitBox::Type myType;
      HitBox::Type otherType;
    };

    class CollisionEvent : public Scriptable<CollisionEvent>
    {
    public:
      CollisionEvent() {}
      CollisionEvent(Vec2 collisionNormal_, size_t otherID_) :
        collisionNormal(collisionNormal_), otherID(otherID_) {}
      ~CollisionEvent() { ScriptableDestroy(); }

      bool operator==(const CollisionEvent &rhs)
      {
        return otherID == rhs.otherID && collisionNormal == rhs.collisionNormal;
      }

      static void v8_Register();

      Vec2 collisionNormal;
      size_t otherID;
    };

  }
}
