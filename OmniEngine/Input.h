/******************************************************************************
Filename: Input.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "stdafx.h"
#include <XInput.h>
#include "Vec2.h"
#include <unordered_map>

#define THUMBSTICK_THRESHOLD 0.95f
#define THUMBSTICK_MENU_THRESHOLD  0.75f

namespace Omni
{

  enum class ButtonState
  {
    Up,
    Pressed,
    Down,
    Released,
    ReleasedOut
  };

  class Gamepad
  {
  public:

    enum class ConnectionStatus
    {
      Disconnected,
      JustConnected,
      Connected,
      JustDisconnected
    };

    Gamepad();

    void Update(DWORD id);

    ButtonState A() const;
    ButtonState B() const;
    ButtonState X() const;
    ButtonState Y() const;
    ButtonState LB() const;
    ButtonState RB() const;
    ButtonState Back() const;
    ButtonState Start() const;
    ButtonState LS() const;
    ButtonState RS() const;
    ButtonState DpadUp() const;
    ButtonState DpadDown() const;
    ButtonState DpadLeft() const;
    ButtonState DpadRight() const;
    ButtonState LeftStick_Up() const;
    ButtonState LeftStick_Down() const;
    ButtonState LeftStick_Left() const;
    ButtonState LeftStick_Right() const;
    ButtonState RightStick_Up() const;
    ButtonState RightStick_Down() const;
    ButtonState RightStick_Left() const;
    ButtonState RightStick_Right() const;

    Vec2 LeftStick() const;
    Vec2 LeftStick_Exact() const;

    Vec2 RightStick() const;
    Vec2 RightStick_Exact() const;

    float LeftTrigger() const;
    float RightTrigger() const;
    
    void Vibrate(float left, float right);
    ConnectionStatus Status() { return status; }

    ButtonState MakeButtonState(unsigned flag) const;
    
  private:
    XINPUT_STATE currentState;
    XINPUT_STATE previousState;
    ConnectionStatus status;
    XINPUT_VIBRATION vibration;
    DWORD id;
  };

  class Keyboard
  {
  public:
    enum class Key
    {
      D0 = 0x30,
      D1,
      D2,
      D3,
      D4,
      D5,
      D6,
      D7,
      D8,
      D9,
      A = 0x41,
      B,
      C,
      D,
      E,
      F,
      G,
      H,
      I,
      J,
      K,
      L,
      M,
      N,
      O,
      P,
      Q,
      R,
      S,
      T,
      U,
      V,
      W,
      X,
      Y,
      Z,
      Space = 0x20,
      Backspace = 0x08,
      Tab,
      Shift = 0x10,
      Control,
      Alt,
      CapsLock = 0x14,
      NumLock = 0x90,
      Enter = 0x0D,
      LeftShift = 0xA0,
      RightShift,
      LeftControl,
      RightControl,
      Escape = 0x1B,
      Delete = 0x2E,
      Left = 0x25,
      Up,
      Right,
      Down,
      Num0 = 0x60,
      Num1,
      Num2,
      Num3,
      Num4,
      Num5,
      Num6,
      Num7,
      Num8,
      Num9,
      Multiply,
      Add,
      Separator,
      Subtract,
      Decimal,
      Divide,
      F1,
      F2,
      F3,
      F4,
      F5,
      F6,
      F7,
      F8,
      F9,
      F10,
      F11,
      F12,
      Colon = 0xBA,
      Equals,
      Comma,
      Minus,
      Period,
      QuestionMark,
      Tilde,
      LeftBracket = 0xDB,
      Backslash,
      RightBracket,
      Quote,
      _Last
    };
    ButtonState KeyState(Keyboard::Key key) const;
    void Update();
    bool currentState[(unsigned)Key::_Last];
    bool previousState[(unsigned)Key::_Last];

    Keyboard()
    {
      std::fill_n(currentState, (unsigned)Key::_Last, false);
      std::fill_n(previousState, (unsigned)Key::_Last, false);
    }
  };

  class Mouse
  {
  public:
    enum class Button
    {
      Left = 0,
      Right,
      Middle,
      ScrollUp,
      ScrollDown,
      _Last
    };

    bool buttons_current[(unsigned)Button::_Last];
    bool buttons_last[(unsigned)Button::_Last];
    bool buttons_changed[(unsigned)Button::_Last];
    ButtonState mouseStates[(unsigned)Button::_Last];
    POINT position;
    POINT position_last;

    void Update();
    inline void SetPosition(int x, int y)
    {
      position.x = x;
      position.y = y;
    }
    inline void SetButton(Mouse::Button button, bool state)
    {
      buttons_current[(unsigned)button] = state;
    }

    Mouse()
    {
      std::fill_n(buttons_current, (unsigned)Button::_Last, false);
      std::fill_n(buttons_last, (unsigned)Button::_Last, false);
    }
  };

  class Input
  {
  public:
    enum class Mode
    {
      Keyboard = 0,
      Gamepad = 1,
      MindControl = 2
    };

    static Gamepad gamepads[XUSER_MAX_COUNT];
    static Keyboard keyboard;
    static Mouse mouse;
    static Mode mode;
    static Mode modePrevious;
    static bool modeChanged;

    static std::unordered_map<std::string, std::function<bool(void)>> actions;
    static std::unordered_map<std::string, std::function<bool(void)>> systemActions;
    static ButtonState MakeButtonState(bool last, bool cur);
    static bool autoplay;

    static void Initialize();
    static bool Check(std::string action)
    {
      if (!autoplay)
        return actions[action]();
      else
      {
        if (action != "pause")
        {
          int random = std::rand() % 2;
          if (random == 1)
            return true;
          else
            return false;
        }

        // do not let the autoplayer pause
        return false;
      }
    }

    inline static bool SystemCheck(std::string action)
    {
      return systemActions[action]();
    }

    static void Shutdown();

    static void Update();
    static void UpdateGamepads();
  };

}
