/******************************************************************************
Filename: Face.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "Vec2.h"

namespace Omni
{
  class Face
  {
  public:
    Vec2 point1;
    Vec2 point2;

    // default constructor
    Face() : point1(), point2() {}

    Face(Vec2 a, Vec2 b) : point1(a), point2(b) {}

    // returns the right normal
    Vec2 findNormal() const
    {
      Vec2 ret = { point2.y - point1.y, -(point2.x - point1.x) };
      return ret;
    }

    // finds the vector from point 1 to point 2
    Vec2 findVector() const
    {
      Vec2 ret = { point2.x - point1.x, point2.y - point1.y };
      return ret;
    }

    Vec2 findMidPoint() const
    {
      Vec2 ret = { point1.x + point2.x / 2, point1.y + point2.y / 2 };
      return ret;
    }

    Vec2 &operator[](const int index)
    {
      if (index == 0)
        return point1;
      else
        return point2;
    }
  };
}