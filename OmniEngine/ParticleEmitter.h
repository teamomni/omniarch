/******************************************************************************
Filename: ParticleEmitter.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Michael Tyler

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "Component.h"
#include <memory>
#include "AntTweakBar.h"
#include "Image.h"
#include "Vec2.h"

namespace Omni
{

#ifndef M_PI
  #define M_PI 		  3.1415926535897932384626433832795f    // PI
  #define M_2_PI 		6.28318530717958647692528676655901f		// PI*2
#endif

  struct Time
  {
    float x;
    float y;
    float z;
  };

  enum PosType
  {
    PT_NONE = -1,
    PT_POS,
    PT_CIR,
    PT_BOX,
    PT_MAX
  };

  enum BehaviourType
  {
    Default = 0,
    Trail = 1
  };

  // Container for Particles
  class ParticleData
  {
  public:
    bool *m_alive{nullptr};                // Is particle active
    Vec2 *m_pos{ nullptr };                // Position 
    Vec2 *m_vel{ nullptr };                // Velocity
    Vec2 *m_acc{ nullptr };                // Acceleration
    Vec2 *m_scale{ nullptr };              // Particle Scale
    Vec2 *m_offset{ nullptr };             // used for following transform
    float *m_rot{ nullptr };               // Rotation 
    Time *m_time{ nullptr };               // Time particle is alive
    D3DXCOLOR *m_col{ nullptr };           // Color
    D3DXCOLOR *m_startColor{ nullptr };    // Starting Color
    D3DXCOLOR *m_endColor{ nullptr };      // Ending Color

    unsigned m_count{ 0 };      // Max number of Particle in Container
    unsigned m_countAlive{ 0 }; // Number of Particles alive

    // Constructors and Destructor      
    ParticleData() {}
    explicit ParticleData(int maxCount) { Generate(maxCount); }
    ~ParticleData() {}


    void Generate(int maxSize);         // Allocates memory needed for Container
    void Clean();                       // Deallocates all memory
    void Kill(size_t id);               // Mark particle non-active
    void Wake(size_t id);               // Mark particle active
    void SwapData(size_t a, size_t b);  // Swap particles in container
  };

  class ParticleEmitter : public SystemComponent<ParticleEmitter>
  {
    public:
      ParticleEmitter();
      ~ParticleEmitter();

      // component functions
      void Update(float dt);
      void Initialize();
      void Destroy();
      static void Register();

      // v8 stuff
      static void v8_Register();

      void emit(float dt);

      //-------------------------- Update ---------------------------//
      void TimeUpdate(float dt);
      void AttractorUpdate(float dt);
      void AccelUpdate(float dt);
      void ColorUpdate(float dt);
      //-------------------------------------------------------------//

      //------------------------ Generators -------------------------//
      void TimeGen(unsigned startPos, unsigned endPos);
      void ColorGen(unsigned startPos, unsigned endPos);
      void PosGen(unsigned startPos, unsigned endPos);
      void CirGen(unsigned startPos, unsigned endPos);
      void BoxGen(unsigned startPos, unsigned endPos);
      void VelGen(unsigned startPos, unsigned endPos);
      void ScaleGen(unsigned startPos, unsigned endPos);
      void RotGen(unsigned startPos, unsigned endPos);
      //-------------------------------------------------------------//

      //------------------------ Generators -------------------------//
      void addUI(TwBar *ownerBar);
      static void TW_CALL SetImageCB(const void *value, void *clientData);
      static void TW_CALL GetImageCB(void *value, void *clientData);
      //-------------------------------------------------------------//

      std::string &GetImage();
      Image *GetImagePointer();
      void SetImage(std::string imageName);

      bool m_isActive;                // Controls if emitter creates particles
      bool m_additive;                // makes sprite additive
      bool m_isFlippedX;              // Flips the particles sprites
      bool m_isFlippedY;              // Flips the particles sprites

      // properties
      ParticleData particles;         // Particle Container                 
      BehaviourType behaviourType;    // Trail behaviour is a type

      // trail stuff
      float m_stretchSpeed;
      bool  m_displayIfStatic;
      bool  m_hasSpawnedParticles;
      std::vector<Vec2> startPositions;
      float m_startingAlpha;
      float m_endingAlpha;

      // Dampening

      // Image for particle
      Image *image = nullptr;         // Image of particles
      Property::String p_Image = "";  // String name
      float p_Depth;                  // Z buffer depth
      Vec2 p_UV1 = Vec2(0, 0);
      Vec2 p_UV2 = Vec2(1, 1);

      Vec2 m_scale;                   // scale of the particles
      float m_rot;                    // Rotation of the particles
      float m_emitRate;               // emitter rate
      unsigned m_emitCount;           // Spawns this number of particles and stop (if 0 spawn infinetly)
      unsigned m_spawned;             // particles that have spawned
      
      int m_posType;                  // method which the particle are spawned
      
      //---------------------- Spawning Data -------------------------//
      float m_minTime;                // Min life time
      float m_maxTime;                // Max life time

      float m_radX;                   // Radius in the X direction
      float m_radY;                   // Radius in the Y direction

      float m_width;                  // Width of the Box spawning
      float m_height;                 // Height of the Box spawning

      Vec2 m_MaxOffsetPos;            // Offset from component's Tranform
      
      Vec2 m_minStartVel;             // Min start value for velocity
      Vec2 m_maxStartVel;             // Max Start value of velocity

      Vec2 m_minStartPosition;        // Min start value for position
      Vec2 m_maxStartPosition;        // Max Start value of position

      float m_minScalar;              // Min start value for scale multiplier
      float m_maxScalar;              // Max Start value of scale multiplier
      bool  m_scaleX;                 // Determines if x should be scaled by scalar
      bool  m_scaleY;                 // Determines if y should be scaled by scalar
      
      bool  m_followTransform;        // Determines if particles should follow transform

      D3DXCOLOR m_minStartCol;        // Min start value of color 
      D3DXCOLOR m_maxStartCol;        // Max start value of color
      D3DXCOLOR m_minEndCol;          // Min end vlaue of color
      D3DXCOLOR m_maxEndCol;          // Max start value of color
      //--------------------------------------------------------------//

      Vec2 m_globalAcceleration;      // Acceleration of particles in given diretion

      //-------------------------- Effects ---------------------------//
      // Attractor
      bool m_isAttracting;            // Turns attracotr effect on and off
      float m_strength;               // How stonger the particle is pulled
      float m_minDistance;            // min distance from emitter
      //float m_maxDistance;            // max distance from emitter 
      //--------------------------------------------------------------//
  };

  
}