/******************************************************************************
Filename: Entity.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Entity.h"
#include "Game.h"
#include "PropertyHandle.h"
#include "RigidBody.h"
#include "ParticleEmitter.h"
#include <algorithm>

namespace Omni
{
  size_t Entity::nextID = 0;
  size_t Entity::nextlinkID = 1;

  Entity::Entity(Space *space_, size_t handle_) : space(space_), name(""), id(GenerateID()), linkID(0), clickable(), archetype(""), componentToCreate("Enter Text"), active(true)
  { 
    dispatcher = new EventDispatcher;
    dispatcher->type = EventDispatcher::Type::Entity;
    dispatcher->ownerID = id;
    clickable.ownerHandle = Handlet<Entity>(selfPtr);
    clickable.space = space;
  }

  Entity::Entity(Space *space_, std::string name_, size_t handle_) : space(space_), name(name_), id(GenerateID()), linkID(0), clickable(), archetype(""), componentToCreate("Enter Text"), active(true)
  {
    dispatcher = new EventDispatcher;
    dispatcher->type = EventDispatcher::Type::Entity;
    dispatcher->ownerID = id;
    clickable.ownerHandle = Handlet<Entity>(selfPtr);
    clickable.space = space;
  }

  void Entity::Initialize()
  {
    // initialize all components
    auto comps = components;

    for (auto &comp : comps)
    {
      comp.second->Initialize();
    }
  }
    
  void Entity::Destroy()
  {
    active = false;
    Game &game = Game::Instance();
    while (!components.empty())
    {
      RemoveComponent(components.begin()->second->name);
    }
    space->MarkEntityForDeletion(this);
    game.entityHandles.erase(id);
    dispatcher->Destroy(); //destroy the dispatcher
    delete dispatcher; // delete the dispatcher pointer
    *selfPtr = nullptr; // deleted in space destructor
    ScriptableDestroy(); // needs to be called both in destroy and destructor
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    if (tweakBar != nullptr)
      TwDeleteBar(tweakBar);
#endif
  }

  void Entity::DeleteTweakBar()
  {
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    if (tweakBar != nullptr)
    {
      TwDeleteBar(tweakBar);
      tweakBar = nullptr;
    }
#endif
  }

  Handlet<Component> Entity::AddComponentWhileEditing(std::string name)
  {
    // create the component and initialize it
    Handlet<Component> created = AddComponent(name);
    created->Initialize();
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    // respawn the tweakbar
    if (tweakBar)
    {
      DeleteTweakBar();
    }
    SpawnTweakBar();
#endif
    return Handlet<Component>(created->selfPtr);
  }

  Handlet<Component> Entity::AddComponent(std::string name)
  {
    Handlet<Component> created = Game::Instance().componentDefinitions[name].Create(this);
    components.insert({ name, created });
    return created;
  }

  void Entity::RemoveComponent(std::string name)
  {
    Component *comp = *GetComponent(name).object;
    Component **compSelfPtr = comp->selfPtr;
    comp->_Destroy();
    if (comp->type == Component::Type::User) 
    {
      delete compSelfPtr;
      delete comp;
    }
    if (comp->type == Component::Type::System)
      comp->Finalize();
    components.erase(name);
  }

  void Entity::ApplyArchetype(std::string archetypeName)
  {
    if (Game::Instance().assets.archetypes.count(archetypeName) == 0)
    {
      Console::Error("Critical error loading archetype", "Archetype \"" + archetypeName + "\" does not exist, but Entity #" + std::to_string(id) + " has it set as its Archetype.");
      return;
    }
    Archetype &arch = Game::Instance().assets.archetypes[archetypeName];

    if (arch.hasTranslation)
      transform.translation = arch.transform.translation;

    if (arch.hasRotation)
      transform.rotation = arch.transform.rotation;

    if (arch.hasScale)
      transform.scale = arch.transform.scale;

    for (auto const &compDef : arch.components)
    {
      if (Game::Instance().componentDefinitions.count(compDef.first) == 0)
      {
        Console::Error("Critical Entity loading error", "Entity #" + std::to_string(id) + " contains data for the \"" + compDef.first + "\" component, which does not exist.");
        continue;
      }

      Handlet<Component> comp = AddComponent(compDef.first);
      
      // set all the property stuff
      for (auto const &propDef : compDef.second)
      {
        switch (propDef.second.type)
        {
        case Property::Type::Bool:
          comp->SetProperty_Bool(propDef.first.c_str(), propDef.second.data_bool);
          break;
        case Property::Type::Color:
          comp->SetProperty_Color(propDef.first.c_str(), propDef.second.data_color);
          break;
        case Property::Type::Float:
          comp->SetProperty_Float(propDef.first.c_str(), propDef.second.data_float);
          break;
        case Property::Type::Int:
          comp->SetProperty_Int(propDef.first.c_str(), propDef.second.data_int);
          break;
        case Property::Type::String:
          comp->SetProperty_String(propDef.first.c_str(), propDef.second.data_string);
          break;
        case Property::Type::Undefined:
          // TODO: ERROR?!?!?!
          break;
        case Property::Type::Unsigned:
          comp->SetProperty_Unsigned(propDef.first.c_str(), propDef.second.data_unsigned);
          break;
        case Property::Type::Vec2:
          comp->SetProperty_Vec2(propDef.first.c_str(), propDef.second.data_vec2);
          break;
        case Property::Type::Vec2List:
          comp->SetProperty_Vec2List(propDef.first.c_str(), propDef.second.data_vec2List);
          break;
        }
      }
    }
    archetype = archetypeName;
  }

  size_t Entity::GetLinkID()
  {
    return linkID;
  }

  void Entity::SetLinkID(size_t linkID_)
  {
    linkID = linkID_;

    // make sure to store the current highest linkid
    if (linkID > nextlinkID)
      nextlinkID = linkID;
  }

  void Entity::Serialize(Json::Value &root) const
  {
    //Console::Info("", "  Serializing ENTITY #" + std::to_string(id) + (name == "" ? "" : " (" + name + ")"));
    root["name"] = name;
    if (archetype != "")
      root["archetype"] = archetype;
    root["transform"]["translation"]["x"] = transform.translation.x;
    root["transform"]["translation"]["y"] = transform.translation.y;
    root["transform"]["scale"]["x"] = transform.scale.x;
    root["transform"]["scale"]["y"] = transform.scale.y;
	  root["transform"]["rotation"] = transform.rotation;
	  root["linkID"] = linkID ? linkID : generateLinkID();
    
    bool addedComponent = false;
    size_t i = 0;
    for (auto const &compPair : components)
    {
      //Component *comp = Game::Instance().GetSystemInstance(compPair.first, compPair.second);
      Component *comp = *compPair.second;
      std::unordered_map<std::string, PropertyContainer> archProps;

      PropertyDefinitionMap &compProps = Game::Instance().componentDefinitions[compPair.first].properties;

      std::vector<std::string> changedProps;

      bool compIsInArchetype = false;
      if (archetype != "" && Game::Instance().assets.archetypes[archetype].components.count(compPair.first) != 0)
      {
        archProps = Game::Instance().assets.archetypes[archetype].components[compPair.first];
        compIsInArchetype = true;
      }

      for (auto const &propPair : compPair.second->properties)
      {
        if (compIsInArchetype)
        {
          if (archProps.count(propPair.first) > 0)
          {
            switch (archProps[propPair.first].type)
            {
            case Property::Type::Bool:
              if (comp->GetProperty_Bool(propPair.first.c_str()) != archProps[propPair.first].data_bool)
                changedProps.push_back(propPair.first);
              break;
            case Property::Type::Color:
              if (comp->GetProperty_Color(propPair.first.c_str()) != archProps[propPair.first].data_color)
                changedProps.push_back(propPair.first);
              break;
            case Property::Type::Float:
              if (comp->GetProperty_Float(propPair.first.c_str()) != archProps[propPair.first].data_float)
                changedProps.push_back(propPair.first);
              break;
            case Property::Type::Int:
              if (comp->GetProperty_Int(propPair.first.c_str()) != archProps[propPair.first].data_int)
                changedProps.push_back(propPair.first);
              break;
            case Property::Type::String:
              if (comp->GetProperty_String(propPair.first.c_str()) != archProps[propPair.first].data_string)
                changedProps.push_back(propPair.first);
              break;
            case Property::Type::Undefined:
              // TODO: error or warning or something??
              break;
            case Property::Type::Unsigned:
              if (comp->GetProperty_Unsigned(propPair.first.c_str()) != archProps[propPair.first].data_unsigned)
                changedProps.push_back(propPair.first);
              break;
            case Property::Type::Vec2:
              if (comp->GetProperty_Vec2(propPair.first.c_str()) != archProps[propPair.first].data_vec2)
                changedProps.push_back(propPair.first);
              break;
            case Property::Type::Vec2List:
              if (comp->GetProperty_Vec2List(propPair.first.c_str()) != archProps[propPair.first].data_vec2List)
                changedProps.push_back(propPair.first);
              break;
            }
            continue;
          }
        }

        switch (compProps[propPair.first].type)
        {
        case Property::Type::Bool:
          if (comp->GetProperty_Bool(propPair.first.c_str()) != compProps[propPair.first].v_bool)
          {
            changedProps.push_back(propPair.first);
            continue;
          }
          break;
        case Property::Type::Color:
          if (comp->GetProperty_Color(propPair.first.c_str()) != compProps[propPair.first].v_color)
          {
            changedProps.push_back(propPair.first);
            continue;
          }
          break;
        case Property::Type::Float:
          if (comp->GetProperty_Float(propPair.first.c_str()) != compProps[propPair.first].v_float)
          {
            changedProps.push_back(propPair.first);
            continue;
          }
          break;
        case Property::Type::Int:
          if (comp->GetProperty_Int(propPair.first.c_str()) != compProps[propPair.first].v_int)
          {
            changedProps.push_back(propPair.first);
            continue;
          }
          break;
        case Property::Type::String:
          if (comp->GetProperty_String(propPair.first.c_str()) != compProps[propPair.first].v_string)
          {
            changedProps.push_back(propPair.first);
            continue;
          }
          break;
        case Property::Type::Undefined:
          // TODO: error or warning or something??
          break;
        case Property::Type::Unsigned:
          if (comp->GetProperty_Unsigned(propPair.first.c_str()) != compProps[propPair.first].v_unsigned)
          {
            changedProps.push_back(propPair.first);
            continue;
          }
          break;
        case Property::Type::Vec2:
          if (comp->GetProperty_Vec2(propPair.first.c_str()) != compProps[propPair.first].v_vec2)
          {
            changedProps.push_back(propPair.first);
            continue;
          }
          break;
        case Property::Type::Vec2List:
        {
          Property::Vec2List &mine = comp->GetProperty_Vec2List(propPair.first.c_str());
          Property::Vec2List &default = compProps[propPair.first].v_vec2list;
            if (mine.size() != default.size() && mine != default)
            {
              changedProps.push_back(propPair.first);
              continue;
            }
          break;
        }
        }

      }

      if (changedProps.size() > 0 || !compIsInArchetype)
      {
        if (!addedComponent)
        {
          root["components"] = Json::arrayValue;
          addedComponent = true;
        }
        root["components"].append(Json::objectValue);
        Json::Value &compElem = root["components"][i++];
        compElem["name"] = compPair.first;

        if (changedProps.size() > 0)
        {
          compElem["properties"] = Json::objectValue;
          for (auto const &prop : changedProps)
          {
            switch (compProps[prop].type)
            {
            case Property::Type::Bool:
              compElem["properties"][prop] = comp->GetProperty_Bool(prop.c_str());
              break;
            case Property::Type::Color:
            {
              Property::Color &color = comp->GetProperty_Color(prop.c_str());
              compElem["properties"][prop] = Json::objectValue;
              compElem["properties"][prop]["r"] = color.r;
              compElem["properties"][prop]["g"] = color.g;
              compElem["properties"][prop]["b"] = color.b;
              compElem["properties"][prop]["a"] = color.a;
            }
            break;
            case Property::Type::Float:
              compElem["properties"][prop] = comp->GetProperty_Float(prop.c_str());
              break;
            case Property::Type::Int:
              compElem["properties"][prop] = comp->GetProperty_Int(prop.c_str());
              break;
            case Property::Type::String:
              compElem["properties"][prop] = comp->GetProperty_String(prop.c_str());
              break;
            case Property::Type::Undefined:
              // TODO: error or warning or something??
              break;
            case Property::Type::Unsigned:
              compElem["properties"][prop] = static_cast<Json::UInt64>(comp->GetProperty_Unsigned(prop.c_str()));
              break;
            case Property::Type::Vec2:
            {
              Property::Vec2 &vec = comp->GetProperty_Vec2(prop.c_str());
              compElem["properties"][prop] = Json::objectValue;
              compElem["properties"][prop]["x"] = vec.x;
              compElem["properties"][prop]["y"] = vec.y;
            }
              break;
            case Property::Type::Vec2List:
            {
              Property::Vec2List &vecList = comp->GetProperty_Vec2List(prop.c_str());
              compElem["properties"][prop] = Json::arrayValue;
              for (auto const &vec : vecList)
              {
                Json::Value &elem = compElem["properties"][prop].append(Json::objectValue);
                elem["x"] = vec.x;
                elem["y"] = vec.y;
              }
            }
              break;
            }
          }
        }
      }

      
      
      //Json::Value &elem = root["components"].append(Json::objectValue);
      //comp->Serialize(elem);
    }
  }

  void Entity::SpawnTweakBar()
  {
#if !defined(_DEBUG) && !defined(_RELEASE)
    return;
#endif
    if (tweakBar != nullptr)
      return;
    tweakBar = TwNewBar((std::string("[E] ") + name).c_str());
    
    TwDefine("");
    FillTweakBar();
  }

  std::string componentToCreate = "NULL";

  static void TW_CALL SetComponentName(const void *value, void *clientData)
  {
    Entity *entity = *static_cast<Entity **>(clientData);
    entity->componentToCreate = *static_cast<const std::string *>(value);
  }

  static void TW_CALL GetComponentName(void *value, void *clientData)
  {
    // Get: copy the value of s to AntTweakBar
    Entity *entity = *static_cast<Entity **>(clientData);
    std::string *destPtr = static_cast<std::string *>(value);
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    TwCopyStdStringToLibrary(*destPtr, entity->componentToCreate); // the use of TwCopyStdStringToLibrary is required here
#endif
  }

  void Entity::FillTweakBar()
  {
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    if (tweakBar == nullptr)
      return;
    TwRemoveAllVars(tweakBar);
    TwAddVarCB(tweakBar, "_ID", TW_TYPE_UINT32, NULL,
      [](void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      *static_cast<size_t *>(value) = entity->id;
    },
      selfPtr,
      " label=ID"
      );

    TwAddVarCB(tweakBar, "_LINKID", TW_TYPE_UINT32, NULL,
      [](void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      *static_cast<size_t *>(value) = entity->GetLinkID();
    },
      selfPtr,
      " label=LinkID"
      );

    TwAddVarCB(tweakBar, "_Name", TW_TYPE_STDSTRING, [](const void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      entity->name = *static_cast<const std::string *>(value);
    },
      [](void *value, void *clientData)
    {
      // Get: copy the value of s to AntTweakBar
      Entity *entity = *static_cast<Entity **>(clientData);
      std::string *destPtr = static_cast<std::string *>(value);
      TwCopyStdStringToLibrary(*destPtr, entity->name); // the use of TwCopyStdStringToLibrary is required here
    }, selfPtr, " label = Name");

    TwAddVarCB(tweakBar, "_Comp/Arch", TW_TYPE_STDSTRING, SetComponentName, GetComponentName, 
      selfPtr, " label = Comp/Arch");

    TwAddButton(tweakBar, "AddComponent", [](void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      // only create if the component definition is correct
      if (Game::Instance().componentDefinitions.find(entity->componentToCreate) != Game::Instance().componentDefinitions.end())
      {
        // only create if it doesn't already have it
        if (entity->GetComponent(entity->componentToCreate) == nullptr)
          entity->AddComponentWhileEditing(entity->componentToCreate);
        else
          Console::Warning("Componenet Already Added", "");
      }
      else
        Console::Warning("Invalid Component", "Check Your Spelling Ed.");
    }, selfPtr, NULL);

    TwAddButton(tweakBar, "ApplyArchetype", [](void *clientData)
    {
      Handlet<Entity> entity = (*static_cast<Entity **>(clientData))->selfPtr;
      // only create if the archetype exists
      if (Game::Instance().assets.archetypes.count(entity->componentToCreate) != 0)
      {
        // also store this in the game level tweakbar
        Game::Instance().lastCreatedArchetype = entity->componentToCreate;
        entity->ApplyArchetype(entity->componentToCreate);
        entity->Initialize();
        // respawn the tweakbar
        if (entity->tweakBar)
        {
          entity->DeleteTweakBar();
        }
        entity->SpawnTweakBar();
      }
      else

        Console::Warning("Invalid Archetype", "Check Your Spelling Ed.");
    }, selfPtr, NULL);

    TwAddButton(tweakBar, "_Transform", NULL, NULL, " label=Transform");
    TwAddVarCB(tweakBar, "_PosX", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      entity->transform.translation.x = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      *static_cast<float *>(value) = entity->transform.translation.x;
    },
      selfPtr,
      " label=X step=0.001 group=Translation"
      );
    TwAddVarCB(tweakBar, "_PosY", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      entity->transform.translation.y = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      *static_cast<float *>(value) = entity->transform.translation.y;
    },
      selfPtr,
      " label=Y step=0.001 group=Translation"
      );

    TwAddVarCB(tweakBar, "_ScaleX", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      entity->transform.scale.x = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      *static_cast<float *>(value) = entity->transform.scale.x;
    },
      selfPtr,
      " label=X step=0.001 group=Scale"
      );

    TwAddVarCB(tweakBar, "_ScaleY", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      entity->transform.scale.y = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      *static_cast<float *>(value) = entity->transform.scale.y;
    },
      selfPtr,
      " label=Y step=0.001 group=Scale"
      );

    TwAddVarCB(tweakBar, "_Rotation", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      entity->transform.rotation = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      *static_cast<float *>(value) = entity->transform.rotation;
    },
      selfPtr,
      " label=Rotation step=0.001"
      );

    for (auto const &modPair : components)
    {
      
      if (!strcmp(modPair.first.c_str(),"ParticleEmitter"))
      {
        TwAddSeparator(tweakBar, NULL, NULL);
        ParticleEmitter *emitter = reinterpret_cast<ParticleEmitter *>(*modPair.second);
        emitter->addUI(tweakBar);
        continue;
      }

      TwAddSeparator(tweakBar, NULL, NULL);
      TwAddButton(tweakBar, modPair.first.c_str(), NULL, NULL, NULL);
      
      for (auto const &propPair : modPair.second->properties)
      {
        AddTweakBarProperty(modPair.first, Game::Instance().GetPropertyHandle(modPair.second, propPair.first), propPair.second.type);
      }
    }

    TwAddSeparator(tweakBar, NULL, NULL);
    TwAddButton(tweakBar, "Destroy", [](void *clientData)
    {
      Entity *entity = *static_cast<Entity **>(clientData);
      entity->Destroy();
    }, selfPtr, NULL);
#endif
  }

  void Entity::AddTweakBarProperty(std::string compName, PropertyHandle *propHandle, Property::Type propType)
  {
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    switch (propType)
    {
    case Property::Type::Bool:
      TwAddVarCB(tweakBar, std::string(compName + propHandle->property).c_str(), TW_TYPE_BOOLCPP, [](const void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        handle->GetComponent()->SetProperty_Bool(handle->property.c_str(), *reinterpret_cast<const Property::Bool *>(value));
      },
        [](void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        *reinterpret_cast<Property::Bool *>(value) = handle->GetComponent()->GetProperty_Bool(handle->property.c_str());
      },
        propHandle,
        (std::string(" label=") + propHandle->property).c_str());
      break;
    case Property::Type::Unsigned:
      TwAddVarCB(tweakBar, std::string(compName + propHandle->property).c_str(), TW_TYPE_UINT32, [](const void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        handle->GetComponent()->SetProperty_Unsigned(handle->property.c_str(), *reinterpret_cast<const Property::Unsigned *>(value));
      },
        [](void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        *reinterpret_cast<Property::Unsigned *>(value) = handle->GetComponent()->GetProperty_Unsigned(handle->property.c_str());
      },
        propHandle,
        (std::string(" step=1 label=") + propHandle->property).c_str());
      break;
    case Property::Type::Int:
      TwAddVarCB(tweakBar, std::string(compName + propHandle->property).c_str(), TW_TYPE_INT32, [](const void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        handle->GetComponent()->SetProperty_Int(handle->property.c_str(), *reinterpret_cast<const Property::Int *>(value));
      },
        [](void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        *reinterpret_cast<Property::Int *>(value) = handle->GetComponent()->GetProperty_Int(handle->property.c_str());
      },
        propHandle,
        (std::string(" step=1 label=") + propHandle->property).c_str());
      break;
    case Property::Type::Float:
      TwAddVarCB(tweakBar, std::string(compName + propHandle->property).c_str(), TW_TYPE_FLOAT, [](const void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        handle->GetComponent()->SetProperty_Float(handle->property.c_str(), *reinterpret_cast<const Property::Float *>(value));
      },
        [](void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        *reinterpret_cast<Property::Float *>(value) = handle->GetComponent()->GetProperty_Float(handle->property.c_str());
      },
        propHandle,
        (std::string(" step=0.01 label=") + propHandle->property).c_str());
      break;
    case Property::Type::String:
      TwAddVarCB(tweakBar, std::string(compName + propHandle->property).c_str(), TW_TYPE_STDSTRING, [](const void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        handle->GetComponent()->SetProperty_String(handle->property.c_str(), *reinterpret_cast<const Property::String *>(value));
      },
        [](void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        TwCopyStdStringToLibrary(*reinterpret_cast<Property::String *>(value), handle->GetComponent()->GetProperty_String(handle->property.c_str()));
      },
        propHandle,
        (std::string(" label=") + propHandle->property).c_str());
      break;
    case Property::Type::Vec2:
      TwAddVarCB(tweakBar, (std::string(compName + propHandle->property) + std::string("X")).c_str(), TW_TYPE_FLOAT, [](const void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        Property::Vec2 cur = handle->GetComponent()->GetProperty_Vec2(handle->property.c_str());
        cur.x = *reinterpret_cast<const Property::Float *>(value);
        handle->GetComponent()->SetProperty_Vec2(handle->property.c_str(), cur);
      },
        [](void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        *reinterpret_cast<Property::Float *>(value) = handle->GetComponent()->GetProperty_Vec2(handle->property.c_str()).x;
      },
        propHandle,
        (std::string(" step=0.01 label=X group=") + propHandle->property).c_str());
      TwAddVarCB(tweakBar, (std::string(compName + propHandle->property) + std::string("Y")).c_str(), TW_TYPE_FLOAT, [](const void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        Property::Vec2 cur = handle->GetComponent()->GetProperty_Vec2(handle->property.c_str());
        cur.y = *reinterpret_cast<const Property::Float *>(value);
        handle->GetComponent()->SetProperty_Vec2(handle->property.c_str(), cur);
      },
        [](void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        *reinterpret_cast<Property::Float *>(value) = handle->GetComponent()->GetProperty_Vec2(handle->property.c_str()).y;
      },
        propHandle,
        (std::string(" step=0.01 label=Y group=") + propHandle->property).c_str());
      break;
    case Property::Type::Color:
      
      TwAddVarCB(tweakBar, (std::string(compName + propHandle->property) + std::string("R")).c_str(), TW_TYPE_FLOAT, [](const void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        Property::Color cur = handle->GetComponent()->GetProperty_Color(handle->property.c_str());
        cur.r = *reinterpret_cast<const Property::Float *>(value);
        handle->GetComponent()->SetProperty_Color(handle->property.c_str(), cur);
      },
        [](void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        *reinterpret_cast<Property::Float *>(value) = handle->GetComponent()->GetProperty_Color(handle->property.c_str()).r;
      },
        propHandle,
        (std::string(" min=0 max=1 step=0.005 label=R group=") + propHandle->property).c_str());
      TwAddVarCB(tweakBar, (std::string(compName + propHandle->property) + std::string("G")).c_str(), TW_TYPE_FLOAT, [](const void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        Property::Color cur = handle->GetComponent()->GetProperty_Color(handle->property.c_str());
        cur.g = *reinterpret_cast<const Property::Float *>(value);
        handle->GetComponent()->SetProperty_Color(handle->property.c_str(), cur);
      },
        [](void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        *reinterpret_cast<Property::Float *>(value) = handle->GetComponent()->GetProperty_Color(handle->property.c_str()).g;
      },
        propHandle,
        (std::string(" min=0 max=1 step=0.005 label=G group=") + propHandle->property).c_str());
      TwAddVarCB(tweakBar, (std::string(compName + propHandle->property) + std::string("B")).c_str(), TW_TYPE_FLOAT, [](const void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        Property::Color cur = handle->GetComponent()->GetProperty_Color(handle->property.c_str());
        cur.b = *reinterpret_cast<const Property::Float *>(value);
        handle->GetComponent()->SetProperty_Color(handle->property.c_str(), cur);
      },
        [](void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        *reinterpret_cast<Property::Float *>(value) = handle->GetComponent()->GetProperty_Color(handle->property.c_str()).b;
      },
        propHandle,
        (std::string(" min=0 max=1 step=0.005 label=B group=") + propHandle->property).c_str());
      TwAddVarCB(tweakBar, (std::string(compName + propHandle->property) + std::string("A")).c_str(), TW_TYPE_FLOAT, [](const void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        Property::Color cur = handle->GetComponent()->GetProperty_Color(handle->property.c_str());
        cur.a = *reinterpret_cast<const Property::Float *>(value);
        handle->GetComponent()->SetProperty_Color(handle->property.c_str(), cur);
      },
        [](void *value, void *clientData)
      {
        auto handle = reinterpret_cast<PropertyHandle *>(clientData);
        *reinterpret_cast<Property::Float *>(value) = handle->GetComponent()->GetProperty_Color(handle->property.c_str()).a;
      },
        propHandle,
        (std::string(" min=0 max=1 step=0.005 label=A group=") + propHandle->property).c_str());
      break;
    }
#endif
  }

  bool Game::EntityIsAlive(size_t id)
  {
    Handlet<Entity> eh = Game::Instance().GetEntityByID(id);
    return (eh.isValid() && eh->active);
  }
}