/******************************************************************************
Filename: Material.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

struct Material
{
  float density;
  float restitution;
  float friction;
  Material(float density_, float restitution_, float friction_) : density(density_), restitution(restitution_), friction(friction_) {}
  Material() : density(1.0f), restitution(0.6f), friction(0.4f) {}
};