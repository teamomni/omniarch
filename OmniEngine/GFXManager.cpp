/******************************************************************************
Filename: GFXManager.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//Juli Gregg
//GFXManager.cpp
//Manages the renderer object

#include "stdafx.h"
#include "Game.h"
#include "GFXManager.h"
#include <iostream>
#include "GFXDebugDraw.h"
#include "Space.h"

namespace Omni
{
  namespace Graphics
  {


    GFXManager::GFXManager(void)
    {
      m_renderer = nullptr;
    }

    GFXManager::~GFXManager(void)
    {
    }

    //------------------------------------------//
    // Sets up the renderer and buffers needed  //
    //------------------------------------------//
    bool GFXManager::Initialize(WindowSystem& Window)
    {
      //Create a new Renderer object if doesn't already exists
      if (!m_renderer)
        m_renderer = new Graphics::GFXRenderer();

      //Check if graphics was already initialized
      if (!m_renderer->IsInitialized())
      {
        //Initialize graphics
        if (!m_renderer->Initialized(Window))
        {
          std::cout << "FAILED TO INITIALIZE GRAPHICS" << std::endl;
          return false;
        }
        else
        {
          std::cout << "INITIALIZED GRAPHICS" << std::endl;

          //Initialize TweakBar
#if defined(_DEBUG) || defined(_RELEASE)
          TwInit(TW_DIRECT3D11, m_renderer->GetDevice());
          TwWindowSize(Window.GetClientWidth(), Window.GetClientHeight());

          //Set Up Graphics TweakBar
          if (m_graphicsBar == NULL)
          {
            m_graphicsBar = TwNewBar("OmniBar");

            TwAddVarRW(m_graphicsBar, "BackgroundColor",     TW_TYPE_COLOR3F, &m_clearColor,      "");
            TwAddVarRW(m_graphicsBar, "DebugDraw",           TW_TYPE_BOOLCPP, &m_debugDraw,       "");
            TwAddVarRW(m_graphicsBar, "Wireframe",           TW_TYPE_BOOLCPP, &m_wireframe,       "");
            TwAddVarRW(m_graphicsBar, "FullScreen",          TW_TYPE_BOOLCPP, &m_fullScreen,      "");
            TwAddVarRW(m_graphicsBar, "RT Opaque",           TW_TYPE_BOOLCPP, &m_opaque_rt,       "");
            TwAddVarRW(m_graphicsBar, "RT Glow: Alpha",      TW_TYPE_BOOLCPP, &m_glow_rt,         "");
            TwAddVarRW(m_graphicsBar, "RT Glow: Additive",   TW_TYPE_BOOLCPP, &m_glow_Add_rt,     "");
            TwAddVarRW(m_graphicsBar, "Blur Pass",           TW_TYPE_BOOLCPP, &m_blur_pass,       "");
            TwAddVarRW(m_graphicsBar, "Bloom Pass",          TW_TYPE_BOOLCPP, &m_bloom_pass,      "");
            TwAddVarRW(m_graphicsBar, "Bloom Threshold",     TW_TYPE_FLOAT,   &m_bloom_threshold, "step=0.01");
            TwAddVarRW(m_graphicsBar, "Global Glow Enabled", TW_TYPE_BOOLCPP, &m_glow, "");

            TwAddVarCB(m_graphicsBar, "_Level", TW_TYPE_STDSTRING, [](const void *value, void *clientData)
            {
              Game::Instance().nextLevel = *static_cast<const std::string *>(value);
            },
              [](void *value, void *clientData)
            {
              // Get: copy the value of s to AntTweakBar
              std::string *destPtr = static_cast<std::string *>(value);
              TwCopyStdStringToLibrary(*destPtr, Game::Instance().nextLevel); // the use of TwCopyStdStringToLibrary is required here
            }, 0, " label = Level");

            TwAddButton(m_graphicsBar, "LoadLevel", [](void *clientData)
            {
              Game::Instance().loadNextLevel = true;
            }, 0, NULL);

            TwAddVarRW(m_graphicsBar, "Serialize Camera", TW_TYPE_BOOLCPP, &Game::Instance().serializeCamera, "");

            TwAddButton(m_graphicsBar, "Emergency Save", [](void *clientData)
            {
              Game::Instance().Serialize();
            }, 0, NULL);

            TwAddVarCB(m_graphicsBar, "_Archetype", TW_TYPE_STDSTRING, [](const void *value, void *clientData)
            {
              Game::Instance().lastCreatedArchetype = *static_cast<const std::string *>(value);
            },
              [](void *value, void *clientData)
            {
              // Get: copy the value of s to AntTweakBar
              std::string *destPtr = static_cast<std::string *>(value);
              TwCopyStdStringToLibrary(*destPtr, Game::Instance().lastCreatedArchetype); // the use of TwCopyStdStringToLibrary is required here
            }, 0, " label = Archetype");

            TwAddVarCB(m_graphicsBar, "_CurrObject", TW_TYPE_STDSTRING, [](const void *value, void *clientData)
            {
              Game::Instance().currObject = *static_cast<const std::string *>(value);
            },
              [](void *value, void *clientData)
            {
              // Get: copy the value of s to AntTweakBar
              std::string *destPtr = static_cast<std::string *>(value);
              TwCopyStdStringToLibrary(*destPtr, Game::Instance().currObject); // the use of TwCopyStdStringToLibrary is required here
            }, 0, " label = CurrObject");

            TwAddButton(m_graphicsBar, "SelectObject", [](void *clientData)
            {
              Handlet<Entity> target = Game::Instance().GetEntityByName(Game::Instance().currObject);
              if (target.isValid())
                target->SpawnTweakBar();
            }, 0, NULL);

            TwAddVarCB(m_graphicsBar, "FPS (Render)", TW_TYPE_FLOAT, NULL, [](void *value, void *clientData)
            {
              *reinterpret_cast<float *>(value) = Game::Instance().framesPerSecond_render;
            }, NULL, NULL);
            TwAddVarCB(m_graphicsBar, "FPS (Update)", TW_TYPE_FLOAT, NULL, [](void *value, void *clientData)
            {
              *reinterpret_cast<float *>(value) = Game::Instance().framesPerSecond_update;
            }, NULL, NULL);
            
          }
#endif

          ///Setup Default Properties of Graphics//
          //(Shaders, Constant Buffer, Vertex Buffer, Index Buffer)
          InitHandles();
          
          m_TextManager.Initilize();

          return true;
        }
      }
      else
        std::cout << "GRAPHICS ALREADY EXISTS" << std::endl;

      return true;
    }

    void GFXManager::InitHandles()
    {
      auto game = Game::Instance();
      std::string fileName(game.GetFileLocation().begin(), game.GetFileLocation().end());

      //--------------------------------//
      // Get The Correct Path to Files  //
      //--------------------------------//
#if (defined(_DEBUG) || defined(_RELEASE)) && !defined(_EDITOR)
      fileName.append("\\..\\..\\..\\OmniEngine\\");
#else
#if defined(_EDITOR)
      fileName.clear();
      fileName = Game::GetMyDocumentsDirectory().append("\\Data\\Shaders\\");
#else
      fileName.append("\\Data\\Shaders\\");
#endif
#endif

      fileName.append("BasicVShader.hlsl");
      m_handles.push_back(Handle());
      if (!m_renderer->CreateVertexShader(m_handles[BasicVShader], fileName, InputLayout()))
        assert(0);

      fileName.erase(fileName.find("BasicVShader.hlsl"), fileName.size() - 1);
      fileName.append("BasicPShader.hlsl");
      m_handles.push_back(Handle());
      if (!m_renderer->CreatePixelShader(m_handles[BasicPShader], fileName))
        assert(0);

      //---------------------------------//
      // Create Layout for SpriteVShader //
      //---------------------------------//
      InputLayout iLayout_SVS;
      iLayout_SVS.push_back({ "COLOR", GFX_FORMAT::color4 });
      iLayout_SVS.push_back({ "TRANS", GFX_FORMAT::float2 });
      iLayout_SVS.push_back({ "SCALE", GFX_FORMAT::float2 });
      iLayout_SVS.push_back({ "THETA", GFX_FORMAT::float1 });
      iLayout_SVS.push_back({ "UVONE", GFX_FORMAT::float2 });
      iLayout_SVS.push_back({ "UVTWO", GFX_FORMAT::float2 });
      iLayout_SVS.push_back({ "CINDEX", GFX_FORMAT::uint1 });
      iLayout_SVS.push_back({ "GLOW", GFX_FORMAT::float4 });
      iLayout_SVS.push_back({ "GINTENSITY", GFX_FORMAT::float1 });
      iLayout_SVS.push_back({ "GENABLED", GFX_FORMAT::booltf });

      //--------------------------------//
      // Create SpriteVShader           //
      //--------------------------------//
      fileName.erase(fileName.find("BasicPShader.hlsl"), fileName.size() - 1);
      fileName.append("SpriteVShader.hlsl");
      m_handles.push_back(Handle());
      if (!m_renderer->CreateVertexShader(m_handles[SpriteVShader], fileName, iLayout_SVS))
        assert(0);


      //--------------------------------//
      // Create SpritePShader           //
      //--------------------------------//
      fileName.erase(fileName.find("SpriteVShader.hlsl"), fileName.size() - 1);
      fileName.append("SpritePShader.hlsl");

      m_handles.push_back(Handle());
      if (!m_renderer->CreatePixelShader(m_handles[SpritePShader], fileName))
        assert(0);

      //--------------------------------//
      // Create ParticleAddPShader      //
      //--------------------------------//
      fileName.erase(fileName.find("SpritePShader.hlsl"), fileName.size() - 1);
      fileName.append("ParticleAddPShader.hlsl");

      m_handles.push_back(Handle());
      if (!m_renderer->CreatePixelShader(m_handles[ParticleAddPShader], fileName))
        assert(0);

      //--------------------------------//
      // Create Blur Shaders Layout     //
      //--------------------------------//
      InputLayout iLayout_BVS;
      iLayout_BVS.push_back({"SCREEN", GFX_FORMAT::float1});

      //--------------------------------//
      // Create Blur Shaders Layout     //
      //--------------------------------//
      InputLayout iLayout_BLVS;
      iLayout_BLVS.push_back({ "THRESHOLD", GFX_FORMAT::float1 });

      //--------------------------------//
      // Create Bloom Vertex Shader     //
      //--------------------------------//
      fileName.erase(fileName.find("ParticleAddPShader.hlsl"), fileName.size() - 1);
      fileName.append("BloomVShader.hlsl");

      m_handles.push_back(Handle());
      if (!m_renderer->CreateVertexShader(m_handles[BloomVShader], fileName, iLayout_BLVS))
        assert(0);

      //--------------------------------//
      // Create Bloom Pixel Shader      //
      //--------------------------------//
      fileName.erase(fileName.find("BloomVShader.hlsl"), fileName.size() - 1);
      fileName.append("BloomPShader.hlsl");

      m_handles.push_back(Handle());
      if (!m_renderer->CreatePixelShader(m_handles[BloomPShader], fileName))
        assert(0);

      //--------------------------------//
      // Create Horiz Blur Vertex SHader//
      //--------------------------------//
      fileName.erase(fileName.find("BloomPShader.hlsl"), fileName.size() - 1);
      fileName.append("HorizBlurVShader.hlsl");

      m_handles.push_back(Handle());
      if (!m_renderer->CreateVertexShader(m_handles[HorizBlurVShader], fileName, iLayout_BVS))
        assert(0);

      //--------------------------------//
      // Create Horiz Blur Pixel Shader //
      //--------------------------------//
      fileName.erase(fileName.find("HorizBlurVShader.hlsl"), fileName.size() - 1);
      fileName.append("HorizBlurPShader.hlsl");

      m_handles.push_back(Handle());
      if (!m_renderer->CreatePixelShader(m_handles[HorizBlurPShader], fileName))
        assert(0);

      //--------------------------------//
      // Create Vert Blur Vertex Shader //
      //--------------------------------//
      fileName.erase(fileName.find("HorizBlurPShader.hlsl"), fileName.size() - 1);
      fileName.append("VertBlurVShader.hlsl");

      m_handles.push_back(Handle());
      if (!m_renderer->CreateVertexShader(m_handles[VertBlurVShader], fileName, iLayout_BVS))
        assert(0);

      //--------------------------------//
      // Create Vert Blur Pixel Shader  //
      //--------------------------------//
      fileName.erase(fileName.find("VertBlurVShader.hlsl"), fileName.size() - 1);
      fileName.append("VertBlurPShader.hlsl");

      m_handles.push_back(Handle());
      if (!m_renderer->CreatePixelShader(m_handles[VertBlurPShader], fileName))
        assert(0);

      //--------------------------------//
      // Create VertexBuffer            //
      //--------------------------------//
      m_handles.push_back(Handle());
      if (!m_renderer->CreateVertexBuffer(m_handles[DefaultVBuffer]))
        assert(0);
      ///--------------------------------//
      // Create IndexBuffer              //
      //---------------------------------//
      m_handles.push_back(Handle());
      if (!m_renderer->CreateIndexBuffer(m_handles[DefaultIBuffer]))
        assert(0);
      ///--------------------------------//
      // Create ConstantBuffer           //
      //---------------------------------//
      m_handles.push_back(Handle());
      if (!m_renderer->CreateConstantBuffer(m_handles[DefaultCBuffer], sizeof(D3DXMATRIX) * NUM_OF_CAMERAS))
        assert(0);

      //#if _DEBUG

      //---------------------------------//
      // Create Layout for SpriteVShader //
      //---------------------------------//
      InputLayout iLayout_DVS;
      iLayout_DVS.push_back({ "COLOR", GFX_FORMAT::color4 });
      iLayout_DVS.push_back({ "TRANS", GFX_FORMAT::float2 });
      iLayout_DVS.push_back({ "SCALE", GFX_FORMAT::float2 });
      iLayout_DVS.push_back({ "THETA", GFX_FORMAT::float1 });
      iLayout_DVS.push_back({ "CINDEX", GFX_FORMAT::uint1 });

      //--------------------------------//
      // Create DebugVShader            //
      //--------------------------------//
      fileName.erase(fileName.find("VertBlurPShader.hlsl"), fileName.size() - 1);
      fileName.append("DebugVShader.hlsl");
      m_debugHandles.push_back(Handle());
      if (!m_renderer->CreateVertexShader(m_debugHandles[DebugVShader], fileName, iLayout_DVS))
        assert(0);

      //--------------------------------//
      // Create DebugPShader            //
      //--------------------------------//
      fileName.erase(fileName.find("DebugVShader.hlsl"), fileName.size() - 1);
      fileName.append("DebugPShader.hlsl");

      m_debugHandles.push_back(Handle());
      if (!m_renderer->CreatePixelShader(m_debugHandles[DebugPShader], fileName))
        assert(0);

      //---------------------------------//
      // Create IndexBuffer -Lines       //
      //---------------------------------//
      m_debugHandles.push_back(Handle());
      DWORD indexes[2] = { 4, 5 };
      if (!m_renderer->CreateIndexBuffer(m_debugHandles[DebugIBuffer_Line], indexes, 2))
        assert(0);
      //---------------------------------//
      // Create IndexBuffer -Quads       //
      //---------------------------------//
      m_debugHandles.push_back(Handle());
      DWORD indexes2[8] = { 0, 1, 1, 3, 2, 3, 2, 0 };
      if (!m_renderer->CreateIndexBuffer(m_debugHandles[DebugIBuffer_Quad], indexes2, 8))
        assert(0);
      //---------------------------------//
      // Create IndexBuffer -Triangles   //
      //---------------------------------//
      m_debugHandles.push_back(Handle());
      DWORD indexes3[6] = { 0, 1, 1, 3, 3, 0 };
      if (!m_renderer->CreateIndexBuffer(m_debugHandles[DebugIBuffer_Tri], indexes3, 6))
        assert(0);

      //#endif


      //---------------------------------//
      // Bind Buffers to Start           //
      //---------------------------------//
      m_renderer->BindVertexBuffer(m_handles[DefaultVBuffer], sizeof(Vertex), 0);
      m_renderer->BindIndexBuffer(m_handles[DefaultIBuffer]);
      m_renderer->BindConstantBuffer(0, m_handles[DefaultCBuffer]);

      //---------------------------------//
      // Bind Shaders                    //
      //---------------------------------//
      m_renderer->BindVertexShader(m_handles[SpriteVShader]);
      m_renderer->BindPixelShader(m_handles[SpritePShader]);

      //-----------------------------------//
      // Create Addidtional Render Targets //
      //-----------------------------------//
      m_handles.push_back(Handle());
      //Opaque Target
      if (!m_renderer->CreateRenderTarget(m_handles[O_RTarget]))
        assert(0);
      m_handles.push_back(Handle());
      //Glow Target
      if (!m_renderer->CreateRenderTarget(m_handles[G_RTarget]))
        assert(0);
      m_handles.push_back(Handle());
      //Screen Target
      if (!m_renderer->CreateRenderTarget(m_handles[S_RTarget]))
        assert(0);
      m_handles.push_back(Handle());
      //Effect Target
      if (!m_renderer->CreateRenderTarget(m_handles[E_RTarget]))
        assert(0);
      // Additional Bloom Target
      m_handles.push_back(Handle());
      if (!m_renderer->CreateRenderTarget(m_handles[B_RTarget]))
        assert(0);

      //-----------------------------------//
      // Create Blend Modes Currently Used //
      //-----------------------------------//
      m_handles.push_back(Handle());
      m_renderer->CreateBlendMode(m_handles[B_Alpha], BlendModes::Alpha);

      m_handles.push_back(Handle());
      m_renderer->CreateBlendMode(m_handles[B_Add], BlendModes::Additive);

      m_handles.push_back(Handle());
      BlendModes bModes[8];
      for (unsigned i = 0; i < 8; i++)
        bModes[i] = BlendModes::Alpha;
      bModes[1] = BlendModes::Additive;
      m_renderer->CreateBlendMode(m_handles[B_AlphaAdd], bModes);


      Console::WriteLine("SETUP GRAPHICS DEFAULTS");
    }

    //---------------------//
    // Shutsdown graphics  //
    //---------------------//
    void GFXManager::Shutdown(void)
    {
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
      TwTerminate();
#endif

      for (Handle& handle : m_handles)
        m_renderer->Release(handle);
      //#if _DEBUG
      for (Handle& handle : m_debugHandles)
        m_renderer->Release(handle);
      //#endif

      m_TextManager.Shutdown();

      m_renderer->UnInitialize();

      delete m_renderer;
    }

    //-------------------//
    // Draws everything  //
    //-------------------//
    bool GFXManager::Present(RenderState &renderState, bool loadingScreen)
    {

      //Set Full Screen
      m_renderer->SetFullScreen(m_fullScreen);

      //Add wireframe rasterizer if debug draw is on
      if (m_debugDraw)
        AddRasterizersStates();
      else if (!m_debugDraw && m_wireframe)
        m_wireframe = false;


      //
      // PER VIEWPORT
      //
      //Set background color
      if (loadingScreen)
        m_renderer->SetClearColor(D3DXCOLOR(0, 0, 0, 1));
      else
        m_renderer->SetClearColor(m_clearColor);
      m_renderer->ClearBackBuffer();
      //m_renderer->SetClearColor(D3DXCOLOR(1, 0, 0, 1));
      m_renderer->ClearRenderTarget(m_handles[O_RTarget]);
      //m_renderer->SetClearColor(D3DXCOLOR(0, 1, 0, 1));
      m_renderer->ClearRenderTarget(m_handles[S_RTarget]);
      m_renderer->ClearRenderTarget(m_handles[E_RTarget]);
      m_renderer->SetClearColor(D3DXCOLOR(0, 0, 0, 1));
      m_renderer->ClearRenderTarget(m_handles[G_RTarget]);

      //
      // PER FRAME
      //
      SetupCamera(renderState);
      SetupRasterizers();

      

      //
      //DrawSprites
      //
      if (renderState.objects.size() != 0)
      {
        BeginSpriteDraw();
        DrawSprites(renderState);
        EndSpriteDraw();
      }

      //
      // Full Screen Pass
      //

      //Copy Opaque to Screen Target
      if (m_opaque_rt)
      {
        m_renderer->SetBlendMode(m_handles[B_Alpha]);
        CopyRenderTarget(m_handles[S_RTarget], m_handles[O_RTarget]);
      }

      if (m_glow_rt)
      {

        //Copy Glow to Screen Target
        if (m_glow_Add_rt)
          m_renderer->SetBlendMode(m_handles[B_Add]);
        else
          m_renderer->SetBlendMode(m_handles[B_Alpha]);

        //Copy
        CopyRenderTarget(m_handles[S_RTarget], m_handles[G_RTarget]);
      }

      //Extra whole-screen effects shaders
      ApplyEffectShaders();


      //Copy Screen Target to BackBuffer
      m_renderer->SetBlendMode(m_handles[B_Alpha]);
      CopyRenderTarget(Handle(), m_handles[S_RTarget]);
      
      //
      // Draw Text 
      //
      if (renderState.textObjects.size() != 0)
        DrawSpriteText(renderState);

      //#if _DEBUG
      Game& game = Game::Instance();

      if (m_debugDraw)
        game.GetDebugDraw()->StartDrawDebug(*this, renderState);
      else
        game.GetDebugDraw()->ClearVectors();
      //#endif


      //Draw TweakBar Here
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
      if (game.IsLoaded())
        TwDraw();
#endif

      //
      // Present Back Buffer To Front Buffer
      //
      m_renderer->Present();

      return true;
    }

    //-------------------------------------------//
    // Updates the constant buffer with cameras  //
    //-------------------------------------------//
    void GFXManager::SetupCamera(RenderState& data)
    {
      //  KEY
      //A1 B1 C1 D1
      //A2 B2 C2 D2
      //A3 B3 C3 D3
      //A4 B4 C4 D4

      D3DXMATRIX orthographic;
      D3DXMatrixIdentity(&orthographic);

      float cWidth;
      float cHeight;

      if ((float(m_renderer->GetClientWidth()) / float(m_renderer->GetClientHeight())) >= (16.0 / 9.0))
      {
        cWidth = (2.0f * m_renderer->GetClientWidth() / m_renderer->GetClientHeight());
        cHeight = 2.0f;
      }
      else
      {
        cWidth = 16.0f / 9.0f * 2.0f;
        cHeight = 16.0f / 9.0f * 2.0f * (float(m_renderer->GetClientHeight()) / float(m_renderer->GetClientWidth()));
      }

      ////Create an orthographic matrix
      orthographic.m[0][0] = 2.0f / cWidth;
      orthographic.m[1][1] = 2.0f / cHeight;

      //Camera Objects
      std::vector<D3DXMATRIX> cObjs;

      //Add a default to start
      D3DXMATRIX Default;
      D3DXMatrixIdentity(&Default);
      Default.m[0][0] = 2.0f / (2.0f * 1920.0f / 1080.0f);
      Default.m[1][1] = 1.0f;
      cObjs.push_back(Default);

      //Add all the cameras
      for (Transform camera : data.cameras)
      {
        cObjs.push_back(camera.GetMatrix());
        D3DXMatrixInverse(&cObjs[cObjs.size() - 1], NULL, &cObjs[cObjs.size() - 1]);

        cObjs[cObjs.size() - 1] = cObjs[cObjs.size() - 1] * orthographic;
      }


      while (cObjs.size() != 8)
        cObjs.push_back(Default);

      //Add the cameras to the constant buffer
      m_renderer->UpdateConstantBuffer(m_handles[DefaultCBuffer], &cObjs[0]);

      cObjs.clear();
    }

    //-----------------//
    // Sets rasterizer //
    //-----------------//
    void GFXManager::SetupRasterizers(void)
    {
      //if debug and wireframe
      if (m_debugDraw && m_wireframe)
        m_renderer->BindRasterizerStateObject(m_handles[m_wireframe_index]);
      //if debug but not wireframe
      else if (m_debugDraw)
        m_renderer->BindRasterizerStateObject(m_handles[m_solid_index]);
      //if not debug always be solid
      else if (m_renderer->GetRasterizerStateObjCount() > 0)
        m_renderer->BindRasterizerStateObject(m_handles[m_solid_index]);
    }

    //------------------------------//
    // Sets topology and rasterizer //
    //------------------------------//
    void GFXManager::BeginSpriteDraw(void)
    {
      //Drawing with triangles (fill)
      m_renderer->SetPrimitiveTopology(PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLELIST);

      Handle rTargets[] { m_handles[O_RTarget], m_handles[G_RTarget], m_handles[E_RTarget]};

      m_renderer->BindRenderTargets(3, rTargets);

      m_renderer->BindVertexShader(m_handles[SpriteVShader]);
      m_renderer->BindPixelShader(m_handles[SpritePShader]);
    }

    //-------------------//
    // Batch up sprites  //
    //-------------------//
    void GFXManager::DrawSprites(RenderState &renderState)
    {

      //If there are no objects don't try to draw any
      if (renderState.objects.size() == 0)
        return;

      SpriteBatch batch;

      //Were storing two textures (opaque and glow)
      batch.textures.push_back(Handle());
      batch.textures.push_back(Handle());

      bool glow = renderState.objects.begin()->glow;

      //Get the first texture
      Handle tHandle = renderState.objects.begin()->image->texture;
      batch.textures[0] = tHandle;
      if (renderState.objects.begin()->glow)
        batch.textures[1] = GetGlowTexture(renderState.objects.begin()->image->name);

      batch.glow = glow;
      batch.bloom = renderState.objects.begin()->bloomEnabled;
      batch.blur = renderState.objects.begin()->blurEnabled;

      //Iterate through the sprites
      for (auto batchEnd = renderState.objects.begin(); batchEnd != renderState.objects.end(); batchEnd++)
      {
        if (batchEnd->particleEmitter)
        {
        
          BatchParticleEmitter(*batchEnd);

          if (batchEnd + 1 == renderState.objects.end())
            continue;

          batch.ClearBatch((batchEnd + 1)->image->texture,
            (batchEnd + 1)->glow ? GetGlowTexture((batchEnd + 1)->image->name) : Handle(),
            (batchEnd + 1)->glow,
            (batchEnd + 1)->bloomEnabled,
            (batchEnd + 1)->blurEnabled);

         }
        else
        {
          if (batch.textures[0] == batchEnd->image->texture)
          {
            if (batch.glow == batchEnd->glow)
            {
              if (batch.bloom == batchEnd->bloomEnabled)
              {
                batch.instanceData.push_back(CreateInstance(*batchEnd, batchEnd->glow));

                if (batch.bloom)
                {
                  batch.instanceBloom.SCREEN = FLOAT(batchEnd->bloomThreshold);
                  m_spriteBatches.push_back(batch);

                  if ((batchEnd + 1) != renderState.objects.end())
                  {
                    batch.ClearBatch((batchEnd + 1)->image->texture,
                                     (batchEnd + 1)->glow ? GetGlowTexture((batchEnd + 1)->image->name) : Handle(),
                                     (batchEnd + 1)->glow,
                                     (batchEnd + 1)->bloomEnabled,
                                     (batchEnd + 1)->blurEnabled);
                  }
                }
                else if (batch.blur == batchEnd->blurEnabled)
                {
                  m_spriteBatches.push_back(batch);

                  if ((batchEnd + 1) != renderState.objects.end())
                  {
                    batch.ClearBatch((batchEnd + 1)->image->texture,
                                     (batchEnd + 1)->glow ? GetGlowTexture((batchEnd + 1)->image->name) : Handle(),
                                     (batchEnd + 1)->glow,
                                     (batchEnd + 1)->bloomEnabled,
                                     (batchEnd + 1)->blurEnabled);
                  }
                }
              }
              
            }
          }
          else
          {
            m_spriteBatches.push_back(batch);

            batch.ClearBatch(batchEnd->image->texture,
                             batchEnd->glow ? GetGlowTexture(batchEnd->image->name) : Handle(),
                             batchEnd->glow,
                             batchEnd->bloomEnabled,
                             batchEnd->blurEnabled);

            batchEnd--;

          }

          if ((batchEnd + 1) == renderState.objects.end())
            m_spriteBatches.push_back(batch);
        }
      }

      //Make buffers
      for (unsigned i = 0; i < m_spriteBatches.size(); i++)
      {
        if (m_spriteBatches[i].instanceData.size() != 0)
        {
          m_instanceBuffers.push_back(Handle());
          m_renderer->CreateInstanceBuffer(m_instanceBuffers[m_instanceBuffers.size() - 1], 
                                           &m_spriteBatches[i].instanceData[0], 
                                           sizeof(Instance) * m_spriteBatches[i].instanceData.size());

          if (m_spriteBatches[i].bloom)
            m_renderer->CreateInstanceBuffer(m_spriteBatches[i].instanceBloomData,
                                             &m_spriteBatches[i].instanceBloom,
                                             sizeof(BlurData));
        }
      }

    }

    //-------------------//
    // Draw the batches  //
    //-------------------//
    void GFXManager::EndSpriteDraw(void)
    {
      //Handle bTarget;
      //m_renderer->CreateRenderTarget(bTarget);
      m_renderer->ClearRenderTarget(m_handles[E_RTarget]);
      Handle rTargets[] { m_handles[O_RTarget], m_handles[G_RTarget], m_handles[E_RTarget]};

      for (unsigned i = 0; i < m_instanceBuffers.size(); i++)
      {
        // Bloom, Blur, and Additive Particles need extra render target
        //if (m_spriteBatches[i].bloom || m_spriteBatches[i].blur)
        //{
        //  m_renderer->SetClearColor(D3DXCOLOR(0, 0, 0, 1));
        //  m_renderer->ClearRenderTarget(m_handles[E_RTarget]);
        //}

        //Bind Instance Data
        m_renderer->BindVertexBuffer(m_instanceBuffers[i], sizeof(Instance), 1);

        //Set Blend Mode (for both targets) & Bind Textures
        if (m_spriteBatches[i].glow)
        {
          m_renderer->BindTextures(2, &m_spriteBatches[i].textures[0]);
          m_renderer->SetBlendMode(m_handles[B_Alpha]);
        }
        else
        {
          m_renderer->BindTexture(0, m_spriteBatches[i].textures[0]);
          m_renderer->SetBlendMode(m_handles[B_Alpha]);
        }

        // Handle Additive particles differently
        //if (m_spriteBatches[i].particle)
        //{
        //  // Clear Render Target
        //  m_renderer->SetClearColor(D3DXCOLOR(0, 0, 0, 0));
        //  m_renderer->ClearRenderTarget(m_handles[E_RTarget]);

        //  // Set Blend Mode and Pixel Shader
        //  m_renderer->SetBlendMode(m_handles[B_Add]);
        //  m_renderer->BindPixelShader(m_handles[ParticleAddPShader]);

        //  // Draw Particles
        //  m_renderer->DrawIndexedInstanced(6, m_spriteBatches[i].instanceData.size());

        //  // Set Blend Mode for Copying
        //  m_renderer->SetBlendMode(m_handles[B_Alpha]);

        //  // Copy to the screen
        //  CopyRenderTarget(rTargets[0], rTargets[2]);

        //  // Restore Original Settings
        //  m_renderer->BindVertexShader(m_handles[SpriteVShader]);
        //  m_renderer->BindPixelShader(m_handles[SpritePShader]);
        //  m_renderer->BindTexture(0, m_spriteBatches[i].textures[0]);
        //  m_renderer->BindRenderTargets(3, rTargets);
        //}
        //else //Draw
          m_renderer->DrawIndexedInstanced(6, m_spriteBatches[i].instanceData.size());

		
    //    if (m_spriteBatches[i].bloom)
    //    {
    //      m_renderer->UnBindRenderTargets();

    //      //Bind the texture were pulling from
    //      m_renderer->BindRenderTargetAsTexture(m_handles[E_RTarget]);

    //      //Clear & Bind render Target
    //      m_renderer->ClearRenderTarget(m_handles[B_RTarget]);
    //      m_renderer->BindRenderTarget(m_handles[B_RTarget]);
  
    //      //Bind instance buffer
		  ////m_renderer->BindVertexBuffer(m_spriteBatches[i].instanceBloomData, sizeof(BlurData), 1);
		  //Handle temp;
		  //BlurData t(m_spriteBatches[i].instanceBloom.SCREEN);
		  //m_renderer->CreateInstanceBuffer(temp, &t, sizeof(BlurData));
		  //m_renderer->BindVertexBuffer(temp, sizeof(BlurData), 1);
		  //

    //      //Bind Shaders
    //      m_renderer->BindVertexShader(m_handles[BloomVShader]);
    //      m_renderer->BindPixelShader(m_handles[BloomPShader]);

    //      //Draw
    //      m_renderer->DrawIndexedInstanced(6, 1);
    //      m_renderer->BindTexture(0, m_spriteBatches[i].textures[0]);

    //      m_renderer->SetBlendMode(m_handles[B_Alpha]);
    //      m_renderer->ClearRenderTarget(m_handles[E_RTarget]);

    //      ApplyBlur(m_handles[E_RTarget], m_handles[B_RTarget]);

    //      //Set blend mode for copying
    //      m_renderer->SetBlendMode(m_handles[B_Add]);

    //      //copy Render Target to current opaque/glow target
    //      if (m_spriteBatches[i].glow)
    //        CopyRenderTarget(m_handles[G_RTarget], m_handles[B_RTarget]);
    //      else
    //        CopyRenderTarget(m_handles[O_RTarget], m_handles[B_RTarget]);

		  //m_renderer->Release(temp);

    //      m_renderer->BindTexture(0, m_spriteBatches[i].textures[0]);
    //      m_renderer->BindRenderTargets(3, rTargets);
    //      m_renderer->BindVertexShader(m_handles[SpriteVShader]);
    //      m_renderer->BindPixelShader(m_handles[SpritePShader]);
    //    }
		
      }


      //Release instance buffers
      for (unsigned i = 0; i < m_instanceBuffers.size(); i++)
        m_renderer->Release(m_instanceBuffers[i]);


      m_instanceBuffers.clear();
      for (unsigned i = 0; i < m_spriteBatches.size(); i++)
      {
        if (m_spriteBatches[i].bloom)
          m_renderer->Release(m_spriteBatches[i].instanceBloomData);
        m_spriteBatches[i].instanceData.clear();
		m_spriteBatches[i].textures.clear();
      }
      m_spriteBatches.clear();

      m_renderer->UnBindRenderTargets();
    }

    void GFXManager::BatchParticleEmitter(RenderObject& particles)
    {
      SpriteBatch batch;

      batch.glow = false;
      batch.bloom = false;
      batch.blur = false;

      if (particles.additive)
        batch.particle = true;
             

      batch.textures.push_back(particles.image->texture);

      for (unsigned i = 0; i < particles.translations.size(); i++)
        batch.instanceData.push_back(CreateInstance(particles, i));

      m_spriteBatches.push_back(batch);
    }


    void GFXManager::CopyRenderTarget(Handle& destination, Handle& copy, ShaderPair& shaders)
    {
      m_renderer->UnBindRenderTargets();
      m_renderer->SetPrimitiveTopology(PrimitiveTopology::PRIMITIVE_TOPOLOGY_TRIANGLELIST);
      m_renderer->BindRenderTargetAsTexture(copy);  

      if (destination.isNull())
        m_renderer->BindBackBuffer();
      else
        m_renderer->BindRenderTarget(destination);

      m_renderer->BindVertexShader(m_handles[shaders.vShader]);
      m_renderer->BindPixelShader(m_handles[shaders.pShader]);


      m_renderer->DrawIndexed(6);

      if (!destination.isNull())
       m_renderer->UnBindRenderTargets();
    }

    //----------------------------------//
    // Create and instance of an object //
    //----------------------------------//
    Instance GFXManager::CreateInstance(RenderObject& currentSprite, bool glow, bool glowButNoShow)
    {
      Instance instance;

      instance.Color = currentSprite.color;

      if (glow)
        instance.gColor = currentSprite.glowColor;
      else
        instance.gColor = D3DXCOLOR(0, 0, 0, 1);

      if (currentSprite.brightness <= 0)
        instance.gIntensity = 0.0f;
      else
        instance.gIntensity = currentSprite.brightness;

      //Set Translation, Scale, and Rotation
      instance.X = currentSprite.translation.x;
      instance.Y = currentSprite.translation.y;

      instance.A = currentSprite.scale.x;
      instance.B = currentSprite.scale.y;

      instance.THETA = currentSprite.rotation;

      //No flip
      if (!currentSprite.flipX && !currentSprite.flipY)
      {
        instance.U1 = currentSprite.u1;
        instance.V1 = currentSprite.v1;
        instance.U2 = currentSprite.u2;
        instance.V2 = currentSprite.v2;
      }
      //FlipX
      else if (!currentSprite.flipY)
      {
        instance.U1 = currentSprite.u2;
        instance.V1 = currentSprite.v1;
        instance.U2 = currentSprite.u1;
        instance.V2 = currentSprite.v2;
      }
      //FlipY
      else if (!currentSprite.flipX)
      {
        instance.U1 = currentSprite.u1;
        instance.V1 = currentSprite.v2;
        instance.U2 = currentSprite.u2;
        instance.V2 = currentSprite.v1;
      }
      //FlipX and FlipY
      else
      {
        instance.U1 = currentSprite.u2;
        instance.V1 = currentSprite.v2;
        instance.U2 = currentSprite.u1;
        instance.V2 = currentSprite.v1;
      }

      instance.Camera = currentSprite.space;

      instance.gEnabled = glow;

      return instance;
    }

    Instance GFXManager::CreateInstance(RenderObject& particles, unsigned index)
    {
      Instance instance;

      // Grab individual particles color
      instance.Color = particles.colors[index];

      // No glow
      instance.gEnabled = false;
      instance.gColor = D3DXCOLOR(0, 0, 0, 1);
      instance.gIntensity = 0.0f;

      // Set Translation, Scale, and Rotation
      instance.X = particles.translations[index].x;
      instance.Y = particles.translations[index].y;

      instance.A = particles.scales[index].x;
      instance.B = particles.scales[index].y;

      instance.THETA = particles.rotations[index];

      //No flip
      if (!particles.flipX && !particles.flipY)
      {
        instance.U1 = 0.0f;
        instance.V1 = 0.0f;
        instance.U2 = 1.0f;
        instance.V2 = 1.0f;
      }
      //FlipX
      else if (!particles.flipY)
      {
        instance.U1 = 1.0f;
        instance.V1 = 0.0f;
        instance.U2 = 0.0f;
        instance.V2 = 1.0f;
      }
      //FlipY
      else if (!particles.flipX)
      {
        instance.U1 = 0.0f;
        instance.V1 = 1.0f;
        instance.U2 = 1.0f;
        instance.V2 = 0.0f;
      }
      //FlipX and FlipY
      else
      {
        instance.U1 = 1.0f;
        instance.V1 = 1.0f;
        instance.U2 = 0.0f;
        instance.V2 = 0.0f;
      }
      
      // Space index
      instance.Camera = particles.space;

      return instance;
    }

    //----------------------------------------------- //
    // Builds the name and grabs glow sprite handle   //
    //----------------------------------------------- //
    Handle& GFXManager::GetGlowTexture(std::string& name)
    {
      //Build the name of the glow image
      std::string glowName = name;
      //size_t pos = glowName.find_last_of("_");
      //glowName.insert(pos, "_glow");
      glowName += "_glow";
      //glowName.insert(0, "_");

      //Texture doesn't actually exist
      if (Game::Instance().assets.images.find(glowName.c_str()) == Game::Instance().assets.images.end())
      {
        //Console::WriteLine("Can't find Glow Sprite or there is no glow sprite", Console::Color::Blue);
        return Game::Instance().assets.images[name].texture;
      }

      //auto &opaq = Game::Instance().assets.images[name].texture;
      //auto &handle = Game::Instance().assets.images[glowName].texture;
      //It does exist, so grab it and return it
      return Game::Instance().assets.images[glowName].texture;
    }

    //----------------------------------------------- //
    // Update the buffer size if window size changes  //
    //----------------------------------------------- //
    void GFXManager::UpdateWindowSize(LPARAM lParam)
    {
      m_renderer->Resize(lParam);
    }

    //------------------------------------------------//
    // Adds rasterizer states to render as wireframe  //
    //------------------------------------------------//
    void GFXManager::AddRasterizersStates()
    {
      if (m_renderer->GetRasterizerStateObjCount() > 0)
        return;

      m_handles.push_back(Handle());
      m_solid_index = m_handles.size() - 1;
      m_renderer->CreateRasterizerStateObject(m_handles[m_solid_index]);

      m_handles.push_back(Handle());
      m_wireframe_index = m_handles.size() - 1;
      m_renderer->CreateRasterizerStateObject(m_handles[m_wireframe_index], true);

    }

    //-------------------------------------------------------------------------------------------//
    // Apply final full-screen effects shaders just before copying everything to the backbuffer  //
    //-------------------------------------------------------------------------------------------//
    void GFXManager::ApplyEffectShaders()
    {

      /*if (m_blur_pass)
      {
        ApplyBlur(m_handles[E_RTarget], m_handles[S_RTarget]);
      }

      if (m_bloom_pass)
      {
        m_renderer->UnBindRenderTargets();

        Handle tempInst, rTarget;
        BlurData threshold = { m_bloom_threshold };

        m_renderer->ClearRenderTarget(m_handles[E_RTarget]);
        m_renderer->BindRenderTarget(m_handles[E_RTarget]);
        m_renderer->BindRenderTargetAsTexture(m_handles[S_RTarget]);

        m_renderer->CreateInstanceBuffer(tempInst, &threshold, sizeof(BlurData));
        m_renderer->BindVertexBuffer(tempInst, sizeof(BlurData), 1);
        m_renderer->BindVertexShader(m_handles[BloomVShader]);
        m_renderer->BindPixelShader(m_handles[BloomPShader]);
        m_renderer->DrawIndexedInstanced(6, 1);

        m_renderer->Release(tempInst);

        m_renderer->CreateRenderTarget(rTarget);
        m_renderer->ClearRenderTarget(rTarget);
        m_renderer->BindRenderTarget(rTarget);
        
        ApplyBlur(rTarget, m_handles[E_RTarget]);

        m_renderer->Release(rTarget);

        m_renderer->SetBlendMode(m_handles[B_Add]);
        CopyRenderTarget(m_handles[S_RTarget], m_handles[E_RTarget]);

      }*/
    }
     
    //
    // Apply a Blur Effect ot a RenderTarget
    //
    void GFXManager::ApplyBlur(Handle effect, Handle original)
    {
      //
      // BlendMode
      //
      m_renderer->SetBlendMode(m_handles[B_Alpha]);

      //
      // Extra Render Target to use
      //
      //m_renderer->SetClearColor(D3DXCOLOR(0, 0, 0, 1));
      m_renderer->ClearRenderTarget(effect);

      //
      // Set the Blur data for 1st and 2nd Pass
      //
      BlurData width = { float(m_renderer->GetClientWidth()) },
               height = { float(m_renderer->GetClientHeight()) };

      //
      // For instance buffer
      //
      Handle local;

      ////////////////
      // Horizontal //
      ////////////////

      //
      // Ensure none of our resources are bound
      //
      m_renderer->UnBindRenderTargets();
      m_renderer->UnBindTexture(0);
      m_renderer->UnBindTexture(1);

      //
      // Bind resources we need
      //
      m_renderer->BindRenderTarget(effect);
      m_renderer->BindRenderTargetAsTexture(original);

      //
      // Create/Bind Vertex Data
      //
      m_renderer->CreateInstanceBuffer(local, &width, sizeof(BlurData));
      m_renderer->BindVertexBuffer(local, sizeof(BlurData), 1);
      
      //
      // Bind the correct Shaders
      //
      m_renderer->BindVertexShader(m_handles[HorizBlurVShader]);
      m_renderer->BindPixelShader(m_handles[HorizBlurPShader]);

      //
      // Draw (Pass 1)
      //
      m_renderer->DrawIndexedInstanced(6, 1);

      //
      // Release Instance Buffer
      //
      m_renderer->Release(local);

      ////////////////
      // Vertical   //
      ////////////////

      //
      // Ensure none of our resources are bound
      //
      m_renderer->UnBindRenderTargets();
      m_renderer->UnBindTexture(0);
      m_renderer->UnBindTexture(1);

      //
      // Bind resources we need
      //
      m_renderer->BindRenderTargetAsTexture(effect);
      m_renderer->BindRenderTarget(original);

      //
      // Create/Bind Vertex Data
      //
      m_renderer->CreateInstanceBuffer(local, &height, sizeof(BlurData));
      m_renderer->BindVertexBuffer(local, sizeof(BlurData), 1);

      //
      // Bind the correct Shaders
      //
      m_renderer->BindVertexShader(m_handles[VertBlurVShader]);
      m_renderer->BindPixelShader(m_handles[VertBlurPShader]);

      //
      // Draw (Pass 1)
      //
      m_renderer->DrawIndexedInstanced(6, 1);

      //
      // Release Instance Buffer
      //
      m_renderer->Release(local);

      m_renderer->SetClearColor(m_clearColor);
    }


    //-------------------------------------------------------------------------------//
    // Draw all the text objects (they will be drawn on top because ... just beacuse //
    //-------------------------------------------------------------------------------//
    void GFXManager::DrawSpriteText(RenderState& renderState)
    {
      //Setup Orthographic Matrix
      Vec2 windowSize = ((float)m_renderer->GetClientWidth(), (float)m_renderer->GetClientHeight());
      float cWidth  = 2 * windowSize.x / windowSize.y;
      float cHeight = 2;
      Matrix3 orthographic(2 / cWidth, 2 / cHeight);
      
      //Create all the camera matrices
      std::vector<Matrix3> cObjs;

      //Add a default to start
      Matrix3 Default;
      Default.m[0][0] = 2.0f / (2.0f * 1920.0f / 1080.0f);
      Default.m[1][1] = 1.0f;
      cObjs.push_back(Default);

      for (auto& camera : renderState.cameras)
        cObjs.push_back(camera.GetMatrix3().Inverse());

      //Loop through all text objects
      for (auto& tObj : renderState.textObjects)
      {
        //To NDC Space
        if (!tObj.space_default)
        {
          tObj.translation = orthographic * cObjs[tObj.space + 1] * tObj.translation;
          //To Screen Space
          tObj.translation.x = (tObj.translation.x + 1.0f) / 2.0f;
          tObj.translation.y = -(tObj.translation.y - 1.0f) / 2.0f;
          tObj.translation.x *= windowSize.x;
          tObj.translation.y *= windowSize.y;
        }
        //Because we need a wide string
        std::wstring wText(tObj.text.begin(), tObj.text.end());

        //Check if we already have this font loaded
        m_TextManager.CreateFontWrapper(m_renderer->GetDevice(), tObj.font);
        auto fontWrapper = m_TextManager.m_fontWrappersRes.find(tObj.font);

        //Draw text
        fontWrapper->second->DrawString(m_renderer->GetDeviceContext(),  // Device Context
                                        wText.c_str(),                   // Text to write
                                        tObj.size,                       // Font Size 
                                        tObj.translation.x,              // X Position
                                        tObj.translation.y,              // Y Position
                                        tObj.color,                      // Text Color
                                        FW1_RESTORESTATE);               // Flags

      }


    }

    //---------------------------------//
    // Create the Font Wrapper Factory // 
    //---------------------------------//
    void GFXManager::Text::Initilize(void)
    {
      HRESULT result = FW1CreateFactory(FW1_VERSION, &m_fontFactory);
      if (result != S_OK)
        assert(0);

    }

    //-----------------------------------------//
    // Create Font Wrapper for a Specific Font //
    //-----------------------------------------//
    void GFXManager::Text::CreateFontWrapper(ID3D11Device * device, std::string fontName)
    {
      //Already exists
      if (m_fontWrappersRes.find(fontName) != m_fontWrappersRes.end())
        return;

      IFW1FontWrapper *fontWrapper;

      std::wstring name(fontName.begin(), fontName.end());

      HRESULT result = m_fontFactory->CreateFontWrapper(device, name.c_str(), &fontWrapper);
      if (result != S_OK)
        assert(0);

      m_fontWrappersRes.insert({ fontName, fontWrapper });

    }

    //------------------------------------------//
    // Release all the Wrappers and the Factory //
    //------------------------------------------//
    void GFXManager::Text::Shutdown(void)
    {
      for (auto& wrapper : m_fontWrappersRes)
        wrapper.second->Release();
      
      m_fontFactory->Release();
    }

  } //Graphics
} //Omni