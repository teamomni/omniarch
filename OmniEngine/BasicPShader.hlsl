/******************************************************************************
Filename: BasicPShader.hlsl

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//
//Basic Pixel Shader for Copying Render Targets
//

//-----------------------------------//
// Textures Should Be Binded on CPU  //
//-----------------------------------//
Texture2D Texture0 : register(t0);


//-----------------------------------//
// Sample State Set on the CPU       //
//-----------------------------------//
SamplerState sampleState;

//-----------------------------------//
// Passed info from vertex shader    //
//-----------------------------------//
struct vOutput
{
  float4 position  : SV_POSITION;
  float2 texcoord  : TEXCOORD;
};

//-----------------------------------//
// Main Function of Pixel Shader     //
//-----------------------------------//
float4 PShader(vOutput input) : SV_Target0
{
  //For regular sprite render target
  return Texture0.Sample(sampleState, input.texcoord);
}

