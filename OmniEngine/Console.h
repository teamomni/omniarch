/******************************************************************************
Filename: Console.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once
#include <string>
#include <windows.h>
#include <iosfwd>

namespace Omni
{

  class Console
  {
  public:

    enum struct Color
    {
      Black = 0,
      DarkBlue = 1,
      DarkGreen = 2,
      DarkCyan = 3,
      DarkRed = 4,
      DarkMagenta = 5,
      DarkYellow = 6,
      DarkWhite = 7,
      Gray = 8,
      Blue = 9,
      Green = 10,
      Cyan = 11,
      Red = 12,
      Magenta = 13,
      Yellow = 14,
      White = 15
    };
    static bool s_enabled;

  private:
    static HANDLE s_conOut;
    static const Color info_heading_text = Color::Cyan;
    static const Color info_heading_back = Color::DarkCyan;
    static const Color info_body_text = Color::DarkCyan;
    static const Color info_body_back = Color::Black;
    static const Color warning_heading_text = Color::Yellow;
    static const Color warning_heading_back = Color::DarkYellow;
    static const Color warning_body_text = Color::DarkYellow;
    static const Color warning_body_back = Color::Black;
    static const Color error_heading_text = Color::White;
    static const Color error_heading_back = Color::DarkRed;
    static const Color error_body_text = Color::DarkRed;
    static const Color error_body_back = Color::Black;

  public:
    static inline void Init(bool enabled)
    {
      if (!enabled)
      {
        s_enabled = false;
       
        return;
      }
      s_enabled = true;

//#if defined(_EDITOR) || defined(_DEBUG) || defined(_RELEASE)
      AllocConsole();
      freopen("conin$", "r", stdin);
      freopen("conout$", "w", stdout);
      freopen("conout$", "w", stderr);
      s_conOut = GetStdHandle(STD_OUTPUT_HANDLE);
//#endif
    }

    static inline void SetColors(Color textColor = Color::DarkWhite, Color backColor = Color::Black)
    {
      if (!s_enabled)
        return;
      SetConsoleTextAttribute(s_conOut, ((unsigned int)backColor << 4) | (unsigned int)textColor);
    }

    static inline void Write(std::string text, Color textColor = Color::DarkWhite, Color backColor = Color::Black)
    {
      if (!s_enabled)
        return;
      SetColors(textColor, backColor);
      std::cout << text;
      SetColors();
    }
    static inline void WriteLine(std::string text, Color textColor = Color::DarkWhite, Color backColor = Color::Black)
    {
      if (!s_enabled)
        return;
      SetColors(textColor, backColor);
      std::cout << text << std::endl;
      SetColors();
    }

    static inline int GetBufferWidth()
    {
      if (!s_enabled)
        return 0;
      CONSOLE_SCREEN_BUFFER_INFO bufferInfo;
      int bufferWidth, result;

      result = GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &bufferInfo);

      if (result)
      {
        bufferWidth = bufferInfo.dwSize.X;
      }

      return bufferWidth;
    }

    static inline void WrapText(std::string &body)
    {
      if (!s_enabled)
        return;
      int bufferWidth = GetBufferWidth();
      if (bufferWidth == 0)
        return;
      body.insert(0, 4, ' ');
      for (unsigned int i = 1; i <= body.length(); i++)
      {
        char c = body[i - 1];

        int spaceCount = 4;

        // Add whitespace if newline detected.
        if (c == '\n')
        {
          int charNumOnLine = ((i) % bufferWidth);
          spaceCount = bufferWidth - charNumOnLine;
          body.insert((i - 1), (spaceCount), ' ');
          i += (spaceCount);
          continue;
        }

        if ((i % bufferWidth) == 0)
        {
          if (c != ' ')
          {
            for (int j = (i - 1); j > -1; j--)
            {
              if (body[j] == ' ')
              {
                body.insert(j, spaceCount, ' ');
                break;
              }
              else spaceCount++;
            }
          }
        }
      }
    }

    static inline void Info(std::string heading, std::string body = "")
    {
      if (!s_enabled)
        return;
      if (heading != "")
      {
        std::cout << " ";
        heading.insert(0, " ");
        heading.append(" ");
        WriteLine(heading, info_heading_text, info_heading_back);
      }
      if (body == "")
        return;
      WrapText(body);
      SetColors(info_body_text, info_body_back);
      std::cout << body;
      SetColors();
      std::cout << std::endl;
    }
    static inline void Warning(std::string heading, std::string body = "")
    {
      if (!s_enabled)
        return;
      if (heading != "")
      {
        std::cout << " ";
        heading.insert(0, " ");
        heading.append(" ");
        WriteLine(heading, warning_heading_text, warning_heading_back);
      }
      if (body == "")
        return;
      WrapText(body);
      SetColors(warning_body_text, warning_body_back);
      std::cout << body;
      SetColors();
      std::cout << std::endl;
    }
    static inline void Error(std::string heading, std::string body = "")
    {
      if (!s_enabled)
        return;
      if (heading != "")
      {
        std::cout << " ";
        heading.insert(0, " ");
        heading.append(" ");
        WriteLine(heading, error_heading_text, error_heading_back);
      }
      if (body == "")
        return;
      WrapText(body);
      SetColors(error_body_text, error_body_back);
      std::cout << body;
      SetColors();
      std::cout << std::endl;
    }
  };

  template<class elem, class traits>
  inline std::basic_ostream<elem, traits>& operator<<(std::basic_ostream<elem, traits>& os, Console::Color col)
  {
    if (!Console::s_enabled)
      return;
    os.flush();
    Console::SetColors(col);
    return os;
  }

  template<class elem, class traits>
  inline std::basic_istream<elem, traits>& operator>>(std::basic_istream<elem, traits>& is, Console::Color col)
  {
    if (!Console::s_enabled)
      return;
    std::basic_ostream<elem, traits>* p = is.tie();
    if (p != NULL)
      p->flush();
    Console::SetColors(col);
    return is;
  }
}
