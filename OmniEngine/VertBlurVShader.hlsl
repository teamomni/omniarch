/******************************************************************************
Filename: VertBlurVShader.hlsl

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// Purpose: Horizontal Blur Vertex Shader
// Date   : Spring 2014
// Type   : Vertex Shader

//Full screen pass

//-----------------------------------//
// Struct Passed from CPU            //
//-----------------------------------//
struct vInput
{
  float3 position : POSITION; //
  float2 texcoord : TEXCOORD; //
  float  sHeight  : SCREEN;   // Screen Height
};

//-----------------------------------//
// Pass info onto pixel shader       //
//-----------------------------------//
struct vOutput
{
  float4 position : SV_POSITION;
  float2 texcoord : TEXCOORD;

  float2 texCoord1 : TEXCOORD1;
  float2 texCoord2 : TEXCOORD2;
  float2 texCoord3 : TEXCOORD3;
  float2 texCoord4 : TEXCOORD4;
  float2 texCoord5 : TEXCOORD5;
  float2 texCoord6 : TEXCOORD6;
  float2 texCoord7 : TEXCOORD7;
  float2 texCoord8 : TEXCOORD8;
  float2 texCoord9 : TEXCOORD9;

};

//-----------------------------------//
// Main Function of Vertex Shader     //
//-----------------------------------//
vOutput VShader(vInput input)
{
  vOutput output;

  output.position = float4(input.position.xy * 2, 0, 1);

  //Original texture coordinate
  output.texcoord = input.texcoord;

  float blurSize = 1.0f / input.sHeight;

  //Grabbing the coordinates of 4 pixels to the left and right of original pixel
  output.texCoord1 = input.texcoord + float2(0.0f, blurSize * -4.0f);
  output.texCoord2 = input.texcoord + float2(0.0f, blurSize * -3.0f);
  output.texCoord3 = input.texcoord + float2(0.0f, blurSize * -2.0f);
  output.texCoord4 = input.texcoord + float2(0.0f, blurSize * -1.0f);
  output.texCoord5 = input.texcoord + float2(0.0f, blurSize *  0.0f);
  output.texCoord6 = input.texcoord + float2(0.0f, blurSize *  1.0f);
  output.texCoord7 = input.texcoord + float2(0.0f, blurSize *  2.0f);
  output.texCoord8 = input.texcoord + float2(0.0f, blurSize *  3.0f);
  output.texCoord9 = input.texcoord + float2(0.0f, blurSize *  4.0f);

  return output;
}