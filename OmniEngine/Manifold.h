/******************************************************************************
Filename: Manifold.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "Vec2.h"
#include "Face.h"
#include "Handlet.h"

namespace Omni
{
  class RigidBody;

  class Manifold {
  public:
    enum class Type
    {
      PolyToPoly,
      PolyToCircle,
      CircleToPoly,
      CircleToCircle
    };

    Manifold();

    void ApplyCollisionImpulse(float dt);
    void PositionalCorrection();

    bool collision;
    char contactCount;
    float collisionDepth;
    Vec2 collisionNormal;
    Vec2 contacts[2];
    bool flip;
    Handlet<RigidBody>bodyA;
    Handlet<RigidBody>bodyB;
    std::string nameB;
    std::string nameA;

    // accumulated impulse. this is used to achieve more accurate results
    float accumulatedImpulseMagnitude[2];

  };
}
