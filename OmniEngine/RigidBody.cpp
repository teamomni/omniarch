/******************************************************************************
Filename: RigidBody.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "RigidBody.h"
#include "Shape.h"
#include "Game.h"
#include <vector>
#include "Manifold.h"

namespace Omni
{
  RigidBody::CollisionState RigidBody::collisionTable[16][16] = { CollisionState(0) };
  unsigned RigidBody::loopsPerFrame = 0;
  float RigidBody::accumulator = 0;
  float RigidBody::gravity = 2.5f;
  std::vector<Manifold> RigidBody::manifolds;
  unsigned RigidBody::physicsIterations = 3;

  // default constructor
  RigidBody::RigidBody() : fixed(false), rotationLocked(false), ghost(false),
    totalImpulse(), totalForce(), velocity(), torque(0), angularVelocity(0), gravityScale(1), shape(nullptr),
    material(), onGround(false), p_BodyRadius(0), group(Group::Default), onScreen(true)
  {}

  // constructor that sets shape and material
  RigidBody::RigidBody(const Shape *shape_, const Material &material_) : fixed(false),
    rotationLocked(false), ghost(false), totalImpulse(), totalForce(), velocity(), angularVelocity(0),
    gravityScale(1), onGround(false), torque(0), p_BodyRadius(0), group(Group::Default), onScreen(true)
  {
    // copy the material data over
    material = material_;

    // get a new copy of the shape
    shape = shape_->Clone();

    // make the shape point at this rigid body
    shape->body = this;

    // initialize the shape. this will also compute mass data
    shape->Initialize();
  }

  RigidBody::~RigidBody()
  {
    ScriptableDestroy();
  }

  void RigidBody::Register()
  {
    RegisterName("RigidBody", &Game::Instance().systemContainers, PropertyDefinitionMap({
      { "angularVelocity", PropertyDefinition::Float(   0.0f,   offsetof(RigidBody, angularVelocity), true) },
      { "velocity",        PropertyDefinition::Vec2(    Vec2(), offsetof(RigidBody, velocity),        true) },
      { "static",          PropertyDefinition::Bool(    false,  offsetof(RigidBody, fixed),           true) },
      { "rotationLocked",  PropertyDefinition::Bool(    false,  offsetof(RigidBody, rotationLocked),  true) },
      { "ghost",           PropertyDefinition::Bool(    false,  offsetof(RigidBody, ghost),           true) },
      { "gravityScale",    PropertyDefinition::Float(   1.0f,   offsetof(RigidBody, gravityScale),    true) },
      { "material",        PropertyDefinition::String(  "",     offsetof(RigidBody, p_Material),      true) },
      { "bodyRadius",      PropertyDefinition::Float(   0,      offsetof(RigidBody, p_BodyRadius),    true) },
      { "bodyPoints",      PropertyDefinition::Vec2List(        offsetof(RigidBody, p_BodyPoints),    true) },
      { "collisionGroup",  PropertyDefinition::Int(     0,      offsetof(RigidBody, group),           true) }
    }), false);
  }

  void RigidBody::Set(const Shape *shape_, const Material &material_)
  {

    std::string nameA = GetOwner()->name;


    // clear the old shape, if there is one
    if (shape)
      delete shape;

    // copy the material data over
    material = material_;

    // get a new copy of the shape
    shape = shape_->Clone();

    // make the shape point at this rigid body
    shape->body = this;

    // initialize the shape. this will also compute mass data
    shape->Initialize();
  }

  void RigidBody::Set(const Shape *shape_)
  {
    try
    {
      // clear the old shape if there is one
      if (shape)
        delete shape;

      // get a new copy of the shape
      shape = shape_->Clone();

      // make the shape point at this rigid body
      shape->body = this;

      // initialize the shape. this will also compute mass data
      shape->Initialize();
    }

    // catches exception if no material
    catch (std::exception e)
    {
      // TODO
    }
  }

  void RigidBody::Set(const Material &material_)
  {
    try
    {
      // copy the material data over
      material = material_;

      // initialize the shape. this will also compute mass data
      if (shape)
      {
        shape->Initialize();
      }
      // if no shape, throw exception
      else
      {
        throw std::exception();
      }
    }

    // catches exception if no shape
    catch (std::exception e)
    {
      // TODO
    }
  }

  // gettor/settor for gravity scale
  float RigidBody::GetGravityScale() const
  {
    return gravityScale;
  }

  void RigidBody::SetGravityScale(float newScale)
  {
    gravityScale = newScale;
  }

  // gettor for shape
  Shape *RigidBody::GetShape() const
  {
    return shape;
  }

  // set switches
  void RigidBody::SetGhost(bool ghost_)
  {
    ghost = ghost_;
  }

  void RigidBody::SetFixed(bool fixed_)
  {
    fixed = fixed_;
  }

  void RigidBody::SetRotationLocked(bool locked)
  {
    rotationLocked = locked;
  }

  // apply linear impulse/force
  void RigidBody::ApplyLinearImpulse(Vec2 impulse)
  {
    totalImpulse += impulse;
  }
  void RigidBody::ApplyLinearImpulse(float x, float y)
  {
    Vec2 impulse(x, y);
    totalImpulse += impulse;
  }
  void RigidBody::ApplyLinearForce(Vec2 force)
  {
    totalForce += force;
  }
  void RigidBody::ApplyLinearForce(float x, float y)
  {
    Vec2 force(x, y);
    totalForce += force;
  }

  // toggle switches
  void RigidBody::ToggleGhost()
  {
    ghost = ghost ? false : true;
  }
  void RigidBody::ToggleFixed()
  {
    fixed = fixed ? false : true;
  }
  void RigidBody::ToggleRotationLocked()
  {
    rotationLocked = rotationLocked ? false : true;
  }

  RigidBody::CollisionState RigidBody::GetCollisionTableState(Group group1, Group group2)
  {
    return collisionTable[int(group1)][int(group2)];
  }

  void RigidBody::SetCollisionTableState(Group group1, Group group2, CollisionState state)
  {
    collisionTable[int(group1)][int(group2)] = collisionTable[int(group2)][int(group1)] = state;
  }

  // collision mask functions
  void RigidBody::SetGroup(Group group_)
  {
    group = group_;
  }

  RigidBody::Group RigidBody::GetGroup() const
  {
    return group;
  }

  void RigidBody::DispatchCollisionEvents()
  {
    // get self pointer and use that instead of this pointer
    Handlet<RigidBody> self = reinterpret_cast<RigidBody **>(this->selfPtr);

    // clear collisionEnded
    collisionEnded.clear();

    // make sure current frame collisions only has unique hit events
    auto collisionCopy = currentFrameCollisions;
    auto newEnd = std::unique(currentFrameCollisions.begin(), currentFrameCollisions.end());

    // remove stuff if you need to
    if (newEnd != currentFrameCollisions.end())
      currentFrameCollisions.erase(newEnd, currentFrameCollisions.end());

    int firstcount = 0;

    // loop through all previous frame collisions
    for (auto i = collisionPersisted.begin(); i != collisionPersisted.end(); ++i)
    {
      Events::CollisionEvent currentEvent(*i);

      // if not found in current frame's collisions, move to collision ended
      auto objectPositionInCurrent = std::find(currentFrameCollisions.begin(), currentFrameCollisions.end(), currentEvent);
      if (objectPositionInCurrent == currentFrameCollisions.end())
      {
        firstcount++;
        eventsToClear.push_back(currentEvent);

        collisionEnded.push_back(currentEvent);
      }

      // if yes found, remove from current frame collisions
      else
      {
        currentFrameCollisions.erase(objectPositionInCurrent);
      }
    }

    int count = 0;
    auto persistedCopy = collisionPersisted;

    // loop through iterators to clear and clear them from collision persisted
    for (auto i = eventsToClear.begin(); i != eventsToClear.end(); ++i)
    {
      ++count;
      collisionPersisted.erase(std::find(collisionPersisted.begin(), collisionPersisted.end(), *i));
    }

    eventsToClear.clear();

    // loop through all collision started
    for (auto i = collisionStarted.begin(); i != collisionStarted.end(); ++i)
    {
      Events::CollisionEvent currentEvent(*i);

      // if not found in current frame's collisions, move to collision ended
      auto objectPositionInCurrent = std::find(currentFrameCollisions.begin(), currentFrameCollisions.end(), currentEvent);
      if (objectPositionInCurrent == currentFrameCollisions.end())
      {
        collisionEnded.push_back(currentEvent);
      }

      // if yes found, push to collision persist and remove from current frame collisions
      else
      {
        collisionPersisted.push_back(currentEvent);
        currentFrameCollisions.erase(objectPositionInCurrent);
      }
    }

    collisionStarted.clear();

    // the only collisions left should be collision started
    unsigned numStarted = currentFrameCollisions.size();
    for (unsigned i = 0; i < numStarted; ++i)
    {
      collisionStarted.push_back(currentFrameCollisions[i]);
    }

    currentFrameCollisions.clear();

    // now that all vectors have been updated, send event to the entity
    Handlet<Entity> dispatchHolder = GetOwner();

    auto &game = Game::Instance();

    v8::Locker locker(game.isolate);
    v8::Isolate::Scope isolate_scope(game.isolate);
    v8::HandleScope handleScope(game.isolate);

    v8::Context::Scope contextScope(v8::Local<v8::Context>::New(game.isolate, game.context));

    // collision started
    size_t vecSize = collisionStarted.size();
    for (size_t i = 0; i < vecSize; ++i)
    {
      LocalObject obj = self->collisionStarted[i].v8_GetObject();
      obj->SetInternalField(0, v8::External::New(game.isolate, &self->collisionStarted[i]));
      dispatchHolder->dispatcher->DispatchEvent("onCollisionStart", obj);

      // if the self object has been deleted, stop executing this function
      if (!self.isValid())
        return;
    }

    // collision persisted
    vecSize = self->collisionPersisted.size();
    for (size_t i = 0; i < vecSize; ++i)
    {
      LocalObject obj = self->collisionPersisted[i].v8_GetObject();
      obj->SetInternalField(0, v8::External::New(game.isolate, &self->collisionPersisted[i]));
      dispatchHolder->dispatcher->DispatchEvent("onCollisionPersist", obj);

      // if the self object has been deleted, stop executing this function
      if (!self.isValid())
        return;
    }

    // collision ended
    vecSize = self->collisionEnded.size();
    for (size_t i = 0; i < vecSize; ++i)
    {
      LocalObject obj = self->collisionEnded[i].v8_GetObject();
      obj->SetInternalField(0, v8::External::New(game.isolate, &self->collisionEnded[i]));
      dispatchHolder->dispatcher->DispatchEvent("onCollisionEnd", obj);

      // if the self object has been deleted, stop executing this function
      if (!self.isValid())
        return;
    }
  }

  // integrates linear and angular velocities
  void RigidBody::Update(float dt)
  {
    // scale dt by timescale
    dt *= Game::Instance().timescale;

    // get the self ptr
    Handlet<Entity> selfOwner = owner;

    // if owner is not on screen, do not update
    if (!onScreen)
      return;

    if (owner->space->paused)
    {
      dt = physicsTimestep;
      return;
    }

    // skip if destroying
    if (this->active == false)
      return;

    Transform &transform = owner->transform;

    // store prev frame stuff
    transform.UpdateLastTransformData(); 

    // update all the collision stuff
    DispatchCollisionEvents();

    if (this->fixed)
      return;


    // if owner is invalid at this point, means my self is inactive
    if (!selfOwner.isValid())
      return;

    // HACKY CODE FOR ONGROUND
    onGround = false;

    this->velocity.y -= gravity * gravityScale * dt;

    // add forces to impulse
    Vec2 forcesImpulse = totalForce * dt;
    totalImpulse += forcesImpulse;

    // add linear velocity
    velocity += totalImpulse;
    transform.translation += velocity * dt;

    // add torque
    angularVelocity += torque;

    // add rotational velocity
    transform.rotation += angularVelocity * dt;

    // this->totalForce.y = 0;
    // this->totalForce.x = 0;
    totalImpulse.x = 0;
    totalImpulse.y = 0;
    torque = 0;

//#if _DEBUG
    // if it is a polygon, draw it out
    if (GetShape()->GetType() == Shape::Type::Polygon)
    {
      auto &game = Game::Instance();

      auto transform = this->GetOwner()->transform.GetMatrix3();
      Polygon *poly = reinterpret_cast<Polygon *>(GetShape());

      Vec2 tempVertices[MaxVertexCount];

      Vec2 start(0, 0);
      Vec2 end(1, 1);

      // copy over the vertices and multiply by transform
      for (unsigned i = 0; i < poly->vertexCount; ++i)
      {
        tempVertices[i] = transform * poly->vertices[i];
      }

      unsigned vertexCount = poly->vertexCount;

      // draw the lines
      for (unsigned i = 0; i < vertexCount; ++i)
      {
        game.GetDebugDraw()->DrawPoint(tempVertices[i], this->owner->space->index, D3DXCOLOR(1, 1, 1, 1));
        game.GetDebugDraw()->DrawLine(tempVertices[i], tempVertices[((i + 1) % vertexCount)], this->owner->space->index, D3DXCOLOR(0, 0, 0, 1));
      }
    }

    // if it is a circle, draw the points around it
    else
    {
      auto &game = Game::Instance();

      Circle *circle = reinterpret_cast<Circle *>(GetShape());

      float radius = owner->transform.scale.x * circle->radius;
      game.GetDebugDraw()->DrawPoint(owner->transform.translation + Vec2(radius, 0), this->owner->space->index, D3DXCOLOR(1, 1, 1, 1));
      game.GetDebugDraw()->DrawPoint(owner->transform.translation + Vec2(-radius, 0), this->owner->space->index, D3DXCOLOR(1, 1, 1, 1));
      game.GetDebugDraw()->DrawPoint(owner->transform.translation + Vec2(0, radius), this->owner->space->index, D3DXCOLOR(1, 1, 1, 1));
      game.GetDebugDraw()->DrawPoint(owner->transform.translation + Vec2(0, -radius), this->owner->space->index, D3DXCOLOR(1, 1, 1, 1));

    }
//#endif

  }

  // properties initialize
  void RigidBody::Initialize()
  {
    auto &game = Game::Instance();

    // pop up message box
    // MessageBoxA(0, "Hello World!", "title", MB_OK | MB_ICONINFORMATION);

    // get the material first
    material = game.assets.materials[p_Material];

    // if it is a circle
    if (p_BodyRadius)
    {
      shape = new Circle(p_BodyRadius);
    }

    // if it is a polygon
    else
    {
      shape = new Polygon(p_BodyPoints);
    }

    // shape needs material to compute mass
    shape->body = this;
    shape->Initialize();

  }

  void RigidBody::Destroy()
  {
    delete shape;
    ScriptableDestroy();
    Game::Instance().DiscardModulePropertyHandles(Handlet<Component>(selfPtr));
  }

  void RigidBody::ApplyImpulseAtPoint(Vec2 impulse, Vec2 relativePoint)
  {
    Vec2 impulseDirection = relativePoint.Normalized();
    Vec2 contactVector = this->owner->transform.GetMatrix3() * relativePoint;
    float inverseIntertia = rotationLocked ? 0 : massData.inverseInertia;
    float rotationalDivisors = inverseIntertia * (contactVector.Cross(impulseDirection)) * (contactVector.Cross(impulseDirection));

    impulse /= (massData.inverseMass + rotationalDivisors);

    totalImpulse += impulse * massData.inverseMass;
    angularVelocity += inverseIntertia * contactVector.Cross(impulse);
  }

  void RigidBody::EndFrame()
  {
    // get the vector of rigid bodies
    std::vector<RigidBody> &RigidBodies = GetInstances();

    // get the end of the vector
    unsigned vectorSize = RigidBodies.size();

    // loop through the vector of rigid bodies
    for (unsigned j = 0; j < vectorSize; ++j)
    {
      // skip if destroying
      // if (RigidBodies[j].active == false)
      //   return;

      // RigidBody *bodyA = &RigidBodies[j];
      //if (bodyA->owner->name == "BombDrone")
      //{
      //  printf("asDASDASDASDASD");
      //}

      RigidBodies[j].totalForce = Vec2(0, 0);
    }

    // also clear the vector of manifolds.
    manifolds.clear();
  }

  void RigidBody::Step(float dt)
  {
    // scale dt by timescale
    dt *= Game::Instance().timescale;

    // get the vector of rigid bodies
    std::vector<RigidBody> &RigidBodies = GetInstances();

    // get the end of the vector
    unsigned vectorSize = RigidBodies.size();

    // loop through the vector of rigid bodies
    for (unsigned j = 0; j < vectorSize; ++j)
    {
      // set up pointer to the current rigid body
      RigidBody *bodyA = &RigidBodies[j];

      // skip if object is destroying
      if (bodyA->active == false || bodyA->owner->space->paused)
        continue;

      // skip if object is not on screen
      if (!bodyA->GetOwner()->space->QueryOnScreen(*(bodyA->GetOwner())))
      {
        bodyA->onScreen = false;
        continue;
      }

      bodyA->onScreen = true;

      // for each object, loop through every other object
      for (unsigned k = j + 1; k < vectorSize; ++k)
      {
        // skip if object is destroying or ghost
        if (RigidBodies[k].active == false)
          continue;

        // set up pointers to the other rigid body
        RigidBody *bodyB = &RigidBodies[k];

        // skip if both objects are static
        if (bodyA->fixed && bodyB->fixed)
          continue;

        // skip if collision table says to not resolve
        if (RigidBody::GetCollisionTableState(bodyA->GetGroup(), bodyB->GetGroup()) == CollisionState::Ignore)
          continue;

        // do collision and get manifold data
        Manifold result;
        bodyA->ResolveCollision(result, *bodyB);

        // if no collision, skip to next object
        if (result.collision == false)
          continue;

        // dispatch the collision events to both bodies
        // collision normals not done for now
        Events::CollisionEvent eventA(Vec2(0,0), bodyB->GetOwner()->id);
        Events::CollisionEvent eventB(Vec2(0, 0), bodyA->GetOwner()->id);

        bodyA->currentFrameCollisions.push_back(eventA);
        bodyB->currentFrameCollisions.push_back(eventB);

        // do not apply impulse if ghost
        if (bodyA->ghost || bodyB->ghost)
          continue;

        // skip if collision table says not to resolve
        if (RigidBody::GetCollisionTableState(bodyA->GetGroup(), bodyB->GetGroup()) == CollisionState::SkipResolution)
          continue;

        // push the manifold into the vector of collisions
        if (result.flip)
        {
          result.bodyA = reinterpret_cast<RigidBody **>(bodyB->selfPtr);
          result.bodyB = reinterpret_cast<RigidBody **>(bodyA->selfPtr);
        }
        else
        {
          result.bodyB = reinterpret_cast<RigidBody **>(bodyB->selfPtr);
          result.bodyA = reinterpret_cast<RigidBody **>(bodyA->selfPtr);
        }

        manifolds.push_back(result);
      }
    }

    // once all manifolds are generated, resolve the collisions in them
    // this process is repeated to create better accuracy
    size_t numManifolds = manifolds.size();
    for (unsigned i = 0; i < physicsIterations; ++i)
    {
      for (size_t j = 0; j < numManifolds; ++j)
      {
        manifolds[j].ApplyCollisionImpulse(dt);
      }
    }

    // now do positional correction, once all impulses are done
    for (unsigned i = 0; i < numManifolds; ++i)
    {
      Manifold current = manifolds[i];
      current.PositionalCorrection();
    }
  }

  // get the bounding box of a rigid body
  BoundingBox RigidBody::GetBoundingBox()
  {
    if (shape->GetType() == Shape::Type::Polygon)
    {
      Polygon *poly = reinterpret_cast<Polygon *>(shape);


      // get scaled vertices
      Vec2 scaledVertices[MaxVertexCount];
      unsigned vertexCount = poly->vertexCount;

      Matrix3 transform = GetOwner()->transform.GetMatrix3();

      for (unsigned i = 0; i < vertexCount; ++i)
      {
        scaledVertices[i] = transform * poly->vertices[i];
      }

      BoundingBox box = { scaledVertices[0], scaledVertices[0] };

      // loop through all vertices and get min/max
      for (unsigned i = 1; i < vertexCount; ++i)
      {
        Vec2 currentVert = scaledVertices[i];

        // make comparisons
        if (currentVert.x < box.bottomLeft.x)
          box.bottomLeft.x = currentVert.x;

        if (currentVert.y < box.bottomLeft.y)
          box.bottomLeft.y = currentVert.y;

        if (currentVert.x > box.topRight.x)
          box.topRight.x = currentVert.x;

        if (currentVert.y > box.topRight.y)
          box.topRight.y = currentVert.y;
      }

      return box;
    }

    // if circle
    else
      return BoundingBox();
  }



  std::vector<Handlet<Entity>> RigidBody::CastRay(Vec2 point, Vec2 direction, unsigned *count)
  {
    // get the vector of rigid bodies
    std::vector<RigidBody> &rigidBodies = GetInstances();
    unsigned vectorSize = rigidBodies.size();

    // initialize the vector of raycasted objects
    std::vector<RayCastResult> results;
    unsigned currentCount = 0;

    // get the tangential direction
    Vec2 tangent = { -direction.y, direction.x };
    float pointTangentDotProduct = tangent * point;
    float pointDirectionDotProduct = direction * point;

    // loop through all rigid bodies
    for (unsigned i = 0; i < vectorSize; ++i)
    {
      RigidBody &currentBody = rigidBodies[i];

      if (!currentBody.active)
        continue;

      // for now, don't raycast against circle colliders
      if (currentBody.shape->GetType() == Shape::Type::Polygon)
      {
        Polygon *poly = reinterpret_cast<Polygon *>(currentBody.shape);

        // get scaled vertices
        Vec2 scaledVertices[MaxVertexCount];
        unsigned vertexCount = poly->vertexCount;

        Matrix3 transform = currentBody.GetOwner()->transform.GetMatrix3();

        for (unsigned i = 0; i < vertexCount; ++i)
        {
          scaledVertices[i] = transform * poly->vertices[i];
        }

        float minProj = scaledVertices[0] * tangent;
        float maxProj = scaledVertices[0] * tangent;
        unsigned minIndex = 0;

        // loop through all the scaled vertices and get max / min projections
        for (unsigned i = 1; i < vertexCount; ++i)
        {
          float currProj = scaledVertices[i] * tangent;
          if (currProj < minProj)
          {
            minProj = currProj;
            minIndex = i;
          }
          if (currProj > maxProj)
            maxProj = currProj;
        }

        // if the ray does intersect, push a new raycast result into the vector
        if (pointTangentDotProduct > minProj && pointTangentDotProduct < maxProj)
        {
          // only push if the thing is in front of the point
          float distance = scaledVertices[minIndex] * direction - pointDirectionDotProduct;
          if (distance > 0)
            results.push_back({ distance, currentBody.GetOwner() });
        }
      }
    }

    // sort the results by distance
    std::sort(results.begin(), results.end());
    std::vector<Handlet<Entity>>ret;

    // get the number of things to return
    unsigned finalCount = results.size() < *count ? results.size() : *count;
    *count = finalCount;

    ret.reserve(finalCount);

    // copy over the right amount of objects
    for (unsigned i = 0; i < finalCount; ++i)
    {
      ret.push_back(results[i].entity);
    }

    return ret;
  }

}
