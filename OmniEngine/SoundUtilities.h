/******************************************************************************
Filename: SoundUtilities.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once
#include "stdafx.h"
#include <unordered_map>

namespace Omni
{
  namespace SoundUtilities
  {

    //To make things a little bit easier to read
    typedef FMOD::Studio::Bank* Bank;
    typedef FMOD::Studio::EventDescription* EventDesc;
    typedef FMOD::Studio::EventInstance* Instance;
    typedef FMOD::Studio::ParameterInstance* Parameter;
    typedef FMOD_STUDIO_PARAMETER_DESCRIPTION ParameterDesc;
    typedef FMOD_STUDIO_PLAYBACK_STATE PlayBackState;

    //More accessible if here
    void FMODErrCheck(FMOD_RESULT result, std::string name = "");

    struct ParameterHandle
    {
      size_t instance;
      size_t parameter;

      ParameterHandle(size_t instance_, size_t parameter_) : instance(instance_), parameter(parameter_) {}
    };


    struct Event
    {
      int paramCount;
      std::vector<ParameterDesc> parameters;
      EventDesc desc;

      ~Event() { parameters.clear(); };

    };

    struct SoundCue
    {
      Instance instance = nullptr;
      Event* myEvent;

      bool IsPlaying();
      ~SoundCue() 
      { 
        if (instance)
          //SoundUtilities::FMODErrCheck(instance->release());
          instance->release();
      };
    };


  }//Namespace SoundUtilities

} //Namespace Omni
