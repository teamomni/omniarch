/******************************************************************************
Filename: Event.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Event.h"
#include "Game.h"

namespace Omni
{
  namespace Events
  {
    using namespace v8;

    /***************************************
    *            Update Event              *
    ***************************************/

    static void v8_GetDt(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      auto &game = Game::Instance();

      Locker locker(info.GetIsolate());
      Isolate::Scope isolate_scope(info.GetIsolate());
      HandleScope handleScope(info.GetIsolate());

      // unwrapping and getting the event pointer
      Local<Object> eventObj = info.Holder();
      Local<External> wrap = Local<External>::Cast(eventObj->GetInternalField(0));
      UpdateEvent *updateEvent = static_cast<UpdateEvent *>(wrap->Value());

      info.GetReturnValue().Set(Number::New(game.isolate, updateEvent->dt));
    }

    void UpdateEvent::v8_Register()
    {
      auto &game = Game::Instance();

      Locker locker(game.isolate);
      Isolate::Scope isolate_scope(game.isolate);
      HandleScope handleScope(game.isolate);

      Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

      Local<ObjectTemplate> objTemplate_temp = ObjectTemplate::New(game.isolate);

      objTemplate_temp->SetInternalFieldCount(1);

      objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "dt"), AccessorGetterCallback(v8_GetDt), 0);

      objTemplate.Reset(game.isolate, objTemplate_temp);
    }

    /***************************************
    *            Hit Event                 *
    ***************************************/

    static void v8_GetOtherID(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      Locker locker(info.GetIsolate());
      Isolate::Scope isolate_scope(info.GetIsolate());
      HandleScope handleScope(info.GetIsolate());

      // unwrapping and getting the event pointer
      HitEvent *hitEvent = Take<HitEvent>::FromHolder_NonComponent(info);

      info.GetReturnValue().Set(Integer::New(info.GetIsolate(), int(hitEvent->otherID)));
    }

    static void v8_GetOther(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      Locker locker(info.GetIsolate());
      Isolate::Scope isolate_scope(info.GetIsolate());
      HandleScope handleScope(info.GetIsolate());

      // unwrapping and getting the event pointer
      HitEvent *hitEvent = Take<HitEvent>::FromHolder_NonComponent(info);

      // get the entity the ID points to
      Handlet<Entity> attacker = Game::Instance().GetEntityByID(hitEvent->otherID);

      // only return if the attacker is still alive, else return undefined
      if (attacker.isValid())
      {
        // wrap the attacker, and return to v8
        LocalObject attackerObj = attacker->v8_GetObject();
        attackerObj->SetInternalField(0, External::New(info.GetIsolate(), attacker->selfPtr));
        info.GetReturnValue().Set(attackerObj);
      }
      else
      {
        info.GetReturnValue().Set(Undefined(info.GetIsolate()));
      }
    }

    static void v8_GetMyType(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      Locker locker(info.GetIsolate());
      Isolate::Scope isolate_scope(info.GetIsolate());
      HandleScope handleScope(info.GetIsolate());

      // unwrapping and getting the event pointer
      HitEvent *hitEvent = Take<HitEvent>::FromHolder_NonComponent(info);

      // get the type of the hit event
      HitBox::Type type = hitEvent->myType;

      // return it to v8
      info.GetReturnValue().Set(Integer::New(info.GetIsolate(), int(type)));
    }

    static void v8_GetOtherType(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      Locker locker(info.GetIsolate());
      Isolate::Scope isolate_scope(info.GetIsolate());
      HandleScope handleScope(info.GetIsolate());

      // unwrapping and getting the event pointer
      HitEvent *hitEvent = Take<HitEvent>::FromHolder_NonComponent(info);

      // get the type of the hit event
      HitBox::Type type = hitEvent->otherType;

      // return it to v8
      info.GetReturnValue().Set(Integer::New(info.GetIsolate(), int(type)));
    }

    void HitEvent::v8_Register()
    {
      auto &game = Game::Instance();

      Locker locker(game.isolate);
      Isolate::Scope isolate_scope(game.isolate);
      HandleScope handleScope(game.isolate);

      Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

      Local<ObjectTemplate> objTemplate_temp = ObjectTemplate::New(game.isolate);

      objTemplate_temp->SetInternalFieldCount(1);

      objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "otherID"), AccessorGetterCallback(v8_GetOtherID), 0);
      objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "otherEntity"), AccessorGetterCallback(v8_GetOther), 0);
      objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "myType"), AccessorGetterCallback(v8_GetMyType), 0);
      objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "otherType"), AccessorGetterCallback(v8_GetOtherType), 0);

      objTemplate.Reset(game.isolate, objTemplate_temp);
    }

    /***************************************
    *         Collision Event              *
    ***************************************/
    static void v8_CE_GetOtherID(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      Locker locker(info.GetIsolate());
      Isolate::Scope isolate_scope(info.GetIsolate());
      HandleScope handleScope(info.GetIsolate());

      // unwrapping and getting the event pointer
      CollisionEvent *colEvent = Take<CollisionEvent>::FromHolder_NonComponent(info);

      info.GetReturnValue().Set(Integer::New(info.GetIsolate(), int(colEvent->otherID)));
    }

    static void v8_CE_GetOther(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      Locker locker(info.GetIsolate());
      Isolate::Scope isolate_scope(info.GetIsolate());
      HandleScope handleScope(info.GetIsolate());

      // unwrapping and getting the event pointer
      CollisionEvent *colEvent = Take<CollisionEvent>::FromHolder_NonComponent(info);

      // get the entity the ID points to
      Handlet<Entity> attacker = Game::Instance().GetEntityByID(colEvent->otherID);

      // only return if the attacker is still alive, else return undefined
      if (attacker.isValid())
      {
        // wrap the attacker, and return to v8
        LocalObject attackerObj = attacker->v8_GetObject();
        attackerObj->SetInternalField(0, External::New(info.GetIsolate(), attacker->selfPtr));
        info.GetReturnValue().Set(attackerObj);
      }
      else
      {
        info.GetReturnValue().Set(Undefined(info.GetIsolate()));
      }
    }

    static void v8_CE_GetCollisionNormal(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      Locker locker(info.GetIsolate());
      Isolate::Scope isolate_scope(info.GetIsolate());
      HandleScope handleScope(info.GetIsolate());

      // unwrapping and getting the event pointer
      CollisionEvent *colEvent = Take<CollisionEvent>::FromHolder_NonComponent(info);

      // make a js vector to return to v8
      Handle<Object> vec2Instance = Object::New(info.GetIsolate());
      vec2Instance->Set(String::NewFromUtf8(info.GetIsolate(), "x"), Number::New(info.GetIsolate(), colEvent->collisionNormal.x));
      vec2Instance->Set(String::NewFromUtf8(info.GetIsolate(), "y"), Number::New(info.GetIsolate(), colEvent->collisionNormal.y));

      info.GetReturnValue().Set(vec2Instance);
    }

    void CollisionEvent::v8_Register()
    {
      auto &game = Game::Instance();

      Locker locker(game.isolate);
      Isolate::Scope isolate_scope(game.isolate);
      HandleScope handleScope(game.isolate);

      Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

      Local<ObjectTemplate> objTemplate_temp = ObjectTemplate::New(game.isolate);

      objTemplate_temp->SetInternalFieldCount(1);

      objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "otherID"), AccessorGetterCallback(v8_CE_GetOtherID), 0);
      objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "otherEntity"), AccessorGetterCallback(v8_CE_GetOther), 0);
      objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "collisionNormal"), AccessorGetterCallback(v8_CE_GetCollisionNormal), 0);

      objTemplate.Reset(game.isolate, objTemplate_temp);
    }
  }
}
