/******************************************************************************
Filename: Image.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "GFXRenderer.h"
#include "GFXHandle.h"
#include "Vec2.h"

namespace Omni
{

  struct Image
  {
    Vec2 uv[2];
    Vec2 dimensions;
    Vec2 offset;
    Graphics::Handle texture;
    unsigned pixelsPerUnit;
    std::string name;

    Image(std::string name_, Graphics::Handle texture_) : dimensions(Vec2(32, 32)), texture(texture_), pixelsPerUnit(32), name(name_), offset(Vec2(0, 0))
    {
      uv[0] = Vec2(0, 0);
      uv[1] = Vec2(1, 1);
    }

    Image(){};
    ~Image(){};
  };
}

