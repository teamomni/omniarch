/******************************************************************************
Filename: UserComponent.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Component.h"
#include "Game.h"

namespace Omni
{
  using namespace v8;
  PersistentObjectTemplate UserComponent::objTemplate_cpp;
  std::unordered_map<std::string, PersistentFunction> UserComponent::creationFunctions;

  void UserComponent::InsertProperties()
  {
    auto &game = Game::Instance();

    // store the property definitions
    //createdModule->properties = game.componentDefinitions[moduleName].properties;
    for (auto const &propDefPair : game.componentDefinitions[name].properties)
    {
      properties.insert({ propDefPair.first, Property(propDefPair.second.type) });
      // SET DEFAULT VALUE HERE
      switch (propDefPair.second.type)
      {
      case Property::Type::Bool:
        SetProperty_Bool(propDefPair.first.c_str(), propDefPair.second.v_bool);
        break;
      case Property::Type::Color:
        SetProperty_Color(propDefPair.first.c_str(), propDefPair.second.v_color);
        // set to default
        break;
      case Property::Type::Float:
        SetProperty_Float(propDefPair.first.c_str(), propDefPair.second.v_float);
        break;
      case Property::Type::Int:
        SetProperty_Int(propDefPair.first.c_str(), propDefPair.second.v_int);
        break;
      case Property::Type::String:
        SetProperty_String(propDefPair.first.c_str(), propDefPair.second.v_string);
        break;
      case Property::Type::Undefined:
        // TODO: ERROR?!?!?!
        break;
      case Property::Type::Unsigned:
        SetProperty_Unsigned(propDefPair.first.c_str(), propDefPair.second.v_unsigned);
        break;
      case Property::Type::Vec2:
        // set to (0, 0)
        break;
      case Property::Type::Vec2List:
        // nope
        break;
      }
    }
  }

  static void v8_AddProperty(const FunctionCallbackInfo<Value> &info)
  {
    // the call will be component.AddProperty("positionx", Type.Int);

    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    // get the cpp module
    Local<Object> cppModuleObj = info.Holder();
    Local<External> wrap = Local<External>::Cast(cppModuleObj->GetInternalField(0));
    UserComponent *module = static_cast<UserComponent *>(wrap->Value());

    // get the property name
    Local<Value> check(info[0]);
    String::Utf8Value value(check);
    std::string propName(*value);

    // get the property type
    Property::Type type = static_cast<Property::Type>(info[1]->IntegerValue());

    module->properties.insert(std::pair<std::string, Property::Type>(propName, type));
    module->owner->FillTweakBar();
  }


  void UserComponent::v8_Register()
  {
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    Local<ObjectTemplate> objTemplate_temp = ObjectTemplate::New(game.isolate);

    objTemplate_temp->SetInternalFieldCount(1);

    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "AddProperty"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_AddProperty)));

    objTemplate_cpp.Reset(game.isolate, objTemplate_temp);
  }

  void Entity::v8_GetUserComponent_CPP(const FunctionCallbackInfo<Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the entity pointer
    Entity *entity = Take<Entity>::FromHolder(info);

    // getting the module name
    Local<Value> check(info[0]);
    String::Utf8Value value(check);

    // getting the module pointer through the c++ function
    UserComponent *module = static_cast<UserComponent *>(*entity->GetComponent(*value));

    // create an instance of the js object
    Handle<Object> result = module->v8_GetObject();

    // create a wrapper for the actual pointer
    Handle<External> module_ptr = External::New(info.GetIsolate(), module);

    // store the pointer in the js object
    result->SetInternalField(0, module_ptr);

    info.GetReturnValue().Set(result);
  }

  void Entity::v8_GetUserComponent(const FunctionCallbackInfo<Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Entity *owner = Take<Entity>::FromHolder(info);
    std::string name = Take<std::string>::FromV8(info[0]);

    if (owner == nullptr)
      return;

    UserComponent *userComp = reinterpret_cast<UserComponent *>(*owner->GetComponent(name));

    if (userComp)
      info.GetReturnValue().Set(LocalObject::New(info.GetIsolate(), userComp->v8_module));
    else
      info.GetReturnValue().Set(Undefined(info.GetIsolate()));
  }

  void UserComponent::Initialize()
  {
    using namespace v8;

    auto &game = Game::Instance();

    //isolate->AdjustAmountOfExternalAllocatedMemory(static_cast<int>(sizeof(Module)));
    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    // if no initialize, skip
    if (v8_init.IsEmpty())
      return;

    Handlet<Entity> entPtr = GetOwner();

    // get the owner pointer and stuff it in the javascript object
    Local<Object> owner = Local<Object>::New(game.isolate, v8_owner);

    Local<Object> v8_module_local = Local<Object>::New(game.isolate, v8_module);
    Local<Object> cpp_module = v8_GetObject();

    owner->SetInternalField(0, v8::External::New(game.isolate, entPtr->selfPtr));
    cpp_module->SetInternalField(0, v8::External::New(game.isolate, this));

    v8::Local<v8::Value> args[2] = { owner, cpp_module };
    Local<Function> initialize = Local<Function>::New(game.isolate, v8_init);

    // only call the initialize function if the game is not in editor mode
    if (!game.isEditing)
    {
      v8::TryCatch trycatch;
      Local<Value> v = initialize->Call(v8_module_local, 2, args);

      // check return value. if undefined, catch error
      if (trycatch.HasCaught())
      {
        auto e = trycatch.Message();
        if (!e.IsEmpty())
        {
          Local<Value> exception = trycatch.Exception();
          String::Utf8Value exception_str(exception);
          int errorLineNumber = e->GetLineNumber();
          String::Utf8Value errorLine(e->GetSourceLine());
          String::Utf8Value errorFile(e->GetScriptResourceName());
          char buffer[25];
          Console::Error("JavaScript Initialize Error : ", *exception_str);
          std::sprintf(buffer, ", Line Number %d : ", errorLineNumber);
          Console::Write(*errorFile, Omni::Console::Color::Red);
          Console::Write(buffer, Omni::Console::Color::Red);
          Console::WriteLine(*errorLine, Omni::Console::Color::Red);
        }
      }
    }
  }

  void UserComponent::Finalize() { /* Dummy function, does nothing. */ }


  UserComponent *UserComponent::Create(Entity *owner, std::string moduleName)
  {
    // get a reference to the game
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    UserComponent *createdModule = new UserComponent;
    createdModule->selfPtr = new Component *(createdModule);
    createdModule->type = Component::Type::User;

    // get the isolate
    auto isolate = game.isolate;

    // get the global object from the context
    auto global = isolate->GetEnteredContext()->Global();

    // get the module constructor from the script
    Local<Value> moduleRaw = global->Get(String::NewFromUtf8(isolate, moduleName.c_str()));
    Local<Object> moduleConstructor = Local<Object>::Cast(moduleRaw);

    // if we don't have the factory yet, get the factory from the script
    if (creationFunctions.find(moduleName) == creationFunctions.end())
    {
      Local<Value> moduleCreateRaw = moduleConstructor->Get(String::NewFromUtf8(isolate, "Create"));
      Local<Function> moduleCreate = Local<Function>::Cast(moduleCreateRaw);
      creationFunctions.insert({ moduleName, PersistentFunction(isolate, moduleCreate) });
    }

    // set up arguments to pass into the factory
    Local<Value> args[1] = { Int32::New(isolate, owner->id) };

    // get the created javascript object and store it
    Local<Function> moduleCreate = Local<Function>::New(isolate, creationFunctions[moduleName]);
    Local<Value> newModuleRaw = moduleCreate->Call(global, 1, args);
    Local<Object> newModuleTemp = Local<Object>::Cast(newModuleRaw);

    createdModule->v8_module.Reset(isolate, newModuleTemp);

    // get the initialize function
    if (newModuleTemp->Has(String::NewFromUtf8(isolate, "Initialize")))
    {
      Local<Function> init = Local<Function>::Cast(newModuleTemp->Get(String::NewFromUtf8(isolate, "Initialize")));
      createdModule->v8_init.Reset(isolate, init);
    }
    // pair this module to the entity
    createdModule->name = moduleName;

    // create an entity JS object
    createdModule->v8_owner.Reset(isolate, owner->v8_Createv8Entity());

    // also store the entity handle
    createdModule->owner = Handlet<Entity>(owner->selfPtr);

    // also store the space
    createdModule->space = owner->space;

    // inserts and stores the prop definitions, and also applies defaults
    createdModule->InsertProperties();

    createdModule->active = true;

    return createdModule;
  }

  Property::Bool UserComponent::GetProperty_Bool(const char *name) const
  {
    const Property &prop = properties.at(std::string(name));
    if (prop.type != Property::Type::Bool)
      return Property::Bool(false);
    
    // get a reference to the game
    auto &game = Game::Instance();
    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    LocalObject v8_localModule = LocalObject::New(game.isolate, v8_module);
    return v8_localModule->Get(String::NewFromUtf8(game.isolate, name))->BooleanValue();
  }
  void UserComponent::SetProperty_Bool(const char *name, Property::Bool value)
  {
    Property &prop = properties.at(std::string(name));
    if (prop.type != Property::Type::Bool)
      return;
    
    // get a reference to the game
    auto &game = Game::Instance();
    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    LocalObject v8_localModule = LocalObject::New(game.isolate, v8_module);
    v8_localModule->Set(String::NewFromUtf8(game.isolate, name), Boolean::New(game.isolate, value));
  }
  Property::Int UserComponent::GetProperty_Int(const char *name) const
  {
    const Property &prop = properties.at(std::string(name));
    if (prop.type != Property::Type::Int)
      return Property::Int(0);

    // get a reference to the game
    auto &game = Game::Instance();
    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    LocalObject v8_localModule = LocalObject::New(game.isolate, v8_module);
    return static_cast<Property::Int>(v8_localModule->Get(String::NewFromUtf8(game.isolate, name))->IntegerValue());
  }

  void UserComponent::SetProperty_Int(const char *name, Property::Int value)
  {
    Property &prop = properties.at(std::string(name));
    if (prop.type != Property::Type::Int)
      return;

    // get a reference to the game
    auto &game = Game::Instance();
    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    LocalObject v8_localModule = LocalObject::New(game.isolate, v8_module);
    v8_localModule->Set(String::NewFromUtf8(game.isolate, name), Integer::New(game.isolate, value));
  }
  Property::Unsigned UserComponent::GetProperty_Unsigned(const char *name) const
  {
    const Property &prop = properties.at(std::string(name));
    if (prop.type != Property::Type::Unsigned)
      return Property::Unsigned(0);

    // get a reference to the game
    auto &game = Game::Instance();
    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    LocalObject v8_localModule = LocalObject::New(game.isolate, v8_module);
    return static_cast<Property::Unsigned>(v8_localModule->Get(String::NewFromUtf8(game.isolate, name))->Uint32Value());
  }
  void UserComponent::SetProperty_Unsigned(const char *name, Property::Unsigned value)
  {
    Property &prop = properties.at(std::string(name));
    if (prop.type != Property::Type::Unsigned)
      return;
    
    // get a reference to the game
    auto &game = Game::Instance();
    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    LocalObject v8_localModule = LocalObject::New(game.isolate, v8_module);
    v8_localModule->Set(String::NewFromUtf8(game.isolate, name), Integer::NewFromUnsigned(game.isolate, value));
  }

  Property::Float UserComponent::GetProperty_Float(const char *name) const
  {
    const Property &prop = properties.at(std::string(name));
    if (prop.type != Property::Type::Float)
      return Property::Float(0);
    
    // get a reference to the game
    auto &game = Game::Instance();
    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    LocalObject v8_localModule = LocalObject::New(game.isolate, v8_module);
    return static_cast<Property::Float>(v8_localModule->Get(String::NewFromUtf8(game.isolate, name))->NumberValue());
  }
  void UserComponent::SetProperty_Float(const char *name, Property::Float value)
  {
    Property &prop = properties.at(std::string(name));
    if (prop.type != Property::Type::Float)
      return;
    
    // get a reference to the game
    auto &game = Game::Instance();
    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    Local<Object> v8_localModule = LocalObject::New(game.isolate, v8_module);
    v8_localModule->Set(String::NewFromUtf8(game.isolate, name), Number::New(game.isolate, double(value)));
  }
  Property::String UserComponent::GetProperty_String(const char *name) const
  {
    const Property &prop = properties.at(std::string(name));
    if (prop.type != Property::Type::String)
      return Property::String("");
    
    // get a reference to the game
    auto &game = Game::Instance();
    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    LocalObject v8_localModule = LocalObject::New(game.isolate, v8_module);

    Local<Value> check(v8_localModule->Get(String::NewFromUtf8(game.isolate, name)));
    String::Utf8Value value(check);
    return static_cast<Property::String>(std::string(*value));
  }
  void UserComponent::SetProperty_String(const char *name, Property::String value)
  {
    Property &prop = properties.at(std::string(name));
    if (prop.type != Property::Type::String)
      return;

    // get a reference to the game
    auto &game = Game::Instance();
    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    LocalObject v8_localModule = LocalObject::New(game.isolate, v8_module);
    v8_localModule->Set(String::NewFromUtf8(game.isolate, name), String::NewFromUtf8(game.isolate, value.c_str()));
  }
  Property::Vec2 UserComponent::GetProperty_Vec2(const char *name) const
  {
    return Property::Vec2(0.0f, 0.0f);
  }
  void UserComponent::SetProperty_Vec2(const char *name, Property::Vec2 value)
  {
  }
  Property::Vec2List UserComponent::GetProperty_Vec2List(const char *name) const
  {
    return Property::Vec2List();
  }
  void UserComponent::SetProperty_Vec2List(const char *name, Property::Vec2List value)
  {
  }
  Property::Color UserComponent::GetProperty_Color(const char *name) const
  {
    return Property::Color(1.0f, 1.0f, 1.0f, 1.0f);
  }
  void UserComponent::SetProperty_Color(const char *name, Property::Color value)
  {
  }
  
}
