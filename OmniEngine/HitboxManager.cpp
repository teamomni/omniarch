/******************************************************************************
Filename: HitboxManager.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "HitboxManager.h"
#include "Game.h"

namespace Omni
{
  void HitBoxManager::PushNewCollision(size_t otherID, HitBox::Type myType, HitBox::Type otherType)
  {
    Events::HitEvent currentEvent(otherID, myType, otherType);

    bool duplicate = std::find(currentFrameCollisions.begin(),
      currentFrameCollisions.end(), currentEvent) != currentFrameCollisions.end();

    if (!duplicate)
      currentFrameCollisions.push_back(currentEvent);
  }

  void HitBoxManager::ClearCollisions()
  {
    currentFrameCollisions.clear();
  }

  void HitBoxManager::HitBoxStep(float dt)
  {
    std::vector<HitBoxManager> &hitBoxManagers = GetInstances();

    // loop through all hitboxes
    unsigned vectorSize = hitBoxManagers.size();

    for (unsigned i = 0; i < vectorSize; ++i)
    {
      // skip inactive hitbox manager
      if (hitBoxManagers[i].active == false)
        continue;
     
      HitBoxManager &lhsManager = hitBoxManagers[i];

      lhsManager.ClearCollisions();

      // for each hitbox manager, loop through all other managers
      for (unsigned j = 0; j < vectorSize; ++j)
      {
        // if checking with self
        if (j == i)
          continue;

        // skip inactive hitbox manager
        if (hitBoxManagers[j].active == false)
          continue;

        HitBoxManager &rhsManager = hitBoxManagers[j];

        unsigned lhsNumHitboxes = lhsManager.hitBoxes.size();
        unsigned rhsNumHitboxes = rhsManager.hitBoxes.size();
        int outerHitMask = 0;

        // within each manager, loop through all of their hitboxes
        for (unsigned k = 0; k < lhsNumHitboxes; ++k)
        {
          HitBox *lhsHitBox = lhsManager.hitBoxes[k];

          int hitMask = 0;

          // for each hitbox, check against other managers' hitboxes
          for (unsigned m = 0; m < rhsNumHitboxes; ++m)
          {
            HitBox *rhsHitBox = rhsManager.hitBoxes[m];

            lhsHitBox->ResolveCollision(rhsHitBox);

            // if the rhs hit box type has already been checked, skip
            //if (hitMask & int(rhsHitBox->GetType()))
            //  continue;

            //hitMask = hitMask | lhsHitBox->ResolveCollision(rhsHitBox);

            // if my type and his type have already been checked, skip
            //if ((int(lhsHitBox->GetType()) & outerHitMask) && (int(rhsHitBox->GetType()) & hitMask))
            //  continue;

            // resolves collision and pushes it to the manager
            // if there is collision, break, as we don't need to check collisions
            // with all the other hit boxes
            //int result = lhsHitBox->ResolveCollision(rhsHitBox) | hitMask;

            //if (result)
            //  outerHitMask = outerHitMask | int(lhsHitBox->GetType());

            //hitMask = hitMask | result;

            //if (hitMask == 3)
            //  break;
          }

          // if all combinations have been checked, stop checking
          //if (outerHitMask == 3 && hitMask == 3)
          //  break;
        }
      }
    }
  }

  HitBoxManager::HitBoxManager() : hitBoxes(0), collisionStarted(0), collisionPersisted(0),
    collisionEnded(0), currentFrameCollisions(0)
  {
  }


  HitBoxManager::~HitBoxManager()
  {
  }

  void HitBoxManager::Initialize()
  {
  }

  void HitBoxManager::Register()
  {
    RegisterName("HitBoxManager", &Game::Instance().systemContainers);
  }

  // checks all current frame collisions and pushes them to the right vectors
  void HitBoxManager::Update(float dt)
  {
    // get self pointer and use that instead of this pointer
    Handlet<HitBoxManager> self = reinterpret_cast<HitBoxManager **>(this->selfPtr);

    // clear collisionEnded
    collisionEnded.clear();

    // make sure current frame collisions only has unique hit events
    auto collisionCopy = currentFrameCollisions;
    auto newEnd = std::unique(currentFrameCollisions.begin(), currentFrameCollisions.end());
    
    // remove stuff if you need to
    if (newEnd != currentFrameCollisions.end())
      currentFrameCollisions.erase(newEnd, currentFrameCollisions.end());

    int firstcount = 0;

    // loop through all previous frame collisions
    for (auto i = collisionPersisted.begin(); i != collisionPersisted.end(); ++i)
    {
      Events::HitEvent currentEvent(*i);

      // if not found in current frame's collisions, move to collision ended
      auto objectPositionInCurrent = std::find(currentFrameCollisions.begin(), currentFrameCollisions.end(), currentEvent);
      if (objectPositionInCurrent == currentFrameCollisions.end())
      {
        firstcount++;
        eventsToClear.push_back(currentEvent);

        collisionEnded.push_back(currentEvent);
      }

      // if yes found, remove from current frame collisions
      else
      {
        currentFrameCollisions.erase(objectPositionInCurrent);
      }
    }

    int count = 0;
    auto persistedCopy = collisionPersisted;

    // loop through iterators to clear and clear them from collision persisted
    for (auto i = eventsToClear.begin(); i != eventsToClear.end(); ++i)
    {
      ++count;
      collisionPersisted.erase(std::find(collisionPersisted.begin(), collisionPersisted.end(), *i));
    }

    eventsToClear.clear();

    // loop through all collision started
    for (auto i = collisionStarted.begin(); i != collisionStarted.end(); ++i)
    {
      Events::HitEvent currentEvent(*i);

      // if not found in current frame's collisions, move to collision ended
      auto objectPositionInCurrent = std::find(currentFrameCollisions.begin(), currentFrameCollisions.end(), currentEvent);
      if (objectPositionInCurrent == currentFrameCollisions.end())
      {
        collisionEnded.push_back(currentEvent);
      }

      // if yes found, push to collision persist and remove from current frame collisions
      else
      {
        collisionPersisted.push_back(currentEvent);
        currentFrameCollisions.erase(objectPositionInCurrent);
      }
    }

    collisionStarted.clear();

    // the only collisions left should be collision started
    unsigned numStarted = currentFrameCollisions.size();
    for (unsigned i = 0; i < numStarted; ++i)
    {
      collisionStarted.push_back(currentFrameCollisions[i]);
    }

    currentFrameCollisions.clear();

    // now that all vectors have been updated, send event to the entity
    Handlet<Entity> dispatchHolder = GetOwner();
   
    auto &game = Game::Instance();

    v8::Locker locker(game.isolate);
    v8::Isolate::Scope isolate_scope(game.isolate);
    v8::HandleScope handleScope(game.isolate);

    v8::Context::Scope contextScope(v8::Local<v8::Context>::New(game.isolate, game.context));

    // collision started
    size_t vecSize = collisionStarted.size();
    for (size_t i = 0; i < vecSize; ++i)
    {
      LocalObject obj = self->collisionStarted[i].v8_GetObject();
      obj->SetInternalField(0, v8::External::New(game.isolate, &self->collisionStarted[i]));
      
      if (dispatchHolder.isValid())
        dispatchHolder->dispatcher->DispatchEvent("onHitStart", obj);

      // if the self object has been deleted, stop executing this function
      if (!self.isValid())
        return;
    }

    // collision persisted
    vecSize = self->collisionPersisted.size();
    for (size_t i = 0; i < vecSize; ++i)
    {
      LocalObject obj = self->collisionPersisted[i].v8_GetObject();
      obj->SetInternalField(0, v8::External::New(game.isolate, &self->collisionPersisted[i]));
      dispatchHolder->dispatcher->DispatchEvent("onHitPersist", obj);

      // if the self object has been deleted, stop executing this function
      if (!self.isValid())
        return;
    }

    // collision ended
    vecSize = self->collisionEnded.size();
    for (size_t i = 0; i < vecSize; ++i)
    {
      LocalObject obj = self->collisionEnded[i].v8_GetObject();
      obj->SetInternalField(0, v8::External::New(game.isolate, &self->collisionEnded[i]));
      dispatchHolder->dispatcher->DispatchEvent("onHitEnd", obj);

      // if the self object has been deleted, stop executing this function
      if (!self.isValid())
        return;
    }

  }

  void HitBoxManager::Destroy()
  {
    // deleted the malloc-ed hitboxes
    size_t numHitBoxes = hitBoxes.size();
    for (size_t i = 0; i < numHitBoxes; ++i)
    {
      delete hitBoxes[i];
    }
    hitBoxes.clear();
    Game::Instance().DiscardModulePropertyHandles(Handlet<Component>(selfPtr));
  }

  // adding hitboxes
  void HitBoxManager::AddBox(const Vec2 &offset, float hw, float hh, HitBox::Type type)
  {
    HitBox *newBox = new AABBHitBox(offset, hw, hh);
    newBox->SetType(type);
    newBox->SetManager(this);
    hitBoxes.push_back(newBox);
  }
  void HitBoxManager::AddCircle(const Vec2 &offset, float radius, HitBox::Type type)
  {
    HitBox *newBox = new CircleHitBox(offset, radius);
    newBox->SetType(type);
    newBox->SetManager(this);
    hitBoxes.push_back(newBox);
  }
}