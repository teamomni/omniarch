/******************************************************************************
Filename: Animator.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Animator.h"
#include "Game.h"
#include "HitboxManager.h"
#include "Sprite.h"
#include "SoundEmitter.h"
#include <iostream>

namespace Omni
{
  Animator::~Animator()
  {
    ScriptableDestroy();
  }

  void Animator::Initialize()
  {
    if (defaultAnimation != "")
    {
      Set(defaultAnimation);
    }
  }

  void Animator::Update(float dt)
  {
    done = false;

    // if there's no animations in the queue, bail
    if (animations.size() == 0)
    {
      frameRemaining = 0.0f;
      currentFrame = 0;
      done = true;
      return;
    }

    Animation *anim = &Game::Instance().assets.animations[animations.front()];
    frameRemaining -= dt;

    bool frameChanged = false;

    while (frameRemaining <= 0.0f)
    {
      ++currentFrame;
      if (AnimationCount() > 0)
        frameChanged = true;

      // if this animation is "over" (time to restart, get new animation, or freeze)
      if (currentFrame > anim->frames.size() - 1)
      {
        // this animation is looping so let's start it over again
        if (anim->looping)
        {
          currentFrame = 0;
          frameRemaining += anim->frames[0].duration;
          continue;
        }
        else
        {
          // this animation is the only one and it's non-looping, so freeze it
          if (animations.size() == 1)
          {
            frameRemaining = 0.0f;
            break;
          }
          // there are more animations and it's time to move onto the next one
          else
          {
            currentFrame = 0;
            animations.pop();
            anim = &Game::Instance().assets.animations[animations.front()];
            frameRemaining += anim->frames[0].duration;
            continue;
          }
        }
      }
      else
      {
        frameRemaining += anim->frames[currentFrame].duration;
      }
    }

    currentFrame = min(currentFrame, anim->frames.size() - 1);
    if (currentFrame == anim->frames.size() - 1 && frameRemaining <= 0)
      done = true;

    reinterpret_cast<Sprite *>(*owner->GetComponent("Sprite"))->SetImage(anim->frames[currentFrame].image);
    if (anim->frames[currentFrame].soundCue != "")
    {
      SoundEmitter *emitter = static_cast<SoundEmitter *>(*owner->GetComponent("SoundEmitter"));
      emitter->PlaySound(anim->frames[currentFrame].soundCue);
    }

    if (frameChanged && !CurrentFrame()->inheritHitboxes)
    {
      HitBoxManager *hbm = static_cast<HitBoxManager *>(*owner->GetComponent("HitBoxManager"));
      if (hbm)
      {
        hbm->Clear();
        for (HitBoxData &hbd : CurrentFrame()->hitboxes)
        {
          if (hbd.radius != 0)
            hbm->AddCircle(hbd.center, hbd.radius, hbd.type);
          else
          {
            float scaleX = hbd.scale.x / 2;
            Vec2 center;
            center.x = hbd.center.x;
            center.y = hbd.center.y;
            if (reinterpret_cast<Sprite *>(*owner->GetComponent("Sprite"))->p_FlipX)
            {
              center.x = -center.x;
            }
            hbm->AddBox(center, scaleX, hbd.scale.y / 2, hbd.type);
          }
        }
      }
    }

  }

  void Animator::Destroy()
  {
    ScriptableDestroy();
    Game::Instance().DiscardModulePropertyHandles(Handlet<Component>(selfPtr));
  }

  void Animator::Register()
  {
    RegisterName("Animator", &Game::Instance().systemContainers, PropertyDefinitionMap({
      { "defaultAnimation", PropertyDefinition::String("", offsetof(Animator, defaultAnimation), true) } }
    ));
  }

  void Animator::Queue(std::string animation)
  {
    if (animations.size() == 0)
    {
      currentFrame = 0;
      Animation *anim = &Game::Instance().assets.animations[animation];
      frameRemaining += anim->frames[0].duration;
    }
    animations.push(animation);
    done = false;
  }

  void Animator::Clear()
  {
    std::queue<std::string>().swap(animations);
    frameRemaining = 0;
    currentFrame = 0;
  }

  void Animator::Set(std::string animation)
  {
    Clear();
    Queue(animation);
  }

  Frame *Animator::CurrentFrame()
  {
    if (animations.size() > 0)
      return &Game::Instance().assets.animations[animations.front()].frames[currentFrame];
    return nullptr;
  }

}
