/******************************************************************************
Filename: GFXDebugDraw.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "GFXRenderer.h"
#include "GFXManager.h"
#include "Vec2.h"


namespace Omni
{
namespace Graphics
{
  class GFXDebugDraw
  {
    public:
      GFXDebugDraw();
      ~GFXDebugDraw();

      void ShutDown(void);

      void ClearVectors(void);

      void DrawLine(Vec2 start, Vec2 end, size_t space = 0, D3DXCOLOR color = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
      void DrawPoint(Vec2 position, size_t space = 0, D3DXCOLOR color = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
      void DrawQuad(Transform transform, size_t space = 0, D3DXCOLOR color = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
      void DrawQuad(Vec2 position, Vec2 scale, float rotation, size_t space = 0, D3DXCOLOR color = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));
      void DrawArrow(Vec2 start, Vec2 dirNormal, size_t space = 0, D3DXCOLOR color = D3DXCOLOR(1.0f, 0.0f, 0.0f, 1.0f));

    private:

      void StartDrawDebug(GFXManager &gfxManager, RenderState &renderState);

      void BeginDrawDebug(GFXManager &gfxManager);
      void DrawDebug(GFXManager &gfxManager, RenderState &renderState);
      void EndDebugDraw(GFXManager &gfxManager);

      debugInstance CreateInstance(RenderState::Line& currentLine);
      debugInstance CreateInstance(RenderState::Quad& currentQuad);
      debugInstance CreateInstance(RenderState::Arrow& currentArrow);

      
      Handle m_instanceBuffer[3];
      /*std::vector<Line> m_lines;
      std::vector<Quad> m_quads;
      std::vector<Arrow> m_arrows;*/
      std::vector<debugInstance> m_instanceLineData;
      std::vector<debugInstance> m_instanceQuadData;
      std::vector<debugInstance> m_instanceArrowData;


    friend class GFXManager;

  };
} //Namespace Graphics
} //Namespace Omni
