/******************************************************************************
Filename: Transformv8.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Transform.h"
#include "v8.h"
#include "Game.h"
#include "Camera.h"
#include "Entity.h"

using namespace v8;

namespace Omni
{
  v8::Persistent<v8::ObjectTemplate, v8::CopyablePersistentTraits<v8::ObjectTemplate>> Transform::objTemplate;

  static void v8_GetTranslation(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the animator pointer
    Local<Object> animatorObj = info.Holder();
    Local<External> wrap = Local<External>::Cast(animatorObj->GetInternalField(0));
    Transform *transform = static_cast<Transform *>(wrap->Value());

    // getting the vec2 template from the global object
    Game &game = Game::Instance();
    Handle<Value> vec2_raw = game.isolate->GetEnteredContext()->Global()->Get(String::NewFromUtf8(info.GetIsolate(), "Vector"));
    Handle<Function> vec2Constructor = Handle<Function>::Cast(vec2_raw);

    // make a new js object and store the vec2 pointer in there
    Handle<Object> vec2Instance = vec2Constructor->NewInstance();
    vec2Instance->SetInternalField(0, External::New(info.GetIsolate(), &transform->translation));
    
    // return the vector
    info.GetReturnValue().Set(vec2Instance);
  }

  static void v8_SetTranslation(v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the transform pointer
    Local<Object> spriteObj = info.Holder();
    Local<External> wrap = Local<External>::Cast(spriteObj->GetInternalField(0));
    Transform *transform = static_cast<Transform *>(wrap->Value());

    Local<Object> vec2Obj = Local<Object>::Cast(value);
    float newX = float(vec2Obj->Get(String::NewFromUtf8(info.GetIsolate(), "x"))->NumberValue());
    float newY = float(vec2Obj->Get(String::NewFromUtf8(info.GetIsolate(), "y"))->NumberValue());

    transform->translation.x = newX;
    transform->translation.y = newY;
  }

  static void v8_GetScale(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the animator pointer
    Local<Object> animatorObj = info.Holder();
    Local<External> wrap = Local<External>::Cast(animatorObj->GetInternalField(0));
    Transform *transform = static_cast<Transform *>(wrap->Value());

    // getting the vec2 template from the global object
    Game &game = Game::Instance();
    Handle<Value> vec2_raw = game.isolate->GetEnteredContext()->Global()->Get(String::NewFromUtf8(info.GetIsolate(), "Vector"));
    Handle<Function> vec2Constructor = Handle<Function>::Cast(vec2_raw);

    // make a new js object and store the vec2 pointer in there
    Handle<Object> vec2Instance = vec2Constructor->NewInstance();
    vec2Instance->SetInternalField(0, External::New(info.GetIsolate(), &transform->scale));

    // return the vector
    info.GetReturnValue().Set(vec2Instance);
  }

  static void v8_SetScale(v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the transform pointer
    Local<Object> spriteObj = info.Holder();
    Local<External> wrap = Local<External>::Cast(spriteObj->GetInternalField(0));
    Transform *transform = static_cast<Transform *>(wrap->Value());

    Local<Object> vec2Obj = Local<Object>::Cast(value);
    float newX = float(vec2Obj->Get(String::NewFromUtf8(info.GetIsolate(), "x"))->NumberValue());;
    float newY = float(vec2Obj->Get(String::NewFromUtf8(info.GetIsolate(), "y"))->NumberValue());;

    transform->scale.x = newX;
    transform->scale.y = newY;
  }

  static void v8_GetRotation(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the animator pointer
    Local<Object> animatorObj = info.Holder();
    Local<External> wrap = Local<External>::Cast(animatorObj->GetInternalField(0));
    Transform *transform = static_cast<Transform *>(wrap->Value());

    // return the array
    info.GetReturnValue().Set(Number::New(info.GetIsolate(), transform->GetRotation()));
  }

  static void v8_SetRotation(v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the transform pointer
    Local<Object> spriteObj = info.Holder();
    Local<External> wrap = Local<External>::Cast(spriteObj->GetInternalField(0));
    Transform *transform = static_cast<Transform *>(wrap->Value());

    transform->SetRotation(float(value->NumberValue()));
  }

  void Transform::v8_Register()
  {
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    // wrapping the transform
    Handle<ObjectTemplate> transformTemplate = ObjectTemplate::New();

    transformTemplate->SetInternalFieldCount(1);

    transformTemplate->SetAccessor(String::NewFromUtf8(game.isolate, "translation"), AccessorGetterCallback(v8_GetTranslation), AccessorSetterCallback(v8_SetTranslation));
    transformTemplate->SetAccessor(String::NewFromUtf8(game.isolate, "scale"), AccessorGetterCallback(v8_GetScale), AccessorSetterCallback(v8_SetScale));
    transformTemplate->SetAccessor(String::NewFromUtf8(game.isolate, "rotation"), AccessorGetterCallback(v8_GetRotation), AccessorSetterCallback(v8_SetRotation));
  
    objTemplate.Reset(game.isolate, transformTemplate);
  }

  void Camera::v8_GetTransform(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the pointer
    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    Camera *transformHolder = static_cast<Camera *>(wrap->Value());

    // getting the transform pointer
    Transform *transform = &transformHolder->transform;

    // create an instance of the js object
    Handle<Object> result = transform->v8_GetObject();

    // create a wrapper for the actual pointer
    Handle<External> transformPtr = External::New(info.GetIsolate(), transform);

    // store the pointer in the js object
    result->SetInternalField(0, transformPtr);

    info.GetReturnValue().Set(result);
  }

  void Entity::v8_GetTransform(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the pointer
    Entity *transformHolder = Take<Entity>::FromHolder(info);

    // if the entity is invalid, do not check
    if (transformHolder == nullptr)
    {
      // std::cout << "trying to get transform of " << info.Holder()->Get(String::NewFromUtf8(info.GetIsolate(), "owner_id"))->IntegerValue() << std::endl;
      info.GetReturnValue().Set(Undefined(info.GetIsolate()));
      return;
    }

    // getting the transform pointer
    Transform *transform = &transformHolder->transform;

    // if the transform pointer is invalid, do not check
    if (transform == nullptr)
    {
      info.GetReturnValue().Set(Undefined(info.GetIsolate()));
      return;
    }

    // create an instance of the js object
    Local<Object> result = transform->v8_GetObject();

    // create a wrapper for the actual pointer
    Local<External> transformPtr = External::New(info.GetIsolate(), transform);

    // store the pointer in the js object
    result->SetInternalField(0, transformPtr);

    info.GetReturnValue().Set(result);
  };
}