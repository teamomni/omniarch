/******************************************************************************
Filename: SoundEmitterv8.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "SoundEmitter.h"

namespace Omni
{
  using namespace v8;

  static void v8_SetParameter(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    SoundEmitter *emitter = Take<SoundEmitter>::FromHolder(info);

    //emitter->SetParameter(Take<std::string>::FromV8(info[0]), Take<float>::FromV8(info[1]));
   
    try
    {
      emitter->m_paramVals.at(Take<int>::FromV8(info[0])) = Take<float>::FromV8(info[1]);
    }
    catch (std::out_of_range)
    {}
  }

  void SoundEmitter::v8_Register()
  {
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    Local<ObjectTemplate> objTemplate_temp;

    // if there's nothing in the obj template right now, make a new one
    if (objTemplate.IsEmpty())
    {
      objTemplate_temp = ObjectTemplate::New(game.isolate);
      objTemplate_temp->SetInternalFieldCount(1);
    }
    // else, get it from the object
    else
      objTemplate_temp = LocalObjectTemplate::New(game.isolate, objTemplate);

    // REGISTER FUNCTIIONS HERE

    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "SetParameter"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_SetParameter)));

    objTemplate.Reset(game.isolate, objTemplate_temp);

    ExposeProperties();
  }

  v8_RegisterComponentWithEntity(SoundEmitter);
}