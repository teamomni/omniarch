/******************************************************************************
Filename: RenderState.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "RenderState.h"

namespace Omni
{
  void RenderState::BeginUpdate(size_t tick)
  {
    beginTick = tick;
    cameras.clear();
    objects.clear();
    arrows.clear();
    quads.clear();
    lines.clear();
    textObjects.clear();

  }

  void RenderState::EndUpdate(size_t tick)
  {
    endTick = tick;
  }

  void RenderState::Clear()
  {
    BeginUpdate(0);
    endTick = 0;
  }


}
