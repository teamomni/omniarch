/******************************************************************************
Filename: DirectXIncludes.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// File     :    DirectXIncludes.h
// Author   :    Juli Gregg
// Brief    :    DirectX10/11 Headers and includes

#pragma once

#include <d3d11.h>
#include <d3dx11.h>
#include <d3dx10.h>
#include "D3DCompiler.h"



// include the Direct3D Library file
#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dx11.lib")
#pragma comment (lib, "d3dx10.lib")


