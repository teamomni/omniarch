/******************************************************************************
Filename: GFXTools.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// Juli Gregg
// GFXTools.h

#pragma once

#include "stdafx.h"
#include <string>
#include <vector>

//Effects the size of the constant buffer
#define NUM_OF_CAMERAS 8

namespace Omni
{
  namespace Graphics
  {
    struct Vertex
    {
      FLOAT X, Y, Z;
      FLOAT U, V;
    };

    struct Instance
    {
      D3DXCOLOR Color;
      FLOAT X, Y;
      FLOAT A, B;
      FLOAT THETA;
      FLOAT U1, V1;
      FLOAT U2, V2;
      UINT Camera;
      D3DXCOLOR gColor;
      FLOAT gIntensity;
      BOOL gEnabled;
    };

    struct debugInstance
    {
      D3DXCOLOR Color;
      FLOAT X, Y;
      FLOAT A, B;
      FLOAT THETA;
      UINT Camera;
    };

    struct BlurData
    {
      FLOAT SCREEN;

      BlurData(){};
      BlurData(float _screen) :SCREEN(_screen){}
    };


    enum class OBJT
    {
      None = -1,
      VerterxShader,
      PixelShader,
      Texture,
      VertexBuffer,
      IndexBuffer,
      ConstantBuffer,
      RenderTarget,
      BackBuffer,
      RasterizerState,
      FontWrapper,
      BlendMode

    };

    enum MngrHandles
    {
      BasicVShader = 0,
      BasicPShader,
      SpriteVShader,
      SpritePShader,
      ParticleAddPShader,
      BloomVShader,
      BloomPShader,
      HorizBlurVShader,
      HorizBlurPShader,
      VertBlurVShader,
      VertBlurPShader,
      DefaultVBuffer,
      DefaultIBuffer,
      DefaultCBuffer,
      O_RTarget,
      G_RTarget,
      S_RTarget,
      E_RTarget,
      B_RTarget,
      B_Alpha,
      B_Add,
      B_AlphaAdd
      

    };

    enum DebugHandles
    {
      DebugVShader = 0,
      DebugPShader,
      DebugIBuffer_Line,
      DebugIBuffer_Quad,
      DebugIBuffer_Tri
    };

    enum PrimitiveTopology
    {
      PRIMITIVE_TOPOLOGY_POINTLIST = 2,
      PRIMITIVE_TOPOLOGY_LINELIST = 3,
      PRIMITIVE_TOPOLOGY_TRIANGLELIST = 4,
      PRIMITIVE_TOPOLOGY_TRIANGLESTRIP
    };

    enum class BlendModes
    {
      Alpha,
      Additive
    };

    enum class GFX_FORMAT
    {
      float1   = DXGI_FORMAT_R32_FLOAT,
      float2   = DXGI_FORMAT_R32G32_FLOAT,
      float3   = DXGI_FORMAT_R32G32B32_FLOAT,
      float4   = DXGI_FORMAT_R32G32B32A32_FLOAT,
      color4   = DXGI_FORMAT_R32G32B32A32_FLOAT,
      uint1    = DXGI_FORMAT_R32_UINT,
      booltf   = DXGI_FORMAT_R8_UINT
    };

    struct InputElement
    {
      InputElement(const std::string& semantic_, GFX_FORMAT format_, bool instance_ = true, unsigned structureSlot_ = 0)
        : semantic(semantic_), format(format_), instance(instance_), structureSlot(structureSlot_) {}

      std::string semantic;
      GFX_FORMAT format;
      bool instance = true;
      unsigned structureSlot = 0;
    };

    typedef std::vector<InputElement> InputLayout;

    struct Viewport
    {
      int xOffset, yOffset;
      float dimensionX, dimensionY;
    };

  }
}
