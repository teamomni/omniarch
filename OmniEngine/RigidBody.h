/******************************************************************************
Filename: RigidBody.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "Manifold.h"
#include "Vec2.h"
#include "Matrix3.h"
#include "Component.h"
#include "Face.h"
#include "Material.h"
#include "v8.h"
#include "Event.h"

namespace Omni
{
  struct Shape; // forward declaration

  struct BoundingBox
  {
    Vec2 bottomLeft;
    Vec2 topRight;

    BoundingBox() : bottomLeft(), topRight() {}
    BoundingBox(float hw, float hh) : bottomLeft(-hw, -hh), topRight(hw, hh) {}
    BoundingBox(Vec2 bottomLeft_, Vec2 topRight_) : bottomLeft(bottomLeft_), topRight(topRight_) {}

  };

  struct RayCastResult
  {
    float distance;
    Handlet<Entity> entity;

    bool operator<(const RayCastResult &rhs)
    {
      return distance < rhs.distance;
    }
  };

  class RigidBody : public SystemComponent<RigidBody>
  {
  public:
    enum class Group {
      Default = 0,
      Group1 = 1,
      Group2 = 2,
      Group3 = 3,
      Group4 = 4,
      Group5 = 5,
      Group6 = 6,
      Group7 = 7,
      Group8 = 8
    };

    enum class CollisionState {
      Resolve = 0,
      Ignore = 1,
      SkipResolution = 2
    };

    RigidBody::RigidBody();
    RigidBody(const Shape *shape_, const Material &material_);
    ~RigidBody();
    static void Register();

    // function to set the shape and material of the rigid body
    void Set(const Shape *shape_, const Material &material_);
    void Set(const Shape *shape_);
    void Set(const Material &material_);

    // apply impulse at point
    void ApplyImpulseAtPoint(Vec2 force, Vec2 relativePoint);

    // v8 register function
    static void v8_Register();

    // gettor/settor for gravity scale
    float GetGravityScale() const;
    void SetGravityScale(float newScale);

    // apply linear impulse/force
    void ApplyLinearImpulse(Vec2 impulse);
    void ApplyLinearImpulse(float x, float y);
    void ApplyLinearForce(Vec2 force);
    void ApplyLinearForce(float x, float y);

    // gettor for shape
    Shape *RigidBody::GetShape() const;

    // get switches
    bool GetGhost() { return ghost; }
    bool GetFixed() { return fixed; }
    bool GetRotationLocked() { return rotationLocked; }

    // set switches
    void SetGhost(bool ghost_);
    void SetFixed(bool fixed_);
    void SetRotationLocked(bool locked);

    // toggle switches
    void ToggleGhost();
    void ToggleFixed();
    void ToggleRotationLocked();

    // collision group functions
    void SetGroup(Group group_);
    Group GetGroup() const;

    // component functions
    void Update(float dt);
    void Initialize();
    void Destroy();

    // collision event function
    void DispatchCollisionEvents();

    // static functions
    static void Step(float dt);
    static void EndFrame();

    // loops per frame for force application
    static unsigned loopsPerFrame;
    static float accumulator;

    // framerate stuff
    static const float physicsFrameRate;
    static const float physicsTimestep;

    // gravity
    static float gravity;

    // struct containing information about an object's mass
    struct {
      // linear mass data
      float mass;
      float inverseMass;

      // rotational mass data
      float inertia;
      float inverseInertia;
    } massData;

    // vectors for events
    std::vector<Events::CollisionEvent>collisionStarted;
    std::vector<Events::CollisionEvent>collisionPersisted;
    std::vector<Events::CollisionEvent>collisionEnded;
    std::vector<Events::CollisionEvent> currentFrameCollisions;

    // linked list of iterators to clear
    std::list<Events::CollisionEvent> eventsToClear;

    // struct containing information about an object's material
    Material material;

    Property::Vec2 velocity;       // object's current velocity
    
    // rotational components
    Property::Float angularVelocity;  // rotational velocity in radians

    Property::String p_Material;
    std::string materialLast;

    Property::Float p_BodyRadius;
    Property::Vec2List p_BodyPoints;

    // bool to indicate if on ground
    bool onGround;

    // static 2D array for collision table
    static CollisionState collisionTable[16][16];

    // get the bounding box of a rigid body THIS FUNCTION IS TERRIBLE DO NOT CALL IT
    BoundingBox GetBoundingBox();

    // static function to get/set collision table state
    static CollisionState GetCollisionTableState(Group group1, Group group2);
    static void SetCollisionTableState(Group group1, Group group2, CollisionState state);
    static std::vector<Handlet<Entity>> CastRay(Vec2 point, Vec2 direction, unsigned *count);

    // static vector of all manifolds of all collisions
    static std::vector<Manifold> manifolds;

    // number of iterations to carry out per frame
    static unsigned physicsIterations;

    // bool to check whether on screen or not
    bool onScreen;

    // collision mask
    Group group;

    // linear components
    Vec2 totalImpulse;   // sum of all impulses at the end of a step
    Vec2 totalForce;     // forces that need to be multiplied by dt

  private:
    // collision functions
    void PolygonToPolygon(Manifold &result, RigidBody &rhs);
    void PolygonToCircle(Manifold &result, RigidBody &rhs);
    void CircleToPolygon(Manifold &result, RigidBody &rhs);
    void CircleToCircle(Manifold &result, RigidBody &rhs);

    // ref/incident face functions
    bool findReferenceFace(Face &referenceFace, Vec2 collisionNormal);
    bool findIncidentFace(Face &incidentFace, const Face &referenceFace, Vec2 collisionNormal);

    // clipping algorithm
    static unsigned Clip(Vec2 referenceNormal, float distOriginToFace, Face &inputFace, Face &contactPoints);
    
    // checks for collision with other rigid body and returns a manifold
    Manifold narrowCheck(RigidBody &rhs);

    // returns the penetration
    float findAxisOfLeastPenetration(const RigidBody &bodyB, Manifold &result);

    // calls the appropriate collision function based on shape type
    void ResolveCollision(Manifold &result, RigidBody &rhs);

    // switches
    Property::Bool fixed;           // switch to set whether object moves or not
    Property::Bool rotationLocked;  // switch to set whether object rotates or not
    Property::Bool ghost;           // switch to set whether object should collide or not

    // rotational components
    float torque;       // sum of all additional rotational additions at the end of a step

    // gravitational scale
    Property::Float gravityScale;

    // pointer to its shape
    Shape *shape;

    // matrix to hold its rotation and scale
    Matrix3 u;
  };
}