/******************************************************************************
Filename: PropertyContainer.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "Property.h"

namespace Omni
{
  struct PropertyContainer
  {
    std::string name;
    Property::Type type;
    union
    {
      Property::Bool data_bool;
      Property::Unsigned data_unsigned;
      Property::Int data_int;
      Property::Float data_float;
    };
    Property::String data_string;
    Property::Vec2 data_vec2;
    Property::Color data_color;
    Property::Vec2List data_vec2List;

    PropertyContainer() : type(Property::Type::Undefined) {}
    PropertyContainer(std::string name_, Property::Type type_) : name(name_), type(type_) {}

    static PropertyContainer CreateBool(std::string name, Property::Bool data)
    {
      PropertyContainer pc(name, Property::Type::Bool);
      pc.data_bool = data;
      return pc;
    }
    static PropertyContainer CreateUnsigned(std::string name, Property::Unsigned data)
    {
      PropertyContainer pc(name, Property::Type::Unsigned);
      pc.data_unsigned = data;
      return pc;
    }
    static PropertyContainer CreateInt(std::string name, Property::Int data)
    {
      PropertyContainer pc(name, Property::Type::Int);
      pc.data_int = data;
      return pc;
    }
    static PropertyContainer CreateFloat(std::string name, Property::Float data)
    {
      PropertyContainer pc(name, Property::Type::Float);
      pc.data_float = data;
      return pc;
    }
    static PropertyContainer CreateString(std::string name, Property::String data)
    {
      PropertyContainer pc(name, Property::Type::String);
      pc.data_string = data;
      return pc;
    }
    static PropertyContainer CreateVec2(std::string name, Property::Vec2 data)
    {
      PropertyContainer pc(name, Property::Type::Vec2);
      pc.data_vec2 = data;
      return pc;
    }
    static PropertyContainer CreateVec2List(std::string name, Property::Vec2List data)
    {
      PropertyContainer pc(name, Property::Type::Vec2List);
      pc.data_vec2List = data;
      return pc;
    }
    static PropertyContainer CreateColor(std::string name, Property::Color data)
    {
      PropertyContainer pc(name, Property::Type::Color);
      pc.data_color = data;
      return pc;
    }
  };
}