/******************************************************************************
Filename: HitBox.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "HitboxManager.h"
#include "Game.h"

#define _PHYSICSDEBUG 1

namespace Omni {

  D3DXCOLOR HitBox::GetColor() const
  {
    switch (type)
    {
    case Type::Body:
      return D3DXCOLOR(0, 1.0f, 0, 1.0f);
    case Type::Damage:
      return D3DXCOLOR(1.0f, 0, 0, 1.0f);
    case Type::Block:
      return D3DXCOLOR(0, 0, 1.0f, 1.0f);
    }
    return D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
  }

  CircleHitBox::CircleHitBox(const Vec2 &offset, float radius_) : HitBox(), radius(radius_)
  {
    SetRelativePosition(offset);
    SetShape(Shape::Circle);
  }

  AABBHitBox::AABBHitBox(const Vec2 &offset, float hw, float hh) : HitBox(), halfHeight(hh), halfWidth(hw)
  {
    SetRelativePosition(offset);
    SetShape(Shape::Box);
  }

  void HitBox::SetManager(HitBoxManager * managerHandle_)
  { 
    managerHandle = managerHandle_->selfPtr;
  }

  HitBoxManager *HitBox::GetManager() const
  {
    return reinterpret_cast<HitBoxManager *>(*managerHandle);
  }

  bool HitBox::AABBtoAABBCheck(const AABBHitBox &lhs, const AABBHitBox &rhs)
  {
    Vec2 lhsOwnerPosition = lhs.GetManager()->GetOwner()->transform.translation;
    Vec2 rhsOwnerPosition = rhs.GetManager()->GetOwner()->transform.translation;

    Vec2 lhsOwnerScale = lhs.GetManager()->GetOwner()->transform.scale;
    Vec2 rhsOwnerScale = rhs.GetManager()->GetOwner()->transform.scale;

    Vec2 lhsWorldPosition = { lhsOwnerPosition.x + lhs.GetRelativePosition().x * lhsOwnerScale.x,
      lhsOwnerPosition.y + lhs.GetRelativePosition().y * lhsOwnerScale.y };

    Vec2 rhsWorldPosition = { rhsOwnerPosition.x + rhs.GetRelativePosition().x * rhsOwnerScale.x,
      rhsOwnerPosition.y + rhs.GetRelativePosition().y * rhsOwnerScale.y };

    float scaledWidth = lhsOwnerScale.x * lhs.GetHalfWidth();
    float scaledHeight = lhsOwnerScale.y * lhs.GetHalfHeight();

    float rhsScaledWidth = rhsOwnerScale.x * rhs.GetHalfWidth();
    float rhsScaledHeight = rhsOwnerScale.y * rhs.GetHalfHeight();

    float lhsMax = lhsWorldPosition.x + scaledWidth;
    float lhsMin = lhsWorldPosition.x - scaledWidth;

    float rhsMax = rhsOwnerPosition.x + rhsScaledWidth;
    float rhsMin = rhsOwnerPosition.x - rhsScaledWidth;

#if _PHYSICSDEBUG
    auto &game = Game::Instance();
    game.GetDebugDraw()->DrawQuad(lhsWorldPosition, Vec2(2 * scaledWidth, 2 * scaledHeight), 0, lhs.GetManager()->GetOwner()->space->index, lhs.GetColor());
    game.GetDebugDraw()->DrawQuad(rhsWorldPosition, Vec2(2 * rhsScaledWidth, 2 * rhsScaledHeight), 0, lhs.GetManager()->GetOwner()->space->index, rhs.GetColor());
#endif


    // left right check
    if (lhsWorldPosition.x + scaledWidth < rhsWorldPosition.x - rhsScaledWidth ||
      rhsWorldPosition.x + rhsScaledWidth < lhsWorldPosition.x - scaledWidth)
      return false;

    // up down check
    if (lhsWorldPosition.y + scaledHeight < rhsWorldPosition.y - rhsScaledHeight ||
      rhsWorldPosition.y + rhsScaledHeight < lhsWorldPosition.y - scaledHeight)
      return false;

    return true;
  }

  bool HitBox::AABBtoCircleCheck(const AABBHitBox &lhs, const CircleHitBox &rhs)
  {
    Vec2 lhsOwnerPosition = lhs.GetManager()->GetOwner()->transform.translation;
    Vec2 rhsOwnerPosition = rhs.GetManager()->GetOwner()->transform.translation;

    Vec2 lhsOwnerScale = lhs.GetManager()->GetOwner()->transform.scale;
    Vec2 rhsOwnerScale = rhs.GetManager()->GetOwner()->transform.scale;

    Vec2 lhsWorldPosition = { lhsOwnerPosition.x + lhs.GetRelativePosition().x * lhsOwnerScale.x,
      lhsOwnerPosition.y + lhs.GetRelativePosition().y * lhsOwnerScale.y };

    Vec2 rhsWorldPosition = { rhsOwnerPosition.x + rhs.GetRelativePosition().x * rhsOwnerScale.x,
      rhsOwnerPosition.y + rhs.GetRelativePosition().y * rhsOwnerScale.y };

    float scaledWidth = lhsOwnerScale.x * lhs.GetHalfWidth();
    float scaledHeight = lhsOwnerScale.y * lhs.GetHalfHeight();

    float rhsScaledRadius = rhsOwnerScale.x * rhs.GetRadius();

    
#if _PHYSICSDEBUG
    auto &game = Game::Instance();
    game.GetDebugDraw()->DrawQuad(lhsWorldPosition, Vec2(2 * scaledWidth, 2 * scaledHeight), 0, lhs.GetManager()->GetOwner()->space->index, lhs.GetColor());

    game.GetDebugDraw()->DrawPoint(rhsWorldPosition + Vec2(rhsScaledRadius, 0), rhs.GetManager()->GetOwner()->space->index, rhs.GetColor());
    game.GetDebugDraw()->DrawPoint(rhsWorldPosition + Vec2(-rhsScaledRadius, 0), rhs.GetManager()->GetOwner()->space->index, rhs.GetColor());
    game.GetDebugDraw()->DrawPoint(rhsWorldPosition + Vec2(0, rhsScaledRadius), rhs.GetManager()->GetOwner()->space->index, rhs.GetColor());
    game.GetDebugDraw()->DrawPoint(rhsWorldPosition + Vec2(0, -rhsScaledRadius), rhs.GetManager()->GetOwner()->space->index, rhs.GetColor());
#endif

    // left right check
    if (lhsWorldPosition.x + scaledWidth < rhsWorldPosition.x - rhsScaledRadius ||
      rhsWorldPosition.x + rhsScaledRadius < lhsWorldPosition.x - scaledWidth)
      return false;

    // up down check
    if (lhsWorldPosition.y + scaledHeight < rhsWorldPosition.y - rhsScaledRadius ||
      rhsWorldPosition.y + rhsScaledRadius < lhsWorldPosition.y - scaledHeight)
      return false;

    return true;
  }

  bool HitBox::CircletoCircleCheck(const CircleHitBox &lhs, const CircleHitBox &rhs)
  {
    Vec2 lhsOwnerPosition = lhs.GetManager()->GetOwner()->transform.translation;
    Vec2 rhsOwnerPosition = rhs.GetManager()->GetOwner()->transform.translation;

    Vec2 lhsOwnerScale = lhs.GetManager()->GetOwner()->transform.scale;
    Vec2 rhsOwnerScale = rhs.GetManager()->GetOwner()->transform.scale;

    Vec2 lhsWorldPosition = { lhsOwnerPosition.x + lhs.GetRelativePosition().x * lhsOwnerScale.x,
      lhsOwnerPosition.y + lhs.GetRelativePosition().y * lhsOwnerScale.y };

    Vec2 rhsWorldPosition = { rhsOwnerPosition.x + rhs.GetRelativePosition().x * rhsOwnerScale.x,
      rhsOwnerPosition.y + rhs.GetRelativePosition().y * rhsOwnerScale.y };

    float lhsScaledRadius = lhsOwnerScale.x * lhs.GetRadius();
    float rhsScaledRadius = rhsOwnerScale.x * rhs.GetRadius();

    Vec2 relativePosition = rhsWorldPosition - lhsWorldPosition;

    if ((lhsScaledRadius + rhsScaledRadius) *(lhsScaledRadius + rhsScaledRadius) > relativePosition.LengthSquared())
      return false;

    return true;
  }

  int HitBox::ResolveCollision(const HitBox *rhs)
  {
    bool collision = false;

    // AABB to AABB
    if (GetShape() == Shape::Box && rhs->GetShape() == Shape::Box)
    {
      collision = AABBtoAABBCheck(*(AABBHitBox *)this, *(AABBHitBox *)rhs);
    }

    // AABB to Circle
    if (GetShape() == Shape::Box && rhs->GetShape() == Shape::Circle)
    {
      collision = AABBtoCircleCheck(*(AABBHitBox *)this, *(CircleHitBox *)rhs);
    }

    // Circle to AABB
    if (GetShape() == Shape::Circle && rhs->GetShape() == Shape::Box)
    {
      collision = AABBtoCircleCheck(*(AABBHitBox *)rhs, *(CircleHitBox *)this);
    }

    // Circle to Circle
    if (GetShape() == Shape::Circle && rhs->GetShape() == Shape::Circle)
    {
      collision = CircletoCircleCheck(*(CircleHitBox *)this, *(CircleHitBox *)rhs);
    }

    // if no collision, do nothing
    if (collision == false)
      return 0;

    // if yes collision, push the necessary information to the manager
    size_t otherID = rhs->GetManager()->GetOwner()->id;
    GetManager()->PushNewCollision(otherID, GetType(), rhs->GetType());

    return int(rhs->GetType());
  }
}

