/******************************************************************************
Filename: HitBox.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "Vec2.h"
#include "Handlet.h"

static const unsigned MaxAnimationFrames = 8;

namespace Omni
{
  class HitBoxManager;
  class Component;

  struct CircleHitBoxInfo
  {
    float radius;
  };

  struct AABBHitBoxInfo
  {
    float halfWidth;
    float halfHeight;
  };

  class AABBHitBox;
  class CircleHitBox;

  class HitBox
  {
  public:
    enum class Shape
    {
      Circle,
      Box
    };

    enum class Type
    {
      Body = 1,
      Damage = 2,
      Block
    };

    int HitBox::ResolveCollision(const HitBox *rhs);
    static bool AABBtoAABBCheck(const AABBHitBox &lhs, const AABBHitBox &rhs);
    static bool CircletoCircleCheck(const CircleHitBox &lhs, const CircleHitBox &rhs);
    static bool AABBtoCircleCheck(const AABBHitBox &lhs, const CircleHitBox &rhs);
    inline Shape GetShape() const { return shape; }
    inline void SetShape(Shape newType) { shape = newType; }
    inline Type GetType() const { return type; }
    inline void SetType(Type newType) { type = newType; }
    D3DXCOLOR GetColor() const;

    inline void SetRelativePosition(Vec2 newPos) { relativePosition = newPos; }
    
    inline Vec2 GetRelativePosition() const { return relativePosition; }

    HitBoxManager *GetManager() const;
    void SetManager(HitBoxManager * managerHandle_); 

  private:
    Type type;
    Shape shape;
    Vec2 relativePosition;
    Handlet<Component> managerHandle;
  };

  class CircleHitBox : public HitBox
  {
  public:
    CircleHitBox(const Vec2 &offset, float radius_);
    float GetRadius() const { return radius; }
    void AnimationStep();

  private:
    float radius;
    CircleHitBoxInfo animationInfo[MaxAnimationFrames];
  };

  class AABBHitBox : public HitBox
  {
  public:
    AABBHitBox(const Vec2 &offset, float hw, float hh);

    void AnimationStep();

    float GetHalfWidth() const { return halfWidth; }
    float GetHalfHeight() const { return halfHeight; }

    void SetHalfWidth(float newHW) { halfWidth = newHW; }
    void SetHalfHeight(float newHH) { halfHeight = newHH; }

  private:
    // AABBHitBoxInfo animationInfo[MaxAnimationFrames];
    float halfWidth;
    float halfHeight;
  };
}