/******************************************************************************
Filename: Camera.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "stdafx.h"
#include "Scriptable.h"
#include "Game.h"

namespace Omni
{
  class Transform;
  struct EntityHandle;

  class Camera : public Scriptable<Camera>
  {
  public:
    Camera()
    {
      shakeOffset.x = 0.0f;
      shakeOffset.y = 0.0f;
      shakeOffset.z = 0.0f;
    };

    Transform transform;

    float GetSize() const
    {
      return _size;
    }
    void SetSize(float __size)
    {
      _size = max(__size, 0.001f);
    }
    __declspec(property(get = GetSize, put = SetSize)) float size;

    Transform GetScaledTransform() const;

    static void v8_GetTransform(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);

    void Update(float dt);
    void Shake(float distance, float duration, float zoom, float rotation);

    Vec2 velocity;
    Vec2 offset;
    Handlet<Entity> followingEntity = nullptr;
    void Follow(Handlet<Entity> entity);
    void StopFollowing();
    void SetOffset(float x, float y, float time = 0.0f);
    void Pan(Vec2 vec, float duration);
    void StopPanning();
    void Zoom(float scale, float duration);
    void StopZooming();

    static void v8_Register();

  private:
    float _size = 15.0f;
    struct
    {
      float x = 0.0f;
      float y = 0.0f;
      float z = 0.0f;
    } shakeOffset;
    float shakeRotation = 0.0f;
    float shakeAngle = 0.0f;
    float shakeMagnitude = 0.0f;
    float shakeZoom = 0.0f;
    float shakeTime = 0.0f;
    float shakeDuration = 0.0f;
    
  };
} //Namespace Omni
