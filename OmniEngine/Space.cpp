/******************************************************************************
Filename: Space.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Space.h"
#include "Entity.h"
#include "Game.h"
#include "Component.h"
#include <algorithm>
#include <iostream>
#include "Input.h"
#include "Sprite.h"

namespace Omni
{
  Space::Space(std::string name_) : name(name_) 
  { 
    entities.reserve(1024); 
    dispatcher = new EventDispatcher;
    dispatcher->type = EventDispatcher::Type::Space;
  }

  Space::~Space()
  {
    for (auto it = entities.begin(); it != entities.end(); ++it)
    {
      if (it->active)
      {
        it->Destroy();
        delete it->selfPtr;
        /*it->destroying = true;
        destroyingEntities.push_back(it->handle);*/
      }
    }
    Cleanup();
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    if (tweakBar != nullptr)
      TwDeleteBar(tweakBar);
#endif
  }

  std::vector<Handlet<Entity> >Space::GetEntitiesByName(std::string name)
  {
    std::vector<Handlet<Entity> > result;

    int numEntities = entities.size();
    for (int i = 0; i < numEntities; ++i)
    {
      Entity currentEntity = entities[i];
      if (currentEntity.active && currentEntity.name == name)
        result.push_back(currentEntity.selfPtr);
    }

    return result;
  }

  std::vector<Handlet<Entity> >Space::GetAllEntityHandles()
  {
    std::vector<Handlet<Entity> > result;

    int numEntities = entities.size();
    for (int i = 0; i < numEntities; ++i)
      if (entities[i].active)
        result.push_back(entities[i].selfPtr);

    return result;
  }

  Vec2 Space::GetMousePosition()
  {
    Vec2 windowSize = Game::Instance().GetWindowSystem()->GetClientSize();
    float cWidth = 2 * windowSize.x / windowSize.y;
    float cHeight = 2;

    Matrix3 unProject(2 / windowSize.x, -2 / windowSize.y);
    Matrix3 trans(Vec2(-1.0f, 1.0f));
    Matrix3 cameraMatrix = camera.GetScaledTransform().GetMatrix3();
    Matrix3 orthographic(cWidth / 2, cHeight / 2);

    int mouseX = int(Input::mouse.position.x);
    float f_mouseX = float(mouseX);

    int mouseY = int(Input::mouse.position.y);
    float f_mouseY = float(mouseY);

    Vec2 mousePoint = { f_mouseX, f_mouseY };
    Vec2 mousePosInSpace = cameraMatrix * orthographic * trans * unProject * mousePoint;

    //std::cout << "MP: " << mousePosInSpace.x << ", " << mousePosInSpace.y << std::endl;

    return mousePosInSpace;
  }

  Vec2 Space::GetPointInSpace(const Vec2 &point)
  {
    Vec2 windowSize = Game::Instance().GetWindowSystem()->GetClientSize();
    float cWidth = 2 * windowSize.x / windowSize.y;
    float cHeight = 2;

    Matrix3 unProject(2 / windowSize.x, -2 / windowSize.y);
    Matrix3 trans(Vec2(-1.0f, 1.0f));
    Matrix3 cameraMatrix = camera.GetScaledTransform().GetMatrix3();
    Matrix3 orthographic(cWidth / 2, cHeight / 2);

    return cameraMatrix * orthographic * trans * unProject * point;
  }

  bool Space::QueryOnScreen(Entity *target)
  {
    float size = camera.GetSize();
    Vec2 cPos = camera.transform.translation;
    auto &game = Game::Instance();
    Vec2 window = game.GetWindowSystem()->GetClientSize();

    float screenHalfWidth = (size * window.x / window.y) / 2.0f;
    float screenHalfHeight = size / 2.0f;

    Transform trans = target->transform;
    Vec2 finalScale = trans.scale;
    Sprite *sprite = reinterpret_cast<Sprite *>(*target->GetComponent("Sprite"));

    if (sprite == nullptr)
      return true;

    Image *image = sprite->GetImagePointer();

    if (image == nullptr)
      return true;

    finalScale.x *= image->dimensions.x / image->pixelsPerUnit;
    finalScale.y *= image->dimensions.y / image->pixelsPerUnit;

    Vec2 finalTranslation = trans.translation;
    finalTranslation.x -= image->offset.x * finalScale.x;
    finalTranslation.y -= image->offset.y * finalScale.y;

    float ownerXSize = finalScale.x / 2;
    float ownerYSize = finalScale.y / 2;

    float totalX = screenHalfWidth + ownerXSize;
    float totalY = screenHalfHeight + ownerYSize;

    float totalRadiusSquared = totalX * totalX + totalY * totalY;

    float separationSquared = (finalTranslation - cPos).LengthSquared();

    // if it's outside the screen, don't draw
    if (separationSquared > totalRadiusSquared)
      return false;

    return true;
  }

  Handlet<Entity> Space::CreateEntity(std::string name)
  {
    using namespace v8;

    auto &game = Game::Instance();
    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    // if no vacancies, just create a new one
    if (vacancies.empty())
    {
      size_t entCap = entities.capacity();
      entities.push_back(Entity(this, name));

      auto &entityToUpdate = entities.back();
      entityToUpdate.selfPtr = new Entity *(&entityToUpdate);
      entityToUpdate.destroying = false;
      if (entities.capacity() > entCap)
      {
        // update all selfPtrs of entities
        for (auto &ent : entities)
          if (ent.active)
            *ent.selfPtr = &ent;
      }

      // update the clickable's handle
      entityToUpdate.clickable.ownerHandle = entityToUpdate.selfPtr;

      // update the dispatcher's owner id
      entityToUpdate.dispatcher->ownerID = entityToUpdate.id;
      if (name == "")
        name = std::string("unnamed_") + std::to_string(entityToUpdate.id);
      entityToUpdate.name = name;

      game.entityHandles.insert({ entityToUpdate.id, Handlet<Entity>(entityToUpdate.selfPtr) });
      namedEntities.insert({ name, Handlet<Entity>(entityToUpdate.selfPtr) });
      return Handlet<Entity>(entityToUpdate.selfPtr);
    }

    // if yes vacancies, create in place
    size_t ind = vacancies.front();
    vacancies.pop_front();
    Entity prevEnt = entities[ind];
    Entity **selfPtr = prevEnt.selfPtr;
    entities[ind] = Entity(this, name);

    auto &entityToUpdate = entities[ind];
    entityToUpdate.selfPtr = selfPtr;
    *entityToUpdate.selfPtr = &entityToUpdate;
    entityToUpdate.destroying = false;

    // update the clickable's handle
    entityToUpdate.clickable.ownerHandle = entityToUpdate.selfPtr;
    // update the dispatcher's owner id
    entityToUpdate.dispatcher->ownerID = entityToUpdate.id;
    if (name == "")
      name = std::string("unnamed_") + std::to_string(entityToUpdate.id);
    entityToUpdate.name = name;
    namedEntities.insert({ name, Handlet<Entity>(entityToUpdate.selfPtr) });
    game.entityHandles.insert({ entityToUpdate.id, Handlet<Entity>(entityToUpdate.selfPtr) });
    return Handlet<Entity>(entityToUpdate.selfPtr);
  }

  Handlet<Entity> Space::CreateEntityAtPosition(std::string name, Vec2 position, std::string archetype)
  {
    Handlet<Entity> created = CreateEntity(name);
    created->transform.translation = position;
    if (archetype != "")
    {
      created->ApplyArchetype(archetype);
    }

    created->transform.UpdateLastTransformData();
    return created;
  }

  Handlet<Entity> Space::GetEntityByName(std::string name)
  {
    auto result = namedEntities.find(name);
    if (result == namedEntities.end())
      return nullptr;
    return GetEntityByHandle(result->second);
  }

  Handlet<Entity> Space::GetEntityByHandle(Handlet<Entity> handle)
  {
    return handle;
  }

  void Space::MarkEntityForDeletion(Entity *entity)
  {
    if (entity->name == "")
      return;

    for (auto it = namedEntities.begin(); it != namedEntities.end(); ++it)
      if (*it->second->selfPtr == entity)
      {
        namedEntities.erase(it);
        break;
      }
  }

  void Space::Cleanup()
  {
  }

  void Space::SpawnCreatedEntities()
  {
    for (auto const &ent : creatingEntities)
    {
      entities.push_back(ent);
    }
    creatingEntities.clear();
  }

  void Space::SpawnTweakBar()
  {
#if !defined(_DEBUG) && !defined(_RELEASE)
    return;
#endif
    if (tweakBar != nullptr)
      return;
    tweakBar = TwNewBar(std::string(std::string("[S] ") + name).c_str());

    TwAddVarCB(tweakBar, "_EntityCount", TW_TYPE_UINT16, NULL,
      [](void *value, void *clientData)
    {
      *static_cast<unsigned *>(value) = static_cast<Space *>(clientData)->GetEntityCount();
    },
      this,
      " label=EntityCount"
      );

    TwAddSeparator(tweakBar, NULL, NULL);

    TwAddButton(tweakBar, "_Camera", NULL, NULL, " label=Camera");
    TwAddVarCB(tweakBar, "_PosX", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      static_cast<Space *>(clientData)->camera.transform.translation.x = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      *static_cast<float *>(value) = static_cast<Space *>(clientData)->camera.transform.translation.x;
    },
      this,
      " label=X step=0.001 group=Translation"
      );
    TwAddVarCB(tweakBar, "_PosY", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      static_cast<Space *>(clientData)->camera.transform.translation.y = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      *static_cast<float *>(value) = static_cast<Space *>(clientData)->camera.transform.translation.y;
    },
      this,
      " label=Y step=0.001 group=Translation"
      );

    TwAddVarCB(tweakBar, "_OffX", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      static_cast<Space *>(clientData)->camera.offset.x = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      *static_cast<float *>(value) = static_cast<Space *>(clientData)->camera.offset.x;
    },
      this,
      " label=X step=0.001 group=Offset"
      );
    TwAddVarCB(tweakBar, "_OffY", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      static_cast<Space *>(clientData)->camera.offset.y = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      *static_cast<float *>(value) = static_cast<Space *>(clientData)->camera.offset.y;
    },
      this,
      " label=Y step=0.001 group=Offset"
      );

    TwAddVarCB(tweakBar, "_Rotation", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      static_cast<Space *>(clientData)->camera.transform.rotation = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      *static_cast<float *>(value) = static_cast<Space *>(clientData)->camera.transform.rotation;
    },
      this,
      " label=Rotation step=0.001"
      );

    TwAddVarCB(tweakBar, "_Depth", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      static_cast<Space *>(clientData)->depth = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      *static_cast<float *>(value) = static_cast<Space *>(clientData)->depth;
    },
      this,
      " label=Depth step=0.01"
      );

    TwAddVarCB(tweakBar, "_Size", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      static_cast<Space *>(clientData)->camera.size = max(*static_cast<const float *>(value), 0.001f);
    },
      [](void *value, void *clientData)
    {
      *static_cast<float *>(value) = static_cast<Space *>(clientData)->camera.size;
    },
      this,
      " label=Size step=0.1"
      );

    TwAddSeparator(tweakBar, NULL, NULL);
    TwAddButton(tweakBar, "Destroy", [](void *clientData)
    {
      Game::Instance().DestroySpace(reinterpret_cast<Space *>(clientData)->name);
    }, this, NULL);
  }

  void Space::Serialize(Json::Value &root) const
  {
    //Console::Info("", "Serializing SPACE #" + std::to_string(index) + " (" + name + ")");
    root["name"] = name;
    if (Game::Instance().serializeCamera)
    {
      root["camera"]["translation"]["x"] = camera.transform.translation.x;
      root["camera"]["translation"]["y"] = camera.transform.translation.y;
      root["camera"]["rotation"] = camera.transform.rotation;
      root["camera"]["size"] = camera.size;
    }

    else
    {
      root["camera"]["translation"]["x"] = initialCamera.transform.translation.x;
      root["camera"]["translation"]["y"] = initialCamera.transform.translation.y;
      root["camera"]["rotation"] = initialCamera.transform.rotation;
      root["camera"]["size"] = initialCamera.size;
    }

    if (!parallaxLayers.empty())
    {
      root["parallax"] = Json::objectValue;
      root["parallax"]["layers"] = Json::arrayValue;
      unsigned i = 0;
      for (auto const layer : parallaxLayers)
      {
        root["parallax"]["layers"].append(Json::arrayValue);
        root["parallax"]["layers"][i][0] = std::get<0>(layer);
        root["parallax"]["layers"][i][1] = std::get<1>(layer);
        root["parallax"]["layers"][i][2] = std::get<2>(layer);
        ++i;
      }
    }

    root["entities"] = Json::arrayValue;
    size_t i = 0;
    for (auto const &entity : entities)
    {
      if (!entity.active)
        continue;
      root["entities"].append(Json::objectValue);
      entity.Serialize(root["entities"][i++]);
    }
  }

}
