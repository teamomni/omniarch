/******************************************************************************
Filename: HitboxManager.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "Component.h"
#include <string>
#include "HitBox.h"
#include "Event.h"

namespace Omni
{
  class HitBoxManager : public SystemComponent<HitBoxManager>
  {
  public:
    static void HitBoxStep(float dt);
    HitBoxManager();
    ~HitBoxManager();
    // void PlayAnimation(std::string name);

    // required system functions
    virtual void Initialize();
    virtual void Update(float dt);
    virtual void Destroy();

    // register
    static void HitBoxManager::Register();

    // adding hitboxes
    void AddBox(const Vec2 &offset, float hw, float hh, HitBox::Type type);
    void AddCircle(const Vec2 &offset, float radius, HitBox::Type type);

    inline void Clear() 
    { 
      for (auto it = hitBoxes.begin(); it != hitBoxes.end(); ++it)
      {
        delete *it;
      }
      hitBoxes.clear();
    }

    // push new collision into current frame collisions
    void PushNewCollision(size_t receiverID, HitBox::Type receiverType, HitBox::Type dispatcherType);

    // clear collisions
    void ClearCollisions();

    std::vector <HitBox *>hitBoxes;
    std::vector<Events::HitEvent>collisionStarted;
    std::vector<Events::HitEvent>collisionPersisted;
    std::vector<Events::HitEvent>collisionEnded;
    std::vector<Events::HitEvent> currentFrameCollisions;

    // linked list of iterators to clear
    std::list<Events::HitEvent> eventsToClear;

    std::function<void(HitBoxManager *, int)> damageCallback;

  private:
    
    // float currentAnimationTimer;
    // unsigned currentFrame;
    // HitBoxAnimation currentAnimation;
    // unordered map of animations
  };
}
