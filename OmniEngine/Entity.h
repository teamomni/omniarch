/******************************************************************************
Filename: Entity.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include <vector>
#include <string>
#include <unordered_map>
#include <type_traits>
#include "Transform.h"
#include "v8.h"
#include "Scriptable.h"
#include "EventDispatcher.h"
#include "Property.h"
#include "Archetype.h"
#include "Clickable.h"
#include "ISerializable.h"
#include "Handlet.h"

namespace Omni
{
  class Component;
  class UserComponent;
  class Space;
  struct EntityHandle;
  class Transform;
  class ParticleEmitter;

  class Entity : public Scriptable<Entity>, public ISerializable
  {
  public:
    Entity() {};
    Entity(Space *space_, size_t handle_);
    Entity(Space *space_, std::string name_, size_t handle_ = 0);
    ~Entity() { ScriptableDestroy(); }

    size_t id;

    Transform transform;
    Space *space;
    std::string name;
    std::string archetype;
    bool active = false;
    bool destroying = false; // used for scripting destroy
    bool isCreating = true;

    Entity **selfPtr;
    std::map<std::string, Handlet<Component>> components;

    Handlet<Component> AddComponentWhileEditing(std::string name);

    Handlet<Component> AddComponent(std::string name);

    void Initialize();

    inline bool HasComponent(std::string name)
    {
      return components.count(name) != 0;
    }
    inline Handlet<Component> GetComponent(std::string name)
    {
      if (components.count(name) != 0)
        return components[name];
      return Handlet<Component>(nullptr);
    }
    void RemoveComponent(std::string name);

    void ApplyArchetype(std::string archetypeName);

    EntityHandle *MakeHandle();

    /*template<typename Derived>
    Derived *GetSystem(std::string name)
    {
      static_assert <std::is_base_of<ComponentBase, Derived>::value();
      return NULL;
    }*/

    // v8 gettors from entity
    static void v8_GetRigidBody(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);
    static void v8_GetSprite(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);
    static void v8_GetAnimator(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);
    static void v8_GetSpace(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);
    static void v8_GetTransform(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);
    static void v8_GetClickable(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);
    static void v8_GetSoundEmitter(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);
    static void v8_GetParticleEmitter(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);
    static void v8_GetBlurEffect(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);
    static void v8_GetBloomEffect(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);
    static void v8_GetUserComponent_CPP(const v8::FunctionCallbackInfo<v8::Value> &info);
    static void v8_GetUserComponent(const v8::FunctionCallbackInfo<v8::Value> &info);

    static void v8_Register();

    // string to hold the tweakbar component name
    std::string componentToCreate;

    // function that creates a template and returns it
    v8::Handle<v8::Object> v8_Createv8Entity();

    // Event dispatcher class
    EventDispatcher *dispatcher;

    // inherent clickable property
    Clickable clickable;

    // function to set the link ID
    size_t GetLinkID();
    void SetLinkID(size_t linkID_);

    void Destroy();

    void SpawnTweakBar();
    void FillTweakBar();
    void AddTweakBarProperty(std::string compName, PropertyHandle *propHandle, Property::Type propType);
    void DeleteTweakBar();

    static void ResetIDCounting()
    {
      nextID = 0;
    }

    virtual void Serialize(Json::Value &root) const;

  private:
    TwBar *tweakBar = nullptr;
    static size_t nextID;
    static size_t GenerateID()
    {
      return nextID++;
    }

	  // hidden ID for entity path stuff
    static size_t nextlinkID;
    size_t linkID;
    static size_t generateLinkID()
	  {
      return ++nextlinkID;
	  }

  };

}
