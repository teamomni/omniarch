/******************************************************************************
Filename: SoundEmitter.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "stdafx.h"
#include "Component.h"
#include "SoundSystem.h"

namespace Omni
{

  class SoundEmitter : public SystemComponent <SoundEmitter>
  {
    public:
      ~SoundEmitter() { ScriptableDestroy(); }

      virtual void Initialize();
      virtual void Update(float dt);
      virtual void Destroy();
      static void Register();
      static void v8_Register();

      Property::Bool p_StartPlaying = false;
      Property::Bool p_Pause = false;
      Property::String p_SoundCue = "";
      Property::Float p_Pitch = 1.0f;
      Property::Float p_Volume = 1.0f;
      Property::Bool p_Loop = false;
      Property::Bool p_AffectedByTimeScale = true;
      Property::Bool p_IsMusic = true;

      //In the future make some system to
      //dynamically put parameters into tweak bar
      Property::Bool p_ShowParameterBar;

      void SetParameters();
      //float GetParameter(std::string);
      void SetSoundCue();
      std::vector<float> m_paramVals;
      std::string m_oldName = "";

      void PlaySound(std::string cue);

      static bool playSoundEffects;
      static bool playMusic;


    private:
      SoundUtilities::SoundCue* m_soundCue = nullptr;

      bool m_paramBarVisible = false;
      TwBar *m_paramBar = NULL;
      std::string barName;

      void PlaySound();
      void TogglePause(bool);
      void SetPitch(float);
      void SetVolume(float);
      void StopSound();

      void MakeTweakBar();
      void RemoveHandles();
  };

} //Namespace Omni
