/******************************************************************************
Filename: EventDispatcher.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "EventDispatcher.h"
#include <list>
#include "Game.h"
#include "Entity.h"

namespace Omni
{
  int EventListener::Call(v8::Local<v8::Object> eventObject)
  {
    auto &game = Game::Instance();

    using namespace v8;
   
    Isolate::Scope isoScope(game.isolate);
    Locker locker(game.isolate);
    HandleScope handleScope(game.isolate);

    // get the object that's calling this function
    Local<Object> localObject = Local<Object>::New(game.isolate, object);

    Local<Function> localFunction = Local<Function>::New(game.isolate, eventCallback);

    // get the owner pointer
    Handlet<Entity> owner = game.GetEntityByID(ownerID);

    // only call if owner is valid

    if (owner.isValid())
    {
      // set the currently updating object in game
      game.currentlyUpdatingIDs.push(static_cast<int>(ownerID));

      if (game.debugMessageSetting == DebugMessageSetting::All ||
        game.debugMessageSetting == DebugMessageSetting::EventDispatch)
      {
        Console::Write("Event dispatching to ");
        Console::WriteLine(owner->name);
      }

      Handle<External> ownerPtr = External::New(game.isolate, owner->selfPtr);

      // get the owner object
      Local<Object> ownerObj = owner->v8_GetObject();
      ownerObj->SetInternalField(0, ownerPtr);

      Local<Value> args[2] = { ownerObj, eventObject };

      // std::cout << "inside id is " << ownerID << std::endl;

      //if (owner.isValid())
        //printf("VALID %d\n", ownerID);

      // call the actual function
      localFunction->Call(localObject, 2, args);

      // std::cout << "after updating, inside id is " << ownerID << std::endl;

      // once done, pop the current updating id off
      game.currentlyUpdatingIDs.pop();

      // make sure to check that it's valid first
      // now that JS code has actually been executed, check destroying
      // if object is now destroying, destroy it
      if (owner.isValid() && owner->destroying)
      {
        //owner->Destroy();
        return owner->id;
      }
    }
    return -1;
  }


  void EventDispatcher::Destroy()
  {
    using namespace v8;
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    for (auto &pair : eventListeners)
    {
      auto &list = pair.second;
      for (auto &node : list)
      {
        node.Destroy();
      }
    }

    eventListeners.clear();
  }

  // connect new event
  void EventDispatcher::ConnectEvent(std::string eventName, v8::Local<v8::Function> eventCallback, v8::Local<v8::Object> object_, Entity *owner)
  {
    using namespace v8;

    auto &game = Game::Instance();

    Isolate::Scope isoScope(game.isolate);
    Locker locker(game.isolate);
    HandleScope handleScope(game.isolate);

    // if event name does not exist, create the list and push this function onto it
    if (eventListeners.find(eventName) == eventListeners.end())
    {
      // create a new list
      std::list<EventListener> newList;
      
      // create new event listener 
      EventListener listener;

      listener.eventCallback.Reset(game.isolate, eventCallback);
      listener.object.Reset(game.isolate, object_);
      listener.ownerID = static_cast<size_t>(object_->Get(String::NewFromUtf8(game.isolate, "owner_id"))->IntegerValue());

      // push back the function
      newList.push_back(listener);
      
      // push the list into the map
      eventListeners.insert(std::pair<std::string, std::list<EventListener>>(eventName, newList));
    }

    else
    {
      // get a reference to the list
      auto &list = eventListeners[eventName];

      // create new event listener 
      EventListener listener;

      listener.eventCallback.Reset(game.isolate, eventCallback);
      listener.object.Reset(game.isolate, object_);
      listener.ownerID = static_cast<size_t>(object_->Get(String::NewFromUtf8(game.isolate, "owner_id"))->IntegerValue());

      // push back the function
      list.push_back(listener);
    }
  }

  // dispatch event function
  void EventDispatcher::DispatchEvent(std::string eventName, v8::Local<v8::Object> eventObject)
  {
    using namespace v8;

    auto &game = Game::Instance();

    Isolate::Scope isoScope(game.isolate);
    Locker locker(game.isolate);
    HandleScope handleScope(game.isolate);

    // because this pointer nonsense urgh why vectors why
    EventDispatcher::Type selfType = type;
    size_t selfOwnerID = ownerID;

    // if event name does not exist, return
    if (eventListeners.find(eventName) == eventListeners.end())
    {
      return;
    }

    if (game.debugMessageSetting == DebugMessageSetting::All ||
      game.debugMessageSetting == DebugMessageSetting::EventDispatch)
    {
      Console::WriteLine("\n==========================================");
      Console::Write("Event \"");
      Console::Write(eventName);
      Console::WriteLine("\" is being sent to");
      Console::WriteLine("------------------------------------------");
    }

    auto &list = eventListeners[eventName];



    // loop through list and call each function
    for (auto iterator = list.begin(); iterator != list.end(); ++iterator)
    {
      int objectDestroyed = -1;

      // dispatch events here
      TryCatch trycatch;

      size_t id = iterator->ownerID;
      // std::cout << "oustide id is " << id << std::endl;

      // if object is dead, don't call function, and clear it from the list
      if (!game.EntityIsAlive(iterator->ownerID))
      {
        // if deleting the first thing, just point it back at the beginning and update that
        if (iterator == list.begin())
        {
          //std::cout << "list begin" << std::endl;
          list.erase(iterator);
          iterator = list.begin();
          // only update if there's actually something there
          if (!list.empty())
          {
            //std::cout << "list empty" << std::endl;
            objectDestroyed = iterator->Call(eventObject);
          }
          else
          {
            //std::cout << "list not empty" << std::endl;
            break;
          }
        }

        else
        {
          // if not first thing, just decrement the iterator
          //std::cout << "decrement iterator" << std::endl;
          list.erase(iterator--);
        }
      }
      
      else
      {
        //std::cout << "no deletion" << std::endl;
        objectDestroyed = iterator->Call(eventObject);
      }

      // catches errors
      if (trycatch.HasCaught())
      {
        auto e = trycatch.Message();
        if (!e.IsEmpty())
        {
          Local<Value> exception = trycatch.Exception();
          String::Utf8Value exception_str(exception);
          int errorLineNumber = e->GetLineNumber();
          String::Utf8Value errorLine(e->GetSourceLine());
          String::Utf8Value errorFile(e->GetScriptResourceName());
          char buffer[60];
          std::sprintf(buffer, "Event(%s) Error: ", eventName.c_str());
          Console::Error(buffer, *exception_str);
          std::sprintf(buffer, ", Line Number %d : ", errorLineNumber);
          Console::Write(*errorFile, Omni::Console::Color::Red);
          Console::Write(buffer, Omni::Console::Color::Red);
          Console::WriteLine(*errorLine, Omni::Console::Color::Red);
          //Sleep(10000);
        }
      }

      // if the updating object needs to be destroyed, destroy it
      if (objectDestroyed >= 0)
      {
        Handlet<Entity> toDestroy = game.GetEntityByID(objectDestroyed);
        if (toDestroy.isValid())
        {
          toDestroy->Destroy();
        }
      }

      // if my owner is destroyed, stop execution
      if (selfType == Type::Entity && !game.EntityIsAlive(selfOwnerID))
        return;
    }
  }
}