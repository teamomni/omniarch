/******************************************************************************
Filename: Clock.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Clock.h"

Clock::Clock()
{
  // assign to a single processor
  SetThreadAffinityMask(GetCurrentThread(), 1);

  // get the frequency of this processor
  QueryPerformanceFrequency(&freq);

  // set the initial times
  Start();
  Stop();
}

void Clock::Start()
{
  QueryPerformanceCounter(&start);
}

void Clock::Stop()
{
  QueryPerformanceCounter(&stop);
}

float Clock::Elapsed()
{
  QueryPerformanceCounter(&current);
  // take the number of ticks between started and current, and divide that by ticks per second
  return(current.QuadPart - start.QuadPart) / float(freq.QuadPart);
}

float Clock::Difference()
{
  // take the number of ticks between stop and started, and divide that by ticks per second
  return (stop.QuadPart - start.QuadPart) / float(freq.QuadPart);
}

LONGLONG Clock::Current()
{
  QueryPerformanceCounter(&current);
  return current.QuadPart;
}

LONGLONG Clock::TimeInTicks(float time)
{
  return LONGLONG(time * freq.QuadPart);
}

float Clock::TicksInTime(LONGLONG ticks)
{
  return ticks / float(freq.QuadPart);
}

Clock::~Clock()
{
}
