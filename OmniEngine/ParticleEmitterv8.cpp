/******************************************************************************
Filename: ParticleEmitterv8.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "ParticleEmitter.h"

namespace Omni
{
  using namespace v8;
  
  static void v8_SetParticle(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the body pointer
    ParticleEmitter *emitter = Take<ParticleEmitter>::FromHolder(info);

    emitter->particles.Generate(Take<int>::FromV8(info[0]));
  }
  
  static void v8_ResetTrail(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the body pointer
    ParticleEmitter *emitter = Take<ParticleEmitter>::FromHolder(info);

    Vec2 targetPosition = Take<Vec2>::FromV8(info[0]);

    int particlesCount = emitter->particles.m_count;

    for (int i = 0; i < particlesCount; ++i)
    {
      emitter->startPositions[i] = targetPosition;
      // emitter->particles.m_pos[i] = targetPosition;
      // emitter->particles.m_scale[i].x = 0;
    }
  }

  void ParticleEmitter::v8_Register()
  {
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    Local<ObjectTemplate> objTemplate_temp;

    // if there's nothing in the obj template right now, make a new one
    if (objTemplate.IsEmpty())
    {
      objTemplate_temp = ObjectTemplate::New(game.isolate);
      objTemplate_temp->SetInternalFieldCount(1);
    }
    // else, get it from the object
    else
      objTemplate_temp = LocalObjectTemplate::New(game.isolate, objTemplate);

    // REGISTER FUNCTIONS HERE //
    // objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "EmitSomeFunParticles"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_EmitSomeFunParticles)));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "SetParticle"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_SetParticle)));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "ResetTrail"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_ResetTrail)));

    objTemplate.Reset(game.isolate, objTemplate_temp);

    ExposeProperties();
  }

  v8_RegisterComponentWithEntity(ParticleEmitter);
}