/******************************************************************************
Filename: BloomEffect.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "BloomEffect.h"

namespace Omni
{

  void BloomEffect::Initialize()
  {
  }

  void BloomEffect::Update(float dt)
  {
    //Add to renderable objects;
  }

  void BloomEffect::Destroy()
  {
    ScriptableDestroy();
    Game::Instance().DiscardModulePropertyHandles(Handlet<Component>(selfPtr));
  }

  void BloomEffect::Register()
  {
    RegisterName("BloomEffect", &Game::Instance().systemContainers, PropertyDefinitionMap({
        { "threshold", PropertyDefinition::Float(0.0f, offsetof(BloomEffect, p_Threshold), true) },
    }), false);
  }

  void BloomEffect::v8_Register()
  {
    using namespace v8;

    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    Local<ObjectTemplate> objTemplate_temp;

    // if there's nothing in the obj template right now, make a new one
    if (objTemplate.IsEmpty())
    {
      objTemplate_temp = ObjectTemplate::New(game.isolate);
      objTemplate_temp->SetInternalFieldCount(1);
    }
    // else, get it from the object
    else
      objTemplate_temp = LocalObjectTemplate::New(game.isolate, objTemplate);

    objTemplate.Reset(game.isolate, objTemplate_temp);

    ExposeProperties();
  }

  v8_RegisterComponentWithEntity(BloomEffect);
  
} //Namespace Omni
