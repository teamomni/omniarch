/******************************************************************************
Filename: RenderState.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once
#include "stdafx.h"
#include <vector>
#include "Image.h"
#include "Transform.h"
#include "RenderState.h"

namespace Omni
{

  struct RenderObject
  {
    //
    // SPRITE SPECIFIC
    //
    Vec2 translation;
    float rotation;
    D3DXCOLOR color;
    float u1, v1;
    float u2, v2;
    Vec2 scale;


    bool glow;
    D3DXCOLOR glowColor;
    float brightness;

    bool bloomEnabled;
    float bloomThreshold;

    bool blurEnabled;

    // 
    // PARTICLE SPECIFIC
    //
    std::vector<Vec2> translations;
    std::vector<D3DXCOLOR> colors;
    bool additive;
    std::vector<Vec2> scales;
    std::vector<float> rotations;


    //
    // BOTH
    //

    Image *image;
    float depth;
    size_t space;
    bool particleEmitter;
    bool flipX;
    bool flipY;

    //
    // SPRITE CONSTRUCTOR
    //

    RenderObject(Image *image_, 
                 Vec2 translation_, 
                 Vec2 scale_, 
                 float rotation_, 
                 float depth_, 
                 D3DXCOLOR color_, 
                 Vec2 uv1, 
                 Vec2 uv2, 
                 bool flipX_,
                 bool flipY_, 
                 size_t space_, 
                 bool glow_, 
                 D3DXCOLOR glowColor_, 
                 float brightness_,
                 bool bloomEnabled_ = false,
                 float bloomThreshold_ = 1.0,
                 bool blurEnabled_ = false) : image(image_), 
                                              translation(translation_), 
                                              scale(scale_), 
                                              rotation(rotation_), 
                                              depth(depth_), 
                                              color(color_), 
                                              u1(uv1.x), 
                                              u2(uv2.x), 
                                              v1(uv1.y), 
                                              v2(uv2.y), 
                                              flipX(flipX_), 
                                              flipY(flipY_), 
                                              space(space_), 
                                              glow(glow_), 
                                              glowColor(glowColor_), 
                                              brightness(brightness_),
                                              bloomEnabled(bloomEnabled_),
                                              bloomThreshold(bloomThreshold_),
                                              blurEnabled(blurEnabled_),
                                              particleEmitter(false){}

    //
    // PARTICLE CONSTRUCTOR
    //

    RenderObject(Image* image_,
                 std::vector<Vec2> translations_,
                 std::vector<D3DXCOLOR> colors_,
                 std::vector<Vec2> scale_,
                 std::vector<float> rotations_,
                 bool additive_,
                 float depth_,
                 size_t space_,
                 bool flipX_,
                 bool flipY_ ) : image(image_),
                                 translations(translations_),
                                 colors(colors_),
                                 scales(scale_),
                                 rotations(rotations_),
                                 additive(additive_),
                                 depth(depth_),
                                 space(space_),
                                 flipX(flipX_),
                                 flipY(flipY_),
                                 particleEmitter(true) {}

  };

  struct RenderTextObject
  {
    std::string text;
    std::string font;
    float size;
    UINT32 color;
    Vec2 translation;

    size_t space;
    bool space_default;

    RenderTextObject(std::string text_, std::string font_, float size_, UINT32 color_, Vec2 trans_, size_t space_, bool s_default_ = false) : text(text_), font(font_), size(size_), color(color_), translation(trans_), space(space_), space_default(s_default_){}
  };

  struct RenderState
  {
    
    struct Line
    {
      Transform transform;
      D3DXCOLOR color;
      size_t space;
    };

    struct Quad
    {
      Transform transform;
      D3DXCOLOR color;
      size_t space;

    };

    struct Arrow
    {
      //Triangle
      Transform transform;
      D3DXCOLOR color;
      size_t space;

    };

    size_t beginTick = 0;
    size_t endTick = 0;
    std::vector<Transform> cameras;
    std::vector<RenderObject> objects;
    std::vector <RenderTextObject> textObjects;
    std::vector<Line> lines;
    std::vector<Quad> quads;
    std::vector<Arrow> arrows;

    void BeginUpdate(size_t tick);

    void EndUpdate(size_t tick);

    void Clear();

  };
}
