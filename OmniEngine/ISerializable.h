/******************************************************************************
Filename: ISerializable.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "json/json.h"

namespace Omni
{
  class ISerializable
  {
  public:
    virtual ~ISerializable() {}
    virtual void Serialize(Json::Value &root) const = 0;
  };
}

