/******************************************************************************
Filename: HorizBlurPShader.hlsl

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// Purpose: Horizontal Blur Pixel Shader
// Date   : Spring 2014
// Type   : Pixel Shader

//-----------------------------------//
// Textures Should Be Binded on CPU  //
//-----------------------------------//
Texture2D Texture0 : register(t0);

//-----------------------------------//
// Sample State Set on the CPU       //
//-----------------------------------//
SamplerState sampleState;

//-----------------------------------//
// Struct Passed from Vertex Shader  //
//-----------------------------------//
struct vOutput
{
  float4 position : SV_POSITION;
  float2 texcoord : TEXCOORD;

  float2 texCoord1 : TEXCOORD1;
  float2 texCoord2 : TEXCOORD2;
  float2 texCoord3 : TEXCOORD3;
  float2 texCoord4 : TEXCOORD4;
  float2 texCoord5 : TEXCOORD5;
  float2 texCoord6 : TEXCOORD6;
  float2 texCoord7 : TEXCOORD7;
  float2 texCoord8 : TEXCOORD8;
  float2 texCoord9 : TEXCOORD9;
};

//-----------------------------------//
// Main Function of Pixel Shader     //
//-----------------------------------//
float4 PShader(vOutput input) : SV_Target0
{
  float4 sum = float4(0.0f, 0.0f, 0.0f, 0.0f);

  sum += Texture0.Sample(sampleState, input.texCoord1) * 0.05f;
  sum += Texture0.Sample(sampleState, input.texCoord2) * 0.09f;
  sum += Texture0.Sample(sampleState, input.texCoord3) * 0.12f;
  sum += Texture0.Sample(sampleState, input.texCoord4) * 0.15f;
  sum += Texture0.Sample(sampleState, input.texCoord5) * 0.16f;
  sum += Texture0.Sample(sampleState, input.texCoord6) * 0.15f;
  sum += Texture0.Sample(sampleState, input.texCoord7) * 0.12f;
  sum += Texture0.Sample(sampleState, input.texCoord8) * 0.09f;
  sum += Texture0.Sample(sampleState, input.texCoord9) * 0.05f;

  return sum;
}