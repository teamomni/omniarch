/******************************************************************************
Filename: v8DebugSettings.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


namespace Omni
{
  enum DebugMessageSetting
  {
    None = 0,
    All,
    Destruction,
    EventDispatch
  };
}