/******************************************************************************
Filename: Game.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include <list>
#include <unordered_map>
#include <string>
#include <chrono>
#include "WindowSystem.h"
#include "SoundSystem.h"
#include "SystemContainer.h"
#include "PropertyHandle.h"
#include "GFXDebugDraw.h"
#include "RenderState.h"
#include "Animation.h"
#include <mutex>
#include "v8Definitions.h"
#include "SoundUtilities.h"
#include "EventDispatcher.h"
#include "ComponentDefinition.h"
#include "Material.h"
#include "Archetype.h"
#include "ISerializable.h"
#include "Clock.h"
#include "Handlet.h"
#include "v8DebugSettings.h"
#include <stack>

namespace Omni
{
  class Space;
  struct EntityHandle;
  class Entity;
  class Component;
  class Editor;

  class Game : public ISerializable
  {

  public:
    // Destructor that calls Shutdown() and cleans up the engine
    ~Game();

    // Easy way to get the one and only instance of the engine
    inline static Game& Instance() { return s_instance; }

    // These methods will be edited by engine programmers as they add new
    // SystemComponents and so forth
    void Init();
    void Init(HINSTANCE hInstance, unsigned iconID);
    void InitTest();
    void Update(float dt = 1.0f / s_tickRate);
    void Render(RenderState *renderState, float dt);
    void Shutdown();
    void ShutdownDueToGraphicsCard();

    // This is called once to start the game
    void Start(HINSTANCE hInstance_, unsigned iconID_, const char *dataLocation_, bool isFinal_);
    void StartTest();
    void Stop() { isRunning = false; }

    bool IsTesting() { return isTesting; }

    // Threaded functions
    void UpdateLoop();
    void RenderLoop();

    // Wrapper around Update()
    void Loop(float dt);

    // Test function, emulates a loop
    void TestLoop();

    // Remove destroying spaces, etc.
    void Cleanup();

    void Restart();

    bool GetInput(MSG msg);

    void LoadJavaScriptAssets();

    void LoadAssets(std::string name, std::function<void(std::string, std::string, void *)> callback, void *arbitrary, std::string preExtension = "");

    void LoadDataAssets();

    static std::mutex mtx;
    static std::mutex mtx_input;

    static std::queue<MSG *> s_messages;
    static std::condition_variable s_messageCondition;

    static std::string GetMyDocumentsDirectory();

    inline RenderState &GetUpdateRenderState()
    {
      return renderStates[state_updating];
    }

    bool EditorIsActive() const;

    float framesPerSecond_update = 0.0f;
    float framesPerSecond_render = 0.0f;

    inline bool IsLoaded()
    {
      return loaded;
    }
    
    bool isFinal;

    // Game-level editor
    Editor *editor;
    std::string level;
    std::string nextLevel;
    bool loadNextLevel;
    bool serializeCamera;
    std::string lastCreatedArchetype;
    std::string currObject;

    // timescale stuff
    float timescale = 1.0f;

    std::string windowText = "OmniArch";
    std::string windowTextLast = "OmniArch";

    // Spaces
    Space *CreateSpace_Top(std::string name);
    void DestroySpace(std::string name);
    Space *GetSpaceByName(std::string name);
    inline unsigned GetSpaceCount() { return spaces.size(); }
    std::unordered_map<std::string, Space *> &GetAllSpacesWithNames() { return namedSpaces; }
    inline std::list<Space> &GetAllSpaces() { return spaces; }

    void PauseAllExcept(std::string except);
    void UnpauseAll();

    inline std::wstring& GetFileLocation() { return fileLocation; };
    inline SoundSystem* GetSoundSystem() { return soundSystem; };
    inline WindowSystem* GetWindowSystem() { return windowSystem; };
    inline Graphics::GFXManager* GetGraphicsManager() { return graphicsManager; };

//#if _DEBUG
    //So anyone can access debug draw functions from game
    inline Graphics::GFXDebugDraw* GetDebugDraw() { return debugDraw; };
//#endif

    ModulePropertyHandle *GetPropertyHandle(Handlet<Component> component, std::string property);
    Handlet<Entity> GetEntityByID(size_t id);
    Handlet<Entity> GetEntityByName(std::string);
    bool EntityIsAlive(size_t id);
   
    // Link ID stuff
    std::unordered_map<size_t, size_t> linkIDtoUniqueID;
    Handlet<Entity> GetEntityByLinkID(size_t linkID);
    size_t GetUniqueIDFromLinkID(size_t linkID);

    void LoadLevel(std::string level);

    // Components
    inline void RegisterComponent(std::string name, ComponentDefinition definition)
    {
      componentDefinitions.insert({ name, definition });
    }

    // Whether or not the game is running
    inline bool IsRunning() { return isRunning; }

    struct
    {
      std::unordered_map<std::string, Graphics::Handle> textures;
      std::unordered_map<std::string, Image> images;
      std::unordered_map<std::string, Animation> animations;
      std::unordered_map<std::string, Material> materials;
      std::unordered_map<std::string, Archetype> archetypes;
    } assets;

    //std::list<EntityHandle> entityHandles;
    std::unordered_map<size_t, Handlet<Entity>> entityHandles;
    std::unordered_map<std::string, SystemContainer> systemContainers;
    std::list<ModulePropertyHandle> modulePropertyHandles;

    std::unordered_map<std::string, ComponentDefinition> componentDefinitions;

    void DiscardSystemPropertyHandles(std::string system, size_t instance);
    void DiscardModulePropertyHandles(Handlet<Component> module);

    std::list<SoundUtilities::ParameterHandle> parameterHandles;
    SoundUtilities::ParameterHandle *GetParameterHandle(size_t instance, size_t parameter);

    // v8 stuff
    bool v8_ready;
    void v8_RegisterPrintMessage(v8::Handle<v8::ObjectTemplate> globalTemplate);
    void v8_RegisterCheckInput(v8::Handle<v8::ObjectTemplate> globalTemplate);
    void v8_RegisterVec2(v8::Handle<v8::ObjectTemplate> globalTemplate);
    void v8_RegisterVec4(v8::Handle<v8::ObjectTemplate> globalTemplate);
    void v8_CreateConsole();
    void v8_ExposeGame();
    PersistentContext context;
    
    std::stack<int> currentlyUpdatingIDs;
    DebugMessageSetting debugMessageSetting = DebugMessageSetting::None;

    EventDispatcher *dispatcher;

    v8::Isolate *isolate;

    // map of function templates
    std::unordered_map<std::string, v8::Handle<v8::ObjectTemplate>> v8Functions;

    // check to see if game is in editor mode
    bool isEditing = false;

    // game level clock
    Clock clock;
    
    virtual void Serialize(Json::Value &root) const;
    virtual void Serialize() const;

  private:
    // Whether or not the game is running
    bool isRunning = false;

    bool isRestarting = false;

    bool isTesting = true;

    bool didTest = false;

    bool loaded = false;

    bool ranOnce = false;

    HINSTANCE hInstance;
    unsigned iconID;

    WindowSystem *windowSystem = nullptr;

    Graphics::GFXManager *graphicsManager = nullptr;

//#if _DEBUG

    Graphics::GFXDebugDraw *debugDraw = nullptr;

//#endif

    SoundSystem *soundSystem = nullptr;

    std::wstring fileLocation;
    std::string dataLocation;
    std::wstring dataLocation_w; // why not?

    // Private static instance variable
    static Game s_instance;

    // List of all current spaces
    std::list<Space> spaces;
    std::unordered_map<std::string, Space *> namedSpaces;
    std::list<Space *> destroyingSpaces;

    //std::unordered_map<std::string, ComponentDefinition> componentDefinitions;

    // Rendering
    static const size_t s_tickRate = 60;
    static const float s_frameTime;
    static const size_t s_garbageMinimum = 50;
    size_t tick = 0;
    typedef std::chrono::duration<double, std::ratio<1, s_tickRate>> frameLength;

    RenderState renderStates[3];
    short state_rendering = 0;
    short state_updating = 1;
    short state_lastUpdated = 2;

    int r = 255;
    int g = 255;
    int b = 255;
    void UpdateLoadingScreen();

  };
}

