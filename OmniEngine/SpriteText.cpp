/******************************************************************************
Filename: SpriteText.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"

#include "SpriteText.h"

namespace Omni
{
  void SpriteText::Initialize()
  {
  }

  void SpriteText::Update(float dt)
  {
    
    //Setting the Color into a UINT32
    int r = static_cast<int>(p_Color.r * 255.0f);
    int g = static_cast<int>(p_Color.g * 255.0f);
    int b = static_cast<int>(p_Color.b * 255.0f);
    int a = static_cast<int>(p_Color.a * 255.0f);

    m_Color = a << 24 | b << 16 | g << 8 | r;

    RenderState &rs = Game::Instance().GetUpdateRenderState();

    rs.textObjects.push_back(RenderTextObject(p_Text, p_Font, p_Size, m_Color, owner->transform.translation, owner->space->index));
    
  }
 
  void SpriteText::Destroy()
  {
    ScriptableDestroy();
    Game::Instance().DiscardModulePropertyHandles(Handlet<Component>(selfPtr));
  }

  void SpriteText::Register()
  {
    RegisterName("SpriteText", &Game::Instance().systemContainers, PropertyDefinitionMap({
        { "text",     PropertyDefinition::String("",            offsetof(SpriteText, p_Text),  true) },
        { "font",     PropertyDefinition::String("Courier New", offsetof(SpriteText, p_Font),  true) },
        { "size",     PropertyDefinition::Float(12.0f,          offsetof(SpriteText, p_Size),  true) },
        { "color",    PropertyDefinition::Color(                offsetof(SpriteText, p_Color), true) }
    }));
  }


} //namespace Omni
