/******************************************************************************
Filename: Matrix3.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Matrix3.h" 
#include <iostream>

namespace Omni
{
  Matrix3::Matrix3()
  {
    m00 = 1; m01 = 0; m02 = 0;
    m10 = 0; m11 = 1; m12 = 0;
    m20 = 0; m21 = 0; m22 = 1;
  }

  Matrix3::Matrix3(float theta)
  {
    m00 = cosf(theta); m01 = -sinf(theta); m02 = 0;
    m10 = sinf(theta); m11 = cosf(theta);  m12 = 0;
    m20 = 0;           m21 = 0;            m22 = 1;
  }

  Matrix3::Matrix3(float scaleX, float scaleY)
  {
    m00 = scaleX; m01 = 0;      m02 = 0;
    m10 = 0;      m11 = scaleY; m12 = 0;
    m20 = 0;      m21 = 0;      m22 = 1;
  }

  Matrix3::Matrix3(Vec2 translation)
  {
    m00 = 1;  m01 = 0;  m02 = translation.x;
    m10 = 0;  m11 = 1;  m12 = translation.y;
    m20 = 0;  m21 = 0;  m22 = 1;
  }

  Matrix3 Matrix3::operator*(const Matrix3 &rhs) const
  {
    Matrix3 result;

    // row 1 of resultant matrix
    result.m00 = m00 * rhs.m00 + m01 * rhs.m10 + m02 * rhs.m20;
    result.m01 = m00 * rhs.m01 + m01 * rhs.m11 + m02 * rhs.m21;
    result.m02 = m00 * rhs.m02 + m01 * rhs.m12 + m02 * rhs.m22;

    // row 2 of resultant matrix
    result.m10 = m10 * rhs.m00 + m11 * rhs.m10 + m12 * rhs.m20;
    result.m11 = m10 * rhs.m01 + m11 * rhs.m11 + m12 * rhs.m21;
    result.m12 = m10 * rhs.m02 + m11 * rhs.m12 + m12 * rhs.m22;

    // row 3 of resultant matrix
    result.m20 = m20 * rhs.m00 + m21 * rhs.m10 + m22 * rhs.m20;
    result.m21 = m20 * rhs.m01 + m21 * rhs.m11 + m22 * rhs.m21;
    result.m22 = m20 * rhs.m02 + m21 * rhs.m12 + m22 * rhs.m22;

    return result;
  }

  // makes the lhs matrix lhs * rhs
  Matrix3 &Matrix3::operator*= (const Matrix3 &rhs)
  {
    Matrix3 result;

    // column 1 of resultant matrix
    result.m00 = m00 * rhs.m00 + m01 * rhs.m10 + m02 * rhs.m20;
    result.m01 = m00 * rhs.m01 + m01 * rhs.m11 + m02 * rhs.m21;
    result.m02 = m00 * rhs.m02 + m01 * rhs.m12 + m02 * rhs.m22;

    // column 2 of resultant matrix
    result.m10 = m10 * rhs.m00 + m11 * rhs.m10 + m12 * rhs.m20;
    result.m11 = m10 * rhs.m01 + m11 * rhs.m11 + m12 * rhs.m21;
    result.m12 = m10 * rhs.m02 + m11 * rhs.m12 + m12 * rhs.m22;

    // column 3 of resultant matrix
    result.m20 = m20 * rhs.m00 + m21 * rhs.m10 + m22 * rhs.m20;
    result.m21 = m20 * rhs.m01 + m21 * rhs.m11 + m22 * rhs.m21;
    result.m22 = m20 * rhs.m02 + m21 * rhs.m12 + m22 * rhs.m22;

    for (int i = 0; i < 9; ++i)
      v[i] = result.v[i];

    return *this;
  }

  Vec2 Matrix3::operator*(const Vec2 &rhs) const
  {

    Vec2 result;
    result.x = m00 * rhs.x + m01 * rhs.y + m02;
    result.y = m10 * rhs.x + m11 * rhs.y + m12;

    return result;
  }

  void Matrix3::Scale(float scaleX, float scaleY)
  {
    m00 = scaleX; m01 = 0;      m02 = 0;
    m10 = 0;      m11 = scaleY; m12 = 0;
    m20 = 0;      m21 = 0;      m22 = 1;
  }
  void Matrix3::Scale(Vec2 scale)
  {
    m00 = scale.x;  m01 = 0;        m02 = 0;
    m10 = 0;        m11 = scale.y;  m12 = 0;
    m20 = 0;        m21 = 0;        m22 = 1;
  }

  void Matrix3::Rotate(float theta)
  {
    m00 = cosf(theta); m01 = -sinf(theta); m02 = 0;
    m10 = sinf(theta); m11 = cosf(theta);  m12 = 0;
    m20 = 0;           m21 = 0;            m22 = 1;
  }

  void Matrix3::Translate(float x, float y)
  {
    m00 = 1;  m01 = 0;  m02 = x;
    m10 = 0;  m11 = 1;  m12 = y;
    m20 = 0;  m21 = 0;  m22 = 1;
  }
  void Matrix3::Translate(Vec2 translation)
  {
    m00 = 1;  m01 = 0;  m02 = translation.x;
    m10 = 0;  m11 = 1;  m12 = translation.y;
    m20 = 0;  m21 = 0;  m22 = 1;
  }

  void Matrix3::Apply(Vec2 &target)
  {
    Vec2 result;
    result.x = m00 * target.x + m01 * target.y + m02;
    result.y = m10 * target.x + m11 * target.y + m12;

    target = result;
  }

  Matrix3 Matrix3::Inverse(void)
  {
    //Inverse is the negated point
    Vec2 dsp(-m[0][2], -m[1][2]);
    Matrix3 TransInv(dsp);

    //find determinant
    float det = (1 / (m[0][0] * m[1][1] - m[0][1] * m[1][0]));

    //m00 = 1; m01 = 0; m02 = 0;
    //m10 = 0; m11 = 1; m12 = 0;
    //m20 = 0; m21 = 0; m22 = 1;


    Matrix3 inverse;

    inverse.m[0][0] = m[1][1] * det;
    inverse.m[0][1] = -m[0][1] * det;

    inverse.m[1][0] = -m[1][0] * det;
    inverse.m[1][1] = m[0][0] * det;



    return inverse * TransInv;
  }

  std::ostream &operator<<(std::ostream &os, const Matrix3 &input)
  {
    os << input.m00 << " " << input.m01 << " " << input.m02 << std::endl;
    os << input.m10 << " " << input.m11 << " " << input.m12 << std::endl;
    os << input.m20 << " " << input.m21 << " " << input.m22 << std::endl;

    return os;
  }
}
