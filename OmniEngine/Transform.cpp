/******************************************************************************
Filename: Transform.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Transform.h"

namespace Omni
{
  Transform::Transform() : gottenLastFrameTrans(false)
  {
    //Starting Matrix is the Identity Matrix

    /*  KEY
    A1 B1 C1 D1
    A2 B2 C2 D2
    A3 B3 C3 D3
    A4 B4 C4 D4
    */

    lastMatrix.m[0][0] = 1.0f;    //A1
    lastMatrix.m[1][0] = 0.0f;    //B1
    lastMatrix.m[2][0] = 0.0f;    //C1  Default 
    lastMatrix.m[3][0] = 0.0f;    //D1 (X trans)

    lastMatrix.m[0][1] = 0.0f;    //A2
    lastMatrix.m[1][1] = 1.0f;    //B2
    lastMatrix.m[2][1] = 0.0f;    //C2  Default
    lastMatrix.m[3][1] = 0.0f;    //D2 (Y trans)
    
    lastMatrix.m[0][2] = 0.0f;    //A3  Default
    lastMatrix.m[1][2] = 0.0f;    //B3  Default
    lastMatrix.m[2][2] = 1.0f;    //C3  Default
    lastMatrix.m[3][2] = 0.0f;    //D3  Default
                                  
    lastMatrix.m[0][3] = 0.0f;    //A4  Default
    lastMatrix.m[1][3] = 0.0f;    //B4  Default
    lastMatrix.m[2][3] = 0.0f;    //C4  Default
    lastMatrix.m[3][3] = 1.0f;    //D4  Default

    translation.x = 0;
    translation.y = 0;
    scale.x = 1;
    scale.y = 1;
    rotation = 0.0f;

    lastTranslation.x = 0;
    lastTranslation.y = 0;
    lastScale.x = 1;
    lastScale.y = 1;
    lastRotation = 0.0f;

    lastTranslation3.x = 0;
    lastTranslation3.y = 0;
    lastScale3.x = 1;
    lastScale3.y = 1;
    lastRotation3 = 0.0f;

    UpdateLastTransformData();
  }

  bool Transform::HasChanged()
  {
    return (translation != lastTranslation) || (scale != lastScale) || (rotation != lastRotation);
  }

  bool Transform::HasChanged3()
  {
    return (translation != lastTranslation3) || (scale != lastScale3) || (rotation != lastRotation3);
  }

  void Transform::UpdateLastTransformData()
  {
    lastFrameTrans = translation;
    lastFrameScale = scale;
    lastFrameRot = rotation;
  }

  D3DXMATRIX Transform::GetMatrix()
  {
    if (this->HasChanged())
    {
      Matrix3 Trans, Rot, Scale, Final;

      Trans.Translate(translation);
      Rot.Rotate(rotation);
      Scale.Scale(scale);
      
      Final = Trans * Rot * Scale;
      
      /*  KEY
      A1 B1 C1 D1
      A2 B2 C2 D2
      A3 B3 C3 D3
      A4 B4 C4 D4
      */

      lastMatrix.m[0][0] = Final.m[0][0]; //A1
      lastMatrix.m[1][0] = Final.m[0][1]; //B1
      lastMatrix.m[3][0] = Final.m[0][2]; //D1 (X trans)

      lastMatrix.m[0][1] = Final.m[1][0]; //A2
      lastMatrix.m[1][1] = Final.m[1][1]; //B2
      lastMatrix.m[3][1] = Final.m[1][2]; //D2 (Y trans)

      //The rest are default and should never be changed   

      lastTranslation = translation;
      lastRotation = rotation;
      lastScale = scale;
    }

    return lastMatrix;
  }

  D3DXMATRIX Transform::GetCameraMatrix()
  {

    if (this->HasChanged())
    {
      Matrix3 Trans, Rot, Final;

      Trans.Translate(translation * Vec2(-1, -1));
      Rot.Rotate(rotation);

      Final = Trans * Rot;

      lastMatrix.m[0][0] = Final.m[0][0]; //A1
      lastMatrix.m[1][0] = Final.m[0][1]; //B1
      lastMatrix.m[3][0] = Final.m[0][2]; //D1 (X trans)

      lastMatrix.m[0][1] = Final.m[1][0]; //A2
      lastMatrix.m[1][1] = Final.m[1][1]; //B2
      lastMatrix.m[3][1] = Final.m[1][2]; //D2 (Y trans)

      lastTranslation = translation;
      lastRotation = rotation;
      lastScale = scale;
    }

    return lastMatrix;
  }

  // matrix3 gettor for physics
  Matrix3 Transform::GetMatrix3()
  {
    if (this->HasChanged3())
    {
      Matrix3 Trans, Rot, Scale, Final;

      Trans.Translate(translation);
      Rot.Rotate(rotation);
      Scale.Scale(scale);

      lastMatrix3 = Trans * Rot * Scale;

      lastTranslation3 = translation;
      lastRotation3 = rotation;
      lastScale3 = scale;
    }

    return lastMatrix3;
  }

}
