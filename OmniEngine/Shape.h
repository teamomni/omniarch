/******************************************************************************
Filename: Shape.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

/******************************************************************************

Filename: Shape.h

Author: Tai Der Hui

A shape is a virtual class that can be either a circle or a polygon.

The distinction is very important because different functions need to be called
for different collisions

******************************************************************************/

#pragma once

#include "RigidBody.h"
#include "Matrix3.h"

static const float PI = 3.141593f;
const unsigned int MaxVertexCount = 32;

namespace Omni{
  struct Shape
  {
    enum class Type {
      Polygon,
      Circle
    };

    Shape();
    virtual Shape *Clone() const = 0;
    virtual void Initialize() = 0;
    virtual void ComputeMass() = 0;
    virtual void SetOrientation(float radians) = 0;
    virtual Type GetType() const = 0;

    RigidBody *body;
    Vec2 center;
    Type type;
  };

  struct Circle : public Shape
  {
    Circle(float radius_);

    Shape *Clone() const;
    void Initialize();
    void ComputeMass();
    void SetOrientation(float radians);
    Type GetType() const;

    float radius;
  };

  struct Polygon : public Shape
  {
    Polygon();
    Polygon(float width, float height);
    Polygon(Vec2 vertices_[], unsigned int vertexCount_);
    Polygon::Polygon(Property::Vec2List vec2vector);

    Shape *Clone() const;
    void Initialize();
    void ComputeMass();
    void SetOrientation(float radians);
    Type GetType() const;

    void SetBox(float width, float height);
    void Set(Vec2 vertices_[], unsigned int vertexCount_);

    unsigned int vertexCount;
    Vec2 vertices[MaxVertexCount];
    Vec2 normals[MaxVertexCount];
    Matrix3 rotationMatrix;
  };
}