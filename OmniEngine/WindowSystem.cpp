/******************************************************************************
Filename: WindowSystem.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// File     :    WindowSystem.cpp
// Authror  :    Juli Gregg
// Brief    :    Handles window and Windows messages implementation

#include "stdafx.h"
#include "WindowSystem.h"
#include <iostream>
#include "GFXManager.h"
#include "Game.h"
#include "Input.h"

namespace Omni
{

  //For Windows Messages
  //Currently Only need Quit Message
  
  LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
  {
    // set the scroll things to false
    Input::mouse.SetButton(Mouse::Button::ScrollUp, false);
    Input::mouse.SetButton(Mouse::Button::ScrollDown, false);

    if (message == WM_DESTROY)
    {
      Game::Instance().Stop();
      return 0;
    }
    {
      if (Game::Instance().IsLoaded())
      {
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
        if (TwEventWin(hWnd, message, wParam, lParam)) // send event message to AntTweakBar
          return 0;
#endif
      }
    }
    switch (message)
    {
      
    case WM_SETCURSOR:
    {
      WORD ht = LOWORD(lParam);
      static bool hiddencursor = false;
      if ((HTCLIENT == ht && !Game::Instance().EditorIsActive()) && !hiddencursor)
      {
        hiddencursor = true;
        ShowCursor(false);
      }
      else if ((HTCLIENT != ht || Game::Instance().EditorIsActive()) && hiddencursor)
      {
        hiddencursor = false;
        ShowCursor(true);
      }
    }
      break;
      case WM_DESTROY:
        //PostQuitMessage(0);
        Game::Instance().Stop();
        return 0;
        break;
      case WM_KEYDOWN:
        break;
      case WM_SIZE:
        //Recieves this message when window resizes
        std::cout << "WINDOW RESIZE: " << LOWORD(lParam) << " x " << HIWORD(lParam) << std::endl;

        if (Game::Instance().GetGraphicsManager())
          Game::Instance().GetGraphicsManager()->m_renderer->Resize(lParam);

        
        break;
      case WM_ACTIVATE:
        if (Game::Instance().GetGraphicsManager())
          Game::Instance().GetGraphicsManager()->m_renderer->SetWindowActive(wParam, lParam);
        if (Game::Instance().GetSoundSystem())
          Game::Instance().GetSoundSystem()->SetActive(wParam);

        break;
      case WM_MOUSEMOVE:
        //std::cout << "Mouse Moveing" << std::endl;
        break;
      case WM_LBUTTONDOWN:
        //std::cout << "LEFT MOUSE PRESSED" << std::endl;
        //std::cout << LOWORD(lParam) << ", " << HIWORD(lParam) << std::endl;
        Input::mouse.SetButton(Mouse::Button::Left, true);
        break;
      case WM_LBUTTONUP:
        //std::cout << "LEFT MOUSE RELEASED" << std::endl;
        Input::mouse.SetButton(Mouse::Button::Left, false);
        break;
      case WM_RBUTTONDOWN:
        //std::cout << "RIGHT MOUSE PRESSED" << std::endl;
        Input::mouse.SetButton(Mouse::Button::Right, true);
        break;
      case WM_RBUTTONUP:
        //std::cout << "Right MOUSE RELEASED" << std::endl;
        Input::mouse.SetButton(Mouse::Button::Right, false);
        break;
      case WM_MBUTTONDOWN:
        Input::mouse.SetButton(Mouse::Button::Middle, true);
        break;
      case WM_MBUTTONUP:
        Input::mouse.SetButton(Mouse::Button::Middle, false);
        break;
      case WM_MOUSEWHEEL:
        auto wheelDelta = GET_WHEEL_DELTA_WPARAM(wParam);
        if (wheelDelta > 0)
          Input::mouse.SetButton(Mouse::Button::ScrollUp, true);
        else
          Input::mouse.SetButton(Mouse::Button::ScrollDown, true);
        break;

    }

    

    return DefWindowProc(hWnd, message, wParam, lParam);
  }

  //Constructor for setting up Window System
  WindowSystem::WindowSystem(HINSTANCE hInstance_, unsigned iconID)
  {
    //////////////////////////////
    // Gets Client's Resolution //
    //////////////////////////////

    RECT resolution;
    const HWND hResolution = GetDesktopWindow();

    //fullscreen size
    GetClientRect(hResolution, &resolution);

    Resolution.y = (float)resolution.bottom;
    Resolution.x = (float)resolution.right;

    std::cout << "Client Resolution: " << Resolution.x << " x " << Resolution.y << std::endl;

    //////////////////////////////
    //      Setup Window        //
    //////////////////////////////

    WNDCLASSEX wc;

    hInstance = hInstance_;

    ZeroMemory(&wc, sizeof(WNDCLASSEX));

    wc.cbSize         = sizeof(WNDCLASSEX);
    wc.style          = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc    = WindowProc;
    wc.hInstance      = hInstance;
    wc.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wc.lpszClassName  = L"WindowClass";
    wc.hIcon          = LoadIcon(hInstance, (LPCTSTR)iconID);
    wc.hIconSm        = LoadIcon(wc.hInstance, (LPCTSTR)iconID);

    RegisterClassEx(&wc);

    RECT wr = { 0, 0, (LONG)Resolution.x, (LONG)Resolution.y };
    AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);

    hWnd = CreateWindowEx(NULL,
                          L"WindowClass",
                          L"OmniArch",
                          /*WS_POPUP | WS_SYSMENU | WS_VISIBLE,*/
                          WS_OVERLAPPEDWINDOW, //& ~WS_THICKFRAME,
                          0,
                          0,
                          wr.right - wr.left,
                          wr.bottom - wr.top,
                          NULL,
                          NULL,
                          hInstance,
                          NULL);

    //Will get over written later
    ClientWidth = (int)Resolution.x;
    ClientHeight = (int)Resolution.y;

  }

  //Show the actual window
  void WindowSystem::ActivateWindow(void)
  {
    //Maximize window
    ShowWindow(hWnd, SW_SHOWMAXIMIZED);
    
    //Get the actual window size accounting for toolbars
    RECT resolution;
    GetClientRect(hWnd, &resolution);

    //Store the current size
    ClientHeight = resolution.bottom;
    ClientWidth = resolution.right;

    //Print current size
    std::cout << "Client Resolution: " << ClientWidth << " x " << ClientHeight << std::endl;

    //Tell Windows to Repaint window
    UpdateWindow(hWnd);
  }

}