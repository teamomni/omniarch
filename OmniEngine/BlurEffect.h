/******************************************************************************
Filename: BlurEffect.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#include "stdafx.h"
#include "Component.h"

namespace Omni
{
  class BlurEffect : public SystemComponent<BlurEffect>
  {
  public:
    ~BlurEffect() { ScriptableDestroy(); }

    virtual void Initialize();
    virtual void Update(float dt);
    virtual void Destroy();

    static void Register();
    static void v8_Register();

    Property::Bool p_Active;

  };

} //Namespace Omni
