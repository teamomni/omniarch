/******************************************************************************
Filename: Shape.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Shape.h"
#include <algorithm>

#define PI 3.14f

namespace Omni{
  // Empty constructor for the shape object
  Shape::Shape() {}

  // The shape is constructed without a body. This means it is purely a shape. No mass.
  Circle::Circle(float radius_) : Shape()
  {
    body = NULL;

    type = Type::Circle;

    radius = radius_;

    center = Vec2(0, 0);
  }

  // The shape has to be cloned so that each body has its own shape to point to
  // unsure if this is 100% necessary and might be slow
  Shape * Circle::Clone() const
  {
    return new Circle(radius);
  }

  // this is called when the shape is attached to a body
  void Circle::Initialize()
  {
    ComputeMass();
  }

  // a function that computes the mass of the body. this can only be called when it has a body.
  void Circle::ComputeMass()
  {
    float scale = body->GetOwner()->transform.scale.x;
    float scaledRadius = scale * radius;

    float density = body->material.density;
    float mass = density * PI * scaledRadius * scaledRadius;
    float inertia = mass * scaledRadius * scaledRadius; // need to verify
    body->massData.mass = mass;
    body->massData.inverseMass = mass ? 1.0f / mass : 0.0f; // make sure not to divide by 0

    body->massData.inertia = inertia;
    body->massData.inverseInertia = inertia ? 1.0f / inertia : 0.0f;
  }

  // empty because circles don't rotate lols
  void Circle::SetOrientation(float radians)
  {}

  // gettor for the type
  Shape::Type Circle::GetType() const
  {
    return type;
  }

  // Default constructor for polygons to make copies
  Polygon::Polygon() : Shape(), vertexCount(0), rotationMatrix()
  {
    type = Shape::Type::Polygon;
  }

  // Constructor for a rectangle takes in a width and a height
  Polygon::Polygon(float width, float height) : Shape(), rotationMatrix()
  {
    type = Shape::Type::Polygon;
    SetBox(width, height);
  }

  // Constructor for a weird polygon takes in an array of vertices (vec2) and the number of vertices
  Polygon::Polygon(Vec2 vertices_[], unsigned int vertexCount_) : Shape(), rotationMatrix()
  {
    type = Shape::Type::Polygon;
    Set(vertices_, vertexCount_);
  }

  // Constructor for a weird polygon takes in a vector of vertices (vec2)
  Polygon::Polygon(Property::Vec2List vec2vector) : Shape(), rotationMatrix()
  {
    type = Shape::Type::Polygon;
    Set(vec2vector.data(), vec2vector.size());
  }

  // Create a new polygon with the same vertex list and count
  Shape *Polygon::Clone() const
  {
    Polygon *ret = new Polygon;
    for (unsigned int i = 0; i < vertexCount; ++i)
    {
      ret->vertices[i] = vertices[i];
      ret->normals[i] = normals[i];
    }
    ret->vertexCount = vertexCount;
    ret->center = center;

    return ret;
  }

  // this is called when the shape is attached to a body
  void Polygon::Initialize()
  {
    ComputeMass();
  }

  // a function that computes the mass of the body. this can only be called when it has a body.
  // for polygons, it calculates the SA of all the different triangles to find total SA
  // UNTESTED
  void Polygon::ComputeMass()
  {
    // store the area here
    float totalArea = 0.0f;
    float density = body->material.density;
    Handlet<Entity> owner = body->GetOwner(); // Y U NULL

    Matrix3 scale(body->GetOwner()->transform.scale.x, body->GetOwner()->transform.scale.y);

    // loop through for as many sides as there are, taking 0,0 as the third point on the triangle
    for (unsigned int i = 0; i < vertexCount; ++i)
    {
      Vec2 vertexA = scale * vertices[i];
      Vec2 vertexB = scale * vertices[(i + 1) % vertexCount];

      float area = 0.5f * (vertexA.x * vertexB.y - vertexB.x * vertexA.y);
      totalArea += fabs(area);
    }


    float mass = density * totalArea;
    float inertia = mass * 0.1f; // need to read up

    body->massData.mass = mass;
    body->massData.inverseMass = mass ? 1.0f / mass : 0.0f; // make sure not to divide by 0

    body->massData.inertia = inertia;
    body->massData.inverseInertia = inertia ? 1.0f / inertia : 0.0f;
  }

  // modify the stored rotation matrix (for transforming into an AABB instead of OBB)
  void Polygon::SetOrientation(float radians)
  {
    rotationMatrix.Rotate(radians);
  }

  Shape::Type Polygon::GetType() const
  {
    return type;
  }

  // sets the vertices to a rectangle
  void Polygon::SetBox(float width, float height)
  {
    float hw = width / 2.0f;  // half width
    float hh = height / 2.0f; // half height

    // set vertices, starting top right, going anti-clock
    vertices[0].Set(hw, hh);
    vertices[1].Set(-hw, hh);
    vertices[2].Set(-hw, -hh);
    vertices[3].Set(hw, -hh);

    // set normals, going from the top and going anti-clock
    normals[0].Set(0.0f, 1.0f);
    normals[1].Set(-1.0f, 0.0f);
    normals[2].Set(0.0f, -1.0f);
    normals[3].Set(1.0f, 0.0f);

    // set center 
    center = { 0, 0 };

    // set vertex count too!
    vertexCount = 4;
  }

  // sets the vertices to a polygon
  void Polygon::Set(Vec2 vertices_[], unsigned int vertexCount_)
  {
    // first copy over the vertices
    for (unsigned i = 0; i < vertexCount_; ++i)
    {
      vertices[i] = vertices_[i];
    }

    // also set the new vertex count
    vertexCount = vertexCount_;

    // now setup the normals, taking the outward pointing normals
    for (unsigned i = 0; i < vertexCount; ++i)
    {
      Vec2 rightPoint = vertices[i];
      Vec2 leftPoint = vertices[(i + 1) % vertexCount];

      Face currentFace = { rightPoint, leftPoint };
      Vec2 normal = currentFace.findNormal();
      normal.Normalize();

      normals[i].Set(normal.x, normal.y);
    }

    // now set the center
    Vec2 total = { 0, 0 };
    for (unsigned i = 0; i < vertexCount; ++i)
    {
      total.x += vertices[i].x;
      total.y += vertices[i].y;
    }

    center.x = total.x / vertexCount;
    center.y = total.y / vertexCount;

    if (vertexCount)
    {
      vertexCount = vertexCount_;
    }
  }
  
}