/******************************************************************************
Filename: Editor.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//Juli Gregg

#pragma once

#include "stdafx.h"
#include "GFXRenderer.h"
#include "GFXDebugDraw.h"
#include "Handlet.h"

namespace Omni
{
  class Entity;
  class Graphics::GFXRenderer;
  class TwBar;
  class Space;
  class Editor
  {
    public:
      //Whether we are currently in editor mode
      bool m_active;

      //Tools
      bool m_TransformTool = true;

      Handlet<Entity> CreateFloor(Space *space);
      Handlet<Entity> CreateFloorAtMousePosition(Space *space);
      Handlet<Entity> CreateEntityAtMousePosition(Space *space, std::string archetype = "");

      void Initialize(bool active = true);
      void Update(void);
      void ShutDown(void);

      Vec2 initialMousePosition;

      // id of the last selected object
      size_t selectedID;

      Editor() : m_active(false), selectedID(~static_cast<size_t>(0)) {}
      ~Editor() {}

    private:

      struct EditorObj
      {
        Transform transform;
        D3DXCOLOR color;
      };

      //Editor Tweak Bar
      TwBar *m_editorBar;

      //Current Object
      Handlet<Entity> m_selected;

      friend class Graphics::GFXRenderer;

  };

} //Namespace Omni