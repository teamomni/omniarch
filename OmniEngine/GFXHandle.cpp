/******************************************************************************
Filename: GFXHandle.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// Juli Gregg
// GFXHandle.cpp

#include "stdafx.h"
#include "GFXHandle.h"

namespace Omni
{
namespace Graphics
{

  Handle::Handle()
  {
    type = OBJT::None;
    //-1 == NONE
    index = -1;
  }

  int Handle::operator*(void) const
  {
    return this->index;
  }

  Handle::Handle(const Handle& rhs)
  {
    type = rhs.type;
    index = rhs.index;
  }

  Handle& Handle::operator = (const Handle& rhs)
  {
    type = rhs.type;
    index = rhs.index;

    return *this;
  }


  bool Handle::operator==(const Handle& rhs) const
  {
    if (this->type == rhs.type && this->index == rhs.index)
      return true;
    return false;
  }

  bool Handle::operator!=(const Handle& rhs) const
  {
    if (this->type != rhs.type || this->index != rhs.index)
      return true;
    return false;
  }

  bool Handle::operator<(const Handle& rhs) const
  {
    if (this->index < rhs.index)
      return true;
    return false;
  }

  bool Handle::isNull(void) const
  {
    if (this->type == OBJT::None)
      return true;
    return false;
  }

  //Private Constructor
  Handle::Handle(OBJT type_, int index_) : type(type_), index(index_)
  {
  }

} //namespace Graphics
} //namespace Omni
