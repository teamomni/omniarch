/******************************************************************************
Filename: EventDispatcher.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "v8definitions.h"
#include <unordered_map>

namespace Omni
{
  class Entity;
  
  class EventListener
  {
  public:
    v8::Persistent<v8::Function, v8::CopyablePersistentTraits<v8::Function>> eventCallback;
    v8::Persistent<v8::Object, v8::CopyablePersistentTraits<v8::Object>> object;
    size_t ownerID;
    
    void Destroy() 
    { 
    }

    // returns true if an object has been destroyed
    int Call(v8::Local<v8::Object> eventObject);

    ~EventListener()
    {
      eventCallback.Reset();
      object.Reset();
    }
  };

  class EventDispatcher
  {
  public:
    enum class Type {
      Game,
      Space,
      Entity
    };

    EventDispatcher() : eventListeners() {}
    EventDispatcher(Type type_) : type(type_), eventListeners() {}
    
    void EventDispatcher::Destroy();

    // connect new event
    void ConnectEvent(std::string eventName, v8::Local<v8::Function> eventCallback, v8::Local<v8::Object> object_, Entity *owner);

    // dispatch event function
    void DispatchEvent(std::string eventName, v8::Local<v8::Object> eventObject);
    
    // get type function
    Type GetType() { return type; }

    // create a list to store listeners to remove
    std::list<EventListener> eventsToClear;

    // owner ID ONLY WORKS if owner is an entity
    size_t ownerID;

    // v8 dispatch event
    template <typename T>
    static void v8_DispatchEvent(const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      using namespace v8;

      Locker locker(info.GetIsolate());
      Isolate::Scope isolate_scope(info.GetIsolate());
      HandleScope handleScope(info.GetIsolate());

      // unwrapping and getting the dispatch holder
      T *dispatchHolder = Take<T>::FromHolder(info);

      // get the event name;
      Local<Value> check(info[0]);
      String::Utf8Value eventName(check);

      // get the event object
      Local<Object> eventObj = Local<Object>::Cast(info[1]);

      dispatchHolder->dispatcher->DispatchEvent(*eventName, eventObj);

    }

    Type type;

  private:
    // map of event listeners. key is name of event, value is linked list of functions
    std::unordered_map<std::string, std::list<EventListener>> eventListeners;
  
  };

}