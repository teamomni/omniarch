/******************************************************************************
Filename: DebugPShader.hlsl

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// Author : Juli Gregg
// Purpose: Debug Pixel Shader
// Date   : Fall 2014
// Type   : Pixel Shader

//-----------------------------------//
// Textures Should Be Binded on CPU  //
//-----------------------------------//
Texture2D Texture0 : register(t0); //Debug Texture

//-----------------------------------//
// Sample State Set on the CPU       //
//-----------------------------------//
SamplerState sampleState;

//-----------------------------------//
// Struct Passed from Vertex Shader  //
//-----------------------------------//
struct vOutput
{
  float4 position   : SV_POSITION;  //NDC Position
  float4 color      : COLOR;        //Base Color
  float2 texcoord   : TEXCOORD;     //Texture Coordinate
};

//-----------------------------------//
// Main Function of Pixel Shader     //
//-----------------------------------//
float4 PShader(vOutput input) : SV_Target0
{
  //Sample from Opaque Texture
  return input.color * Texture0.Sample(sampleState, input.texcoord);
}
