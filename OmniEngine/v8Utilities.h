/******************************************************************************
Filename: v8Utilities.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once
#include "v8Definitions.h"
#include "Vec2.h"

namespace Omni
{
  template <>
  struct Take<Vec2>
  {
    static Vec2 FromV8(LocalValue &val)
    {
      LocalObject vec2Obj = LocalObject::Cast(val);
      float newX = float(vec2Obj->Get(v8::String::NewFromUtf8(g_Isolate, "x"))->NumberValue());
      float newY = float(vec2Obj->Get(v8::String::NewFromUtf8(g_Isolate, "y"))->NumberValue());

      return Vec2(newX, newY);
    }

    static Vec2* FromHolder_NonComponent(const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      LocalObject obj = info.Holder();
      LocalExternal wrap = LocalExternal::Cast(obj->GetInternalField(0));
      Vec2 *ptr = static_cast<Vec2 *>(wrap->Value());

      return ptr;
    }
  };
}