/******************************************************************************
Filename: SpriteText.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "stdafx.h"
#include "Component.h"


namespace Omni
{

  //#pragma pack()
  class SpriteText : public SystemComponent <SpriteText>
  {

  public:
    virtual void Initialize();
    virtual void Update(float dt);
    virtual void Destroy();

    static void Register();

    Property::String p_Text = "Default Text";
    Property::String p_Font = "Courier New"; //Could be an enum?
    Property::Float p_Size = 12.0f;
    
    Property::Color p_Color = D3DXCOLOR(0,0,0,1);
                                   
    //Enum Properties?

    //internal color
    UINT32 m_Color;
    

  private:

  };

}