/******************************************************************************
Filename: Space.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include <list>
#include "Camera.h"
#include "Scriptable.h"
#include "Entity.h"
#include "EventDispatcher.h"
#include "ISerializable.h"

namespace Omni
{
  class Space : public Scriptable<Space>, public ISerializable
  {
  public:
    Space(std::string name_);
    ~Space();

    // Name that we can find the space by later
    const std::string name;

    bool destroying = false;

    std::list<size_t> vacancies;

    // Entity methods
    Handlet<Entity> CreateEntity(std::string name);
    Handlet<Entity> CreateEntityAtPosition(std::string name, Vec2 position, std::string archetype = "");
    // TODO: Create entity based on archetype(?)
    inline std::vector<Entity> &GetAllEntities()
    {
      return entities;
    }
    inline std::list<size_t> &GetDestroyingEntities()
    {
      return destroyingEntities;
    }
    Handlet<Entity> GetEntityByName(std::string name);
    Handlet<Entity> GetEntityByHandle(Handlet<Entity> handle);
    inline unsigned GetEntityCount() { return entities.size(); }
    void MarkEntityForDeletion(Entity *entity);

    void SpawnCreatedEntities();

    std::vector<Handlet<Entity> >GetEntitiesByName(std::string name);
    std::vector<Handlet<Entity> >GetAllEntityHandles();

    static void v8_GetCamera(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);
    static void v8_Register();

    Vec2 GetMousePosition();
    Vec2 GetPointInSpace(const Vec2 &point);
    bool QueryOnScreen(Entity *target);

    void Cleanup();

    Camera camera;
    Camera initialCamera;
    size_t index;
    float depth = 0.0f;
    bool paused = false;

    std::vector<std::tuple<std::string, float, bool>> parallaxLayers;
    float parallaxOffset = 0.0f;

    void SpawnTweakBar();
    TwBar *tweakBar = nullptr;

    EventDispatcher *dispatcher;

    virtual void Serialize(Json::Value &root) const;

    static const size_t s_entityBuffer = 128;

  private:
    // List of all entities in this space
    std::vector<Entity> entities;
    std::list<Entity> creatingEntities;

    // List of all entities being destroyed
    std::list<size_t> destroyingEntities;

    // Named entity lookup table
    std::unordered_multimap<std::string, Handlet<Entity>> namedEntities;
  };
}
