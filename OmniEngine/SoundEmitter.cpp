/******************************************************************************
Filename: SoundEmitter.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#include "stdafx.h"
#include "SoundEmitter.h"
#include "Game.h"

namespace Omni
{
  bool SoundEmitter::playMusic = true;
  bool SoundEmitter::playSoundEffects = true;

  void SoundEmitter::Initialize()
  {
    //AddProperty("StopAndFade", Property::Type::Bool, offsetof(SoundEmitter, p_StopAndFade));

    p_ShowParameterBar = false;
    p_Pause = false;
    // p_Pitch = 1.0f;
    // p_Volume = 1.0f;
  }

  void SoundEmitter::Destroy()
  {
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    if (TwGetBarByName(barName.c_str()))
      TwDeleteBar(m_paramBar);
#endif

    StopSound();
	  if (m_soundCue)
      m_soundCue->~SoundCue();
    Game::Instance().DiscardModulePropertyHandles(Handlet<Component>(selfPtr));
  }

  void SoundEmitter::Register()
  {
    RegisterName("SoundEmitter", &Game::Instance().systemContainers, PropertyDefinitionMap({
        { "soundCue", PropertyDefinition::String("", offsetof(SoundEmitter, p_SoundCue), true) },
        { "startPlaying",        PropertyDefinition::Bool(  false, offsetof(SoundEmitter, p_StartPlaying), true) },
        { "pause",               PropertyDefinition::Bool(  false, offsetof(SoundEmitter, p_Pause), true) },
        { "showParameterBar",    PropertyDefinition::Bool(  false, offsetof(SoundEmitter, p_ShowParameterBar), true) },
        { "pitch",               PropertyDefinition::Float( 1.0f,  offsetof(SoundEmitter, p_Pitch), true) },
        { "volume",              PropertyDefinition::Float( 1.0f,  offsetof(SoundEmitter, p_Volume), true) },
        { "looping",             PropertyDefinition::Bool(  false, offsetof(SoundEmitter, p_Loop), true) },
        { "affectedByTimeScale", PropertyDefinition::Bool(  true,  offsetof(SoundEmitter, p_AffectedByTimeScale), true ) },
        { "isMusic",             PropertyDefinition::Bool(  false,  offsetof(SoundEmitter, p_IsMusic), true) }
    }), false);
  }

  void SoundEmitter::Update(float dt)
  {
    if ((selfPtr == nullptr) || (*selfPtr == nullptr))
      return;

    if (!playMusic && p_IsMusic)
    {
      StopSound();
      p_StartPlaying = false;
    }

    if (!playSoundEffects && !p_IsMusic)
    {
      StopSound();
      p_StartPlaying = false;
    }

    if (barName.empty())
    {
      barName = owner->name;
      barName.append("'s SoundCue Parameters");
    }

    if (m_oldName == "SoundEmitter" || p_SoundCue == "SoundEmitter")
      int i = 0;

    //Set Sound Cue
    if (p_SoundCue.empty())
      return;
    else if (!m_soundCue || p_SoundCue != m_oldName)
      SetSoundCue();

    if (p_ShowParameterBar)
    {
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
      //Create a tweakbar with parameters
      if (TwGetBarByName(barName.c_str()) == NULL)
      {
        MakeTweakBar();
      }
#endif
    }

#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    if (!p_ShowParameterBar && TwGetBarByName(barName.c_str()))
      TwDeleteBar(m_paramBar);
#endif

    //
    // Update Properties
    //

    if (p_StartPlaying)
    {
      if (!p_Loop)
      {
        p_StartPlaying = false;
      }
      PlaySound();
    }
    else
      if (p_Loop)
        StopSound();

    SetParameters();

    TogglePause(p_Pause || owner->space->paused);
    SetPitch(p_Pitch);
    SetVolume(p_Volume);

    m_oldName = p_SoundCue;
  }

  void SoundEmitter::SetSoundCue()
  {
    auto &game = Game::Instance();

    auto sSystem = game.GetSoundSystem();

    //TODO release sound cue if resetting it
    m_soundCue = sSystem->GetSoundCue(p_SoundCue, owner->name);

    for (int i = 0; i < m_soundCue->myEvent->paramCount; i++)
      m_paramVals.push_back(0.0f);

    p_SoundCue = name;

  }

  void SoundEmitter::PlaySound()
  {
    if (!m_soundCue->IsPlaying())
    {
      SoundUtilities::FMODErrCheck(m_soundCue->instance->start());
    }
  }

  void SoundEmitter::TogglePause(bool val)
  {
    if ((selfPtr == nullptr) || (*selfPtr == nullptr))
      return;
    
    bool paused;

    SoundUtilities::FMODErrCheck(m_soundCue->instance->getPaused(&paused), p_SoundCue);

    if (paused != val)
      SoundUtilities::FMODErrCheck(m_soundCue->instance->setPaused(val));

  }

  void SoundEmitter::SetPitch(float val)
  {
    if (p_AffectedByTimeScale)
    {
      val = val * Game::Instance().timescale;
    }
    SoundUtilities::FMODErrCheck(m_soundCue->instance->setPitch(val));
  }

  void SoundEmitter::SetVolume(float val)
  {
    SoundUtilities::FMODErrCheck(m_soundCue->instance->setVolume(val));
  }

  void SoundEmitter::SetParameters()
  {
    auto &game = Game::Instance();

    auto sSystem = game.GetSoundSystem();

    m_soundCue = sSystem->GetMySoundCue(owner->name);

    for (int i = 0; i < m_soundCue->myEvent->paramCount; i++)
    {
      std::string name = m_soundCue->myEvent->parameters[i].name;

	  if (name.empty())
		  break;;

      float val = m_paramVals[i];
      SoundUtilities::FMODErrCheck(m_soundCue->instance->setParameterValue(name.c_str(), val));
    }
  }


  void SoundEmitter::StopSound()
  {
    if (m_soundCue)
      SoundUtilities::FMODErrCheck(m_soundCue->instance->stop(FMOD_STUDIO_STOP_IMMEDIATE));
  }

  void SoundEmitter::MakeTweakBar()
  {
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    if (!m_soundCue)
      return;

    //Delete previous info if not previously made
    if (m_paramBar != NULL)
    {
      RemoveHandles();
    }

    m_paramBar = TwNewBar(barName.c_str());

    for (int i = 0; i < m_soundCue->myEvent->paramCount; i++)
    {
      auto *parameterHandle = Game::Instance().GetParameterHandle(handle, i);

      //TwAddVarRW(m_paramBar, m_soundCue->myEvent->parameters[i]->name, TW_TYPE_FLOAT, &m_paramVals[i], "");
      /*
      TwAddVarCB(m_paramBar, m_soundCue->myEvent->parameters[i].name, TW_TYPE_FLOAT, [](const void *value, void *clientData)
      {
        auto handle = reinterpret_cast<SoundUtilities::ParameterHandle *>(clientData);
        reinterpret_cast<SoundEmitter *>(Game::Instance().GetSystemInstance("SoundEmitter", handle->instance))->m_paramVals[handle->parameter] = *reinterpret_cast<const float *>(value);
      },
        [](void *value, void *clientData)
      {
        auto handle = reinterpret_cast<SoundUtilities::ParameterHandle *>(clientData);
        *reinterpret_cast<float *>(value) = reinterpret_cast<SoundEmitter *>(Game::Instance().GetSystemInstance("SoundEmitter", handle->instance))->m_paramVals[handle->parameter];
      },
        parameterHandle, // MAY LEAK (?)
        std::string(" step=0.01 min=").append(std::to_string(m_soundCue->myEvent->parameters[i].minimum)).append(" max=").append(std::to_string(m_soundCue->myEvent->parameters[i].maximum)).c_str());
      */
    }
#endif
  }

  void SoundEmitter::RemoveHandles()
  {
    Game::Instance().parameterHandles.remove_if([&](const SoundUtilities::ParameterHandle &eh)
    {
      return eh.instance == handle;
    });
  }

  void SoundEmitter::PlaySound(std::string cue)
  {
    p_SoundCue = cue;
    PlaySound();
  }

} //Namespace Omni
