/******************************************************************************
Filename: SystemContainer.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include <string>
#include "Handlet.h"

namespace Omni
{
  class Entity;
  class Component;
  typedef Handlet<Component> (*SystemCreationMethod)(Entity *);

  struct SystemContainer
  {
    std::string name;
    void **address;
    size_t elementSize;
    std::list<size_t> *vacancies;
    SystemCreationMethod createMethod;
    


    SystemContainer(std::string name_, void **address_, size_t elementSize_, std::list<size_t> *vacancies_, SystemCreationMethod createMethod_) : name(name_), address(address_), elementSize(elementSize_), vacancies(vacancies_), createMethod(createMethod_) {}
    SystemContainer() : name(""), address(nullptr), elementSize(0), vacancies(nullptr), createMethod(nullptr) {}
  };

}