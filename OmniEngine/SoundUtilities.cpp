/******************************************************************************
Filename: SoundUtilities.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "SoundUtilities.h"
#include <assert.h>

namespace Omni
{
  namespace SoundUtilities
  {
    
    void FMODErrCheck(FMOD_RESULT result, std::string name)
    {
      //Printing error messages
      if (result != FMOD_OK)
      {
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
        Console::Write("FMOD ERROR: ", Console::Color::Red);
        std::cout << "(" << result << ") " << FMOD_ErrorString(result) << std::endl;
        
        std::string message = "FMOD ERROR: " + name + " ";
        message += (FMOD_ErrorString(result));
        std::wstring msg(message.begin(), message.end());
        
        _ASSERT_EXPR(0, msg.c_str());
#endif
      }
    }
    
    bool SoundCue::IsPlaying()
    {
      PlayBackState state;

      instance->getPlaybackState(&state);

      //there are more options but for now we'll focuse on this
      if (state == PlayBackState::FMOD_STUDIO_PLAYBACK_STOPPED)
        return false;
      return true;
    }

  }//Namespace SoundUtilities

} //Namespace Omni