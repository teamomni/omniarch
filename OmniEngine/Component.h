/******************************************************************************
Filename: Component.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include "Space.h"
#include "Entity.h"
#include "Vec2.h"
#include "DirectXIncludes.h"
#include "SystemContainer.h"
#include "Property.h"
#include "v8Definitions.h"
#include "ISerializable.h"
#include "PropertyContainer.h"
#include "Game.h"

namespace Omni
{
  class Entity;
  class Space;
  class Physics;
  class Component;

  //
  // Component - component base class, common way to access all components
  //
  class Component : public ISerializable
  {
  public:
    enum class Type
    {
      Undefined,
      System,
      User
    };

    Component() {}
    virtual ~Component() = default;

    virtual void Initialize() = 0;
    virtual void Destroy() = 0;

    // store the property definitions and applies the property defaults to a component
    virtual void InsertProperties() = 0;

    void _Destroy()
    {
      active = false;
      *selfPtr = nullptr; // deleted differently depending on type
      Destroy();
    }
    virtual void Finalize() = 0;

    virtual Property::Bool GetProperty_Bool(const char *name) const = 0;
    virtual void SetProperty_Bool(const char *name, Property::Bool value) = 0;
    virtual Property::Int GetProperty_Int(const char *name) const = 0;
    virtual void SetProperty_Int(const char *name, Property::Int value) = 0;
    virtual Property::Unsigned GetProperty_Unsigned(const char *name) const = 0;
    virtual void SetProperty_Unsigned(const char *name, Property::Unsigned value) = 0;
    virtual Property::Float GetProperty_Float(const char *name) const = 0;
    virtual void SetProperty_Float(const char *name, Property::Float value) = 0;
    virtual Property::String GetProperty_String(const char *name) const = 0;
    virtual void SetProperty_String(const char *name, Property::String value) = 0;
    virtual Property::Vec2 GetProperty_Vec2(const char *name) const = 0;
    virtual void SetProperty_Vec2(const char *name, Property::Vec2 value) = 0;
    virtual Property::Vec2List GetProperty_Vec2List(const char *name) const = 0;
    virtual void SetProperty_Vec2List(const char *name, Property::Vec2List value) = 0;
    virtual Property::Color GetProperty_Color(const char *name) const = 0;
    virtual void SetProperty_Color(const char *name, Property::Color value) = 0;

    Type type;

    Space *space = nullptr;
    Component **selfPtr = nullptr;

    std::string name;
    PropertyMap properties;
    Handlet<Entity> GetOwner() const
    {
      return owner;
    }
    Handlet<Entity> owner;
    size_t handle;
    bool active = false;

    virtual void Serialize(Json::Value &root) const
    {
      // DEPRECATED
    }
  };

  //std::list<size_t> Component::s_vacancies;

}

/////
// THIS IS PROBABLY BAD BUT WHATEVER

#include "ComponentDefinition.h"

// OH GOD THIS WORKS
/////

namespace Omni {
  //
  // SystemComponent - components that are hard-coded into the engine for maximum performance
  //
  template<typename T>
  class SystemComponent : public Component
  {
  public:

    //SystemComponent<T>() { active = false; }

    inline void AddProperty(const char *name, Property::Type type, size_t offset, bool showInEditor = true)
    {
      properties.insert({ std::string(name), Property(type, offset, showInEditor) });
    }

    virtual Property::Bool GetProperty_Bool(const char *name) const
    {
      const Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Bool)
        return *prop.GetBool(this);
      return Property::Bool(false);
    }
    virtual void SetProperty_Bool(const char *name, Property::Bool value) {
      Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Bool)
        *prop.GetBool(this) = value;
    }
    virtual Property::Int GetProperty_Int(const char *name) const
    {
      const Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Int)
        return *prop.GetInt(this);
      return Property::Int(0);
    }
    virtual void SetProperty_Int(const char *name, Property::Int value)
    {
      const Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Int)
        *prop.GetInt(this) = value;
    }
    virtual Property::Unsigned GetProperty_Unsigned(const char *name) const
    {
      const Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Unsigned)
        return *prop.GetUnsigned(this);
      return Property::Unsigned(0);
    }
    virtual void SetProperty_Unsigned(const char *name, Property::Unsigned value)
    {
      const Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Unsigned)
        *prop.GetUnsigned(this) = value;
    }
    virtual Property::Float GetProperty_Float(const char *name) const
    {
      const Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Float)
        return *prop.GetFloat(this);
      return Property::Float(0);
    }
    virtual void SetProperty_Float(const char *name, Property::Float value)
    {
      Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Float)
        *prop.GetFloat(this) = value;
    }
    virtual Property::String GetProperty_String(const char *name) const
    {
      const Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::String)
        return *prop.GetString(this);
      return Property::String("");
    }
    virtual void SetProperty_String(const char *name, Property::String value)
    {
      Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::String)
        *prop.GetString(this) = value;
    }
    virtual Property::Vec2 GetProperty_Vec2(const char *name) const
    {
      const Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Vec2)
        return *prop.GetVec2(this);
      return Property::Vec2();
    }
    virtual void SetProperty_Vec2(const char *name, Property::Vec2 value)
    {
      Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Vec2)
        *prop.GetVec2(this) = value;
    }
    virtual Property::Vec2List GetProperty_Vec2List(const char *name) const
    {
      const Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Vec2List)
        return *prop.GetVec2List(this);
      return Property::Vec2List();
    }
    virtual void SetProperty_Vec2List(const char *name, Property::Vec2List value)
    {
      Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Vec2List)
        *prop.GetVec2List(this) = value;
    }
    virtual Property::Color GetProperty_Color(const char *name) const
    {
      const Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Color)
        return *prop.GetColor(this);
      return Property::Color();
    }
    virtual void SetProperty_Color(const char *name, Property::Color value)
    {
      Property &prop = properties.at(std::string(name));
      if (prop.type == Property::Type::Color)
        *prop.GetColor(this) = value;
    }

    // v8 stuff
    v8::Persistent<v8::Object, v8::CopyablePersistentTraits<v8::Object>> persObj;
    static v8::Persistent<v8::ObjectTemplate, v8::CopyablePersistentTraits<v8::ObjectTemplate>> objTemplate;

    size_t index;

    void ScriptableDestroy() 
    { 
      using namespace v8;
      auto &game = Game::Instance();

      Locker locker(game.isolate);
      Isolate::Scope isolate_scope(game.isolate);
      HandleScope handleScope(game.isolate);

      Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

      persObj.Reset(); 
    }

    static void ScriptableShutdown() 
    { 
      using namespace v8;
      auto &game = Game::Instance();

      Locker locker(game.isolate);
      Isolate::Scope isolate_scope(game.isolate);
      HandleScope handleScope(game.isolate);

      Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

      objTemplate.Reset(); 
    }

    v8::Handle<v8::Object> v8_GetObject()
    {
      using namespace v8;
      auto &game = Game::Instance();

      Locker locker(game.isolate);
      Isolate::Scope isolate_scope(game.isolate);
      EscapableHandleScope handleScope(game.isolate);

      Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

      // if we don't have a persistent object yet, make it
      if (persObj.IsEmpty())
      {
        Local<ObjectTemplate> newTemplate = Local<ObjectTemplate>::New(game.isolate, objTemplate);
        Local<Object> newObject = newTemplate->NewInstance();
      
        persObj.Reset(game.isolate, newObject);
      }

      Local<Object> returned = Local<Object>::New(game.isolate, persObj);

      // return handle_scope.Escape<Object>(newObj);
      return handleScope.Escape<Object>(returned);
    }

    // This lets us get the list of all existing components of this type
    static std::vector<T> &GetInstances()
    {
      return s_instances;
    }

    // Helper function to free the components vector when the program is shutting down
    static void Cleanup()
    {
      s_components.clear();
    }

    // Where all AddProperty()s should be called, NOT in the constructor
    virtual void Initialize() = 0;

    // Will be called every frame, or wherever this System's Run() is called
    virtual void Update(float dt) = 0;

    virtual void Destroy() = 0;

    virtual void Finalize()
    {
      s_vacancies.push_front(index);
    }

    // store the property definitions and applies the property defaults to a component
    virtual void InsertProperties()
    {
      auto &game = Game::Instance();

      // store the property definitions
      //createdModule->properties = game.componentDefinitions[moduleName].properties;
      for (auto const &propDefPair : game.componentDefinitions[name].properties)
      {
        properties.insert({ propDefPair.first, Property(propDefPair.second.type, propDefPair.second.offset, propDefPair.second.showInEditor) });
        // SET DEFAULT VALUE HERE
        switch (propDefPair.second.type)
        {
        case Property::Type::Bool:
          SetProperty_Bool(propDefPair.first.c_str(), propDefPair.second.v_bool);
          break;
        case Property::Type::Color:
          SetProperty_Color(propDefPair.first.c_str(), propDefPair.second.v_color);
          // set to default
          break;
        case Property::Type::Float:
          SetProperty_Float(propDefPair.first.c_str(), propDefPair.second.v_float);
          break;
        case Property::Type::Int:
          SetProperty_Int(propDefPair.first.c_str(), propDefPair.second.v_int);
          break;
        case Property::Type::String:
          SetProperty_String(propDefPair.first.c_str(), propDefPair.second.v_string);
          break;
        case Property::Type::Undefined:
          // TODO: ERROR?!?!?!
          break;
        case Property::Type::Unsigned:
          SetProperty_Unsigned(propDefPair.first.c_str(), propDefPair.second.v_unsigned);
          break;
        case Property::Type::Vec2:
          // set to (0, 0)
          break;
        case Property::Type::Vec2List:
          // nope
          break;
        }
      }
    }

    // This is what should be called to create the component.
    static Handlet<Component> Create(Entity *owner)
    {
      using namespace v8;

      auto &game = Game::Instance();
      Locker locker(game.isolate);
      Isolate::Scope isolate_scope(game.isolate);
      HandleScope handleScope(game.isolate);

      Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

      if (s_vacancies.empty())
      {
        size_t entCap = s_instances.capacity();
        s_instances.push_back(T());

        s_instances.back().active = true;
        s_instances.back().name = s_name;
        s_instances.back().owner = owner->selfPtr;
        s_instances.back().properties.clear();
        //s_instances.back().properties = Game::Instance().componentDefinitions[s_name].properties;

        // inserts and stores the prop definitions, and also applies defaults
        s_instances.back().InsertProperties();
        
        s_instances.back().type = Component::Type::System;
        s_instances.back().selfPtr = new Component *(&s_instances.back());
        s_instances.back().index = s_instances.size() - 1;
        if (s_instances.capacity() > entCap)
        {
          // update all selfPtrs of entities
          for (auto &comp : s_instances)
            if (comp.active)
              *comp.selfPtr = &comp;
        }
        return Handlet<Component>(s_instances.back().selfPtr);
      }
      size_t ind = s_vacancies.front();
      Component** oldSelfPtr = s_instances[ind].selfPtr;
      
      s_vacancies.pop_front();
      s_instances[ind] = T();
      s_instances[ind].active = true;
      s_instances[ind].name = s_name;
      s_instances[ind].owner = owner->selfPtr;
      s_instances[ind].properties.clear();

      // inserts and stores the prop definitions, and also applies defaults
      s_instances[ind].InsertProperties();

      for (auto const &propPair : Game::Instance().componentDefinitions[s_name].properties)
      {
        s_instances[ind].properties.insert({ propPair.first, Property(propPair.second.type, propPair.second.offset, propPair.second.showInEditor) });
      }
      s_instances[ind].type = Component::Type::System;
      s_instances[ind].index = ind;

      // moving the self pointer over to the newly created object
      *oldSelfPtr = &s_instances[ind];
      s_instances[ind].selfPtr = oldSelfPtr; // deleted in component::shutdown
      return Handlet<Component>(s_instances[ind].selfPtr);
    }

    // This method will be called from the main game loop update function thing
    static void Run(float dt)
    {
      for (size_t i = 0; i < s_instances.size(); ++i)
      {
        auto &component = s_instances[i];
        if (component.active && component.owner.isValid() && ((!component.owner->space->paused && s_skipIfPaused) || (!s_skipIfPaused)))
          component.Update(dt);
      }
    }

    static void Shutdown()
    {
      for (auto &instance : s_instances)
      {
        // clear the self ptr, regardless of whether it's active or not
        delete instance.selfPtr;
        if (instance.active)
          instance.Destroy();
      }

      s_instances.clear();
      s_vacancies.clear();
      ScriptableShutdown();
    }

    static void RegisterName(std::string name, std::unordered_map<std::string, SystemContainer> *systemContainers, PropertyDefinitionMap properties_, bool skipIfPaused = true)
    {
      s_name = name;
      s_instances.reserve(1024);
      s_skipIfPaused = skipIfPaused;
      Game::Instance().RegisterComponent(s_name, ComponentDefinition(s_name, SystemComponent<T>::Create, properties_));
    }

    static void RegisterName(std::string name, std::unordered_map<std::string, SystemContainer> *systemContainers)
    {
      s_name = name;
      s_instances.reserve(1024);
      Game::Instance().RegisterComponent(s_name, ComponentDefinition(s_name, SystemComponent<T>::Create));
    }

    static void ExposeProperties()
    {
      using namespace v8;

      // first create the object template
      auto &game = Game::Instance();

      Locker locker(game.isolate);
      Isolate::Scope isolate_scope(game.isolate);
      HandleScope handleScope(game.isolate);

      Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

      LocalObjectTemplate objTemplate_temp;

      // if there's nothing in the obj template right now, make a new one
      if (objTemplate.IsEmpty())
      {
        objTemplate_temp = ObjectTemplate::New(game.isolate);
        objTemplate_temp->SetInternalFieldCount(1);
      }
      // else, get it from the object
      else
        objTemplate_temp = LocalObjectTemplate::New(game.isolate, objTemplate);

      // loop through all the properties
      auto &properties = game.componentDefinitions[s_name].properties;
      for (auto &property : properties)
      {
        auto gettor = property.second.CreateGettor();
        auto settor = property.second.CreateSettor();

        // check to see if type is even supported
        if (gettor)
        {
          if (settor)
          {
            objTemplate_temp->SetAccessor(v8::String::NewFromUtf8(game.isolate, property.first.c_str()), AccessorGetterCallback(gettor), AccessorSetterCallback(settor));
          }

          else
          {
            objTemplate_temp->SetAccessor(v8::String::NewFromUtf8(game.isolate, property.first.c_str()), AccessorGetterCallback(gettor), 0);
          }
        }
      }

      objTemplate.Reset(game.isolate, objTemplate_temp);
    }

    static void *s_instancePointer;

  private:
    // Static vector of all instances of this system's type
    static std::vector<T> s_instances;
    static std::string s_name;
    static std::list<size_t> s_vacancies;
    static bool s_skipIfPaused;
    //static std::unordered_map<size_t, T> s_creatingInstances;
  };

  // We need to actually instantiate the instances vector because C++ is weird
  template<typename T>
  std::vector<T> SystemComponent<T>::s_instances;

  template<typename T>
  std::string SystemComponent<T>::s_name;

  template<typename T>
  void *SystemComponent<T>::s_instancePointer = nullptr;

  template<typename T>
  std::list<size_t> SystemComponent<T>::s_vacancies;

  template<typename T>
  bool SystemComponent<T>::s_skipIfPaused = true;

  template<typename T>
  v8::Persistent<v8::ObjectTemplate, v8::CopyablePersistentTraits<v8::ObjectTemplate>>SystemComponent<T>::objTemplate;
  //v8::Handle<v8::ObjectTemplate> System<T>::objTemplate;

  //
  // UserComponent - components that are loaded in on-the-fly through external scripting languages
  //
  class UserComponent : public Component
  {
  public:
    static UserComponent *Create(Entity *owner, std::string moduleName);

    virtual void Initialize();
    virtual void Destroy()
    {
      using namespace v8;
      auto &game = Game::Instance();

      Locker locker(game.isolate);
      Isolate::Scope isolate_scope(game.isolate);
      HandleScope handleScope(game.isolate);

      Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

      Game::Instance().DiscardModulePropertyHandles(Handlet<Component>(selfPtr));
      v8_module.Reset();
      v8_owner.Reset();
      v8_init.Reset();
      persObj.Reset();
    }

    virtual void Finalize();
    virtual void InsertProperties();

    virtual Property::Bool GetProperty_Bool(const char *name) const;
    virtual void SetProperty_Bool(const char *name, Property::Bool value);
    virtual Property::Int GetProperty_Int(const char *name) const;
    virtual void SetProperty_Int(const char *name, Property::Int value);
    virtual Property::Unsigned GetProperty_Unsigned(const char *name) const;
    virtual void SetProperty_Unsigned(const char *name, Property::Unsigned value);
    virtual Property::Float GetProperty_Float(const char *name) const;
    virtual void SetProperty_Float(const char *name, Property::Float value);
    virtual Property::String GetProperty_String(const char *name) const;
    virtual void SetProperty_String(const char *name, Property::String value);
    virtual Property::Vec2 GetProperty_Vec2(const char *name) const;
    virtual void SetProperty_Vec2(const char *name, Property::Vec2 value);
    virtual Property::Vec2List GetProperty_Vec2List(const char *name) const;
    virtual void SetProperty_Vec2List(const char *name, Property::Vec2List value);
    virtual Property::Color GetProperty_Color(const char *name) const;
    virtual void SetProperty_Color(const char *name, Property::Color value);

    // TODO: list of events
    // TODO: Attach() events
    // TODO: Detach() events (?)

    PersistentObject v8_module;
    PersistentObject v8_owner;
    PersistentFunction v8_init;
    PersistentObject persObj;

    // static map of creation functions
    static std::unordered_map<std::string, PersistentFunction> creationFunctions;

    // static template for the cpp user component
    static PersistentObjectTemplate objTemplate_cpp;

    static void v8_Register();

    v8::Handle<v8::Object> v8_GetObject()
    {
      using namespace v8;
      auto &game = Game::Instance();

      Locker locker(game.isolate);
      Isolate::Scope isolate_scope(game.isolate);
      EscapableHandleScope handleScope(game.isolate);

      Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

      // if we don't have a persistent object yet, make it
      if (persObj.IsEmpty())
      {
        Local<ObjectTemplate> newTemplate = Local<ObjectTemplate>::New(game.isolate, objTemplate_cpp);
        Local<Object> newObject = newTemplate->NewInstance();

        persObj.Reset(game.isolate, newObject);
      }

      Local<Object> returned = Local<Object>::New(game.isolate, persObj);

      return handleScope.Escape<Object>(returned);
    }

    static void Shutdown()
    {
      // clear all the creation functions
      for (auto &handle : creationFunctions)
      {
        handle.second.Reset();
      }

      creationFunctions.clear();
      objTemplate_cpp.Reset();
    }

  private:
  };

}

/*

GAME      - the single, overall game container thing. contains a list of SPACEs
SPACE     - a container of ENTITYs
ENTITY    - a container of COMPONENTS (both SYSTEMs and MODULEs)
COMPONENT - a piece of functionality. either a SYSTEM or a MODULE. has PROPERTYs
PROPERTY  - a unit of data. can be a string, float, int, unsigned, Vec2, etc.
SYSTEM    - a fast, written-in-C++ COMPONENT. each frame (or draw call, whatever you want), the game will call that system's static Run function, which calls the Update function on each instance of that system in memory
MODULE    - an instance of a SCRIPT. slightly-slower COMPONENT that hooks functions written in JavaScript into various EVENTs
SCRIPT    - a JavaScript script for a COMPONENT (technically a MODULE) loaded into memory. creating instances of the script to attach to ENTITYs makes MODULEs
EVENT     - a place in the code where MODULEs "hook" functions into (this is still kind of shaky)

*/
