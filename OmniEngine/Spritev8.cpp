/******************************************************************************
Filename: Spritev8.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Sprite.h"
#include "Game.h"

using namespace v8;

namespace Omni
{
  void Sprite::v8_Register()
  {
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    Local<ObjectTemplate> objTemplate_temp;

    // if there's nothing in the obj template right now, make a new one
    if (objTemplate.IsEmpty())
    {
      objTemplate_temp = ObjectTemplate::New(game.isolate);
      objTemplate_temp->SetInternalFieldCount(1);
    }
    // else, get it from the object
    else
      objTemplate_temp = LocalObjectTemplate::New(game.isolate, objTemplate);

    // REGISTER FUNCTIIONS HERE

    objTemplate.Reset(game.isolate, objTemplate_temp);

    ExposeProperties();
  }

  v8_RegisterComponentWithEntity(Sprite);

}