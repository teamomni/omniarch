/******************************************************************************
Filename: Animation.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include "Image.h"
#include "HitBox.h"

namespace Omni
{
  struct HitBoxData
  {
    HitBox::Type type;
    Vec2 center;
    float radius = 0.0f;
    Vec2 scale;
  };

  struct Frame
  {
    float duration;
    std::string image;
    std::vector<HitBoxData> hitboxes;
    std::vector<Vec2> points;
    std::vector<std::string> flags;
    std::string soundCue;
    int damage = 0;
    bool inheritHitboxes = false;

    inline bool HasFlag(std::string flag)
    {
      return std::find(flags.begin(), flags.end(), flag) != flags.end();
    }
  };

  struct Animation
  {
    std::string name;
    bool looping = false;
    std::vector<Frame> frames;
    bool ignoreMirrorX = false;
    bool ignoreMirrorY = false;
  };

}
