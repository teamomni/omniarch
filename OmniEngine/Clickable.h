/******************************************************************************
Filename: Clickable.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "Input.h"
#include "Scriptable.h"
#include "Game.h"

namespace Omni
{
  class Entity;

  class Clickable : public Scriptable<Clickable>
  {
  public:
    Clickable();
    ~Clickable();

    // component functions
    void Update();
    void Initialize();
    void Destroy();
    static void Register();

    // mouse states
    ButtonState mouseStates[(unsigned)Mouse::Button::_Last];

    // v8 register function
    static void Clickable::v8_Register();

    // relative mouse mosition
    static Vec2 relativePositionAtPressed;

    // static step function to check what is clicked
    static void Step();

    // static pointer to the currently clicked entity
    static size_t currentlySelectedID[unsigned(Mouse::Button::_Last)];
    static bool currentlyDown[unsigned(Mouse::Button::_Last)];

    // static 2D array of pointers to z-depth/entity pairs for each mouse state
    static std::vector<std::pair < float, size_t>> clickedThisFrame[unsigned(Mouse::Button::_Last)];
  
    // owner handle
    Handlet<Entity> ownerHandle;

    // owner's space
    Space *space;

    // get owner function
    Handlet<Entity> GetOwner();
  };

}