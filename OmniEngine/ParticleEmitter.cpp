/******************************************************************************
Filename: ParticleEmitter.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Michael Tyler

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "ParticleEmitter.h"
#include <random>

namespace Omni
{

  ParticleEmitter::ParticleEmitter() : m_isActive(true), m_additive(true), m_isFlippedX(false), m_isFlippedY(false),
                                       p_Image("_particle_full"), p_Depth(1.0f),
                                       m_scale(Vec2(1, 1)), m_rot(0.0f), m_emitRate(100.0f), m_emitCount(0),
                                       m_spawned(0), m_posType(0), m_minTime(1.0f), 
                                       m_maxTime(2.0f), m_radX(1.0f), m_radY(1.0f),
                                       m_width(0.0f), m_height(0.0f),
                                       m_MaxOffsetPos(Vec2(0.0f, 0.0f)),
                                       m_minStartCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)),
                                       m_maxStartCol(D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f)),
                                       m_minEndCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f)),
                                       m_maxEndCol(D3DXCOLOR(0.0f, 0.0f, 0.0f, 0.0f)),
                                       m_globalAcceleration(Vec2(0.0f, 0.0f)),
                                       m_isAttracting(false), m_strength(1), m_minDistance(0.01f)
  {
    //particles.Generate(20000);
    //SetImage(p_Image);
    //m_isActive = true;
    //m_additive = true;
    //m_isFlipped = false;
    //m_scale = Vec2(1,1);
    //m_emitRate = 100.0f;
    //m_emitCount = 0;
    //m_minTime = 1.0f;
    //m_maxTime = 2.0f; 
    //m_radX = 0.0f;
    //m_radY = 0.0f;
    //m_MaxOffsetPos  = Vec2(0.0f, 0.0f);
    //m_minStartVel   = Vec2(-1.0f, 1.0f);
    //m_maxStartVel   = Vec2(1.0f, 1.0f);
    //m_minStartCol   = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
    //m_maxStartCol   = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
    //m_minEndCol     = D3DXCOLOR(0.0f, 0.0f, 1.0f, 0.0f);
    //m_maxEndCol     = D3DXCOLOR(0.0f, 1.0f, 1.0f, 0.0f);
    //m_globalAcceleration = Vec2(0.0f, 0.0f);
    //m_isAttracting  = false;
    //m_strength      = 1;
    //m_minDistance   = 1;
    //m_maxDistance   = 1;
  }
  
  ParticleEmitter::~ParticleEmitter()
  {
    ScriptableDestroy();
  }

  void ParticleEmitter::Update(float dt)
  {

    if (!image)
      return;

    if (!owner->space->paused)
    {
      // DEFAULT particle behaviour
      if (behaviourType == BehaviourType::Default)
      {
        // Spawn new Particles
        if (m_isActive)
          emit(dt);
        else
          m_spawned = 0;

        // Update particle values
        TimeUpdate(dt);
        ColorUpdate(dt);

        if (m_isAttracting)
          AttractorUpdate(dt);

        AccelUpdate(dt);
      }

      // TRAIL particle behaviour
      else if (behaviourType == BehaviourType::Trail)
      {
        if (!m_hasSpawnedParticles)
        {
          Vec2 ownerTrans = owner->transform.translation;
          float deltaAlpha = (m_endingAlpha - m_startingAlpha) / particles.m_count;
          double deltaScaleY = m_scale.y / particles.m_count;
          float currentAlpha = m_startingAlpha;
          double currentScaleY = m_scale.y;

          for (unsigned i = 0; i < particles.m_count; ++i)
          {
            particles.Wake(i);
            particles.m_pos[i] = ownerTrans;
            startPositions.push_back(ownerTrans);
            particles.m_scale[i] = Vec2(m_scale.x, static_cast<float>(currentScaleY));

            // set colour
            particles.m_col[i].r = m_minStartCol.r;
            particles.m_col[i].g = m_minStartCol.g;
            particles.m_col[i].b = m_minStartCol.b;
            particles.m_col[i].a = currentAlpha;

            currentAlpha += deltaAlpha;
            currentScaleY -= deltaScaleY;
          }

          m_hasSpawnedParticles = true;
        }

        // if inactive, don't do anything. don't even draw!
        if (!m_isActive)
          return;

        Vec2 endPos = owner->transform.translation;

        unsigned particlesCount = particles.m_count;
        // loop through all particles, and update their position from the back forwards
        for (unsigned i = 0; i < particlesCount; ++i)
        {
          Vec2 &currentPos = particles.m_pos[i];
          Vec2 &currentScale = particles.m_scale[i];
          float &currentRot = particles.m_rot[i];

          // now scale the thing and place it between start and end
          Vec2 diff = endPos - startPositions[i];

          // move the start towards the end
          startPositions[i].x += diff.x * m_stretchSpeed;
          startPositions[i].y += diff.y * m_stretchSpeed;

          // update the difference
          diff.x = endPos.x - startPositions[i].x;
          diff.y = endPos.y - startPositions[i].y;

          float length = sqrtf(diff.x * diff.x + diff.y * diff.y);

          currentScale.x = length;
          currentPos.x = startPositions[i].x + diff.x / 2;
          currentPos.y = startPositions[i].y + diff.y / 2;

          endPos = startPositions[i];

          // set rotation
          float rotation = std::atan2(diff.y, diff.x);
          currentRot = rotation;
        }
      }
    }

    unsigned endPos = particles.m_countAlive;

    float size = owner->space->camera.GetSize();
    Vec2 cPos = owner->space->camera.transform.translation;
    Vec2 window = Game::Instance().GetWindowSystem()->GetClientSize();

    float screenHalfWidth = (size * window.x / window.y) / 2.0f;
    float screenHalfHeight = size / 2.0f;

    Transform trans = owner->transform;
    Vec2 finalScale = trans.scale;

    finalScale.x *= image->dimensions.x / image->pixelsPerUnit;
    finalScale.y *= image->dimensions.y / image->pixelsPerUnit;

    Vec2 finalTranslation = trans.translation;
    finalTranslation.x -= image->offset.x * finalScale.x;
    finalTranslation.y -= image->offset.y * finalScale.y;

    float ownerXSize = finalScale.x / 2;
    float ownerYSize = finalScale.y / 2;

    float totalX = screenHalfWidth + ownerXSize;
    float totalY = screenHalfHeight + ownerYSize;

    float totalRadiusSquared = totalX * totalX + totalY * totalY;

    float seprationSquared = (finalTranslation - cPos).LengthSquared();

    if (seprationSquared > totalRadiusSquared)
      return;
    bool hasSprite = owner->HasComponent("Sprite");
    float depth = p_Depth + float(hasSprite ? owner->GetComponent("Sprite")->GetProperty_Float("depth") : 0.0f);

    // Stuff to Draw particles
    std::vector<Vec2> translations_;      // Holds data for each particles translaations
    std::vector<D3DXCOLOR> colors_;       // Holds data for each particles color
    std::vector<Vec2> scale_;             // Holds data for each particles scale
    std::vector<float> rot_;              // Holds data for each particles rotation

    for (unsigned i = 0; i < endPos; ++i)
    {
      translations_.push_back(particles.m_pos[i]);
      colors_.push_back(particles.m_col[i]);
      scale_.push_back(particles.m_scale[i]);
      rot_.push_back(particles.m_rot[i]);
    }

    // Dispatch to the render objects to draw
    if (translations_.size() > 0 )
    {
      RenderState &rs = Game::Instance().GetUpdateRenderState();
      rs.objects.push_back(RenderObject(image, 
                                        translations_, 
                                        colors_, 
                                        scale_,
                                        rot_,
                                        m_additive, 
                                        owner->space->depth + depth, 
                                        owner->space->index, 
                                        m_isFlippedX,
                                        m_isFlippedY));
    }
  }

  void ParticleEmitter::Initialize()
  {
    particles.Generate(particles.m_count);
    SetImage(p_Image);
    //m_isActive = true;
    //m_additive = false;
    //m_isFlippedX = false;
    //m_isFlippedY = false;
    ////m_scale = Vec2(1,1);
    ////m_rot = 0.0f;
    ////p_Depth = 1;
    ////m_emitRate = 100.0f;
    ////m_emitCount = 0;
    ////m_spawned = 0;
    ////m_posType = 0;
    ////m_minTime = 1.0f;
    ////m_maxTime = 2.0f;
    ////m_radX = 0.0f;
    ////m_radY = 0.0f;
    ////m_width = 0.0f;
    ////m_height = 0.0f;
    //m_MaxOffsetPos = Vec2(0.0f, 0.0f);
    //m_minStartVel = Vec2(-1.0f, 1.0f);
    //m_maxStartVel = Vec2(1.0f, 1.0f);
    //m_minStartCol = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
    //m_maxStartCol = D3DXCOLOR(1.0f, 1.0f, 1.0f, 1.0f);
    //m_minEndCol = D3DXCOLOR(0.0f, 0.0f, 1.0f, 0.0f);
    //m_maxEndCol = D3DXCOLOR(0.0f, 1.0f, 1.0f, 0.0f);
    //m_globalAcceleration = Vec2(0.0f, 0.0f);
    //m_isAttracting = false;
    //m_strength = 1;
    //m_minDistance = 1;
    m_hasSpawnedParticles = false;

    //m_maxDistance   = 1;
  }

  // Clean up Component on destruction
  void ParticleEmitter::Destroy()
  {
    ScriptableDestroy();
    particles.Clean();
  }

  void ParticleEmitter::Register()
  {
    RegisterName("ParticleEmitter", &Game::Instance().systemContainers, PropertyDefinitionMap({
      { "active",         PropertyDefinition::Bool(   true,   offsetof(ParticleEmitter, m_isActive),            true) },
      { "spriteFlippedX", PropertyDefinition::Bool(   false,  offsetof(ParticleEmitter, m_isFlippedX),          true) },
      { "spriteFlippedY", PropertyDefinition::Bool(   false,  offsetof(ParticleEmitter, m_isFlippedY),          true) },
      { "numParticles",   PropertyDefinition::Int(    100,    offsetof(ParticleEmitter, particles.m_count),     true) },    
      { "emitRate",       PropertyDefinition::Float(  100.0f, offsetof(ParticleEmitter, m_emitRate),            true) },
      { "emitCount",      PropertyDefinition::Int(    0,      offsetof(ParticleEmitter, m_emitCount),           true) },
      { "spawnType",      PropertyDefinition::Int(    0,      offsetof(ParticleEmitter, m_posType),             true) },
      { "image",          PropertyDefinition::String( "_particle_full",     offsetof(ParticleEmitter, p_Image), true) }, 
      { "depth",          PropertyDefinition::Float(  1.0f,   offsetof(ParticleEmitter, p_Depth),               true) }, 
      { "minLifeTime,",   PropertyDefinition::Float(  1.0f,   offsetof(ParticleEmitter, m_minTime),             true) },
      { "maxLifeTime,",   PropertyDefinition::Float(  1.0f,   offsetof(ParticleEmitter, m_maxTime),             true) },
      { "radX,",          PropertyDefinition::Float(  1.0f,   offsetof(ParticleEmitter, m_radX),                true) },
      { "radY,",          PropertyDefinition::Float(  1.0f,   offsetof(ParticleEmitter, m_radY),                true) },
      { "width,",         PropertyDefinition::Float(  1.0f,   offsetof(ParticleEmitter, m_width),               true) },
      { "height,",        PropertyDefinition::Float(  1.0f,   offsetof(ParticleEmitter, m_height),              true) },
      { "offsetPosition", PropertyDefinition::Vec2(   Vec2(), offsetof(ParticleEmitter, m_MaxOffsetPos),        true) },
      { "minVelocity",    PropertyDefinition::Vec2( Vec2(-1,1), offsetof(ParticleEmitter, m_minStartVel),       true) },
      { "maxVelocity",    PropertyDefinition::Vec2( Vec2(1,1),  offsetof(ParticleEmitter, m_maxStartVel),       true) },
      { "minPosition",    PropertyDefinition::Vec2( Vec2(0, 0), offsetof(ParticleEmitter, m_minStartPosition),  true) },
      { "maxPosition",    PropertyDefinition::Vec2( Vec2(0, 0), offsetof(ParticleEmitter, m_maxStartPosition),  true) },
      { "acceleration",   PropertyDefinition::Vec2(   Vec2(), offsetof(ParticleEmitter, m_globalAcceleration),  true) },
      { "minStartColor",  PropertyDefinition::Color(          offsetof(ParticleEmitter, m_minStartCol),         true) },
      { "maxStartColor",  PropertyDefinition::Color(          offsetof(ParticleEmitter, m_maxStartCol),         true) },
      { "minEndColor",    PropertyDefinition::Color(          offsetof(ParticleEmitter, m_minEndCol),           true) },
      { "maxEndColor",    PropertyDefinition::Color(          offsetof(ParticleEmitter, m_maxEndCol),           true) },
      { "attractor_On",   PropertyDefinition::Bool(   false,  offsetof(ParticleEmitter, m_isAttracting),        true) },
      { "followTransform",PropertyDefinition::Bool(   false,  offsetof(ParticleEmitter, m_followTransform),     true) },
      { "strength",       PropertyDefinition::Float(  1.0f,   offsetof(ParticleEmitter, m_strength),            true) },
      { "minDistance",    PropertyDefinition::Float(  0.01f,  offsetof(ParticleEmitter, m_minDistance),         true) },
      //{ "maxDistance",    PropertyDefinition::Float(  1.0f,   offsetof(ParticleEmitter, m_maxDistance),         true) },
      { "particleScale",  PropertyDefinition::Vec2(   Vec2(1,1), offsetof(ParticleEmitter, m_scale),            true) },
      { "minScalar",      PropertyDefinition::Float(  0.0f,   offsetof(ParticleEmitter, m_minScalar),           true) },
      { "maxScalar",      PropertyDefinition::Float(  0.0f,   offsetof(ParticleEmitter, m_maxScalar),           true) },
      { "scaleX",         PropertyDefinition::Bool(   true,   offsetof(ParticleEmitter, m_scaleX),              true) },
      { "scaleY",         PropertyDefinition::Bool(   true,   offsetof(ParticleEmitter, m_scaleY),              true) },
      { "spriteAdditive", PropertyDefinition::Bool(   false,  offsetof(ParticleEmitter, m_additive),            true) },
      { "behaviourType",  PropertyDefinition::Int(    0,      offsetof(ParticleEmitter, behaviourType),         true) },
      { "stretchSpeed",   PropertyDefinition::Float(  0.8f,   offsetof(ParticleEmitter, m_stretchSpeed),        true) },
      { "displayIfStatic",PropertyDefinition::Bool(   false,  offsetof(ParticleEmitter, m_displayIfStatic),     true) },
      { "startingAlpha",  PropertyDefinition::Float(  1.0f,   offsetof(ParticleEmitter, m_startingAlpha),       true) },
      { "endingAlpha",    PropertyDefinition::Float(  0.0f,   offsetof(ParticleEmitter, m_endingAlpha),         true) }
    }), false);
  }

  void ParticleEmitter::emit(float dt) 
  {
    const unsigned maxSpawn = static_cast<unsigned>(dt * m_emitRate);
    const unsigned startPos = particles.m_countAlive;
    unsigned endPos = min(startPos + maxSpawn, particles.m_count -1);

    if (m_emitCount != 0 && m_spawned >= m_emitCount)
    {
      endPos = m_emitCount;
      m_isActive = false;
      m_spawned = 0;
    }    

    switch (m_posType)
    {
      case PT_POS:
        PosGen(startPos, endPos);
        break;

      case PT_CIR:
        CirGen(startPos, endPos);
        break;

      default:
        BoxGen(startPos, endPos);
        break;
    }
        
    VelGen(startPos, endPos);
    ColorGen(startPos, endPos);
    TimeGen(startPos, endPos);
    ScaleGen(startPos, endPos);
    RotGen(startPos, endPos);

    // Wake Particles
    for (unsigned i = startPos; i < endPos; ++i, ++m_spawned)
      particles.Wake(i);
  }

  void ParticleEmitter::TimeUpdate(float dt)
  {
    // End position in contain that needs updating
    unsigned endPos = particles.m_countAlive;
    // Data from Particle class thats needed
    Time * __restrict time = particles.m_time;

    if (endPos == 0)
      return;

    for (unsigned i = 0; i < endPos; ++i)
    {
      time[i].x -= dt;
      time[i].z = 1.0f - (time[i].x * time[i].y);

      if (time[i].x < 0.0f)
      {
        // Put Particle to sleep
        particles.Kill(i);

        // Update endPos
        if (particles.m_countAlive < particles.m_count)
          endPos = particles.m_countAlive;
        else
          endPos = particles.m_count;
      }
    }
  }

  void ParticleEmitter::AttractorUpdate(float dt)
  {
    // End position in contain that needs updating
    unsigned endPos = particles.m_countAlive;
    // Data from Particle class thats needed
    Vec2 * __restrict acc = particles.m_acc;
    Vec2 * __restrict pos = particles.m_pos;

    float distance;
    Vec2 off;

    for (unsigned i = 0; i < endPos; ++i)
    {
      off = (owner->transform.translation + m_MaxOffsetPos) - pos[i];
      distance = (off * off);

      if (distance < m_minDistance)
        acc[i] = 0;
      else
        acc[i] = off * (1 / distance) * m_strength;
    }
  }

  void ParticleEmitter::AccelUpdate(float dt)
  {
    // End position in contain that needs updating
    unsigned endPos = particles.m_countAlive;
    // Data from Particle class thats needed
    Vec2 * __restrict acc = particles.m_acc;
    Vec2 * __restrict vel = particles.m_vel;
    Vec2 * __restrict pos = particles.m_pos;
    Vec2 * __restrict off = particles.m_offset;
    Vec2 trans = owner->transform.translation;

    const Vec2 globalAccel(dt * m_globalAcceleration.x, dt * m_globalAcceleration.y);

    for (unsigned i = 0; i < endPos; ++i)
      acc[i] += globalAccel;

    for (unsigned i = 0; i < endPos; ++i)
      vel[i] += acc[i] * dt;

    for (unsigned i = 0; i < endPos; ++i)
    {
      if (m_followTransform)
      {
        pos[i] = trans + off[i];
      }
      else
        pos[i] += vel[i] * dt;
    }
  }

  void ParticleEmitter::ColorUpdate(float dt)
  {
    // End position in contain that needs updating
    unsigned endPos = particles.m_countAlive;

    // Data from Particle class thats needed
    Time      * __restrict time     = particles.m_time;
    D3DXCOLOR * __restrict col      = particles.m_col;
    D3DXCOLOR * __restrict startcol = particles.m_startColor;
    D3DXCOLOR * __restrict endcol   = particles.m_endColor;

    for (unsigned i = 0; i < endPos; ++i)
      col[i] = startcol[i] * (1.0f - time[i].z) + endcol[i] * time[i].z;
  }

  void ParticleEmitter::TimeGen(unsigned startPos, unsigned endPos)
  {
    std::random_device rd;
    std::mt19937 gen(rd());

    if (m_minTime > m_maxTime)
      std::swap(m_minTime, m_maxTime);

    std::uniform_real_distribution<float> dis(m_minTime, m_maxTime);

    for (unsigned i = startPos; i < endPos; ++i)
    {
      particles.m_time[i].x = dis(gen);
      particles.m_time[i].z = 0.0f;
      particles.m_time[i].y = 1.0f / particles.m_time[i].x;
    }
  }

  void ParticleEmitter::ColorGen(unsigned startPos, unsigned endPos)
  {
    std::random_device rd;
    std::mt19937 gen(rd());

    if (m_minStartCol.r > m_maxStartCol.r)
      std::swap(m_minStartCol.r, m_maxStartCol.r);

    if (m_minStartCol.g > m_maxStartCol.g)
      std::swap(m_minStartCol.g, m_maxStartCol.g);

    if (m_minStartCol.b > m_maxStartCol.b)
      std::swap(m_minStartCol.b, m_maxStartCol.b);

    if (m_minStartCol.a > m_maxStartCol.a)
      std::swap(m_minStartCol.a, m_maxStartCol.a);

    std::uniform_real_distribution<float> scolorR(m_minStartCol.r, m_maxStartCol.r);
    std::uniform_real_distribution<float> scolorG(m_minStartCol.g, m_maxStartCol.g);
    std::uniform_real_distribution<float> scolorB(m_minStartCol.b, m_maxStartCol.b);
    std::uniform_real_distribution<float> scolorA(m_minStartCol.a, m_maxStartCol.a);

    if (m_minEndCol.r > m_maxEndCol.r)
      std::swap(m_minEndCol.r, m_maxEndCol.r);

    if (m_minEndCol.g > m_maxEndCol.g)
      std::swap(m_minEndCol.g, m_maxEndCol.g);

    if (m_minEndCol.b > m_maxEndCol.b)
      std::swap(m_minEndCol.b, m_maxEndCol.b);

    if (m_minEndCol.a > m_maxEndCol.a)
      std::swap(m_minEndCol.a, m_maxEndCol.a);

    std::uniform_real_distribution<float> ecolorR(m_minEndCol.r, m_maxEndCol.r);
    std::uniform_real_distribution<float> ecolorG(m_minEndCol.g, m_maxEndCol.g);
    std::uniform_real_distribution<float> ecolorB(m_minEndCol.b, m_maxEndCol.b);
    std::uniform_real_distribution<float> ecolorA(m_minEndCol.a, m_maxEndCol.a);

    for (unsigned i = startPos; i < endPos; ++i)
    {
      particles.m_startColor[i].r = scolorR(gen);
      particles.m_startColor[i].b = scolorB(gen);
      particles.m_startColor[i].g = scolorG(gen);
      particles.m_startColor[i].a = scolorA(gen);

      particles.m_endColor[i].r = ecolorR(gen);
      particles.m_endColor[i].g = ecolorG(gen);
      particles.m_endColor[i].b = ecolorB(gen);
      particles.m_endColor[i].a = ecolorA(gen);
    }
  }

  void ParticleEmitter::PosGen(unsigned startPos, unsigned endPos)
  {
    Vec2 zeroVector;
    
    // if we were given random start/end positions, spawn randomly
    if (m_minStartPosition != zeroVector && m_maxStartPosition != zeroVector)
    {
      std::random_device rd;
      std::mt19937 gen(rd());
      std::uniform_real_distribution<float> posX(m_minStartPosition.x, m_maxStartPosition.x);
      std::uniform_real_distribution<float> posY(m_minStartPosition.y, m_maxStartPosition.y);
      for (unsigned i = startPos; i < endPos; ++i)
      {
        if (m_followTransform)
          particles.m_offset[i] = Vec2(posX(gen), posY(gen));
        else
          particles.m_pos[i] = owner->transform.translation + Vec2(posX(gen), posY(gen));
      }
    }

    // if not, spawn using the max offset
    else
    {
      for (unsigned i = startPos; i < endPos; ++i)
      {
        if (m_followTransform)
          particles.m_offset[i] = m_MaxOffsetPos;
        else
          particles.m_pos[i] = owner->transform.translation + m_MaxOffsetPos;
      }
    }
  }

  void ParticleEmitter::CirGen(unsigned startPos, unsigned endPos)
  {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> angle(0.0, M_2_PI);
    for (unsigned i = startPos; i < endPos; ++i)
    {
      float ang = angle(gen);
      if (m_followTransform)
      {
        particles.m_offset[i] = m_MaxOffsetPos;
        particles.m_offset[i].x += m_radX*sin(ang);
        particles.m_offset[i].y += m_radY*cos(ang);
      }
      else
      {
        particles.m_pos[i] = owner->transform.translation + m_MaxOffsetPos;
        particles.m_pos[i].x += m_radX*sin(ang);
        particles.m_pos[i].y += m_radY*cos(ang);
      }
    }
  }

  void ParticleEmitter::BoxGen(unsigned startPos, unsigned endPos)
  {
    // Get Offset of min and max coners of the box
    Vec2 tran = owner->transform.translation;
    Vec2 posMin{tran.x + m_MaxOffsetPos.x - m_width / 2, tran.y + m_MaxOffsetPos.y - m_height / 2};
    Vec2 posMax{tran.x + m_MaxOffsetPos.x + m_width / 2, tran.y + m_MaxOffsetPos.y + m_height / 2};

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> PosX(posMin.x, posMax.x);
    std::uniform_real_distribution<float> PosY(posMin.y, posMax.y);

    for (unsigned i = startPos; i < endPos; ++i)
    {
      if (m_followTransform)
      {
        particles.m_offset[i].x = PosX(gen);
        particles.m_offset[i].y = PosY(gen);
      }
      else
      {
        particles.m_pos[i].x = PosX(gen);
        particles.m_pos[i].y = PosY(gen);
      }
    }
  }

  void ParticleEmitter::VelGen(unsigned startPos, unsigned endPos)
  {
    std::random_device rd;
    std::mt19937 gen(rd());

    if (m_minStartVel.x > m_maxStartVel.x)
      std::swap(m_minStartVel.x, m_maxStartVel.x);

    if (m_minStartVel.y > m_maxStartVel.y)
      std::swap(m_minStartVel.y, m_maxStartVel.y);

    std::uniform_real_distribution<float> velX(m_minStartVel.x, m_maxStartVel.x);
    std::uniform_real_distribution<float> velY(m_minStartVel.y, m_maxStartVel.y);
    
    for (unsigned i = startPos; i < endPos; ++i)
    {
      particles.m_vel[i] = Vec2(velX(gen), velY(gen));
    }
  }

  void ParticleEmitter::ScaleGen(unsigned startPos, unsigned endPos)
  {
    // if we were given random start/end scale, spawn randomly
    if (m_minScalar != 0 && m_maxScalar != 0)
    {
      std::random_device rd;
      std::mt19937 gen(rd());
      std::uniform_real_distribution<float> scalar(m_minScalar, m_maxScalar);
      for (unsigned i = startPos; i < endPos; ++i)
      {
        float scale = scalar(gen);
        particles.m_scale[i] = Vec2(scale * m_scale.x, scale * m_scale.y);
      }
    }

    // if not, use default scale
    else
    {
      for (unsigned i = startPos; i < endPos; ++i)
      {
        particles.m_scale[i] = m_scale;
      }
    }
  }

  void ParticleEmitter::RotGen(unsigned startPos, unsigned endPos)
  {
    for (unsigned i = startPos; i < endPos; ++i)
    {
      particles.m_rot[i] = m_rot;
    }
  }

  std::string &ParticleEmitter::GetImage()
  {
    if (image == nullptr)
      assert(0);
    return image->name;
  }

  Image *ParticleEmitter::GetImagePointer()
  {
    return image;
  }

  void ParticleEmitter::SetImage(std::string imageName)
  {
    if (image && image->name == imageName)
      return;
    //p_CurrentFrame = 0;
    //m_timePassed = 0;
    Game &game = Game::Instance();
    std::unordered_map<std::string, Image>::iterator got = game.assets.images.find(imageName);
    if (got == game.assets.images.end())
    {
      Console::Error("Sprite::SetImage failed", "Image \"" + imageName + "\" not found!");
      if (image)
        p_Image = image->name;
      else
        p_Image = "";
    }
    else
    {
      p_UV1 = (got->second).uv[0];
      p_UV2 = (got->second).uv[1];
      //UpdateUV(1.0f / 60.f);
      image = &(got->second);
      p_Image = imageName;
    }
  }

  // Allocates memory needed for Container
  void ParticleData::Generate(int maxSize)
  {
    Clean();

    // Set Particle counts
    m_count = maxSize;
    m_countAlive = 0;

    // Allocate memory for array
    m_alive       = new bool[maxSize];
    m_pos         = new Vec2[maxSize];
    m_vel         = new Vec2[maxSize];
    m_acc         = new Vec2[maxSize];
    m_scale       = new Vec2[maxSize];
    m_offset      = new Vec2[maxSize];
    m_rot         = new float[maxSize];
    m_time        = new Time[maxSize];
    m_col         = new D3DXCOLOR[maxSize];
    m_startColor  = new D3DXCOLOR[maxSize];
    m_endColor    = new D3DXCOLOR[maxSize];
  }

  // Deallocates all memory
  void ParticleData::Clean()
  {
    if (m_alive)
    {
      delete[] m_alive;
      delete[] m_pos;
      delete[] m_vel;
      delete[] m_acc;
      delete[] m_scale;
      delete[] m_offset;
      delete[] m_rot;
      delete[] m_time;
      delete[] m_col;
      delete[] m_startColor;
      delete[] m_endColor;
    }
  }

  // Mark particle non-active
  void ParticleData::Kill(size_t id)
  {
    SwapData(id, m_countAlive - 1);
    m_alive[id] = true;
    m_alive[m_countAlive - 1] = false;
    --m_countAlive;
  }

  // Mark particle active
  void ParticleData::Wake(size_t id)
  {
    m_alive[id] = true;
    ++m_countAlive;
  }

  // Swap particles in container
  void ParticleData::SwapData(size_t a, size_t b)
  {
    m_pos[a] = m_pos[b];
    m_vel[a] = m_vel[b];
    m_acc[a] = m_acc[b];
    m_acc[b] = Vec2(0, 0);
    m_scale[a] = m_scale[b];
    m_time[a] = m_time[b];
    m_col[a] = m_col[b];
    m_startColor[a] = m_startColor[b];
    m_endColor[a] = m_endColor[b];
  }

  void ParticleEmitter::addUI(TwBar *ownerBar)
  {
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    // Active
    TwAddVarCB(ownerBar, "emitParticles", TW_TYPE_BOOLCPP, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_isActive = *static_cast<const bool*>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<bool *>(value) = emitter->m_isActive;
    },
      this,
      "label='Active' group=ParticleEmitter help='Determines if the emitter spawn particles'"
      );

    // Follow Transform
    TwAddVarCB(ownerBar, "followTransform", TW_TYPE_BOOLCPP, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_followTransform = *static_cast<const bool*>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<bool *>(value) = emitter->m_followTransform;
    },
      this,
      "label='followTransform' group=ParticleEmitter help='Determines if the particles should follow trans'"
    );

    // behaviour
    TwAddVarCB(ownerBar, "behaviour", TW_TYPE_INT16, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->behaviourType = BehaviourType(*static_cast<const int*>(value));
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<int *>(value) = emitter->behaviourType;
    },
    this,
    "label='Behaviour' group=ParticleEmitter help='Determines if the emitter spawn particles'"
    );

    // stretch speed
    TwAddVarCB(ownerBar, "stretchSpeed", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_stretchSpeed = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->m_stretchSpeed;
    },
      this,
      "label='StretchSpeed' group=ParticleEmitter help='How fast the particles will catch up to each other' min=0.0"
    );

    // Number of Particles
    TwAddVarCB(ownerBar, "numParticles", TW_TYPE_INT16, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)(clientData);
      emitter->particles.Generate(*static_cast<const int *>(value));
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter =  (ParticleEmitter *)(clientData);
      *static_cast<int *>(value) = emitter->particles.m_count;
    }, 
      this, 
      "label='Num Particles' group=ParticleEmitter help='The maximum number of Particles at one time' min=1"
    );

    // Emit Count
    TwAddVarCB(ownerBar, "emitCount", TW_TYPE_INT16, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_emitCount = *static_cast<const int *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<int *>(value) = emitter->m_emitCount;
    },
      this,
      "label='Emit Count' group=ParticleEmitter help='Emitter will spawn this many paritlces then stop if 0 spawns infinitely' min=0"
    );

    // Emitte Rate
    TwAddVarCB(ownerBar, "emitRate", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_emitRate = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->m_emitRate;
    },
      this,
      "label='EmitRate' group=ParticleEmitter help='Rate at witch particles emit' min=0.0"    
    );

    // Min Life Time
    TwAddVarCB(ownerBar, "minLifeTime", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_minTime = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->m_minTime;
    },
      this,
      "label='Min Life Time' group=ParticleEmitter help='Min time that a particle will live' min=0.0"
    );

    // Max Life Time
    TwAddVarCB(ownerBar, "maxLifeTime", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_maxTime = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->m_maxTime;
    },
      this,
      "label='Max Life Time' group=ParticleEmitter help='Max time that a particle will live' min=0.0"
    );

    // Spawn Method
    TwEnumVal PosMethod[PT_MAX] = { { PT_POS, "On Emitter"}, {PT_CIR, "Circle"}, {PT_BOX, "Box"}};
    TwType PosType = TwDefineEnum("Type", PosMethod, PT_MAX);
    TwAddVarRW(ownerBar, "Type", PosType, &m_posType, " group=ParticleEmitter help = 'Changes Spawning Method' ");

    // Offset Postion
    TwAddVarCB(ownerBar, "offsetPosition", TW_TYPE_DIR3F, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_MaxOffsetPos = *static_cast<const Vec2 *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<Vec2 *>(value) = emitter->m_MaxOffsetPos;
    },
      this,
      "label='Offset Posstion' group=ParticleEmitter help='Offset from spawn location'"
    );

    // Radius X
    TwAddVarCB(ownerBar, "radX", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_radX = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->m_radX;
    },
      this,
      "label='Radius X' group=ParticleEmitter help='Radius in the X direction' min=0.0"
    );

    // Radius Y
    TwAddVarCB(ownerBar, "radY", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_radY = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->m_radY;
    },
      this,
      "label='Radius Y' group=ParticleEmitter help='Radius in the Y direction' min=0.0"
    );
    
    // Width of Box
    TwAddVarCB(ownerBar, "width", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_width = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->m_width;
    },
      this,
      "label='Width' group=ParticleEmitter help='Width of Box Spawner' min=0.0"
      );

    // Height of Box
    TwAddVarCB(ownerBar, "height", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_height = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->m_height;
    },
      this,
      "label='Height' group=ParticleEmitter help='Height of Box Spawner' min=0.0"
      );

    // Additive
    TwAddVarCB(ownerBar, "spriteAdditive", TW_TYPE_BOOLCPP, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_additive = *static_cast<const bool*>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<bool *>(value) = emitter->m_additive;
    },
      this,
      "label='Additive' group=ParticleEmitter"
    );
    
    // FlippedX
    TwAddVarCB(ownerBar, "spriteFlippedX", TW_TYPE_BOOLCPP, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_isFlippedX = *static_cast<const bool*>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<bool *>(value) = emitter->m_isFlippedX;
    },
      this,
      "label='FlipX' group=ParticleEmitter help='Flips the sprite Imgae on X-axis'"
    );

    // FlippedY
    TwAddVarCB(ownerBar, "spriteFlippedY", TW_TYPE_BOOLCPP, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_isFlippedY = *static_cast<const bool*>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<bool *>(value) = emitter->m_isFlippedY;
    },
      this,
      "label='FlipY' group=ParticleEmitter help='Flips the sprite Imgae on Y-axis'"
      );

    // Sprite Image
    TwAddVarCB(ownerBar, "image", TW_TYPE_STDSTRING, SetImageCB, GetImageCB, this, "label='Sprite' group=ParticleEmitter help='Name of the Texture appiled to each particle'");

    // Depth
    TwAddVarCB(ownerBar, "depth", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->p_Depth = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->p_Depth;
    },
      this,
      "label='Depth offset' group=ParticleEmitter help='Z-buffer depth offset' "
      );

    // Sprite Scale
    TwAddVarCB(ownerBar, "particleScale", TW_TYPE_DIR3F, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_scale = *static_cast<const Vec2 *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<Vec2 *>(value) = emitter->m_scale;
    },
      this,
      "label='Particle Scale' group=ParticleEmitter help='Scaling factor of the Texture for the partcile' min=0.01"
    );

    // Min scale
    TwAddVarCB(ownerBar, "minScalar", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_minScalar = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->m_minScalar;
    },
      this,
      "label='Min Scalar' group=ParticleEmitter help='Minimum scale multiplier of particle'"
      );

    // Max scale
    TwAddVarCB(ownerBar, "maxScale", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_maxScalar = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->m_maxScalar;
    },
      this,
      "label='Max Scalar' group=ParticleEmitter help='Maximum scale multiplier of particle'"
      );

    // ScaleX
    TwAddVarCB(ownerBar, "scale_x", TW_TYPE_BOOLCPP, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_scaleX = *static_cast<const bool*>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<bool *>(value) = emitter->m_scaleX;
    },
      this,
      "label='ScaleX' group=ParticleEmitter"
      );

    // ScaleY
    TwAddVarCB(ownerBar, "scale_y", TW_TYPE_BOOLCPP, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_scaleY = *static_cast<const bool*>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<bool *>(value) = emitter->m_scaleY;
    },
      this,
      "label='ScaleY' group=ParticleEmitter"
      );

    // Min Start Color
    TwAddVarCB(ownerBar, "minStartColor", TW_TYPE_COLOR4F, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_minStartCol = *static_cast<const D3DXCOLOR *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<D3DXCOLOR *>(value) = emitter->m_minStartCol;
    },
      this,
      "label='Min Start Col' group=ParticleEmitter help='Color values that are set to the particles in RGBA values'"
    );
    
    // Max Start Color
    TwAddVarCB(ownerBar, "maxStartColor", TW_TYPE_COLOR4F, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_maxStartCol = *static_cast<const D3DXCOLOR *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<D3DXCOLOR *>(value) = emitter->m_maxStartCol;
    },
      this,
      "label='Max Start Col' group=ParticleEmitter help='Color values that are set to the particles in RGBA values'"
    );

    // Min End Color
    TwAddVarCB(ownerBar, "minEndColor", TW_TYPE_COLOR4F, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_minEndCol = *static_cast<const D3DXCOLOR *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<D3DXCOLOR *>(value) = emitter->m_minEndCol;
    },
      this,
      "label='Min End Col' group=ParticleEmitter help='Color values that are set to the particles in RGBA values'"
    );

    // Max End Color
    TwAddVarCB(ownerBar, "maxEndColor", TW_TYPE_COLOR4F, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_maxEndCol = *static_cast<const D3DXCOLOR *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<D3DXCOLOR *>(value) = emitter->m_maxEndCol;
    },
      this,
      "label='Max End Col' group=ParticleEmitter help='Color values that are set to the particles in RGBA values'"
    );
    
    // Min Velocity
    TwAddVarCB(ownerBar, "minVelocity", TW_TYPE_DIR3F, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_minStartVel = *static_cast<const Vec2 *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<Vec2 *>(value) = emitter->m_minStartVel;
    },
      this,
      "label='Min Velocity' group=ParticleEmitter help='Minimum starting velocity of particle'"
    );

    // Max Velocity
    TwAddVarCB(ownerBar, "maxVelocity", TW_TYPE_DIR3F, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_maxStartVel = *static_cast<const Vec2 *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<Vec2 *>(value) = emitter->m_maxStartVel;
    },
      this,
      "label='Max Velocity' group=ParticleEmitter help='Maximum starting velocity of particle'"
    );

    // Min position
    TwAddVarCB(ownerBar, "minPosition", TW_TYPE_DIR3F, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_minStartPosition = *static_cast<const Vec2 *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<Vec2 *>(value) = emitter->m_minStartPosition;
    },
      this,
      "label='Min Position' group=ParticleEmitter help='Minimum starting position of particle'"
      );

    // Max position
    TwAddVarCB(ownerBar, "maxPosition", TW_TYPE_DIR3F, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_maxStartPosition = *static_cast<const Vec2 *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<Vec2 *>(value) = emitter->m_maxStartPosition;
    },
      this,
      "label='Max Position' group=ParticleEmitter help='Maximum starting position of particle'"
      );

    // Accleration
    TwAddVarCB(ownerBar, "acceleration", TW_TYPE_DIR3F, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_globalAcceleration = *static_cast<const Vec2 *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<Vec2 *>(value) = emitter->m_globalAcceleration;
    },
      this,
      "label='Acceleration' group=ParticleEmitter help='Acceleration of particle'"
    );

    // Attractor
    TwAddVarCB(ownerBar, "attractor_On", TW_TYPE_BOOLCPP, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_isAttracting = *static_cast<const bool*>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<bool *>(value) = emitter->m_isAttracting;
    },
      this,
      "label='Attractor' group=ParticleEmitter"
    );

    // Strength
    TwAddVarCB(ownerBar, "strength", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_strength = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->m_strength;
    },
      this,
      "label='Strength' group=ParticleEmitter help='Strength the attractor will pull' min=0.0"
    );

    // Min Distance
    TwAddVarCB(ownerBar, "minDistance", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      emitter->m_minDistance = *static_cast<const float *>(value);
    },
      [](void *value, void *clientData)
    {
      ParticleEmitter *emitter = (ParticleEmitter *)clientData;
      *static_cast<float *>(value) = emitter->m_minDistance;
    },
      this,
      "label='Min Distance' group=ParticleEmitter help='Minimum distance' min=0.0"
    );
#endif
    ////  { "maxDistance", PropertyDefinition::Float(1.0f, offsetof(ParticleEmitter, m_maxDistance), true) },
    //// Max Distance
    //TwAddVarCB(ownerBar, "maxDistance", TW_TYPE_FLOAT, [](const void *value, void *clientData)
    //{
    //  ParticleEmitter *emitter = (ParticleEmitter *)clientData;
    //  emitter->m_maxDistance = *static_cast<const float *>(value);
    //},
    //  [](void *value, void *clientData)
    //{
    //  ParticleEmitter *emitter = (ParticleEmitter *)clientData;
    //  *static_cast<float *>(value) = emitter->m_maxDistance;
    //},
    //  this,
    //  "label='Max Distance' group=ParticleEmitter help='Maximum distance' min=0.0"
    //);
  }

  void TW_CALL ParticleEmitter::SetImageCB(const void *value, void *clientData)
  {
    static_cast<ParticleEmitter *>(clientData)->SetImage(*static_cast<const std::string *>(value));
  }

  void TW_CALL ParticleEmitter::GetImageCB(void *value, void *clientData)
  {
    ParticleEmitter *emitter = (ParticleEmitter *)clientData;
    std::string *destStringPtr = (std::string *)(value);

    std::string title = emitter->p_Image;
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    TwCopyStdStringToLibrary(*destStringPtr, title);
#endif
  }
}
