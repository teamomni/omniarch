/******************************************************************************
Filename: Archetype.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include <unordered_map>
#include "Property.h"
#include "Console.h"
#include "Transform.h"

#include "PropertyContainer.h"

namespace Omni
{
  struct Archetype
  {
    std::string archetypeName;
    std::string name;
    std::unordered_map<std::string, std::unordered_map<std::string, PropertyContainer>> components;
    Transform transform;
    bool hasTranslation = false;
    bool hasRotation = false;
    bool hasScale = false;

    Archetype() {}
    Archetype(std::string archetypeName_) : archetypeName(archetypeName_) {}

    inline void AddComponent(std::string componentName)
    {
      if (components.count(componentName) != 0)
      {
        Console::Error("Critical Archetype load error", "While loading archetype \"" + archetypeName + "\", attempted to add component \"" + componentName + "\", but that component already exists in the archetype.");
        return;
      }
      components.insert({ componentName, std::unordered_map<std::string, PropertyContainer>() });
    }

    inline void AddProperty(std::string componentName, std::string propertyName, PropertyContainer propertyContainer)
    {
      if (components.count(componentName) == 0)
      {
        Console::Error("Critical Archetype load error", "While loading archetype \"" + archetypeName + "\", attempted to add property \"" + propertyName + "\" to component \"" + componentName + "\", but that component does not exist in the archetype (how did this happen?!).");
        return;
      }
      if (components[componentName].count(propertyName) != 0)
      {
        Console::Error("Critical Archetype load error", "While loading archetype \"" + archetypeName + "\", attempted to add property \"" + propertyName + "\" to component \"" + componentName + "\", but that component already has that property defined in this archetype.");
        return;
      }
      components[componentName].insert({ propertyName, propertyContainer });
    }
  };
}


