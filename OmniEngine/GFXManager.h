/******************************************************************************
Filename: GFXManager.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// Juli Gregg
// GFXManager.h
// Manages the renderer object

#pragma once

#include "GFXRenderer.h"
#include "RenderState.h"
#include <unordered_map>

namespace Omni
{
  class Sprite;
  
  namespace Graphics
  {

    class GFXManager
    {

      struct ShaderPair;

    public:
      GFXManager();
      ~GFXManager();

      bool Initialize(WindowSystem& Window);
      void Shutdown();

      //Actually goes through and calls all draw calls
      //and then sends to screen
      bool Present(RenderState &renderState, bool loadingScreen = false);

      void UpdateWindowSize(LPARAM);

      GFXRenderer* m_renderer;

      inline void DebugDrawOn(bool state) { m_debugDraw = state; }

      bool GetFullScreen() { return m_fullScreen; }

      void SetFullScreen(bool fullScreen) { m_fullScreen = fullScreen; }

      inline void SetClearColor(D3DXCOLOR c) { m_clearColor = c; }

    private:

      TwBar *m_graphicsBar = nullptr;
      D3DXCOLOR m_clearColor = D3DXCOLOR(.508f, .508f, .508f, 1.0f);
      bool m_debugDraw = false;
      bool m_fullScreen = false;
      bool m_glow = true;

      void InitHandles();

      void SetupCamera(RenderState&);
      void SetupRasterizers(void);

      //To be called from Present function
      void BeginSpriteDraw(void);
      void DrawSprites(RenderState &);
      void EndSpriteDraw(void);

      void BatchParticleEmitter(RenderObject&);

      void DrawSpriteText(RenderState&);


      void CopyRenderTarget(Handle& destination, Handle& copy, ShaderPair& shaders = ShaderPair(BasicVShader, BasicPShader));

      Instance CreateInstance(RenderObject& currentSprite, bool glow = false, bool glowButNoShow = false);
      Instance CreateInstance(RenderObject& particles, unsigned index);


      Handle& GetGlowTexture(std::string& name);

      void AddRasterizersStates(void);

      unsigned m_solid_index;
      unsigned m_wireframe_index;
      bool m_wireframe = false;
      bool m_opaque_rt = true;
      bool m_glow_rt = true;
      bool m_glow_Add_rt = true;
      bool m_blur_pass = false;
      bool m_bloom_pass = false;
      float m_bloom_threshold = 0.0f;

      void ApplyEffectShaders();
      void ApplyBlur(Handle effect, Handle destination);

      struct SpriteBatch
      {
        std::vector<Instance> instanceData;
        BlurData instanceBloom;
        Handle instanceBloomData;
        std::vector<Handle> textures;
        BlendModes blendMode;
        bool glow = false;
        bool bloom = false; 
        bool blur = false;
        bool particle = false;

        SpriteBatch()
        {
          blendMode = BlendModes::Alpha;
          particle = false;
        };

        void ClearBatch(Handle& tHandle, Handle& gHandle, bool isGlow, bool isBloom, bool isBlur)
        {
          instanceData.clear();
          textures[0] = tHandle;
          textures[1] = gHandle;
          glow = isGlow;
          bloom = isBloom;
          blur = isBlur;
          particle = false;
        }
      };

      struct ShaderPair
      {
        MngrHandles pShader;
        MngrHandles vShader;

        ShaderPair(MngrHandles v, MngrHandles p) : vShader(v), pShader(p) {};
      };

      struct Text
      {
        void Initilize();
        void CreateFontWrapper(ID3D11Device * device, std::string fontName);
        void Shutdown();

        IFW1Factory* m_fontFactory;
        std::unordered_map<std::string, IFW1FontWrapper*> m_fontWrappersRes;

      }m_TextManager;

      //#if _DEBUG
      std::vector<Handle> m_debugHandles;
      //#endif

      std::vector<Handle> m_handles;
      std::vector<Handle> m_instanceBuffers;
      std::vector<SpriteBatch> m_spriteBatches;

      friend class GFXDebugDraw;

    };

  } //namespace Graphics
} //namespace Omni