/******************************************************************************
Filename: BasicVShader.hlsl

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//
//Basic Vertex Shader for Copying Render Targets
//

//-----------------------------------//
// Struct Passed from CPU            //
//-----------------------------------//
struct vInput
{
  float3 position : POSITION;
  float2 texcoord : TEXCOORD;
};

//-----------------------------------//
// Pass info onto pixel shader       //
//-----------------------------------//
struct vOutput
{
  float4 position : SV_POSITION;
  float2 texcoord : TEXCOORD;
};

//-----------------------------------//
// Main Function of Vertex Shader    //
//-----------------------------------//
vOutput VShader(vInput input)
{
  vOutput output;

  output.position = float4(input.position.xy * 2, 0, 1);
  output.texcoord = input.texcoord;

  return output;
}