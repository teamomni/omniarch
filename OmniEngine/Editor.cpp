/******************************************************************************
Filename: Editor.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//Juli Gregg

#include "stdafx.h"
#include "Editor.h"
#include "Space.h"
#include "RigidBody.h"
#include "Sprite.h"
#include "Shape.h"

namespace Omni
{
  void Editor::Initialize(bool active)
  {
    m_active = active;
    m_selected = nullptr;
  }

  void Editor::Update(void)
  {
    Game::Instance().GetGraphicsManager()->DebugDrawOn(m_active);

    // if rmb is pressed, print mouse position in main space
    if (Input::mouse.mouseStates[int(Mouse::Button::Right)] == ButtonState::Pressed)
    {
      char buffer[30];
      Console::WriteLine("Mouse position in main space is ", Console::Color::Blue);

      Space *main = Game::Instance().GetSpaceByName("main");
      Vec2 mousePos = main->GetMousePosition();

      sprintf(buffer, "x : %.4f ", mousePos.x);
      Console::WriteLine(buffer, Console::Color::Magenta);

      sprintf(buffer, "y : %.4f ", mousePos.y);
      Console::WriteLine(buffer, Console::Color::Magenta);
    }

    // only check for new selection if lmb is pressed
    if (Input::mouse.mouseStates[int(Mouse::Button::Left)] == ButtonState::Pressed)
    {
      // if the currently selected thing has changed, destroy old tweakbar and spawn new one
      if (selectedID != Clickable::currentlySelectedID[unsigned(Mouse::Button::Left)])
      {
        Handlet<Entity> prev = Game::Instance().GetEntityByID(selectedID);
        Handlet<Entity> selected = Game::Instance().GetEntityByID(Clickable::currentlySelectedID[unsigned(Mouse::Button::Left)]);

#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
        // check if the entity even exists
        if (prev)
          prev->DeleteTweakBar();

        // check if selected exists
        if (selected)
          selected->SpawnTweakBar();
#endif

        selectedID = Clickable::currentlySelectedID[unsigned(Mouse::Button::Left)];
      }
    }

    // if middle mouse is pressed, store the mouse to camera vector
    if (Input::mouse.mouseStates[int(Mouse::Button::Middle)] == ButtonState::Pressed)
    {
      int mouseX = int(Input::mouse.position.x);
      float f_mouseX = float(mouseX);

      int mouseY = int(Input::mouse.position.y);
      float f_mouseY = float(mouseY);

      initialMousePosition = Vec2(f_mouseX, f_mouseY);
    }

    // if middle mouse is down, move camera
    if (Input::mouse.mouseStates[unsigned(Mouse::Button::Middle)] == ButtonState::Down)
    {
      int mouseX = int(Input::mouse.position.x);
      float f_mouseX = float(mouseX);

      int mouseY = int(Input::mouse.position.y);
      float f_mouseY = float(mouseY);

      Space *main = Game::Instance().GetSpaceByName("main");

      Game::Instance().GetDebugDraw()->DrawPoint(main->GetPointInSpace(initialMousePosition), main->index);

      Vec2 mousePosition = Vec2(f_mouseX, f_mouseY);
      Vec2 diff = (mousePosition - initialMousePosition) * 0.001f;
      diff.y = -diff.y;
      main->camera.transform.translation += diff;
    }

    Handlet<Entity> current = Game::Instance().GetEntityByID(selectedID);
    if (current && m_active)
    {
      Game::Instance().GetDebugDraw()->DrawArrow(current->transform.translation, Vec2(1, 0), current->space->index, D3DXCOLOR(1, 0, 0, 1));
      Game::Instance().GetDebugDraw()->DrawArrow(current->transform.translation, Vec2(0, 1), current->space->index, D3DXCOLOR(0, 1, 0, 1));
      Game::Instance().GetDebugDraw()->DrawQuad(current->transform, current->space->index, D3DXCOLOR(1, 1, 1, 1));
      Game::Instance().GetDebugDraw()->DrawPoint(current->transform.translation, current->space->index, D3DXCOLOR(0, 0, 1, 1));
    }
  }

  Handlet<Entity> Editor::CreateFloorAtMousePosition(Space *space)
  {
    Handlet<Entity> created = CreateFloor(space);
    Vec2 mousePos = space->GetMousePosition();
    created->transform.translation = mousePos;

    Handlet<Entity> prev = Game::Instance().GetEntityByID(selectedID);
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    // check if there are any selections
    if (prev.isValid())
      prev->DeleteTweakBar();

    // check if selected exists
    created->SpawnTweakBar();
#endif
    selectedID = created->id;

    return created;
  }

  Handlet<Entity> Editor::CreateEntityAtMousePosition(Space *space, std::string archetype)
  {
    Vec2 mousePos = space->GetMousePosition();
    Handlet<Entity> created = space->CreateEntityAtPosition(archetype, mousePos, archetype);

    Handlet<Entity> prev = Game::Instance().GetEntityByID(selectedID);
#if defined(_DEBUG) || defined(_RELEASE) || defined(_EDITOR)
    // check if there are any selections
    if (prev.isValid())
      prev->DeleteTweakBar();

    // check if selected exists
    created->SpawnTweakBar();
#endif
    selectedID = created->id;

    created->Initialize();

    return created;
  }
    
  Handlet<Entity> Editor::CreateFloor(Space *space)
  {
    Handlet<Entity> created = space->CreateEntityAtPosition("terrain", Vec2(), "GenericTerrain");
    created->Initialize();
    return created;
  }

  void Editor::ShutDown(void)
  {

  }

} //Namespace Omni