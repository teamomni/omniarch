/******************************************************************************
Filename: Input.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Input.h"
#include "AntTweakBar.h"
#include "Game.h"
#include <iostream>

#pragma comment(lib, "Xinput.lib")

namespace Omni
{
  bool Input::autoplay = false;
  bool Input::modeChanged = false;
  Input::Mode Input::mode = Input::Mode::Keyboard;
  Input::Mode Input::modePrevious = Input::Mode::Gamepad;
  Keyboard Input::keyboard;
  Mouse Input::mouse;
  Gamepad Input::gamepads[XUSER_MAX_COUNT];
  std::unordered_map<std::string, std::function<bool(void)>> Input::actions;
  std::unordered_map<std::string, std::function<bool(void)>> Input::systemActions;

  ButtonState Input::MakeButtonState(bool last, bool cur)
  {
    if (!last && !cur)
      return ButtonState::Up;
    if (!last && cur)
      return ButtonState::Pressed;
    if (last && cur)
      return ButtonState::Down;
    return ButtonState::Released;
  }

  Gamepad::Gamepad() : status(ConnectionStatus::Disconnected)
  {
    ZeroMemory(&currentState, sizeof(XINPUT_STATE));
    ZeroMemory(&previousState, sizeof(XINPUT_STATE));
    ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));
  }

  void Gamepad::Update(DWORD id_)
  {
    id = id_;
    std::swap(currentState, previousState);
    ZeroMemory(&currentState, sizeof(XINPUT_STATE));
    DWORD dwResult = XInputGetState(id, &currentState);

    if (currentState.dwPacketNumber > previousState.dwPacketNumber && previousState.dwPacketNumber != 0)
      Input::mode = Input::Mode::Gamepad;

    if (dwResult == ERROR_SUCCESS)
    {
      switch (status)
      {
      case ConnectionStatus::Disconnected:
        status = ConnectionStatus::JustConnected;
        break;
      case ConnectionStatus::JustConnected:
        status = ConnectionStatus::Connected;
        break;
      case ConnectionStatus::JustDisconnected: // unlikely
        status = ConnectionStatus::JustConnected;
        break;
      }
    }
    else
    {
      switch (status)
      {
      case ConnectionStatus::Connected:
        status = ConnectionStatus::JustDisconnected;
        break;
      case ConnectionStatus::JustDisconnected:
        status = ConnectionStatus::Disconnected;
        break;
      case ConnectionStatus::JustConnected: // unlikely
        status = ConnectionStatus::JustDisconnected;
        break;
      }
    }
  }

  void Gamepad::Vibrate(float left, float right)
  {
    ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));
    vibration.wLeftMotorSpeed = (WORD)(65535.0f * left);
    vibration.wRightMotorSpeed = (WORD)(65535.0f * right);
    XInputSetState(id, &vibration);
  }

  ButtonState Gamepad::MakeButtonState(unsigned flag) const
  {
    return Input::MakeButtonState((previousState.Gamepad.wButtons & flag) != 0, (currentState.Gamepad.wButtons & flag) != 0);
  }

  float Gamepad::LeftTrigger() const
  {
    return currentState.Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD ? (float)currentState.Gamepad.bLeftTrigger * (1.0f / 255.0f) : 0.0f;
  }
  float Gamepad::RightTrigger() const
  {
    return currentState.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD ? (float)currentState.Gamepad.bRightTrigger * (1.0f / 255.0f) : 0.0f;
  }

  Vec2 Gamepad::LeftStick_Exact() const
  {
    return Vec2((float)currentState.Gamepad.sThumbLX / 32767.0f, (float)currentState.Gamepad.sThumbLY / 32768.0f);
  }
  Vec2 Gamepad::LeftStick() const
  {
    Vec2 vec = LeftStick_Exact();
    if (vec.x > 0.0f && currentState.Gamepad.sThumbLX < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
      vec.x = 0.0f;
    if (vec.x < 0.0f && currentState.Gamepad.sThumbLX > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
      vec.x = 0.0f;
    if (vec.x > THUMBSTICK_THRESHOLD)
      vec.x = 1.0f;
    if (vec.x < -THUMBSTICK_THRESHOLD)
      vec.x = -1.0f;
    if (vec.y > 0.0f && currentState.Gamepad.sThumbLY < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
      vec.y = 0.0f;
    if (vec.y < 0.0f && currentState.Gamepad.sThumbLY > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
      vec.y = 0.0f;
    if (vec.y > THUMBSTICK_THRESHOLD)
      vec.y = 1.0f;
    if (vec.y < -THUMBSTICK_THRESHOLD)
      vec.y = -1.0f;
    return vec;
  }


  Vec2 Gamepad::RightStick_Exact() const
  {
    return Vec2((float)currentState.Gamepad.sThumbRX / 32768.0f, (float)currentState.Gamepad.sThumbRY / 32768.0f);
  }

  Vec2 Gamepad::RightStick() const
  {
    Vec2 vec = RightStick_Exact();
    if (vec.x > 0.0f && currentState.Gamepad.sThumbRX < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
      vec.x = 0.0f;
    if (vec.x < 0.0f && currentState.Gamepad.sThumbRX > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
      vec.x = 0.0f;
    if (vec.x > THUMBSTICK_THRESHOLD)
      vec.x = 1.0f;
    if (vec.x < -THUMBSTICK_THRESHOLD)
      vec.x = -1.0f;
    if (vec.y > 0.0f && currentState.Gamepad.sThumbRY < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
      vec.y = 0.0f;
    if (vec.y < 0.0f && currentState.Gamepad.sThumbRY > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
      vec.y = 0.0f;
    if (vec.y > THUMBSTICK_THRESHOLD)
      vec.y = 1.0f;
    if (vec.y < -THUMBSTICK_THRESHOLD)
      vec.y = -1.0f;
    return vec;
  }

  ButtonState Gamepad::A() const
  {
    return MakeButtonState(0x1000);
  }
  ButtonState Gamepad::B() const
  {
    return MakeButtonState(0x2000);
  }
  ButtonState Gamepad::X() const
  {
    return MakeButtonState(0x4000);
  }
  ButtonState Gamepad::Y() const
  {
    return MakeButtonState(0x8000);
  }
  ButtonState Gamepad::LB() const
  {
    return MakeButtonState(0x0100);
  }
  ButtonState Gamepad::RB() const
  {
    return MakeButtonState(0x0200);
  }
  ButtonState Gamepad::Back() const
  {
    return MakeButtonState(0x0020);
  }
  ButtonState Gamepad::Start() const
  {
    return MakeButtonState(0x0010);
  }
  ButtonState Gamepad::LS() const
  {
    return MakeButtonState(0x0040);
  }
  ButtonState Gamepad::RS() const
  {
    return MakeButtonState(0x0080);
  }
  ButtonState Gamepad::DpadUp() const
  {
    return MakeButtonState(0x0001);
  }
  ButtonState Gamepad::DpadDown() const
  {
    return MakeButtonState(0x0002);
  }
  ButtonState Gamepad::DpadLeft() const
  {
    return MakeButtonState(0x0004);
  }
  ButtonState Gamepad::DpadRight() const
  {
    return MakeButtonState(0x0008);
  }

  ButtonState Gamepad::LeftStick_Up() const
  {
    float y = (float)currentState.Gamepad.sThumbLY / 32767.0f;
    float yp = (float)previousState.Gamepad.sThumbLY / 32767.0f;
    if (y > THUMBSTICK_MENU_THRESHOLD)
      if (yp > THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Down;
      else
        return ButtonState::Pressed;
    else
      if (yp > THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Released;
      else
        return ButtonState::Up;
  }
  ButtonState Gamepad::LeftStick_Down() const
  {
    float y = (float)currentState.Gamepad.sThumbLY / 32767.0f;
    float yp = (float)previousState.Gamepad.sThumbLY / 32767.0f;
    if (y < -THUMBSTICK_MENU_THRESHOLD)
      if (yp < -THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Down;
      else
        return ButtonState::Pressed;
    else
      if (yp < -THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Released;
      else
        return ButtonState::Up;
  }
  ButtonState Gamepad::LeftStick_Left() const
  {
    float x = (float)currentState.Gamepad.sThumbLX / 32767.0f;
    float xp = (float)previousState.Gamepad.sThumbLX / 32767.0f;
    if (x < -THUMBSTICK_MENU_THRESHOLD)
      if (xp < -THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Down;
      else
        return ButtonState::Pressed;
    else
      if (xp < -THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Released;
      else
        return ButtonState::Up;
  }
  ButtonState Gamepad::LeftStick_Right() const
  {
    float x = (float)currentState.Gamepad.sThumbLX / 32767.0f;
    float xp = (float)previousState.Gamepad.sThumbLX / 32767.0f;
    if (x > THUMBSTICK_MENU_THRESHOLD)
      if (xp > THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Down;
      else
        return ButtonState::Pressed;
    else
      if (xp > THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Released;
      else
        return ButtonState::Up;
  }
  ButtonState Gamepad::RightStick_Up() const
  {
    float y = (float)currentState.Gamepad.sThumbRY / 32767.0f;
    float yp = (float)previousState.Gamepad.sThumbRY / 32767.0f;
    if (y > THUMBSTICK_MENU_THRESHOLD)
      if (yp > THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Down;
      else
        return ButtonState::Pressed;
    else
      if (yp > THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Released;
      else
        return ButtonState::Up;
  }
  ButtonState Gamepad::RightStick_Down() const
  {
    float y = (float)currentState.Gamepad.sThumbRY / 32767.0f;
    float yp = (float)previousState.Gamepad.sThumbRY / 32767.0f;
    if (y < -THUMBSTICK_MENU_THRESHOLD)
      if (yp < -THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Down;
      else
        return ButtonState::Pressed;
    else
      if (yp < -THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Released;
      else
        return ButtonState::Up;
  }
  ButtonState Gamepad::RightStick_Left() const
  {
    float x = (float)currentState.Gamepad.sThumbRX / 32767.0f;
    float xp = (float)previousState.Gamepad.sThumbRX / 32767.0f;
    if (x < -THUMBSTICK_MENU_THRESHOLD)
      if (xp < -THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Down;
      else
        return ButtonState::Pressed;
    else
      if (xp < -THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Released;
      else
        return ButtonState::Up;
  }
  ButtonState Gamepad::RightStick_Right() const
  {
    float x = (float)currentState.Gamepad.sThumbRX / 32767.0f;
    float xp = (float)previousState.Gamepad.sThumbRX / 32767.0f;
    if (x > THUMBSTICK_MENU_THRESHOLD)
      if (xp > THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Down;
      else
        return ButtonState::Pressed;
    else
      if (xp > THUMBSTICK_MENU_THRESHOLD)
        return ButtonState::Released;
      else
        return ButtonState::Up;
  }

  void Input::Initialize()
  {
    Gamepad &gp = Input::gamepads[0];
    Keyboard &kb = Input::keyboard;

    autoplay = false;
    UpdateGamepads();

    if (gp.Status() == Gamepad::ConnectionStatus::Connected || gp.Status() == Gamepad::ConnectionStatus::JustConnected)
      mode = Input::Mode::Gamepad;
    else
      mode = Input::Mode::Keyboard;

    /********************* System Actions *****************************/

    systemActions.insert({ "editor_activate", [&]()
    {
      return kb.KeyState(Keyboard::Key::F6) == ButtonState::Pressed;
    } });

    systemActions.insert({ "deactivate_autoplay", [&]()
    {
      return kb.KeyState(Keyboard::Key::Escape) == ButtonState::Pressed || gp.Start() == ButtonState::Pressed;
    } });

    systemActions.insert({ "editor_save", [&]()
    {
      return kb.KeyState(Keyboard::Key::F7) == ButtonState::Pressed;
    } });

    systemActions.insert({ "load_level", [&]()
    {
      return gp.Back() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::F8) == ButtonState::Pressed;
    } });

    systemActions.insert({ "console", [&]()
    {
      return kb.KeyState(Keyboard::Key::Tilde) == ButtonState::Pressed;
    } });

    systemActions.insert({ "create_terrain_at_mouse", [&]()
    {
      return kb.KeyState(Keyboard::Key::F2) == ButtonState::Pressed;
    } });

    systemActions.insert({ "create_entity_at_mouse", [&]()
    {
      return kb.KeyState(Keyboard::Key::F1) == ButtonState::Pressed;
    } });

    systemActions.insert({ "create_last_archetype", [&]()
    {
      return (kb.KeyState(Keyboard::Key::LeftShift) == ButtonState::Down && Input::mouse.mouseStates[int(Mouse::Button::Left)] == ButtonState::Pressed);
    } });

    systemActions.insert({ "restart", [&]()
    {
      return kb.KeyState(Keyboard::Key::F5) == ButtonState::Pressed;
    } });

    systemActions.insert({ "autoplay", [&]()
    {
      return kb.KeyState(Keyboard::Key::Equals) == ButtonState::Pressed;
    } });

    // Ctrl + G = God Mode
    systemActions.insert({ "god_mode", [&]()
    {
      return ((gp.RightTrigger() > 0.75 && gp.Back() == ButtonState::Pressed)) || (kb.KeyState(Keyboard::Key::Control) == ButtonState::Down && kb.KeyState(Keyboard::Key::G) == ButtonState::Pressed);
    } });

    // Ctrl + N = Next Level
    systemActions.insert({ "next_level", [&]()
    {
      return (kb.KeyState(Keyboard::Key::Control) == ButtonState::Down && kb.KeyState(Keyboard::Key::N) == ButtonState::Pressed);
    } });

    // Ctrl + B = Previous Level
    systemActions.insert({ "previous_level", [&]()
    {
      return (kb.KeyState(Keyboard::Key::Control) == ButtonState::Down && kb.KeyState(Keyboard::Key::B) == ButtonState::Pressed);
    } });

    // Ctrl + M = End Game
    systemActions.insert({ "end_game", [&]()
    {
      return (kb.KeyState(Keyboard::Key::Control) == ButtonState::Down && kb.KeyState(Keyboard::Key::M) == ButtonState::Pressed);
    } });

    /********************* Gameplay Actions *****************************/

    actions.insert({ "menu_up", [&]()
    {
      return gp.DpadUp() == ButtonState::Pressed || gp.RightStick_Up() == ButtonState::Pressed || gp.LeftStick_Up() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::Up) == ButtonState::Pressed;
    } });

    actions.insert({ "menu_down", [&]()
    {
      return gp.DpadDown() == ButtonState::Pressed || gp.RightStick_Down() == ButtonState::Pressed || gp.LeftStick_Down() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::Down) == ButtonState::Pressed;
    } });

    actions.insert({ "menu_left", [&]()
    {
      return gp.DpadLeft() == ButtonState::Pressed || gp.RightStick_Left() == ButtonState::Pressed || gp.LeftStick_Left() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::Left) == ButtonState::Pressed;
    } });

    actions.insert({ "menu_right", [&]()
    {
      return gp.DpadRight() == ButtonState::Pressed || gp.RightStick_Right() == ButtonState::Pressed || gp.LeftStick_Right() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::Right) == ButtonState::Pressed;
    } });

    actions.insert({ "menu_confirm", [&]()
    {
      return gp.A() == ButtonState::Pressed || gp.Start() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::Space) == ButtonState::Pressed || kb.KeyState(Keyboard::Key::Enter) == ButtonState::Pressed;
    } });

    actions.insert({ "menu_cancel", [&]()
    {
      return gp.B() == ButtonState::Pressed || gp.Back() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::Escape) == ButtonState::Pressed;
    } });

    actions.insert({ "pause", [&]()
    {
      return gp.Start() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::Escape) == ButtonState::Pressed;
    } });

    actions.insert({ "jump", [&]()
    {
      return gp.A() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::Space) == ButtonState::Pressed;
    } });

    actions.insert({ "jump_hold", [&]()
    {
      return gp.A() == ButtonState::Down || kb.KeyState(Keyboard::Key::Space) == ButtonState::Down;
    } });

    actions.insert({ "attack_punch", [&]()
    {
      return gp.X() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::X) == ButtonState::Pressed;
    } });

    actions.insert({ "attack_punch_hold", [&]()
    {
      return gp.X() == ButtonState::Down || kb.KeyState(Keyboard::Key::X) == ButtonState::Down;
    } });

    actions.insert({ "throw", [&]()
    {
      return gp.Y() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::F) == ButtonState::Pressed;
    } });

    actions.insert({ "pickup_pressed", [&]()
    {
      return gp.B() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::S) == ButtonState::Pressed;
    } });

    actions.insert({ "pickup_hold", [&]()
    {
      return gp.B() == ButtonState::Down || kb.KeyState(Keyboard::Key::S) == ButtonState::Down;
    } });

    actions.insert({ "move_left", [&]()
    {
      return gp.DpadLeft() == ButtonState::Down || gp.LeftStick().x < 0.0f || kb.KeyState(Keyboard::Key::Left) == ButtonState::Down;
    } });

    actions.insert({ "move_right", [&]()
    {
      return gp.DpadRight() == ButtonState::Down || gp.LeftStick().x > 0.0f || kb.KeyState(Keyboard::Key::Right) == ButtonState::Down;
    } });

    actions.insert({ "evadeLeft", [&]()
    {
      return gp.LB() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::Z) == ButtonState::Pressed;
    } });

    actions.insert({ "evadeRight", [&]()
    {
      return gp.RB() == ButtonState::Pressed || kb.KeyState(Keyboard::Key::C) == ButtonState::Pressed;
    } });

    actions.insert({ "Num1", [&]()
    {
      return (kb.KeyState(Keyboard::Key::Num1) == ButtonState::Pressed && kb.KeyState(Keyboard::Key::LeftShift) == ButtonState::Down);
    } });

    actions.insert({ "Num2", [&]()
    {
      return (kb.KeyState(Keyboard::Key::Num2) == ButtonState::Pressed && kb.KeyState(Keyboard::Key::LeftShift) == ButtonState::Down);
    } });

    actions.insert({ "Num3", [&]()
    {
      return (kb.KeyState(Keyboard::Key::Num3) == ButtonState::Pressed && kb.KeyState(Keyboard::Key::LeftShift) == ButtonState::Down);
    } });

    actions.insert({ "Num4", [&]()
    {
      return (kb.KeyState(Keyboard::Key::Num4) == ButtonState::Pressed && kb.KeyState(Keyboard::Key::LeftShift) == ButtonState::Down);
    } });

    actions.insert({ "Num5", [&]()
    {
      return (kb.KeyState(Keyboard::Key::Num5) == ButtonState::Pressed && kb.KeyState(Keyboard::Key::LeftShift) == ButtonState::Down);
    } });

    actions.insert({ "Num6", [&]()
    {
      return (kb.KeyState(Keyboard::Key::Num6) == ButtonState::Pressed && kb.KeyState(Keyboard::Key::LeftShift) == ButtonState::Down);
    } });

    actions.insert({ "Num7", [&]()
    {
      return (kb.KeyState(Keyboard::Key::Num7) == ButtonState::Pressed && kb.KeyState(Keyboard::Key::LeftShift) == ButtonState::Down);
    } });

    actions.insert({ "Num8", [&]()
    {
      return (kb.KeyState(Keyboard::Key::Num8) == ButtonState::Pressed && kb.KeyState(Keyboard::Key::LeftShift) == ButtonState::Down);
    } });

    actions.insert({ "Num9", [&]()
    {
      return (kb.KeyState(Keyboard::Key::Num9) == ButtonState::Pressed && kb.KeyState(Keyboard::Key::LeftShift) == ButtonState::Down);
    } });

    actions.insert({ "zoom_in", [&]()
    {
      return kb.KeyState(Keyboard::Key::Up) == ButtonState::Down || mouse.buttons_current[unsigned(Mouse::Button::ScrollUp)];
    } });

    actions.insert({ "zoom_out", [&]()
    {
      return kb.KeyState(Keyboard::Key::Down) == ButtonState::Down || mouse.buttons_current[unsigned(Mouse::Button::ScrollDown)];
    } });


    actions.insert({ "debug_explode", [&]()
    {
      return kb.KeyState(Keyboard::Key::E) == ButtonState::Pressed;
    } });

    actions.insert({ "destroy_enemies_on_screen", [&]()
    {
      return kb.KeyState(Keyboard::Key::F4) == ButtonState::Pressed;
    } });

    actions.insert({ "damage_enemies_on_screen", [&]()
    {
      return kb.KeyState(Keyboard::Key::F3) == ButtonState::Pressed;
    } });

    actions.insert({ "debug_bullettime", [&]()
    {
      return gp.Y() == ButtonState::Pressed;
    } });

    // l/r mouse, enter, space bar, escape, start, a
    actions.insert({ "skip_menus", [&]()
    {
      return (Input::mouse.mouseStates[int(Mouse::Button::Left)] == ButtonState::Pressed || Input::mouse.mouseStates[int(Mouse::Button::Right)] == ButtonState::Pressed || 
        kb.KeyState(Keyboard::Key::Enter) == ButtonState::Pressed || kb.KeyState(Keyboard::Key::Space) == ButtonState::Pressed || kb.KeyState(Keyboard::Key::Escape) == ButtonState::Pressed || 
        gp.Start() == ButtonState::Pressed || gp.A() == ButtonState::Pressed);
    } });
  }

  void Input::Shutdown()
  {
    actions.clear();
  }

  void Input::Update()
  {
    HWND console = GetConsoleWindow();
    HWND game = Game::Instance().GetWindowSystem()->GetWindowHandle();
    HWND current = GetForegroundWindow();

    if ((unsigned)current != (unsigned)game && (unsigned)current != (unsigned)console)
      return;

    UpdateGamepads();
    keyboard.Update();
    mouse.Update();
    Input::modeChanged = (Input::mode != Input::modePrevious);
    Input::modePrevious = Input::mode;

    /*if (Check("console"))
    {
      std::cout << "Game:" << game << " Console:" << console << " Current:" << current << std::endl;
      
      if (current == game)
      {
        ShowWindow(console, SW_SHOW);
        FlashWindow(console, true);
        SetFocus(console);
      }
      else if (current == console)
      {
        ShowWindow(console, SW_HIDE);
        FlashWindow(game, true);
        SetFocus(game);
      }
    }*/
  }

  void Input::UpdateGamepads()
  {
    for (DWORD i = 0; i< XUSER_MAX_COUNT; i++)
    {
      gamepads[i].Update(i);
    }
  }

  ButtonState Keyboard::KeyState(Keyboard::Key key) const
  {
    return Input::MakeButtonState(previousState[(int)key], currentState[(int)key]);
  }
  void Keyboard::Update()
  {
    std::swap(currentState, previousState);
    for (int i = 0; i < (int)Key::_Last; ++i)
    {
      currentState[i] = (GetAsyncKeyState(i) != 0);
      if (currentState[i])
        Input::mode = Input::Mode::Keyboard;
    }
  }

  void Mouse::Update()
  {
    GetCursorPos(&position);
    ScreenToClient(Game::Instance().GetWindowSystem()->GetWindowHandle(), &position);
    /*if (position.x != position_last.x || position.y != position_last.y)
      TwMouseMotion(position.x, position.y);

    Omni::ButtonState left = Input::MakeButtonState(buttons_last[(unsigned)Button::Left], buttons_current[(unsigned)Button::Left]);
    Omni::ButtonState right = Input::MakeButtonState(buttons_last[(unsigned)Button::Right], buttons_current[(unsigned)Button::Right]);
    if (left == Omni::ButtonState::Pressed)
      TwMouseButton(TW_MOUSE_PRESSED, TW_MOUSE_LEFT);
    if (left == Omni::ButtonState::Released)
      TwMouseButton(TW_MOUSE_RELEASED, TW_MOUSE_LEFT);
    if (right == Omni::ButtonState::Pressed)
      TwMouseButton(TW_MOUSE_PRESSED, TW_MOUSE_RIGHT);
    if (right == Omni::ButtonState::Released)
      TwMouseButton(TW_MOUSE_RELEASED, TW_MOUSE_RIGHT);*/

    // loop through each mouse button, and update the mouse states
    for (unsigned i = 0; i < (unsigned)Button::_Last; ++i)
    {
      // make sure to reset to up at the beginning of frame
      mouseStates[i] = ButtonState::Up;

      // if currently pressed
      if (buttons_current[i])
      {
        // if last was also true, set to down
        if (buttons_last[i])
          mouseStates[i] = ButtonState::Down;
        else
          mouseStates[i] = ButtonState::Pressed;
      }
      // if currently not pressed
      else
      {
        // if last was also true, set to released
        if (buttons_last[i])
          mouseStates[i] = ButtonState::Released;
      }
    }

    for (unsigned i = 0; i < (unsigned)Button::_Last; ++i)
      buttons_last[i] = buttons_current[i];
  }
}
