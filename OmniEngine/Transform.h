/******************************************************************************
Filename: Transform.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "stdafx.h"
#include "Vec2.h"
#include "Matrix3.h"
#include "v8.h"
#include "Scriptable.h"

namespace Omni
{
  class Transform : public Scriptable<Transform>
  {
  public:
    Transform();
    ~Transform(){};

    Vec2 translation = { 0, 0 };
    Vec2 scale = { 1, 1 };

    bool gottenLastFrameTrans;
    Vec2 lastFrameTrans;
    Vec2 lastFrameScale;
    float lastFrameRot;

    float GetRotation() const
    {
      return _rotation;
    }
    void SetRotation(float value)
    {
      if (value < 0.0f)
        value += 3.141593f * 2.0f;
      if (value >= 3.141593f * 2.0f)
        value -= 3.141593f * 2.0f;
      _rotation = value;
    }
    __declspec(property(get = GetRotation, put = SetRotation)) float rotation;

    inline Vec2 GetLastTranslation() { return lastTranslation; }
    inline Vec2 GetLastScale() { return lastScale; }
    inline float GetLastRotation() { return lastRotation; }

    bool HasChanged();
    bool Transform::HasChanged3();

    D3DXMATRIX GetMatrix();
    D3DXMATRIX GetCameraMatrix();
    Matrix3 GetMatrix3();

    void UpdateLastTransformData();

    static void v8_Register();

  private:

    float _rotation;

    Vec2 lastTranslation;
    Vec2 lastScale;
    float lastRotation;
    D3DXMATRIX lastMatrix;

    Vec2 lastTranslation3;
    Vec2 lastScale3;
    float lastRotation3;
    Matrix3 lastMatrix3;
  };
}
