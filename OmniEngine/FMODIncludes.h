/******************************************************************************
Filename: FMODIncludes.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "fmod_studio.hpp"
#include "fmod_studio_common.h"
#include "fmod.hpp"
#include "fmod_errors.h"
