/******************************************************************************
Filename: Camera.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Space.h"
#include <cmath>
#include <cstdlib>

namespace Omni
{

  float HackyLerp(float x, float target, float speed = 0.1f)
  {
    /*float ret = x + (target - x) * speed;
    if (fabs(x - target) <= 0.001f)
      ret = target;
    return ret;*/
    return target;
  }

  Transform Camera::GetScaledTransform() const
  {
    Transform modified(transform);
    modified.translation.x += shakeOffset.x;
    modified.translation.y += shakeOffset.y;
    modified.rotation = transform.rotation + shakeAngle;
    modified.scale = Vec2((size + shakeOffset.z) / 2.0f, (size + shakeOffset.z) / 2.0f);

    return modified;
  }

  void Camera::Shake(float distance, float duration, float zoom, float rotation = 0.025f)
  {
    shakeMagnitude = distance;
    shakeZoom = zoom;
    shakeTime = 0.0f;
    shakeDuration = duration;
    shakeRotation = rotation;
  }

  void Camera::Update(float dt)
  {
    // move
    if (followingEntity)
    {
      transform.translation.x = HackyLerp(transform.translation.x, followingEntity->transform.translation.x + offset.x, 0.18f);
      transform.translation.y = HackyLerp(transform.translation.y, followingEntity->transform.translation.y + offset.y, 0.18f);

      // Keep above minimum
      if (transform.translation.y < -5.8f)
        transform.translation.y = -5.8f;
    }

    // shake
    shakeTime += dt;
    if (shakeTime >= shakeDuration)
    {
      shakeMagnitude = 0.0f;
      shakeOffset.x = 0.0f;
      shakeOffset.y = 0.0f;
      shakeOffset.z = 0.0f;
      shakeAngle = 0.0f;
    }
    else
    {
      float shakeCurrentMagnitude = shakeMagnitude - shakeMagnitude * (shakeTime / shakeDuration);
      float shakeCurrentZoom = shakeZoom - shakeZoom * (shakeTime / shakeDuration);
      float theta = static_cast<float>(rand()) / static_cast<float>(RAND_MAX)* 3.14f;
      float theta2 = (-shakeRotation / 2.0f + static_cast<float>(rand()) / static_cast<float>(RAND_MAX)* shakeRotation) * shakeCurrentMagnitude;
      shakeAngle = theta2;// theta2 - theta2 * (shakeTime / shakeDuration);
      shakeOffset.x = shakeCurrentMagnitude * cos(theta);
      shakeOffset.y = shakeCurrentMagnitude * sin(theta);
      shakeOffset.z = -shakeCurrentZoom / 2.0f + static_cast<float>(rand()) / static_cast<float>(RAND_MAX)* shakeCurrentZoom;
    }
  }

  void Camera::Follow(Handlet<Entity> entity)
  {
    followingEntity = entity;
  }

  void Camera::StopFollowing()
  {
    followingEntity = nullptr;
  }

  void Camera::SetOffset(float x, float y, float time)
  {
    offset.Set(x, y);
  }

  void Camera::Pan(Vec2 vec, float duration = 0.0f) { Console::Info("Camera::Pan() not implemented yet."); }
  void Camera::StopPanning() { Console::Info("Camera::StopPanning() not implemented yet."); }
  void Camera::Zoom(float scale, float duration = 0.0f) { Console::Info("Camera::Zoom() not implemented yet."); }
  void Camera::StopZooming() { Console::Info("Camera::StopZooming() not implemented yet."); }

  static void v8_Pan(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());
    
    Camera *camera = Take<Camera>::FromHolder_NonComponent(info);

    LocalObject vector = LocalObject::Cast(info[0]);
    Vec2 vec = { float(vector->Get(String::NewFromUtf8(info.GetIsolate(), "x"))->NumberValue()),
      float(vector->Get(String::NewFromUtf8(info.GetIsolate(), "y"))->NumberValue()) };
  
    // if there is a duration given, send it in
    if (info.Length() > 1)
      camera->Pan(vec, Take<float>::FromV8(info[1]));
    else
      camera->Pan(vec);
  }

  static void v8_StopPanning(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Camera *camera = Take<Camera>::FromHolder_NonComponent(info);
    camera->StopPanning();
  }

  static void v8_Zoom(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Camera *camera = Take<Camera>::FromHolder_NonComponent(info);

    // if there is a duration given, send it in
    if (info.Length() > 1)
      camera->Zoom(Take<float>::FromV8(info[0]), Take<float>::FromV8(info[1]));
    else
      camera->Zoom(Take<float>::FromV8(info[0]));
  }
  
  static void v8_StopZooming(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Camera *camera = Take<Camera>::FromHolder_NonComponent(info);
    camera->StopZooming();
  }

  static void v8_Follow(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Camera *camera = Take<Camera>::FromHolder_NonComponent(info);
    
    // unwrapping and getting the animator pointer
    Local<Object> entityObj = Local<Object>::Cast(info[0]);
    Local<External> wrap = Local<External>::Cast(entityObj->GetInternalField(0));
    Entity **entitySelfPtr = static_cast<Entity **>(wrap->Value());

    camera->Follow(Handlet<Entity>(entitySelfPtr));
  }

  static void v8_StopFollowing(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Camera *camera = Take<Camera>::FromHolder_NonComponent(info);
    camera->StopFollowing();
  }

  static void v8_Shake(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Camera *camera = Take<Camera>::FromHolder_NonComponent(info);

    // if there is a rotation given, send it in
    if (info.Length() > 3)
      camera->Shake(Take<float>::FromV8(info[0]), Take<float>::FromV8(info[1]), Take<float>::FromV8(info[2]), Take<float>::FromV8(info[3]));
    else
      camera->Shake(Take<float>::FromV8(info[0]), Take<float>::FromV8(info[1]), Take<float>::FromV8(info[2]));
  }

  static void v8_SetOffset(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Camera *camera = Take<Camera>::FromHolder_NonComponent(info);

    // if there is a time given, send it in
    if (info.Length() > 2)
      camera->SetOffset(Take<float>::FromV8(info[0]), Take<float>::FromV8(info[1]), Take<float>::FromV8(info[2]));
    else
      camera->SetOffset(Take<float>::FromV8(info[0]), Take<float>::FromV8(info[1]));
  }

  static void v8_GetSize(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // getting the camera pointer
    Camera *camera = Take<Camera>::FromHolder_NonComponent(info);

    info.GetReturnValue().Set(Number::New(info.GetIsolate(), camera->GetSize()));
  }

  static void v8_SetSize(v8::Local<v8::String> property, v8::Local<v8::Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the transform pointer
    Camera *camera = Take<Camera>::FromHolder_NonComponent(info);

    camera->SetSize(float(value->NumberValue()));
  }

  void Camera::v8_Register()
  {
    using namespace v8;

    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    Local<ObjectTemplate> objTemplate_temp = ObjectTemplate::New(g_Isolate);

    objTemplate_temp->SetAccessor(String::NewFromUtf8(g_Isolate, "transform"), AccessorGetterCallback(v8_GetTransform), 0);
    objTemplate_temp->Set(String::NewFromUtf8(g_Isolate, "Zoom"), FunctionTemplate::New(g_Isolate, FunctionCallback(v8_Zoom)));
    objTemplate_temp->Set(String::NewFromUtf8(g_Isolate, "StopZooming"), FunctionTemplate::New(g_Isolate, FunctionCallback(v8_StopZooming)));
    objTemplate_temp->Set(String::NewFromUtf8(g_Isolate, "Pan"), FunctionTemplate::New(g_Isolate, FunctionCallback(v8_Pan)));
    objTemplate_temp->Set(String::NewFromUtf8(g_Isolate, "StopPanning"), FunctionTemplate::New(g_Isolate, FunctionCallback(v8_StopPanning)));
    objTemplate_temp->Set(String::NewFromUtf8(g_Isolate, "Follow"), FunctionTemplate::New(g_Isolate, FunctionCallback(v8_Follow)));
    objTemplate_temp->Set(String::NewFromUtf8(g_Isolate, "StopFollowing"), FunctionTemplate::New(g_Isolate, FunctionCallback(v8_StopFollowing)));
    objTemplate_temp->Set(String::NewFromUtf8(g_Isolate, "Shake"), FunctionTemplate::New(g_Isolate, FunctionCallback(v8_Shake)));
    objTemplate_temp->Set(String::NewFromUtf8(g_Isolate, "SetOffset"), FunctionTemplate::New(g_Isolate, FunctionCallback(v8_SetOffset)));
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "size"), AccessorGetterCallback(v8_GetSize), AccessorSetterCallback(v8_SetSize));

    objTemplate_temp->SetInternalFieldCount(1);

    objTemplate.Reset(g_Isolate, objTemplate_temp);
  }

  void Space::v8_GetCamera(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    using namespace v8;

    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the space pointer
    Local<Object> self = info.Holder(); 
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    Space *space = static_cast<Space *>(wrap->Value());

    // getting the camera pointer
    Camera *camera = &space->camera;

    // create an instance of the js object
    Handle<Object> result = camera->v8_GetObject();

    // create a wrapper for the actual pointer
    Handle<External> cameraPtr = External::New(info.GetIsolate(), camera);

    // store the pointer in the js object
    result->SetInternalField(0, cameraPtr);

    info.GetReturnValue().Set(result);
  }
}
