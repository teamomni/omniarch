/******************************************************************************
Filename: Vec4.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Game.h"

#define v8_Vec4Gettor(target) \
static void v8_Vec4Get_##target(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value>& info)\
{\
  using namespace v8; \
  HandleScope scope(info.GetIsolate()); \
  Property::Color *color = Take<Property::Color>::FromHolder_NonComponent(info); \
  double f = (double)color->target; \
  Local<Value> number = Number::New(info.GetIsolate(), f); \
  info.GetReturnValue().Set(number); \
}

#define v8_Vec4Settor(target) \
  static void v8_Vec4Set_##target(v8::Local<v8::String> property, v8::Local<v8::Value> value, const v8::FunctionCallbackInfo<v8::Value>& info)\
{\
  using namespace v8; \
  HandleScope scope(info.GetIsolate()); \
  Property::Color *color = Take<Property::Color>::FromHolder_NonComponent(info); \
  color->target = float(value->NumberValue()); \
}

namespace Omni
{
  extern v8::Isolate *g_Isolate;

  v8_Vec4Gettor(r);
  v8_Vec4Gettor(g);
  v8_Vec4Gettor(b);
  v8_Vec4Gettor(a);

  v8_Vec4Settor(r);
  v8_Vec4Settor(g);
  v8_Vec4Settor(b);
  v8_Vec4Settor(a);

  static void v8_Vec4Constructor(const v8::FunctionCallbackInfo<v8::Value>& args)
  {
    v8::HandleScope scope(args.GetIsolate());

    // create a js version and return it
    v8::Handle<v8::Object> self = args.Holder();

    args.GetReturnValue().Set(self);
  }

  void Game::v8_RegisterVec4(v8::Handle<v8::ObjectTemplate> globalTemplate)
  {
    using namespace v8;

    HandleScope scope(g_Isolate);

    Handle<FunctionTemplate> local_Vec2Template = FunctionTemplate::New(isolate, FunctionCallback(v8_Vec4Constructor));
    local_Vec2Template->SetClassName(String::NewFromUtf8(isolate, "Color"));

    Handle<ObjectTemplate> vec2_instance_template = local_Vec2Template->InstanceTemplate();

    // create space for the c++ pointer
    vec2_instance_template->SetInternalFieldCount(1);

    vec2_instance_template->SetAccessor(String::NewFromUtf8(isolate, "r"), AccessorGetterCallback(v8_Vec4Get_r), AccessorSetterCallback(v8_Vec4Set_r));
    vec2_instance_template->SetAccessor(String::NewFromUtf8(isolate, "g"), AccessorGetterCallback(v8_Vec4Get_g), AccessorSetterCallback(v8_Vec4Set_g));
    vec2_instance_template->SetAccessor(String::NewFromUtf8(isolate, "b"), AccessorGetterCallback(v8_Vec4Get_b), AccessorSetterCallback(v8_Vec4Set_b));
    vec2_instance_template->SetAccessor(String::NewFromUtf8(isolate, "a"), AccessorGetterCallback(v8_Vec4Get_a), AccessorSetterCallback(v8_Vec4Set_a));

    globalTemplate->Set(String::NewFromUtf8(isolate, "Color"), local_Vec2Template);
  }

}