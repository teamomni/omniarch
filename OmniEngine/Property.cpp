/******************************************************************************
Filename: Property.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Component.h"

namespace Omni
{

  GettorFunctionPointer PropertyDefinition::CreateGettor()
  {
    using namespace v8;

    switch (type)
    {
    case Property::Type::Bool:
      return [](v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info) 
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        auto &game = Game::Instance();
        Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        bool prop = *component->properties[name].GetBool(component);

        info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), prop));
      };
      break;
    case Property::Type::Color:
      return[](v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        auto &game = Game::Instance();
        Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        Property::Color *prop = component->properties[name].GetColor(component);

        // getting the color template from the global object
        auto enteredContext = game.isolate->GetEnteredContext();
        Local<Value> color_raw = enteredContext->Global()->Get(v8::String::NewFromUtf8(info.GetIsolate(), "Color"));
        Local<Function> colorConstructor = Handle<Function>::Cast(color_raw);

        // make a new js object and store the color pointer in there
        Local<Object> colorInstance = colorConstructor->NewInstance();
        colorInstance->SetInternalField(0, External::New(info.GetIsolate(), prop));

        info.GetReturnValue().Set(colorInstance);
      };
      break;
    case Property::Type::Float:
      return [](v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        auto &game = Game::Instance();
        Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        float prop = *component->properties[name].GetFloat(component);

        info.GetReturnValue().Set(Number::New(info.GetIsolate(), double(prop)));
      };
      break;
    case Property::Type::Int:
      return [](v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        auto &game = Game::Instance();
        Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        int prop = *component->properties[name].GetInt(component);

        info.GetReturnValue().Set(Integer::New(info.GetIsolate(), prop));
      };
      break;
    case Property::Type::String:
      return [](v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        auto &game = Game::Instance();
        Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        std::string prop(*component->properties[name].GetString(component));

        info.GetReturnValue().Set(v8::String::NewFromUtf8(info.GetIsolate(), prop.c_str()));
      };
      break;
    case Property::Type::Undefined:
      return nullptr;
      break;
    case Property::Type::Unsigned:
      return [](v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        auto &game = Game::Instance();
        Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        int prop = int(*component->properties[name].GetUnsigned(component));

        info.GetReturnValue().Set(Integer::New(info.GetIsolate(), prop));
      };
      break;
    case Property::Type::Vec2:
      return [](v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        Property::Vec2 *prop = component->properties[name].GetVec2(component);

        // getting the vec2 template from the global object
        Game &game = Game::Instance();
        Local<Value> vec2_raw = game.isolate->GetEnteredContext()->Global()->Get(v8::String::NewFromUtf8(info.GetIsolate(), "Vector"));
        Local<Function> vec2Constructor = Handle<Function>::Cast(vec2_raw);

        // make a new js object and store the vec2 pointer in there
        Local<Object> vec2Instance = vec2Constructor->NewInstance();
        vec2Instance->SetInternalField(0, External::New(info.GetIsolate(), prop));

        info.GetReturnValue().Set(vec2Instance);
      };
      break;
    case Property::Type::Vec2List:
      return nullptr;
      break;
    }

    return nullptr;
  }
  SettorFunctionPointer PropertyDefinition::CreateSettor()
  {
    using namespace v8;

    switch (type)
    {
    case Property::Type::Bool:
      return [](v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        Property::Bool *prop = component->properties[name].GetBool(component);

        // assign the actual value
        *prop = value->BooleanValue();
      };
      break;
    case Property::Type::Color:
      return [](v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        Property::Color *prop = component->properties[name].GetColor(component);

        // get the stuff from the vector object
        Local<Object> colorObj = Local<Object>::Cast(value);
        float newR = float(colorObj->Get(v8::String::NewFromUtf8(info.GetIsolate(), "r"))->NumberValue());
        float newG = float(colorObj->Get(v8::String::NewFromUtf8(info.GetIsolate(), "g"))->NumberValue());
        float newB = float(colorObj->Get(v8::String::NewFromUtf8(info.GetIsolate(), "b"))->NumberValue());
        float newA = float(colorObj->Get(v8::String::NewFromUtf8(info.GetIsolate(), "a"))->NumberValue());

        // assign the actual value
        prop->r = newR;
        prop->g = newG;
        prop->b = newB;
        prop->a = newA;
      };
      break;
    case Property::Type::Float:
      return [](v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        Property::Float *prop = component->properties[name].GetFloat(component);

        // assign the actual value
        *prop = float(value->NumberValue());
      };
      break;
    case Property::Type::Int:
      return [](v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        Property::Int *prop = component->properties[name].GetInt(component);

        // assign the actual value
        *prop = static_cast<Property::Int>(value->IntegerValue());
      };
      break;
    case Property::Type::String:
      return [](v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        Property::String *prop = component->properties[name].GetString(component);

        // assign the actual value
        *prop = *v8::String::Utf8Value(value);
      };
      break;
    case Property::Type::Undefined:
      return nullptr;
      break;
    case Property::Type::Unsigned:
      return [](v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        Property::Unsigned *prop = component->properties[name].GetUnsigned(component);

        // assign the actual value
        *prop = Property::Unsigned(value->IntegerValue());
      };
      break;
    case Property::Type::Vec2:
      return [](v8::Local<v8::String> property, Local<Value> value, const v8::FunctionCallbackInfo<v8::Value> &info)
      {
        Locker locker(info.GetIsolate());
        Isolate::Scope isolate_scope(info.GetIsolate());
        HandleScope handleScope(info.GetIsolate());

        // unwrap the component pointer
        Component *component = Take<Component>::FromHolder(info);

        // get the name of the property we're accessing
        std::string name = *(v8::String::Utf8Value(property));

        // get the actual property
        Property::Vec2 *prop = component->properties[name].GetVec2(component);

        // get the stuff from the vector object
        Local<Object> vec2Obj = Local<Object>::Cast(value);
        float newX = float(vec2Obj->Get(v8::String::NewFromUtf8(info.GetIsolate(), "x"))->NumberValue());;
        float newY = float(vec2Obj->Get(v8::String::NewFromUtf8(info.GetIsolate(), "y"))->NumberValue());;

        // assign the actual value
        prop->x = newX;
        prop->y = newY;
      };
      break;
    case Property::Type::Vec2List:
      return nullptr;
      break;
    }
    return nullptr;
  }
}
