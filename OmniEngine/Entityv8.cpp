/******************************************************************************
Filename: Entityv8.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Entity.h"
#include "Game.h"
#include "EventDispatcher.h"
#include "Component.h"

using namespace v8;

namespace Omni 
{
  static void v8_Destroy(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    v8::Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));
    Entity *entity = Take<Entity>::FromHolder(info);

    // if invalid entity, just don't do anything
    if (entity == nullptr)
      return;

    // print to console destruction
    if (game.debugMessageSetting == DebugMessageSetting::All || game.debugMessageSetting == DebugMessageSetting::Destruction)
    {
      Console::Write(entity->name);
      Console::WriteLine(" has been destroyed by v8");
    }

    // if it is the currently updating object, just set the destroying flag
    if (static_cast<int>(entity->id) == game.currentlyUpdatingIDs.top())
      entity->destroying = true;

    // if it's not the currently updating thing, just destroy it
    else
      entity->Destroy();
  }

  static void v8_OnScreen(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    v8::Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    Entity *entity = Take<Entity>::FromHolder(info);

    // if invalid entity, just return undefined
    if (entity == nullptr)
    {
      info.GetReturnValue().Set(Undefined(info.GetIsolate()));
      return;
    }
    
    info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), entity->space->QueryOnScreen(entity)));
  }

  static void v8_GetName(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    v8::Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    Entity *owner = Take<Entity>::FromHolder(info);

    // if invalid entity, just return undefined
    if (owner == nullptr)
      info.GetReturnValue().Set(Undefined(info.GetIsolate()));

    info.GetReturnValue().Set(String::NewFromUtf8(info.GetIsolate(), owner->name.c_str()));
  }

  static void v8_HasComponent(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    v8::Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    Entity *owner = Take<Entity>::FromHolder(info);

    // if invalid entity, just return undefined
    if (owner == nullptr)
    {
      info.GetReturnValue().Set(Undefined(info.GetIsolate()));
      return;
    }

    std::string compName = Take<std::string>::FromV8(info[0]);

    bool result = owner->HasComponent(compName);
    info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), result));
  }

  static void v8_FollowEntity(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    auto &game = Game::Instance();
    v8::Context::Scope contextScope(Local<Context>::New(info.GetIsolate(), game.context));

    Entity *owner = Take<Entity>::FromHolder(info);
    
    if (owner == nullptr)
      return;

    LocalObject obj = LocalObject::Cast(info[0]);
    LocalExternal wrap = LocalExternal::Cast(obj->GetInternalField(0));
    Entity **ptr = static_cast<Entity **>(wrap->Value());

    if (ptr == nullptr)
      return;

    Entity *target = *ptr;

    if (target == nullptr)
      return;

    float xOffset = Take<float>::FromV8(info[1]);
    float yOffset = Take<float>::FromV8(info[2]);
    Vec2 offset(xOffset, yOffset);

    owner->transform.lastFrameTrans = target->transform.lastFrameTrans + offset;
    owner->transform.translation = target->transform.translation + offset;
  }

  void Entity::v8_Register()
  {
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    v8::Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    Local<ObjectTemplate> objTemplate_temp = ObjectTemplate::New(game.isolate);

    objTemplate_temp->SetInternalFieldCount(1);

    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "RigidBody"), AccessorGetterCallback(Entity::v8_GetRigidBody), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "Sprite"), AccessorGetterCallback(Entity::v8_GetSprite), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "ParticleEmitter"), AccessorGetterCallback(Entity::v8_GetParticleEmitter), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "Animator"), AccessorGetterCallback(Entity::v8_GetAnimator), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "space"), AccessorGetterCallback(Entity::v8_GetSpace), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "transform"), AccessorGetterCallback(v8_GetTransform), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "Clickable"), AccessorGetterCallback(v8_GetClickable), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "SoundEmitter"), AccessorGetterCallback(v8_GetSoundEmitter), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "name"), AccessorGetterCallback(v8_GetName), 0);
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "GetModule_CPP"), Function::New(game.isolate, v8_GetUserComponent_CPP));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "GetComponent"), Function::New(game.isolate, v8_GetUserComponent));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "HasComponent"), Function::New(game.isolate, v8_HasComponent));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "DispatchEvent"), Function::New(game.isolate, &EventDispatcher::v8_DispatchEvent<Entity>));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "Destroy"), Function::New(game.isolate, v8_Destroy));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "OnScreen"), Function::New(game.isolate, v8_OnScreen));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "FollowEntity"), Function::New(game.isolate, v8_FollowEntity));

    objTemplate.Reset(game.isolate, objTemplate_temp);
  }

  Handle<Object> Entity::v8_Createv8Entity()
  {
    // get an instance of game
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    EscapableHandleScope scope(game.isolate);

    v8::Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    // create an instance of the JS object
    Local<Object> result = v8_GetObject();

    // create a wrapper for the actual pointer
    Local<External> entityPtr = External::New(game.isolate, this);

    // store the pointer in the js object
    result->SetInternalField(0, entityPtr);

    // return the js object
    return scope.Escape(result);
  }
}