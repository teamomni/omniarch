How to call debug drawing from anywhere:

DrawLine
=============
 Call DrawLine function
	(2 Versions):
	Game::Instance().GetDebugDraw()->DrawLine(Vec2 start, Vec2 end, size_t space); //Defualt color is red
	Game::Instance().GetDebugDraw()->DrawLine(Vec2 start, Vec2 end, size_t space, D3DXCOLOR color);
	(where D3DXCOLOR is RGBA)

DrawPoint
=============
Call DrawPoint function
	(2 Versions)
	Game::Instance().GetDebugDraw()->DrawPoint(Vec2 pos, size_t space); //Default color is red
	Game::Instance().GetDebugDraw()->DrawPoint(Vec2 pos, size_t space, D3DXCOLOR color);
	(where D3DXCOLOR is RGBA)

DrawQuad
=============
Call DrawQuad Function
	(4 Versions)
	Game::Instance().GetDebugDraw()->DrawQuad(Vec2 pos, Vec2 scale, float rot, size_t space); //Default color is red
	Game::Instance().GetDebugDraw()->DrawQuad(Vec2 pos, Vec2 scale, float rot, size_t space, D3DXCOLOR color); 
	Game::Instance().GetDebugDraw()->DrawQuad(Transform transform, size_t space); //Default Color is red
	Game::Instance().GetDebugDraw()->DrawQuad(Transform transform, size_t space, D3DXCOLOR color);
	(where D3DXCOLOR is RGBA) 

DrawArrow
=============
Call DrawArrow Function
	(2 versions)
	Game::Instance().GetDebugDraw()->DrawArrow(Vec2 start, Vec2 Normal_Or_Direction, size_t space); //Default color is red
	Game::Instance().GetDebugDraw()->DrawArrow(Vec2 start, Vec2 Normal_Or_Direction, size_t space, D3DXCOLOR color);
	(where D3DXCOLOR is RGBA)

