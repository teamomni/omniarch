/******************************************************************************
Filename: Matrix3.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include <ostream>
#include "Vec2.h"

namespace Omni
{
  struct Matrix3
  {
    /****** Public Methods ******/
    Matrix3();
    Matrix3(float theta);
    Matrix3(float scaleX, float scaleY);
    Matrix3(Vec2 translation);

    Matrix3 operator*(const Matrix3 &rhs) const;
    Matrix3 &operator*= (const Matrix3 &rhs);
    Vec2 operator*(const Vec2 &rhs) const;

    void Scale(float x, float y);
    void Scale(Vec2 scale);

    void Rotate(float theta);

    void Translate(float x, float y);
    void Translate(Vec2 translation);

    void Apply(Vec2 &target);

    Matrix3 Inverse(void);

    friend std::ostream &operator<<(std::ostream &os, const Matrix3 &input);

    /******* Public Data *******/
    union
    {
      struct
      {
        float m00, m01, m02,
          m10, m11, m12,
          m20, m21, m22;
      };
      float m[3][3];
      float v[9];
    };
  };
}
