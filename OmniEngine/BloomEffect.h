/******************************************************************************
Filename: BloomEffect.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#include "stdafx.h"
#include "Component.h"
#include "Image.h"

namespace Omni
{
  class BloomEffect : public SystemComponent<BloomEffect>
  {
    public:
      ~BloomEffect() { ScriptableDestroy(); }
      virtual void Initialize();
      virtual void Update(float dt);
      virtual void Destroy();

      static void Register();
      static void v8_Register();

      Property::Float p_Threshold = 1.0;

  };

} //Namespace Omni

