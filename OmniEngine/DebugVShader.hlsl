/******************************************************************************
Filename: DebugVShader.hlsl

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// Author : Juli Gregg
// Purpose: Debug Rendering
// Date   : Fall 2014
// Type   : Vertex Shader

//
//Function Prototypes
//
float4x4 PickCamera(float index);
float4x4 CreateRotationMatrix(float theta);

//-----------------------------------//
// Cameras of Spaces from the Game   //
//-----------------------------------//
cbuffer ConstantBuffer
{
  //PerFrame Data
  float4x4 cd; //Always a default
  float4x4 c1; //Space Camera 1
  float4x4 c2; //Space Camera 2
  float4x4 c3; //Space Camera 3
  float4x4 c4; //Space Camera 4
  float4x4 c5; //Space Camera 5
  float4x4 c6; //Space Camera 6
  float4x4 c7; //Space Camera 7


}

//-----------------------------------//
// Struct Passed Into the Shader     //
//-----------------------------------//
struct vInput
{
  //Defaults
  float3 position : POSITION; //Unit Square (1/2 Size)
  float2 texcoord : TEXCOORD; //Starting UVs [0,1]

  //Per Object Data
  float4 color      : COLOR;        //Base Color of Sprite
  float2 trans      : TRANS;        //World Translation
  float2 scale      : SCALE;        //World Scale
  float  rot        : THETA;        //World Rotation
  uint   camera     : CINDEX;       //Space Index for Camera

};

//-----------------------------------//
// Struct Pass to the Pixel Shader   //
//-----------------------------------//
struct vOutput
{
  float4 position   : SV_POSITION;  //NDC Position
  float4 color      : COLOR;        //Base Color
  float2 texcoord   : TEXCOORD;     //Texture Coordinate
};

//-----------------------------------//
// Main Function of Vertex Shader    //
//-----------------------------------//
vOutput VShader(vInput input)
{
  //Struct to pass to Pixel Shader
  vOutput output;

  //Get objects rotation matrix
  float4x4 rot = CreateRotationMatrix(input.rot);

  //Transform Object into World Space
  output.position = float4(input.position.xy * input.scale.xy, 0, 1);
  output.position = mul(rot, output.position);
  output.position = float4(output.position.x + input.trans.x, output.position.y + input.trans.y, 0, 1);

  //Transform Object into Camera Space then NDC Space
  output.position = mul(PickCamera(input.camera + 1), output.position);

  //Set color
  output.color = input.color;

  //Set texture coordinates
  output.texcoord = input.texcoord;

  return output;
}

//-----------------------------------//
// Choose the Correct Camera for Obj //
//-----------------------------------//
float4x4 PickCamera(float index)
{
  //Notes: Case 0 is also Default
  switch (index)
  {
  case 1:
    return c1;  //Space 1
  case 2:
    return c2;  //Space 2
  case 3:
    return c3;  //Space 3
  case 4:
    return c4;  //Space 4
  case 5:
    return c5;  //Space 5
  case 6:
    return c6;  //Space 6
  case 7:
    return c7;  //Space 7
  default:
    return cd;  //Default Camera (Orthographic NDC)
  }

  return cd;
}

//-----------------------------------//
// Create a Rotation Matrix w/theta  //
//-----------------------------------//
float4x4 CreateRotationMatrix(float theta)
{
  //Builds a Rotation Matrix (around Z-Axis
  return float4x4(cos(theta), -sin(theta), 0, 0,
    sin(theta), cos(theta), 0, 0,
    0, 0, 1, 0,
    0, 0, 0, 1);
}


