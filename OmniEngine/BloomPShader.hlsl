/******************************************************************************
Filename: BloomPShader.hlsl

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//
// Pixel Shader for Creating Bloom Effect
//

//-----------------------------------//
// Textures Should Be Binded on CPU  //
//-----------------------------------//
Texture2D Texture0 : register(t0);

//-----------------------------------//
// Sample State Set on the CPU       //
//-----------------------------------//
SamplerState sampleState;

//-----------------------------------//
// Passed info from vertex shader    //
//-----------------------------------//
struct vOutput
{
  float4 position  : SV_POSITION;
  float2 texcoord  : TEXCOORD;
  float  threshold : THRESHOLD;
};

//-----------------------------------//
// Main Function of Pixel Shader     //
//-----------------------------------//
float4 PShader(vOutput input) : SV_Target0
{
  float4 color = Texture0.Sample(sampleState, input.texcoord);

  //Y = 0.2126 R + 0.7152 G + 0.0722 B (Luminensce Formula)
  float brightness = 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;

  if (brightness > input.threshold)
    return color;
  else
    return float4(0, 0, 0, 0);

}