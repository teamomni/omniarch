/******************************************************************************
Filename: HitBoxAnimation.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include <vector>
#include "HitBox.h"

namespace Omni
{
  class HitBoxAnimation
  {
  public:
    HitBoxAnimation();
    ~HitBoxAnimation();
    void AnimationStep();

  private:
    unsigned frames;
    float animationTimes[MaxAnimationFrames];
    std::vector<HitBox *> HitBoxes;
  };
}