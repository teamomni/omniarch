
Explanantion of Shaders

BasicVShader
-Takes a position and texture coordinate
-Interpolates across texture coordinates
-Send it out the Pixel Shader

BasicPShader
-Takes info from BasicVShader(pos, texCoord)
-Samples this data from Texture0
-Texture0 is the ScreenTarget (rendertarget in texture form)
-Sends that to the back buffer


SpriteVShader
-Constant BUffer
	-Takes 8 Matrices that are Cameras
-Takes instance data struct
	-pos			f4
	-texcoord		f2
	-color			f4	
	-trans			f2
	-scale			f2
	-rot			f1
	-UVstart		f2
	-UVend			f2
	-cameraIndex	ui
-Model to World Space
-World to Camera Space
-Set Color and interpolate texture coordinates

SpritePShader
