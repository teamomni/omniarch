/******************************************************************************
Filename: GFXRenderer.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//Juli Gregg
// GFXWrapper.cpp


#include "stdafx.h"
#include "GFXRenderer.h"
#include "GFXHandle.h"
#include <iostream>

#define GFX_DEBUG
#define GFX_DEBUG_DRAW

namespace Omni
{
  namespace Graphics
  {

    //------------//
    // INITIALIZE //
    //------------//

    //------------------------------------------------------//
    // Returns true if device has already been initialized  //
    //------------------------------------------------------//
    bool GFXRenderer::IsInitialized(void) const
    {
      if (m_device)
        return true;
      return false;
    }

    //----------------------------------------------------------------//
    // Calls internal functions to setup basic systems for rendering  //
    //----------------------------------------------------------------//
    bool GFXRenderer::Initialized(WindowSystem& Window)
    {
      if (!m_device)
      {
        //Save the window size for when we make the buffer
        m_resX = Window.GetClientWidth();
        m_resY = Window.GetClientHeight();

        //For full screen switching
        m_nativeRes = Vec2((float)m_resX, (float)m_resY);
        m_resFullScreen = Window.GetFullScreenSize();

        //Save the window handle
        m_hwnd = Window.GetWindowHandle();

        //Save a pointer to the window system
        m_windowSystem = &Window;

        //Initialize Various Graphics Objects/Interfafces
        try
        {
          InitializeDeviceAndSwapChain();
        }
        catch (...)
        {
          return false;
        }
        InitializeBackBuffer();
        InitializeViewport();
        InitializeSamplerState();
        InitializeBlendModes();
      }
      else
        return true;

      if (!m_device)
        return false;

      return true;
    }

    //------------------------------------------------------//
    // Releases all the systems created by the renderer     //
    //------------------------------------------------------//
    void GFXRenderer::UnInitialize(void)
    {
      
      if (m_device)
      {
        //Force windowed mode
        ErrorCheck(m_swapChain->SetFullscreenState(FALSE, NULL), true, "FAILED TO LEAVE FULLSCREEN MODE"); //switch to windowed mode

        //
        // Release Any remaining assets
        //
        for (auto& vShader : m_vertexShaderRes)
        {
          if (vShader.inputLayout)
            vShader.inputLayout->Release();
          if (vShader.vShader)
            vShader.vShader->Release();
        }
        for (auto& pShader : m_pixelShaderRes)
          if (pShader)
            pShader->Release();
        for (auto& texture : m_textureRes)
          if (texture.sResourceView)
            texture.sResourceView->Release();
        for (auto& vBuffer : m_vertexBufferRes)
          if (vBuffer)
            vBuffer->Release();
        for (auto& iBuffer : m_indexBufferRes)
          if (iBuffer)
            iBuffer->Release();
        for (auto& cBuffer : m_constBufferRes)
          if (cBuffer)
            cBuffer->Release();
        for (auto& rTarget : m_renderTargetRes)
        {
          if (rTarget.sResourceView)
            rTarget.sResourceView->Release();
          if (rTarget.texture2D)
            rTarget.texture2D->Release();
          if (rTarget.renderTargetView)
            rTarget.renderTargetView->Release();
        }
        for (auto& rStateO : m_rasterizerStateObjRes)
          if (rStateO)
            rStateO->Release();
        for (auto& bState : m_blendStateRes)
          if (bState)
            bState->Release();
        

        //Clear All Containers
        m_vertexShaderRes.clear();
        m_pixelShaderRes.clear();
        m_textureRes.clear();
        m_vertexBufferRes.clear();
        m_indexBufferRes.clear();
        m_constBufferRes.clear();
        m_renderTargetRes.clear();
        m_rasterizerStateObjRes.clear();
        m_blendStateRes.clear();
        m_vacancies.clear();


        //
        // Release Remaining Resources
        //
        m_blendState->Release();
        m_sampleStates->Release();
        m_swapChain->Release();
        m_backBuffer->Release();
        m_deviceContext->Release();
        m_device->Release();

       
      }
    }

    //------------------------------------------------------//
    // Error Checking of HRESULTS with Assert/Messages      //
    //------------------------------------------------------//
    bool GFXRenderer::ErrorCheck(HRESULT hr, bool assert, std::string message)
    {
#if defined GFX_DEBUG

      if (FAILED(hr))
      {

        HRESULT err = m_device->GetDeviceRemovedReason();

        //Write Error Message to Console
        Console::WriteLine("##");
        Console::Write("## GRAPHICS ERROR: ", Console::Color::Red);
        Console::WriteLine(message, Console::Color::DarkWhite, Console::Color::Red);
        Console::WriteLine("##");
        
        //If we want to assert, push message to assert as well
        if (assert)
        {
          std::wstring mes(message.begin(), message.end());
            _ASSERT_EXPR(0, mes.c_str());
        }

        //We hit an error
        return true;
      }
#endif
      //No Error or GFX Debug not on
      return false;
    }

    //----------------------------------------------------------//
    // Error Overload assumes that an error has already occured //
    //----------------------------------------------------------//
    void GFXRenderer::ErrorCheck(std::string message, bool assert)
    {
#if defined GFX_DEBUG

      //Write Error Message to console
      Console::WriteLine("##");
      Console::Write("## GRAPHICS ERROR: ", Console::Color::Red);
      Console::WriteLine(message, Console::Color::DarkWhite, Console::Color::Red);
      Console::WriteLine("##");

      //Write Error Message to Assert if we want to assert
      if (assert)
      {
        std::wstring mes(message.begin(), message.end());
        _ASSERT_EXPR(0, mes.c_str());
      }
#endif
    }

    //------------//
    // DRAW CALLS //
    //------------//

    //------------------------------------------------------------------//
    // Draws one object based off of the vertices in the vertex buffer  //
    //------------------------------------------------------------------//
    void GFXRenderer::Draw(unsigned vertCount, unsigned vertStart)
    {
      //DirectX Function
      m_deviceContext->Draw(vertCount, vertStart);
    }

    //------------------------------------------------------------------------//
    // Draws one object based off indexing into vertices in the vertex buffer //
    //------------------------------------------------------------------------//
    void GFXRenderer::DrawIndexed(unsigned indexCount, unsigned indexStart, unsigned vertStart)
    {
      //DirectX Function
      m_deviceContext->DrawIndexed(indexCount, indexStart, vertStart);
    }

    //------------------------------------------------------------------------------//
    // Draws multiple objects with same vertices but with difference instanced data //
    //------------------------------------------------------------------------------//
    void GFXRenderer::DrawInstanced(unsigned vertCount, unsigned instCount, unsigned vertStart, unsigned instStart)
    {
      //DirectX Function
      m_deviceContext->DrawInstanced(vertCount, instCount, vertStart, instStart);
    }

    //--------------------------------------------------------------------------------//
    // Draws multiple objects by indexing into vertex buffer and using instance data  //
    //--------------------------------------------------------------------------------//
    void GFXRenderer::DrawIndexedInstanced(unsigned indexCountPerInst, unsigned instCount, unsigned indexStart, unsigned vertStart, unsigned instStart)
    {
      //DirectX Function
      m_deviceContext->DrawIndexedInstanced(indexCountPerInst, instCount, indexStart, vertStart, instStart);
    }

    //---------------------------------------------//
    // switch the back buffer and the front buffer //
    //---------------------------------------------//
    void GFXRenderer::Present(void)
    {
      if (m_fullscreen)
        m_swapChain->Present(1, 0);
      else
        m_swapChain->Present(0, 0);
    }



    //--------------//
    // CREATE CALLS //
    //--------------//

    //----------------------------------------------------------------------------------------------------//
    // Creates a vertex shader based off hlsl file, and sets up the input layout the shader is expecting  //
    //----------------------------------------------------------------------------------------------------//
    bool GFXRenderer::CreateVertexShader(Handle& handle, const std::string& filename, InputLayout& iLayout, const std::string& entryFunc)
    {
      ID3D10Blob   *vertexShader  = nullptr;
      ID3D10Blob   *errorBlob     = nullptr;
      VertexShader  vertShader;

      //----------------------------//
      // COMPILE VERTEX SHADER FILE //
      //----------------------------//

      unsigned compileFlags = NULL;

#if defined GFX_DEBUG

      //These are set so graphics debugger can step through shaders
      compileFlags = D3DCOMPILE_SKIP_OPTIMIZATION | D3DCOMPILE_DEBUG;

#endif

      //Convert to a long string
      std::wstring fnTemp = std::wstring(filename.begin(), filename.end());
      LPCWSTR fileName = fnTemp.c_str();

      //Compile the Shader File
      HRESULT errorHR = D3DX11CompileFromFile(fileName, 
                                              0, 
                                              0, 
                                              entryFunc.c_str(), 
                                              "vs_4_0", 
                                              compileFlags, 
                                              0, 
                                              0, 
                                              &vertexShader, 
                                              &errorBlob, 
                                              0);

      ErrorCheck(errorHR, false, "FAILED TO COMPILE VERTEX SHADER :: CHECK VSOUT.log");

#if defined(_EDITOR) || defined(_DEBUG) || defined(_RELEASE)
      //If there was an error writes to log text file
      if (errorBlob)
      {
        FILE *fp;
        fopen_s(&fp, "VSOUT.log", "w");
        fprintf_s(fp, "Vertex Shader Compile Error: %s\n", static_cast<char*>(errorBlob->GetBufferPointer()));
        fclose(fp);
        return false;
      }
#endif
      if (errorBlob)
        errorBlob->Release();
      

      //Create Vertex Shader Object
      errorHR = m_device->CreateVertexShader(vertexShader->GetBufferPointer(), 
                                             vertexShader->GetBufferSize(), 
                                             NULL, 
                                             &vertShader.vShader);

      if (ErrorCheck(errorHR, true, "FAILED TO CREATE VERTEX SHADER"))
        return false;

      //----------------------------//
      //    CREATE INPUT LAYOUT     //
      //----------------------------//

      std::vector<D3D11_INPUT_ELEMENT_DESC> inputElementDesc;

      //These are always set because of how our vertex buffers are set up
      inputElementDesc.push_back({ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 });
      inputElementDesc.push_back({ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 });

      //Add additional elements based on Input Layout Passed (this is most likely instance data)
      for (auto iElement = iLayout.begin(); iElement != iLayout.end(); iElement++)
      {
        inputElementDesc.push_back({ iElement->semantic.c_str(),
                                     iElement->structureSlot, 
                                     DXGI_FORMAT(iElement->format), 
                                     1, 
                                     D3D11_APPEND_ALIGNED_ELEMENT, 
                                     iElement->instance ? D3D11_INPUT_PER_INSTANCE_DATA : D3D11_INPUT_PER_VERTEX_DATA,
                                     iElement->instance });
      }

      //Create the Input Layout Object
      errorHR = m_device->CreateInputLayout(inputElementDesc.data(), 
                                            inputElementDesc.size(), 
                                            vertexShader->GetBufferPointer(), 
                                            vertexShader->GetBufferSize(), 
                                            &vertShader.inputLayout);

      if (ErrorCheck(errorHR, true, "FAILED TO CREATE INPUT LAYOUT"))
        return false;

      //Try and find an empty slot so we don't expand the vector indefinitely
      if (FindEmptySlot(handle, OBJT::VerterxShader))
        m_vertexShaderRes[handle.index] = vertShader;
      else
      {
        m_vertexShaderRes.push_back(vertShader);
        
        //Set Handle
        handle = Handle(OBJT::VerterxShader, m_vertexShaderRes.size() - 1);
      }

      return true;
    }


    //---------------------------------------------//
    // Creates a pixel shader based off hlsl file  //
    //---------------------------------------------//
    bool GFXRenderer::CreatePixelShader(Handle& handle, const std::string& filename, const std::string& entryFunc)
    {
      ID3D10Blob *pixelShader       = nullptr;
      ID3D10Blob *errorBlob         = nullptr;
      ID3D11PixelShader *pixShader  = nullptr;

      //----------------------------//
      // COMPILE PIXEL SHADER FILE  //
      //----------------------------//

      unsigned compileFlags = NULL;

#if defined GFX_DEBUG

      //For being able to step throuhg shader in debugger
      compileFlags = D3DCOMPILE_SKIP_OPTIMIZATION | D3DCOMPILE_DEBUG;

#endif

      //Convert to a long string
      std::wstring fnTemp = std::wstring(filename.begin(), filename.end());
      LPCWSTR fileName = fnTemp.c_str();

      HRESULT errorHR = D3DX11CompileFromFile(fileName, 
                                              0, 
                                              0, 
                                              entryFunc.c_str(), 
                                              "ps_4_0", 
                                              compileFlags, 
                                              0, 
                                              0, 
                                              &pixelShader, 
                                              &errorBlob, 
                                              0);

      ErrorCheck(errorHR, false, "FAILED TO COMPILE PIXEL SHADER :: CHECK PSOUT.log");

#if defined(_EDITOR) || defined(_DEBUG) || defined(_RELEASE)
      //If there was an error writes to log text file
      if (errorBlob)
      {
        FILE *fp;
        fopen_s(&fp, "PSOUT.log", "w");
        fprintf_s(fp, "Pixel Shader Compile Error: %s\n", static_cast<char*>(errorBlob->GetBufferPointer()));
        fclose(fp);
        return false;
      }
#endif
      if (errorBlob)
        errorBlob->Release();

      //Create the Pixel Shader Object
      errorHR = m_device->CreatePixelShader(pixelShader->GetBufferPointer(), pixelShader->GetBufferSize(), NULL, &pixShader);

      ErrorCheck(errorHR, true, "FAILED TO CREATE PIXEL SHADER");

      //Try to find an empty slot in the resource vector
      if (FindEmptySlot(handle, OBJT::PixelShader))
        m_pixelShaderRes[handle.index] = pixShader;
      else
      {
        m_pixelShaderRes.push_back(pixShader);

        //Set Handle
        handle = Handle(OBJT::PixelShader, m_pixelShaderRes.size() - 1);
      }

      return true;
    }

    //--------------------------------//
    // Creates a texture from a file  //
    //--------------------------------//
    bool GFXRenderer::CreateTexture(Handle& handle, const std::string& filename, std::mutex* mtx)
    {
      Texture pTexture;

      //Convert to a long string
      std::wstring fnTemp = std::wstring(filename.begin(), filename.end());
      LPCWSTR fileName = fnTemp.c_str();

      //Create a Texture Object
      HRESULT errorHR = D3DX11CreateShaderResourceViewFromFile(m_device, 
                                                               fileName, 
                                                               NULL, 
                                                               NULL, 
                                                               &pTexture.sResourceView, 
                                                               NULL);

      std::string message("FAILED TO LOAD TEXTURE: ");
      message += filename;
      if (ErrorCheck(errorHR, false, message))
        return false;

      mtx->lock();
      if (FindEmptySlot(handle, OBJT::Texture))
        m_textureRes[handle.index] = pTexture;
      else
      {
        m_textureRes.push_back(pTexture);

        //Set Handle
        handle = Handle(OBJT::Texture, m_textureRes.size() - 1);
      }
      mtx->unlock();


      return true;
    }

    //-----------------------------------------------------//
    // Creates a vertex buffer filled with a basic square  //
    //-----------------------------------------------------//
    bool GFXRenderer::CreateVertexBuffer(Handle& handle)
    {
      ID3D11Buffer *vertexBuffer = nullptr;

      //Create the 2D Square that we need
      Vertex quadFill[] =
      {
        { -0.5f,  0.5f, 0.0f, 0.0f, 0.0f }, //Top Left
        {  0.5f,  0.5f, 0.0f, 1.0f, 0.0f }, //Top Right
        { -0.5f, -0.5f, 0.0f, 0.0f, 1.0f }, //Bottom Left
        {  0.5f, -0.5f, 0.0f, 1.0f, 1.0f }  //Bottom Right
#if defined GFX_DEBUG_DRAW
        //For debug lines
        , { -0.5f, 0.0f, 0.0f, 0.0f, 0.0f }, //Center
        {    0.5f, 0.0f, 0.0f, 1.0f, 0.0f }  //Right
#endif
      };

      D3D11_BUFFER_DESC vertexBufferDesc;

      ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));

      vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;

#if defined GFX_DEBUG_DRAW
      //Since we have 6 vertecies if we are doing debug draw
      vertexBufferDesc.ByteWidth = sizeof(Vertex) * 6;
#else
      //Only 4 vertecies if we are not doing debug draw
      vertexBufferDesc.ByteWidth = sizeof(Vertex) * 4;
#endif

      //Set Flags
      vertexBufferDesc.BindFlags      = D3D11_BIND_VERTEX_BUFFER;
      vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

      
      D3D11_SUBRESOURCE_DATA subResource;

      //Bind the data
      subResource.pSysMem = quadFill;

      //Create the Buffer
      HRESULT errorHR = m_device->CreateBuffer(&vertexBufferDesc, 
                                               &subResource, 
                                               &vertexBuffer);

      if (ErrorCheck(errorHR, true, "FAILED TO CREATE VERTEX BUFFER"))
        return false;

      if (FindEmptySlot(handle, OBJT::VertexBuffer))
        m_vertexBufferRes[handle.index] = vertexBuffer;
      else
      {
        m_vertexBufferRes.push_back(vertexBuffer);

        //Set Handle
        handle = Handle(OBJT::VertexBuffer, m_vertexBufferRes.size() - 1);
      }

      return true;
    }

    //---------------------------------------------//
    // Creates an index buffer for a basic square  //
    //---------------------------------------------//
    bool GFXRenderer::CreateIndexBuffer(Handle& handle)
    {
      //Sets up the 3 endpoint of the 2 triangle that make up a square
      DWORD indexes[] =
      {
        0, 1, 2,    // Triangle 1 (TopLeft     TopRight   BottomLeft)
        2, 1, 3,    // Triangle 2 (BottomLeft  TopRight   BottomRight) 
      };

      return CreateIndexBuffer(handle, indexes, 6);
    }

    //----------------------------------------------------------//
    // Creates a custom index buffer based on indexes passed in //
    //----------------------------------------------------------//
    bool GFXRenderer::CreateIndexBuffer(Handle& handle, DWORD* indexes, unsigned size)
    {
      ID3D11Buffer *indexBuffer = nullptr;

      D3D11_BUFFER_DESC indexBufferDesc;

      ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));

      indexBufferDesc.Usage           = D3D11_USAGE_DYNAMIC;
      indexBufferDesc.ByteWidth       = sizeof(DWORD) * size;
      indexBufferDesc.BindFlags       = D3D11_BIND_INDEX_BUFFER;
      indexBufferDesc.CPUAccessFlags  = D3D11_CPU_ACCESS_WRITE;
      indexBufferDesc.MiscFlags       = 0;

      D3D11_SUBRESOURCE_DATA subResource;

      //Bind the data
      subResource.pSysMem = indexes;

      //Create the Buffer
      HRESULT errorHR = m_device->CreateBuffer(&indexBufferDesc, &subResource, &indexBuffer);

      if (ErrorCheck(errorHR, true, "FAILED TO CREATE INDEX BUFFER"))
        return false;

      if (FindEmptySlot(handle, OBJT::IndexBuffer))
        m_indexBufferRes[handle.index] = indexBuffer;
      else
      {
        m_indexBufferRes.push_back(indexBuffer);

        //Set Handle
        handle = Handle(OBJT::IndexBuffer, m_indexBufferRes.size() - 1);
      }

      return true;
    }

    //--------------------------------------------//
    // Creates a constant buffer with custom size //
    //--------------------------------------------//
    bool GFXRenderer::CreateConstantBuffer(Handle& handle, unsigned size)
    {
      ID3D11Buffer *constantBuffer = nullptr;
      D3D11_BUFFER_DESC constantBufferDesc;

      ZeroMemory(&constantBufferDesc, sizeof(constantBufferDesc));

      constantBufferDesc.Usage      = D3D11_USAGE_DEFAULT;
      constantBufferDesc.ByteWidth  = size;
      constantBufferDesc.BindFlags  = D3D11_BIND_CONSTANT_BUFFER;

      //Create Buffer
      HRESULT errorHR = m_device->CreateBuffer(&constantBufferDesc, NULL, &constantBuffer);

      if (ErrorCheck(errorHR, true, "FAILED TO CREATE CONSTANT BUFFER"))
        return false;

      if (FindEmptySlot(handle, OBJT::ConstantBuffer))
        m_constBufferRes[handle.index] = constantBuffer;
      else
      {
        m_constBufferRes.push_back(constantBuffer);

        //Set Handle
        handle = Handle(OBJT::ConstantBuffer, m_constBufferRes.size() - 1);
      }

      return true;
    }

    //-------------------------//
    // Creates a Render Target //
    //-------------------------//
    bool GFXRenderer::CreateRenderTarget(Handle& handle, /*Render Targer mode*/ /*const Format*/ const float downsamplePercentager /*dimensions*/)
    {
      RenderTarget rt;
      rt.downssamplePercentage = downsamplePercentager;

      //Set up texture first
      D3D11_TEXTURE2D_DESC textureDesc;

      ZeroMemory(&textureDesc, sizeof(textureDesc));

      //TODO : Make it so you can use custom dimensions
      textureDesc.Width             = m_resX;
      textureDesc.Height            = m_resY;
      textureDesc.Format            = DXGI_FORMAT_R8G8B8A8_UNORM;
      textureDesc.MipLevels         = 1;
      textureDesc.ArraySize         = 1;
      textureDesc.SampleDesc.Count  = 1;
      textureDesc.Usage             = D3D11_USAGE_DEFAULT;
      textureDesc.BindFlags         = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
      textureDesc.CPUAccessFlags    = 0;
      textureDesc.MiscFlags         = 0;

      //Create the Texture2D Object
      HRESULT errorHR = m_device->CreateTexture2D(&textureDesc, NULL, &rt.texture2D);

      if (ErrorCheck(errorHR, true, "FAILED TO GET ADDRESS OF RT TEXTURE2D"))
        return false;

      //Set up the render target view
      D3D11_RENDER_TARGET_VIEW_DESC rtViewDesc;

      rtViewDesc.Format               = textureDesc.Format;
      rtViewDesc.ViewDimension        = D3D11_RTV_DIMENSION_TEXTURE2D;
      rtViewDesc.Texture2D.MipSlice   = 0;

      //Create Render Target Object
      errorHR = m_device->CreateRenderTargetView(rt.texture2D, &rtViewDesc, &rt.renderTargetView);

      if (ErrorCheck(errorHR, true, "FAILED TO GET ADDRESS OF RENDER TARGET VIEW"))
        return false;

      //Set up Shader Resource View
      D3D11_SHADER_RESOURCE_VIEW_DESC sResourceViewDesc;

      sResourceViewDesc.Format                      = textureDesc.Format;
      sResourceViewDesc.ViewDimension               = D3D11_SRV_DIMENSION_TEXTURE2D;
      sResourceViewDesc.Texture2D.MostDetailedMip   = 0;
      sResourceViewDesc.Texture2D.MipLevels         = 1;

      //Create Texture that can be binded
      errorHR = m_device->CreateShaderResourceView(rt.texture2D, &sResourceViewDesc, &rt.sResourceView);

      if (ErrorCheck(errorHR, true, "FAILED TO GET ADDRESS OF RENDER TARGET SHADER RESOURCE"))
        return false;

      if (FindEmptySlot(handle, OBJT::RenderTarget))
        m_renderTargetRes[handle.index] = rt;
      else
      {
        m_renderTargetRes.push_back(rt);

        //Set Handle
        handle = Handle(OBJT::RenderTarget, m_renderTargetRes.size() - 1);
      }

      return true;
    }

    //------------------------------------------------------------//
    // Creates an Instance Buffer (in the form of a vertex buffer //
    //------------------------------------------------------------//
    bool GFXRenderer::CreateInstanceBuffer(Handle& handle, void* data, size_t size)
    {
      ID3D11Buffer *instanceBuffer = nullptr;
      D3D11_BUFFER_DESC instanceBufferDesc;

      ZeroMemory(&instanceBufferDesc, sizeof(instanceBufferDesc));

      instanceBufferDesc.Usage            = D3D11_USAGE_DYNAMIC;
      instanceBufferDesc.ByteWidth        = size;
      instanceBufferDesc.BindFlags        = D3D11_BIND_VERTEX_BUFFER;
      instanceBufferDesc.CPUAccessFlags   = D3D11_CPU_ACCESS_WRITE;

      D3D11_SUBRESOURCE_DATA subResource;

      //Bind all the Instance Data 
      //Using a void ptr makes it so we can bind different structs
      subResource.pSysMem = data;

      //Create Buffer
      HRESULT errorHR = m_device->CreateBuffer(&instanceBufferDesc, &subResource, &instanceBuffer);

      if (ErrorCheck(errorHR, true, "FAILED TO CREATE INSTANCE BUFFER"))
        return false;

      if (FindEmptySlot(handle, OBJT::VertexBuffer))
        m_vertexBufferRes[handle.index] = instanceBuffer;
      else
      {
        //Treated as a Vertex Buffer since you bind the instance buffer with the vertex buffer
        m_vertexBufferRes.push_back(instanceBuffer);

        //Set Handle
        handle = Handle(OBJT::VertexBuffer, m_vertexBufferRes.size() - 1);
      }

      return true;
    }

    //-------------------------------------------//
    // Creates a Rasterizer Object for Wireframe //
    //-------------------------------------------//
    bool GFXRenderer::CreateRasterizerStateObject(Handle& handle, bool WireFrame, bool Antialiased, bool Multisampled)
    {
      ID3D11RasterizerState *rasterizer = nullptr;
      D3D11_RASTERIZER_DESC rasterizerDesc;

      if (WireFrame)
        rasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;
      else
        rasterizerDesc.FillMode = D3D11_FILL_SOLID;

      //Default Settings
      rasterizerDesc.CullMode               = D3D11_CULL_BACK;
      rasterizerDesc.FrontCounterClockwise  = FALSE;
      rasterizerDesc.DepthClipEnable        = TRUE;
      rasterizerDesc.ScissorEnable          = FALSE;
      rasterizerDesc.DepthBias              = 0;
      rasterizerDesc.DepthBiasClamp         = 0.0f;
      rasterizerDesc.SlopeScaledDepthBias   = 0.0f;

      //Antialised/MultiSample
      if (Antialiased)
        rasterizerDesc.AntialiasedLineEnable  = TRUE;
      else
        rasterizerDesc.AntialiasedLineEnable  = FALSE;
      if (Multisampled)
        rasterizerDesc.MultisampleEnable      = TRUE;
      else
        rasterizerDesc.MultisampleEnable      = FALSE;

      //Create the Rasterizer Object
      HRESULT errorHR = m_device->CreateRasterizerState(&rasterizerDesc, &rasterizer);

      if (ErrorCheck(errorHR, true, "FAILED TO CREATE RASTERIZER STATE"))
        return false;

      if (FindEmptySlot(handle, OBJT::RasterizerState))
        m_rasterizerStateObjRes[handle.index] = rasterizer;
      else
      {
        m_rasterizerStateObjRes.push_back(rasterizer);

        //Set Handle
        handle = Handle(OBJT::RasterizerState, m_rasterizerStateObjRes.size() - 1);
      }

      return true;
    }

    //----------------------------------//
    // Creates a series of blend states //
    //----------------------------------//
    void GFXRenderer::CreateBlendMode(Handle& handle, BlendModes* mode)
    {
      ID3D11BlendState* blendMode = nullptr;
      D3D11_BLEND_DESC blendDesc;

      for (unsigned i = 0; i < 8; i++)
      {

        blendDesc.RenderTarget[i].BlendEnable           = TRUE;
        blendDesc.RenderTarget[i].BlendOp               = D3D11_BLEND_OP_ADD;

        if (mode[i] == BlendModes::Additive)
        {
          blendDesc.RenderTarget[i].SrcBlend            = D3D11_BLEND_SRC_ALPHA;
          blendDesc.RenderTarget[i].DestBlend           = D3D11_BLEND_ONE;
          blendDesc.RenderTarget[i].DestBlendAlpha      = D3D11_BLEND_ONE;
          blendDesc.RenderTarget[i].SrcBlendAlpha       = D3D11_BLEND_ONE;

        }
        else //Alpha Blending
        {
          blendDesc.RenderTarget[i].SrcBlend            = D3D11_BLEND_SRC_ALPHA;
          blendDesc.RenderTarget[i].DestBlend           = D3D11_BLEND_INV_SRC_ALPHA;
          blendDesc.RenderTarget[i].DestBlendAlpha      = D3D11_BLEND_ONE;
          //blendDesc.RenderTarget[i].SrcBlendAlpha = D3D11_BLEND_ZERO;
          blendDesc.RenderTarget[i].SrcBlendAlpha = D3D11_BLEND_ONE;

        }

        blendDesc.RenderTarget[i].BlendOpAlpha          = D3D11_BLEND_OP_ADD;
        blendDesc.RenderTarget[i].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
      }

      blendDesc.IndependentBlendEnable                  = TRUE;
      blendDesc.AlphaToCoverageEnable                   = FALSE;

      //Create the Blend States
      HRESULT errorHR = m_device->CreateBlendState(&blendDesc, &blendMode);

      ErrorCheck(errorHR, true, "FAILED TO CREATE BLEND STATE");

      if (FindEmptySlot(handle, OBJT::BlendMode))
        m_blendStateRes[handle.index] = blendMode;
      else
      {
        m_blendStateRes.push_back(blendMode);

        //Set Handle
        handle = Handle(OBJT::BlendMode, m_blendStateRes.size() - 1);
      }
    }

    //---------------------------------//
    // Creates a series of blend state //
    //---------------------------------//
    void GFXRenderer::CreateBlendMode(Handle& handle, BlendModes mode)
    {
      ID3D11BlendState* blendMode;
      D3D11_BLEND_DESC blendDesc;

      for (unsigned i = 0; i < 8; ++i)
      {
        blendDesc.RenderTarget[i].BlendEnable           = TRUE;
        blendDesc.RenderTarget[i].BlendOp               = D3D11_BLEND_OP_ADD;

        if (mode == BlendModes::Additive)
        {
          blendDesc.RenderTarget[i].SrcBlend            = D3D11_BLEND_ONE;
          blendDesc.RenderTarget[i].DestBlend           = D3D11_BLEND_ONE;
          blendDesc.RenderTarget[i].DestBlendAlpha      = D3D11_BLEND_ONE;
          blendDesc.RenderTarget[i].SrcBlendAlpha       = D3D11_BLEND_ONE;
        }
        else //Alpha Blending
        {
          blendDesc.RenderTarget[i].SrcBlend            = D3D11_BLEND_SRC_ALPHA;
          blendDesc.RenderTarget[i].DestBlend           = D3D11_BLEND_INV_SRC_ALPHA;
          blendDesc.RenderTarget[i].DestBlendAlpha      = D3D11_BLEND_ONE;
          //blendDesc.RenderTarget[i].SrcBlendAlpha = D3D11_BLEND_INV_DEST_ALPHA;
          //blendDesc.RenderTarget[i].SrcBlendAlpha = D3D11_BLEND_ZERO;
          blendDesc.RenderTarget[i].SrcBlendAlpha = D3D11_BLEND_ONE;

        }

        blendDesc.RenderTarget[i].BlendOpAlpha          = D3D11_BLEND_OP_ADD;
        blendDesc.RenderTarget[i].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
      }
      
      blendDesc.IndependentBlendEnable                  = FALSE;
      blendDesc.AlphaToCoverageEnable                   = FALSE;

      //Create Blend State
      HRESULT errorHR = m_device->CreateBlendState(&blendDesc, &blendMode);

      ErrorCheck(errorHR, true, "FAILED TO CREATE BLEND STATE");

      if (FindEmptySlot(handle, OBJT::BlendMode))
        m_blendStateRes[handle.index] = blendMode;
      else
      {
        m_blendStateRes.push_back(blendMode);

        //Set Handle
        handle = Handle(OBJT::BlendMode, m_blendStateRes.size() - 1);
      }
    }

    //--------------//
    // BIND CALLS   //
    //--------------//

    //------------------------//
    // Bind the Vertex Shader //
    //------------------------//
    void GFXRenderer::BindVertexShader(const Handle& vsHandle)
    {
      if (vsHandle.type != OBJT::VerterxShader)
        ErrorCheck("INCORRECT TYPE: VS");

      //Bind pointer
      m_deviceContext->IASetInputLayout(m_vertexShaderRes[vsHandle.index].inputLayout);
      m_deviceContext->VSSetShader(m_vertexShaderRes[vsHandle.index].vShader, 0, 0);
    }

    //-----------------------//
    // Bind the Pixel Shader //
    //-----------------------//
    void GFXRenderer::BindPixelShader(const Handle& psHandle)
    {
      if (psHandle.type != OBJT::PixelShader)
        ErrorCheck("INCORRECT TYPE: PS");

      //Bind Pointer
      m_deviceContext->PSSetShader(m_pixelShaderRes[psHandle.index], 0, 0);
    }

    //------------------------//
    // Bind Texture (single)  //
    //------------------------//
    void GFXRenderer::BindTexture(unsigned slot, const Handle& texHandle)
    {
      if (texHandle.type != OBJT::Texture)
        ErrorCheck("INCORRECT TYPE: TEXTURE");

      //Bind pointer
      m_deviceContext->PSSetShaderResources(slot, 1, &m_textureRes[texHandle.index].sResourceView);
    }

    //--------------------------//
    // Bind Texture (multiple)  //
    //--------------------------//
    void GFXRenderer::BindTextures(unsigned count, const Handle texHandles[], unsigned startSlot)
    {
      std::vector<ID3D11ShaderResourceView*> textures;

      //Bind several pointers
      for (unsigned i = 0; i < count; i++)
      {
        if (texHandles[i].type != OBJT::Texture)
          ErrorCheck("INCORRECT TYPE: TEXTURE");

        textures.push_back(m_textureRes[texHandles[i].index].sResourceView);
      }

      //Bind data
      m_deviceContext->PSSetShaderResources(startSlot, count, textures.data());
    }

    //-------------------------------------//
    // UnBind a Texture at a Specific Slot //
    //-------------------------------------//
    void GFXRenderer::UnBindTexture(unsigned slot)
    {
      m_deviceContext->PSSetShaderResources(slot, 0, NULL);
    }


    //---------------------------------//
    // Bind Render Target as a Texture //
    //---------------------------------//
    void GFXRenderer::BindRenderTargetAsTexture(Handle& rTarget, unsigned slot)
    {
      if (rTarget.type != OBJT::RenderTarget)
        ErrorCheck("INCORRECT TYPE: RENDER TARGET AS TEXTURE");

      //Bind pointer
      m_deviceContext->PSSetShaderResources(slot, 1, &m_renderTargetRes[rTarget.index].sResourceView);
    }

    //-------------------------------------------//
    // Bind Vertex Buffer (single) - no instance //
    //-------------------------------------------//
    void GFXRenderer::BindVertexBuffer(const Handle& vbHandle, size_t stride, unsigned slot, size_t offset)
    {
      if (vbHandle.type != OBJT::VertexBuffer)
        ErrorCheck("INCORRECT TYPE: VB");

      //Bind Pointer
      m_deviceContext->IASetVertexBuffers(slot, 1, &m_vertexBufferRes[vbHandle.index], &stride, &offset);
    }


    //----------------------------------------------//
    // Bind Vertex Buffers (multiple) - no instance //
    //----------------------------------------------//
    void GFXRenderer::BindVertexBuffers(unsigned count, const Handle vertexBuffers[], size_t strides[], size_t offsets[])
    {
      //Bind Pointers
      for (unsigned i = 0; i < count; i++)
      {
        if (vertexBuffers[i].type != OBJT::VertexBuffer)
          ErrorCheck("INCORRECT TYPE: VB");

        m_deviceContext->IASetVertexBuffers(i, 1, &m_vertexBufferRes[vertexBuffers[i].index], &strides[i], &offsets[i]);
      }
    }

    //-----------------------------//
    // Bind Index Buffer (single) //
    //-----------------------------//
    void GFXRenderer::BindIndexBuffer(const Handle& ibHandle)
    {
      if (ibHandle.type != OBJT::IndexBuffer)
        ErrorCheck("INCORRECT TYPE: IB");

      m_deviceContext->IASetIndexBuffer(m_indexBufferRes[ibHandle.index], DXGI_FORMAT_R32_UINT, 0);
    }

    //-------------------------------//
    // Bind Constant Buffer (single) //
    //-------------------------------//
    void GFXRenderer::BindConstantBuffer(unsigned slot, const Handle& cbHandle)
    {
      if (cbHandle.type != OBJT::ConstantBuffer)
        ErrorCheck("INCORRECT TYPE: CB");

      m_deviceContext->VSSetConstantBuffers(slot, 1, &m_constBufferRes[cbHandle.index]);
    }

    //--------------------------------//
    // Bind Back Buffer Render Target //
    //--------------------------------//
    void GFXRenderer::BindBackBuffer(void)
    {
      m_deviceContext->OMSetRenderTargets(1, &m_backBuffer, NULL);
    }

    //-------------------------------//
    // Bind Render Target   (single) //
    //-------------------------------//
    void GFXRenderer::BindRenderTarget(const Handle& rtHandle)
    {
      if (rtHandle.type != OBJT::RenderTarget)
        ErrorCheck("INCORRECT TYPE: RT");

      m_deviceContext->OMSetRenderTargets(1, &m_renderTargetRes[rtHandle.index].renderTargetView, NULL);
    }

    //-------------------------------//
    // Bind Render Target (multiple) //
    //-------------------------------//
    void GFXRenderer::BindRenderTargets(unsigned count, const Handle* rtHandles)
    {
      std::vector<ID3D11RenderTargetView*> rTargets;
      for (unsigned i = 0; i < count; i++)
      {
        if (rtHandles[i].type != OBJT::RenderTarget)
          ErrorCheck("INCORRECT TYPE: RT");

        rTargets.push_back(m_renderTargetRes[rtHandles[i].index].renderTargetView);
      }
      
      m_deviceContext->OMSetRenderTargets(count, &rTargets[0], NULL);
	  rTargets.clear();
    }

    //-------------------------------//
    // Unbind Curren Render Targets  //
    //-------------------------------//
    void GFXRenderer::UnBindRenderTargets(void)
    {
      m_deviceContext->OMSetRenderTargets(0, 0, 0);
    }

    //---------------------------------//
    // Bind Rasterizer Object (single) //
    //---------------------------------//
    void GFXRenderer::BindRasterizerStateObject(Handle& rhandle)
    {
      if (rhandle.type != OBJT::RasterizerState)
        ErrorCheck("INCORRECT TYPE: RASTERIZER");

      m_deviceContext->RSSetState(m_rasterizerStateObjRes[rhandle.index]);
    }


    //----------//
    // SETTERS  //
    //----------//

    //----------------------//
    // Set Background Color //
    //----------------------//
    void GFXRenderer::SetClearColor(const float r, const float g, const float b, const float a)
    {
      m_clearColor = D3DXCOLOR(r, g, b, a);
    }

    //----------------------//
    // Set Background Color //
    //----------------------//
    void GFXRenderer::SetClearColor(const D3DXCOLOR& color)
    {
      m_clearColor = color;
    }

    //----------------//
    // Set Fullscreen //
    //----------------//
    void GFXRenderer::SetFullScreen(const bool fullscreen)
    {
      if (fullscreen == m_fullscreen)
      {
        //Say that were no longer changing into fullscreen mode
        m_changingFSMode = false;
        return;
      }

      if (m_swapChain && !m_changingFSMode)
      {
        m_fullscreen      = fullscreen;
        m_changingFSMode  = true;

        if (!m_fullscreen)
          m_swapChain->SetFullscreenState(false, NULL);

        ID3D11RenderTargetView* nullRT = nullptr;
        m_deviceContext->OMSetRenderTargets(1, &nullRT, NULL);

        //Release the back buffer
        m_backBuffer->Release();

        // Preserve the existing buffer count and format.
        // Automatically choose the width and height to match the client rect for HWNDs.
        HRESULT errorHR;
        if (fullscreen)
          errorHR = m_swapChain->ResizeBuffers(0, 
                                              (unsigned)m_resFullScreen.x, 
                                              (unsigned)m_resFullScreen.y, 
                                              DXGI_FORMAT_R8G8B8A8_UNORM,
                                              DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH);
        else
          errorHR = m_swapChain->ResizeBuffers(0, 
                                              (unsigned)m_nativeRes.x, 
                                              (unsigned)m_nativeRes.y, 
                                              DXGI_FORMAT_R8G8B8A8_UNORM,
                                              DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH);

        ErrorCheck(errorHR, true, "FAILED TO SWITCH FULL SCREEN MODE: (RESIZE BUFFER)");

        ID3D11Texture2D* backBuffer = nullptr;

        //Get the newly resized back buffer
        errorHR = m_swapChain->GetBuffer(0, 
                                        __uuidof(ID3D11Texture2D), 
                                        (void**)&backBuffer);

        ErrorCheck(errorHR, true, "FAILED TO SWITCH FULL SCREEN MODE: (LOCAL BACKBUFFER)");

        //Create a Render Target Object
        errorHR = m_device->CreateRenderTargetView(backBuffer, 
                                                   NULL, 
                                                   &m_backBuffer);

        ErrorCheck(errorHR, true, "FAILED TO SWITCH FULL SCREEN MODE: (BACKBUFFER)");

        //Set the back buffer as the current render target for good measure
        m_deviceContext->OMSetRenderTargets(1, &m_backBuffer, NULL);

        // Set the viewport
        D3D11_VIEWPORT viewport;

        ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

        viewport.TopLeftX = 0;
        viewport.TopLeftY = 0;

        if (m_fullscreen)
        {
          viewport.Width  = (float)m_resFullScreen.x;
          viewport.Height = (float)m_resFullScreen.y;

          //Set the appropriate variables
          m_windowSystem->SetClientSize((int)m_resFullScreen.x, (int)m_resFullScreen.y);
          m_resX = (int)m_resFullScreen.x;
          m_resY = (int)m_resFullScreen.y;
        }
        else //Windowed mode
        {
          viewport.Width  = (float)m_nativeRes.x;
          viewport.Height = (float)m_nativeRes.y;

          //Set the appropriate variables
          m_windowSystem->SetClientSize((int)m_nativeRes.x, (int)m_nativeRes.y);
          m_resX = (int)m_nativeRes.x;
          m_resY = (int)m_nativeRes.y;
        }

        // Bind viewport to pipeline
        m_deviceContext->RSSetViewports(1, &viewport);

        //Resize all the other rendertargets currently exisiting
        for (unsigned i = 0; i < m_renderTargetRes.size(); i++)
        {
          if(ReleaseRenderTargetIntern(Handle(OBJT::RenderTarget, i)))
			CreateRenderTarget(Handle());
        }

        if (fullscreen)
        {
          m_swapChain->SetFullscreenState(true, NULL);

          //Clip the cursor to the screen if fullscreen
          RECT rcClip;
          GetWindowRect(m_hwnd, &rcClip);
          ClipCursor(&rcClip);

        }

        if (!fullscreen)
        {
          SetWindowLongPtr(m_hwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW | WS_VISIBLE);
          ShowWindow(m_hwnd, SW_SHOWMAXIMIZED);

          //Put the window back in the place and at the size it was before
          RECT resolution;
          GetClientRect(m_hwnd, &resolution);
          AdjustWindowRect(&resolution, WS_OVERLAPPEDWINDOW, FALSE);
          MoveWindow(m_hwnd, 0, 0, resolution.right - resolution.left, resolution.bottom - resolution.top, TRUE);
          UpdateWindow(m_hwnd);
        }

        //Release our local backbuffer
        backBuffer->Release();
      }
    }


    //--------------------------//
    // Set Priminitive Topology //
    //--------------------------//
    void GFXRenderer::SetPrimitiveTopology(const PrimitiveTopology primitiveTopology)
    {
      m_deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY(primitiveTopology));
    }

    //-----------------------//
    // Set Viewport          //
    //-----------------------//
    void GFXRenderer::SetViewport(int xOffset, int yOffset, float dimensionX, float dimensionY)
    {
      // Set the viewport
      D3D11_VIEWPORT viewport;

      // Clear of the memory
      ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

      viewport.TopLeftX = (FLOAT)xOffset;
      viewport.TopLeftY = (FLOAT)yOffset;
      viewport.Width = dimensionX;
      viewport.Height = dimensionY;

      // Bind viewport to pipeline
      m_deviceContext->RSSetViewports(1, &viewport);
    }

    //-----------------------//
    // Set Viewport          //
    //-----------------------//
    void GFXRenderer::SetViewport(const Viewport& viewport)
    {
      SetViewport(viewport.xOffset, viewport.yOffset, viewport.dimensionX, viewport.dimensionY);
    }

    //-------------------//
    // Set Vsync Status  //
    //-------------------//
    void GFXRenderer::SetVSync(bool vsync)
    {
      //This doesn't actually do anything right now.
      m_vsync = vsync;
    }

    //---------//
    // GETTERS //
    //---------//

    //---------------------------------//
    // Get a handle to the back buffer //
    //---------------------------------//
    Handle GFXRenderer::GetBackBuffer(void) const
    {
      return Handle(OBJT::BackBuffer, 0);
    }

    //---------------------------------//
    // Get if currently in full screen //
    //---------------------------------//
    bool GFXRenderer::GetFullScreen(void) const
    {
      return m_fullscreen;
    }

    //-------------------------//
    // Get the direct X device //
    //-------------------------//
    ID3D11Device* GFXRenderer::GetDevice(void) const
    {
      return m_device;
    }

    //----------------------------------//
    // Get a the DirectX device context //
    //----------------------------------//
    ID3D11DeviceContext* GFXRenderer::GetDeviceContext(void) const
    {
      return m_deviceContext;
    }

    //----------------------------//
    // Get the current clearColor //
    //----------------------------//
    D3DXCOLOR GFXRenderer::GetClearColor(void) const
    {
      return m_clearColor;
    }

    //-----------//
    // UTILITIES //
    //-----------//

    //------------------------------------------//
    // Clear the back buffer to the clear color //
    //------------------------------------------//
    void GFXRenderer::ClearBackBuffer(void)
    {
      m_deviceContext->ClearRenderTargetView(m_backBuffer, m_clearColor);
    }

    //---------------------------------------//
    // Clear the the specified render target //
    //---------------------------------------//
    void GFXRenderer::ClearRenderTarget(Handle& rTarget)
    {
      m_deviceContext->ClearRenderTargetView(m_renderTargetRes[rTarget.index].renderTargetView, m_clearColor);
    }

    //----------------------------//
    // Set the current blend mode //
    //----------------------------//
    void GFXRenderer::SetBlendMode(Handle& bHandle)
    {
      if (bHandle.type != OBJT::BlendMode)
        ErrorCheck("INCORRECT TYPE: BLEND STATE");

      m_deviceContext->OMSetBlendState(m_blendStateRes[bHandle.index], 0, 0xffffffff);
    }

    //---------------------------------------------------//
    // Resize rendertargets when the window size changes //
    //---------------------------------------------------//
    void GFXRenderer::Resize(LPARAM lParam)
    {
      if (m_device)
      {
        //Don't bother resizing if window went to size 0x0 (meaning minimized)
        if (m_swapChain && !m_changingFSMode && (LOWORD(lParam) != 0 && HIWORD(lParam) != 0))
        {
          ID3D11Texture2D* backBuffer = nullptr;
          HRESULT errorHR;
          
          //Detached All Render Targets
          m_deviceContext->OMSetRenderTargets(0, 0, 0);

          //Handling the Back Buffer
          m_backBuffer->Release();

          errorHR = m_swapChain->ResizeBuffers(0, 
                                               LOWORD(lParam), 
                                               HIWORD(lParam), 
                                               DXGI_FORMAT_R8G8B8A8_UNORM,
                                               DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH);

          ErrorCheck(errorHR, true, "FAILED TO CHANGE SCREEN SIZE: (RESIZE BUFFER)");

          errorHR = m_swapChain->GetBuffer(0, 
                                           __uuidof(ID3D11Texture2D), 
                                           (void**)&backBuffer);

          ErrorCheck(errorHR, true, "FAILED TO CHANGE SCREEN SIZE: (LOCAL BACKBUFFER)");

          errorHR = m_device->CreateRenderTargetView(backBuffer, 
                                                     NULL, 
                                                     &m_backBuffer);

          ErrorCheck(errorHR, true, "FAILED TO CHANGE SCREEN SIZE: (BACKBUFFER)");

          //Reset the Back Buffer as the binded render target
          m_deviceContext->OMSetRenderTargets(1, &m_backBuffer, NULL);

          // Set the viewport
          D3D11_VIEWPORT viewport;

          ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

          viewport.TopLeftX = 0;
          viewport.TopLeftY = 0;
          viewport.Width    = (float)LOWORD(lParam);
          viewport.Height   = (float)HIWORD(lParam);


          // Bind viewport to pipeline
          m_deviceContext->RSSetViewports(1, &viewport);

          //Release our local back buffer
          backBuffer->Release();

          //Set appropriate variables
          m_windowSystem->SetClientSize((int)LOWORD(lParam), (int)HIWORD(lParam));
          m_resX = (int)LOWORD(lParam);
          m_resY = (int)HIWORD(lParam);

          //Resize all the other rendertargets currently exisiting
          for (unsigned i = 0; i < m_renderTargetRes.size(); i++)
          {
            if(ReleaseRenderTargetIntern(Handle(OBJT::RenderTarget, i)))
	            CreateRenderTarget(Handle());
          }
        }
      }
    }

    //---------------------------------------------------------//
    // Update the Constant Buffer - pass Matrix as a Reference //
    //---------------------------------------------------------//
    void GFXRenderer::UpdateConstantBuffer(const Handle& cHandle, D3DXMATRIX& cameraMatrix)
    {
      m_deviceContext->UpdateSubresource(m_constBufferRes[cHandle.index], 0, 0, &cameraMatrix, 0, 0);
    }

    //---------------------------------------------------------//
    // Update the Constant Buffer - pass Matrix as a pointer   //
    //---------------------------------------------------------//
    void GFXRenderer::UpdateConstantBuffer(const Handle& cHandle, D3DXMATRIX* cameraMatrixes)
    {
      m_deviceContext->UpdateSubresource(m_constBufferRes[cHandle.index], 0, 0, cameraMatrixes, 0, 0);
    }

    //--------------------------------------------------------//
    // This handles alt-tab, input and cursor clipping things //
    //--------------------------------------------------------//
    void GFXRenderer::SetWindowActive(WPARAM wparam, LPARAM lparam)
    {
      //Ignore if Graphics hasn't been made yet
      if (!m_device)
        return;

      //Is the window currently in focus
      bool wActive = LOWORD(wparam) == WA_ACTIVE;

      //Handle differently based on if in fullscreen
      if (GetFullScreen())
      {
        if (wActive)
        {
          ShowWindow(m_hwnd, SW_RESTORE);
          m_swapChain->SetFullscreenState(true, nullptr);
          
          BlockInput(false);

          //Clip cursor
          RECT rcClip;
          GetWindowRect(m_hwnd, &rcClip);
          ClipCursor(&rcClip);
        }
        else //InActive
        {
          m_swapChain->SetFullscreenState(FALSE, nullptr);
          ShowWindow(m_hwnd, SW_MINIMIZE);
          BlockInput(true);
          
        }
      }
      else if (wActive)
      {
        ShowWindow(m_hwnd, SW_MAXIMIZE);
        BlockInput(false);
      }
      else //InActive
      {
        BlockInput(true);
      }
    }

    //-------------------------//
    // PUBLIC RELEASE FUNCTION //
    //-------------------------//
    void GFXRenderer::Release(Handle &handle)
    {
      switch (handle.type)
      {
      case OBJT::BackBuffer:
        //No release function for this yet
        m_backBuffer->Release();
        break;
      case OBJT::ConstantBuffer:
        ReleaseConstantBufferIntern(handle);
        break;
      case OBJT::IndexBuffer:
        ReleaseIndexBufferIntern(handle);
        break;
      case OBJT::PixelShader:
        ReleasePixelShaderIntern(handle);
        break;
      case OBJT::RenderTarget:
        ReleaseRenderTargetIntern(handle);
        break;
      case OBJT::Texture:
        ReleaseTextureIntern(handle);
        break;
      case OBJT::VertexBuffer:
        ReleaseVertexBufferIntern(handle);
        break;
      case OBJT::VerterxShader:
        ReleaseVertexShaderIntern(handle);
        break;
      case OBJT::BlendMode:
        ReleaseBlendStateIntern(handle);
        break;
      case OBJT::RasterizerState:
        ReleaseRasterizerObjectIntern(handle);
        break;
      default:
        break;
      }

      handle = Handle();
    }

    //------------------//
    // INITIALIZE CALLS //                        PRIVATE
    //------------------//

    //-------------------//
    // Initialize Stuffs //                       PRIVATE
    //-------------------//
    void GFXRenderer::InitializeDXGI(void)
    {
      //No clue what goes in here yet
    }

    int DisplayResourceNAMessageBox()
    {
      int msgboxID = MessageBox(
        NULL,
        (LPCWSTR)L"This graphics card is not recommended for this game.\n Do you want to continue anyway?",
        (LPCWSTR)L"Graphics Card Compatibility",
        MB_ICONWARNING | MB_YESNO | MB_DEFBUTTON2
        );

      switch (msgboxID)
      {
      case IDNO:
        return true;
        break;
      case IDYES:
        return false;
        break;
      }

      return msgboxID;
    }

    //--------------------------------------//
    // Initialize the Device and Swap Chain //    PRIVATE
    //--------------------------------------//
    void GFXRenderer::InitializeDeviceAndSwapChain(void)
    {
      ////////////////////////////////////////////////////////////////////////////////////////////////////////
      // DXGI_SWAP_CHAIN_DESC                                                                               //
      //  PARAM1 : BufferDesc   : DXGI_MODE_DESC   - Struct describing a display mode (variables below)     //
      //                          (Width, Height, RefreshRate, Format, ScanLineOrdering, Scaling)           //
      //  PARAM2 : SampleDesc   : DXGI_SAMPLE_DESC - Struct describing multi-sampling params for a resource //
      //  PARAM3 : BufferUsage  : DXGI_USAGE       - ENUM for CPU access and surface usage                  //
      //  PARAM4 : BufferCount  : UINT             - Number of back buffers                                 //
      //  PARAM5 : OutputWindow : HWND             - Window Handle for this application                     //
      //  PARAM6 : Windowed     : BOOL             - TRUE = Windowed Mode, use diff function for FullScreen //
      //  PARAM7 : SwapEffect   : DXGI_SWAP_EFFECT - For handling pixels, use default (0)                   //
      //  PARAM8 : Flags        : UINT             - DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH can FullScreen  //
      ////////////////////////////////////////////////////////////////////////////////////////////////////////

      // Create a struct to hold info about the swap chain
      DXGI_SWAP_CHAIN_DESC swapChainDesc;
      
      // Clear out the struct
      ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

      // Fill the struct (and sub-structs)
      swapChainDesc.BufferCount           = 1;
      swapChainDesc.BufferDesc.Format     = DXGI_FORMAT_R8G8B8A8_UNORM;       // use 32-bit color
      swapChainDesc.BufferDesc.Width      = m_resX;                           // set the back buffer width
      swapChainDesc.BufferDesc.Height     = m_resY;                           // set the back buffer height
      swapChainDesc.BufferUsage           = DXGI_USAGE_RENDER_TARGET_OUTPUT;
      swapChainDesc.OutputWindow          = m_hwnd;
      swapChainDesc.SampleDesc.Count      = 1;
      swapChainDesc.Windowed              = TRUE;
      swapChainDesc.Flags                 = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH; //Alt-Enter fullscreens
      //swapChainDesc.BufferDesc.RefreshRate.Numerator = 1;
      //swapChainDesc.BufferDesc.RefreshRate.Denominator = 60;

      ////////////////////////////////////////////////////////////////////////////////////////////////////////
      // D3D11CreateDeviceAndSwapChain                                                                      //
      //  BREIF  : Creates a device that represents the display adapter and a swap chain used for rendering //
      //  PARAM1 : pAdapter           : IDXGIAdapter*              - NULL for Default Video Adapter         //
      //  PARAM2 : DriverType         : D3D_DRIVER_TYPE            - D3D_DRIVER_TYPE_HARDWARE, best perf.   //
      //  PARAM3 : Software           : HMODULE                    - Only !NULL when above is ..._SOFTWARE  //
      //  PARAM4 : Flags              : UINT                       - Flags we probably don't need           //
      //  PARAM5 : pFeatureLevels     : const D3D_FEATURE_LEVEL    - NULL for Defualt feature levels        //
      //  PARAM6 : FeatureLevels      : UINT                       - NULL since above is NULL               //
      //  PARAM7 : SDKVersion         : UINT                       - D3D11_SDK_VERSION (DirectX 11)         //
      //  PARAM8 : pSwapChainDesc     : const DXGI_SWAP_CHAIN_DESC - Pointer to Swap Chain Desc Struct      //
      //  PARAM9 : ppSwapChain        : IDXGISwapChain**           - Address of ptr of the Swap Chain       //
      //  PARAMA : ppDevice           : ID3D11Device**             - Address of ptr of the Device Interface //
      //  PARAMB : pFeatureLevel      : D3D_FEATURE_LEVEL*         - NULL since FeatureLevels is NULL       //
      //  PARAMC : ppImmediateContext : ID3D11DeviceContext**      - Address of ptr of the Device Context   //
      ////////////////////////////////////////////////////////////////////////////////////////////////////////

      unsigned deviceFlags = NULL;

#if defined GFX_DEBUG
      //deviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
      //deviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
      // Create a device, device context and swap chain using the information in the above struct
      HRESULT errorHR = D3D11CreateDeviceAndSwapChain(NULL,
                                                      D3D_DRIVER_TYPE_HARDWARE,
                                                      NULL,
                                                      deviceFlags,
                                                      NULL,
                                                      NULL,
                                                      D3D11_SDK_VERSION,
                                                      &swapChainDesc,
                                                      &m_swapChain,
                                                      &m_device,
                                                      NULL,
                                                      &m_deviceContext);

      

      ErrorCheck(errorHR, true, "FAILED TO CREATE CONSTANT SWAP CHAIN");

      IDXGIFactory1 *pFactory = nullptr;

      //Disable Alt-Enter switching into fullscreen mode
      errorHR = m_swapChain->GetParent(__uuidof (IDXGIFactory1), (void **)&pFactory);
      ErrorCheck(errorHR, true, "FAILED DISABLE ALT ENTER - GET PARENT FUNCTION");
      
      errorHR = pFactory->MakeWindowAssociation(m_hwnd, DXGI_MWA_NO_ALT_ENTER);
      ErrorCheck(errorHR, true, "FAILED DISABLE ALT ENTER - MAKE WIND ASSOC FUNCTION");
      
      IDXGIAdapter1* pAdapter;
      pFactory->EnumAdapters1(0, &pAdapter);
      DXGI_ADAPTER_DESC1 Desc;
      pAdapter->GetDesc1(&Desc);
      unsigned vram = Desc.DedicatedVideoMemory;
      
      if (vram == 0)
      {
        bool abort = DisplayResourceNAMessageBox();
        if (abort)
        {
          pAdapter->Release();
          pFactory->Release();
          m_swapChain->Release();
          m_deviceContext->Release();
          m_device->Release();
          throw(true);
        }
      }
     
      
      pAdapter->Release();
      pFactory->Release();
    }

    

    //----------------------------//
    // Initialize the Back Buffer //              PRIVATE
    //----------------------------//
    void GFXRenderer::InitializeBackBuffer(void)
    {
      ////////////////////////////////////////////////////////////////////////////////////////////////////////
      // ID3D11Texture2D                                                                                    //
      //  BREIF  : A 2D texture interface manages texel data, which is structured memory                    //
      ////////////////////////////////////////////////////////////////////////////////////////////////////////

      ID3D11Texture2D *backBuffer = nullptr;

      ////////////////////////////////////////////////////////////////////////////////////////////////////////
      // GetBuffer                                                                                          //
      //  BREIF  : Accesses one of the swap-chain's back buffers.                                           //
      //  PARAM1 : Buffer     : UINT    - A zero based buffer index                                         //
      //  PARAM2 : riid       : REFIID  - Type of interface used to manipulate the buffer                   //
      //  PARAM3 : ppSurface  : void ** - A pointer to a back-buffer interface.                             //
      //  RETURN : HRESULT    : Returns an error enum                                                       //
      ////////////////////////////////////////////////////////////////////////////////////////////////////////

      // Get the address of the back buffer
      HRESULT errorHR_1 = m_swapChain->GetBuffer(0, 
                                                 __uuidof(ID3D11Texture2D), 
                                                 (LPVOID*)&backBuffer);

      ErrorCheck(errorHR_1, true, "FAILED TO GET ADDRESS OF BACK BUFFER");

      ///////////////////////////////////////////////////////////////////////////////////////////////////////
      // CreateRenderTargetView                                                                             //             
      //  BREIF  : Creates a render-target view for accessing resource data                                 //
      //  PARAM1 : pResource  : D3D11Resource*                        - ID3D11Texture2D ptr = render target //
      //  PARAM2 : pDesc      : const D3D11_RENDER_TARGET_VIEW_DESC*  - NULL  = access all subresources     //
      //  PARAM3 : ppRTView   : ID3D11RenderTargetView*               - Render target subresources          //
      //  RETURN : HRESULT    : Returns an error enum                                                       //
      ////////////////////////////////////////////////////////////////////////////////////////////////////////

      // Use the back buffer address to create the render target
      HRESULT errorHR_2 = m_device->CreateRenderTargetView(backBuffer, NULL, &m_backBuffer);

      ErrorCheck(errorHR_2, true, "FAILED TO CREATE RENDER TARGET");

      // Free the local object
      backBuffer->Release();

      ////////////////////////////////////////////////////////////////////////////////////////////////////////
      // OMSetRenderTargets                                                                                 //             
      //  BREIF  : Bind one or more render targets atomically & depth-stencil buffer to output-merger stage //
      //  PARAM1 : NumViews             : UINT                          - Number of render targets to bind  //
      //  PARAM2 : ppRenderTargetViews  : ID3D11RenderTargetView *const - Render targets to bind to device  //
      //  PARAM3 : pDepthStencilView    : ID3D11DepthStencilView *      - depth-stencil view, NULL when N/A //
      //  RETURN : NONE                                                                                     //
      ////////////////////////////////////////////////////////////////////////////////////////////////////////

      // Set the render target as the back buffer for good measure
      m_deviceContext->OMSetRenderTargets(1, &m_backBuffer, NULL);

    }

    //-------------------------//
    // Initialize the ViewPort //                 PRIVATE
    //-------------------------//
    void GFXRenderer::InitializeViewport(void)
    {
      ////////////////////////////////////////////////////////////////////////////////////////////////////////
      // D3D11_VIEWPORT                                                                                     //
      //  PARAM1 : FLOAT  : TopLeftX  - X position of the left hand side of the Viewport                    //
      //  PARAM2 : FLOAT  : TopLeftY  - Y position of the top of the view port                              //
      //  PARAM3 : FLOAT  : Width     - Width of the viewport                                               //
      //  PARAM4 : FLOAT  : Height    - Height of the viewport                                              //
      //  PARAM5 : FLOAT  : MinDepth  - Minimium depth of the viewport (Ranges between 0 and 1)             //
      //  PARAM6 : FLOAT  : MaxDepth  - Maximium depth of the viewport (Ranges between 0 and 1)             //
      ////////////////////////////////////////////////////////////////////////////////////////////////////////

      // Set the viewport
      D3D11_VIEWPORT viewport;

      // Clear of the memory
      ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

      viewport.TopLeftX = 0;
      viewport.TopLeftY = 0;
      viewport.Width    = (float)m_resX;
      viewport.Height   = (float)m_resY;
      viewport.MinDepth = 0.0f;
      viewport.MaxDepth = 1.0f;

      ////////////////////////////////////////////////////////////////////////////////////////////////////////
      // RSSetViewports                                                                                     //             
      //  BREIF  : Bind an array of viewports to the rasterizer stage of the pipeline                       //
      //  PARAM1 : NumViewports : UINT                  - Number of viewports to bind.                      //
      //  PARAM2 : pViewports   : const D3D11_VIEWPORT* - Array of viewport structs to bind to the device   //
      //  RETURN : NONE                                                                                     //
      ////////////////////////////////////////////////////////////////////////////////////////////////////////

      // Bind viewport to pipeline
      m_deviceContext->RSSetViewports(1, &viewport);
    }

    //---------------------//
    // Setup Sampler State //                     PRIVATE
    //---------------------//
    void GFXRenderer::InitializeSamplerState(void)
    {
      D3D11_SAMPLER_DESC sampleDesc;

      ZeroMemory(&sampleDesc, sizeof(D3D11_SAMPLER_DESC));

      sampleDesc.Filter   = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
      sampleDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
      sampleDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
      sampleDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
      sampleDesc.MinLOD   = 0;
      sampleDesc.MaxLOD   = D3D11_FLOAT32_MAX;

      sampleDesc.MaxAnisotropy    = 1;
      sampleDesc.ComparisonFunc   = D3D11_COMPARISON_ALWAYS;
      HRESULT errorHR             = m_device->CreateSamplerState(&sampleDesc, &m_sampleStates);

      ErrorCheck(errorHR, true, "FAILED TO SETUP SAMPLE STATE");

      m_deviceContext->PSSetSamplers(0, 1, &m_sampleStates);
    }

    //------------------------------------//
    // Setup a Default Blend Mode - Alpha //      PRIVATE
    //------------------------------------//
    void GFXRenderer::InitializeBlendModes(void)
    {
      D3D11_BLEND_DESC blendDesc;

      blendDesc.RenderTarget[0].BlendEnable           = TRUE;
      blendDesc.RenderTarget[0].BlendOp               = D3D11_BLEND_OP_ADD;
      blendDesc.RenderTarget[0].SrcBlend              = D3D11_BLEND_ONE;
      blendDesc.RenderTarget[0].DestBlend             = D3D11_BLEND_INV_SRC_ALPHA;
      blendDesc.RenderTarget[0].BlendOpAlpha          = D3D11_BLEND_OP_ADD;
      blendDesc.RenderTarget[0].SrcBlendAlpha         = D3D11_BLEND_ONE;
      blendDesc.RenderTarget[0].DestBlendAlpha        =  D3D11_BLEND_ONE;
      blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
      blendDesc.IndependentBlendEnable                = FALSE;
      blendDesc.AlphaToCoverageEnable                 = FALSE;

      m_device->CreateBlendState(&blendDesc, &m_blendState);

      m_deviceContext->OMSetBlendState(m_blendState, 0, 0xffffffff);
    }

    //---------------//
    // RELEASE CALLS //
    //---------------//

    //-----------------------------//
    // Release Textures Internally //             PRIVATE
    //-----------------------------//
    void GFXRenderer::ReleaseTextureIntern(const Handle& texture)
    {
      if (texture.type != OBJT::Texture)
        ErrorCheck("INCORRECT TYPE: TEXTURE");

      //Release a single texture
      m_textureRes[texture.index].sResourceView->Release();
      m_textureRes[texture.index].sResourceView = nullptr;

      m_vacancies.push_back(texture);
    }

    //----------------------------------//
    // Release Vertex Shader Internally //        PRIVATE
    //----------------------------------//
    void GFXRenderer::ReleaseVertexShaderIntern(const Handle& vertexShader)
    {
      if (vertexShader.type != OBJT::VerterxShader)
        ErrorCheck("INCORRECT TYPE: VS");

      m_vertexShaderRes[vertexShader.index].vShader->Release();
      m_vertexShaderRes[vertexShader.index].vShader = nullptr;
      m_vertexShaderRes[vertexShader.index].inputLayout->Release();
      m_vertexShaderRes[vertexShader.index].inputLayout = nullptr;

      m_vacancies.push_back(vertexShader);
    }

    //----------------------------------//
    // Release Pixel Shader Internally  //        PRIVATE
    //----------------------------------//
    void GFXRenderer::ReleasePixelShaderIntern(const Handle& pixelShader)
    {
      if (pixelShader.type != OBJT::PixelShader)
        ErrorCheck("INCORRECT TYPE: PS");

      m_pixelShaderRes[pixelShader.index]->Release();
      m_pixelShaderRes[pixelShader.index] = nullptr;
      m_vacancies.push_back(pixelShader);
    }

    //-----------------------------------//
    // Release Vertex Buffer Internally  //       PRIVATE
    //-----------------------------------//
    void GFXRenderer::ReleaseVertexBufferIntern(const Handle& vertexBuffer)
    {
      if (vertexBuffer.type != OBJT::VertexBuffer)
        ErrorCheck("INCORRECT TYPE: VB");

      m_vertexBufferRes[vertexBuffer.index]->Release();
      m_vertexBufferRes[vertexBuffer.index] = nullptr;
      m_vacancies.push_back(vertexBuffer);
    }

    //----------------------------------//
    // Release Index Buffer Internally  //        PRIVATE
    //----------------------------------//
    void GFXRenderer::ReleaseIndexBufferIntern(const Handle& indexBuffer)
    {
      if (indexBuffer.type != OBJT::IndexBuffer)
        ErrorCheck("INCORRECT TYPE: IB");

      m_indexBufferRes[indexBuffer.index]->Release();
      m_indexBufferRes[indexBuffer.index] = nullptr;
      m_vacancies.push_back(indexBuffer);
    }

    //-------------------------------------//
    // Release Constant Buffer Internally  //     PRIVATE
    //-------------------------------------//
    void GFXRenderer::ReleaseConstantBufferIntern(const Handle& constantBuffer)
    {
      if (constantBuffer.type != OBJT::ConstantBuffer)
        ErrorCheck("INCORRECT TYPE: CB");

      m_constBufferRes[constantBuffer.index]->Release();
      m_constBufferRes[constantBuffer.index] = nullptr;

      m_vacancies.push_back(constantBuffer);
    }

    //----------------------------------//
    // Release RenderTarget Internally  //        PRIVATE
    //----------------------------------//
    bool GFXRenderer::ReleaseRenderTargetIntern(const Handle& renderTarget)
    {
      if (renderTarget.isNull())
        return false;

      if (renderTarget.type != OBJT::RenderTarget)
        ErrorCheck("INCORRECT TYPE: RT");

      if (!m_renderTargetRes[renderTarget.index].texture2D        &&
          !m_renderTargetRes[renderTarget.index].renderTargetView &&
          !m_renderTargetRes[renderTarget.index].sResourceView)
        return false;

      //Release the various objects in the RenderTarget struct
      m_renderTargetRes[renderTarget.index].texture2D->Release();
      m_renderTargetRes[renderTarget.index].renderTargetView->Release();
      m_renderTargetRes[renderTarget.index].sResourceView->Release();

      //Set them to null just to be safe
      m_renderTargetRes[renderTarget.index].texture2D = nullptr;
      m_renderTargetRes[renderTarget.index].renderTargetView = nullptr;
      m_renderTargetRes[renderTarget.index].sResourceView = nullptr;

      m_vacancies.push_back(renderTarget);

	  return true;
    }

    //----------------------------------//
    // Release BlendState Internally    //        PRIVATE
    //----------------------------------//
    void GFXRenderer::ReleaseBlendStateIntern(const Handle& blendMode)
    {
      if (blendMode.type != OBJT::BlendMode)
        ErrorCheck("INCORRECT TYPE: BM");

      m_blendStateRes[blendMode.index]->Release();
      m_blendStateRes[blendMode.index] = nullptr;

      m_vacancies.push_back(blendMode);
    }

    //--------------------------------------//
    // Release RasterizerObject Internally  //    PRIVATE
    //--------------------------------------//
    void GFXRenderer::ReleaseRasterizerObjectIntern(const Handle& rasterizerObject)
    {
      if (rasterizerObject.type != OBJT::RasterizerState)
        ErrorCheck("INCORRECT TYPE: RO");

      m_rasterizerStateObjRes[rasterizerObject.index]->Release();
      m_rasterizerStateObjRes[rasterizerObject.index] = nullptr;

      m_vacancies.push_back(rasterizerObject);
    }

    //---------------------------------------------------------//
    // Find and empty slot for this handle and set that handle //   PRIVATE
    //---------------------------------------------------------//
    bool GFXRenderer::FindEmptySlot(Handle& handle, OBJT type)
    {
      //Lamda? I don't even know ya!
      auto it = std::find_if(m_vacancies.begin(), m_vacancies.end(), [&](const Handle &h)
      {
        return h.type == type;
      });

      //Set the handle to the vacant one found
      if (it != m_vacancies.end())
      {
        handle = Handle(it->type, it->index);
        m_vacancies.erase(it);
        return true;
      }

      return false;
    }

    //-----------------//
    // Deconstructor   //     
    //-----------------//
    GFXRenderer::~GFXRenderer(void)
    {
      //So UnInitialize
    }

  } //namespace Graphics
} //namespace Omni
