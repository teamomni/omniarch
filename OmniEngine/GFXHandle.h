/******************************************************************************
Filename: GFXHandle.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// Juli Gregg
// GFXHandle.h

#pragma once

#include "GFXTools.h"

namespace Omni
{
  namespace Graphics
  {

    class Handle
    {
    public:
      //Makes a blank handle
      Handle();

      //Dereference Operator
      int operator*(void) const;

      Handle(const Handle& rhs);
      Handle& operator=(const Handle& rhs);
      bool operator==(const Handle& rhs) const;
      bool operator!=(const Handle& rhs) const;
      bool operator<(const Handle& rhs) const;
      bool isNull(void) const;

    private:
      //Constructor
      Handle(OBJT type_, int index_);

      OBJT type;
      int index;

      //friend classes
      friend class GFXRenderer;
      friend struct SpriteBatch;
      friend class Text;
    };

  } //namespace Graphics
} //namespace Omni
