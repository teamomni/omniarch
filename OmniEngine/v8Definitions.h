/******************************************************************************
Filename: v8Definitions.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "v8.h"

namespace Omni
{
  extern v8::Isolate *g_Isolate;
  class Space;

  // PERSISTENTS
  typedef v8::Persistent<v8::Object, v8::CopyablePersistentTraits<v8::Object>> PersistentObject;
  typedef v8::Persistent<v8::ObjectTemplate, v8::CopyablePersistentTraits<v8::ObjectTemplate>> PersistentObjectTemplate;
  typedef v8::Persistent<v8::Function, v8::CopyablePersistentTraits<v8::Function>> PersistentFunction;
  typedef v8::Persistent<v8::FunctionTemplate, v8::CopyablePersistentTraits<v8::FunctionTemplate>> PersistentFunctionTemplate;
  typedef v8::Persistent<v8::Context, v8::CopyablePersistentTraits<v8::Context>> PersistentContext;

  // LOCALS
  typedef v8::Local<v8::Value> LocalValue;
  typedef v8::Local<v8::Object> LocalObject;
  typedef v8::Local<v8::External> LocalExternal;
  typedef v8::Local<v8::Function> LocalFunction;
  typedef v8::Local<v8::ObjectTemplate> LocalObjectTemplate;
  typedef v8::Local<v8::FunctionTemplate> LocalFunctionTemplate;

  // FUNCTION POINTERS
  typedef void(*GettorFunctionPointer)(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info);
  typedef void(*SettorFunctionPointer)(v8::Local<v8::String> property, v8::Local<v8::Value> value, const v8::FunctionCallbackInfo<v8::Value> &info);

  // Templated structs
  template <typename T>
  struct Take
  {
    static T* FromHolder_NonComponent(const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      LocalObject obj = info.Holder();
      LocalExternal wrap = LocalExternal::Cast(obj->GetInternalField(0));
      T *ptr = static_cast<T *>(wrap->Value());

      return ptr;
    }

    static T* FromHolder(const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      LocalObject obj = info.Holder();
      LocalExternal wrap = LocalExternal::Cast(obj->GetInternalField(0));
      T **ptr = static_cast<T **>(wrap->Value());

      if (ptr)
        return *ptr;

      return nullptr;
    }

    static T* FromV8(LocalValue &val)
    {
      LocalExternal wrap = LocalExternal::Cast(LocalObject::Cast(val)->GetInternalField(0));
      T *ptr = static_cast<T *>(wrap->Value());

      return ptr;
    }
  };

  template <>
  struct Take<Space>
  {
    static Space *FromHolder(const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      LocalObject obj = info.Holder();
      LocalExternal wrap = LocalExternal::Cast(obj->GetInternalField(0));
      Space *ptr = static_cast<Space *>(wrap->Value());

      return ptr;
    }

    static Space* FromHolder_NonComponent(const v8::FunctionCallbackInfo<v8::Value> &info)
    {
      LocalObject obj = info.Holder();
      LocalExternal wrap = LocalExternal::Cast(obj->GetInternalField(0));
      Space *ptr = static_cast<Space *>(wrap->Value());

      return ptr;
    }
  };


  template <>
  struct Take<int>
  {
    static int FromV8(LocalValue &val)
    {
      return int(val->Int32Value());
    }
  };

  template <>
  struct Take<float>
  {
    static float FromV8(LocalValue &val)
    {
      return float(val->NumberValue());
    }
  };

  template <>
  struct Take<bool>
  {
    static bool FromV8(LocalValue &val)
    {
      return val->BooleanValue();
    }
  };

  template <>
  struct Take<std::string>
  {
    static std::string FromV8(LocalValue &val)
    {
      v8::String::Utf8Value v8String(val);
      std::string ret(*v8String);
      return ret;
    }
  };


}
