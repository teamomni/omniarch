/******************************************************************************
Filename: ComponentDefinition.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once
#include "Property.h"
#include "Handlet.h"

namespace Omni
{
  struct ComponentDefinition
  {
    enum class Type
    {
      Undefined,
      System,
      User
    };

    std::string name;
    Type type;
    std::function<Handlet<Component>(Entity *)> systemComponentCreateFunction; // that's gross
    PropertyDefinitionMap properties;

    ComponentDefinition() : type(Type::Undefined) {}
    ComponentDefinition(std::string name_) : name(name_), type(Type::User) {}
    ComponentDefinition(std::string name_, PropertyDefinitionMap properties_) : name(name_), type(Type::User), properties(properties_) {}
    ComponentDefinition(std::string name_, std::function<Handlet<Component>(Entity *)> createFunction) : name(name_), systemComponentCreateFunction(createFunction), type(Type::System) {}
    ComponentDefinition(std::string name_, std::function<Handlet<Component>(Entity *)> createFunction, PropertyDefinitionMap properties_): name(name_), systemComponentCreateFunction(createFunction), type(Type::System), properties(properties_) {}

    Handlet<Component> Create(Entity *entity);
    void AddProperty(std::string name_, Property::Type type_);
  };
}