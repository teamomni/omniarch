/******************************************************************************
Filename: Clock.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "stdafx.h"

class Clock
{
public:
  Clock();
  ~Clock();

  // stores current tick count in start
  void Start();

  // stores current tick count in stop
  void Stop();

  // gets the amount of time passed since start
  float Elapsed();

  // gets the time diff between start and stop
  float Difference();

  // stores the current tick count in current
  LONGLONG Current();

  // given a duration of time, return number of ticks
  LONGLONG TimeInTicks(float time);

  // given a number of ticks, return duration of time
  float TicksInTime(LONGLONG ticks);

  // the tick when the last second started
  LARGE_INTEGER secondStart;

private:
  LARGE_INTEGER freq;
  LARGE_INTEGER start, stop, current;

  void Query(LARGE_INTEGER& query);
};

