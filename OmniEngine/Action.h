/******************************************************************************
Filename: Action.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include <list>

class ActionList;

class Action
{
public:
  virtual void Update(float dt);
  virtual void OnStart();
  virtual void OnEnd();
  bool isFinished;
  bool isBlocking;
  float elapsed;
  float duration;

private:
  ActionList *owner;
};

class ActionList
{
public:
  void Update(float dt);
  void PushFront(Action *action);
  void PushBack(Action *action);
  void InsertBefore(Action *existing, Action *action);
  void InsertAfter(Action *existing, Action *action);
  Action *Remove(Action *action);

  inline Action *Begin() { return *actions.begin(); }
  inline Action *End() { return *actions.end(); }

  inline bool IsEmpty() const { return actions.empty(); }
  inline float TimeLeft() const { return duration - timeElapsed; }
  inline bool IsBlocking() const { return blocking; }

private:
  float duration = 0.0f;
  float timeElapsed = 0.0f;
  float percentDone;
  bool blocking;
  std::list<Action *> actions;
};