/******************************************************************************
Filename: PropertyHandle.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "PropertyHandle.h"
#include "Game.h"

namespace Omni
{
  bool ModulePropertyHandle::operator==(const ModulePropertyHandle &rhs) const
  {
    return component == rhs.component && property == rhs.property;
  }

  Handlet<Component> ModulePropertyHandle::GetComponent()
  {
    return component;
  }
}