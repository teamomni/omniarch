/******************************************************************************
Filename: Vec2.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include <ostream>

namespace Omni
{
  struct Vec2
  {
  public:

    /******* Public Data *******/
    float x;
    float y;

    /****** Public Methods ******/
    Vec2();
    Vec2(float x_, float y_);
    Vec2(float theta);

    void Set(float x_, float y_);
    void Normalize();
    Vec2 Normalized();
    float Length();
    float LengthSquared();
    float GetAngle() const;
    float Cross(const Vec2 &rhs);
    Vec2 Midpoint(Vec2 &rhs);

    Vec2 operator+(const Vec2 &rhs) const;
    Vec2 &operator+=(const Vec2 &rhs);
    Vec2 operator-(const Vec2 &rhs) const;
    Vec2 &operator-=(const Vec2 &rhs);
    Vec2 operator*(float scalar) const;
    float operator*(const Vec2 &rhs) const;
    Vec2 &operator*=(float scalar);
    Vec2 operator/(float divisor) const;
    Vec2 &operator/=(float divisor);
    bool operator==(const Vec2 &rhs) const;
    bool Vec2::operator>(const Vec2 &rhs) const;
    bool Vec2::operator<(const Vec2 &rhs) const;
    bool operator!=(const Vec2 &rhs) const;

    friend std::ostream &operator<<(std::ostream &os, const Vec2 &input);
    friend Vec2 Cross(float scalar, const Vec2 &vector);
    friend Vec2 Cross(const Vec2 &vector, float scalar);

    /******* Static Stuff ********/
    static void TbType();
  };
}
