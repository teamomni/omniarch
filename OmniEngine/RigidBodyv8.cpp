/******************************************************************************
Filename: RigidBodyv8.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "RigidBody.h"
#include "Shape.h"
#include "Game.h"
#include <vector>
#include "Manifold.h"

using namespace v8;

namespace Omni
{
  static void v8_ApplyImpulse(const FunctionCallbackInfo<Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    RigidBody *body = Take<RigidBody>::FromHolder(info);
    float x = float(info[0]->NumberValue());
    float y = float(info[1]->NumberValue());

    body->ApplyLinearImpulse(x, y);
  }

  static void v8_ApplyForce(const FunctionCallbackInfo<Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    RigidBody *body = Take<RigidBody>::FromHolder(info);
    float x = float(info[0]->NumberValue());
    float y = float(info[1]->NumberValue());

    body->ApplyLinearForce(x, y);
  }

  static void v8_OnGround(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the body pointer
    RigidBody *body = Take<RigidBody>::FromHolder(info);

    bool onGround = body->onGround;

    info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), onGround));
  }

  static void v8_SetGroup(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    RigidBody *body = Take<RigidBody>::FromHolder(info);
    RigidBody::Group group = RigidBody::Group(Take<int>::FromV8(info[0]));
    
    body->SetGroup(group);
  }

  static void v8_GetGroup(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    RigidBody *body = Take<RigidBody>::FromHolder(info);

    info.GetReturnValue().Set(Integer::New(info.GetIsolate(), int(body->GetGroup())));
  }

  static void v8_Clear(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    RigidBody *body = Take<RigidBody>::FromHolder(info);

    body->velocity = Vec2(0, 0);
    body->angularVelocity = 0;
    body->totalForce = Vec2(0, 0);
    body->totalImpulse = Vec2(0, 0);
  }
  
  void RigidBody::v8_Register()
  {
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    Local<ObjectTemplate> objTemplate_temp;

    // if there's nothing in the obj template right now, make a new one
    if (objTemplate.IsEmpty())
    {
      objTemplate_temp = ObjectTemplate::New(game.isolate);
      objTemplate_temp->SetInternalFieldCount(1);
    }
    // else, get it from the object
    else
      objTemplate_temp = LocalObjectTemplate::New(game.isolate, objTemplate);

    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "IsOnGround"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_OnGround)));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "ApplyLinearImpulse"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_ApplyImpulse)));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "ApplyLinearForce"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_ApplyForce)));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "SetGroup"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_SetGroup)));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "GetGroup"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_GetGroup)));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "Clear"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_Clear)));

    objTemplate.Reset(game.isolate, objTemplate_temp);

    ExposeProperties();
  }

  v8_RegisterComponentWithEntity(RigidBody);
}