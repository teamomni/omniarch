/******************************************************************************
Filename: ParticleAddPShader.hlsl

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// Author : Juli Gregg
// Purpose: Additively Draw Particles
// Date   : Fall 2014
// Type   : Pixel Shader

//-----------------------------------//
// Textures Should Be Binded on CPU  //
//-----------------------------------//
Texture2D Texture0 : register(t0); //Opaque Texture
Texture2D Texture1 : register(t1); //Glow Texture

//-----------------------------------//
// Sample State Set on the CPU       //
//-----------------------------------//
SamplerState sampleState : register(s0);

//-----------------------------------//
// Struct Passed from Vertex Shader  //
//-----------------------------------//
struct vOutput
{
  float4 position   : SV_POSITION;  //NDC Position
  float4 color      : COLOR;        //Base Color
  float2 texcoord   : TEXCOORD;     //Texture Coordinate
  float4 gColor     : GLOW;         //Glow Color
  float  gIntensity : GINTENSITY;   //Glow Intensity
  bool   gEnabled   : GENABLED;
};

//-----------------------------------//
// Struct Passed onto Render Target  //
//-----------------------------------//
struct pOutput
{
  float4 color : SV_TARGET0;  //Render Target for Opaque Images
  float4 glow  : SV_TARGET1;  //Render Target for Glow Images
  float4 bloom : SV_TARGET2;  //Render Target for Bloom Pixels
};

//-----------------------------------//
// Main Function of Pixel Shader     //
//-----------------------------------//
pOutput PShader(vOutput input)
{
  //Struct holding Render Target Data
  pOutput output;

  //Sample from Opaque Texture
  output.bloom = input.color * Texture0.Sample(sampleState, input.texcoord);
  output.color = float4(0, 0, 0, 0);
  output.glow  = float4(0, 0, 0, 1);
  
  return output;
}
