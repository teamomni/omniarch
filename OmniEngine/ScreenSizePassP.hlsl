/******************************************************************************
Filename: ScreenSizePassP.hlsl

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


struct vInput
{
  float4 position : POSITION; //Hard Coded
  float2 texcoord : TEXCOORD; //Hard Coded
};

struct vOutput
{
  float4 position : SV_POSITION;
  float4 color : COLOR;
  float2 texcoord : TEXCOORD;
};


