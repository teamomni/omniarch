/******************************************************************************
Filename: Sprite.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Sprite.h"
#include "Game.h"
#include "RenderState.h"
#include "RigidBody.h"
#include <iostream>

namespace Omni
{
  bool Sprite::isEditor = false;
  
  void Sprite::Initialize()
  {
    checkedRigidBody = false;
    hasRigidBody = false;
  }

  //--------//
  // Update //
  //--------//
  void Sprite::Update(float dt)
  {
    if (!checkedRigidBody)
    {
      hasRigidBody = owner->HasComponent("RigidBody");
      isParallax = (owner->name == "Parallax");
      checkedRigidBody = true;
    }

    if (p_Image.empty())
      return;

    if (p_Image != lastImage)
    {
      SetImage(p_Image);
      lastImage = p_Image;
    }

    if (p_Image.empty())
      return;
    

    //std::cout << "SPRITE UPDATING" << std::endl;

    //Update UVs
    /*if (p_AnimationActive)
      UpdateUV(dt);*/
    //
    // DH TEST ME HERE!!!
    //

    float size = owner->space->camera.GetSize();
    Vec2 cPos = owner->space->camera.transform.translation;
    Vec2 window = Game::Instance().GetWindowSystem()->GetClientSize();

    float screenHalfWidth = (size * window.x / window.y) / 2.0f;
    float screenHalfHeight = size / 2.0f;

    // this is untweened
    Transform &trans = owner->transform;

    // set the last translation ONCE on the first frame
    if (!trans.gottenLastFrameTrans)
    {
      trans.UpdateLastTransformData();
      trans.gottenLastFrameTrans = true;
    }

    Vec2 finalScale = trans.scale;
    Vec2 finalTranslation = trans.translation;
    float finalRot = trans.rotation;

    // tweening time baby
    if (p_Tweening && !isParallax && !owner->space->paused && !isEditor)
    {
      float alpha = RigidBody::accumulator / RigidBody::physicsTimestep;
      float inverseAlpha = 1 - alpha;
      finalScale = trans.lastFrameScale * inverseAlpha + trans.scale * alpha;
      finalTranslation = trans.lastFrameTrans * inverseAlpha + trans.translation * alpha;

      /*if (owner->HasComponent("FollowPlayer") && owner->GetComponent("FollowPlayer")->GetProperty_Bool("active") == true)
        printf("hello");*/
    }

    // store prev frame stuff
    if (!hasRigidBody)
     trans.UpdateLastTransformData();
    
    

    /*if (image->pixelsPerUnit != 0)
    {*/
    finalScale.x *= image->dimensions.x / image->pixelsPerUnit;
    finalScale.y *= image->dimensions.y / image->pixelsPerUnit;
    //}

    finalTranslation.x -= image->offset.x * finalScale.x;
    finalTranslation.y -= image->offset.y * finalScale.y;

    float ownerXSize = finalScale.x / 2;
    float ownerYSize = finalScale.y / 2;

    float totalX = screenHalfWidth + ownerXSize;
    float totalY = screenHalfHeight + ownerYSize;

    float totalRadiusSquared = totalX * totalX + totalY * totalY;

    float separationSquared = (finalTranslation - cPos).LengthSquared();

    // if it's outside the screen, don't draw
    if (separationSquared > totalRadiusSquared)
      return;

    if (!p_Visible || !image || p_Color.a < 0.0001f)
      return;
     
    //Cap brightness at 5
    if (p_Brightness > 5)
      p_Brightness = 5;


    RenderState &currentState = Game::Instance().GetUpdateRenderState();

    bool hasBloom = owner->HasComponent("BloomEffect");
    bool hasBlur  = owner->HasComponent("BlurEffect");

    try
    {
      float bloom = float(hasBloom ? owner->GetComponent("BloomEffect")->GetProperty_Float("threshold") : 1.0);

      currentState.objects.push_back(RenderObject(image,
        finalTranslation,
        finalScale,
        finalRot,
        p_Depth + owner->space->depth,
        p_Color,
        p_UV1,
        p_UV2,
        p_FlipX,
        p_FlipY,
        owner->space->index,
        p_Glow,
        p_GlowColor,
        p_Brightness,
        hasBloom,
        float(hasBloom ? owner->GetComponent("BloomEffect")->GetProperty_Float("threshold") : 1.0),
        hasBlur ? owner->GetComponent("BlurEffect")->GetProperty_Bool("active") : false));
    }
    catch (const std::exception& ex)
    {
      std::cout << ex.what() << std::endl;
    }
  }

  void Sprite::Destroy()
  {
    ScriptableDestroy();
    Game::Instance().DiscardModulePropertyHandles(Handlet<Component>(selfPtr));
  }

  void Sprite::Register()
  {
    RegisterName("Sprite", &Game::Instance().systemContainers, PropertyDefinitionMap({
        { "image",       PropertyDefinition::String("",   offsetof(Sprite, p_Image),      true) },
        { "color",       PropertyDefinition::Color(       offsetof(Sprite, p_Color),      true) },
        { "visible",     PropertyDefinition::Bool(true,   offsetof(Sprite, p_Visible),    true) },
        { "depth",       PropertyDefinition::Float(0.0f,  offsetof(Sprite, p_Depth),      true) },
        { "flipX",       PropertyDefinition::Bool(false,  offsetof(Sprite, p_FlipX),      true) },
        { "flipY",       PropertyDefinition::Bool(false,  offsetof(Sprite, p_FlipY),      true) },
        { "glowEnabled", PropertyDefinition::Bool(false,  offsetof(Sprite, p_Glow),       true) },
        { "glowColor",   PropertyDefinition::Color(       offsetof(Sprite, p_GlowColor),  true) },
        { "brightness",  PropertyDefinition::Float(0.0f,  offsetof(Sprite, p_Brightness), true) },
        { "tweening",    PropertyDefinition::Bool(true,   offsetof(Sprite, p_Tweening),   true) }
    }), false);
  }

  std::string &Sprite::GetImage()
  {
    if (image == nullptr)
      assert(0);
    return image->name;
  }

  Image *Sprite::GetImagePointer()
  {
    return image;
  }

  void Sprite::SetImage(std::string imageName)
  {
    if (image && image->name == imageName)
      return;
    //p_CurrentFrame = 0;
    //m_timePassed = 0;
    Game &game = Game::Instance();
    std::unordered_map<std::string, Image>::iterator got = game.assets.images.find(imageName);
    if (got == game.assets.images.end())
    {
      Console::Error("Sprite::SetImage failed", "Image \"" + imageName + "\" not found!");
      if (image)
        p_Image = image->name;
      else
        p_Image = "";
    }
    else
    {
      p_UV1 = (got->second).uv[0];
      p_UV2 = (got->second).uv[1];
      //UpdateUV(1.0f / 60.f);
      image = &(got->second);
      p_Image = imageName;
      lastImage = imageName;
    }

    //UpdateUV(1.0f / 60.f);
  }

  /*void Sprite::UpdateUV(float dt)
  {
    if (p_FramesPerCol == 0 || p_FramesPerRow == 0)
    {
      std::cout << "DIVIDE BY ZERO ERROR: IMAGE::UPDATE UV" << std::endl;
      assert(0);
    }

    if (p_FramesPerCol == 1 && p_FramesPerRow == 1)
      p_CurrentFrame = 0;

    if (p_AnimationSpeed != 0)
    {
      m_timePassed += (1 / 60.0f);

      if (m_timePassed > (1.0f / p_AnimationSpeed))
        m_timePassed = 0.0f;
      else
        return;
        
    }

    float frameWidth = 1.0f / p_FramesPerRow;
    float frameHeight = 1.0f / p_FramesPerCol;

    float currentRow = (float)(p_CurrentFrame % p_FramesPerRow);
    float currentCol = (float)(p_CurrentFrame % p_FramesPerCol);


    p_UV1.x = currentRow * frameWidth;
    p_UV1.y = currentCol * frameHeight;
    p_UV2.x = (currentRow + 1) * frameWidth;
    p_UV2.y = (currentCol + 1) * frameHeight;

    //set to next frame of animation for update
    if ((p_CurrentFrame + 1) == p_FramesPerRow * p_FramesPerCol || (p_CurrentFrame + 1) > p_FramesPerRow * p_FramesPerCol)
      p_CurrentFrame = 0;
    else
      p_CurrentFrame++;
  }
  */

  

}//Namespace Omni