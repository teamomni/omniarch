/******************************************************************************
Filename: WindowSystem.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// File     :    WindowSystem.h
// Author  :     Juli Gregg
// Brief    :    Handles window and Windows messages

#pragma once

#include <windows.h>
#include <windowsx.h>
#include "Vec2.h"

namespace Omni
{

  namespace Graphics
  {
    class GFXRenderer;
  }

  class WindowSystem
  {
  public:

    // Methods
    WindowSystem(HINSTANCE hInstance_, unsigned iconID);
    ~WindowSystem(){};

    void ActivateWindow(void);

    HINSTANCE GetInstance(void) { return hInstance; };
    HWND GetWindowHandle(void) { return hWnd; };
    int GetClientWidth(void) { return ClientWidth; };
    int GetClientHeight(void) { return ClientHeight; };

    Vec2 GetFullScreenSize(void) { return Resolution; };
    Vec2 GetClientSize(void) { return Vec2(float(ClientWidth), float(ClientHeight)); };

  private:
    // Variables
    HINSTANCE hInstance;
    HWND hWnd;

    //When in windowed
    int ClientWidth;
    int ClientHeight;

    //When in fullscreen
    Vec2 Resolution;

    void SetClientSize(int w, int h) { ClientWidth = w; ClientHeight = h; };

    friend class Graphics::GFXRenderer;

  };

}