/******************************************************************************
Filename: Console.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Console.h"

namespace Omni
{
  HANDLE Console::s_conOut;
  bool Console::s_enabled = false;
}