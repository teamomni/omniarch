/******************************************************************************
Filename: SoundSystem.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


#include "stdafx.h"
#include "SoundSystem.h"
#include <iostream>
#include <assert.h>
#include "Game.h"
#include "SoundUtilities.h"

namespace Omni
{

  SoundSystem::SoundSystem()
  {

  }

  SoundSystem::~SoundSystem()
  {
    
  }

  
  //-------------------------//
  // Initiliaze FMOD System  //
  //-------------------------//
  bool SoundSystem::Initialize(void)
  {
    
    //Check if trying to initialize fmod again
    if (m_system != NULL)
    {
      Console::WriteLine("FMOD ALREADY INITIALIZED!");
      return true;
    }
    //Initialize FMOD
    else
    {
      // Create the Studio System object.
      SoundUtilities::FMODErrCheck(FMOD::Studio::System::create(&m_system));

      // Initialize FMOD Studio, (also initialize FMOD Low Level)
      SoundUtilities::FMODErrCheck(m_system->initialize(512, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, 0));

      // Grab some low level data
      m_system->getLowLevelSystem(&m_lowLevelSystem);
      m_lowLevelSystem->getMasterChannelGroup(&m_masterChannelGrp);
    }

    if (m_system == NULL)
      return false;

    Console::WriteLine("FMOD INITIALIZED!");

    return true;

  }

  //---------------------//
  // Update FMOD System  //
  //---------------------//
  void SoundSystem::Update(void)
  {
    //Updates system so sounds keep running as they should
    SoundUtilities::FMODErrCheck(m_system->update());
  }

  //-----------------------//
  // ShutDown FMOD System  //
  //-----------------------//
  void SoundSystem::ShutDown(void)
  {
    for (auto const &event : m_eventRes)
      SoundUtilities::FMODErrCheck(event.second.desc->releaseAllInstances());

    m_cueRes.clear();
    m_eventRes.clear();

    for (auto const &bank : m_bankRes)
      bank.second->unload();

    m_bankRes.clear();

    //Close and release system
    SoundUtilities::FMODErrCheck(m_system->release());

    Console::WriteLine("FMOD RELEASED");
  }
  
  //-----------------------//
  // Load Sound Bank       //
  //-----------------------//
  void SoundSystem::LoadBank(std::string filename)
  {
    if (filename.size() != 0)
    {
      auto& game = Game::Instance();

      std::string file(game.GetFileLocation().begin(), game.GetFileLocation().end());

#if (defined(_DEBUG) || defined(_RELEASE)) && !defined(_EDITOR)
      file.append("\\..\\..\\..\\OmniFMOD\\Build\\Desktop\\");
#else
#if defined(_EDITOR)
      file = Game::GetMyDocumentsDirectory().append("\\Data\\SoundBanks\\");
#else
      file.append("\\Data\\SoundBanks\\");
#endif
#endif
      
      file.append(filename);

      SoundUtilities::Bank bank;
      
      SoundUtilities::FMODErrCheck(m_system->loadBankFile(file.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &bank));

      LoadAllBankData(bank);

      m_bankRes.insert({ filename, bank });

    }
  }

  //-----------------------//
  // Release Sound Bank    //
  //-----------------------//
  void SoundSystem::UnloadBank(std::string filename)
  {
    if (filename.size() != 0)
    {
      UnloadAllBankData(m_bankRes.find(filename)->second);

      SoundUtilities::FMODErrCheck(m_bankRes.find(filename)->second->unload());

      m_bankRes.erase(filename);
    }

  }

  //
  // For DH
  //
  void SoundSystem::ReleaseAllSoundCues(void)
  {

    for (auto &event : m_eventRes)
      SoundUtilities::FMODErrCheck(event.second.desc->releaseAllInstances());

    m_cueRes.clear();
    m_eventRes.clear();


    //
    // DH : if this doesn't work uncomment the stuff below and reload the banks as well
    //

    //for (auto const &bank : m_bankRes)
    //  bank.second->unload();
    //
    //m_bankRes.clear();
  }


  SoundUtilities::SoundCue* SoundSystem::GetSoundCue(std::string eventName, std::string objName)
  {
    //If we haven't loaded this event before load it now
    if (m_eventRes.find(eventName) == m_eventRes.end())
    {
      FMOD_GUID id;

      m_eventRes.insert({ eventName, SoundUtilities::Event() });

      SoundUtilities::FMODErrCheck(m_system->lookupID(eventName.c_str(), &id));

      SoundUtilities::FMODErrCheck(m_system->getEventByID(&id, &m_eventRes[eventName].desc));

      m_eventRes[eventName].desc->getParameterCount(&m_eventRes[eventName].paramCount);

      //grab all the parameters descriptions
      for (int i = 0; i < m_eventRes[eventName].paramCount; i++)
      {
        m_eventRes[eventName].parameters.push_back(SoundUtilities::ParameterDesc());
        SoundUtilities::FMODErrCheck(m_eventRes[eventName].desc->getParameterByIndex(i, &m_eventRes[eventName].parameters.back()));
      }

    }

    //Now we have the event
    m_cueRes.insert({ objName, SoundUtilities::SoundCue() });

    m_eventRes[eventName].desc->createInstance(&m_cueRes[objName].instance);

    m_cueRes[objName].myEvent = &m_eventRes[eventName];

   
    return &m_cueRes[objName];
  }

  SoundUtilities::SoundCue* SoundSystem::GetMySoundCue(std::string objName)
  {
    return &m_cueRes[objName];
  }

  void SoundSystem::SetActive(WPARAM wparam)
  {
    if (!m_system)
      return;

    bool wActive = LOWORD(wparam) != WA_INACTIVE;

    if (!wActive)
    {
      PauseAll();
    }
    else
    {
      UnPauseAll();
    }
  }

  void SoundSystem::PauseAll()
  {
    if (!m_pauseSystem)
    {
      m_masterChannelGrp->setPaused(true);
      m_pauseSystem = true;
    }
  }

  void SoundSystem::UnPauseAll()
  {
    if (m_pauseSystem)
    {
      m_masterChannelGrp->setPaused(false);
      m_pauseSystem = false;
    }
  }


  //PRIVATE FUNCTIONS
  void SoundSystem::LoadAllBankData(SoundUtilities::Bank& bank)
  {
    //Load the data from bank
    SoundUtilities::FMODErrCheck(bank->loadSampleData());

  }

  void SoundSystem::UnloadAllBankData(SoundUtilities::Bank& bank)
  {
    SoundUtilities::FMODErrCheck(bank->unloadSampleData());
  }

} //Namespace Omni