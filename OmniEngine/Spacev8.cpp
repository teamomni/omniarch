/******************************************************************************
Filename: Spacev8.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Space.h"
#include "Entity.h"
#include "Camera.h"
#include "Game.h"

using namespace v8;

namespace Omni
{
  static void v8_CreateEntity(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    LocalObject creationObject = LocalObject::Cast(info[0]);
    Space *space = Take<Space>::FromHolder_NonComponent(info);
    Handlet<Entity> created;
    auto archetypeSymbol = String::NewFromUtf8(info.GetIsolate(), "archetype");
    auto positionSymbol = String::NewFromUtf8(info.GetIsolate(), "position");

    // if it has an archetype, create from archetype
    if (creationObject->Has(archetypeSymbol))
    {
      std::string archetypeName = Take<std::string>::FromV8(creationObject->Get(archetypeSymbol));
      std::string name = Take<std::string>::FromV8(creationObject->Get(String::NewFromUtf8(info.GetIsolate(), "name")));
      // create with archetype
      created = space->CreateEntityAtPosition(name, Vec2(0, 0), archetypeName);
    }

    // if no archetype given, just create
    else
    {
      created = space->CreateEntity(Take<std::string>::FromV8(creationObject->Get(String::NewFromUtf8(info.GetIsolate(), "name"))));
    }

    // if position given, assign given position to entity's translation
    if (creationObject->Has(positionSymbol))
    {
      // hecka long lines to get the translation
      LocalObject vector = LocalObject::Cast(creationObject->Get(positionSymbol));
      created->transform.translation.x = float(vector->Get(String::NewFromUtf8(info.GetIsolate(), "x"))->NumberValue());
      created->transform.translation.y = float(vector->Get(String::NewFromUtf8(info.GetIsolate(), "y"))->NumberValue());
    }

    // call initialize on the entity
    created->Initialize();

    // also update its prev transform stuff
    created->transform.UpdateLastTransformData();

    LocalObject createdObj = created->v8_GetObject();
    createdObj->SetInternalField(0, External::New(info.GetIsolate(), created->selfPtr));

    info.GetReturnValue().Set(createdObj);
  }

  static void v8_FindEntityByName(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Space *space = Take<Space>::FromHolder_NonComponent(info);
    std::string name = Take<std::string>::FromV8(info[0]);

    Handlet<Entity> found = space->GetEntityByName(name);

    if (found)
    {
      LocalObject foundObj = found->v8_GetObject();

      foundObj->SetInternalField(0, External::New(info.GetIsolate(), found->selfPtr));

      info.GetReturnValue().Set(foundObj);
    }
    else
      info.GetReturnValue().Set(Undefined(info.GetIsolate()));
  }

  static void v8_FindIDByName(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Space *space = Take<Space>::FromHolder_NonComponent(info);
    std::string name = Take<std::string>::FromV8(info[0]);

    Handlet<Entity> found = space->GetEntityByName(name);

    if (found)
      info.GetReturnValue().Set(Integer::New(info.GetIsolate(), int(found->id)));
    else
      info.GetReturnValue().Set(Undefined(info.GetIsolate()));
  }
  
  static void v8_FindEntitiesByName(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Space *space = Take<Space>::FromHolder_NonComponent(info);
    std::string name = Take<std::string>::FromV8(info[0]);

    // get all the entities of a name
    std::vector<Handlet<Entity> > entities = space->GetEntitiesByName(name);

    // now push them into a js array
    size_t numEnts = entities.size();
    Handle<Array> newArray = Array::New(info.GetIsolate(), numEnts);

    for (size_t i = 0; i < numEnts; ++i)
    {
      // get the v8 object and stuff the handle in there
      Handle<Object> currEnt =  entities[i]->v8_GetObject();
      currEnt->SetInternalField(0, External::New(info.GetIsolate(), entities[i]->selfPtr));
      newArray->Set(i, currEnt);
    }

    info.GetReturnValue().Set(newArray);
  }

  static void v8_GetAllEntities(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Space *space = Take<Space>::FromHolder_NonComponent(info);

    // get all the entities of a name
    std::vector<Handlet<Entity> > entities = space->GetAllEntityHandles();

    // now push them into a js array
    size_t numEnts = entities.size();
    Handle<Array> newArray = Array::New(info.GetIsolate(), numEnts);

    for (size_t i = 0; i < numEnts; ++i)
    {
      // get the v8 object and stuff the handle in there
      Handle<Object> currEnt = entities[i]->v8_GetObject();
      currEnt->SetInternalField(0, External::New(info.GetIsolate(), entities[i]->selfPtr));
      newArray->Set(i, currEnt);
    }

    info.GetReturnValue().Set(newArray);
  }

  static void v8_GetParallaxLayers(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Space *space = Take<Space>::FromHolder_NonComponent(info);
    
    int numLayers = space->parallaxLayers.size();
    Handle<Array> ret = Array::New(info.GetIsolate(), numLayers);

    // loop through all parallax layers and push them into an array
    for (int i = 0; i < numLayers; ++i)
    {
      Handle<Array> currTuple = Array::New(info.GetIsolate(), 3);
      currTuple->Set(0, String::NewFromUtf8(info.GetIsolate(), std::get<0>(space->parallaxLayers[i]).c_str()));
      currTuple->Set(1, Number::New(info.GetIsolate(), std::get<1>(space->parallaxLayers[i])));
      currTuple->Set(2, Boolean::New(info.GetIsolate(), std::get<2>(space->parallaxLayers[i])));
      ret->Set(i, currTuple);
    }

    info.GetReturnValue().Set(ret);
  }

  static void v8_GetParallaxOffset(const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Space *space = Take<Space>::FromHolder_NonComponent(info);
    if (space)
      info.GetReturnValue().Set(Number::New(info.GetIsolate(), space->parallaxOffset));
    else
      info.GetReturnValue().Set(Undefined(info.GetIsolate()));
  }

  static void v8_GetPaused(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Space *space = Take<Space>::FromHolder_NonComponent(info);

    info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), space->paused));
  }

  void Space::v8_Register()
  {
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    Local<ObjectTemplate> objTemplate_temp = ObjectTemplate::New(game.isolate);

    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "camera"), AccessorGetterCallback(v8_GetCamera), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "paused"), AccessorGetterCallback(v8_GetPaused), 0);
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "DispatchEvent"), Function::New(game.isolate, &EventDispatcher::v8_DispatchEvent<Space>));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "CreateEntity"), Function::New(game.isolate, v8_CreateEntity));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "FindEntityByName"), Function::New(game.isolate, v8_FindEntityByName));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "FindIDByName"), Function::New(game.isolate, v8_FindIDByName));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "FindEntitiesByName"), Function::New(game.isolate, v8_FindEntitiesByName));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "GetAllEntities"), Function::New(game.isolate, v8_GetAllEntities));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "GetParallaxLayers"), Function::New(game.isolate, v8_GetParallaxLayers));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "GetParallaxOffset"), Function::New(game.isolate, v8_GetParallaxOffset));

    objTemplate_temp->SetInternalFieldCount(1);

    objTemplate.Reset(game.isolate, objTemplate_temp);
  }

  void Entity::v8_GetSpace(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the entity pointer
    Entity *entity = Take<Entity>::FromHolder(info);

    // getting the space pointer
    Space *space = entity->space;

    // create an instance of the js object
    Local<Object> result = space->v8_GetObject();

    // create a wrapper for the actual pointer
    Local<External> spacePtr = External::New(info.GetIsolate(), space);

    // store the pointer in the js object
    result->SetInternalField(0, spacePtr);

    info.GetReturnValue().Set(result);
  }
}