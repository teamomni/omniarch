/******************************************************************************
Filename: Handlet.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

namespace Omni
{
  template <typename T>
  struct Handlet
  {
    T **object = nullptr;

    T *const operator->() const { return *object; }
    T *      operator->() { return *object; }
    T *const operator*() const { return object ? *object : nullptr; }
    T *      operator*() { return object? *object : nullptr; }
    Handlet<T> &operator= (T **rhs) { object = rhs; return *this; }
    Handlet<T> &operator= (const Handlet<T> &rhs) { object = rhs.object; return *this; }
    bool     operator==(const Handlet<T> &rhs) const { return rhs.object == object; }
    bool     operator==(void *rhs) const { return object == rhs; }
    bool     operator!=(void *rhs) const { return object != rhs; }
             operator bool() const { return isValid(); }
             operator void *() const { return *object; }
    bool     isValid() const { return (object != nullptr && *object != nullptr); }

    Handlet(T **object_) : object(object_) {}
    Handlet() : object(nullptr) {}
  };
}