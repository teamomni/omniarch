/******************************************************************************
Filename: SoundSystem.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "stdafx.h"
#include <unordered_map>
#include "SoundUtilities.h"


namespace Omni
{

  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // FMOD STUDIO NOTES:                                                                                          //
  //                                                                                                             //
  // BANK : file contains event metadata and sound data in the one file                                          //
  //        -Loading a bank will load all metadata, which contains information about                             //
  //         all the events, parameters, and other data needed for all events                                    //
  //         associated with that bank.                                                                          //
  //                                                                                                             //
  //  For cases where most or all of the events may play at any time,                                            //
  //  then loading calling Studio::Bank::loadSampleData to load all data up front                                //
  //  may be the best approach.                                                                                  //
  //                                                                                                             //
  //  Studio::Bank::loadSampleData             : loads all data used by events in bank                           //
  //  Studio::EventDescription::loadSampleData : loads all data used for a specific event                        //
  //  Studio::EventDescription::createInstance : creates a playable instance, loads data asynchronously          //
  //                                             data can be loaded ahead of time with above functions           //
  //                                             use Studio::EventDescription::getSampleLoadingState             //
  //                                             to check the loading status.                                    //
  //                                                                                                             //
  //  Studio::Bank::getEventCount : Retrieves the number of EventDescriptions in the bank.                       //
  //  Studio::Bank::getEventList  : Retrieves the EventDescriptions in the bank.                                 //
  //                                                                                                             //
  //  Studio::Bank::unload : Unloads the bank and all of its data.                                               //
  //  *Note : Unloading a bank will free all sample data, invalidate the events descriptions belonging           //
  //          to that bank, and destroy associated instances.                                                    //
  //                                                                                                             //
  // INSTANCES :                                                                                                 //
  //  Studio::EventDescription::getLength           : Retrieves the length of the event's timeline.              //
  //  Studio::EventDescription::isOneshot           : Retrieves the event's oneshot status,                      //
  //                                                  indicating whether the event will naturally terminate.     //
  //  Studio::EventDescription::releaseAllInstances : Releases all instances of the event.                       //
  //                                                                                                             //
  // EVENTS INSTANCE :                                                                                           //
  //  Studio::EventInstance::start            : Starts playback of the event instance.                           //
  //  Studio::EventInstance::stop             : Studio::EventInstance::stop                                      //
  //  Studio::EventInstance::setVolume        : Sets the volume level on the event instance.                     //
  //  Studio::EventInstance::getVolume        : Retrieves the volume level set by the API on the event instance. //
  //  Studio::EventInstance::setPaused        : Sets the pause state of the event instance.                      //
  //  Studio::EventInstance::getPaused        : Retrieves the pause state of the event instance.                 //
  //  Studio::EventInstance::release          : Releases the event instance.                                     //
  //  Studio::EventInstance::getPlaybackState : Retrieves the playback state of the event instance.              //
  //  *Note: There are more functions invovling Parameters. We don't need these unless Spencer wants them.       //
  //                                                                                                             //
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  class SoundSystem
  {
    public:
      
      //This is a pointer
      //typedef FMOD::Studio::EventInstance* Event;

      SoundSystem();
      ~SoundSystem();

      bool Initialize(void);
      void Update(void);
      void ShutDown(void);

      void LoadBank(std::string filename);
      void UnloadBank(std::string filename);

      void ReleaseAllSoundCues(void);

      inline int GetBankCount(void) { return m_bankRes.size(); };

      SoundUtilities::SoundCue* GetSoundCue(std::string, std::string objName);
      SoundUtilities::SoundCue* GetMySoundCue(std::string);

      void SetActive(WPARAM wparam);
      void PauseAll();
      void UnPauseAll();

    private:
      FMOD::Studio::System* m_system = nullptr;

      FMOD::System*       m_lowLevelSystem = nullptr;
      FMOD::ChannelGroup* m_masterChannelGrp = nullptr;


      std::unordered_map<std::string, SoundUtilities::Bank> m_bankRes;
      std::unordered_map<std::string, SoundUtilities::Event> m_eventRes;
      std::unordered_map<std::string, SoundUtilities::SoundCue> m_cueRes;

      bool m_pauseSystem = false;

      //Loads all events from bank
      void LoadAllBankData(SoundUtilities::Bank&);

      void UnloadAllBankData(SoundUtilities::Bank&);

  };

} //Namespace Omni