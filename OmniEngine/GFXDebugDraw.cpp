/******************************************************************************
Filename: GFXDebugDraw.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"

#include "GFXDebugDraw.h"
#include "Game.h"

//#define OLDMATH

namespace Omni
{
namespace Graphics
{

//#if _DEBUG

  static const float PI = 3.141593f;

  GFXDebugDraw::GFXDebugDraw()
  {

  }

  GFXDebugDraw::~GFXDebugDraw()
  {
    ShutDown();
  }

  void GFXDebugDraw::ShutDown(void)
  {
    ClearVectors();


  }

  void GFXDebugDraw::ClearVectors(void)
  {
    /*m_lines.clear();
    m_quads.clear();
    m_arrows.clear();*/

  }

  void GFXDebugDraw::DrawLine(Vec2 start, Vec2 end, size_t space, D3DXCOLOR color)
  {
    RenderState::Line line;

    //Middle, Rotation, Scale

    line.transform.translation = Vec2(start.Midpoint(end));

    if (start < end)
      line.transform.rotation = asinf((end.y - (start.Midpoint(end)).y) / ((start - end).Length() / 2.0f));
    else 
      line.transform.rotation = -asinf((end.y - (start.Midpoint(end)).y) / ((start - end).Length() / 2.0f));


    line.transform.scale.x = (start - end).Length();
    line.transform.scale.y = (start - end).Length();

    line.color = color; 

    line.space = space;
    //m_lines.push_back(line);

    Game::Instance().GetUpdateRenderState().lines.push_back(line);
  }

  void GFXDebugDraw::DrawPoint(Vec2 position, size_t space, D3DXCOLOR color)
  {
    RenderState::Quad quad;

    quad.transform.translation = position;

    quad.color = color;

    //Fixed Scale
    quad.transform.scale = Vec2(0.1f, 0.1f);
    //Fixed Rotation (90 degrees)
    quad.transform.rotation = PI / 4.0f;

    quad.space = space;

    //m_quads.push_back(quad);

    Game::Instance().GetUpdateRenderState().quads.push_back(quad);

  }

  void GFXDebugDraw::DrawQuad(Transform transform, size_t space, D3DXCOLOR color)
  {
    RenderState::Quad quad;

    quad.transform = transform;

    quad.color = color;

    quad.space = space;

    //m_quads.push_back(quad);
    Game::Instance().GetUpdateRenderState().quads.push_back(quad);
  }

  void GFXDebugDraw::DrawQuad(Vec2 position, Vec2 scale, float rotation, size_t space, D3DXCOLOR color)
  {
    RenderState::Quad quad;

    quad.transform.translation = position;

    quad.color = color;

    //Fixed Scale
    quad.transform.scale = scale;
    //Fixed Rotation (90 degrees)
    quad.transform.rotation = rotation;

    quad.space = space;

    //m_quads.push_back(quad);
    Game::Instance().GetUpdateRenderState().quads.push_back(quad);
  }

  void GFXDebugDraw::DrawArrow(Vec2 start, Vec2 dirNormal, size_t space, D3DXCOLOR color)
  {
    RenderState::Line line;

    Vec2 Offset(0.5f, 0.0f);


    line.transform.rotation = atanf(dirNormal.y / dirNormal.x);


    Offset = Matrix3(1.0f, 1.0f) * Matrix3(line.transform.rotation) * Offset;

    if (dirNormal < Vec2(0.0f, 0.0f))
      line.transform.translation = start - Offset;
    else
      line.transform.translation = start + Offset;


    line.transform.scale = Vec2(1.0f, 1.0f);

    line.color = color;

    line.space = space;

    //m_lines.push_back(line);
    Game::Instance().GetUpdateRenderState().lines.push_back(line);


    RenderState::Arrow arrow;

    if (dirNormal < Vec2(0.0f, 0.0f))
    {
      arrow.transform.translation = start - Offset - Offset;
      arrow.transform.rotation = PI - (PI / 4.0f - line.transform.rotation);
    }
    else
    {
      arrow.transform.translation = start + Offset + Offset;
      arrow.transform.rotation = -PI / 4.0f + line.transform.rotation;
    }

    arrow.transform.scale = Vec2(0.20f, 0.20f);

    arrow.color = color;

    arrow.space = space;

    //m_arrows.push_back(arrow);
    Game::Instance().GetUpdateRenderState().arrows.push_back(arrow);

    
  }

  void GFXDebugDraw::StartDrawDebug(GFXManager &gfxManager, RenderState &renderState)
  {
    BeginDrawDebug(gfxManager);
    DrawDebug(gfxManager, renderState);
    EndDebugDraw(gfxManager);
  }

  void GFXDebugDraw::BeginDrawDebug(GFXManager &gfxManager)
  {
    //Set Topology
    gfxManager.m_renderer->SetPrimitiveTopology(PrimitiveTopology::PRIMITIVE_TOPOLOGY_LINELIST);
    gfxManager.m_renderer->BindBackBuffer();
    gfxManager.m_renderer->BindVertexShader(gfxManager.m_debugHandles[DebugVShader]);
    gfxManager.m_renderer->BindPixelShader(gfxManager.m_debugHandles[DebugPShader]);

  }

  void GFXDebugDraw::DrawDebug(GFXManager &gfxManager, RenderState &renderState)
  {
    //For Debug Lines
    for (RenderState::Line &line : renderState.lines)
    {
      m_instanceLineData.push_back(CreateInstance(line));
    }

    //For debug quads/points
    for (RenderState::Quad &quad : renderState.quads)
    {
      m_instanceQuadData.push_back(CreateInstance(quad));
    }

    //For debug triangles/arrows
    for (RenderState::Arrow &arrow : renderState.arrows)
    {
      m_instanceArrowData.push_back(CreateInstance(arrow));
    }

    //Clean up for new frame
    //ClearVectors();

    //Create buffer for lines
    if (m_instanceLineData.size() != 0)
      gfxManager.m_renderer->CreateInstanceBuffer(m_instanceBuffer[0], &m_instanceLineData[0], sizeof(debugInstance) * m_instanceLineData.size());

    //Create buffer for quads
    if (m_instanceQuadData.size() != 0)
      gfxManager.m_renderer->CreateInstanceBuffer(m_instanceBuffer[1], &m_instanceQuadData[0], sizeof(debugInstance) * m_instanceQuadData.size());


    //Create Arrow
    if (m_instanceArrowData.size() != 0)
      gfxManager.m_renderer->CreateInstanceBuffer(m_instanceBuffer[2], &m_instanceArrowData[0], sizeof(debugInstance) * m_instanceArrowData.size());

  }

  void GFXDebugDraw::EndDebugDraw(GFXManager &gfxManager)
  {
    ///////////////////////////////////////////////////////////////
    // Quad
    ///////////////////////////////////////////////////////////////
    if (m_instanceQuadData.size() != 0)
    {
      //Bind the Instance Buffer
      gfxManager.m_renderer->BindVertexBuffer(m_instanceBuffer[1], sizeof(debugInstance), 1);

      //Bind Index Buffer
      gfxManager.m_renderer->BindIndexBuffer(gfxManager.m_debugHandles[DebugIBuffer_Quad]);

      //Bind Texture
      Game& game = Game::Instance();
      gfxManager.m_renderer->BindTexture(0, game.assets.images["_DebugSprite_full"].texture);

      //Draw
      gfxManager.m_renderer->DrawIndexedInstanced(8, m_instanceQuadData.size());

      m_instanceQuadData.clear();
    }
    ///////////////////////////////////////////////////////////////
    // Lines
    ///////////////////////////////////////////////////////////////
    if (m_instanceLineData.size() != 0)
    {
      //Bind the Instance Buffer
      gfxManager.m_renderer->BindVertexBuffer(m_instanceBuffer[0], sizeof(debugInstance), 1);

      //Bind Index Buffer
      gfxManager.m_renderer->BindIndexBuffer(gfxManager.m_debugHandles[DebugIBuffer_Line]);

      //Bind Texture
      Game& game = Game::Instance();
      gfxManager.m_renderer->BindTexture(0, game.assets.images["_DebugSprite_full"].texture);

      //Draw
      gfxManager.m_renderer->DrawIndexedInstanced(2, m_instanceLineData.size());

      m_instanceLineData.clear();
    }

    ///////////////////////////////////////////////////////////////
    // Arrow
    ///////////////////////////////////////////////////////////////
    if (m_instanceArrowData.size() != 0)
    {
      //Bind the instance Buffer
      gfxManager.m_renderer->BindVertexBuffer(m_instanceBuffer[2], sizeof(debugInstance), 1);

      //Bind Index Buffer
      gfxManager.m_renderer->BindIndexBuffer(gfxManager.m_debugHandles[DebugIBuffer_Tri]);

      //Bind Texture
      Game& game = Game::Instance();
      gfxManager.m_renderer->BindTexture(0, game.assets.images["_DebugSprite_full"].texture);

      //Draw
      gfxManager.m_renderer->DrawIndexedInstanced(6, m_instanceArrowData.size());

      m_instanceArrowData.clear();
    }


    for (unsigned i = 0; i < 3; i++)
      gfxManager.m_renderer->Release(m_instanceBuffer[i]);

    //Bind original Index Buffer for regular drawing
    gfxManager.m_renderer->BindIndexBuffer(gfxManager.m_handles[DefaultIBuffer]);

  }

  debugInstance GFXDebugDraw::CreateInstance(RenderState::Line& currentLine)
  {
    debugInstance instance;

    instance.Color = currentLine.color;

    instance.X = currentLine.transform.translation.x;
    instance.Y = currentLine.transform.translation.y;

    instance.A = currentLine.transform.scale.x;
    instance.B = currentLine.transform.scale.y;

    instance.THETA = currentLine.transform.rotation;


    Game& game = Game::Instance();
    if (game.assets.images.find("_DebugSprite_full") == game.assets.images.end())
    {
      std::cout << "Can't Find Debug Image" << std::endl;
      assert(0);
    }

    //instance.U1 = game.assets.images["_DebugSprite_full"].uv[0].x;
    //instance.V1 = game.assets.images["_DebugSprite_full"].uv[0].y;
    //instance.U2 = game.assets.images["_DebugSprite_full"].uv[1].x;
    //instance.V2 = game.assets.images["_DebugSprite_full"].uv[1].y;

    instance.Camera = currentLine.space;

    return instance;

  }

  debugInstance GFXDebugDraw::CreateInstance(RenderState::Quad& currentQuad)
  {
    debugInstance instance;

    instance.Color = currentQuad.color;

    instance.X = currentQuad.transform.translation.x;
    instance.Y = currentQuad.transform.translation.y;

    instance.A = currentQuad.transform.scale.x;
    instance.B = currentQuad.transform.scale.y;

    instance.THETA = currentQuad.transform.rotation;

    Game& game = Game::Instance();
    if (game.assets.images.find("_DebugSprite_full") == game.assets.images.end())
    {
      std::cout << "Can't Find Debug Image" << std::endl;
      assert(0);
    }

    //instance.U1 = game.assets.images["_DebugSprite_full"].uv[0].x;
    //instance.V1 = game.assets.images["_DebugSprite_full"].uv[0].y;
    //instance.U2 = game.assets.images["_DebugSprite_full"].uv[1].x;
    //instance.V2 = game.assets.images["_DebugSprite_full"].uv[1].y;

    instance.Camera = currentQuad.space;


    return instance;
  }

  debugInstance GFXDebugDraw::CreateInstance(RenderState::Arrow& currentArrow)
  {
    debugInstance instance;

    instance.Color = currentArrow.color;

    instance.X = currentArrow.transform.translation.x;
    instance.Y = currentArrow.transform.translation.y;

    instance.A = currentArrow.transform.scale.x;
    instance.B = currentArrow.transform.scale.y;

    instance.THETA = currentArrow.transform.rotation;

    Game& game = Game::Instance();
    if (game.assets.images.find("_DebugSprite_full") == game.assets.images.end())
    {
      std::cout << "Can't Find Debug Image" << std::endl;
      assert(0);
    }

    //instance.U1 = game.assets.images["_DebugSprite_full"].uv[0].x;
    //instance.V1 = game.assets.images["_DebugSprite_full"].uv[0].y;
    //instance.U2 = game.assets.images["_DebugSprite_full"].uv[1].x;
    //instance.V2 = game.assets.images["_DebugSprite_full"].uv[1].y;

    instance.Camera = currentArrow.space;

    return instance;

  }

//#endif

} //Namespace Graphics
} //Namespace Omni

