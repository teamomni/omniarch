/******************************************************************************
Filename: Sprite.h

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#pragma once

#include "stdafx.h"
#include "Component.h"
#include "Image.h"

namespace Omni
{

//#pragma pack()
  class Sprite : public SystemComponent<Sprite>
  {
  public:
    ~Sprite() { ScriptableDestroy(); }

    virtual void Initialize();
    virtual void Update(float dt);
    virtual void Destroy();

    static void Register();

    std::string &GetImage();
    Image *Sprite::GetImagePointer();
    void SetImage(std::string imageName);

    Property::String p_Image = "";
    std::string lastImage = "";

    static void v8_Register();

    Property::Bool p_Visible = true;
    Property::Bool p_FlipX = false;
    Property::Bool p_FlipY = false;

    Property::Float p_Depth = 0.0f;

    Property::Vec2 p_UV1 = Vec2(0, 0);
    Property::Vec2 p_UV2 = Vec2(1, 1);
    
    Property::Color p_Color = D3DXCOLOR(1, 1, 1, 1);

    Property::Bool p_Tweening = true;

    //
    // TAG: GLOW
    //
    Property::Bool p_Glow = false;
    Property::Color p_GlowColor = D3DXCOLOR(1, 1, 1, 1);
    Property::Float p_Brightness = 0;

    bool offScreen;
    bool hasRigidBody;
    bool checkedRigidBody;
    bool isParallax;
    static bool isEditor;

  private:
    Image *image = nullptr;

    //float m_timePassed = 0.0f;

    void UpdateUV(float);
 
  };

}
