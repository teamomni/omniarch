/******************************************************************************
Filename: Animatorv8.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Animator.h"
#include "Game.h"

using namespace v8;

namespace Omni
{
  static void v8_Set(const FunctionCallbackInfo<Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Animator *animator = Take<Animator>::FromHolder(info);

    Handle<Value> message(info[0]);
    String::Utf8Value value(message);

    animator->Set(*value);
  }

  static void v8_Queue(const FunctionCallbackInfo<Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Animator *animator = Take<Animator>::FromHolder(info);

    Handle<Value> message(info[0]);
    String::Utf8Value value(message);

    animator->Queue(*value);
  }

  static void v8_Clear(const FunctionCallbackInfo<Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Animator *animator = Take<Animator>::FromHolder(info);

    animator->Clear();
  }

  static void v8_GetAnimationDone(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the animator pointer
    Animator *animator = Take<Animator>::FromHolder(info);

    bool animationDone = animator->AnimationDone();

    info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), animationDone));
  }

  static void v8_GetAnimationCount(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the animator pointer
    Animator *animator = Take<Animator>::FromHolder(info);

    int animationCount = int(animator->AnimationCount());

    info.GetReturnValue().Set(Integer::New(info.GetIsolate(), animationCount));
  }

  static void v8_GetAnimatorPoints(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the animator pointer
    Animator *animator = Take<Animator>::FromHolder(info);

    // creating a new array of the appropriate size
    Frame *currentFrame = animator->CurrentFrame();
    if (currentFrame)
    {
      unsigned framePointSize = currentFrame->points.size();
      Handle<Array> ret = Array::New(info.GetIsolate(), framePointSize);

      // copy the data over to the array
      for (unsigned i = 0; i < framePointSize; ++i)
      {
        // make a new js object and store the vec2 pointer in there
        Handle<Object> vec2Instance = Object::New(info.GetIsolate());
        vec2Instance->Set(String::NewFromUtf8(info.GetIsolate(), "x"), Number::New(info.GetIsolate(), currentFrame->points[i].x));
        vec2Instance->Set(String::NewFromUtf8(info.GetIsolate(), "y"), Number::New(info.GetIsolate(), currentFrame->points[i].y));

        ret->Set(i, vec2Instance);
      }

      // return the array
      info.GetReturnValue().Set(ret);
    }

    // return empty array
    Handle<Array> ret = Array::New(info.GetIsolate(), 0);
    info.GetReturnValue().Set(ret);
  }

  static void v8_GetAnimatorFrame(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the animator pointer
    Animator *animator = Take<Animator>::FromHolder(info);

    // return the current frame
    info.GetReturnValue().Set(Integer::New(info.GetIsolate(), animator->currentFrame));
  }

  static void v8_AnimatorHasFlag(const FunctionCallbackInfo<Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Animator *animator = Take<Animator>::FromHolder(info);

    Local<Value> message(info[0]);
    String::Utf8Value value(message);

    info.GetReturnValue().Set(Boolean::New(info.GetIsolate(), animator->CurrentFrame()->HasFlag(*value)));
  }

  static void v8_GetAnimatorFlags(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value> &info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    // unwrapping and getting the animator pointer
    Animator *animator = Take<Animator>::FromHolder(info);

    // creating a new array of the appropriate size
    Frame *currentFrame = animator->CurrentFrame();
    if (currentFrame)
    {
      unsigned frameFlagsSize = currentFrame->flags.size();
      Handle<Array> ret = Array::New(info.GetIsolate(), int(frameFlagsSize));

      // copy the data over to the array
      for (unsigned i = 0; i < frameFlagsSize; ++i)
      {
        ret->Set(i, String::NewFromUtf8(info.GetIsolate(), currentFrame->flags[i].c_str()));
      }

      // return the array
      info.GetReturnValue().Set(ret);
    }
    else
    {
      // return empty array
      Handle<Array> ret = Array::New(info.GetIsolate(), 0);
      info.GetReturnValue().Set(ret);
    }
  }

  void Animator::v8_Register()
  {
    auto &game = Game::Instance();

    Locker locker(game.isolate);
    Isolate::Scope isolate_scope(game.isolate);
    HandleScope handleScope(game.isolate);

    Context::Scope contextScope(Local<Context>::New(game.isolate, game.context));

    Local<ObjectTemplate> objTemplate_temp;

    // if there's nothing in the obj template right now, make a new one
    if (objTemplate.IsEmpty())
    {
      objTemplate_temp = ObjectTemplate::New(game.isolate);
      objTemplate_temp->SetInternalFieldCount(1);
    }
    // else, get it from the object
    else
      objTemplate_temp = LocalObjectTemplate::New(game.isolate, objTemplate);

    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "animationDone"), AccessorGetterCallback(v8_GetAnimationDone), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "animationCount"), AccessorGetterCallback(v8_GetAnimationCount), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "points"), AccessorGetterCallback(v8_GetAnimatorPoints), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "flags"), AccessorGetterCallback(v8_GetAnimatorFlags), 0);
    objTemplate_temp->SetAccessor(String::NewFromUtf8(game.isolate, "frame"), AccessorGetterCallback(v8_GetAnimatorFrame), 0);

    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "Set"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_Set)));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "Queue"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_Queue)));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "Clear"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_Clear)));
    objTemplate_temp->Set(String::NewFromUtf8(game.isolate, "HasFlag"), FunctionTemplate::New(game.isolate, FunctionCallback(v8_AnimatorHasFlag)));

    objTemplate.Reset(game.isolate, objTemplate_temp);

    ExposeProperties();
  }

  v8_RegisterComponentWithEntity(Animator);
}