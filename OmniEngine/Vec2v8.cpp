/******************************************************************************
Filename: Vec2v8.cpp

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

#include "stdafx.h"
#include "Vec2.h"
#include "Game.h"

using namespace v8;

#define v8_Vec2Gettor(target) \
static void v8_Vec2Get_##target(v8::Local<v8::String> property, const v8::FunctionCallbackInfo<v8::Value>& info)\
{\
  using namespace v8; \
  Locker locker(info.GetIsolate()); \
  Isolate::Scope isolate_scope(info.GetIsolate()); \
  HandleScope handleScope(info.GetIsolate()); \
  Vec2 *vector = Take<Vec2>::FromHolder_NonComponent(info); \
  double f = (double)vector->target; \
  Local<Value> number = Number::New(info.GetIsolate(), f); \
  info.GetReturnValue().Set(number); \
}

#define v8_Vec2Settor(target) \
static void v8_Vec2Set_##target(Local<String> property, Local<Value> value, const FunctionCallbackInfo<Value>& info)\
{\
  using namespace v8; \
  Locker locker(info.GetIsolate()); \
  Isolate::Scope isolate_scope(info.GetIsolate()); \
  HandleScope handleScope(info.GetIsolate()); \
  Vec2 *vector = Take<Vec2>::FromHolder_NonComponent(info); \
  vector->target = float(value->NumberValue());\
}

namespace Omni
{
  extern v8::Isolate *g_Isolate;

  v8_Vec2Gettor(x);
  v8_Vec2Gettor(y);
  v8_Vec2Settor(x);
  v8_Vec2Settor(y);

  static void v8_Vec2Constructor(const FunctionCallbackInfo<Value>& args)
  {
    Locker locker(args.GetIsolate());
    Isolate::Scope isolate_scope(args.GetIsolate());
    HandleScope handleScope(args.GetIsolate());

    // create a js version and return it
    Handle<Object> self = args.Holder();

    args.GetReturnValue().Set(self);
  }


  static void y_settor(Local<String> property, Local<Value> value, const FunctionCallbackInfo<Value>& info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    Vec2 *vector = static_cast<Vec2 *>(wrap->Value());
    vector->y = float(value->NumberValue());
  }

  static void x_settor(Local<String> property, Local<Value> value, const FunctionCallbackInfo<Value>& info)
  {
    Locker locker(info.GetIsolate());
    Isolate::Scope isolate_scope(info.GetIsolate());
    HandleScope handleScope(info.GetIsolate());

    Local<Object> self = info.Holder();
    Local<External> wrap = Local<External>::Cast(self->GetInternalField(0));
    Vec2 *vector = static_cast<Vec2 *>(wrap->Value());
    vector->x = float(value->NumberValue());
  }

  void Game::v8_RegisterVec2(Handle<ObjectTemplate> globalTemplate)
  {
    Locker locker(isolate);
    Isolate::Scope isolate_scope(isolate);
    HandleScope handleScope(isolate);

    Handle<FunctionTemplate> local_Vec2Template = FunctionTemplate::New(isolate, FunctionCallback(v8_Vec2Constructor));
    local_Vec2Template->SetClassName(String::NewFromUtf8(isolate, "Vector2"));

    Handle<ObjectTemplate> vec2_instance_template = local_Vec2Template->InstanceTemplate();

    // create space for the c++ pointer
    vec2_instance_template->SetInternalFieldCount(1);

    vec2_instance_template->SetAccessor(String::NewFromUtf8(isolate, "x"), AccessorGetterCallback(v8_Vec2Get_x), AccessorSetterCallback(v8_Vec2Set_x));
    vec2_instance_template->SetAccessor(String::NewFromUtf8(isolate, "y"), AccessorGetterCallback(v8_Vec2Get_y), AccessorSetterCallback(v8_Vec2Set_y));

    globalTemplate->Set(String::NewFromUtf8(isolate, "Vector"), local_Vec2Template);
  }
}