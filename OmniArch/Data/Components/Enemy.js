/******************************************************************************
Filename: Enemy.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var Enemy = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.maxHealth = 20;
  this.currentHealth = 1;

  this.flashDuration = 0.1;
  this.flashTimer = 0;
  this.flashing = false;
  
  this.generatorLinkID = 59;

  this.state = EnemyStates.Idle;
};

var EnemyStates = {
  Idle : 0,
  Hostile : 1,
  Defeated : 2,
  Deactivated : 3
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Enemy.Create = function() {
  // required function; do not remove
  return new Enemy(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

Enemy.properties = {
  "maxHealth": Property(Type.Float, 20),
  "connectDamage" : Property(Type.Bool, true),
  "colorUpdate" : Property(Type.Bool, true),
  "damageSound": Property(Type.String_, "event:/robot-damageget")
};

//////////////////
//  INITIALIZE  //
//////////////////

Enemy.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  this.currentHealth = this.maxHealth;

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "changeState", owner, this.changeState);
  // only connect damage if property is true
  if (this.connectDamage)
    Omni.Connect(this, "entity", "damageEvent", owner, this.onDamage);

  owner.Sprite.brightness = 1.25;
  
  owner.SoundEmitter.looping = false;
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
Enemy.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  var healthPercentage = this.currentHealth / this.maxHealth;
  
  if (Omni.Debugging && owner.OnScreen() && CheckInput("destroy_enemies_on_screen"))
    this.Die(owner);
  
  if (this.flashing) {
    this.flashTimer += updateEvent.dt;
    if (this.flashTimer >= this.flashDuration) {
      this.flashing = false;
      this.flashTimer = 0;
    }
  }
  
  if (this.colorUpdate) {
    owner.Sprite.glowColor.g = Ease.Easy(owner.Sprite.glowColor.g, 0, 0.1);
    owner.Sprite.glowColor.b = Ease.Easy(owner.Sprite.glowColor.b, 0, 0.1);
  }

  if (owner.transform.translation.y < -10 || this.state == EnemyStates.Defeated)
  {
    this.Die(owner);
  }
  
  if (owner.OnScreen() && CheckInput("damage_enemies_on_screen"))
    owner.DispatchEvent("damageEvent", {damage : 10});
};

Enemy.prototype.onDamage = function() {
  var owner = arguments[0];
  var damageEvent = arguments[1];
  
  this.currentHealth -= damageEvent.damage;
  this.flashing = true;
  var sprite = owner.Sprite;
  
  sprite.glowColor.g = 1;
  sprite.glowColor.b = 1;
  sprite.brightness = 1.25;
  

  var soundEvent = new SoundEvent(this.damageSound);
  owner.space.DispatchEvent("playSound", soundEvent);

  if (this.currentHealth <= 0)
  {
    if (this.state != EnemyStates.Defeated)
    {
      var stateEvent = new StateEvent(EnemyStates.Defeated);
      owner.DispatchEvent("changeState", stateEvent);
    }
  }
};

Enemy.prototype.changeState = function() {
  var owner = arguments[0];
  var stateEvent = arguments[1];

  if (stateEvent.newState == EnemyStates.Idle) {
    this.state = stateEvent.newState;
  }

  if (stateEvent.newState == EnemyStates.Hostile) {
    this.state = stateEvent.newState;
  }

  if (stateEvent.newState == EnemyStates.Defeated) {
    this.state = EnemyStates.Defeated;
  }

};

Enemy.prototype.Die = function(owner) {
  owner.space.DispatchEvent("EnemyDeath", {});
  owner.DispatchEvent("Death", {});

  var position = new Vec2(owner.transform.translation.x, owner.transform.translation.y + 1);
  if (owner.HasComponent("ShieldEnemy"))
    position.y -= 1.5;
  var explosion = owner.space.CreateEntity({name : "ExplosionSmall", position : position, archetype : "ExplosionSmall"});
  explosion.Animator.Set("explosion_small");

  
  var pos = owner.transform.translation;
  if (owner.HasComponent("ShieldEnemy"))
    owner.transform.translation.y -= 0.8;
  
  var explosion2 = owner.space.CreateEntity({name : "Explosion", position : pos, archetype : "CartoonExplosion"});
  explosion2.ParticleEmitter.active = true;
  explosion2.GetComponent("TimedDeath").active = true;

  // create explosions of nuts and bolts!
  Utilities.CreateNutsAndBolts(6, 0, position);
  
  // SFX
  var soundEvent = new SoundEvent("event:/enemy-defeat");
  owner.space.DispatchEvent("playSound", soundEvent);

  
  owner.Destroy();
};

var StateEvent = function (state) {
  this.newState = state;
};
