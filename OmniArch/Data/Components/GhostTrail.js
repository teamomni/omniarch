/******************************************************************************
Filename: GhostTrail.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var GhostTrail = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.fadeSpeed = 14;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

GhostTrail.Create = function() {
  // required function; do not remove
  return new GhostTrail(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

GhostTrail.properties = {

};

//////////////////
//  INITIALIZE  //
//////////////////

GhostTrail.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
GhostTrail.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (owner.space.paused)
    return; // needed?

  owner.Sprite.color.a -= this.fadeSpeed * updateEvent.dt;
  owner.Sprite.depth += 0.01;
  if (owner.Sprite.color.a <= 0)
    owner.Destroy();

};
