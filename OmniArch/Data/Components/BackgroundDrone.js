/******************************************************************************
Filename: BackgroundDrone.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var BackgroundDrone = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.onScreen  = false; // Should always spawn off screen
  this.layer = 12; // aka depth (but in parallax layers)
  this.moveSpeed = -1;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

BackgroundDrone.Create = function() {
  // required function; do not remove
  return new BackgroundDrone(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

BackgroundDrone.properties = {
  
};

//////////////////
//  INITIALIZE  //
//////////////////

BackgroundDrone.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);

  // Scale
  owner.transform.scale.x *= 0.75;
  owner.transform.scale.y *= 0.75;

  owner.Sprite.depth = this.layer;
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
BackgroundDrone.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  var followCamera = owner.GetComponent("FollowCamera");
  
  followCamera.xOffset += this.moveSpeed * updateEvent.dt;

  if ((this.moveSpeed > 0 && followCamera.xOffset > 6) || (this.moveSpeed < 0 && followCamera.xOffset < -6))
    owner.Destroy();
};
