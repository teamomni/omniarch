/******************************************************************************
Filename: Checkpoint.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var Checkpoint = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.chargeBooth = 0;

  this.activated = false;

};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Checkpoint.Create = function() {
  // required function; do not remove
  return new Checkpoint(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

Checkpoint.properties = {

};

//////////////////
//  INITIALIZE  //
//////////////////

Checkpoint.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  //Omni.Connect(this, "space", "PlayerDeath", owner, 0, this.ReleaseCamera);
  
  Omni.Connect(this, "entity", "onCollisionStart", owner, this.onCollisionStart);
  Omni.Connect(this, "entity", "onCollisionEnd", owner, this.onCollisionEnd);
  
  // create the actual checkpoint
  this.chargeBooth = owner.space.CreateEntity(
      {"name" : "checkpoint_ChargeBooth", "archetype" : "ChargeBooth", 
      "position" : {"x" : owner.transform.translation.x, "y" : owner.transform.translation.y}});
      
  // make my own sprite invisible
  owner.Sprite.visible = false;


  owner.SoundEmitter.soundCue = "event:/checkpoint";
   
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
Checkpoint.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
};


Checkpoint.prototype.onCollisionStart = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];
  
  // if we collide with the player, change color to green
  if (collisionEvent.otherEntity.name == "Player") {
    collisionEvent.otherEntity.GetComponent("PlayerLogic").recharging = true;
    owner.ParticleEmitter.active = true;

    if (!this.activated)
    {
      this.chargeBooth.Sprite.glowColor.r = 0.5;
      this.chargeBooth.Sprite.glowColor.b = 0.5;

      var player = owner.space.FindEntityByName("Player");
      player.GetComponent("PlayerLogic").spawnPoint.set(owner.transform.translation.x, owner.transform.translation.y);
      
      // do not play if first checkpoint
      if (owner.name != "firstCheckpoint")
        owner.SoundEmitter.startPlaying = true;
        
      this.activated = true;
    }
    
  }
};

Checkpoint.prototype.onCollisionEnd = function() {

  var owner = arguments[0];
  var collisionEvent = arguments[1];

  if (collisionEvent.otherEntity === undefined)
    return;
  
  // if we collide with the player, change color to green
  if (collisionEvent.otherEntity.name == "Player") {
    //this.chargeBooth.Sprite.glowColor.r = 1;
    //this.chargeBooth.Sprite.glowColor.b = 1;
    collisionEvent.otherEntity.GetComponent("PlayerLogic").recharging = false;
    owner.ParticleEmitter.active = false;
  }
};