/******************************************************************************
Filename: Bullet.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var Bullet = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.status = Status.Hostile;

  this.lifetime = 8;
  this.lifeTimer = 0;
  this.toDestroy = false;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Bullet.Create = function() {
  // required function; do not remove
  return new Bullet(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

Bullet.properties = {

};

//////////////////
//  INITIALIZE  //
//////////////////

Bullet.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "onHitStart", owner, this.onHitStart);
  Omni.Connect(this, "entity", "onCollisionStart", owner, this.onCollisionStart);

  owner.Animator.Set("plasmaball");
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
Bullet.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  this.lifeTimer += updateEvent.dt;

  this.CheckSlowMotion(owner);

  if (this.toDestroy || this.lifeTimer > this.lifetime)
    owner.Destroy();
};

Bullet.prototype.CheckSlowMotion = function(owner)
{
  var player = owner.space.FindEntityByName("Player");
  var distance = new Vec2(owner.transform.translation.x, owner.transform.translation.y);
  distance = distance.sub(distance, player.transform.translation);

  //if (distance.length() < 1.75)
    //Utilities.Slow(0.5);
};

Bullet.prototype.ChangeStatus = function (owner, status) {
  this.status = status;

  if (status == Status.Hostile) {
    owner.Sprite.glowColor.r = 0.86;
    owner.Sprite.glowColor.g = 0;
    owner.Sprite.glowColor.b = 0;
  }
  if (status == Status.Friendly) {
    owner.Sprite.glowColor.r = 0.41;
    owner.Sprite.glowColor.g = 0.99;
    owner.Sprite.glowColor.b = 0.86;
  }

  owner.RigidBody.velocity.x *= -3;
  owner.RigidBody.velocity.y *= -3;
};

Bullet.prototype.onHitStart = function() {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  if (hitEvent.otherType == HitBoxType.Damage && hitEvent.otherEntity.GetComponent("PlayerLogic")) {
    this.ChangeStatus(owner, Status.Friendly);
  }

  var damageEvent = new DamageEvent(10);

  if (hitEvent.myType == HitBoxType.Damage && hitEvent.otherType == HitBoxType.Body) {
    // Player
    if (hitEvent.otherEntity.GetComponent("PlayerLogic") && owner.GetComponent("Bullet").status == Status.Hostile) {
      var explosion = owner.space.CreateEntity({name : "BulletParticle", position : owner.transform.translation, archetype : "BulletParticle"});
      hitEvent.otherEntity.DispatchEvent("damageEvent", damageEvent);
      owner.Destroy();
    }
    if (hitEvent.otherEntity.GetComponent("Enemy") && owner.GetComponent("Bullet").status == Status.Friendly) {
      hitEvent.otherEntity.DispatchEvent("damageEvent", damageEvent);
      owner.Destroy();
    }
  }
};

Bullet.prototype.onCollisionStart = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];
  
  this.toDestroy = true;

  //owner.Destroy();
  //owner.transform.translation.y -= 100;
};
