/******************************************************************************
Filename: FollowCamera.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var FollowCamera = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  this.xOffset = 0;
  this.yOffset = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

FollowCamera.Create = function() {
  // required function; do not remove
  return new FollowCamera(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

FollowCamera.properties = {
  "xOffset": Property(Type.Float, 0),
  "yOffset": Property(Type.Float, 0)
};

//////////////////
//  INITIALIZE  //
//////////////////

FollowCamera.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onFrameUpdate", owner, 0, this.onUpdate);

  if (owner.HasComponent("Sprite"))
    owner.Sprite.tweening = false;
  
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
FollowCamera.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  var player = owner.space.FindEntityByName("Player");
    
  // Follow Camera
  owner.transform.translation.x = owner.space.camera.transform.translation.x + this.xOffset;
  owner.transform.translation.y = owner.space.camera.transform.translation.y + this.yOffset;
};
