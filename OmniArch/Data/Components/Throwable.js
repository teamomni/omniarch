/******************************************************************************
Filename: Throwable.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var Throwable = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.pickupTime = 0.3;

  this.thrown = false;
  this.throwTimer = 0;
  this.gravityDelay = 0.5;
  this.pickedUp = false;
  this.damage = 10;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Throwable.Create = function() {
  // required function; do not remove
  return new Throwable(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

Throwable.properties = {

};

//////////////////
//  INITIALIZE  //
//////////////////

Throwable.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "pickup", owner, this.onPickup);
  Omni.Connect(this, "entity", "throw", owner, this.onThrow);
  Omni.Connect(this, "entity", "drop", owner, this.onDrop);
  Omni.Connect(this, "entity", "onHitStart", owner, this.onHitStart);
  Omni.Connect(this, "entity", "selectEvent", owner, this.onSelect);
  Omni.Connect(this, "entity", "deselectEvent", owner, this.onDeselect);
  Omni.Connect(this, "entity", "Death", owner, this.onDeath);

  owner.Animator.Set(owner.Sprite.image);
  owner.RigidBody.SetGroup(CollisionGroup.DetectAllButDefault);

  owner.GetComponent("FollowPlayer").active = false;
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
Throwable.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (this.pickedUp) {
    var rigidBody = owner.RigidBody;
    rigidBody.angularVelocity = 0;
    rigidBody.velocity.x = 0;
    rigidBody.velocity.y = 0;
  }
  
  if (this.thrown) {
    this.throwTimer += updateEvent.dt;
  }

  if (this.thrown && this.throwTimer > this.gravityDelay) {
    owner.RigidBody.gravityScale = 10;
    owner.DispatchEvent("ThrownFar", {});
    this.thrown = false;
  }

};

Throwable.prototype.onPickup = function() {
  var owner = arguments[0];

  owner.GetComponent("FollowPlayer").active = true;
  owner.RigidBody.gravityScale = 0;
  owner.RigidBody.angularVelocity = 0;
  owner.transform.rotation = 0;
  this.throwTimer = 0;
  owner.RigidBody.rotationLocked = true;
  owner.DispatchEvent("PickedUp", {});
  owner.RigidBody.SetGroup(CollisionGroup.GrabedObject);
  this.pickedUp = true;
};

Throwable.prototype.onThrow = function() {
  var owner = arguments[0];
  
  this.thrown = true;
  owner.GetComponent("FollowPlayer").active = false;
  owner.RigidBody.ApplyLinearImpulse(15 * owner.space.FindEntityByName("Player").GetComponent("PlayerController").facing, 0);
  owner.RigidBody.angularVelocity = -10;
  owner.RigidBody.rotationLocked = false;
  owner.DispatchEvent("Thrown", {});
  owner.RigidBody.SetGroup(CollisionGroup.DetectAllButDefault);
  
  var sprite = owner.Sprite;
  sprite.glowColor.r = 0.75;
  sprite.glowColor.b = 0.75;
  sprite.glowColor.g = 0.75;
  sprite.brightness = 0.5;
  
  this.pickedUp = false;
};

Throwable.prototype.onDrop = function() {
  var owner = arguments[0];

  owner.GetComponent("FollowPlayer").active = false;
  owner.Sprite.glowColor.r = 1.0;
  owner.Sprite.glowColor.g = 1.0;
  owner.Sprite.brightness = 0.5;
  owner.RigidBody.rotationLocked = false;
  owner.RigidBody.gravityScale = 10;
  owner.DispatchEvent("Dropped", {});
  owner.RigidBody.SetGroup(CollisionGroup.DetectAllButDefault);
  this.pickedUp = false;
};

Throwable.prototype.onHitStart = function() {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  if (hitEvent.myType == HitBoxType.Body && hitEvent.otherType == HitBoxType.Body && hitEvent.otherEntity.HasComponent("Enemy")) {

    if (this.thrown) {
      var dmgEvent = new DamageEvent(this.damage);

      // set the damage event's facing based on x velocity
      var velocityX = owner.RigidBody.velocity.x;
      if (velocityX > 0)
        dmgEvent.direction = Facing.Right;
      else
        dmgEvent.direction = Facing.Left;

      hitEvent.otherEntity.DispatchEvent("damageEvent", dmgEvent);
      owner.DispatchEvent("damageEvent", new DamageEvent(1));
    }
  }
};

Throwable.prototype.onSelect = function() {
  var owner = arguments[0];

  var sprite = owner.Sprite;
  sprite.glowColor.r = 0.5;
  sprite.glowColor.b = 0.5;
  sprite.glowColor.g = 0.75;
  sprite.brightness = 1.3;
};

Throwable.prototype.onDeselect = function() {
  var owner = arguments[0];

  var sprite = owner.Sprite;
  sprite.glowColor.r = 0.75;
  sprite.glowColor.b = 0.75;
  sprite.glowColor.g = 0.75;
  sprite.brightness = 0.5;
};

Throwable.prototype.onCollisionStart = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];

  if (this.thrown)
  {
    owner.DispatchEvent("damageEvent", new DamageEvent(this.damage));
  }
};

Throwable.prototype.onDeath = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];

  if (this.pickedUp) {
    var player = owner.space.FindEntityByName("Player");
    player.GetComponent("PlayerController").interactiveTargetID = 0;
    player.GetComponent("PlayerController").holdingObject = false;
  }
};
