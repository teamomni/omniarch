/******************************************************************************
Filename: NutsAndBolts.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var NutsAndBolts = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
  this.solidTime = 2.0;
  this.lifespan = 4.0;
  this.fadeTime = this.lifespan - this.solidTime;
  this.active = true;
  this.timer = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

NutsAndBolts.Create = function() {
  // required function; do not remove
  return new NutsAndBolts(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

NutsAndBolts.properties = {
  "active":         Property(Type.Bool,  true),
  "solidTime":      Property(Type.Float, 2.0),
  "lifespan":       Property(Type.Float,  4.0)
};

//////////////////
//  INITIALIZE  //
//////////////////

NutsAndBolts.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  
  this.fadeTime = this.lifespan - this.solidTime;
  
  var randomScale = (Math.random() % 0.2 - 0.1) + 0.15;
  owner.transform.scale.x = randomScale;
  owner.transform.scale.y = randomScale;
  
  owner.RigidBody.ApplyLinearImpulse(Math.random() * 8 - 4, Math.random() * 5);
  owner.RigidBody.angularVelocity = (5 + Math.random() * 5);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
NutsAndBolts.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  if (!this.active)
    return;

  else {
    this.timer += updateEvent.dt;
    
    if (this.timer >= this.solidTime) {
      owner.Sprite.color.a = 1 - (this.timer - this.solidTime) / this.fadeTime;
    }
    
    if (this.timer >= this.lifespan) {
      // TODO: Switch back to regular destroy
      owner.Destroy();
    }
  }
};
