/******************************************************************************
Filename: RespawnInitiator.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var RespawnInitiator = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
  
  this.timer = 0;
  this.player = 0;
  this.playerGravScale = 0;
  
  // fade to black stuff
  this.fadeToBlackTime = 0.3;
  this.fadedToBlack = false;
  
  // player fade to black stuff
  this.playerFadeTime = 0.7;
  this.playerFaded = false;
  
  // moving the player back stuff
  this.movedPlayerBack = false;
  this.cameraPanned = false
  this.cameraPanTime = 1;
  
  // particle effect for player
  this.respawnedPlayer = false;
  this.respawnTime = 1;
  
  // fade black out when done respawning
  this.fadedBlackOut = false;
  this.fadedBlackTime = 1.0;
  
  this.targetAlpha = 0.8;
  
  this.playerGlowRed = 0;
  this.playerGlowGreen = 0;
  this.playerGlowBlue = 0;
  
  this.spawning = false;
  this.checkedSpawning = false;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

RespawnInitiator.Create = function() {
  // required function; do not remove
  return new RespawnInitiator(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

RespawnInitiator.properties = {
};

//////////////////
//  INITIALIZE  //
//////////////////

RespawnInitiator.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];
  
  this.fadeout = Omni.FindSpaceByName("ui").CreateEntity({"name" : "respawn_FadeToBlack", "archetype" : "GenericSprite", "position" : {"x" : 0, "y" : 0}});
  this.fadeout.Sprite.image = "screenFade";
  this.fadeout.Sprite.color.r = 0;
  this.fadeout.Sprite.color.g = 0;
  this.fadeout.Sprite.color.b = 0;
  this.fadeout.Sprite.color.a = 0;
  
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  
  this.player = Omni.FindSpaceByName("main").FindEntityByName("Player");
  this.player.GetComponent("PlayerController").active = false;
  this.playerGravScale = this.player.RigidBody.gravityScale;
  this.player.RigidBody.gravityScale = 0;
  this.player.RigidBody.velocity.x = 0;
  this.player.RigidBody.velocity.y = 0;
  this.player.RigidBody.static = true;
  this.player.RigidBody.ghost = true;
  this.player.RigidBody.Clear();
  
  this.playerGlowRed = this.player.Sprite.glowColor.r;
  this.playerGlowGreen = this.player.Sprite.glowColor.g;
  this.playerGlowBlue = this.player.Sprite.glowColor.b;

  owner.SoundEmitter.startPlaying = true;
  owner.SoundEmitter.SetParameter(0, 0);

  this.bgm = Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic");

  // set the active flag
  this.bgm.GetComponent("BackgroundMusic").active = false;

  // tutorial 0 to 1
  if (Omni.currentLevel == "Trains")
    this.bgm.SoundEmitter.SetParameter(0, 0);

  // non tutorial 0.7 to 0
  else {
    this.bgm.SoundEmitter.SetParameter(1, 0.7);
  }
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
RespawnInitiator.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  this.timer += updateEvent.dt;
  
  if (!this.checkedSpawning) {
    if (this.spawning) {
      this.fadedToBlack = true;
      this.playerFaded = true;
      this.respawnedPlayer = true;
      this.player.Sprite.color.a = 1;
      this.fadeout.Sprite.color.a = this.targetAlpha;
      this.player.Animator.Set("plr_coolguy");
      this.cameraPanned = true;
    }
    else
      this.player.Animator.Set("plr_hitstun");
      
    this.checkedSpawning = true;
  }
  
  // first fade the screen to black
  if (!this.fadedToBlack) {
    // respawn sound
    var parameter = 0.5 * this.timer / (this.fadeToBlackTime + this.playerFadeTime);
    owner.SoundEmitter.SetParameter(0, parameter);
    this.fadeout.Sprite.color.a = (this.targetAlpha * this.timer / this.fadeToBlackTime);
    
    if (this.timer >= this.fadeToBlackTime) {
      this.fadeout.Sprite.color.a = this.targetAlpha;
      this.fadedToBlack = true;
      this.timer = 0;
    }
  }
  
  // then fade the player out
  else if (!this.playerFaded) {
    var parameter = 0.5 * (this.fadeToBlackTime + this.timer)  / (this.fadeToBlackTime + this.playerFadeTime);
    owner.SoundEmitter.SetParameter(0, parameter);

    this.player.Sprite.color.a = 1 - (this.timer / this.playerFadeTime);

    this.player.Sprite.glowColor.a = 1 - (this.timer / this.playerFadeTime);
    this.player.Sprite.glowColor.r = this.playerGlowRed + ((1 - this.playerGlowRed) * this.timer / this.playerFadeTime);
    this.player.Sprite.glowColor.g = this.playerGlowGreen + ((1 - this.playerGlowGreen) * this.timer / this.playerFadeTime);
    this.player.Sprite.glowColor.b = this.playerGlowBlue + ((1 - this.playerGlowBlue) * this.timer / this.playerFadeTime);
    
    
    if (this.timer >= this.playerFadeTime) {
      this.player.Sprite.color.a = 0;
      this.player.ParticleEmitter.active = false;
      this.playerFaded = true;
      this.timer = 0;
      this.player.GetComponent("PlayerCamera").scalar = 2;
    }
  }
  
  // then move the player back
  else if (!this.movedPlayerBack) {
    this.player.transform.translation.x = this.player.GetComponent("PlayerLogic").spawnPoint.x;
    this.player.transform.translation.y = this.player.GetComponent("PlayerLogic").spawnPoint.y;
    this.player.GetComponent("PlayerController").respawnEffect.ParticleEmitter.active = true;
    
    // make the player face right
    this.player.Sprite.flipX = false;
    this.player.GetComponent("PlayerController").facing = Facing.Right;
    
    this.movedPlayerBack = true;
    this.timer = 0;
  }
  
  else if (!this.cameraPanned) {
    if (this.timer > this.cameraPanTime) {
      this.cameraPanned = true;
      this.timer = 0;
    }
  }
  
  // then fade him back in with the particle emitter
  else if (!this.respawnedPlayer) {
    // respawn music
    var parameter = 0.5 + 0.5 * this.timer / (this.respawnTime + this.fadedBlackTime);
    owner.SoundEmitter.SetParameter(0, parameter);

    // music music
    // tutorial 0 to 1
    if (Omni.currentLevel == "Trains")
      this.bgm.SoundEmitter.SetParameter(0, this.timer / (this.respawnTime + this.fadedBlackTime));

    // non tutorial 0.7 to 0
    else
      this.bgm.SoundEmitter.SetParameter(1, 0.7 - 0.7 * this.timer / (this.respawnTime + this.fadedBlackTime));

    this.player.Sprite.color.a = this.timer / this.respawnTime;
    this.player.Sprite.glowColor.a = this.timer / this.respawnTime;
    this.player.RigidBody.velocity.y = 0;
    
    if (this.timer >= this.respawnTime) {
      this.player.Sprite.color.a = 1;
      var position = new Vec2(this.player.transform.translation.x, this.player.transform.translation.y);
      this.player.ParticleEmitter.ResetTrail(position);
      this.player.ParticleEmitter.active = true;
      this.respawnedPlayer = true;
      this.timer = 0;
    }
  }
  
  // then fade the black out
  else if (!this.fadedBlackOut) {
    // respawn sound
    var parameter = 0.5 + 0.5 * (this.respawnTime + this.timer) / (this.respawnTime + this.fadedBlackTime);
    owner.SoundEmitter.SetParameter(0, parameter);

    // music music
    // tutorial 0 to 1
    if (Omni.currentLevel == "Trains")
      this.bgm.SoundEmitter.SetParameter(0, (this.respawnTime + this.timer) / (this.respawnTime + this.fadedBlackTime));

    // non tutorial 0.7 to 0
    else
      this.bgm.SoundEmitter.SetParameter(1, 0.7 - 0.7 * (this.respawnTime + this.timer) / (this.respawnTime + this.fadedBlackTime));

    this.fadeout.Sprite.color.a = this.targetAlpha - (this.targetAlpha * this.timer / this.fadedBlackTime);
    this.fadeout.Sprite.glowColor.a = this.targetAlpha - (this.targetAlpha * this.timer / this.fadedBlackTime);
    this.player.RigidBody.velocity.y = 0;
    
    this.player.Sprite.glowColor.r = 1 - ((1 - this.playerGlowRed) * this.timer / this.fadedBlackTime);
    this.player.Sprite.glowColor.g = 1 - ((1 - this.playerGlowGreen) * this.timer / this.fadedBlackTime);
    this.player.Sprite.glowColor.b = 1 - ((1 - this.playerGlowBlue) * this.timer / this.fadedBlackTime);
    
    if (this.timer >= this.fadedBlackTime) {
      this.player.Sprite.glowColor.r = this.playerGlowRed;
      this.player.Sprite.glowColor.g = this.playerGlowGreen;
      this.player.Sprite.glowColor.b = this.playerGlowBlue;
    
      this.fadeout.Sprite.color.a = 0;
      this.fadedBlackOut = true;
      this.timer = 0;
      this.fadeout.Destroy();
    }
  }
  
  // if everything is done, destroy myself, and my sprite thing
  else {
    this.bgm.GetComponent("BackgroundMusic").active = true;
    this.player.RigidBody.gravityScale = this.playerGravScale;
    this.player.RigidBody.Clear();
    this.player.RigidBody.static = false;
    this.player.RigidBody.ghost = false;
    this.player.GetComponent("PlayerController").reactivate();
    this.player.GetComponent("PlayerController").respawnEffect.ParticleEmitter.active = false;
    this.player.GetComponent("PlayerLogic").killed = false;
    this.player.GetComponent("PlayerCamera").scalar = 3;
    owner.Destroy();
  }
}