/******************************************************************************
Filename: DeactivateOnObjectDeath.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var DeactivateOnObjectDeath = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.connected = false;

};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

DeactivateOnObjectDeath.Create = function() {
  // required function; do not remove
  return new DeactivateOnObjectDeath(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

DeactivateOnObjectDeath.properties = {
  "target": Property(Type.Int, 0),
};

//////////////////
//  INITIALIZE  //
//////////////////

DeactivateOnObjectDeath.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
DeactivateOnObjectDeath.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEVent = arguments[1];
  
  if (!this.connected) {
    var target = Omni.FindEntityByLinkID(this.target);
    Omni.Connect(this, "entity", "Death", target, this.onDeath);
    this.connected = true;
  }
};

DeactivateOnObjectDeath.prototype.onDeath = function() {
  var owner = arguments[0];
  
  //Console.WriteLine("deadeadeadaedaedead");
  
  //Console.WriteLine(owner.name);
  owner.DispatchEvent("pass", {});
}
