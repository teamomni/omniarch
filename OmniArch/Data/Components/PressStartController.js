/******************************************************************************
Filename: PressStartController.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////
var PressStartController = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // initialize member variables here
  this.fadingOut = false;
  this.initialPosition = { x: 0, y: 0 };
  this.elapsedTime = 0;

  // run any other code that should be executed when this Component
  // is created here (note that this is executed right when the
  // Component is created, but BEFORE it has been attached to its
  // owner Entity
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////
PressStartController.Create = function() {
  // required function; do not remove
  return new PressStartController(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////
PressStartController.properties = {};

//////////////////
//  INITIALIZE  //
//////////////////
PressStartController.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  this.initialPosition.x = owner.transform.translation.x;
  this.initialPosition.y = owner.transform.translation.y;
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
PressStartController.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (CheckInput("menu_confirm") && !this.fadingOut) {
    this.fadingOut = true;
  }
  if (this.fadingOut) {
    owner.Sprite.color.a -= 0.05;
    /*owner.transform.scale.x += 0.05;
    owner.transform.scale.y += 0.05;*/
    owner.transform.translation.x += 0.1;

    if (owner.Sprite.color.a <= 0) {
      owner.Sprite.visible = false;
      owner.Sprite.color.a = 0;
      /*owner.transform.scale.x = 1;
      owner.transform.scale.y = 1;*/
      owner.transform.translation.x = this.initialPosition.x;
      owner.transform.translation.y = this.initialPosition.y;
    }
  }
  else {
    this.elapsedTime += updateEvent.dt;
    owner.Sprite.color.a = Math.min(Math.sin(this.elapsedTime * 2) + 1, 1);
  }


};
