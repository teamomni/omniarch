/******************************************************************************
Filename: ElectricFence.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var ElectricFence = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.damage = 20;

  this.active = true;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

ElectricFence.Create = function() {
  // required function; do not remove
  return new ElectricFence(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

ElectricFence.properties = {

};

//////////////////
//  INITIALIZE  //
//////////////////

ElectricFence.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "onHitPersist", owner, this.onHitPersist);
  Omni.Connect(this, "entity", "deactivate", owner, this.onDeactivate);

  owner.Animator.Set("electricfence");
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
ElectricFence.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

};

ElectricFence.prototype.onDeactivate = function() {
  var owner = arguments[0];

  this.active = false;
  owner.Sprite.glowColor.a = 0;
  owner.RigidBody.ghost = true;
};

ElectricFence.prototype.onHitPersist = function() {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  if (!this.active || hitEvent.otherEntity === undefined)
    return;

  if (hitEvent.otherType == HitBoxType.Body && hitEvent.otherEntity.GetComponent("PlayerLogic")) {
    hitEvent.otherEntity.DispatchEvent("damageEvent", new DamageEvent(this.damage));
    hitEvent.otherEntity.DispatchEvent("knockbackEvent", new KnockbackEvent(owner));
  }
};
