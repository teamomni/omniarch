/******************************************************************************
Filename: ChangeLevelInitiator.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var ChangeLevelInitiator = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
  
  this.timer = 0;
  this.player = 0;
  this.playerGravScale = 0;
  
  // stand still stuff
  this.stoodStill = false;
  this.standStillTime = 0.8;
  
  // faded stuff
  this.faded = false;
  this.fadeTime = 1.0;
  
  // fade to black stuff
  this.fadedToBlack = false;
  this.fadeToBlackTime = 0.3;
  
  this.targetAlpha = 0.8;
  
  this.playerGlowRed = 0;
  this.playerGlowGreen = 0;
  this.playerGlowBlue = 0;
  
  this.spawning = false;
  this.checkedSpawning = false;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

ChangeLevelInitiator.Create = function() {
  // required function; do not remove
  return new ChangeLevelInitiator(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

ChangeLevelInitiator.properties = {
  "level":       Property(Type.String_, "Trains")
};

//////////////////
//  INITIALIZE  //
//////////////////

ChangeLevelInitiator.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];
  
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  
  this.player = Omni.FindSpaceByName("main").FindEntityByName("Player");
  this.player.GetComponent("PlayerController").active = false;
  this.playerGravScale = this.player.RigidBody.gravityScale;
  this.player.RigidBody.gravityScale = 0;
  this.player.RigidBody.velocity.x = 0;
  this.player.RigidBody.velocity.y = 0;
  this.player.RigidBody.static = true;
  this.player.RigidBody.ghost = true;
  this.player.RigidBody.Clear();
  
  this.fadeout = Omni.FindSpaceByName("ui").CreateEntity({"name" : "respawn_FadeToBlack", "archetype" : "GenericSprite", "position" : {"x" : 0, "y" : 0}});
  this.fadeout.Sprite.image = "screenFade";
  this.fadeout.Sprite.color.r = 0;
  this.fadeout.Sprite.color.g = 0;
  this.fadeout.Sprite.color.b = 0;
  this.fadeout.Sprite.color.a = 0;
  
  this.playerGlowRed = this.player.Sprite.glowColor.r;
  this.playerGlowGreen = this.player.Sprite.glowColor.g;
  this.playerGlowBlue = this.player.Sprite.glowColor.b;
  this.player.Animator.Set("plr_idle");

};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
ChangeLevelInitiator.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  this.timer += updateEvent.dt;
  
  if (!this.stoodStill) {
    if (this.timer >= this.standStillTime) {
      this.player.Animator.Set("plr_coolguy");
      this.stoodStill = true;
      this.timer = 0;
    }
  }
  
  else if (!this.faded) {
    this.player.Sprite.color.a = 1 - (this.timer / this.fadeTime);
    this.player.Sprite.glowColor.a = 1 - (this.timer / this.fadeTime);
    
    if (this.timer >= this.fadeTime) {
      this.player.Sprite.color.a = 0;
      this.player.Sprite.glowColor.a = 0;
      this.faded = true;
      this.timer = 0;
    }
  }
  
  else if (!this.fadedToBlack) {
    this.fadeout.Sprite.color.a = (this.timer / this.fadeToBlackTime);
    if (this.timer >= this.fadeToBlackTime) {
      this.fadeout.Sprite.color.a = 1;
      this.fadedToBlack = true;
      this.timer = 0;
    }
  }
  
  else {
    Omni.LoadLevel(this.level);
  }
  
}