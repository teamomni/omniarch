/******************************************************************************
Filename: DestructableObject.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var DestructableObject = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.integrity = 6;

  this.flashDuration = 0.1;
  this.flashTimer = 0;
  this.flashing = false;

  this.startColor = {r:1, g:1, b:1};
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

DestructableObject.Create = function() {
  // required function; do not remove
  return new DestructableObject(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

DestructableObject.properties = {
  "integrity": Property(Type.Float, 6),
  "bombOnly" : Property(Type.Bool, false),
  "active" : Property(Type.Bool, true),
  "colorUpdate" : Property(Type.Bool, true),
  "damageSound": Property(Type.String_, "event:/robot-damageget")
};

//////////////////
//  INITIALIZE  //
//////////////////

DestructableObject.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "damageEvent", owner, this.onDamage);
  Omni.Connect(this, "entity", "Thrown", owner, this.onThrow);
  Omni.Connect(this, "entity", "ThrownFar", owner, this.onThrownFar);
    
  owner.SoundEmitter.looping = false;
  owner.SoundEmitter.startPlaying = false;

  this.startColor.r = owner.Sprite.glowColor.r;
  this.startColor.g = owner.Sprite.glowColor.g;
  this.startColor.b = owner.Sprite.glowColor.b;
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
DestructableObject.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  if (this.flashing) {
    this.flashTimer += updateEvent.dt;
    if (this.flashTimer >= this.flashDuration) {
      this.flashing = false;
      this.flashTimer = 0;
    }
  }

  if (this.colorUpdate) {
    owner.Sprite.glowColor.r = Ease.Easy(owner.Sprite.glowColor.r, this.startColor.r, 0.1);
    owner.Sprite.glowColor.g = Ease.Easy(owner.Sprite.glowColor.g, this.startColor.g, 0.1);
    owner.Sprite.glowColor.b = Ease.Easy(owner.Sprite.glowColor.b, this.startColor.b, 0.1);
  }
};

DestructableObject.prototype.onDamage = function() {
  var owner = arguments[0];
  var damageEvent = arguments[1];


  // disable if it's not active
  if (!this.active)
    return;
  
  if (this.bombOnly && damageEvent.sourceType != "Bomb")
    return;


  this.flashing = true;
  var sprite = owner.Sprite;

  sprite.glowColor.g = 1;
  sprite.glowColor.b = 1;
  sprite.glowColor.r = 1;
  sprite.brightness = 1.25;

  this.integrity -= damageEvent.damage;

  var soundEvent = new SoundEvent(this.damageSound);
  owner.space.DispatchEvent("playSound", soundEvent);

  // Die
  if (this.integrity <= 0) {
    var a = new Vec2(owner.transform.translation.x, owner.transform.translation.y + 0.5);
    var explosion = owner.space.CreateEntity({name : "ExplosionSmall", position : a, archetype : "ExplosionSmall"});
    explosion.Animator.Set("explosion_small");

    var soundEvent = new SoundEvent("event:/break");
    owner.space.DispatchEvent("playSound", soundEvent);

    owner.DispatchEvent("Death", {});
    owner.Destroy();
  }
};


DestructableObject.prototype.onThrow = function() {
  this.active = true;
};
  
DestructableObject.prototype.onThrownFar = function() {
  this.active = true;
  
};
