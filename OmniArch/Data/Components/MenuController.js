/******************************************************************************
Filename: MenuController.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////
var MenuController = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // initialize member variables here
  this.isSplashScreen = false;
  this.elapsedTime = 0;
  this.menuStack = [];
  this.selectedIndex = 0;
  this.xOrig = 0;
  this.logoHeight = 0.75;
  this.controlsVisible = false;
  this.duration_splash = 115;
  this.splashTimer = 0;
  this.letterbox1 = null;
  this.letterbox2 = null;
  this.letterboxSize = 16;
  this.buttoningThrough = false;
  this.menus = {
    "digipen": {
      noControls: true,
      noCopyright: true,
      noLogo: true,
      onOpen: function(menu) {
        this.splashTimer = this.duration_splash;
      },
      entries: [
        {
          text: "[DIGIPEN LOGO]",
          image: "digipen",
          centered: true,
          onConfirm: function(owner)
          {
            if (this.buttoningThrough) 
              return;
            this.buttoningThrough = true;
            this.splashTimer = 0;
            this.Close();
            this.Open("casualBabyDucks");
          },
          onUpdate: function(owner, entry) {
            this.splashTimer -= 1;
            if (this.splashTimer <= 0) {
              this.Close();
              this.Open("casualBabyDucks");
            }
          }
        }
      ]

    },
    "casualBabyDucks": {
      noControls: true,
      noCopyright: true,
      noLogo: true,
      onOpen: function(menu) {
        this.splashTimer = this.duration_splash;
      },
      entries: [
        {
          text: "[CASUAL BABY DUCKS LOGO]",
          image: "casualBabyDucks",
          centered: true,
          trueColor: true,
          onConfirm: function(owner)
          {
            if (this.buttoningThrough)
              return;
            this.buttoningThrough = true;
            this.splashTimer = 0;
            this.Close();
            this.Open("bestWithGamepad");
          },
          onUpdate: function(owner, entry) {
            this.splashTimer -= 1;
            if (this.splashTimer <= 0) {
              this.Close();
              this.Open("bestWithGamepad");
            }
          }
        }
      ]
    },
    "bestWithGamepad": {
      noControls: true,
      noCopyright: true,
      noLogo: true,
      onOpen: function(menu) {
        this.splashTimer = this.duration_splash;
      },
      entries: [
      {
        text: "THIS GAME IS BEST PLAYED WITH A GAMEPAD.",
        image: "bestWithGamepad",
        centered: true,
        onConfirm: function(owner)
        {
          if (this.buttoningThrough) 
            return;
          this.buttoningThrough = true;
          this.splashTimer = 0;
          this.Close();
          // Stop Opening Title menu and Load Train level
          Omni.LoadLevel("Trains");          
          //this.Open("title");
        },
        onUpdate: function(owner, entry) {
          this.splashTimer -= 1;
          if (this.splashTimer <= 0) {
            this.Close();
            Omni.LoadLevel("Trains");
            this.elapsedTime = 0;
          }
        }
      }
      ]
    },
    "victory": {
      //noCopyright: true,
      noLogo: true,
      noSelection: true,
      confirmOverride: true,
      confirmImage: "menubtn_proceed",
      entries: [
        {
          text: "YOU DEFEATED",
          image: "victory",
          centered: true,
          onConfirm: function(owner) {
            this.Open("endCredits");
          }
        }
      ]
    },
    "title": {
      noControls: true,
      defaultIndex: 0,
      cancelImage: "menubtn_exitGame",
      onCancel: function() {
        //Omni.QuitGame();
        this.Open("quit");
      },
      entries: [
        {
          text: "PRESS START",
          image: "menu_pressStart",
          isPulsing: true,
          onConfirm: function(owner) {
            this.Unpause();
            //this.controlsVisible = true;
          },
          onUpdate: function(owner, entry) {
            switch (Omni.CheckInputMethod()) {
              case InputMethod.Keyboard:
                entry.Sprite.image = "menu_pressEnter";
                break;
              case InputMethod.Gamepad:
                entry.Sprite.image = "menu_pressStart";
                break;
            }
          }
        }
      ]
    },

    "pause": {
      defaultIndex: 1,
      onCancel: function(owner) {
        this.Unpause();
      },
      onOpen: function(menu) {
        //Omni.PauseAllSounds();
      },
      onClose: function(menu) {
        //Omni.UnpauseAllSounds();
      },
      entries: [
        {
          isHeading: true,
          text: "GAME PAUSED",
          image: "menu_h_gamePaused"
        },
        {
          text: "RESUME",
          image: "menu_resume",
          onConfirm: function(owner) {
            this.Unpause();
          }
        },
        {
          text: "HOW TO PLAY",
          image: "menu_howToPlay",
          onConfirm: function(owner) {
            this.Open("howToPlay");
          }
        },
        {
          text: "OPTIONS",
          image: "menu_options",
          onConfirm: function(owner) {
            this.Open("options");
          }
        },
        {
          text: "CREDITS",
          image: "menu_credits",
          onConfirm: function(owner) {
            this.Open("credits");
          }
        },
        // Quit to main menu option 
        {
          text: "QUIT TO MENU",
          image: "menu_quit_to_main",
          onConfirm: function(owner) {
            this.Open("quit2");
          }
        },
        {
          text: "QUIT GAME",
          image: "menu_quit",
          onConfirm: function(owner) {
            this.Open("quit");
          }
        },
      ]
    },

    "howToPlay": {
      cancelClose: true,
      noSelection: true,
      noLogo: true,
      entries: [
        {
          image: "howToPlay_kbd",
          centered: true,
          onUpdate: function(owner, entry) {
            switch (Omni.CheckInputMethod()) {
              case InputMethod.Keyboard:
                entry.Sprite.image = "howToPlay_kbd";
                break;
                case InputMethod.Gamepad:
                  entry.Sprite.image = "howToPlay_pad";
                  break;
            }
          }
        }
      ]
    },

    "options": {
      cancelClose: true,
      defaultIndex: 1,
      confirmImage: "menubtn_toggle",
      entries: [
        {
          isHeading: true,
          text: "OPTIONS",
          image: "menu_h_options"
        },
        {
          text: "FULLSCREEN ON/OFF",
          image: "menu_fullscreen_on",
          onUpdate: function(owner, entry)
          {
            if (Omni.IsFullScreen())
              entry.Sprite.image = "menu_fullscreen_on";
            else
              entry.Sprite.image = "menu_fullscreen_off";
          },
          onConfirm: function(owner, entry) {
            Omni.SetFullScreen(!Omni.IsFullScreen());
          }
        },
        {
          text: "SOUNDS ON/OFF",
          image: "menu_sfx_on",
          onUpdate: function(owner, entry)
          {
            if (Omni.playSoundEffects)
              entry.Sprite.image = "menu_sfx_on";
            else
              entry.Sprite.image = "menu_sfx_off";
          },
          onConfirm: function(owner, entry)
          {
            if (entry.Sprite.image == "menu_sfx_off") {
              Omni.playSoundEffects = true;
            }
            else {
              Omni.playSoundEffects = false;
            }
          }
        },
        {
          text: "MUSIC ON/OFF",
          image: "menu_music_on",
          onUpdate: function(owner, entry)
          {
            if (Omni.playMusic)
              entry.Sprite.image = "menu_music_on";
            else
              entry.Sprite.image = "menu_music_off";
          },
          onConfirm: function(owner, entry) {
            if (entry.Sprite.image == "menu_music_off") {
              Omni.playMusic = true;
              Omni.DispatchEvent("musicStart");
            }
            else {
              Omni.playMusic = false;
              Omni.DispatchEvent("musicStop");
            }
          }
        }
      ]
    },

    "credits": {
      cancelClose: true,
      noSelection: true,
      noLogo: true,
      entries: [
        {
          text: "[CREDITS]",
          image: "credits",
          centered: true
        }
      ]
    },

    "endCredits": {
      noSelection: true,
      noLogo: true,
      cancelImage: "menubtn_exitDemo",
      onCancel: function() {
        this.Open("quit2");
      },
      entries: [
        {
          text: "[CREDITS]",
          image: "credits",
          centered: true
        }
      ]
    },

    "quit": {
      defaultIndex: 2,
      onCancel: function(owner) {
        if (this.selectedIndex == 2)
          this.Close();
        else
          this.selectedIndex = 2;
      },
      entries: [
        {
          isHeading: true,
          text: "ARE YOU SURE?",
          image: "menu_h_areYouSure"
        },
        {
          text: "YES",
          image: "menu_yes",
          onConfirm: function(owner) {
            Omni.QuitGame();
          }
        },
        {
          text: "NO",
          image: "menu_no",
          onConfirm: function(owner) {
            this.Close();
          }
        }
      ]
    },

    "quit2": {
      defaultIndex: 2,
      onCancel: function(owner) {
        if (this.selectedIndex == 2)
          this.Close();
          else
            this.selectedIndex = 2;
        },
        entries: [
          {
            isHeading: true,
            text: "ARE YOU SURE?",
            image: "menu_h_areYouSure"
          },
          {
            text: "YES",
            image: "menu_yes",
            onConfirm: function(owner) {
              Omni.LoadLevel("Trains");
            }
          },
          {
            text: "NO",
            image: "menu_no",
            onConfirm: function(owner) {
              this.Close();
            }
          }
        ]
      }
  };

  this.pausedOnce = 0;

  // run any other code that should be executed when this Component
  // is created here (note that this is executed right when the
  // Component is created, but BEFORE it has been attached to its
  // owner Entity
  //this.PauseAllExcept("ui");
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////
MenuController.Create = function() {
  // required function; do not remove
  return new MenuController(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////
MenuController.properties = {};

//////////////////
//  INITIALIZE  //
//////////////////
MenuController.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onFrameUpdate", owner, 0, this.onUpdate);

  this.letterbox1 = owner.space.CreateEntity({ name: "letterbox", position: new Vec2(0, 0), archetype: "GenericSprite" });
  this.letterbox1.Sprite.image = "pixel";
  this.letterbox1.Sprite.depth = -10000;
  this.letterbox1.Sprite.color.r = 0;
  this.letterbox1.Sprite.color.g = 0;
  this.letterbox1.Sprite.color.b = 0;
  this.letterbox1.Sprite.color.a = 1;
  this.letterbox1.transform.scale.x = this.letterboxSize;
  this.letterbox1.transform.scale.y = this.letterboxSize;

  this.letterbox2 = owner.space.CreateEntity({ name: "letterbox", position: new Vec2(0, 0), archetype: "GenericSprite" });
  this.letterbox2.Sprite.image = "pixel";
  this.letterbox2.Sprite.depth = -10000;
  this.letterbox2.Sprite.color.r = 0;
  this.letterbox2.Sprite.color.g = 0;
  this.letterbox2.Sprite.color.b = 0;
  this.letterbox2.Sprite.color.a = 1;
  this.letterbox2.transform.scale.x = this.letterboxSize;
  this.letterbox2.transform.scale.y = this.letterboxSize;


  var blackbg = owner.space.CreateEntity({ name: "blackbg", position: new Vec2(0, 0), archetype: "GenericSprite" });
  blackbg.Sprite.image = "pixel";
  blackbg.Sprite.depth = -308;
  blackbg.Sprite.color.r = 23/256;
  blackbg.Sprite.color.g = 40/256;
  blackbg.Sprite.color.b = 44/256;
  blackbg.Sprite.color.a = 1;

  blackbg.transform.scale.x = 16;
  blackbg.transform.scale.y = 16;

  var bg = owner.space.CreateEntity({ name: "menubg", position: new Vec2(1.5, 0), archetype: "GenericSprite" });
  bg.Sprite.image = "menubg";
  bg.Sprite.depth = -305;
  bg.Sprite.color.a = 0;
  bg.transform.scale.x = 0.5;
  bg.transform.scale.y = 0.5;

  var logo = owner.space.CreateEntity({ name: "menulogo", position: new Vec2(1.8, 0), archetype: "GenericSprite" });
  logo.Sprite.image = "omniarch_logo_menu";
  logo.Sprite.depth = -310;
  logo.Sprite.color.a = 0;

  var copyright = owner.space.CreateEntity({ name: "menucopyright", position: new Vec2(0, -2.9), archetype: "GenericSprite" });
  copyright.Sprite.image = "mainMenu_copyright";
  copyright.Sprite.depth = -380;
  copyright.Sprite.color.a = 0;

  for (var menu in this.menus) {
    var i = 0;
    for (var entry in this.menus[menu].entries) {
      var pos = new Vec2(this.xOrig, 0);
      var name = this.MakeEntryName(menu, entry);
      if (this.menus[menu].entries[entry].image) {
        var ent = owner.space.CreateEntity({ name: name, position: pos, archetype: "GenericSprite" });
        ent.Sprite.image = this.menus[menu].entries[entry].image;
        ent.Sprite.depth = -320;
        ent.Sprite.color.a = 0;
        //ent.Sprite.color.a = 0;
        ++i;
      }
    }
  }

  var menuConfirm = owner.space.CreateEntity({ name: "menuConfirm", position: new Vec2(2.5, -2.25), archetype: "GenericSprite" });
  menuConfirm.Sprite.image = "menubtn_confirm";
  menuConfirm.Sprite.depth = -320;
  menuConfirm.Sprite.color.r = 0.6;
  menuConfirm.Sprite.color.g = 0.6;
  menuConfirm.Sprite.color.b = 0.6;
  menuConfirm.Sprite.color.a = 0;
  menuConfirm.transform.scale.x = 2;

  var menuBack = owner.space.CreateEntity({ name: "menuBack", position: new Vec2(2.5, -2.5), archetype: "GenericSprite" });
  menuBack.Sprite.image = "menubtn_back";
  menuBack.Sprite.depth = -320;
  menuBack.Sprite.color.r = 0.6;
  menuBack.Sprite.color.g = 0.6;
  menuBack.Sprite.color.b = 0.6;
  menuBack.Sprite.color.a = 0;
  menuBack.transform.scale.x = 2;

  var menuInput_confirm = owner.space.CreateEntity({ name: "menuInput_confirm", position: new Vec2(1.9, -2.25), archetype: "GenericSprite" });
  menuInput_confirm.Sprite.image = "btn_A";
  menuInput_confirm.Sprite.depth = -320;
  menuInput_confirm.Sprite.color.a = 0;
  menuInput_confirm.transform.scale.x = 0.5;
  menuInput_confirm.transform.scale.y = 0.5;

  var menuInput_back = owner.space.CreateEntity({ name: "menuInput_back", position: new Vec2(1.9, -2.5), archetype: "GenericSprite" });
  menuInput_back.Sprite.image = "btn_B";
  menuInput_back.Sprite.depth = -320;
  menuInput_back.Sprite.color.a = 0;
  menuInput_back.transform.scale.x = 0.5;
  menuInput_back.transform.scale.y = 0.5;

  var groundControls = owner.space.CreateEntity({ name: "groundControls", position: new Vec2(0, -2.5), archetype: "GenericSprite" });
  groundControls.Sprite.image = "ground_controls_pad";
  groundControls.Sprite.color.a = 0;

  //this.Open("title");
  //this.Open("splashSpacer");
  //this.Open("casualBabyDucks");
  //this.Open("splashSpacer");
  
  // Loading Splash Screens
  if (Omni.currentLevel == "SplashScreens") {
      this.Open("digipen");
      this.isSplashScreen = true;
  }
  
  // Main Menu on Trains Level
  if (Omni.currentLevel == "Trains")
      this.Open("title");
};

//---------------------------//
//  example helper function  //
//---------------------------//
MenuController.prototype.MakeEntryName = function(menu, entry) {
  return "menu_" + menu + "_" + entry;
};

MenuController.prototype.CalculateEntryHeight = function(entry) {
  var height = 0.375;
  if (entry.isHeading)
    height = 0.25;
  if (entry.height)
    height = entry.height;
  return height;
};
MenuController.prototype.IncrementSelection = function() {
  var start = this.selectedIndex;
  do {
    ++this.selectedIndex;
    if (this.selectedIndex >= this.menus[this.menuStack[this.menuStack.length - 1]].entries.length) {
      this.selectedIndex -= this.menus[this.menuStack[this.menuStack.length - 1]].entries.length;
    }
  } while ((this.menus[this.menuStack[this.menuStack.length - 1]].entries[this.selectedIndex].disabled || this.menus[this.menuStack[this.menuStack.length - 1]].entries[this.selectedIndex].isHeading) && this.selectedIndex != start);
};

MenuController.prototype.DecrementSelection = function() {
  var start = this.selectedIndex;
  do {
    --this.selectedIndex;
    if (this.selectedIndex < 0) {
      this.selectedIndex += this.menus[this.menuStack[this.menuStack.length - 1]].entries.length;
    }
  } while ((this.menus[this.menuStack[this.menuStack.length - 1]].entries[this.selectedIndex].disabled || this.menus[this.menuStack[this.menuStack.length - 1]].entries[this.selectedIndex].isHeading) && this.selectedIndex != start);
};

MenuController.prototype.Close = function() {
  if (this.menuStack.length > 0 && this.menus[this.menuStack[this.menuStack.length - 1]].onClose !== undefined)
    this.menus[this.menuStack[this.menuStack.length - 1]].onClose.call(this, this.menuStack[this.menuStack.length - 1]);
  if (this.menuStack.length > 0)
    this.menuStack.pop();
  if (this.menuStack.length > 0 && this.menus[this.menuStack[this.menuStack.length - 1]].defaultIndex !== undefined)
    this.selectedIndex = this.menus[this.menuStack[this.menuStack.length - 1]].defaultIndex;
  if (this.menuStack.length > 0 && this.menus[this.menuStack[this.menuStack.length - 1]].onEnter !== undefined)
    this.menus[this.menuStack[this.menuStack.length - 1]].onEnter.call(this, this.menuStack[this.menuStack.length - 1]);
};

MenuController.prototype.CloseAll = function() {
  while (this.menuStack.length)
    this.Close();
};

MenuController.prototype.Pause = function(owner) {
  Omni.PauseAllExcept("ui");
  this.Close();
  this.Open("pause", 1);
  //Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic").SoundEmitter.pause = true;
};

MenuController.prototype.Unpause = function(owner) {
  Omni.UnpauseAll();
  this.CloseAll();
  //Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic").SoundEmitter.pause = false;
};

MenuController.prototype.Open = function(menu) {
  this.menuStack.push(menu);
  var ind = 0;
  if (this.menus[menu].defaultIndex !== undefined)
    ind = this.menus[menu].defaultIndex;
  this.selectedIndex = ind;
  if (this.menus[menu].onOpen !== undefined)
    this.menus[menu].onOpen.call(this, this.menus[menu]);
};

MenuController.prototype.HideControls = function() {
  this.controlsVisible = false;
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
MenuController.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  this.buttoningThrough = false;


  //skip splash screen
  if (this.isSplashScreen) {
    if (CheckInput("skip_menus"))
      Omni.LoadLevel("Trains");
  }

  /*
   * BLACK BAR CODE GOES HERE
   */


  var clientRatio = Omni.clientWidth / Omni.clientHeight;
  var gameRatio = 16 / 9;
  var cameraSize = owner.space.camera.size;
  if (Math.abs(clientRatio - gameRatio) === 0) {
    this.letterbox1.Sprite.visible = false;
    this.letterbox2.Sprite.visible = false;
  }
  else {
    this.letterbox1.Sprite.visible = true;
    this.letterbox2.Sprite.visible = true;
    if (clientRatio > gameRatio)
    {
      this.letterbox1.transform.translation.x = -cameraSize * 16 / 9 / 2 - this.letterboxSize / 2;
      this.letterbox1.transform.translation.y = 0;
      this.letterbox2.transform.translation.x = cameraSize * 16 / 9 / 2 + this.letterboxSize / 2;
      this.letterbox2.transform.translation.y = 0;
    }
    else
    {
      this.letterbox1.transform.translation.x = 0;
      this.letterbox1.transform.translation.y = -cameraSize / 16 * 9 - this.letterboxSize / 2.1;
      this.letterbox2.transform.translation.x = 0;
      this.letterbox2.transform.translation.y = cameraSize / 16 * 9 + this.letterboxSize / 2.1;
    }
  }

  if (this.pausedOnce < 2) {
    this.pausedOnce++;
  }
  if (this.pausedOnce == 2) {
    if (Omni.currentLevel == "Trains") 
      Omni.PauseAllExcept("ui");
    this.pausedOnce = 3;
  }

  if (this.menuStack.length === 0) {
    if (CheckInput("pause"))
      this.Pause(owner);
  }
  else {
    // Check input
    var name_ = this.MakeEntryName(this.menuStack[this.menuStack.length - 1], this.selectedIndex);
    var ent_ = owner.space.FindEntityByName(name_);
    
    // Menu Selection Movement (UP/DOWN) defaultIndex
    if (CheckInput("menu_down") && !this.menus[this.menuStack[this.menuStack.length - 1]].noSelection && this.menus[this.menuStack[this.menuStack.length - 1]].defaultIndex != 0) {
      owner.SoundEmitter.soundCue = "event:/menu-select";
      owner.SoundEmitter.startPlaying = true;
      this.IncrementSelection();
    }
    if (CheckInput("menu_up") && !this.menus[this.menuStack[this.menuStack.length - 1]].noSelection && this.menus[this.menuStack[this.menuStack.length - 1]].defaultIndex != 0) {
      owner.SoundEmitter.soundCue = "event:/menu-select";
      owner.SoundEmitter.startPlaying = true;
      this.DecrementSelection();
    }
    
    if (CheckInput("menu_confirm") && (!this.menus[this.menuStack[this.menuStack.length - 1]].noSelection || (this.menus[this.menuStack[this.menuStack.length - 1]].noSelection && this.menus[this.menuStack[this.menuStack.length - 1]].confirmOverride))) {
      var onConfirm = this.menus[this.menuStack[this.menuStack.length - 1]].entries[this.selectedIndex].onConfirm;
      if (onConfirm !== undefined) {
        owner.SoundEmitter.soundCue = "event:/menu-confirm";
        owner.SoundEmitter.startPlaying = true;
        onConfirm.call(this, owner, ent_);
      }
    }
    if (CheckInput("menu_cancel")) {
      if(Omni.currentlevel == "SplashScreens")
      {
        return;
      }
      if (this.menus[this.menuStack[this.menuStack.length - 1]].cancelClose) {
        owner.SoundEmitter.soundCue = "event:/menu-cancel";
        owner.SoundEmitter.startPlaying = true;
        this.Close();
        return;
      }
      var onCancel = this.menus[this.menuStack[this.menuStack.length - 1]].onCancel;
      if (onCancel !== undefined) {
        /*owner.SoundEmitter.soundCue = "event:/menu-cancel";
        owner.SoundEmitter.startPlaying = true;*/
        onCancel.call(this, owner, ent_);
      }
    }
  }

  var bg = owner.space.FindEntityByName("menubg");
  var bga = 0;
  if (this.menuStack.length > 0)
    bga = 1;
  bg.Sprite.color.a = Ease.Easy(bg.Sprite.color.a, bga);

  var logo = owner.space.FindEntityByName("menulogo");
  logo.Sprite.color.a = Ease.Easy(logo.Sprite.color.a, (this.menuStack.length > 0 && this.menus[this.menuStack[this.menuStack.length - 1]].noLogo) ? 0 : bga, 0.4);

  var copyright = owner.space.FindEntityByName("menucopyright");
  copyright.Sprite.color.a = Ease.Easy(copyright.Sprite.color.a, this.menuStack.length > 0 && this.menus[this.menuStack[this.menuStack.length - 1]].noCopyright ? 0 : bga);

  var menuConfirm = owner.space.FindEntityByName("menuConfirm");
  var menuInput_confirm = owner.space.FindEntityByName("menuInput_confirm");
  var menuBack = owner.space.FindEntityByName("menuBack");
  var menuInput_back = owner.space.FindEntityByName("menuInput_back");

  var select = true;
  var cancel = true;
  if (this.menuStack.length === 0) {
    select = false;
    cancel = false;
  }
  else if (this.menus[this.menuStack[this.menuStack.length - 1]].noControls) {
    select = false;
    if (!this.menus[this.menuStack[this.menuStack.length - 1]].onCancel && !this.menus[this.menuStack[this.menuStack.length - 1]].cancelClose)
      cancel = false;
  }
  else {
    if (this.menus[this.menuStack[this.menuStack.length - 1]].noSelection && !this.menus[this.menuStack[this.menuStack.length - 1]].confirmOverride)
      select = false;
    if (!this.menus[this.menuStack[this.menuStack.length - 1]].onCancel && !this.menus[this.menuStack[this.menuStack.length - 1]].cancelClose)
      cancel = false;
  }

  if (this.menuStack.length > 0) {
    menuConfirm.Sprite.image = this.menus[this.menuStack[this.menuStack.length - 1]].confirmImage || "menubtn_confirm";
    menuBack.Sprite.image = this.menus[this.menuStack[this.menuStack.length - 1]].cancelImage || "menubtn_back";
  }

  menuConfirm.Sprite.color.a = Ease.Easy(menuConfirm.Sprite.color.a, (this.menuStack.length > 0 && select) ? 1 : 0, 0.3);
  menuInput_confirm.Sprite.color.a = Ease.Easy(menuInput_confirm.Sprite.color.a, (this.menuStack.length > 0 && select) ? 1 : 0, 0.3);
  menuBack.Sprite.color.a = Ease.Easy(menuBack.Sprite.color.a, (this.menuStack.length > 0 && cancel) ? 1 : 0, 0.3);
  menuInput_back.Sprite.color.a = Ease.Easy(menuInput_back.Sprite.color.a, (this.menuStack.length > 0 && cancel) ? 1 : 0, 0.3);

  menuConfirm.transform.translation.y = Ease.Easy(menuConfirm.transform.translation.y, select ? -2.25 : -2.75, 0.2);
  menuInput_confirm.transform.translation.y = Ease.Easy(menuInput_confirm.transform.translation.y, select ? -2.25 : -2.75, 0.2);

  menuBack.transform.translation.y = Ease.Easy(menuBack.transform.translation.y, cancel ? -2.5 : -3, 0.2);
  menuInput_back.transform.translation.y = Ease.Easy(menuInput_back.transform.translation.y, cancel ? -2.5 : -3, 0.2);

  var groundControls = owner.space.FindEntityByName("groundControls");
  groundControls.Sprite.color.a = Ease.Easy(groundControls.Sprite.color.a, this.controlsVisible ? 1 : 0, this.controlsVisible ? 0.3 : 0.05);
  groundControls.transform.translation.y = Ease.Easy(groundControls.transform.translation.y, this.controlsVisible ? -2 : -2.5, this.controlsVisible ? 0.3 : 0.05);

  switch (Omni.CheckInputMethod()) {
    case InputMethod.Keyboard:
      menuInput_confirm.Sprite.image = "key_space";
      menuInput_back.Sprite.image = "key_esc";
      groundControls.Sprite.image = "ground_controls_kbd";
      break;
    case InputMethod.Gamepad:
      menuInput_confirm.Sprite.image = "btn_A";
      menuInput_back.Sprite.image = "btn_B";
      groundControls.Sprite.image = "ground_controls_pad";
      break;
  }


  var xPositions = {
    "offscreen": this.xOrig,
    "selected": 1.35,
    "active": 1.5
  };
  var colors = {
    "offscreen": {
      r: 0.1,
      g: 0.1,
      b: 0.1,
      a: 0.0
    },
    "selected": {
      r: 0.7,
      g: 0.9,
      b: 0.9,
      a: 1.0
    },
    "active": {
      r: 0.55,
      g: 0.6,
      b: 0.6,
      a: 1.0
    }
  };

  var somethingIsCentered = false;
  for (var menu in this.menus) {
    var i = 0;
    var yOffset = this.logoHeight;
    for (var entry2 in this.menus[menu].entries) {
      yOffset += this.CalculateEntryHeight(this.menus[menu].entries[entry2]);
    }

    yOffset /= 2;

    if (menu == this.menuStack[this.menuStack.length - 1])
      logo.transform.translation.y = Ease.Easy(logo.transform.translation.y, yOffset, 0.125);
    yOffset -= this.logoHeight;

    for (var entry in this.menus[menu].entries) {
      if (this.menus[menu].entries[entry].image === undefined) {
        ++i;
        continue;
      }
      var name = this.MakeEntryName(menu, entry);
      var ent = owner.space.FindEntityByName(name);
      var xPos = "offscreen";
      var centered = this.menus[menu].entries[entry].centered;
      if (menu == this.menuStack[this.menuStack.length - 1]) {
        if (this.menus[menu].entries[entry].onUpdate !== undefined)
          this.menus[menu].entries[entry].onUpdate.call(this, owner, ent);
        if (centered)
          somethingIsCentered = true;
        if (i == this.selectedIndex)
          xPos = "selected";
        else
          xPos = "active";
      }
      if (centered) {
        ent.transform.translation.x = 0;
        ent.transform.translation.y = 0;
      }
      else {
        ent.transform.translation.x = Ease.Easy(ent.transform.translation.x, xPositions[xPos], 0.075 + (0.1 * (entry / this.menus[menu].entries.length)));
        ent.transform.translation.y = yOffset;// - this.CalculateEntryHeight(entry);
      }
      if (this.menus[menu].entries[entry].trueColor) {
        ent.Sprite.color.r = 1;
        ent.Sprite.color.g = 1;
        ent.Sprite.color.b = 1;
      }
      else {
        ent.Sprite.color.r = Ease.Easy(ent.Sprite.color.r, colors[xPos].r, 0.3);
        ent.Sprite.color.g = Ease.Easy(ent.Sprite.color.g, colors[xPos].g, 0.3);
        ent.Sprite.color.b = Ease.Easy(ent.Sprite.color.b, colors[xPos].b, 0.3);
      }
      ent.Sprite.color.a = Ease.Easy(ent.Sprite.color.a, colors[xPos].a * (this.menus[menu].entries[entry].isPulsing ? Math.min(0.5 + Math.cos(this.elapsedTime * 4), 1) : 1), 0.3);
      if (!centered)
        yOffset -= this.CalculateEntryHeight(entry);
      ++i;
    }
  }
  var blackbg = owner.space.FindEntityByName("blackbg");
  blackbg.Sprite.color.a = Ease.Easy(blackbg.Sprite.color.a, somethingIsCentered /*|| this.menus.length === 0 */ || (this.menuStack.length > 0 && this.menus[this.menuStack[this.menuStack.length - 1]].solidBackground)? 1 : 0, 0.4);
  this.elapsedTime += updateEvent.dt;
};
