/******************************************************************************
Filename: PassOnEvents.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var PassOnEvents = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.ChangeAlphaUp = false;
  this.ChangeAlphaDown = false;
  this.MoveUp = false;
  this.MoveDown = false;
  this.MoveLeft = false;
  this.MoveRight = false;
  this.FlipX = false;
  this.FlipY = false;
  this.MoveBack = false;
  this.MoveForward = false;
  this.OldGravityScale = 0.0;
  this.Death = false;
  this.Received = false;
  this.DeathEvent = false;
  
  // stuff for dispatching death events
  this.deathOnReceivingDeathEvent = true;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

PassOnEvents.Create = function() {
  // required function; do not remove
  return new PassOnEvents(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

PassOnEvents.properties = {
  "receiveOnly"       :       Property(Type.Bool, false), 
  "receiveOnce"       :       Property(Type.Bool, false),
  "otherObjLinkID"    :       Property(Type.Int, 1),
  "death"             :       Property(Type.Bool, false),
  "deathEvent"        :       Property(Type.Bool, false),
  "alphaUp"           :       Property(Type.Bool, false),
  "alphaDown"         :       Property(Type.Bool, false),
  "changeAlphaBy"     :       Property(Type.Float, 0.5),
  "moveUp"            :       Property(Type.Bool, false),
  "moveDown"          :       Property(Type.Bool, false),
  "moveLeft"          :       Property(Type.Bool, false),
  "moveRight"         :       Property(Type.Bool, false),
  "changePosBy"       :       Property(Type.Float, 0.5),
  "flipX"             :       Property(Type.Bool, false),
  "flipY"             :       Property(Type.Bool, false),
  "moveBack"          :       Property(Type.Bool, false),
  "moveForward"       :       Property(Type.Bool, false),
  "changeDepthBy"     :       Property(Type.Int, 1),
  "rateScale"         :       Property(Type.Float, 1.0),
  "inactiveColor"     :       Property(Type.Bool, false),
  "noGravityWhileChange"  :       Property(Type.Bool, false),
  "doNotPassEventOnDeath"  :       Property(Type.Bool, false)
};

//////////////////
//  INITIALIZE  //
//////////////////

PassOnEvents.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "Death", owner, this.onDeath);
  Omni.Connect(this, "entity", "deactivate", owner, this.onDeactivate);
  Omni.Connect(this, "entity", "passDeath", owner, this.onPassDeath);
  Omni.Connect(this, "entity", "pass", owner, this.onPass);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
PassOnEvents.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (this.Received)
    return;

  if(this.ChangeAlphaDown || this.ChangeAlphaUp)
  {

    if(this.changeAlphaBy > 0)
    {
      if(this.noGravityWhileChange)
        if(owner.HasComponent("RigidBody")) {
          if(this.OldGravityScale === 0.0)
            this.OldGravityScale = owner.RigidBody.gravityScale;
          owner.RigidBody.gravityScale = 0.0;
        }

      if(this.ChangeAlphaDown)
        owner.Sprite.color.a -= 0.1 * this.rateScale;
      if(this.ChangeAlphaUp)
        owner.Sprite.color.a += 0.1 * this.rateScale;

      this.changeAlphaBy -= 0.1 * this.rateScale;
    }
    else
    {
      if(this.noGravityWhileChange)
        if(owner.HasComponent("RigidBody"))
          owner.RigidBody.gravityScale = this.OldGravityScale;

      if(this.inactiveColor && owner.HasComponent("Sprite"))
      {
        owner.Sprite.glowColor.r = 0.75;
        owner.Sprite.glowColor.g = 0.75;
        owner.Sprite.glowColor.b = 0.75;
      }

      this.ChangeAlphaUp = false;
      this.ChangeAlphaDown = false;
      if (this.receiveOnce)
        this.Received = true;
    }
  }
      


  if(this.MoveUp || this.MoveDown || this.MoveLeft || this.MoveRight)
  {


    if(this.changePosBy > 0)
    {
      if(this.noGravityWhileChange)
        if(owner.HasComponent("RigidBody")) {
          if(this.OldGravityScale === 0.0)
            this.OldGravityScale = owner.RigidBody.gravityScale;
          owner.RigidBody.gravityScale = 0.0;

          
        }

        //Console.WriteLine(this.MoveLeft);

      if(this.MoveUp)
        owner.transform.translation.y += 0.1 * this.rateScale;
      if(this.MoveDown)
        owner.transform.translation.y -= 0.1 * this.rateScale;
      if(this.MoveLeft)
        owner.transform.translation.x -= 0.1 * this.rateScale;
      if(this.MoveRight)
        owner.transform.translation.x += 0.1 * this.rateScale;

      this.changePosBy -= 0.1 * this.rateScale;
    }
    else
    {
      if(this.noGravityWhileChange)
        if(owner.HasComponent("RigidBody"))
          owner.RigidBody.gravityScale = this.OldGravityScale;

      if(this.inactiveColor && owner.HasComponent("Sprite"))
      {
        owner.Sprite.glowColor.r = 0.75;
        owner.Sprite.glowColor.g = 0.75;
        owner.Sprite.glowColor.b = 0.75;
        // Console.WriteLine("Now we here");
      }

      this.MoveUp = false;
      this.MoveDown = false;
      this.MoveRight = false;
      this.MoveLeft = false;
      if (this.receiveOnce)
        this.Received = true;
      return;
    }
  }

  if(this.FlipY)
  {
    owner.Sprite.flipY = !owner.Sprite.flipY;
    this.FlipY = false;
  }


  if(this.FlipX)
  {
    owner.Sprite.flipX = !owner.Sprite.flipX;
    this.FlipX = false;
  }

  if(this.MoveBack)
  {
    owner.Sprite.depth += this.changeDepthBy;
    this.MoveBack = false;
  }

  if(this.MoveForward)
  {
    owner.Sprite.depth -= this.changeDepthBy;
    this.MoveForward = false;
  }

  if(this.Death)
    owner.Destroy();
    
  if(this.DeathEvent)
    owner.DispatchEvent("Death");

};

PassOnEvents.prototype.passEvents = function(owner) {
    //Console.WriteLine(this.alphaDown);
    if(this.alphaUp)
      this.ChangeAlphaUp = true;
    if(this.alphaDown)
      this.ChangeAlphaDown = true;
    if(this.moveUp)
      this.MoveUp = true;
    if(this.moveDown)
      this.MoveDown = true;
    if(this.moveLeft)
      this.MoveLeft = true;
    if(this.moveRight)
      this.MoveRight = true;
    if(this.flipX)
      this.FlipX = true;
    if(this.flipY)
      this.FlipY = true;
    if(this.moveBack)
      this.MoveBack = true;
    if(this.moveForward)
      this.MoveForward = true;
    if(this.death)
      this.Death = true;
    if(this.deathEvent)
      this.DeathEvent = true;

  if(this.receiveOnly)
    return;

  if(this.receiveOnce)
  {
      this.changeAlphaUp = false;
      this.changeAlphaDown = false;
      this.moveUp = false;
      this.moveDown = false;
      this.moveLeft = false;
      this.moveRight = false;
      this.flipX = false;
      this.flipY = false;
      this.moveBack = false;
      this.moveForward = false;
      this.death = false;
      this.deathEvent = false;
  }

  var obj = Omni.FindEntityByLinkID(this.otherObjLinkID);

  if(obj === undefined)
    return;

  if(this.Death) {
    obj.DispatchEvent("passDeath", {});
  }
  else {
    obj.DispatchEvent("pass", {});
  }
}

// Some other component killed us
PassOnEvents.prototype.onDeath = function() {
  owner = arguments[0];

  if (!this.doNotPassEventOnDeath)
    this.passEvents(owner);
}

PassOnEvents.prototype.onDeactivate = function() {
  owner = arguments[0];

  this.passEvents(owner);
  
  if(this.death)
    owner.Destroy();
}

// we recieved an event to destroy ourselves from
// another object
PassOnEvents.prototype.onPassDeath = function() {
  owner = arguments[0];

  this.passEvents(owner);

  if (this.deathOnReceivingDeathEvent)
    owner.Destroy();
}

// we recieved an event to destroy ourselves from
// another object
PassOnEvents.prototype.onPass = function() {
  owner = arguments[0];

  this.passEvents(owner);

}
