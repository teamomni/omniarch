/******************************************************************************
Filename: BombDamage.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var BombDamage = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.firstFrame = true;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

BombDamage.Create = function() {
  // required function; do not remove
  return new BombDamage(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

BombDamage.properties = {
  "damage":      Property(Type.Float,    25)
};

//////////////////
//  INITIALIZE  //
//////////////////

BombDamage.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "onCollisionStart", owner, this.onCollisionStart);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
BombDamage.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  if (this.firstFrame) {
    this.firstFrame = false;
    return;
  }
  
  owner.Destroy();

};

BombDamage.prototype.onCollisionStart = function () {
  var owner = arguments[0];
  var collisionEvent = arguments[1];

  if (collisionEvent.otherEntity === undefined)
    return;

  var damageEvent = new DamageEvent(this.damage);
  damageEvent.sourceType = "Bomb";
  collisionEvent.otherEntity.DispatchEvent("damageEvent", damageEvent);
}
