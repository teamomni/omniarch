/******************************************************************************
Filename: Footsteps.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// constructor
var Footsteps = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member variables
};

// boilerplate create function
Footsteps.Create = function() {
  return new Footsteps(arguments[0]);
};

////////////////////////////////////////////////////////////////////////////////
// PROPERTIES
////////////////////////////////////////////////////////////////////////////////

Footsteps.properties = {
};

// function called after
Footsteps.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);

  owner.SoundEmitter.volume = 0.5;
  owner.SoundEmitter.soundCue = "event:/player-footstep";
  owner.SoundEmitter.startPlaying = false;
};

////////////////////////////////////////////////////////////////////////////////
// EVENTS
////////////////////////////////////////////////////////////////////////////////
Footsteps.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];


};
