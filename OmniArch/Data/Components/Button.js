/******************************************************************************
Filename: Button.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var Button = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables

};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Button.Create = function() {
  // required function; do not remove
  return new Button(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

Button.properties = {
    "objLinkID":     Property(Type.Int, 1),
    "playerOnly":    Property(Type.Bool, false)
};

//////////////////
//  INITIALIZE  //
//////////////////

Button.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "onCollisionStart", owner, this.onCollisionStart);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
Button.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  //Console.WriteLine("here");
  

};


Button.prototype.onCollisionStart = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];

  if (this.playerOnly) {
    if (collisionEvent.otherEntity !== undefined) {
      if (!collisionEvent.otherEntity.HasComponent("PlayerLogic"))
        return;
    }
  }
  
  //Console.WriteLine(collisionEvent.otherEntity.name);

  owner.Sprite.image = "buttonDown";
  owner.Sprite.glowColor.r = 0.75;
  owner.Sprite.glowColor.g = 0.75;
  owner.Sprite.glowColor.b = 0.75;

  owner.SoundEmitter.soundCue = "event:/button";
  owner.SoundEmitter.startPlaying = true;

  var obj = Omni.FindEntityByLinkID(this.objLinkID);
  obj.DispatchEvent("deactivate", {});

};