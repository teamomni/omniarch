/******************************************************************************
Filename: HealthBar.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var HealthBar = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.player = 0;

  this.startX = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

HealthBar.Create = function() {
  // required function; do not remove
  return new HealthBar(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

HealthBar.properties = {
};

//////////////////
//  INITIALIZE  //
//////////////////

HealthBar.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  
  this.player = Omni.FindSpaceByName("main").FindEntityByName("Player");

  this.startX = owner.transform.translation.x;
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
HealthBar.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  var percent = this.player.GetComponent("PlayerLogic").currentHealth / this.player.GetComponent("PlayerLogic").maxHealth;

  owner.transform.scale.x = (1 - percent);

  owner.transform.translation.x = this.startX + ((1 - owner.transform.scale.x) * 1.28);
};
