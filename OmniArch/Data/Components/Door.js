/******************************************************************************
Filename: Door.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var Door = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Door.Create = function() {
  // required function; do not remove
  return new Door(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

//////////////////
//  INITIALIZE  //
//////////////////

Door.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "entity", "damageEvent", owner, this.onDamage);
  owner.SoundEmitter.looping = false;
  
  // set animation
  owner.Animator.Set("door_idle");
};


Door.prototype.onDamage = function() {
  var owner = arguments[0];
  var damageEvent = arguments[1];

  owner.Animator.Set("door_attacked");
};