/******************************************************************************
Filename: FirstLevelIntro.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var FirstLevelIntro = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
  
  this.timer = 0;
  this.noBgmTime = 1.0;
  this.idleTime = 1.0;
  this.player = 0;
  this.doneWithBGM = false;
  this.doneWithPlayer = false;
  this.playerDoneIdling = false;

  this.stopRunningX = -105;
  
  this.createdTitle = false;
  this.createdUI = false;

  this.secondDropTimer = 0;
  this.secondDropped = false;

  this.releaseCameraX = -91.25;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

FirstLevelIntro.Create = function() {
  // required function; do not remove
  return new FirstLevelIntro(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

FirstLevelIntro.properties = {
  "posX":       Property(Type.Float, 0),
  "posY":       Property(Type.Float, 0),
  "targetX":    Property(Type.Float, -99)
};

//////////////////
//  INITIALIZE  //
//////////////////

FirstLevelIntro.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];
  
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  
  this.player = owner.space.FindEntityByName("Player");
  this.player.GetComponent("PlayerController").active = false;
  this.player.Animator.Set("plr_run");
  this.player.GetComponent("PlayerCamera").centerReleased = false;


  this.playerCamera = this.player.GetComponent("PlayerCamera");

};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
FirstLevelIntro.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!this.createdUI) {
    Omni.FindSpaceByName("ui").CreateEntity({"name" : "screenFade", "archetype" : "ScreenFade", "position" : {"x" : 0, "y" : 0}});  
    this.createdUI = true;
    Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusicWind").SoundEmitter.volume = 1;
  }
  
  if(!this.createdTitle) {
    this.createdTitle = true;
  }
  
  /*if (!this.doneWithBGM) {
    //this.timer += updateEvent.dt;
    if (this.timer > this.noBgmTime) {
      Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic").SoundEmitter.startPlaying = true;
      Omni.FindSpaceByName("ui").CreateEntity({"name" : "omniArchLogo", "archetype" : "TitleDrop", "position" : {"x" : 0,  "y" : 1.4}});  
      this.doneWithBGM = true;
      this.timer = 0;
    }
  }
  
  else if (!this.playerDoneIdling) {
    this.timer += updateEvent.dt;
    if (this.timer > this.idleTime) {
      this.player.GetComponent("PlayerController").active = true;
      this.playerDoneIdling = true;
    }
  }*/
  
  var playerTrans = this.player.transform.translation;

  if (!this.doneWithPlayer) {
    playerTrans.x += 0.08;
    if (playerTrans.x >= this.stopRunningX) {
      this.player.Animator.Set("plr_idle");
      this.doneWithPlayer = true;
      this.player.GetComponent("PlayerController").active = true;
    }

    
  }
  if (!this.doneWithBGM)
  {
    if (playerTrans.x >= this.targetX) {
      Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic").SoundEmitter.startPlaying = true;
      Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic").GetComponent("BackgroundMusic").wasPlaying = true;
      Omni.FindSpaceByName("ui").CreateEntity({"name" : "omniArchLogo", "archetype" : "TitleDrop", "position" : {"x" : 0,  "y" : 1.4}});  
      
      this.doneWithBGM = true;
      Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusicWind").SoundEmitter.volume = 0.5;

      this.playerCamera.zoomReleased = false;
      this.playerCamera.intendedSize = 10;
    }
  }

  if (this.doneWithBGM)
  {
    this.secondDropTimer += updateEvent.dt;

    if (this.secondDropTimer >= 0.5 && !this.secondDropped)
    {
      Omni.FindSpaceByName("ui").CreateEntity({"name" : "omniArchLogo", "archetype" : "TitleDrop2", "position" : {"x" : 0,  "y" : 1.4}});  
      this.secondDropped = true;
    }
  }

  if (playerTrans.x >= this.releaseCameraX) {
      this.playerCamera.centerReleased = true;
      this.playerCamera.zoomReleased = true;
      this.playerCamera.intendedSize = this.playerCamera.baseCameraSize;
  }
}