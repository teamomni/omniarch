/******************************************************************************
Filename: Droid.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var Droid = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  this.active = true;

  // Member Variables
  this.aggroRange = 8;
  this.aggroed = false;

  this.accelSpeed = 2;

  this.bulletSpeed = 10;
  this.shotRange = 4;
  this.shotCooldown = 0.3;
  this.shotTimer = 0;
  this.shotsFired = 0;
  this.shotBurstCooldown = 3;
  this.shotBurstTimer = 0;
  this.shotBurstQuantity = 1;

  this.facing = Facing.Left;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Droid.Create = function() {
  // required function; do not remove
  return new Droid(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

Droid.properties = {

};

//////////////////
//  INITIALIZE  //
//////////////////

Droid.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "changeState", owner, this.changeState);

  owner.Animator.Set("droid_idle");
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
Droid.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!this.active)
    return;

  if (owner.GetComponent("Enemy").state == EnemyStates.Idle)
    this.CheckAggro(owner);
  this.FaceTarget(owner);

  this.shotTimer += updateEvent.dt;
  this.shotBurstTimer += updateEvent.dt;

  if (this.aggroed) {
    //this.MoveTowardsPoint(owner);
    this.ShootAtTarget(owner);
  }
};

Droid.prototype.CheckAggro = function(owner) {
  var player = Omni.FindEntityByID(this.playerID);
  var distanceToTarget = new Vec2(owner.transform.translation.x, owner.transform.translation.y);
  distanceToTarget.sub(distanceToTarget, player.transform.translation);

  if (distanceToTarget.length() <= this.aggroRange) {
    this.aggroed = true;
    var stateEvent = new StateEvent(EnemyStates.Hostile);
    owner.DispatchEvent("changeState", stateEvent);
    return true;
  }

  return false;
};

Droid.prototype.MoveTowardsPoint = function(owner) {
  var targetPoint = owner.space.FindEntityByName("Player").transform.translation;
  var direction = new Vec2(targetPoint.x, targetPoint.y);
  direction.sub(direction, owner.transform.translation);

  if (direction.x <= -this.shotRange + 1 || direction.x >= this.shotRange - 1) {
    direction.normalize();

    owner.RigidBody.ApplyLinearForce(direction.x * this.accelSpeed, direction.y);
  }
  else {
    // Break
    owner.RigidBody.velocity.x *= 0.9;
  }
};

Droid.prototype.ShootAtTarget = function(owner) {
  var targetPoint = owner.space.FindEntityByName("Player").transform.translation;
  var direction = new Vec2(targetPoint.x, targetPoint.y);
  direction.sub(direction, owner.transform.translation);

  if (this.shotBurstTimer < this.shotBurstCooldown)
    return;

  // If in range, SHOOT!
  if (direction.x >= -this.shotRange + 1 || direction.x <= this.shotRange - 1) {
    if (this.shotTimer < this.shotCooldown)
      return;

    direction.normalize();
    var a = new Vec2(owner.transform.translation.x, owner.transform.translation.y - 0.5);
    var projectille = owner.space.CreateEntity({name : "PlasmaBall", position : a, archetype : "PlasmaBall"});
    projectille.RigidBody.velocity.x = (direction.x * this.bulletSpeed) + owner.RigidBody.velocity.x;
    projectille.transform.scale.x = 0.3;
    projectille.transform.scale.y = 0.3;
    this.shotTimer = 0;
    this.shotsFired += 1;

    if (this.shotsFired >= this.shotBurstQuantity) {
      this.shotBurstTimer = 0;
      this.shotsFired = 0;
    }
  }
};

Droid.prototype.FaceTarget = function(owner) {
  var targetPoint = owner.space.FindEntityByName("Player").transform.translation;
  var direction = new Vec2(targetPoint.x, targetPoint.y);
  direction.sub(direction, owner.transform.translation);

  if (direction.x > 0 && this.facing == Facing.Left) {
    this.facing = Facing.Right;
    owner.Sprite.flipX = true;
  }
  if (direction.x < 0 && this.facing == Facing.Right) {
    this.facing = Facing.Left;
    owner.Sprite.flipX = false;
  }
};

Droid.prototype.changeState = function() {
  var owner = arguments[0];
  var stateEvent = arguments[1];

  if (stateEvent.newState == EnemyStates.Idle) {
  }

  if (stateEvent.newState == EnemyStates.Hostile) {
  }

  if (stateEvent.newState == EnemyStates.Defeated) {
    this.active = false;
  }
};
