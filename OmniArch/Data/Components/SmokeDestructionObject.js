/******************************************************************************
Filename: SmokeDestructionObject.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var SmokeDestructionObject = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

SmokeDestructionObject.Create = function() {
  // required function; do not remove
  return new SmokeDestructionObject(arguments[0]);
};

//////////////////
//  INITIALIZE  //
//////////////////

SmokeDestructionObject.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];
  
    
  // this is where you connect events
  Omni.Connect(this, "entity", "Death", owner, this.onDeath);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////

SmokeDestructionObject.prototype.onDeath = function() {
  var owner = arguments[0];
  var deathEvent = arguments[1];
  
  // create the particle effects
  var trans = owner.transform.translation;
  var explosionEffect = owner.space.CreateEntity(
    {"name" : "dyingExplosionParticle", "archetype" : "CartoonExplosion", "position" : {"x" : trans.x, "y" : trans.y}});  
    
  explosionEffect.ParticleEmitter.active = true;
  explosionEffect.GetComponent("TimedDeath").active = true;
};

