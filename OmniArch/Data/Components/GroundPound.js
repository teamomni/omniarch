/******************************************************************************
Filename: GroundPound.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var GroundPound = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.leftInChain = 0;
  this.chainDirection = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

GroundPound.Create = function() {
  // required function; do not remove
  return new GroundPound(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

GroundPound.properties = {

};

//////////////////
//  INITIALIZE  //
//////////////////

GroundPound.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "death", owner, this.onDeath);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
GroundPound.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

};

GroundPound.prototype.onDeath = function(owner) {
  //Console.WriteLine(this.leftInChain);
  if (this.leftInChain <= 0)
    return;

  var a = new Vec2(owner.transform.translation.x + (0.75 * this.chainDirection), owner.transform.translation.y + 0.5);
  var newObj = owner.space.CreateEntity({name : "GroundPound", position : a, archetype : "GroundPound"});
  newObj.GetComponent("GroundPound").leftInChain = this.leftInChain - 1;
  newObj.GetComponent("GroundPound").chainDirection = this.chainDirection;
};
