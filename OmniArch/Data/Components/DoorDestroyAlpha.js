/******************************************************************************
Filename: DoorDestroyAlpha.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var DoorDestroyAlpha = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.change = false;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

DoorDestroyAlpha.Create = function() {
  // required function; do not remove
  return new DoorDestroyAlpha(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

DoorDestroyAlpha.properties = {
  
};

//////////////////
//  INITIALIZE  //
//////////////////

DoorDestroyAlpha.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "deactivate", owner, this.onDeactivate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
DoorDestroyAlpha.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  

  if(this.change)
  {
    owner.Sprite.depth = -1;

    if(owner.Sprite.color.a > 0.49)
      owner.Sprite.color.a -= 0.01;
    else
      this.change = false;
  }

};


DoorDestroyAlpha.prototype.onDeactivate = function() {
  var owner = arguments[0];

  this.change = true;
  
};
