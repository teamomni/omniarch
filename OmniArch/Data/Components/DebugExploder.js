/******************************************************************************
Filename: DebugExploder.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var DebugExploder = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables

};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

DebugExploder.Create = function() {
  // required function; do not remove
  return new DebugExploder(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

DebugExploder.properties = {
  
};

//////////////////
//  INITIALIZE  //
//////////////////

DebugExploder.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
DebugExploder.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  if (CheckInput("debug_explode")) {
    //Console.WriteLine("SPLODE");
    var explosion = owner.space.CreateEntity({name : "ExplosionSmall", position : owner.transform.translation, archetype : "ExplosionSmall"});
    explosion.Animator.Set("explosion_small");
    owner.Destroy();
  }
};
