/******************************************************************************
Filename: WaveTrigger.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var WaveTrigger = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

WaveTrigger.Create = function() {
  // required function; do not remove
  return new WaveTrigger(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

WaveTrigger.properties = {
  "waveNumber":  Property(Type.Unsigned, 0)
};

//////////////////
//  INITIALIZE  //
//////////////////

WaveTrigger.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "entity", "onCollisionStart", owner, this.onCollisionStart);
};

WaveTrigger.prototype.onCollisionStart = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];

  if (collisionEvent.otherEntity.GetComponent("PlayerLogic")) {
    owner.space.DispatchEvent("waveTriggered", new EncounterEvent(this.waveNumber));
  }
};

var EncounterEvent = function (encounterNumber) {
  this.encounterNumber = encounterNumber;
};
