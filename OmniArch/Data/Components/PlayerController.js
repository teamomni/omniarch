/******************************************************************************
Filename: PlayerController.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein / Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// constructor
var PlayerController = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  this.initialJumpForce = 6;
  this.jumpHoldForce = 4;
  this.airControlForce = 6;
  this.maxJumpHoldDuration = 0.2;
  this.jumpHoldTimer = 0;
  this.jumpHoverDuration = 0;
  this.jumpHoverTimer = 0;
  this.extraFallGravity = 22;
  this.airBreakScalar = 0.85;

  this.airPunchUsed = false;

  this.accelSpeed = 15;
  this.maxMoveSpeed = 8;
  this.breakSpeed = 0.85;

  this.initialEvadeBurst = 10;
  this.evadeSpeed = 15;
  this.evadeCooldown = 0.5;
  this.evadeCooldownTimer = 0.0;

  this.stickLandingThreshold = 1;
  this.rollThreshold = 2;
  this.stopThreshold = 1;

  this.active = true;

  this.punchDamage = 10;
  this.punchChained = false;
  this.punchForce = 0.2;
  this.punchCounter = 0;

  this.facing = Facing.Right;
  this.turning = false;

  this.stateQueue = [];

  this.SetState("idle");

  this.firstFrame = false;

  this.groundPoundTimer = 0;
  this.groundPoundDamage = 10;
  this.groundPoundReleased = false;

  this.interactiveTargetID = 0;
  this.pickupTime = 0.5;
  this.pickupTimer = 0;
  this.holdingObject = false;

  this.slowTimer = 0;

  this.lastJumpStartHeight = 0;
  this.nextPunchQueued = false;
  
  this.punchEffect = 0;
  this.punchHitEffect = 0;
  
  this.particleMinXVel = 0;
  this.particleMinYVel = 0;
  this.particleMaxXVel = 0;
  this.particleMaxYVel = 0;
};


// boilerplate create function
PlayerController.Create = function() {
  return new PlayerController(arguments[0]);
};

// function called after
PlayerController.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "space", "onFrameUpdate", owner, 0, this.onFrameUpdate);
  Omni.Connect(this, "entity", "onHitStart", owner, this.onHitStart);
  Omni.Connect(this, "entity", "onHitPersist", owner, this.onHitPersist);
  Omni.Connect(this, "entity", "onHitEnd", owner, this.onHitEnd);
  Omni.Connect(this, "entity", "knockbackEvent", owner, this.onKnockback);
  Omni.Connect(this, "entity", "vaultEvent", owner, this.onVaultEvent);

  owner.Animator.Set("plr_idle");
  this.SetCameraToFollow(owner);

  owner.SoundEmitter.looping = false;

  Omni.SetDebugMessageSetting(DebugMessageSetting.None);

  /************************ CREATE NECESSARY PLAYER DEPENDENCIES HERE *****************/
  
  // create the footsteps
  owner.space.CreateEntity(
    {"name" : "Footsteps", "archetype" : "Footsteps", "position" : {"x" : 0, "y" : 0}});  
  
  // create the particle effects
  this.punchEffect = owner.space.CreateEntity(
    {"name" : "player_punchParticle", "archetype" : "PlayerPunchParticle", "position" : {"x" : 0, "y" : 0}});  
    
  // set the depth of the particle effect
  this.punchEffect.ParticleEmitter.depth = owner.Sprite.depth - 1;
  
  // now also set the offset
  this.punchEffect.GetComponent("FollowPlayer").xOffset = 0.52;
  this.punchEffect.GetComponent("FollowPlayer").yOffset = 0.1;
  
  // store the values for going to the RIGHT
  var punchEffectEmitter = this.punchEffect.ParticleEmitter;
  this.particleMinXVel = punchEffectEmitter.minVelocity.x;
  this.particleMinYVel = punchEffectEmitter.minVelocity.y;
  this.particleMaxXVel = punchEffectEmitter.maxVelocity.x;
  this.particleMaxYVel = punchEffectEmitter.maxVelocity.y;
  
  // create the particle effects (the blue explosion)
  this.punchHitEffect = owner.space.CreateEntity(
    {"name" : "player_punchHitParticle", "archetype" : "PlayerPunchHitParticle", "position" : {"x" : 0, "y" : 0}});  
    
  // set the depth of the particle effect
  this.punchHitEffect.ParticleEmitter.depth = owner.Sprite.depth - 1;
  
  // now also set the offset
  this.punchHitEffect.GetComponent("FollowPlayer").xOffset = 0.52;
  this.punchHitEffect.GetComponent("FollowPlayer").yOffset = 0.1;
  
  // create the respawn particle
  this.respawnEffect = owner.space.CreateEntity(
    {"name" : "player_respawnEffect", "archetype" : "RespawnParticle", "position" : {"x" : 0, "y" : 0}});  
    
  // this.respawnEffect.ParticleEmitter.active = true;
  this.respawnEffect.ParticleEmitter.depth = owner.Sprite.depth - 1;
};

////////////////////////////////////////////////////////////////////////////////
// PROPERTIES
////////////////////////////////////////////////////////////////////////////////

PlayerController.properties = {
  "initialJumpForce":      Property(Type.Float,    6),
  "jumpHoldForce":         Property(Type.Float,    4),
  "extraFallGravity":      Property(Type.Float,    22),
  "airControlForce":       Property(Type.Float,    6),
  "accelSpeed":            Property(Type.Float,    15),
  "maxMoveSpeed":          Property(Type.Float,    8),
  "breakSpeed":            Property(Type.Float,    0.85),
  "stickLandingThreshold": Property(Type.Float,    1),
  "rollThreshold":         Property(Type.Float,    2),
  "stopThreshold":         Property(Type.Float,    1),
  "targetLinkID" :         Property(Type.Unsigned, 0),
  "punchForce" :           Property(Type.Float,    1),
  "airBreakScalar":        Property(Type.Float,    0.85),
  "initialEvadeBurst":     Property(Type.Float,    10),
  "evadeSpeed":            Property(Type.Float,    15),
  "pickupTime":            Property(Type.Float,    0),
  "facing":                Property(Type.Int,      0)
};


////////////////////////////////////////////////////////////////////////////////
// STATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

PlayerController.prototype.QueueState = function(state) {
  this.stateQueue.push(this.states[state]);
};
PlayerController.prototype.ClearStates = function() {
  while (this.stateQueue.length > 0) {
    this.stateQueue.pop();
  }
};
PlayerController.prototype.SetState = function(state) {
  this.ClearStates();
  this.stateQueue.push(this.states[state]);
};
PlayerController.prototype.PopState = function() {
  return this.stateQueue.shift();
};
PlayerController.prototype.StateCount = function() {
  return this.stateQueue.length;
};
PlayerController.prototype.MoreStates = function() {
  return this.stateQueue.length > 1;
};
PlayerController.prototype.CurrentState = function() {
  return this.stateQueue[0];
};



////////////////////////////////////////////////////////////////////////////////
// HELPER FUNCTIONS
////////////////////////////////////////////////////////////////////////////////
PlayerController.prototype.DefaultOnGround = function(owner, doLanding) {
  if (owner.RigidBody.IsOnGround()) {
    var vel = Math.abs(owner.RigidBody.velocity.x);
    owner.SoundEmitter.soundCue = "event:/player-land";
    owner.SoundEmitter.startPlaying = true;
    if ((vel < this.stickLandingThreshold || (!CheckInput("move_left") && !CheckInput("move_right"))) && doLanding) {
      this.SetState("idle");
      owner.Animator.Set("plr_sticklanding");
      owner.Animator.Queue("plr_idle");
    }
    else {
      this.SetState("run");
      owner.Animator.Set("plr_run");
      owner.space.FindEntityByName("Footsteps").SoundEmitter.startPlaying = true;
    }

    this.airPunchUsed = false;
    this.jumpHoldTimer = 0;
    this.jumpHoverTimer = 0;
    return true;
  }
  return false;
};
PlayerController.prototype.DefaultAirborne = function(owner) {
  if (!owner.RigidBody.IsOnGround()) {
    this.SetState("airIdle");
    owner.space.FindEntityByName("Footsteps").SoundEmitter.startPlaying = false;

    return true;
  }
  return false;
};
PlayerController.prototype.DefaultJump = function(owner) {
  if (CheckInput("jump") && owner.RigidBody.IsOnGround()) {
    this.lastJumpStartHeight = owner.transform.translation.y;
    owner.RigidBody.ApplyLinearImpulse(0, this.initialJumpForce);
    this.SetState("jumping");
    owner.SoundEmitter.soundCue = "event:/player-jump";
    owner.SoundEmitter.startPlaying = true;
    owner.space.FindEntityByName("Footsteps").SoundEmitter.startPlaying = false;
    return true;
  }
  return false;
};
PlayerController.prototype.DefaultJumpHold = function(owner) {
  // jumpHoldTimer updated in Update()
  if (CheckInput("jump_hold") && this.jumpHoldTimer <= this.maxJumpHoldDuration) {
    owner.RigidBody.ApplyLinearForce(0, this.jumpHoldForce);
    owner.Animator.Set("plr_jump");
    return true;
  }
  // Release
  this.SetState("airIdle");
  return false;
};
PlayerController.prototype.DefaultRun = function(owner) {
  if (CheckInput("move_left") || CheckInput("move_right")) {
    this.SetState("run");
    owner.Animator.Set("plr_idlerun");
    owner.Animator.Queue("plr_run");
    owner.space.FindEntityByName("Footsteps").SoundEmitter.startPlaying = true;
    return true;
  }
  else {
    // Break
    owner.RigidBody.velocity.x *= this.breakSpeed;
  }
  return false;
};
PlayerController.prototype.DefaultPunch = function(owner, dt) {
  if (CheckInput("attack_punch")) {
    if (this.holdingObject)
      this.Throw(owner, dt);

    if (owner.RigidBody.IsOnGround()) {
      owner.space.FindEntityByName("Footsteps").SoundEmitter.startPlaying = false;
      owner.Animator.Set("plr_punch1");
      this.punchCounter = 0;
      this.SetState("punch");
      this.nextPunchQueued = false;
      this.UpdateFacingDirection(owner);

      owner.SoundEmitter.soundCue = "event:/player-punchwhoosh";
      owner.SoundEmitter.startPlaying = true;
    }
    else {
      owner.Animator.Set("plr_airpunch");
      this.SetState("airPunch");
      owner.SoundEmitter.soundCue = "event:/player-punchwhoosh";
      owner.SoundEmitter.startPlaying = true;
      this.UpdateFacingDirection(owner);
      this.airPunchUsed = true;
    }
    return true;
  }
  return false;
};
PlayerController.prototype.Pickup = function(owner, dt) {
  // Pickup an object TOBE this after we have the b holding animation thing
  //if (CheckInput("pickup_hold") && this.interactiveTargetID != 0 && !this.holdingObject)
  //{
  //  this.pickupTimer += dt;
  //
  //  if (this.pickupTimer >= this.pickupTime) {
  //    var object = Omni.FindEntityByID(this.interactiveTargetID);
  //
  //    object.DispatchEvent("pickup", {});
  //
  //    this.pickupTimer = 0;
  //    this.holdingObject = true;
  //  }
  //}

  var object = null;

  if (CheckInput("pickup_pressed") && this.interactiveTargetID !== 0 && !this.holdingObject) {
    object = Omni.FindEntityByID(this.interactiveTargetID);

    object.DispatchEvent("pickup", {});

    this.holdingObject = true;

    return;
  }

  if (CheckInput("pickup_pressed") && this.interactiveTargetID !== 0 && this.holdingObject) {
    this.Drop();
  }
};
PlayerController.prototype.Throw = function(owner, dt) {
  var object = Omni.FindEntityByID(this.interactiveTargetID);

  object.DispatchEvent("throw", {});

  this.pickupTimer = 0;
  this.interactiveTargetID = 0;
  this.holdingObject = false;
};

PlayerController.prototype.Drop = function() {
  object = Omni.FindEntityByID(this.interactiveTargetID);

  object.DispatchEvent("drop", {});

  this.pickupTimer = 0;
  this.holdingObject = false;
  this.interactiveTargetID = 0;
}

PlayerController.prototype.GroundPound = function(owner) {
  if (CheckInput("attack_punch_hold")) {
    this.SetState("groundPound");
    owner.Animator.Set("plr_chargepunch");
    this.groundPoundReleased = false;
    return true;
  }
  return false;
};

PlayerController.prototype.DefaultEvade = function(owner) {

  // Wait for cooldown
  if (this.evadeCooldownTimer < this.evadeCooldown)
    return false;

  // Dash in the direction the player is facing
  if (CheckInput("evadeRight") || CheckInput("evadeLeft")) {
    owner.SoundEmitter.soundCue = "event:/player-evadewhoosh";
    owner.SoundEmitter.startPlaying = true;
    owner.RigidBody.SetGroup(CollisionGroup.Evade);
    owner.Animator.Set("plr_evade");
    this.SetState("evade");
    
    // directional dashing
    if (CheckInput("evadeRight"))
      owner.RigidBody.ApplyLinearImpulse(this.initialEvadeBurst, 0);
    else
      owner.RigidBody.ApplyLinearImpulse(-this.initialEvadeBurst, 0);
      
    owner.space.FindEntityByName("Footsteps").SoundEmitter.startPlaying = false;
    this.evadeCooldownTimer = 0;
    return true;
  }

  return false;
};
PlayerController.prototype.UpdateFacingDirection = function(owner) {
  // Check direction
  if (CheckInput("move_left")) {
    owner.Sprite.flipX = true;
    this.facing = Facing.Left;
  }
  if (CheckInput("move_right")) {
    owner.Sprite.flipX = false;
    this.facing = Facing.Right;
  }
};

////////////////////////////////////////////////////////////////////////////////
// STATES
////////////////////////////////////////////////////////////////////////////////
PlayerController.prototype.states = {};

PlayerController.prototype.states.idle = function(owner, dt) {
  if (this.Pickup(owner, dt)) return;
  if (this.DefaultAirborne(owner)) return;
  if (this.DefaultEvade(owner)) return;
  if (this.DefaultJump(owner)) return;
  if (this.DefaultPunch(owner, dt)) return;
  if (this.DefaultRun(owner)) return;
};
PlayerController.prototype.states.airIdle = function(owner, dt) {
  if (this.Pickup(owner, dt)) return;
  if (this.DefaultEvade(owner)) return;
  if (this.DefaultOnGround(owner, true)) return;
  if (this.DefaultPunch(owner, dt)) return;

  // Slow upward velocity
  if (owner.RigidBody.velocity.y > 0)
    owner.RigidBody.velocity.y *= this.airBreakScalar;

  this.jumpHoverTimer += dt;
  // Add extra gravity after hover
  if(this.jumpHoverTimer > this.jumpHoverDuration) {
    owner.RigidBody.ApplyLinearForce(0, -this.extraFallGravity);
  }

  if (CheckInput("move_left")) {
    owner.RigidBody.ApplyLinearForce(-this.airControlForce, 0);
    owner.Sprite.flipX = true;
    this.facing = Facing.Left;
  }
  else if (CheckInput("move_right")) {
    owner.RigidBody.ApplyLinearForce(this.airControlForce, 0);
    owner.Sprite.flipX = false;
    this.facing = Facing.Right;
  }

  else {
    // Break
    owner.RigidBody.velocity.x *= this.breakSpeed;
  }
  
  if (owner.RigidBody.velocity.y > 0)
    owner.Animator.Set("plr_jump");
  else
    owner.Animator.Set("plr_fall");
};
PlayerController.prototype.states.jumping = function(owner, dt) {
  if (this.DefaultEvade(owner)) return;
  if (this.DefaultPunch(owner, dt)) return;
  if (this.DefaultJumpHold(owner)) return;
};
PlayerController.prototype.states.run = function(owner, dt) {
  if (this.DefaultAirborne(owner)) return;
  if (this.DefaultEvade(owner)) return;
  if (this.DefaultJump(owner)) return;
  if (this.DefaultPunch(owner, dt)) return;

  // Movement
  if (CheckInput("move_left")) {
    owner.RigidBody.ApplyLinearForce(-this.accelSpeed, 0);
    owner.Sprite.flipX = true;
    this.facing = Facing.Left;

    if (owner.RigidBody.velocity.x > 0) {
      owner.Animator.Set("plr_turnaround");
      this.SetState("turnaround");
    }
  }
  else if (CheckInput("move_right")) {
    owner.RigidBody.ApplyLinearForce(this.accelSpeed, 0);
    owner.Sprite.flipX = false;
    this.facing = Facing.Right;

    if (owner.RigidBody.velocity.x < 0) {
      owner.Animator.Set("plr_turnaround");
      this.SetState("turnaround");
    }
  }
  else if (Math.abs(owner.RigidBody.velocity.x) < this.stopThreshold) {
    this.SetState("idle");
    owner.Animator.Set("plr_idle");
    owner.RigidBody.velocity.x *= 0;
    owner.space.FindEntityByName("Footsteps").SoundEmitter.startPlaying = false;
  }
  else {
    // Break
    owner.RigidBody.velocity.x *= this.breakSpeed;
  }

  // allows you to pickup while running
  if (this.Pickup(owner, dt)) return;

};
PlayerController.prototype.states.turnaround = function(owner, dt) {
  if (this.Pickup(owner, dt)) return;
  if (this.DefaultAirborne(owner)) return;
  if (this.DefaultEvade(owner)) return;
  if (this.DefaultJump(owner)) return;
  if (this.DefaultPunch(owner, dt)) return;

  // Movement
  if (CheckInput("move_left")) {
    owner.RigidBody.ApplyLinearForce(-this.accelSpeed, 0);
    owner.Sprite.flipX = true;
    this.facing = Facing.Left;

    if (owner.RigidBody.velocity.x <= 0) {
      if (this.DefaultRun(owner)) return;
    }
  }
  else if (CheckInput("move_right")) {
    owner.RigidBody.ApplyLinearForce(this.accelSpeed, 0);
    owner.Sprite.flipX = false;
    this.facing = Facing.Right;

    if (owner.RigidBody.velocity.x >= 0) {
      if (this.DefaultRun(owner)) return;
    }
  }
  else if (Math.abs(owner.RigidBody.velocity.x) < this.stopThreshold) {
    this.SetState("idle");
    owner.Animator.Set("plr_idle");
    owner.RigidBody.velocity.x *= 0;
  }
  else {
    // Break
    owner.RigidBody.velocity.x *= this.breakSpeed;
  }
};
PlayerController.prototype.states.punch = function(owner, dt) {
  // If timed correctly, go right into the next punch (+momentum)
  var punchForce = 10;
  var xvel = 0;
  
  var facingLeft = CheckInput("move_left");
  var facingRight = CheckInput("move_right");
  
  // only apply the forces if the player is inputting left or right
  if (facingLeft || facingRight) {
    if (owner.Animator.HasFlag("strongforce"))
      xvel = (owner.RigidBody.velocity.x + punchForce * this.facing).clamp(-punchForce, punchForce);
    if (owner.Animator.HasFlag("force"))
      xvel = (owner.RigidBody.velocity.x + punchForce * this.facing * 0.25).clamp(-punchForce, punchForce);
  }
  
  // if facing left or right, change facing
  this.UpdateFacingDirection(owner);
  
  owner.RigidBody.velocity.x = Ease.Easy(owner.RigidBody.velocity.x, xvel, 0.1);

  // if first frame of every animation
  if (owner.Animator.frame == 0)
    this.punchChained = false;
    
  // if third hit, increase damage
  if (owner.Animator.HasFlag("hit3"))
    this.punchDamage = 30;
    
  // if back to first hit, reduce damage
  if(owner.Animator.HasFlag("hit1"))
    this.punchDamage = 10;
  
  if (owner.Animator.HasFlag("final")) {
    this.punchCounter = 0;
    this.punchChained = false;
    if (this.DefaultEvade(owner)) return;
    if (this.DefaultJump(owner)) return;
    if (this.DefaultRun(owner)) return;
  }

  // if this is a follow up frame
  if (owner.Animator.HasFlag("followup")) {
    // if we already have a queued up attack, just do that
    if (this.punchChained) {
      // Move to next animation
      this.punchChained = false;
      if (this.punchCounter === 1) {
        owner.SoundEmitter.soundCue = "event:/player-punchwhoosh";
        owner.SoundEmitter.startPlaying = true;
        owner.Animator.Set("plr_punch2");
      }
      if (this.punchCounter == 2) {
        owner.SoundEmitter.soundCue = "event:/player-punchwhoosh";
        owner.SoundEmitter.startPlaying = true;
        owner.Animator.Set("plr_punch3");
      }
      return;
    }
  
    // if no queued up punch, and if the player presses attack, follow up the attack
    if (CheckInput("attack_punch") && !this.MoreStates()) {
      // Move to next animation
      if (this.punchCounter === 0) {
        owner.Animator.Set("plr_punch2");
      }
      if (this.punchCounter == 1)
        owner.Animator.Set("plr_punch3");

      ++this.punchCounter;
      this.punchChained = false;
      if (this.punchCounter > 2) {
        this.punchCounter = 0;
      }

      this.punchChained = false;
      this.UpdateFacingDirection(owner);
      owner.SoundEmitter.soundCue = "event:/player-punchwhoosh";
      owner.SoundEmitter.startPlaying = true;
      return;
    }
  }

  if (CheckInput("attack_punch") && owner.Animator.HasFlag("queue") && !this.punchChained && !this.MoreStates()) {
    ++this.punchCounter;
    this.punchChained = true;
    if (this.punchCounter > 2)
      this.punchCounter = 0;
  }

  // End the animation
  if (owner.Animator.animationDone || (this.MoreStates() && owner.Animator.HasFlag("skippable"))) {
    this.punchChained = false;
    this.punchCounter = 0;
    if (this.MoreStates()) {
      //this.PopState();
    }
    
    if (this.DefaultAirborne(owner)) return;
    if (this.DefaultOnGround(owner)) return;
  }

  // if it's skippable, and we don't have a punch lined up, allow to cancel into run
  if (!this.punchChained && owner.Animator.HasFlag("skippable")) {
    if (this.DefaultRun(owner)) return;
  }
  
  if (this.DefaultEvade(owner)) return;
  if (this.DefaultJump(owner)) return;
};
PlayerController.prototype.states.groundPound = function(owner, dt) {
  this.groundPoundTimer += dt;

  // Start chain and end anim
  if (this.groundPoundReleased && owner.Animator.animationDone) {
    var a = new Vec2(owner.transform.translation.x + (0.75 * this.facing), owner.transform.translation.y);
    var newObj = owner.space.CreateEntity({name : "GroundPound", position : a, archetype : "GroundPound"});
    newObj.GetComponent("GroundPound").leftInChain = 5;
    newObj.GetComponent("GroundPound").chainDirection = this.facing;

    this.SetState("idle");
    owner.Animator.Set("plr_idle");
  }

  // Execute ground pound
  if (!CheckInput("attack_punch_hold") && this.groundPoundReleased === false) {
    owner.Animator.Set("plr_groundpound");
    this.groundPoundTimer = 0;
    this.groundPoundReleased = true;
  }
};
PlayerController.prototype.states.airPunch = function(owner, dt) {
  if (this.DefaultOnGround(owner, true)) return;

  // If still moving up
  if (owner.RigidBody.velocity.y > 0)
    owner.RigidBody.velocity.y *= 0.98;
  /*if (owner.RigidBody.velocity.y <= 0)
    owner.RigidBody.velocity.y *= 1.05;*/
  /*owner.RigidBody.velocity.y = -0.01;
  owner.RigidBody.velocity.x *= 0.8;*/

  this.jumpHoverTimer += dt;
  // Add extra gravity after hover
  if(this.jumpHoverTimer > this.jumpHoverDuration) {
    owner.RigidBody.ApplyLinearForce(0, -this.extraFallGravity);
  }

  // End the animation
  if (owner.Animator.animationDone) {
    if (this.DefaultAirborne(owner)) return;
    if (this.DefaultOnGround(owner)) return;
  }
};
PlayerController.prototype.states.evade = function(owner, dt) {
  // If timed correctly, punch
  if (CheckInput("attack_punch") && owner.Animator.HasFlag("followup") && !this.MoreStates()) {
    owner.RigidBody.SetGroup(CollisionGroup.Player);
    owner.Animator.Set("plr_punch1");
    this.SetState("punch");
    this.UpdateFacingDirection(owner);
    owner.RigidBody.velocity.x = 0;
    return;
  }

  // If timed correctly, jump
  if (CheckInput("jump") && owner.Animator.HasFlag("followup") && !this.MoreStates()) {
    if (this.DefaultJump(owner)) return;
    return;
  }

  if (owner.Animator.animationDone) {
    // End the evade
    owner.RigidBody.SetGroup(CollisionGroup.Player);
    owner.Animator.Set("plr_sticklanding");
    owner.Animator.Queue("plr_idle");
    this.SetState("idle");
    owner.RigidBody.velocity.x *= 0.5;
  }
};
PlayerController.prototype.states.stun = function(owner, dt) {
  if (owner.Animator.animationDone) {
    owner.Animator.Set("plr_idle");
    this.SetState("idle");
  }
};

////////////////////////////////////////////////////////////////////////////////
// EVENTS
////////////////////////////////////////////////////////////////////////////////
PlayerController.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  var body = owner.RigidBody;
  var sprite = owner.Sprite;

  if (this.CurrentState() != this.states.jumping)
    this.evadeCooldownTimer += updateEvent.dt;

  // dash takes priority
  if (this.dashingThrough) {
    // tween dash
    this.dashThroughTimer += updateEvent.dt;
    owner.transform.translation.x = this.originalX + this.dashDistance * (this.dashThroughTimer / this.dashThroughTime);

    // once done dashing, make it collide with enemies again
    if (this.dashThroughTimer > this.dashThroughTime) {
      this.dashingThrough = false;
      owner.RigidBody.SetGroup(CollisionGroup.Player);
    }
  }

  else {
    if (this.CurrentState() == this.states.jumping)
      this.jumpHoldTimer += updateEvent.dt;

    if (this.active) {
      if (this.StateCount() > 0)
        this.CurrentState().call(this, owner, updateEvent.dt);

      // Clamp movement speed
      if (this.CurrentState() != this.states.evade) {
        var currSpeed = owner.RigidBody.velocity.x;
        if (currSpeed > this.maxMoveSpeed)
          owner.RigidBody.velocity.x = this.maxMoveSpeed;
        if (currSpeed < -this.maxMoveSpeed)
          owner.RigidBody.velocity.x = -this.maxMoveSpeed;
      }
    }
  }
};

PlayerController.prototype.onFrameUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  var body = owner.RigidBody;
  var sprite = owner.Sprite;

  Utilities.UpdateSlow(owner, updateEvent);
};

var DamageEvent = function (damage) {
  this.damage = damage;
  this.sourceType = "";
};

var KnockbackEvent = function (otherObject) {
  this.otherObject = otherObject;
};

PlayerController.prototype.onHitStart = function () {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  if (hitEvent.otherEntity === undefined)
    return;

  // If my type is damage and his type is body
  /////////////////////////////////////
  //       SEND ATTACK
  /////////////////////////////////////
  if (hitEvent.otherEntity.HasComponent("Throwable"))
    if (hitEvent.otherEntity.GetComponent("Throwable").thrown)
      return;

  if (hitEvent.myType == HitBoxType.Damage && hitEvent.otherType == HitBoxType.Body) {
    // activate the hit effect
    var punchEffectEmitter = this.punchEffect.ParticleEmitter;
    punchEffectEmitter.minVelocity.x = this.facing * this.particleMinXVel;
    punchEffectEmitter.maxVelocity.x = this.facing * this.particleMaxXVel;
    punchEffectEmitter.active = true;
    
    this.punchHitEffect.ParticleEmitter.active = true;
    
    
    var damageEvent = new DamageEvent(this.punchDamage * (owner.GetComponent("PlayerLogic").godMode ? 2 : 1));
    damageEvent.direction = this.facing;
    hitEvent.otherEntity.DispatchEvent("damageEvent", damageEvent);
    owner.space.camera.Shake(0.1, 0.15, 0, 1);
    owner.GetComponent("PlayerLogic").IncreaseMomentum(1);
  }

  // Check for interactive objects
  if (hitEvent.myType == HitBoxType.Body && hitEvent.otherType == HitBoxType.Body && hitEvent.otherEntity.HasComponent("Throwable")) {
    if (this.interactiveTargetID === 0 && !this.holdingObject) {
      hitEvent.otherEntity.DispatchEvent("selectEvent", {});
      this.interactiveTargetID = hitEvent.otherID;
    }
  }
};

PlayerController.prototype.onHitPersist = function () {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  // TODO: Stuck under an enemy
};

PlayerController.prototype.onVaultEvent = function () {
  var owner = arguments[0];
  var vaultEvent = arguments[1];

  owner.RigidBody.ApplyLinearImpulse(vaultEvent.impulse * this.facing, 0);
};

PlayerController.prototype.onHitEnd = function () {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  // only deselect if we're not holding it
  if (!this.holdingObject) {
    // Check for interactive objects
    if (hitEvent.myType == HitBoxType.Body && hitEvent.otherType == HitBoxType.Body) {
      // unlink if it is current target
      if (hitEvent.otherID == this.interactiveTargetID) {
        // make sure it's alive before sending event
        var otherEntity = hitEvent.otherEntity;
        if (otherEntity)
          otherEntity.DispatchEvent("deselectEvent", {});
        this.interactiveTargetID = 0;
        if (this.holdingObject)
          this.holdingObject = false;
      }
    }
  }
};

PlayerController.prototype.SetCameraToFollow = function (owner) {
  //owner.space.camera.Follow(owner);
  //owner.space.camera.size = 8;
  //owner.space.camera.SetOffset(3,1);
};

PlayerController.prototype.onKnockback = function (owner) {
  var knockbackEvent = arguments[1];

  var direction = knockbackEvent.otherObject.transform.translation.x - owner.transform.translation.x;
  if (direction > 0)
    owner.RigidBody.ApplyLinearImpulse(-5,0);
  else
    owner.RigidBody.ApplyLinearImpulse(5,0);
};


PlayerController.prototype.reactivate = function () {
  this.active = true;
  this.SetState("idle");
}
