/******************************************************************************
Filename: ChangeLevelBooth.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var ChangeLevelBooth = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.chargeBooth = 0;

  this.activated = false;

};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

ChangeLevelBooth.Create = function() {
  // required function; do not remove
  return new ChangeLevelBooth(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

ChangeLevelBooth.properties = {
  "level":       Property(Type.String_, "Trains")
};

//////////////////
//  INITIALIZE  //
//////////////////

ChangeLevelBooth.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  //Omni.Connect(this, "space", "PlayerDeath", owner, 0, this.ReleaseCamera);
  
  Omni.Connect(this, "entity", "onCollisionStart", owner, this.onCollisionStart);
  
  // create the actual ChangeLevelBooth
  this.chargeBooth = owner.space.CreateEntity(
      {"name" : "ChangeLevelBooth_ChargeBooth", "archetype" : "ChangeLevelBoothSprite", 
      "position" : {"x" : owner.transform.translation.x, "y" : owner.transform.translation.y}});
      
  // make my own sprite invisible
  owner.Sprite.visible = false;
   
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
ChangeLevelBooth.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
};


ChangeLevelBooth.prototype.onCollisionStart = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];
  
  // if we collide with the player, change color to green
  if (collisionEvent.otherEntity.name == "Player") {
    if (!this.activated) {
      var loader = owner.space.CreateEntity({"name" : "changeLevelInitiator", "archetype" : "ChangeLevelInitiator", "position" : {"x" : 0, "y" : 0}});
      loader.GetComponent("ChangeLevelInitiator").level = this.level;
      owner.ParticleEmitter.active = true;
      owner.space.FindEntityByName("Footsteps").SoundEmitter.startPlaying = false;
      this.activated = true;
    }
    
  }
};
