/******************************************************************************
Filename: BackgroundMusic.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// constructor
var BackgroundMusic = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member variables
  this.pauseValue = 0;
  this.active = true;
};

// boilerplate create function
BackgroundMusic.Create = function() {
  return new BackgroundMusic(arguments[0]);
};

////////////////////////////////////////////////////////////////////////////////
// PROPERTIES
////////////////////////////////////////////////////////////////////////////////

BackgroundMusic.properties = {
  "music":                        Property(Type.String_, "event:/game-song"),
  "startPlayingOnInitialize" :    Property(Type.Bool, true),
  "tutorialMusic" :               Property(Type.Bool, false)
};

// function called after
BackgroundMusic.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "game", "musicStart", owner, this.onMusicStart);
  Omni.Connect(this, "game", "musicStop", owner, this.onMusicStop);

  owner.SoundEmitter.soundCue = this.music;
  owner.SoundEmitter.startPlaying = this.startPlayingOnInitialize;
  this.wasPlaying = this.startPlayingOnInitialize;
};

////////////////////////////////////////////////////////////////////////////////
// EVENTS
////////////////////////////////////////////////////////////////////////////////
BackgroundMusic.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (this.active) {
    if (this.tutorialMusic) // TUTORIAL MUSIC
    {
      if (Omni.FindSpaceByName("main").paused)
      {
        this.pauseValue -= 0.05; // Ease back to 0.0
        if (this.pauseValue < 0.0)
          this.pauseValue = 0.0;
		owner.SoundEmitter.volume = 0.4;
      }
      else
      {
        this.pauseValue += 0.05; // Ease up to 1.0
        if (this.pauseValue > 1.0)
          this.pauseValue = 1.0;
		owner.SoundEmitter.volume = 1;
      }
      owner.SoundEmitter.SetParameter(0, this.pauseValue);
    }
    else // GAME SONG
    {
      if (Omni.FindSpaceByName("main").paused)
      {
        this.pauseValue += 0.05; // Ease up to 0.7
        if (this.pauseValue > 0.7)
          this.pauseValue = 0.7;
      }
      else
      {
        this.pauseValue -= 0.05; // Ease back to 0.0
        if (this.pauseValue < 0.0)
          this.pauseValue = 0.0;
      }
      owner.SoundEmitter.SetParameter(1, this.pauseValue);
    }


    
  }

};

BackgroundMusic.prototype.onMusicStart = function() {
  var owner = arguments[0];

  owner.SoundEmitter.startPlaying = this.wasPlaying;
};

BackgroundMusic.prototype.onMusicStop = function() {
  var owner = arguments[0];
  
  this.wasPlaying = owner.SoundEmitter.startPlaying;

};
