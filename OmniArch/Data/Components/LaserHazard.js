/******************************************************************************
Filename: LaserHazard.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var LaserHazard = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.damage = 10;
  this.attacking = false;
  this.attackDuration = 3;
  this.attackTimer = 0;
  this.attackCooldown = 3;

  this.active = true;

  this.flipTimer = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

LaserHazard.Create = function() {
  // required function; do not remove
  return new LaserHazard(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

LaserHazard.properties = {
  "attackCooldown": Property(Type.Float, 2),
  "attackDuration": Property(Type.Float, 2.5),
  "delay": Property(Type.Bool, false),
  "chainDeactivate": Property(Type.Int, 1),
  "damage": Property(Type.Float, 10)
};

//////////////////
//  INITIALIZE  //
//////////////////

LaserHazard.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "onHitPersist", owner, this.onHitPersist);
  Omni.Connect(this, "entity", "deactivate", owner, this.onDeactivate);

  owner.Animator.Set("laserhazard");
  owner.Sprite.brightness = 0;

  if (this.delay)
    this.attackTimer = this.attackCooldown;

  owner.SoundEmitter.soundCue = "event:/electric-fence";
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
LaserHazard.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!owner.OnScreen())
    owner.SoundEmitter.startPlaying = false;

  this.attackTimer += updateEvent.dt;

  if (this.attackTimer >= this.attackCooldown - 0.5 && this.active)
  {
    if (!this.attacking)
      owner.ParticleEmitter.active = true;
    if (owner.SoundEmitter.startPlaying == false && owner.OnScreen())
    {
      owner.SoundEmitter.soundCue = "event:/electric-fence";
      owner.SoundEmitter.startPlaying = true;
    }
  }
  else
  {
    owner.ParticleEmitter.active = false;
    owner.SoundEmitter.startPlaying = false;
  }

  if (this.attackTimer >= this.attackCooldown) {
    this.attacking = true;
    owner.Sprite.brightness = 5;
  }

  if (this.attacking && this.attackTimer >= this.attackDuration + this.attackCooldown) {
    this.attacking = false;
    owner.Sprite.brightness = 0;
    this.attackTimer = 0;
    owner.SoundEmitter.startPlaying = false;

  }

  this.flipTimer += updateEvent.dt;
  if (this.flipTimer > 0.1) {
    owner.Sprite.flipX = !owner.Sprite.flipX;
    this.flipTimer = 0;
  }

};

LaserHazard.prototype.onDeactivate = function() {
  var owner = arguments[0];

  this.active = false;
  owner.Sprite.glowColor.a = 0;
  owner.RigidBody.ghost = true;
  owner.SoundEmitter.startPlaying = false;

  var chain = Omni.FindEntityByLinkID(this.chainDeactivate);
  if (chain != undefined)
    chain.DispatchEvent("deactivate", {});
};

LaserHazard.prototype.onHitPersist = function() {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  if (!this.attacking)
    return;

  if (!this.active || hitEvent.otherEntity === undefined)
    return;

  if (hitEvent.otherType == HitBoxType.Body && hitEvent.otherEntity.GetComponent("PlayerLogic")) {
    hitEvent.otherEntity.DispatchEvent("damageEvent", new DamageEvent(this.damage));
    hitEvent.otherEntity.DispatchEvent("knockbackEvent", new KnockbackEvent(owner));
  }
};
