/******************************************************************************
Filename: SlamDroneController.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// constructor
var SlamDroneController = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  this.active = true;

  this.timer = 0;
  this.idleTime = 0.9;
  this.chargeTime = 0.45;
  this.charging = false;
  this.targetX = 0;
  this.targetY = 0;
  this.attacking = false;
  this.playerLinkID = 1;
  this.tracking = false;
  this.directionToMove = 1;
  this.amountToMove = 3;
  this.positionDelta = 0.05;
  this.originalY = 0;
  this.attackVelocity = 15;
  this.recover = false;
  this.onLeaveScreen = false;
  this.recoverVelocity = 3;
  this.cooldown = 1;
  this.cooledDown = true;
};


// boilerplate create function
SlamDroneController.Create = function() {
  return new SlamDroneController(arguments[0]);
};

// function called after
SlamDroneController.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "onHitStart", owner, this.onHitStart);
  Omni.Connect(this, "entity", "changeState", owner, this.changeState);
  this.originalY = owner.transform.translation.y;
  this.playerID = Omni.GetUniqueIDFromLinkID(this.playerLinkID);
  owner.Animator.Set("slam_idle");
};

// helper function to clear states
SlamDroneController.prototype.ClearStates = function (owner) {
  this.charging = false;
  this.attacking = false;
  this.tracking = false;
  this.recover = false;
  this.onLeaveScreen = false;
  owner.RigidBody.velocity.x = 0;
  owner.RigidBody.velocity.y = 0;
  owner.transform.translation.y = this.originalY;
};

////////////////////////////////////////////////////////////////////////////////
// EVENTS
////////////////////////////////////////////////////////////////////////////////
SlamDroneController.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!this.active)
    return;

  if (owner.GetComponent("Drone").flyAway)
    return;

  if (owner.OnScreen())
  {
    this.onLeaveScreen = true;
    this.timer += updateEvent.dt;

    // if tracking, track to position
    if (this.tracking)
    {
      // track to the player position
      var player = Omni.FindEntityByLinkID(this.playerLinkID);
      this.targetX = player.transform.translation.x;
      this.targetY = player.transform.translation.y;

      // set the direction to move
      if (owner.transform.translation.x > this.targetX)
        this.directionToMove = -1;
      else
        this.directionToMove = 1;

      owner.RigidBody.velocity.x = this.directionToMove * this.amountToMove;

      // if above player, stop tracking, start charging
      if (Math.abs(owner.transform.translation.x - this.targetX) < this.positionDelta)
      {
        this.tracking = false;
        owner.RigidBody.velocity.x = 0;
        this.charging = true;
        this.originalY = owner.transform.translation.y;
        this.timer = 0;
      }
    }

    // charging
    else if (this.charging)
    {
      // once charge is done, attack!
      if (this.timer > this.chargeTime)
      {
        this.charging = false;
        this.attacking = true;
        this.timer = 0;
        this.originalY = owner.transform.translation.y;
        owner.RigidBody.velocity.y = -this.attackVelocity;
      }
    }

    // attacking
    else if (this.attacking)
    {
      // once at target, stop attacking and move back up
      if (owner.transform.translation.y < this.targetY)
      {
        owner.transform.translation.y = this.targetY;
        this.attacking = false;
        this.recover = true;
        this.timer = 0;
        this.cooledDown = false;
        owner.RigidBody.velocity.y = 0;


        var a = new Vec2(owner.transform.translation.x, owner.transform.translation.y - 0.5);
        owner.space.CreateEntity({name : "SlamParticle", position : a, archetype : "SlamParticle"});
      }
    }

    // recovering
    else if (this.recover)
    {
      // cooldown time
      if (!this.cooledDown && this.timer > this.cooldown) 
      {
        owner.RigidBody.velocity.y = this.recoverVelocity;
        this.cooledDown = true;
      }
    
      // once recovered, go back to default state
      if (owner.transform.translation.y > this.originalY )
      {
        this.timer = 0;
        this.recover = false;
        owner.RigidBody.velocity.y = 0;
      }
    }

    // if timer is greater than idle time, make him track the player
    else if (this.timer > this.idleTime)
    {
      this.timer = 0;
      this.tracking = true;
    }
  }
  else
  {
    if (this.onLeaveScreen)
      this.ClearStates(owner);
  }

};

SlamDroneController.prototype.onHitStart = function () {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  /////////////////////////////////////
  //       SEND ATTACK
  /////////////////////////////////////
  if (hitEvent.myType == HitBoxType.Damage && hitEvent.otherType == HitBoxType.Body && this.attacking && hitEvent.otherEntity.GetComponent("PlayerLogic")) {
    var damageEvent = new DamageEvent(10);
    hitEvent.otherEntity.DispatchEvent("damageEvent", damageEvent);
    owner.space.camera.Shake(0.1, 0.15, 0, 1);
  }

};

SlamDroneController.prototype.changeState = function() {
  var owner = arguments[0];
  var stateEvent = arguments[1];

  if (stateEvent.newState == EnemyStates.Idle) {
  }

  if (stateEvent.newState == EnemyStates.Hostile) {
  }

  if (stateEvent.newState == EnemyStates.Defeated) {
    this.active = false;
  }
};
