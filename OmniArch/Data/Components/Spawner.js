/******************************************************************************
Filename: Spawner.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var Spawner = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.spawning = false;
  this.spawnRate = 0.3;
  this.spawnTimer = 0;
  this.burstTimer = 0;
  this.burstCooldown = 3;
  this.enemiesSpawned = 0;
  this.enemiesToSpawn = 1;
  this.continuous = true;
  this.active = true;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Spawner.Create = function() {
  // required function; do not remove
  return new Spawner(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

Spawner.properties = {
  "waveNumber": Property(Type.Unsigned, 0),
  "enemyType":  Property(Type.String_, "BallEnemy"),
  "enemiesToSpawn": Property(Type.Unsigned, 1),
  "continuous": Property(Type.Bool, true),
  "maxX" : Property(Type.Float, 0),
  "minX" : Property(Type.Float, 0),
  "burstCooldown" : Property(Type.Float, 3),
  "spawnRate" : Property(Type.Float, 0.3),
  "moveAfterSpawn" : Property(Type.Bool, false),
  "moveBy"  : Property(Type.Float, 1.2),
  "offsetX" : Property(Type.Float, 0.0),
  "offsetY" : Property(Type.Float, 0.0),

};

//////////////////
//  INITIALIZE  //
//////////////////

Spawner.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "space", "waveUpdate", owner, 0, this.onWaveUpdate);
  Omni.Connect(this, "entity", "endEncounter", owner, this.onEndEncounter);
  Omni.Connect(this, "entity", "pass", owner, this.onPass);
  Omni.Connect(this, "entity", "endLastEncounter", owner, this.onEndLastEncounter);

  this.spawnTimer = this.spawnRate;
  this.burstTimer = this.burstCooldown;
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
Spawner.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!this.active)
    return;
  
  this.CheckIfSpawning(owner);
  
  if (this.spawning) {
    ////////////////
    // Continuous //
    ////////////////
    if (this.continuous)
    {

      this.burstTimer += updateEvent.dt;
      if (this.burstTimer >= this.burstCooldown)
      {
        this.spawnTimer += updateEvent.dt;
      }
    }
    //////////
    // Wave //
    //////////
    else
      this.spawnTimer += updateEvent.dt;

    this.SpawnEnemies(owner);
  }


};

Spawner.prototype.CheckIfSpawning = function(owner) {
  // Wave based spawners are handled through events
  if (!this.continuous && this.waveNumber > 0)
    return;

  var player = owner.space.FindEntityByName("Player");

  if (player.transform.translation.x > this.minX && player.transform.translation.x < this.maxX) {
    this.spawning = true;
    return;
  }


  this.spawning = false;
};

Spawner.prototype.SpawnEnemies = function(owner) {
  var currentEnemies = owner.space.FindEntitiesByName("Spawner"+this.owner_id).length;

  ////////////
  // Single //
  ////////////
  if (!this.continuous && this.waveNumber === 0) {
    
    if (this.enemiesSpawned < this.enemiesToSpawn) {
      if (this.spawnTimer >= this.spawnRate && currentEnemies < this.enemiesToSpawn) {

        var position = new Vec2();
        position.x = owner.transform.translation.x + this.offsetX;
        position.y = owner.transform.translation.y + this.offsetY;
        var enemy = owner.space.CreateEntity({name : "Spawner"+ this.owner_id, position : position, archetype : this.enemyType});

        if(this.moveAfterSpawn) {
          if(owner.Sprite.flipX === true) {
            enemy.GetComponent("PassOnEvents").moveRight = true;
            enemy.GetComponent("Sprite").flipX = true;
          }
          else 
            enemy.GetComponent("PassOnEvents").moveLeft = true;

          enemy.GetComponent("PassOnEvents").changePosBy = this.moveBy;
          enemy.GetComponent("PassOnEvents").receiveOnly = true;
          enemy.GetComponent("PassOnEvents").rateScale = 0.25;
          enemy.GetComponent("PassOnEvents").noGravityWhileChange = true;

          if(owner.Sprite.flipX === true)
            enemy.DispatchEvent("pass", {});
          else
            enemy.DispatchEvent("pass", {});
        }

        this.spawnTimer = 0;
        ++this.enemiesSpawned;
      }
    }

    return;
  }

  ////////////////
  // Continuous //
  ////////////////
  if (this.continuous) {
    if (this.spawnTimer >= this.spawnRate && currentEnemies < this.enemiesToSpawn) {
      
        var position = new Vec2();
        position.x = owner.transform.translation.x + this.offsetX;
        position.y = owner.transform.translation.y + this.offsetY;
        var enemy = owner.space.CreateEntity({name : "Spawner"+ this.owner_id, position : position, archetype : this.enemyType});

        if(this.moveAfterSpawn) {
          if(owner.Sprite.flipX === true) {
            enemy.GetComponent("PassOnEvents").moveRight = true;
            enemy.GetComponent("Sprite").flipX = true;
          }
          else 
            enemy.GetComponent("PassOnEvents").moveLeft = true;

          enemy.GetComponent("PassOnEvents").receiveOnly = true;
          enemy.GetComponent("PassOnEvents").changePosBy = this.moveBy;
          enemy.GetComponent("PassOnEvents").rateScale = 0.25;
          enemy.GetComponent("PassOnEvents").noGravityWhileChange = true;

          if(owner.Sprite.flipX === true)
            enemy.DispatchEvent("pass", {});
          else
            enemy.DispatchEvent("pass", {});
        }

        this.spawnTimer = 0;
        ++this.enemiesSpawned;
    }

    if (currentEnemies == this.enemiesToSpawn)
        this.burstTimer = 0;
    return;
  }

  currentEnemies = owner.space.FindEntitiesByName("Wave"+this.waveNumber).length;

  //////////
  // Wave //
  //////////
  if (this.enemiesSpawned < this.enemiesToSpawn) {
    if (this.spawnTimer >= this.spawnRate) {
      owner.space.CreateEntity({name : "Wave"+this.waveNumber, position : owner.transform.translation, archetype : this.enemyType});
      this.spawnTimer = 0;
      ++this.enemiesSpawned;
    }
  }
};

Spawner.prototype.onWaveUpdate = function() {
  var owner = arguments[0];
  var waveEvent = arguments[1];

  if (waveEvent.newWaveNumber == this.waveNumber) {
    this.spawning = true;
  } 
};

Spawner.prototype.onEndEncounter = function() {
  var owner = arguments[0];

  if (this.spawning) {
    this.spawning = false;
  } 
};

Spawner.prototype.onPass = function() {
  var owner = arguments[0];
  var passEvent = arguments[1];

  if (!this.received) {
    this.deactivate(owner);
    this.received = true;
  }
}

Spawner.prototype.deactivate = function(owner) {
  this.active = false;
  
  if (owner.Sprite.image == "enemyPipe") {
    owner.Sprite.glowColor.r = 0.75;
    owner.Sprite.glowColor.g = 0.75;
    owner.Sprite.glowColor.b = 0.75;
  }

}

Spawner.prototype.onEndLastEncounter = function(owner) {
  // destroy all my dudes
  
  var myDudes = owner.space.FindEntitiesByName("Spawner"+this.owner_id);
  
  for (var i = 0; i < myDudes.length; ++i)
  {
    myDudes[i].DispatchEvent("damageEvent", {"damage" : 10});
  }

}