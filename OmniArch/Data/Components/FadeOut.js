/******************************************************************************
Filename: FadeOut.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var FadeOut = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
  
  this.timer = 0;
  
  this.fadedIn = false;
  this.solided = false;
  this.fadedOut = false;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

FadeOut.Create = function() {
  // required function; do not remove
  return new FadeOut(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

FadeOut.properties = {
  "fadeOutTime":    Property(Type.Float, 1),
  "solidTime":      Property(Type.Float, 0),
  "fadeInTime" :    Property(Type.Float, 0),
  "active":         Property(Type.Bool, true)
};

//////////////////
//  INITIALIZE  //
//////////////////

FadeOut.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
FadeOut.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  var sprite = owner.Sprite;
  
  if (this.active) {
    this.timer += updateEvent.dt; 
    
    // fade in
    if (!this.fadedIn) {
      if (this.timer >= this.fadeInTime) {
        this.fadedIn = true;
        this.timer = 0;
        sprite.color.a = 1;
      }
      else {
        sprite.color.a = this.timer / this.fadeInTime;
      }
    }
    
    // solid
    else if (!this.solided) {
      if (this.timer >= this.solidTime) {
        this.solided = true;
        this.timer = 0;
      }
    }
    
    // fade out
    else if (!this.fadedOut) {
      if (this.timer >= this.fadeOutTime) {
        this.fadedOut = true;
        this.timer = 0;
        sprite.color.a = 0;
      }
      else {
        sprite.color.a = 1 - (this.timer / this.fadeOutTime);
      }
    }
    
    else {
      // TODO: Switch back to regular destroy
      owner.Destroy();
    }
  }
};
