/******************************************************************************
Filename: LevelIndicator.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var LevelIndicator = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.timeBeforeFade = 4;
  this.timer = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

LevelIndicator.Create = function() {
  // required function; do not remove
  return new LevelIndicator(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

LevelIndicator.properties = {
};

//////////////////
//  INITIALIZE  //
//////////////////

LevelIndicator.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);

  if (Omni.currentLevel == "Trains")
    owner.Sprite.image = "level_indicator_1";
  if (Omni.currentLevel == "ShieldGuy")
    owner.Sprite.image = "level_indicator_2";
  if (Omni.currentLevel == "First")
    owner.Sprite.image = "level_indicator_3";
  if (Omni.currentLevel == "Second")
    owner.Sprite.image = "level_indicator_4";
  if (Omni.currentLevel == "Chase")
    owner.Sprite.image = "level_indicator_5";
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
LevelIndicator.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  this.timer += updateEvent.dt;

  if (this.timer >= this.timeBeforeFade)
  {
    if (owner.Sprite.color.a > 0)
      owner.Sprite.color.a -= 0.01;
  }

  if (Omni.FindSpaceByName("main").paused)
  {
    this.timer = 0;
    owner.Sprite.color.a = 1;
  }

};
