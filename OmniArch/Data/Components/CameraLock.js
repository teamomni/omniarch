/******************************************************************************
Filename: CameraLock.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var CameraLock = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.activated = false;
  this.targetLinkID = 0;

  this.triggerOffset = 0;

  this.firstFrame = false;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

CameraLock.Create = function() {
  // required function; do not remove
  return new CameraLock(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

CameraLock.properties = {
  "targetLinkID" :  Property(Type.Unsigned, 0),
  "triggerOffset" : Property(Type.Float, 0)
};

//////////////////
//  INITIALIZE  //
//////////////////

CameraLock.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "space", "PlayerDeath", owner, 0, this.ReleaseCamera);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
CameraLock.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  var player = owner.space.FindEntityByName("Player");
  var target = Omni.FindEntityByLinkID(this.targetLinkID);

  var xThreshold = owner.transform.translation.x + this.triggerOffset;
  var yThreshold = owner.transform.translation.y;

  // Check if player crosses the x and y threshold
  if (player.transform.translation.x >= xThreshold && player.transform.translation.y < yThreshold && target && !this.activated) {
    //owner.space.camera.size = 12; // Zoom (not working?)
    this.SetCameraToFollow(owner);
    this.activated = true;
    Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic").SoundEmitter.SetParameter(0, 1);
    owner.space.FindEntityByName("Barrier1").RigidBody.ghost = false;
    owner.space.FindEntityByName("Barrier2").RigidBody.ghost = false;
  }

  // Check if condition is met
  if (this.activated) {
    if (!target)
      this.ReleaseCamera(owner);
  }
};

CameraLock.prototype.SetCameraToFollow = function(owner) {
  /*owner.space.camera.Follow(owner);
  owner.space.camera.SetOffset(0,0);*/
};

CameraLock.prototype.ReleaseCamera = function(owner) {
  var player = owner.space.FindEntityByName("Player");
  //player.GetComponent("PlayerController").SetCameraToFollow(player);
  this.activated = false;
  Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic").SoundEmitter.SetParameter(0, 0);
  owner.space.FindEntityByName("Barrier1").RigidBody.ghost = true;
  owner.space.FindEntityByName("Barrier2").RigidBody.ghost = true;
};
