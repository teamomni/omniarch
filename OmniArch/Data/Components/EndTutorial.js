/******************************************************************************
Filename: EndTutorial.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var EndTutorial = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

EndTutorial.Create = function() {
  // required function; do not remove
  return new EndTutorial(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

EndTutorial.properties = {
};

//////////////////
//  INITIALIZE  //
//////////////////

EndTutorial.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "entity", "CollisionTriggered", owner, this.onEndTutorialEnd);
};

EndTutorial.prototype.onEndTutorialEnd = function() {
  var owner = arguments[0];
  var triggerEvent = arguments[1];

  owner.SoundEmitter.soundCue = "event:/train-toot";
  owner.SoundEmitter.startPlaying = true;
  
  var camera = owner.space.camera;
  var movingBlackScreen = owner.space.CreateEntity({"name" : "movingBlackScreen", "archetype" : "ScreenWipe2", "position" : {"x" : camera.transform.translation.x + 25, "y" : camera.transform.translation.y}});
  movingBlackScreen.GetComponent("MovingBlackScreen").targetX = camera.transform.translation.x - 25;
  var movingBlackScreen = owner.space.CreateEntity({"name" : "movingBlackScreen", "archetype" : "ScreenWipe2", "position" : {"x" : camera.transform.translation.x + 50, "y" : camera.transform.translation.y}});
  movingBlackScreen.GetComponent("MovingBlackScreen").targetX = camera.transform.translation.x - 25;
  var movingBlackScreen = owner.space.CreateEntity({"name" : "movingBlackScreen", "archetype" : "ScreenWipe2", "position" : {"x" : camera.transform.translation.x + 75, "y" : camera.transform.translation.y}});
  movingBlackScreen.GetComponent("MovingBlackScreen").targetX = camera.transform.translation.x - 25;
  var movingBlackScreen = owner.space.CreateEntity({"name" : "movingBlackScreen", "archetype" : "ScreenWipe2", "position" : {"x" : camera.transform.translation.x + 100, "y" : camera.transform.translation.y}});
  movingBlackScreen.GetComponent("MovingBlackScreen").targetX = camera.transform.translation.x - 25;
  
  owner.space.FindEntityByName("Footsteps").SoundEmitter.startPlaying = false;

  owner.space.camera.Shake(0.05, 1.5, 0, 0.025);
  
  var player = owner.space.FindEntityByName("Player");
  player.GetComponent("PlayerCamera").active = false;
  
  // stop the input from the player
  player.GetComponent("PlayerController").active = false;
  
  // stop the player itself
  player.Animator.Set("plr_idle");
  player.RigidBody.velocity.x = 0;
};

