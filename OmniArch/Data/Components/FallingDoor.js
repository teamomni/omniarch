/******************************************************************************
Filename: FallingDoor.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var FallingDoor = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.drop = false;
  this.max = 2.0;

};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

FallingDoor.Create = function() {
  // required function; do not remove
  return new FallingDoor(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

FallingDoor.properties = {
  "gateLinkID":     Property(Type.Int, 1),
};

//////////////////
//  INITIALIZE  //
//////////////////

FallingDoor.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "deactivate", owner, this.onDeactivate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
FallingDoor.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  


  if(this.drop)
  {
    if(this.max > 0.0)
    {
      owner.transform.translation.y += 0.1;
      this.max -= 0.1;
    }
    else
      this.drop = false;
  }

};


FallingDoor.prototype.onDeactivate = function() {
  var owner = arguments[0];

  var bg = Omni.FindEntityByLinkID(this.gateLinkID);
  bg.DispatchEvent("deactivate", {});

  
  this.drop = true;
};

