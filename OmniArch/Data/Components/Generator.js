/******************************************************************************
Filename: Generator.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var Generator = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables

};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Generator.Create = function() {
  // required function; do not remove
  return new Generator(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

Generator.properties = {
  "gateLinkID":     Property(Type.Int, 1)
};

//////////////////
//  INITIALIZE  //
//////////////////

Generator.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "Death", owner, this.onDeath);

  //owner.Animator.Set("generator");
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
Generator.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  

};

Generator.prototype.onDeath = function() {
  owner = arguments[0];

  var gate = Omni.FindEntityByLinkID(this.gateLinkID);
  gate.DispatchEvent("deactivate", {});
}
