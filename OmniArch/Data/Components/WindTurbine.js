/******************************************************************************
Filename: WindTurbine.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var WindTurbine = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.moveSpeed = 1;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

WindTurbine.Create = function() {
  // required function; do not remove
  return new WindTurbine(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

WindTurbine.properties = {
  "rotationRate":  Property(Type.Float, 0.1),
  "moveSpeed": Property(Type.Float, 0.1)
};

//////////////////
//  INITIALIZE  //
//////////////////

WindTurbine.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
WindTurbine.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  var followCamera = owner.GetComponent("FollowCamera");

  followCamera.xOffset -= 0.01 * this.moveSpeed;
  owner.transform.rotation += 1.0 * this.rotationRate;
  //Console.WriteLine(this.moveSpeed);
};
