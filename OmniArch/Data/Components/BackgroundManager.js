/******************************************************************************
Filename: BackgroundManager.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var BackgroundManager = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.screenHeight = 4;
  this.screenWidth = 6;

  this.spawnTimer = 0;
  this.spawnCooldown = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

BackgroundManager.Create = function() {
  // required function; do not remove
  return new BackgroundManager(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

BackgroundManager.properties = {
  
};

//////////////////
//  INITIALIZE  //
//////////////////

BackgroundManager.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
BackgroundManager.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  currentDrones = owner.space.FindEntitiesByName("BackgroundDrone").length;

  this.spawnTimer += updateEvent.dt;

  if (currentDrones < 3 && this.spawnTimer > this.spawnCooldown) {
    // Spawn background drone
    var pos = new Vec2(owner.space.camera.transform.translation.x + this.screenWidth, Random.RandomRange(-this.screenHeight, this.screenHeight - 5));
    var newDrone = owner.space.CreateEntity({name : "BackgroundDrone", position : owner.space.camera.transform.translation, archetype : "BackgroundDrone"});
    newDrone.GetComponent("FollowCamera").yOffset = pos.y;

    this.spawnCooldown = 2 + Random.RandomRange(-1, 1);
    this.spawnTimer = 0;
  }
};
