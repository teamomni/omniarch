/******************************************************************************
Filename: EndOfSecondLevelScript.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var EndOfSecondLevelScript = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
  
  this.toZoomOut = false;
  this.toZoomIn = false;
  
  this.originalCameraX = 0;
  this.originalCameraY = 0;
  this.originalCameraSize = 8;
  
  this.movedX = false;
  this.movedY = false;
  this.changedSize = false;
  
  this.cameraOutTime = 1.5;
  this.cameraOutTimer = 0;
  
  this.cameraInTime = 0.8;
  this.cameraInTimer = 0;
  
  this.activated = false;
  
  this.lastObject = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

EndOfSecondLevelScript.Create = function() {
  // required function; do not remove
  return new EndOfSecondLevelScript(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

EndOfSecondLevelScript.properties = {
  "lastGeneratorLinkID": Property(Type.Int, 0),
  "firstTurretLinkID": Property(Type.Int, 0),
  
};

//////////////////
//  INITIALIZE  //
//////////////////

EndOfSecondLevelScript.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "space", "onFrameUpdate", owner, 0, this.onFrameUpdate);
  Omni.Connect(this, "entity", "EndEncounter", owner, this.onEndEncounter);
  Omni.Connect(this, "entity", "onHitStart", owner, this.onHitStart);
  Omni.Connect(this, "entity", "onHitEnd", owner, this.onHitEnd);
  
  owner.Animator.Set("endSecondLevel");
  
  this.lastObject = Omni.FindEntityByLinkID(this.lastGeneratorLinkID);
};

EndOfSecondLevelScript.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  if (this.toZoomOut) {
    var dt = updateEvent.dt;
    
    var camera = owner.space.camera;
    var camTrans = owner.space.camera.transform.translation;
    var ownerTrans = owner.transform.translation;
    
    camTrans.x = Ease.Easy(camTrans.x, ownerTrans.x, 0.1);
    camTrans.y = Ease.Easy(camTrans.y, ownerTrans.y, 0.1);
    camera.size = Ease.Easy(camera.size, 12, 0.1);
    
  }
  
  if (this.toZoomIn) {
    var dt = updateEvent.dt;
    
    var camera = owner.space.camera;
    var camTrans = owner.space.camera.transform.translation;
    var ownerTrans = owner.transform.translation;
    
    camTrans.x = Ease.Easy(camTrans.x, this.originalCameraX, 0.2);
    camTrans.y = Ease.Easy(camTrans.y, this.originalCameraY, 0.2);
    camera.size = Ease.Easy(camera.size, 8, 0.2);
  }
}
  

EndOfSecondLevelScript.prototype.onFrameUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  if (this.toZoomOut) {
    this.cameraOutTimer += updateEvent.dt;
    
    if (this.cameraOutTimer >= this.cameraOutTime) {
      this.toZoomIn = true;
      this.toZoomOut = false;
    }
  }
  
  if (this.toZoomIn) {
    this.cameraInTimer += updateEvent.dt;
    
    if (this.cameraInTimer >= this.cameraInTime) {
      this.toZoomIn = false;
      var player = owner.space.FindEntityByName("Player");
      player.GetComponent("PlayerCamera").activate(player);
    }
  }
}
  
EndOfSecondLevelScript.prototype.onHitStart = function() {
  var owner = arguments[0];
  var HitEvent = arguments[1];

  if (!this.activated) {
    if (HitEvent.otherEntity !== undefined) {
      if(HitEvent.otherEntity.HasComponent("PlayerLogic")) {
        // disable the player camera
        HitEvent.otherEntity.GetComponent("PlayerCamera").deactivate(HitEvent.otherEntity);
        
        // store the camera info
        this.originalCameraX = owner.space.camera.transform.translation.x;
        this.originalCameraY = owner.space.camera.transform.translation.y;
        this.originalCameraSize = owner.space.camera.size;
        
        // set some flags
        this.movedX = false;
        this.movedY = false;
        this.changedSize = false;
        
        // set flag to zoom out
        this.toZoomOut = true;
        
        // slow-moooo the game
        Utilities.Slow(this.cameraOutTime);
        
        // don't let it happen again
        this.activated = true;
      }
    }
  }
};

EndOfSecondLevelScript.prototype.onHitEnd = function() {
  var owner = arguments[0];
  var HitEvent = arguments[1];

  if (!this.toZoomOut) {
    if (HitEvent.otherEntity !== undefined) {
      if(HitEvent.otherEntity.HasComponent("PlayerLogic")) {
        HitEvent.otherEntity.GetComponent("PlayerCamera").activate(HitEvent.otherEntity);
      }
    }
  }
};

EndOfSecondLevelScript.prototype.onEndEncounter = function() {
  var owner = arguments[0];
  
  // disable the player camera
  var player = owner.space.FindEntityByName("Player");
  player.GetComponent("PlayerCamera").deactivate(player);
  
  // store the camera info
  this.originalCameraX = owner.space.camera.transform.translation.x;
  this.originalCameraY = owner.space.camera.transform.translation.y;
  this.originalCameraSize = owner.space.camera.size;
  
  // set some flags
  this.movedX = false;
  this.movedY = false;
  this.changedSize = false;
  
  // set flag to zoom out
  this.toZoomOut = true;
  
  // reset the timers
  this.cameraInTimer = 0;
  this.cameraOutTimer = 0;
  
  // set the zoom out time to be a bit longer
  this.cameraOutTime = 2;
  
  // slow-moooo the game
  Utilities.Slow(this.cameraOutTime);
  
  this.toZoomOut = true;
}
