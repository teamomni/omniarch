/******************************************************************************
Filename: FollowPlayer.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var FollowPlayer = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  this.active = true;

  this.xOffset = 0;
  this.yOffset = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

FollowPlayer.Create = function() {
  // required function; do not remove
  return new FollowPlayer(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

FollowPlayer.properties = {
  "xOffset": Property(Type.Float, 0),
  "active": Property(Type.Bool, true),
  "yOffset": Property(Type.Float, 0)
};

//////////////////
//  INITIALIZE  //
//////////////////

FollowPlayer.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  
  // find the player
  this.player = owner.space.FindEntityByName("Player");
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
FollowPlayer.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!this.active)
    return;

  var updatedXOffset = this.xOffset * this.player.GetComponent("PlayerController").facing;

  // Follow Player
  owner.FollowEntity(this.player, updatedXOffset, this.yOffset);
};
