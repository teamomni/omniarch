/******************************************************************************
Filename: CreditsLogic.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var CreditsLogic = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
  
  this.timer = 0;
  this.standStillTime = 2.5;
  
  this.moveSpeed = 0.01;
  this.moveDown = false;

  this.reachedEnd = false;
  
  this.endY = 20.5;

  this.musicFade = 0;

  this.musicStarted = false;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

CreditsLogic.Create = function() {
  // required function; do not remove
  return new CreditsLogic(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

CreditsLogic.properties = {
};

//////////////////
//  INITIALIZE  //
//////////////////

CreditsLogic.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
CreditsLogic.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!this.musicStarted) {
    this.timer += updateEvent.dt;

    if (this.timer >= 2.5) {
      this.timer = 0;
      this.musicStarted = true;
      Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic").SoundEmitter.startPlaying = true;
      Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic").GetComponent("BackgroundMusic").wasPlaying = true;
    }
  }
  
  if (!this.moveDown) {
    this.timer += updateEvent.dt;
    if (this.timer >= this.standStillTime) {
      this.moveDown = true;
    }
  }
  else if (!this.reachedEnd) {
    if (owner.transform.translation.y + 2.5 >= this.endY) {
      if (this.musicFade < 1)
        this.musicFade += 0.4 * updateEvent.dt;
      Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic").SoundEmitter.SetParameter(0, 0 + this.musicFade);
    }

    if (owner.transform.translation.y >= this.endY) {
      this.reachedEnd = true;
      
      this.fadeout = Omni.FindSpaceByName("ui").CreateEntity({"name" : "respawn_FadeToBlack", "archetype" : "GenericSprite", "position" : {"x" : 0, "y" : 0}});
      this.fadeout.Sprite.image = "screenFade";
      this.fadeout.Sprite.color.r = 0;
      this.fadeout.Sprite.color.g = 0;
      this.fadeout.Sprite.color.b = 0;
      this.fadeout.Sprite.color.a = 0;
      this.fadeout.Sprite.depth = -10000;
      this.timer = 0;
    }
      
    else
      owner.transform.translation.y += this.moveSpeed;
  }
  
  else {
    this.timer += updateEvent.dt;
    
    if (this.timer >= 2) {
      this.fadeout.Sprite.color.a = (this.timer - 2) / 1;
      if (this.timer >= 3) {
        this.fadeout.Sprite.color.a = 1;
        Omni.LoadLevel("Trains");
      }
    }
  }
};
