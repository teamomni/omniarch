/******************************************************************************
Filename: Chase.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var Chase = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.speed = 1.5;
  this.maxDistance = 4.25;
  this.active = false;

  this.startLocation = new Vec2();

  this.player;
  this.toKillPlayer = true;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Chase.Create = function() {
  // required function; do not remove
  return new Chase(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

Chase.properties = {
  
};

//////////////////
//  INITIALIZE  //
//////////////////

Chase.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "onCollisionStart", owner, this.onCollisionStart);
  Omni.Connect(this, "space", "PlayerDeath", owner, 0, this.onPlayerDeath);
  Omni.Connect(this, "entity", "activate", owner, this.Activate);
  Omni.Connect(this, "entity", "deactivate", owner, this.Activate);

  this.startLocation.x = owner.transform.translation.x;
  this.startLocation.y = owner.transform.translation.y;

  this.player = owner.space.FindEntityByName("Player");
  this.Deactivate(owner);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
Chase.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!this.active)
    return;

  var distanceX = this.player.transform.translation.x - owner.transform.translation.x;

  if (distanceX > this.maxDistance)
    owner.transform.translation.x = this.player.transform.translation.x - this.maxDistance;
};

Chase.prototype.onCollisionStart = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];

  if (!this.active)
    return;

  if (!this.toKillPlayer) {
    return;
  }
    
  if (collisionEvent.otherEntity.name == "Player") {
    var damageEvent = new DamageEvent(999);
    collisionEvent.otherEntity.DispatchEvent("damageEvent", damageEvent);
  }

};

Chase.prototype.onPlayerDeath = function() {
  var owner = arguments[0];

  Omni.LoadLevel(Omni.currentLevel);
  //owner.transform.translation.x = this.startLocation.x;
  //owner.transform.translation.y = this.startLocation.y;
  //this.Deactivate(owner);
}

Chase.prototype.Activate = function() {
  var owner = arguments[0];

  owner.RigidBody.velocity.x = this.speed;
  owner.ParticleEmitter.active = true;
  owner.SoundEmitter.startPlaying = true;
  this.active = true;
}

Chase.prototype.Deactivate = function(owner) {
  if (owner === undefined)
    owner = arguments[0];

  owner.RigidBody.velocity.x = 0;
  owner.ParticleEmitter.active = false;
  owner.SoundEmitter.startPlaying = false;
  this.active = false;
}
