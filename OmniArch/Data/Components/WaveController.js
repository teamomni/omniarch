/******************************************************************************
Filename: WaveController.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var WaveController = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.active = false;
  this.encounterNumber = 0;

  this.waveTimer = 0.0;

  this.currentWave = 0;
  this.waveActive = false;
  this.waveEnemyCount = 0;
  this.waveEnemies = [];
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

WaveController.Create = function() {
  // required function; do not remove
  return new WaveController(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

WaveController.properties = {
  "encounterNumber":  Property(Type.Unsigned, 0)
};

//////////////////
//  INITIALIZE  //
//////////////////

WaveController.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "space", "encounterTriggered", owner, 0, this.onEncounterTriggered);
  Omni.Connect(this, "space", "waveTriggered", owner, 0, this.onWaveTriggered);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
WaveController.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!this.active)
    return;

  this.waveTimer += updateEvent.dt;

  // Wave 1
  if (this.waveTimer > 0.5 && this.currentWave === 0) {
    owner.space.DispatchEvent("waveUpdate", new WaveEvent(++this.currentWave));
    return;
  }

  var arrayOfEnemies = owner.space.FindEntitiesByName("Wave"+this.currentWave);

  // Wave 2
  if ((arrayOfEnemies.length === 0 && this.currentWave == 1) || (this.waveTimer > 5 && this.currentWave == 1)) {
    owner.space.DispatchEvent("waveUpdate", new WaveEvent(++this.currentWave));
  }

  // Wave 3 gets called by a wave trigger

  if (arrayOfEnemies.length === 0 && this.currentWave == 3)
  {
    this.Deactivate(owner);
  }
};

WaveController.prototype.onEncounterTriggered = function() {
  var encounterEvent = arguments[1];

  // Check if my encounter was activated
  if (encounterEvent.encounterNumber == this.encounterNumber) {
    this.Activate();
  }
};

WaveController.prototype.onWaveTriggered = function() {
  var owner = arguments[0];
  var encounterEvent = arguments[1];

  owner.space.DispatchEvent("waveUpdate", new WaveEvent(encounterEvent.encounterNumber));
  this.currentWave = encounterEvent.encounterNumber;
  Console.WriteLine(encounterEvent.encounterNumber);
};

WaveController.prototype.Activate = function() {
  // Add cool activate effects, hud warnings sound effects
  this.active = true;
  Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic").SoundEmitter.SetParameter(0, 1);
};

WaveController.prototype.Deactivate = function(owner) {
  // Add cool activate effects, hud warnings sound effects
  this.active = false;
  owner.space.DispatchEvent("endEncounter", {});
  Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic").SoundEmitter.SetParameter(0, 1);
};

var WaveEvent = function(newWaveNumber) {
  this.newWaveNumber = newWaveNumber;
};
