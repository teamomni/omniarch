/******************************************************************************
Filename: PlayerLogic.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


///////////////////
//  CONSTRUCTOR  //
///////////////////

var PlayerLogic = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.currentHealth = 1;
  this.recoveryRate = 0; // Per second
  this.rechargeRate = 0.4;
  this.godMode = false;

  this.spawnPoint = new Vec2();

  this.momentum = 0;
  this.timeSinceLastChain = 0;
  this.momentumBuffer = 4;
  this.momentumThreshold = 0;

  this.gameOver = false;

  this.waveMode = false;
  
  this.createdUI = false;
  this.killed = false;
  this.recharging = false;

  this.rechargeSound = false;
  this.doneRecharging = false;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

PlayerLogic.Create = function() {
  // required function; do not remove
  return new PlayerLogic(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

PlayerLogic.properties = {
  "maxHealth":             Property(Type.Float, 100),
  "winX" :            Property(Type.Float, 200)
};

//////////////////
//  INITIALIZE  //
//////////////////

PlayerLogic.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // print an inspirational message
  Inspiration.PrintMessage();
  
  // Set starting health
  this.currentHealth = this.maxHealth;
  this.spawnPoint.x = owner.transform.translation.x;
  this.spawnPoint.y = owner.transform.translation.y;

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "damageEvent", owner, this.onDamage);
  
  // hack to make the transition smooth
  if (Omni.currentLevel != "Trains" && Omni.currentLevel != "ShieldGuy" && Omni.currentLevel != "Ending") {
    this.tempFadeout = owner.space.CreateEntity({"name" : "respawn_FadeToBlack", "archetype" : "GenericSprite", "position" : {"x" : owner.transform.translation.x, "y" : owner.transform.translation.y}});
    this.tempFadeout.Sprite.image = "screenFade";
    this.tempFadeout.Sprite.color.r = 0;
    this.tempFadeout.Sprite.color.g = 0;
    this.tempFadeout.Sprite.color.b = 0;
    this.tempFadeout.Sprite.color.a = 1;
    this.tempFadeout.transform.scale.x = 2;
    this.tempFadeout.transform.scale.y = 2;
  }
  
  if (Omni.currentLevel == "Ending")
    owner.GetComponent("PlayerCamera").active = false;
  
  // INITIALIZE COLLISION TABLE THIS IS BAD
  // Evade / Enemy = Ignore
  Omni.SetCollisionTableState(CollisionGroup.Evade, CollisionGroup.Enemy, CollisionState.Ignore);
  // Player / AvoidPlayer = Ignore
  Omni.SetCollisionTableState(CollisionGroup.Player, CollisionGroup.AvoidPlayer, CollisionState.Ignore);
  // Enemy / Enemy = Ingnore
  Omni.SetCollisionTableState(CollisionGroup.Enemy, CollisionGroup.Enemy, CollisionState.Ignore);
  // Player / Enemy = Detect
  Omni.SetCollisionTableState(CollisionGroup.Player, CollisionGroup.Enemy, CollisionState.SkipResolution);


  // AvoidAllButDefault / Player = Ignore
  Omni.SetCollisionTableState(CollisionGroup.AvoidAllButDefault, CollisionGroup.Player, CollisionState.Ignore);
  // AvoidAllButDefault / Enemy = Ignore
  Omni.SetCollisionTableState(CollisionGroup.AvoidAllButDefault, CollisionGroup.Enemy, CollisionState.Ignore);
  // AvoidAllButDefault / Evade = Ignore
  Omni.SetCollisionTableState(CollisionGroup.AvoidAllButDefault, CollisionGroup.Evade, CollisionState.Ignore);
  // AvoidAllButDefault / AvoidPlayer = Ignore
  Omni.SetCollisionTableState(CollisionGroup.AvoidAllButDefault, CollisionGroup.AvoidPlayer, CollisionState.Ignore);
  // AvoidAllButDefault / AvoidPlayer = Ignore
  Omni.SetCollisionTableState(CollisionGroup.AvoidAllButDefault, CollisionGroup.GrabedObject, CollisionState.Ignore);

  // DetectAllButDefault / Player = SkipResolution
  Omni.SetCollisionTableState(CollisionGroup.DetectAllButDefault, CollisionGroup.Player, CollisionState.SkipResolution);
  // DetectAllButDefault / Enemy = SkipResolution
  Omni.SetCollisionTableState(CollisionGroup.DetectAllButDefault, CollisionGroup.Enemy, CollisionState.SkipResolution);
  // DetectAllButDefault / Evade = SkipResolution
  Omni.SetCollisionTableState(CollisionGroup.DetectAllButDefault, CollisionGroup.Evade, CollisionState.SkipResolution);
  // DetectAllButDefault / AvoidPlayer = SkipResolution
  Omni.SetCollisionTableState(CollisionGroup.DetectAllButDefault, CollisionGroup.AvoidPlayer, CollisionState.SkipResolution);
  // DetectAllButDefault / GrabedObject = SkipResolution
  Omni.SetCollisionTableState(CollisionGroup.DetectAllButDefault, CollisionGroup.GrabedObject, CollisionState.SkipResolution);
  
  // CollideWithPlayer / Evade = Ignore
  Omni.SetCollisionTableState(CollisionGroup.CollideWithPlayer, CollisionGroup.Evade, CollisionState.Ignore);
  // CollideWithPlayer / Enemy = Ignore
  Omni.SetCollisionTableState(CollisionGroup.CollideWithPlayer, CollisionGroup.Enemy, CollisionState.Ignore);

  // Player / DetectPlayer = SkipResolution
  Omni.SetCollisionTableState(CollisionGroup.Player, CollisionGroup.DetectPlayer, CollisionState.SkipResolution);
  
  // Player / GrabedObject = SkipResolution
  Omni.SetCollisionTableState(CollisionGroup.Player, CollisionGroup.GrabedObject, CollisionState.SkipResolution);


  // CREATE SOUND EFFECTS
  var enemyDamage = owner.space.CreateEntity({"name" : "EnemyDamageSound", "archetype" : "SoundEffect", "position" : {"x" : 0, "y" : 0}});
  enemyDamage.GetComponent("SoundEffect").soundEffect = "event:/robot-damageget";

  var bombSound = owner.space.CreateEntity({"name" : "BombSound", "archetype" : "SoundEffect", "position" : {"x" : 0, "y" : 0}});
  bombSound.GetComponent("SoundEffect").soundEffect = "event:/bomb";

  var enemyDefeat = owner.space.CreateEntity({"name" : "EnemyDefeatSound", "archetype" : "SoundEffect", "position" : {"x" : 0, "y" : 0}});
  enemyDefeat.GetComponent("SoundEffect").soundEffect = "event:/enemy-defeat";

  var breakSound = owner.space.CreateEntity({"name" : "BreakSound", "archetype" : "SoundEffect", "position" : {"x" : 0, "y" : 0}});
  breakSound.GetComponent("SoundEffect").soundEffect = "event:/break";

  var healSound = owner.space.CreateEntity({"name" : "HealSound", "archetype" : "SoundEffect", "position" : {"x" : 0, "y" : 0}});
  healSound.GetComponent("SoundEffect").soundEffect = "event:/heal";
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
PlayerLogic.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  // create the health bar and stuff
  if (!this.createdUI) {
    if (Omni.currentLevel != "Ending") {
      if (Omni.currentLevel != "Trains" && Omni.currentLevel != "ShieldGuy") {
        var spawner = owner.space.CreateEntity({"name" : "respawnInitiator", "archetype" : "RespawnInitiator", "position" : {"x" : 0, "y" : 0}});
        spawner.GetComponent("RespawnInitiator").spawning = true;
        spawner.GetComponent("RespawnInitiator").targetAlpha = 1;
        spawner.GetComponent("RespawnInitiator").fadedBlackTime = 1;
        
        // haaaaack
        this.tempFadeout.Destroy();
      }
      
      this.healthBar = Omni.FindSpaceByName("ui").CreateEntity(
        {"name" : "healthBar", "archetype" : "HealthBar", "position" : {"x" : -3.07, "y" : 2.227}});  
        
      this.healthBar.Sprite.depth = GeneralDepth.UI + 1;
      
      this.healthBarBack = Omni.FindSpaceByName("ui").CreateEntity(
        {"name" : "healthBarBack", "archetype" : "HealthBarBack", "position" : {"x" : -3.3, "y" : 2.2}});  
      
      this.healthBarBack.Sprite.depth = GeneralDepth.UI + 2;

      this.levelIndicator = Omni.FindSpaceByName("ui").CreateEntity(
        {"name" : "LevelIndicator", "archetype" : "LevelIndicator", "position" : {"x" : 4, "y" : 2.0}});  

        
      this.levelIndicator.Sprite.depth = GeneralDepth.UI + 1;
    }
    
    this.screenEdges = Omni.FindSpaceByName("ui").CreateEntity(
      {"name" : "ScreenEdges", "archetype" : "ScreenEdges", "position" : {"x" : 0, "y" : 0}});  

    this.screenEdges.Sprite.depth = GeneralDepth.UI;
    this.screenEdges.Sprite.color.a = 0;
    
    this.createdUI = true;
  }

  // End of Level
  // if (owner.transform.translation.x >= -4) {
  //   var menuController = Omni.FindSpaceByName("ui").FindEntityByName("MainMenu");
  //   menuController.GetComponent("MenuController").HideControls();
  // }
  // if (owner.transform.translation.x >= this.winX && !this.gameOver) {
  //   //Omni.LoadLevel("Chase");
  //   /*var menuController2 = Omni.FindSpaceByName("ui").FindEntityByName("MainMenu");
  //   menuController2.GetComponent("MenuController").Open("victory");
  //   Omni.PauseAllExcept("ui");
  //   this.gameOver = true;*/
  // }

  // if recharging, increase health
  if (this.recharging) {
    this.currentHealth += this.rechargeRate;
	this.doneRecharging = true;
	
    if (this.currentHealth > this.maxHealth) {
      this.currentHealth = this.maxHealth;
      var soundEvent = new SoundEvent("event:/heal");
      owner.space.DispatchEvent("stopSound", soundEvent);
    }
    else
    {
      if (!this.rechargeSound) {
        var soundEvent = new SoundEvent("event:/heal");
        owner.space.DispatchEvent("playSound", soundEvent);
        this.rechargeSound = true;
      }
    }
  }
  else {
	if (this.doneRecharging) {
      var soundEvent = new SoundEvent("event:/heal");
      owner.space.DispatchEvent("stopSound", soundEvent);
      this.rechargeSound = false;
	  this.doneRecharging = false;
	}
  }
  
  // Check health
  if (this.currentHealth <= 0 || owner.transform.translation.y < - 10 && !this.killed)
  {
    owner.SoundEmitter.soundCue = "event:/player-damagegrunt";
    owner.SoundEmitter.startPlaying = true;
    owner.GetComponent("PlayerController").Drop();
    this.KillPlayer(owner);
  }
  
  // recover Health
  this.currentHealth += this.recoveryRate * updateEvent.dt;

  // Clamp health
  if (this.currentHealth > this.maxHealth)
    this.currentHealth = this.maxHealth;

  // Toggle Invulnerablility
  if (CheckSystemInput("god_mode"))
    this.godMode = !this.godMode;

  if (CheckSystemInput("next_level")) {
    if (Omni.currentLevel == "Trains")
      Omni.LoadLevel("ShieldGuy");
    else if (Omni.currentLevel == "ShieldGuy")
      Omni.LoadLevel("First");
    else if (Omni.currentLevel == "First")
      Omni.LoadLevel("Second");
    else if (Omni.currentLevel == "Second")
      Omni.LoadLevel("Chase");
    else if (Omni.currentLevel == "Chase")
      Omni.LoadLevel("Ending");
  }

  if (CheckSystemInput("previous_level")) {
    if (Omni.currentLevel == "ShieldGuy")
      Omni.LoadLevel("Trains");
    else if (Omni.currentLevel == "First")
      Omni.LoadLevel("ShieldGuy");
    else if (Omni.currentLevel == "Second")
      Omni.LoadLevel("First");
    else if (Omni.currentLevel == "Chase")
      Omni.LoadLevel("Second");
    else if (Omni.currentLevel == "Ending")
      Omni.LoadLevel("Chase");
  }

  if (CheckSystemInput("end_game")) {
    Omni.LoadLevel("Ending");
  }


  // Update ScreenEdges
  var screenEdges = Omni.FindSpaceByName("ui").FindEntityByName("ScreenEdges");

  if (screenEdges !== undefined)
  {
    screenEdges.Sprite.color.a -= 0.01;
  }
  else
    Console.Warning("Please add ScreenEdges to the level!");

  // Update Momentum
  this.timeSinceLastChain += updateEvent.dt;

  if (this.timeSinceLastChain >= this.momentumBuffer && this.momentum > 0)
    this.momentum = Ease.Easy(this.momentum, 0, 0.05);
};

PlayerLogic.prototype.onDamage = function() {
  var owner = arguments[0];
  var damageEvent = arguments[1];

  if (this.godMode || this.killed)
    return;

  this.momentum = 0;

  this.currentHealth -= damageEvent.damage;
  owner.GetComponent("PlayerController").Drop();
  owner.Animator.Set("plr_hitstun");
  owner.RigidBody.velocity.x = 0;
  owner.RigidBody.velocity.y = 0;
  owner.GetComponent("PlayerController").SetState("stun");
  owner.SoundEmitter.soundCue = "event:/player-damagegrunt";
  owner.SoundEmitter.startPlaying = true;

  var screenEdges = Omni.FindSpaceByName("ui").FindEntityByName("ScreenEdges");
  screenEdges.Sprite.color.a = 1;
};

PlayerLogic.prototype.KillPlayer = function(owner) {
  owner.space.DispatchEvent("PlayerDeath", {});

  if (!this.killed) {
    owner.space.CreateEntity({"name" : "respawnInitiator", "archetype" : "RespawnInitiator", "position" : {"x" : 0, "y" : 0}});  

    this.currentHealth = 1;
    this.momentum = 0;
    this.killed = true;
  }
};

PlayerLogic.prototype.IncreaseMomentum = function(amount) {
  this.momentum += amount;

  // Update threshold
  if (this.momentum >= 23)
    this.momentumThreshold = 3;
  else if (this.momentum >= 10)
    this.momentumThreshold = 2;
  else if (this.momentum >= 5)
    this.momentumThreshold = 1;
  else
    this.momentumThreshold = 0;

  this.timeSinceLastChain = 0;
};
