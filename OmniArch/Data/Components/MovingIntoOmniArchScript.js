/******************************************************************************
Filename: MovingIntoOmniArchScript.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var MovingIntoOmniArchScript = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
  
  this.timer = 0;
  this.player = 0;
  this.active = false;
  this.moveSpeed = 4;
  this.movingUp = false;
  this.targetY = 9;
  this.fadeToBlackTime = 1;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

MovingIntoOmniArchScript.Create = function() {
  // required function; do not remove
  return new MovingIntoOmniArchScript(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

MovingIntoOmniArchScript.properties = {
  "targetX"  :       Property(Type.Float, 0),
};

//////////////////
//  INITIALIZE  //
//////////////////

MovingIntoOmniArchScript.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "CollisionTriggered", owner, this.onTriggered);
  
  this.midwayX = 0.25 * owner.transform.translation.x + 0.75 * this.targetX;
  
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
MovingIntoOmniArchScript.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  if (!this.active)
    return;
  
  if (!this.movedRight) {
    this.player.transform.translation.x += this.moveSpeed * updateEvent.dt;
    if (this.player.transform.translation.x >= this.midwayX) {
      this.player.GetComponent("PlayerCamera").deactivate(this.player);
      this.movingUp = true;
      this.player.GetComponent("PlayerCamera").fadeOutParallax = true;
    }
    if (this.player.transform.translation.x >= this.targetX) {
      this.movedRight = true;
      this.player.Animator.Set("plr_idle");
    }
  }
  
  if (this.movingUp) {
    var cameraTrans = owner.space.camera.transform.translation;
    cameraTrans.y = Ease.Easy(cameraTrans.y, this.targetY, 0.015);
    if (cameraTrans.y >= this.targetY - 0.5) {
      this.timer += updateEvent.dt;
      this.fadeout.Sprite.color.a = this.timer / this.fadeToBlackTime;
      if (this.timer >= this.fadeToBlackTime)
        Omni.LoadLevel("Ending");
    }
  }
};


MovingIntoOmniArchScript.prototype.onTriggered = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];
  
  // get the police chase thing and not make it kill the player
  owner.space.FindEntityByName("PoliceChase").GetComponent("Chase").toKillPlayer = false;
  
  this.player = owner.space.FindEntityByName("Player");
  this.player.GetComponent("PlayerController").active = false;
  this.player.Animator.Set("plr_run");
  // this.player.GetComponent("PlayerCamera").zoomReleased = false;
  // this.player.GetComponent("PlayerCamera").intendedSize = 50;
  this.player.RigidBody.Clear();
  
  this.fadeout = Omni.FindSpaceByName("ui").CreateEntity({"name" : "respawn_FadeToBlack", "archetype" : "GenericSprite", "position" : {"x" : 0, "y" : 0}});
  this.fadeout.Sprite.image = "screenFade";
  this.fadeout.Sprite.color.r = 0;
  this.fadeout.Sprite.color.g = 0;
  this.fadeout.Sprite.color.b = 0;
  this.fadeout.Sprite.color.a = 0;
  
  this.active = true;
};
