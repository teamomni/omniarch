/******************************************************************************
Filename: Drone.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var Drone = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.droneType = Drone.types.Patrol;

  this.pointA = new Vec2();
  this.pointB = new Vec2();
  this.targetPoint = this.pointB;

  this.flyAwayPoint = new Vec2();

  this.parentLinkID = 6;
  this.parentID = 0;

  this.playerLinkID = 1;
  this.playerID = 0;

  this.maxMoveSpeed = 5;
  this.accelSpeed = 5;

  this.aggroRange = 10;
  this.aggroed = false;

  this.flyAway = false;

  this.active = true;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Drone.Create = function() {
  // required function; do not remove
  return new Drone(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

Drone.types = {
  Patrol: 0,
  Sweep: 1
};

Drone.properties = {
  "pointA":       Property(Type.Vec2, Vec2()),
  "pointB":       Property(Type.Vec2, Vec2()),
  "aggroRange":   Property(Type.Float, 10),
  "parentLinkID": Property(Type.Unsigned, 1)
};

//////////////////
//  INITIALIZE  //
//////////////////

Drone.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  this.playerID = Omni.GetUniqueIDFromLinkID(this.playerLinkID);
  this.parentID = Omni.GetUniqueIDFromLinkID(this.parentLinkID);

  this.pointA.set(owner.transform.translation.x, owner.transform.translation.y);
  this.pointB.set(owner.transform.translation.x + 5, owner.transform.translation.y);

  this.flyAwayPoint.set(owner.transform.translation.x + 3, owner.transform.translation.y + 10);

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "FlyAway", owner, this.OnFlyAway);
  Omni.Connect(this, "entity", "changeState", owner, this.changeState);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
Drone.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!owner.OnScreen()) {
    if (this.flyAway) {
      Console.WriteLine("OFF SCREEN DESTRUCTION");
      owner.Destroy();
    }
    return;
  }

  if (!this.active)
    return;

  if (this.flyAway) {
    this.FlyAway(owner);
    return;
  }

  if (owner.OnScreen())
  {
    this.RotateWithVelocity(owner);

    var parent = Omni.FindEntityByLinkID(this.parentLinkID);
    if (!parent)
      this.flyAway = true;

    if (this.CheckAggro(owner))
      owner.DispatchEvent("AggroEvent", {});

    if (this.droneType == Drone.types.Patrol && !this.aggroed)
      this.MoveTowardsPoint(owner);
  }
};

Drone.prototype.CheckAggro = function(owner) {
  var player = Omni.FindEntityByID(this.playerID);
  var distanceToTarget = new Vec2(owner.transform.translation.x, owner.transform.translation.y);
  distanceToTarget.sub(distanceToTarget, player.transform.translation);

  if (distanceToTarget.length() <= this.aggroRange) {
    this.aggroed = true;
    return true;
  }

  return false;
};

Drone.prototype.MoveTowardsPoint = function(owner) {
  var direction = new Vec2(this.targetPoint.x, this.targetPoint.y);
  direction.sub(direction, owner.transform.translation).normalize();

  owner.RigidBody.ApplyLinearForce(direction.x * this.accelSpeed, direction.y);
};

Drone.prototype.RotateWithVelocity = function(owner) {
  // Rotate relative to velocity
  var speedPercent = owner.RigidBody.velocity.x / this.maxMoveSpeed;
  owner.transform.rotation = -0.35 * speedPercent;

  // Clamp move speed
  if (owner.RigidBody.velocity.x > this.maxMoveSpeed)
  {
    owner.RigidBody.velocity.x = this.maxMoveSpeed;
  }
  if (owner.RigidBody.velocity.x < -this.maxMoveSpeed)
  {
    owner.RigidBody.velocity.x = -this.maxMoveSpeed;
  }

};

Drone.prototype.FlyAway = function(owner) {
  var direction = new Vec2(this.flyAwayPoint.x, this.flyAwayPoint.y);
  direction.sub(direction, owner.transform.translation).normalize();

  if (direction.x < 0.05 && direction.y < 0.05) {
    //Console.WriteLine("flown away");
    owner.Destroy();
    return;
  }

  owner.RigidBody.ApplyLinearForce(direction.x * this.accelSpeed, direction.y * this.accelSpeed);
};

Drone.prototype.OnFlyAway = function() {
  this.flyAway = true;
};

Drone.prototype.changeState = function() {
  var owner = arguments[0];
  var stateEvent = arguments[1];

  if (stateEvent.newState == EnemyStates.Idle) {
  }

  if (stateEvent.newState == EnemyStates.Hostile) {
  }

  if (stateEvent.newState == EnemyStates.Defeated) {
    this.active = false;
    owner.RigidBody.gravityScale = 4;
  }
};
