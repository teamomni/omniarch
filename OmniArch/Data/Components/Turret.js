/******************************************************************************
Filename: Turret.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var Turret = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.aggroRange = 8;
  this.aggroed = false;

  this.accelSpeed = 2;

  this.bulletSpeed = 3;
  this.shotRange = 4;
  this.shotCooldown = 0.3;
  this.shotTimer = 0;
  this.shotsFired = 0;
  this.shotBurstCooldown = 3;
  this.shotBurstTimer = 0;
  this.shotBurstQuantity = 1;

  this.facing = Facing.Left;
  this.player = 0;

  // lights stuff
  this.lightsOffDuration = 0;
  this.lightsOffTimer = 0;
  this.lightsNormal = true;
  this.targetBrightness = 1.25;
  this.startBrightness = 1.25;
  this.diffBrightness = 0;
  
  // pass on events stuff
  this.received = false;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Turret.Create = function() {
  // required function; do not remove
  return new Turret(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

Turret.properties = {
  "active" : Property(Type.Bool, true),

};

//////////////////
//  INITIALIZE  //
//////////////////

Turret.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "changeState", owner, this.changeState);
  Omni.Connect(this, "entity", "pass", owner, this.onPass);

  // store the player
  this.player = owner.space.FindEntityByName("Player");

  // initialize animation
  owner.Animator.Set("turret_idle");
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
Turret.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  // if we are deactivating, look sad
  if (this.toDeactivate) {
    owner.transform.rotation = Ease.Easy(owner.transform.rotation, 5.2);
    return;
  } 
  
  if (!this.active)
    return;

  if (!owner.OnScreen())
    return;
    
    
  this.UpdateLights(owner, updateEvent);

  if (owner.GetComponent("Enemy").state == EnemyStates.Idle)
    this.CheckAggro(owner);

  this.shotTimer += updateEvent.dt;
  this.shotBurstTimer += updateEvent.dt;

  if (this.aggroed) {
    this.ShootAtTarget(owner);
  }
};

Turret.prototype.CheckAggro = function(owner) {
    var distanceToTarget = new Vec2(owner.transform.translation.x, owner.transform.translation.y);
  distanceToTarget.sub(distanceToTarget, this.player.transform.translation);

  if (distanceToTarget.length() <= this.aggroRange) {
    this.aggroed = true;
    var stateEvent = new StateEvent(EnemyStates.Hostile);
    owner.DispatchEvent("changeState", stateEvent);
    return true;
  }

  return false;
};


Turret.prototype.ShootAtTarget = function(owner) {
  var targetPoint = this.player.transform.translation;
  var meToTarget = new Vec2(targetPoint.x, targetPoint.y);
  var translation = owner.transform.translation;
  meToTarget.sub(meToTarget, translation);
  var direction = meToTarget.normalized();
  var barrelPoint = new Vec2(translation.x + direction.x, translation.y + direction.y);

  
  // get to where the barrel part is
  var perpendicular = new Vec2();
  perpendicular.x = 0.4 * direction.y;
  perpendicular.y = -0.4 * direction.x;

  // if x is flipped, flip this perpendicular thing
  if (owner.Sprite.flipX) {
    perpendicular.x = - perpendicular.x;
    perpendicular.y = - perpendicular.y;
  }

  // add it to the actual position
  barrelPoint.x += perpendicular.x;
  barrelPoint.y += perpendicular.y;

  // get a vector going from the barrel to the target
  var barrelToTarget = new Vec2(targetPoint.x - barrelPoint.x, targetPoint.y - barrelPoint.y);
  barrelToTarget.normalize();

  var newAngle = Math.atan2(barrelToTarget.y, barrelToTarget.x);

  // if our sprite is flipped, add pi to the angle
  if (owner.Sprite.flipX)
    newAngle += Math.PI;

  // wrap the given angle
  if (newAngle < 0) {
    newAngle = 2 * Math.PI + newAngle;
  }

  // if too close, do not update rotation
  if (meToTarget.length() > 1.0)
    owner.transform.rotation = Ease.Easy(owner.transform.rotation, newAngle);

  if (this.toShoot && owner.Animator.frame == 2) {
    var projectille = owner.space.CreateEntity({name : "PlasmaBall", position : barrelPoint, archetype : "PlasmaBall"});
    projectille.RigidBody.velocity.x = barrelToTarget.x * this.bulletSpeed;
    projectille.RigidBody.velocity.y = barrelToTarget.y * this.bulletSpeed;
    projectille.transform.scale.x = 0.4;
    projectille.transform.scale.y = 0.4;
    this.toShoot = false;
  }

  if (this.shotBurstTimer < this.shotBurstCooldown)
    return;

  // If in range, SHOOT!
  if (meToTarget.x >= -this.shotRange + 1 || meToTarget.x <= this.shotRange - 1) {
    if (this.shotTimer < this.shotCooldown)
      return;

    owner.Animator.Set("turret_attack");
    this.toShoot = true;

    
    this.shotTimer = 0;
    this.shotsFired += 1;

    if (this.shotsFired >= this.shotBurstQuantity) {
      this.shotBurstTimer = 0;
      this.shotsFired = 0;
    }
  }
};

Turret.prototype.changeState = function() {
  var owner = arguments[0];
  var stateEvent = arguments[1];

  if (stateEvent.newState == EnemyStates.Idle) {
    this.LightsOff(owner, 0.25, 0.3);
  }

  if (stateEvent.newState == EnemyStates.Hostile) {
    this.LightsOff(owner, 1.25, 0.3);
  }

  if (stateEvent.newState == EnemyStates.Defeated) {
    this.active = false;
  }
};

Turret.prototype.LightsOff = function(owner, targetBrightness, duration) {
  this.lightsNormal = false;
  this.lightsOffDuration = duration;
  this.lightsOffTimer = 0;
  this.startBrightness = owner.Sprite.brightness;
  this.targetBrightness = targetBrightness;
  this.diffBrightness = targetBrightness - this.startBrightness;
};

Turret.prototype.UpdateLights = function(owner, updateEvent) {
  if (this.lightsNormal)
    return;

  this.lightsOffTimer += updateEvent.dt;
  owner.Sprite.brightness = this.startBrightness + this.lightsOffTimer / this.lightsOffDuration * this.diffBrightness;
  if (this.lightsOffTimer > this.lightsOffDuration) {
    owner.Sprite.brightness = this.targetBrightness;
    this.lightsNormal = true;
    this.lightsOffDuration = 0;
    this.lightsOffTimer = 0;
  }
};

Turret.prototype.onPass = function() {
  var owner = arguments[0];
  var passEvent = arguments[1];

  //Console.WriteLine("passed");
  
  if (!this.active) {
    this.active = true;
  }
  
  else {
    if (!this.received) {
      this.deactivate(owner);
      this.received = true;
    }
  }
};

Turret.prototype.deactivate = function(owner) {
  owner.GetComponent("Enemy").colorUpdate = false;

  owner.Sprite.glowColor.r = 0.2;
  owner.Sprite.glowColor.g = 0.2;
  owner.Sprite.glowColor.b = 0.2;
  
  
  this.toDeactivate = true;
  this.active = false;
};
