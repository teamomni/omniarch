/******************************************************************************
Filename: TranslationFollow.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var TranslationFollow = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

TranslationFollow.Create = function() {
  // required function; do not remove
  return new TranslationFollow(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////
TranslationFollow.properties = {
  "targetLinkID":   Property(Type.Int, 0),
  "offsetX":        Property(Type.Float, 0.0),
  "offsetY":        Property(Type.Float, 0.0),
  "offsetZ":        Property(Type.Float, 0.0),
};

//////////////////
//  INITIALIZE  //
//////////////////

TranslationFollow.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  
  // get the target to follow
  this.targetObject = Omni.FindEntityByLinkID(this.targetLinkID);
};


TranslationFollow.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  var position = owner.transform.translation;
  var targetPosition = this.targetObject.transform.translation;
  
  //Console.WriteLine("helloo");
  
  position.x = targetPosition.x + this.offsetX;
  position.y = targetPosition.y + this.offsetY;
  
};