/******************************************************************************
Filename: SoundEffect.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


// constructor
var SoundEffect = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member variables
};

// boilerplate create function
SoundEffect.Create = function() {
  return new SoundEffect(arguments[0]);
};

////////////////////////////////////////////////////////////////////////////////
// PROPERTIES
////////////////////////////////////////////////////////////////////////////////

SoundEffect.properties = {
  "soundEffect": Property(Type.String_, "")
};

// function called after
SoundEffect.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "space", "playSound", owner, 0, this.onPlaySound);
  Omni.Connect(this, "space", "stopSound", owner, 0, this.onStopSound);

  owner.SoundEmitter.soundCue = this.soundEffect;
};

////////////////////////////////////////////////////////////////////////////////
// EVENTS
////////////////////////////////////////////////////////////////////////////////
SoundEffect.prototype.onPlaySound = function() {
  var owner = arguments[0];
  var soundEvent = arguments[1];

  if (soundEvent.sound == this.soundEffect) {
    owner.SoundEmitter.soundCue = this.soundEffect;
    owner.SoundEmitter.pause = false;
    owner.SoundEmitter.volume = 1;
    if (!owner.SoundEmitter.startPlaying)
      owner.SoundEmitter.startPlaying = true;
  }
};

SoundEffect.prototype.onStopSound = function() {
  var owner = arguments[0];
  var soundEvent = arguments[1];

  if (soundEvent.sound == this.soundEffect) {
    owner.SoundEmitter.pause = true;
    owner.SoundEmitter.volume = 0;
  }
};

var SoundEvent = function (sound) {
  this.sound = sound;
};
