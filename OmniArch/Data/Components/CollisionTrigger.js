/******************************************************************************
Filename: CollisionTrigger.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var CollisionTrigger = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

CollisionTrigger.Create = function() {
  // required function; do not remove
  return new CollisionTrigger(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

CollisionTrigger.properties = {
};

//////////////////
//  INITIALIZE  //
//////////////////

CollisionTrigger.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "entity", "onCollisionStart", owner, this.onCollisionStart);
};

CollisionTrigger.prototype.onCollisionStart = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];

  if (collisionEvent.otherEntity.HasComponent("PlayerLogic")) {
    owner.DispatchEvent("CollisionTriggered", {});
  }
};

