/******************************************************************************
Filename: PlayerCamera.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var PlayerCamera = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.yZoneHeight = 0.5;
  this.baseCameraSize = 8;
  this.actionCameraSize = 3.75;
  this.cameraObject = null;
  this.intendedSize = this.baseCameraSize;
  this.parallaxLayers = [];
  this.parallaxOffset = 0;
  this.parallaxObjs = [];
  this.scalar = 3;
  this.initialScalar = 3;
  //this.parallaxSpeeds = [0.05, 0.03, 0.015, 0.005, 0.002, 0.0];

  this.centerReleased = true;
  this.zoomReleased = true;
  this.fadeOutParallax = false;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

PlayerCamera.Create = function() {
  // required function; do not remove
  return new PlayerCamera(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

PlayerCamera.properties = {
  "setupCamera": Property(Type.Bool, true),
  "active"     : Property(Type.Bool, true)
};

//////////////////
//  INITIALIZE  //
//////////////////

PlayerCamera.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];
  
  // this is where you connect events
  Omni.Connect(this, "space", "onFrameUpdate", owner, 0, this.onUpdate);

  this.parallaxLayers = owner.space.GetParallaxLayers();

  this.parallaxOffset = owner.space.GetParallaxOffset();
};

PlayerCamera.prototype.deactivate = function(owner) {
  this.active = false;
  owner.space.camera.StopFollowing();

}

PlayerCamera.prototype.activate = function(owner) {
  this.active = true;
  owner.space.camera.Follow(this.cameraObject);

}

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
PlayerCamera.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  var controller = owner.GetComponent("PlayerController");
  var body = owner.RigidBody;
  var i;
    
  if (this.fadeOutParallax) {
    for (i = 0; i < this.parallaxLayers.length; ++i)
    {
      this.parallaxObjs[i][0].Sprite.color.a = Ease.Easy(this.parallaxObjs[i][0].Sprite.color.a, 0, 0.01);
      if (this.parallaxLayers[i][2]) {
        this.parallaxObjs[i][1].Sprite.color.a  = Ease.Easy(this.parallaxObjs[i][1].Sprite.color.a, 0, 0.01);
        this.parallaxObjs[i][2].Sprite.color.a  = Ease.Easy(this.parallaxObjs[i][2].Sprite.color.a, 0, 0.01);
      }
    }
  }
    
    
  if (!this.active)
    return;
    
  if (Omni.currentLevel == "Trains")
    this.parallaxLayers = Omni.FindSpaceByName("ui").GetParallaxLayers();
  
  if (Omni.currentLevel == "Trains")
    this.parallaxOffset += 0.01;

  if (!this.cameraObject) {
    this.cameraObject = owner.space.CreateEntity({ name: "Camera" });
    owner.space.camera.Follow(this.cameraObject);
    owner.space.camera.size = 8;
    owner.space.camera.SetOffset(0, 0);
    for (i = 0; i < this.parallaxLayers.length; ++i)
    {
      var obj1 = owner.space.CreateEntity({ name: "Parallax", archetype: "GenericSprite" });
      obj1.Sprite.image = this.parallaxLayers[i][0];
      obj1.Sprite.depth = 1000 + 20 * (i + 1);
      if (this.parallaxLayers[i][2]) {
        var obj2 = owner.space.CreateEntity({ name: "Parallax", archetype: "GenericSprite" });
        obj2.Sprite.image = this.parallaxLayers[i][0];
        obj2.Sprite.depth = 1000 + 20 * (i + 1);
        var obj3 = owner.space.CreateEntity({ name: "Parallax", archetype: "GenericSprite" });
        obj3.Sprite.image = this.parallaxLayers[i][0];
        obj3.Sprite.depth = 1000 + 20 * (i + 1);
        this.parallaxObjs.push([ obj1, obj2, obj3 ]);
      }
      else
        this.parallaxObjs.push([ obj1 ]);
    }
  }



  var offset = new Vec2();

  var base = 2;
  var velScalar = 0.5;
  var velCap = 5;

  if (this.zoomReleased == true)
    this.intendedSize = this.actionCameraSize + (this.baseCameraSize - this.actionCameraSize) * Omni.timeScale;

  owner.space.camera.size = Ease.Easy(owner.space.camera.size, this.intendedSize, 0.075, 0.001);

  offset.y = 1.35;
  if (!owner.RigidBody.IsOnGround()) {
    //if (owner.transform.translation.y > controller.lastJumpStartHeight)
    if (owner.RigidBody.velocity.y > 0)
      offset.y -= Math.max(offset.y - owner.RigidBody.velocity.y, 0);
    else
      offset.y = 0.1 * owner.RigidBody.velocity.y;
      //offset.y -= updateEvent.Dt * 4;
  }

  // This is used in ShieldGuy to keep camera centered
  if (!this.centerReleased)
    base = 0;

  if (controller.facing == Facing.Left) {
    offset.x -= Math.min(base - body.velocity.x * velScalar, velCap) / 3;
  }
  else {
    offset.x += Math.min(base + body.velocity.x * velScalar, velCap);
  }

  var scale = Utilities.slowTimer === 0 ? 1 : 0.33;
  offset.x *= owner.space.camera.size / this.baseCameraSize * scale;
  offset.y *= owner.space.camera.size / this.baseCameraSize * scale;

  if (this.setupCamera) {
    this.cameraObject.transform.translation.x = owner.transform.translation.x + offset.x;
    this.cameraObject.transform.translation.y = owner.transform.translation.y + offset.y;
    this.setupCamera = false;
  }
  else {
    if (Math.abs(this.cameraObject.transform.translation.y - (owner.transform.translation.y + offset.y)) > 0.1) {
      if (owner.transform.translation.y > - 10)
 		   this.cameraObject.transform.translation.y = Ease.Easy(this.cameraObject.transform.translation.y, owner.transform.translation.y + offset.y, updateEvent.dt * this.scalar, 0.005);
 		}
    this.cameraObject.transform.translation.x = Ease.Easy(this.cameraObject.transform.translation.x, owner.transform.translation.x + offset.x, updateEvent.dt * this.scalar, .01);
    //this.cameraObject.transform.translation.y = Ease.Easy(this.cameraObject.transform.translation.y, owner.transform.translation.y + offset.y, updateEvent.dt * scalar, 0.01);
  }

  for (i = 0; i < this.parallaxLayers.length; ++i)
  {
    this.parallaxObjs[i][0].transform.translation.x = this.cameraObject.transform.translation.x - this.parallaxOffset - this.cameraObject.transform.translation.x * this.parallaxLayers[i][1];
    while (this.parallaxObjs[i][0].transform.translation.x > this.cameraObject.transform.translation.x + 4)
      this.parallaxObjs[i][0].transform.translation.x -= 16;
    while (this.parallaxObjs[i][0].transform.translation.x < this.cameraObject.transform.translation.x - 4)
      this.parallaxObjs[i][0].transform.translation.x += 16;
    this.parallaxObjs[i][0].transform.translation.y = this.cameraObject.transform.translation.y - this.cameraObject.transform.translation.y * this.parallaxLayers[i][1];
    if (!this.parallaxLayers[i][2])
      continue;
    this.parallaxObjs[i][1].transform.translation.x = this.parallaxObjs[i][0].transform.translation.x - 16;
    this.parallaxObjs[i][1].transform.translation.y = this.parallaxObjs[i][0].transform.translation.y;
    this.parallaxObjs[i][2].transform.translation.x = this.parallaxObjs[i][0].transform.translation.x + 16;
    this.parallaxObjs[i][2].transform.translation.y = this.parallaxObjs[i][0].transform.translation.y;
  }
  //Console.Warning(this.parallaxOffset);
  //Console.Warning(owner.space.GetParallaxOffset());

};
