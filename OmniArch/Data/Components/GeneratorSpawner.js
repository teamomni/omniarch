/******************************************************************************
Filename: GeneratorSpawner.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Juli Gregg

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var GeneratorSpawner = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
  
  this.smokeEffect1 = 0;
  this.smokeEffect2 = 0;
  this.xOffset1 = 0.36;
  this.xOffset2 = -0.3;
  this.justCreated = true;
  this.shieldDestroyed = true;
  this.shield = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

GeneratorSpawner.Create = function() {
  // required function; do not remove
  return new GeneratorSpawner(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

GeneratorSpawner.properties = {
  "active":    Property(Type.Bool, true),
  "hasShield": Property(Type.Bool, true)
};

//////////////////
//  INITIALIZE  //
//////////////////

GeneratorSpawner.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "entity", "Death", owner, this.onDeathEvent);
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);

  // create the particle effects
  this.smokeEffect1 = owner.space.CreateEntity(
    {"name" : "generator_SmokeParticle", "archetype" : "SmokeParticle", "position" : {"x" : 0, "y" : 0}});  
    
  this.smokeEffect2 = owner.space.CreateEntity(
    {"name" : "generator_SmokeParticle", "archetype" : "SmokeParticle", "position" : {"x" : 0, "y" : 0}});  
   
  // make the particles follow the generator spawner
  var followEntityComp1 = this.smokeEffect1.GetComponent("FollowEntity");
  var followEntityComp2 = this.smokeEffect2.GetComponent("FollowEntity");
    
  followEntityComp1.targetID = this.owner_id;
  followEntityComp1.active = true;
  followEntityComp1.xOffset = 0.36;
  followEntityComp1.yOffset = 0.5;
  
  
  followEntityComp2.targetID = this.owner_id;
  followEntityComp2.active = true;
  followEntityComp2.active = true;
  followEntityComp2.xOffset = -0.3;
  followEntityComp2.yOffset = 0.25;
  
  // activate the particle effects
  this.smokeEffect1.ParticleEmitter.depth = owner.Sprite.depth + 0.1;
  this.smokeEffect2.ParticleEmitter.depth = owner.Sprite.depth + 0.1;
  
  // if we have a shield, create it and set its color
  if (this.hasShield) {
    this.shield = owner.space.CreateEntity(
      {"name" : "generator_Shield", "archetype" : "GenericSprite", 
      "position" : {"x" : owner.transform.translation.x + 0.05, "y" : owner.transform.translation.y - 0.3}});  
      
    this.shieldEffect = owner.space.CreateEntity(
      {"name" : "generator_ShieldParticle", "archetype" : "ShieldParticle", "position" : {"x" : 0, "y" : 0}});  
      
    var followEntityComp3 = this.shieldEffect.GetComponent("FollowEntity");
    followEntityComp3.targetID = this.owner_id;
    followEntityComp3.active = true;
    
    this.shield.transform.scale.x = 2.3;
    this.shield.transform.scale.y = 2.3;
  
    var shieldSprite = this.shield.Sprite;
    shieldSprite.image = "_Hex_Shield_glow_full";
    shieldSprite.color.r = owner.Sprite.glowColor.r;
    shieldSprite.color.g = owner.Sprite.glowColor.g;
    shieldSprite.color.b = owner.Sprite.glowColor.b;
    shieldSprite.color.a = 0.7;
    shieldSprite.depth = owner.Sprite.depth - 0.1;
    
    this.shieldDestroyed = false;
  }
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
GeneratorSpawner.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  // if we were given an ID AFTER we were created (meaning through scripting), then identify here
  if (this.justCreated) {
    var sprite = owner.Sprite;
    if (sprite.flipX)
    {
      this.smokeEffect1.GetComponent("FollowEntity").xOffset = -this.xOffset1;
      this.smokeEffect2.GetComponent("FollowEntity").xOffset = -this.xOffset2;
    }
    this.justCreated = false;
  }
};


GeneratorSpawner.prototype.onDeathEvent = function() {
  var owner = arguments[0];
  var deathEvent = arguments[1];
  
  // if we have a shield, destroy it, and activate the destructable object
  if (!this.shieldDestroyed && this.hasShield) {
    this.shieldDestroyed = true;
    
    owner.GetComponent("DestructableObject").active = true;
    this.shieldEffect.ParticleEmitter.active = true;
    this.shieldEffect.GetComponent("TimedDeath").active = true;
    this.shield.Destroy();
  }
  
  else {
    this.smokeEffect1.Destroy();
    this.smokeEffect2.Destroy();
    if (this.shield)
      this.shield.Destroy();
  }
}

