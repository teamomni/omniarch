/******************************************************************************
Filename: ShieldEnemy.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein / Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

var ShieldStates = {
  NoState : 1,
  MoveToPlayer : 2,
  ChargeAttack : 3,
  StandStill : 4,
  ShieldDown : 5,
  Idling : 6,
  Deactivated : 7
};

// constructor
var ShieldEnemy = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member variables
  this.turnDelay = 2;
  this.turnTimer = 0;
  this.facing = Facing.Left;
  this.moveSpeed = 1;

  this.deactivated = false;
  this.activated = false;

  this.glowFadeTime = 0.3;
  this.animationDelay = 0.4;

  this.inactiveTime = 3.5;
  this.attackChargeUp = 3.5;
  this.cooldownTimer = 0;
  this.attackTimer = 0;
  this.attackRange = 3.5;
  this.aggroRange = 10;

  this.currentHealth = 1;
  this.powered = true;

  this.maxHealth = 20;
  this.maxShieldLife = 10;
  this.shieldUp = true;
  this.onShieldDown = false;
  this.deactiveTimer = 0;
  this.shieldDownTime = 2;

  // lights stuff
  this.lightsOffDuration = 0;
  this.lightsOffTimer = 0;
  this.lightsNormal = true;
  this.targetBrightness = 1.25;
  this.startBrightness = 1.25;
  this.diffBrightness = 0;

  // general state stuff
  this.previousState = ShieldStates.NoState;

  // deactivated stuff
  this.lightsDownTime = 0.5;
  this.lightsUpTime = 0.5;
  this.shieldsUp = false;
  this.deactivatedTimer = 0;
  this.deactivatedTime = 3.0;
  this.inBetweenTime = this.deactivatedTime - this.lightsDownTime - this.lightsUpTime;

  // idle state stuff
  this.idleTimer = 0;
  this.idleTime = 0;

  // new state stuff
  this.currentState = ShieldStates.NoState;
  this.justEnteredNewState = false;

  // stand still stuff
  this.standStillTime = 0;
  this.standStillTimer = 0;
  this.quickAttackTime = 0.8;
  this.quickAttackTimer = 0;
  this.attacking = false;

  // charge attack stuff
  this.createdShockwave = false;
  this.playedReadySound = false;
  };

// boilerplate create function
ShieldEnemy.Create = function() {
  return new ShieldEnemy(arguments[0]);
};



////////////////////////////////////////////////////////////////////////////////
// PROPERTIES
////////////////////////////////////////////////////////////////////////////////

ShieldEnemy.properties = {
  "turnDelay":                Property(Type.Float, 2),
  "moveSpeed":                Property(Type.Float, 1),
  "attackCooldown":           Property(Type.Float, 1),
  "attackRange":              Property(Type.Float, 3.5),
  "aggroRange":               Property(Type.Float, 10),
  "maxHealth":                Property(Type.Float, 50),
  "maxShieldLife":            Property(Type.Float, 40),
  "shieldDownTime":           Property(Type.Float, 2),
  "shieldDownGlowFadeTime":   Property(Type.Float, 0.5),
  "damageSound":              Property(Type.String_, "event:/robot-damageget"),
  "disabled" :                Property(Type.Bool, false),
  "attackAtStart" :           Property(Type.Bool, false),
  "active" :           Property(Type.Bool, true)
  
};

// function called after
ShieldEnemy.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  this.currentHealth = this.maxHealth;
  this.shieldLife = this.maxShieldLife;
  this.player = owner.space.FindEntityByName("Player");

  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "onHitStart", owner, this.onHitStart);
  Omni.Connect(this, "entity", "changeState", owner, this.changeState);
  Omni.Connect(this, "entity", "damageEvent", owner, this.onDamage);
  Omni.Connect(this, "entity", "onCollisionStart", owner, this.onCollisionStart);
  Omni.Connect(this, "entity", "pass", owner, this.onPass);

  if (!this.disabled)
    owner.Animator.Set("shield_idle");

  else {
    owner.Animator.Set("shield_disabled");
    owner.Sprite.brightness = 0.25;
    this.currentState = ShieldStates.Deactivated;
  }
  
  // create the particle effects for charging
  this.chargeEffect = owner.space.CreateEntity(
    {"name" : "shield_chargeParticle", "archetype" : "ShieldEnemyChargeEffect", "position" : {"x" : 0, "y" : 0}});  
    
  // set the depth of the particle effect
  this.chargeEffect.ParticleEmitter.depth = owner.Sprite.depth - 1;
  
  // now also set the offset
  this.chargeEffect.GetComponent("FollowEntity").targetID = this.owner_id;
  this.chargeEffect.GetComponent("FollowEntity").xOffset = 0.7;
  this.chargeEffect.GetComponent("FollowEntity").yOffset = 0.1;
  this.chargeEffect.GetComponent("FollowEntity").hasFacing = true;
  this.chargeEffect.GetComponent("FollowEntity").facingComponent = "ShieldEnemy";
  
  // create the particle effects for charge done
  this.chargeDoneEffect = owner.space.CreateEntity(
    {"name" : "shield_chargeDoneParticle", "archetype" : "ShieldEnemyChargeDoneEffect", "position" : {"x" : 0, "y" : 0}});  
    
  // set the depth of the particle effect
  this.chargeDoneEffect.ParticleEmitter.depth = owner.Sprite.depth - 1;
  
  // now also set the offset
  this.chargeDoneEffect.GetComponent("FollowEntity").targetID = this.owner_id;
  this.chargeDoneEffect.GetComponent("FollowEntity").xOffset = 0.7;
  this.chargeDoneEffect.GetComponent("FollowEntity").yOffset = 0.1;
  this.chargeDoneEffect.GetComponent("FollowEntity").hasFacing = true;
  this.chargeDoneEffect.GetComponent("FollowEntity").facingComponent = "ShieldEnemy";
};

////////////////////////////////////////////////////////////////////////////////
// EVENTS
////////////////////////////////////////////////////////////////////////////////
ShieldEnemy.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  // if this guys is just supposed to be disabled, return
  if (this.disabled)
    return;
    
  // if inactive, return
  if (!this.active)
    return;

  // if not on screen, don't update
  if (!owner.OnScreen())
    return;

  // always update lights, regardless of state
  this.UpdateLights(owner, updateEvent);

  // if no state, roll a new one
  if (this.currentState == ShieldStates.NoState) {
    this.RollState(owner);
  }

  // turning
  var targetDirection =  this.player.transform.translation.x - owner.transform.translation.x;
  if((this.facing * targetDirection < 0) && this.currentState != ShieldStates.Deactivated)
    this.Turn(owner);

  var distance = 0;

  /**********************************************************/
  /*        Deactivated (shield was broken through)         */
  /**********************************************************/

  if (this.currentState == ShieldStates.Deactivated) {
    if (this.justEnteredNewState) {
      // console.writeline("\nEntered DEACTIVATED state");
      this.LightsOff(owner, 0.25, this.lightsDownTime);
      this.deactivatedTimer = 0;
      this.shieldsUp = false;
      this.chargeEffect.ParticleEmitter.active = false;
      Utilities.Slow(0.4);

      // if charging attack, set the animation to staggered
      if (this.previousState == ShieldStates.ChargeAttack)
        owner.Animator.Set("shield_staggered");
      // else, set to the blocking staggered animation
      else
        owner.Animator.Set("shield_deactivated");

      owner.space.camera.Shake(0.01, 0.2, 0, 1);
      this.justEnteredNewState = false;
    }

    this.deactivatedTimer += updateEvent.dt;

    if (this.deactivatedTimer > this.inBetweenTime && !this.shieldsUp) {
      this.LightsOff(owner, 1.25, this.lightsUpTime);
      this.shieldsUp = true;
      owner.Animator.Set("shield_reactivated");
    }

    if (this.deactivatedTimer > this.deactivatedTime) {
      this.shieldLife = this.maxShieldLife;
      owner.Animator.Set("shield_idle");
      this.EnterNewState(ShieldStates.StandStill);
    }
  }

  /**********************************************************/
  /*         Idling state (just exited other state)         */
  /**********************************************************/

  else if (this.currentState == ShieldStates.Idling) {
    if (this.justEnteredNewState) {
      // console.writeline("\nEntered IDLING state");
      // roll a time to be idle for
      this.idleTime = 0.3 + Math.random();
      this.idleTimer = 0;
      // console.writeline("Idle for " + this.idleTime);
      this.chargeEffect.ParticleEmitter.active = false;
      this.justEnteredNewState = false;
    }

    this.idleTimer += updateEvent.dt;
    if (this.idleTimer > this.idleTime) {
      this.EnterNewState(ShieldStates.NoState);
    }
  }

  /**********************************************************/
  /*                Standing still state                    */
  /**********************************************************/

  else if (this.currentState == ShieldStates.StandStill) {
    // just entered this state
    if (this.justEnteredNewState) {
      this.standStillTime = 0.6 + Math.random();
      this.attacking = false;
      this.chargeEffect.ParticleEmitter.active = false;
      this.justEnteredNewState = false;
      this.standStillTimer = 0;
    }


    // Check Distance to target
    distance = Math.abs(targetDirection);

    // if player stands in front of the enemy for certain amount of time, do quick attack
    if (distance <= this.attackRange) {
      if (!this.attacking)
        this.quickAttackTimer += updateEvent.dt;

      if (this.quickAttackTimer > this.quickAttackTime) {
        if (!this.attacking)
          owner.Animator.Set("shield_quick_attack");

        this.attacking = true;
      }
    }

    // if the player has moved away, set the timer back to 0
    else {
      this.quickAttackTimer = 0;
    }

    if (this.attacking && owner.Animator.animationDone) {
      this.quickAttackTimer = 0;
      owner.Animator.Set("shield_idle");
      this.attacking = false;
    }

    // update timer for standing still
    this.standStillTimer += updateEvent.dt;
    //// console.writeline("standStillTimer is " + this.standStillTimer);

    // end this state by going into no state
    if (this.standStillTimer > this.standStillTime) {
      this.EnterNewState(ShieldStates.NoState);
     }
  }

  /**********************************************************/
  /*                Move to player state                    */
  /**********************************************************/
  else if (this.currentState == ShieldStates.MoveToPlayer) {
    if (this.justEnteredNewState) {
      this.chargeEffect.ParticleEmitter.active = false;
      this.justEnteredNewState = false;
    }

    // Check Distance to target
    distance = Math.abs(targetDirection);

    // If within range attack, move to standing still state
    if (distance <= this.attackRange) {
      this.EnterNewState(ShieldStates.StandStill);
    }

    // if not within attack range, take a step towards him
    else if (distance < this.aggroRange) { // Otherwise move towards the target if in aggro range
      owner.SoundEmitter.soundCue = "event:/shieldbot-step";
      owner.SoundEmitter.startPlaying = true;
      owner.Animator.Set("shield_walk");
      owner.Animator.Queue("shield_idle");
      owner.transform.translation.x += this.moveSpeed * this.facing; // Move towards target
      owner.space.camera.Shake(0.01, 0.2, 0, 1);

      this.EnterNewState(ShieldStates.StandStill);
    }
  }

  /**********************************************************/
  /*                Charge attack state                     */
  /**********************************************************/
  else if (this.currentState == ShieldStates.ChargeAttack) {
    if (this.justEnteredNewState) {
      // console.writeline("\nEntered CHARGE ATTACK state");
      owner.Animator.Set("shield_attack");
      this.createdShockwave = false;
      this.justEnteredNewState = false;
      this.playedReadySound = false;
      
      // activate particle effect
      this.chargeEffect.ParticleEmitter.active = true;
    }

    if (!this.playedReadySound && owner.Animator.frame == 2) {
      // change particle effect
      this.chargeEffect.ParticleEmitter.active = false;
      this.chargeDoneEffect.ParticleEmitter.active = true;
      
      owner.SoundEmitter.soundCue = "event:/shieldbot-punchwindup";
      owner.SoundEmitter.startPlaying = true;
      this.playedReadySound = true;
      
    }

    if (!this.createdShockwave && owner.Animator.frame == 3) {
      // create the shockwave attack
      var translation = owner.transform.translation;
      var offset = new Vec2(translation.x + this.facing * 3.5, translation.y - 1);
      var shockwave = owner.space.CreateEntity( { "name" : "splashAttack", "archetype" : "SplashAttack", "position" : offset } );

      // flip the shockwave
      if (this.facing == Facing.Right)
        shockwave.Sprite.flipX = true;

      this.createdShockwave = true;
    }

    if (owner.Animator.animationDone) {
      this.EnterNewState(ShieldStates.StandStill);
      owner.Animator.Set("shield_idle");
    }
  }
};

ShieldEnemy.prototype.EnterNewState = function(state) {
  this.previousState = this.currentState;
  this.currentState = state;
  this.justEnteredNewState = true;
};

ShieldEnemy.prototype.RollState = function(owner) {
  // console.writeline("rollin!");

  // roll number between 1 and 35
  var newState = Math.random() * 4;

  // 1 is chase player
  if (newState <= 2)
    this.currentState = ShieldStates.MoveToPlayer;

  // 2 is stand still
  if (newState > 2 && newState <= 3)
    this.currentState = ShieldStates.StandStill;

  // 3 is charge attack
  if (newState > 3)
    this.currentState = ShieldStates.ChargeAttack;

  this.justEnteredNewState = true;
};

ShieldEnemy.prototype.LightsOff = function(owner, targetBrightness, duration) {
  this.lightsNormal = false;
  this.lightsOffDuration = duration;
  this.lightsOffTimer = 0;
  this.startBrightness = owner.Sprite.brightness;
  this.targetBrightness = targetBrightness;
  this.diffBrightness = targetBrightness - this.startBrightness;
};

ShieldEnemy.prototype.UpdateLights = function(owner, updateEvent) {
  if (this.lightsNormal)
    return;

  this.lightsOffTimer += updateEvent.dt;
  owner.Sprite.brightness = this.startBrightness + this.lightsOffTimer / this.lightsOffDuration * this.diffBrightness;
  if (this.lightsOffTimer > this.lightsOffDuration) {
    owner.Sprite.brightness = this.targetBrightness;
    this.lightsNormal = true;
    this.lightsOffDuration = 0;
    this.lightsOffTimer = 0;
  }
};

ShieldEnemy.prototype.Turn = function(owner) {
  this.facing = this.facing == Facing.Left ? Facing.Right : Facing.Left;
  owner.Sprite.flipX = !owner.Sprite.flipX;
  this.turnTimer = 0;
};

ShieldEnemy.prototype.BigAttack = function(owner, target) {
  // Check Distance to target
  var distance = Math.abs(owner.transform.translation.x - target.transform.translation.x);

  // If within range attack
  if (distance <= this.attackRange) {
    owner.SoundEmitter.soundCue = "event:/shieldbot-punchwindup";
    owner.SoundEmitter.startPlaying = true;
    owner.Animator.Set("shield_attack");
    owner.Animator.Queue("shield_idle");

    // Create particles
    //var a = new Vec2(owner.transform.translation.x + this.facing, owner.transform.translation.y);
    //owner.space.CreateEntity({name : "ShieldSlamParticle", position : a, archetype : "ShieldSlamParticle"});


    // once done attacking, go inactive
    this.powered = false;
    this.deactivated = true;

    // TODO: Queue a screen shake for the second frame
  }
  else if (distance < this.aggroRange) { // Otherwise move towards the target if in aggro range
    owner.SoundEmitter.soundCue = "event:/shieldbot-step";
    owner.SoundEmitter.startPlaying = true;
    owner.Animator.Set("shield_walk");
    owner.Animator.Queue("shield_idle");
    owner.transform.translation.x += this.moveSpeed * this.facing; // Move towards target
    // TODO: Raycast to make sure your not going to stack
    owner.space.camera.Shake(0.01, 0.2, 0, 1);
  }
  // TODO: Move back to position
  this.attackTimer = 0;
};

ShieldEnemy.prototype.onCollisionStart = function () {
  var owner = arguments[0];
  var collisionEvent = arguments[1];

  var otherEntity = collisionEvent.otherEntity;

  if (otherEntity === undefined)
    return;

  // if the other guy is a player, check if he's above us
  if (otherEntity.HasComponent("PlayerLogic")) {
    if (otherEntity.transform.translation.y > owner.transform.translation.y) {
      // console.writeline("HALP HE'S ON TOP OF ME");
      //otherEntity.DispatchEvent("vaultEvent", {"impulse" : 60});
    }
  }
};

ShieldEnemy.prototype.onDamage = function () {
  var owner = arguments[0];
  var damageEvent = arguments[1];
  var soundEvent = null;

  // if we're winding up attack, do big damage!
  if (this.currentState == ShieldStates.ChargeAttack )  {
    var currFrame = owner.Animator.frame;
      if (currFrame == 1 || currFrame == 2) {
      // console.writeline("big damage!");
      this.shieldLife = 0;
      this.EnterNewState(ShieldStates.Deactivated);
      return;
    }
  }

  // only check for block if shields are up
  else if (this.currentState != ShieldStates.Deactivated) {
    // if attack is coming from the front, it is a block!

    var dotprod = damageEvent.direction * this.facing;
    if (dotprod < 0) {
      // console.writeline("blocked!");
      this.shieldLife -= damageEvent.damage;
      // console.writeline("shield life is at " + this.shieldLife);

      // set the block animation
      owner.Animator.Set("shield_idle_block");
      owner.Animator.Queue("shield_idle");

      if (this.shieldLife <= 0) {
        // console.writeline("stunned");
        this.EnterNewState(ShieldStates.Deactivated);
        owner.SoundEmitter.soundCue = "event:/shieldbot-powerdown";
        owner.SoundEmitter.startPlaying = true;
      }
      else {
        owner.SoundEmitter.soundCue = "event:/shieldbot-shieldhit";
        owner.SoundEmitter.startPlaying = true;
      }
      return;
    }
  }

  // if no direction given, or deactivated, then not blocking
  this.currentHealth -= damageEvent.damage;
  this.flashing = true;

  owner.Sprite.glowColor.g = 1;
  owner.Sprite.glowColor.b = 1;

  soundEvent = new SoundEvent("event:/robot-damageget");
  owner.space.DispatchEvent("playSound", soundEvent);

  if (this.currentHealth <= 0)
  {
      // console.writeline("defeated");
    if (this.state != EnemyStates.Defeated)
    {
      var stateEvent = new StateEvent(EnemyStates.Defeated);
      owner.DispatchEvent("changeState", stateEvent);
    }
  }
};

ShieldEnemy.prototype.onHitStart = function() {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  var damageEvent = new DamageEvent(20);
  var knockBackEvent = new KnockbackEvent(owner);
  var otherEntity = hitEvent.otherEntity;

  if (hitEvent.myType == HitBoxType.Damage && hitEvent.otherType == HitBoxType.Body && hitEvent.otherEntity.HasComponent("PlayerLogic")) {
    otherEntity.DispatchEvent("damageEvent", damageEvent);
    otherEntity.DispatchEvent("knockbackEvent", knockBackEvent);

    owner.space.camera.Shake(0.1, 0.35, 0, 1);
  }

};

// so when the door is destroyed, it reactivates
ShieldEnemy.prototype.onPass = function() {
  var owner = arguments[0];
  var passEvent = arguments[1];
  
  this.active = true;
}
  
ShieldEnemy.prototype.changeState = function() {
  var owner = arguments[0];
  var stateEvent = arguments[1];

  if (stateEvent.newState == EnemyStates.Idle) {
  }

  if (stateEvent.newState == EnemyStates.Hostile) {
  }

  // this state is for when his shield is down
  if (stateEvent.newState == EnemyStates.Deactivated) {
    this.shieldUp = false;
    this.deactiveTimer = 0;
    owner.Animator.Set("shield_deactivated");
    this.LightsOff(owner, 0.25, this.shieldDownGlowFadeTime);
    this.onShieldDown = true;
  }

  if (stateEvent.newState == EnemyStates.Defeated) {
    this.active = false;
    owner.Animator.Set("shield_defeated");
    owner.RigidBody.ghost = true;
    //owner.space.CreateEntity({name : "Shield", position : owner.transform.translation, archetype : "Shield"});
    //owner.space.CreateEntity({name : "ShieldArm", position : owner.transform.translation, archetype : "ShieldArm"});
    //var arm = owner.space.CreateEntity({name : "ShieldArm", position : owner.transform.translation, archetype : "ShieldArm"});
    //arm.Sprite.flipX = true;
    var shieldBody = owner.space.CreateEntity({name : "ShieldBody", position : owner.transform.translation, archetype : "ShieldBody"});
    shieldBody.transform.translation.y -= 1;
    shieldBody.RigidBody.angularVelocity = ((Math.random() - 0.5) * 5);
    this.chargeEffect.Destroy();
    this.chargeDoneEffect.Destroy();
    var soundEvent = new SoundEvent("event:/enemy-defeat");
    owner.space.DispatchEvent("playSound", soundEvent);
    Utilities.Slow(0.9);
  }
};

ShieldEnemy.prototype.Die = function(owner) {
  owner.space.DispatchEvent("EnemyDeath", {});

  var position = new Vec2(owner.transform.translation.x, owner.transform.translation.y);
  var explosion = owner.space.CreateEntity({name : "ExplosionSmall", position : position, archetype : "ExplosionSmall"});
  explosion.Animator.Set("explosion_small");

  // create explosions of nuts and bolts!
  Utilities.CreateNutsAndBolts(6, 0, position);

  // SFX
  var soundEvent = new SoundEvent("event:/enemy-defeat");
  owner.space.DispatchEvent("playSound", soundEvent);
  

  owner.Destroy();
};
