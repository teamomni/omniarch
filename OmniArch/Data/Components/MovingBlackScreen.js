/******************************************************************************
Filename: MovingBlackScreen.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var MovingBlackScreen = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
  
  this.direction = 0;
  this.timer = 0;
  this.atTarget = false;
  this.animatedPlayer = false;
  this.movedPlayer = false;
  this.initialized = false;
  this.atTarget = false;
  
  this.bgmFadeTimer = 0;
  this.bgm = 0;
  this.bgmFaded = false;
  this.bgmFadeTime = 2;
  
  this.trainSoundFadeTimer = 0;
  this.trainSound = 0;
  this.trainSoundFaded = false;
  this.trainSoundFadeTime = 2;
  
  this.player = 0;
  this.initialPlayerXPosition = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

MovingBlackScreen.Create = function() {
  // required function; do not remove
  return new MovingBlackScreen(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

MovingBlackScreen.properties = {
  "moveSpeed":       Property(Type.Float, 1),
  "targetX"  :       Property(Type.Float, 0),
  "lifetime" :       Property(Type.Float, 3),
  "level"    :       Property(Type.String_, "ShieldGuy")
};

//////////////////
//  INITIALIZE  //
//////////////////

MovingBlackScreen.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
MovingBlackScreen.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  // initialize code
  if (!this.initialized) {
    // find the bgm
    this.bgm = Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusic");
    this.trainSound = Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusicTrain");
    this.player = owner.space.FindEntityByName("Player");
  
    if (this.targetX > owner.transform.translation.x)
      this.direction = 1;
    else
      this.direction = -1;
    this.initialized = true;
  }
  
  // fade the bgm
  if (!this.bgmFaded) {
    this.bgmFadeTimer += updateEvent.dt;
    this.bgm.SoundEmitter.volume = 1.0 - (this.bgmFadeTimer / this.bgmFadeTime);
      
    if (this.bgmFadeTimer >= this.bgmFadeTime) {
      this.bgm.SoundEmitter.volume = 0;
      this.bgmFaded = true;
    }
  }
  
  // moving the black screen in
  if (!this.atTarget) {
    var trans = owner.transform.translation;
    
    // if moving right
    if (this.direction == 1) {
      trans.x += this.moveSpeed;
      if (trans.x >= this.targetX) {
        trans.x = this.targetX;
        this.atTarget = true;
      }
    }
    
    // if moving left
    else {
      trans.x -= this.moveSpeed;
      if (trans.x <= this.targetX) {
        trans.x = this.targetX;
        this.atTarget = true;
      }
    }
  }
  
  // once we are in place, start the rest of the stuff
  else {
    this.timer += updateEvent.dt;
    
    // once the bgm has faded, move the player
    if (this.bgmFaded) {
      
      // once bgm is faded
      if (!this.trainSoundFaded) {
        this.trainSoundFadeTimer += updateEvent.dt;
        this.trainSound.SoundEmitter.volume = 0.5 - (this.trainSoundFadeTimer / this.trainSoundFadeTime);
          
        if (this.trainSoundFadeTimer >= this.trainSoundFadeTime) {
          this.trainSound.SoundEmitter.volume = 0;
          this.trainSoundFaded = true;
        }
      }
    
      var playerTrans = this.player.transform.translation;
      
      // set the player to start running once
      if (!this.animatedPlayer) {
        this.player.Animator.Set("plr_run");
        this.animatedPlayer = true;
        this.initialPlayerXPosition = this.player.transform.translation.x;
      }
      
      // move the player 
      if (!this.movedPlayer) {
        playerTrans.x += 0.08 * updateEvent.dt * 60;
        
        if (playerTrans.x > this.initialPlayerXPosition + 11) {
          this.movedPlayer = true;
          this.player.RigidBody.static = true;
          Omni.LoadLevel("ShieldGuy");
          this.trainSound = Omni.FindSpaceByName("ui").FindEntityByName("BackgroundMusicTrain");
        }
      }
    }
  }
};
