/******************************************************************************
Filename: BombDrone.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// constructor
var BombDrone = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member variables
  this.dropBombs = false;

  this.bombsDropped = 0;
  this.dropCooldown = 0.42;
  this.dropTimer = 0;

  this.waitTime = 4;
  this.waitTimer = 0;
  this.waiting = false;

  this.maxSpeed = 10;

  this.active = true;
};

// boilerplate create function
BombDrone.Create = function() {
  return new BombDrone(arguments[0]);
};

////////////////////////////////////////////////////////////////////////////////
// PROPERTIES
////////////////////////////////////////////////////////////////////////////////

BombDrone.properties = {

};

// function called after
BombDrone.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "AggroEvent", owner, this.OnAggroEvent);
  Omni.Connect(this, "entity", "changeState", owner, this.changeState);
  owner.Animator.Set("bombdrone_idle");
};

////////////////////////////////////////////////////////////////////////////////
// EVENTS
////////////////////////////////////////////////////////////////////////////////
BombDrone.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!this.active)
    return;

  if (!owner.OnScreen())
    return;
  /*
    2 Move Towards
    3 Drop Bomb
    4 Fly Away
  */
  var player = Omni.FindEntityByID(owner.GetComponent("Drone").playerID);

  if (!this.waiting)
  {
    this.MoveTowardsTarget(owner, player);

    this.dropTimer += updateEvent.dt;
    
    this.CheckBombs(owner, player);
  }
  else {
    this.waitTimer += updateEvent.dt;
    if (this.waitTimer >= this.waitTime)
      this.waiting = false;
  }
  
  
};

BombDrone.prototype.MoveTowardsTarget = function(owner, target) {
  var xDistance = target.transform.translation.x - owner.transform.translation.x;

  if (xDistance > 0)
    owner.RigidBody.ApplyLinearForce(5, 0);
  else
    owner.RigidBody.ApplyLinearForce(-5, 0);

  if (owner.RigidBody.velocity.x > this.maxSpeed)
    owner.RigidBody.velocity.x = this.maxSpeed;
  if (owner.RigidBody.velocity.x < -this.maxSpeed)
    owner.RigidBody.velocity.x = -this.maxSpeed;
};

BombDrone.prototype.CheckBombs = function(owner, target) {
  var xDistance = target.transform.translation.x - owner.transform.translation.x;

  if (xDistance <= 3.5 && xDistance >= -3.5)
    this.dropBombs = true;

  if (this.dropBombs && this.bombsDropped < 5 && this.dropTimer > this.dropCooldown) {
    owner.DispatchEvent("DropBomb", {});
    owner.Animator.Set("bombdrone_drop");
    owner.Animator.Queue("bombdrone_idle");

    var a = new Vec2(owner.transform.translation.x, owner.transform.translation.y);
    var bomb = owner.space.CreateEntity({name : "Bomb", position : a, archetype : "Bomb"});
    bomb.GetComponent("Bomb").thrown = true;
    bomb.GetComponent("Bomb").enemyBomb = true;

    ++this.bombsDropped;
    this.dropTimer = 0;
  }

  if (this.bombsDropped == 5) {
    this.waiting = true;
    this.waitTimer = 0;
    this.bombsDropped = 0;
  }
};

BombDrone.prototype.OnAggroEvent = function() {

};

BombDrone.prototype.changeState = function() {
  var owner = arguments[0];
  var stateEvent = arguments[1];

  if (stateEvent.newState == EnemyStates.Idle) {
  }

  if (stateEvent.newState == EnemyStates.Hostile) {
  }

  if (stateEvent.newState == EnemyStates.Defeated) {
    this.active = false;
  }
};
