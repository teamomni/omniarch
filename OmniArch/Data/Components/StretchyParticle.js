/******************************************************************************
Filename: StretchyParticle.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/


///////////////////
//  CONSTRUCTOR  //
///////////////////

var StretchyParticle = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
  
  // Member Variables
  this.stretchSpeed = 1.0;
  this.startX = 0;
  this.startY = 0;
  this.endX = 0;
  this.endY = 0;
  this.diffX = 0;
  this.diffY = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

StretchyParticle.Create = function() {
  // required function; do not remove
  return new StretchyParticle(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////
StretchyParticle.properties = {
  "stretchSpeed":   Property(Type.Float, 0.4),
  "firstParticle" : Property(Type.Bool, false),
  "targetLinkID":   Property(Type.Int, 0),
  "r":              Property(Type.Float, 1.0),
  "g":              Property(Type.Float, 1.0),
  "b":              Property(Type.Float, 1.0),
  "a":              Property(Type.Float, 1.0),
  
};

//////////////////
//  INITIALIZE  //
//////////////////

StretchyParticle.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];
  
  owner.Sprite.visible = false;
  
  this.startX = this.endX = owner.transform.translation.x;
  this.startY = this.endY = owner.transform.translation.y;
  
  this.targetObject = Omni.FindEntityByLinkID(this.targetLinkID);
  this.targetObjectParticleComp = this.targetObject.GetComponent("StretchyParticle");
  this.targetPlayerController = this.targetObject.GetComponent("PlayerController");
  
  // this is where you connect events
  // Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  
  owner.Sprite.color.r = this.r;
  owner.Sprite.color.g = this.g;
  owner.Sprite.color.b = this.b;
  owner.Sprite.color.a = this.a;
  
  owner.Sprite.depth = -21;
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
StretchyParticle.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  // get the target position
  if (this.firstParticle)
  {
    this.endX = this.targetObject.transform.translation.x;
    this.endY = this.targetObject.transform.translation.y;
    
    var facing = this.targetPlayerController.facing;
    this.endX += facing * 0.2;
    this.endY += 0.05;
  }
  
  // if it's not the first particle, get the previous particle's tail
  else
  {
    this.endX = this.targetObjectParticleComp.startX
    this.endY = this.targetObjectParticleComp.startY;
  }
  
  // now scale the thing and place it between start and end
  this.diffX = this.endX - this.startX;
  this.diffY = this.endY - this.startY;
  
  // move the start towards the end
  this.startX += this.stretchSpeed * this.diffX;
  this.startY += this.stretchSpeed * this.diffY;
  
  // update the difference
  this.diffX = this.endX - this.startX;
  this.diffY = this.endY - this.startY;
  
  var length = Math.sqrt(this.diffX * this.diffX + this.diffY * this.diffY);
  
  owner.transform.scale.x = length;
  owner.transform.translation.x = this.startX + this.diffX / 2;
  owner.transform.translation.y = this.startY + this.diffY / 2;
  
  // set rotation
  var rotation = Math.atan2(this.diffY, this.diffX);
  owner.transform.rotation = rotation;
};

