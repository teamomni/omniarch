/******************************************************************************
Filename: EncounterTrigger.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var EncounterTrigger = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.encounterNumber = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

EncounterTrigger.Create = function() {
  // required function; do not remove
  return new EncounterTrigger(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

EncounterTrigger.properties = {
  "encounterNumber":  Property(Type.Unsigned, 0)
};

//////////////////
//  INITIALIZE  //
//////////////////

EncounterTrigger.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "entity", "onCollisionStart", owner, this.onCollisionStart);
};

EncounterTrigger.prototype.onCollisionStart = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];

  if (collisionEvent.otherEntity.GetComponent("PlayerLogic")) {
    owner.space.DispatchEvent("encounterTriggered", new EncounterEvent(this.encounterNumber));
  }
};

var EncounterEvent = function (encounterNumber) {
  this.encounterNumber = encounterNumber;
};
