/******************************************************************************
Filename: LoadLevelOnTrigger.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var LoadLevelOnTrigger = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

LoadLevelOnTrigger.Create = function() {
  // required function; do not remove
  return new LoadLevelOnTrigger(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

LoadLevelOnTrigger.properties = {
  "levelToLoad" : Property(Type.String_, "First")
};

//////////////////
//  INITIALIZE  //
//////////////////

LoadLevelOnTrigger.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "entity", "CollisionTriggered", owner, this.onCollisionTrigger);
};

LoadLevelOnTrigger.prototype.onCollisionTrigger = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];
  
  Omni.LoadLevel(this.levelToLoad);
};

