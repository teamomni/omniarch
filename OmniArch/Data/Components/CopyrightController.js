/******************************************************************************
Filename: CopyrightController.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////
var CopyrightController = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // initialize member variables here
  this.fadingOut = false;

  // run any other code that should be executed when this Component
  // is created here (note that this is executed right when the
  // Component is created, but BEFORE it has been attached to its
  // owner Entity
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////
CopyrightController.Create = function() {
  // required function; do not remove
  return new CopyrightController(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////
CopyrightController.properties = {};

//////////////////
//  INITIALIZE  //
//////////////////
CopyrightController.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
};

//---------------------------//
//  example helper function  //
//---------------------------//
CopyrightController.prototype.SomeHelperFunction = function(owner) {

};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
CopyrightController.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (CheckInput("menu_confirm") && !this.fadingOut) {
    this.fadingOut = true;
  }
  if (this.fadingOut) {
    owner.Sprite.color.a -= 0.05;
    if (owner.Sprite.color.a <= 0) {
      owner.Sprite.visible = false;
      owner.Sprite.color.a = 0;
    }
  }


};
