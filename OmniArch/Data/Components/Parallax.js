/******************************************************************************
Filename: Parallax.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var Parallax = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.delayScale = 1; // 1 = camera speed, 0.5 = half speed
  this.delayScaleY = 1;

  this.lastCameraTranslation = new Vec2();

  this.firstFrame = false;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

Parallax.Create = function() {
  // required function; do not remove
  return new Parallax(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

Parallax.properties = {
};

//////////////////
//  INITIALIZE  //
//////////////////

Parallax.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onFrameUpdate", owner, 0, this.onUpdate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
Parallax.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  var camera = owner.space.camera;

  var cameraMovementX = camera.transform.translation.x - this.lastCameraTranslation.x;
  var cameraMovementY = camera.transform.translation.y - this.lastCameraTranslation.y;

  if (this.firstFrame) {
    this.firstFrame = true;
    this.lastCameraTranslation.set(owner.space.camera.transform.translation.x, owner.space.camera.transform.translation.y);
    return;
  }

  followCamera.xOffset -= cameraMovementX * this.delayScale;
  followCamera.yOffset -= cameraMovementY * this.delayScale;

  this.lastCameraTranslation.set(owner.space.camera.transform.translation.x, owner.space.camera.transform.translation.y);

};
