/******************************************************************************
Filename: FollowEntity.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var FollowEntity = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  this.active = true;
  this.activated = false;
  this.target = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

FollowEntity.Create = function() {
  // required function; do not remove
  return new FollowEntity(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

FollowEntity.properties = {
  "xOffset": Property(Type.Float, 0),
  "active": Property(Type.Bool, true),
  "yOffset": Property(Type.Float, 0),
  "targetLinkID" : Property(Type.Int, -1),
  "targetID" : Property(Type.Int, -1),
  "hasFacing" : Property(Type.Bool, false),
  "facingComponent" : Property(Type.String_, ""),
  "connectDeath" : Property(Type.Bool, false)
};

//////////////////
//  INITIALIZE  //
//////////////////

FollowEntity.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "Death", owner, this.onDeath);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
FollowEntity.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  // if we were given an ID AFTER we were created (meaning through scripting), then identify here
  if (!this.activated) {
    if (this.targetLinkID != -1) {
      this.target = Omni.FindEntityByLinkID(this.targetLinkID);
      this.activated = true;
    }
    
    if (this.targetID != -1) {
      this.target = Omni.FindEntityByID(this.targetID);
      this.activated = true;
    }
  }
  
  if (!this.activated || !this.active)
    return;

  // Follow Entity
  if (!this.hasFacing)
    owner.FollowEntity(this.target, this.xOffset, this.yOffset);
  
  else {
    var facingComp = this.target.GetComponent(this.facingComponent);
    if (facingComp === undefined)
      return;
    
    var facing = facingComp.facing;
    owner.FollowEntity(this.target, facing * this.xOffset, this.yOffset);
  }
};


FollowEntity.prototype.onDeath = function() {
  var owner = arguments[0];
  var deathEvent = arguments[1];

  // kill if death is set
  if (this.connectDeath) {
    owner.Destroy();
  }
};
