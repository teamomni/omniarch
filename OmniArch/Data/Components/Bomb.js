/******************************************************************************
Filename: Bomb.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// constructor
var Bomb = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member variables
  this.active = false;
  this.thrown = false;
  this.explode = false;
  this.enemyBomb = false;

  this.detonateTimer = 0;
  this.bombPile = false;

  this.hittingPlayer = false;
};

// boilerplate create function
Bomb.Create = function() {
  return new Bomb(arguments[0]);
};

////////////////////////////////////////////////////////////////////////////////
// PROPERTIES
////////////////////////////////////////////////////////////////////////////////

Bomb.properties = {
  "detonateTime":      Property(Type.Float,    8),
  "bombPile":      Property(Type.Bool,    false)
};

// function called after
Bomb.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "onHitStart", owner, this.onHitStart);
  Omni.Connect(this, "entity", "onCollisionStart", owner, this.onCollisionStart);
  Omni.Connect(this, "entity", "onHitEnd", owner, this.onHitEnd);
  Omni.Connect(this, "entity", "PickedUp", owner, this.onPickedUp);
  Omni.Connect(this, "entity", "Thrown", owner, this.onThrown);
  owner.Animator.Set("bomb");
  owner.RigidBody.velocity.y = -3;

  if (this.bombPile)
    owner.Animator.Set("bomb_pile");

};

////////////////////////////////////////////////////////////////////////////////
// EVENTS
////////////////////////////////////////////////////////////////////////////////
Bomb.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!this.active)
    return;

  this.detonateTimer += updateEvent.dt;

  owner.Sprite.brightness = this.detonateTimer % (this.detonateTime - this.detonateTimer - 5.5 + (this.detonateTimer/1.4));

  if (this.detonateTimer >= this.detonateTime) {
    this.Explode(owner);
  }
};

Bomb.prototype.onHitStart = function() {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  if (!hitEvent.otherEntity)
    return;

  if (hitEvent.myType == HitBoxType.Damage && hitEvent.otherType == HitBoxType.Body && hitEvent.otherEntity.GetComponent("PlayerLogic")) {
    this.hittingPlayer = true;
  }
};

Bomb.prototype.onHitEnd = function() {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  if (!hitEvent.otherEntity)
    return;

  if (hitEvent.myType == HitBoxType.Damage && hitEvent.otherType == HitBoxType.Body && hitEvent.otherEntity.GetComponent("PlayerLogic")) {
    this.hittingPlayer = false;
  }
};

Bomb.prototype.onCollisionStart = function() {
  var owner = arguments[0];
  var collisionEvent = arguments[1];

  if (collisionEvent.otherEntity.RigidBody.ghost)
    return;

  if (this.thrown && !collisionEvent.otherEntity.GetComponent("BombDrone"))
    this.Explode(owner);
};

Bomb.prototype.Explode = function(owner) {
  var position = new Vec2(owner.transform.translation.x, owner.transform.translation.y);
  var explosion = owner.space.CreateEntity({name : "ExplosionLarge", position : position, archetype : "ExplosionLarge"});
  explosion.ParticleEmitter.active = true;

  // Damage
  if (this.enemyBomb) {
    if (this.hittingPlayer) {
      var damageEvent = new DamageEvent(30);
      var player = owner.space.FindEntityByName("Player");
      player.DispatchEvent("damageEvent", damageEvent);
    }
  }
  else
    owner.space.CreateEntity({name : "BombDamage", position : position, archetype : "BombDamage"});

  owner.space.camera.Shake(0.01, 0.2, 0, 1);

  var soundEvent = new SoundEvent("event:/bomb");
    owner.space.DispatchEvent("playSound", soundEvent);
  
  owner.DispatchEvent("Death", {});
  owner.Destroy();
};

Bomb.prototype.onPickedUp = function() {
  var owner = arguments[0];

  this.active = true;

  if (this.bombPile)
  {
    owner.Animator.Set("bomb");
    owner.RigidBody.static = false;
    this.bombPile = false;

    var a = new Vec2(owner.transform.translation.x, owner.transform.translation.y);
    var bomb = owner.space.CreateEntity({name : "BombPile", position : a, archetype : "BombPile"});
  }
};

Bomb.prototype.onThrown = function() {
  this.thrown = true;
};
