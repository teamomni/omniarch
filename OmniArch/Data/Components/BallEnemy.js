/******************************************************************************
Filename: BallEnemy.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

///////////////////
//  CONSTRUCTOR  //
///////////////////

var BallEnemy = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  this.active = true;

  // Member Variables
  this.aggroRange = 20;
  this.aggroed = false;

  this.accelSpeed = 5;
  this.maxSpeed = 2.5;

  this.attackCooldown = 1.5;
  this.attackChargeTime = 0.6;
  this.attackTimer = 0;
  this.attackRange = 1;
  this.damage = 5;
  this.cooldownTimer = 0;

  this.hopStrength = 20;
  this.hopCooldown = 0.75;
  this.hopTimer = 0;

  this.hoverHeight = 0.5;
  this.bobRange = 0.1;
  this.bobPosition = 0;

  this.hesitate = false;
  this.hesitateTimer = 0;

  this.facing = Facing.Left;

  this.grounded = false;

  this.state = BallStates.Traveling;

  this.groundY = 0;
  this.groundCheckTimer = 0;

  this.hovering = false;

  this.accountedForFlip = false;

  this.toBob = true;

};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

BallEnemy.Create = function() {
  // required function; do not remove
  return new BallEnemy(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

BallEnemy.properties = {
  "aggroRange":     Property(Type.Float, 20),
  "accelSpeed":     Property(Type.Float, 5),
  "maxSpeed":       Property(Type.Float, 3),
  "attackRange":    Property(Type.Float, 1),
  "damage":         Property(Type.Float, 5),
  "hopStrength":    Property(Type.Float, 12)
};

//////////////////
//  INITIALIZE  //
//////////////////

BallEnemy.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "changeState", owner, this.changeState);
  Omni.Connect(this, "entity", "onCollisionStart", owner, this.onCollisionStart);
  Omni.Connect(this, "entity", "onCollisionPersist", owner, this.onCollisionPersist);
  Omni.Connect(this, "entity", "onCollisionEnd", owner, this.onCollisionEnd);

  owner.Animator.Set("ball_enemy_roll");

  this.attackRange += Random.RandomRange(-0.2, -0.05);
  this.hoverHeight += Random.RandomRange(-0.25, 0.05);
};

var BallStates = {
  Traveling : 0,
  Combat : 1
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
BallEnemy.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  if (!this.active || !owner.OnScreen()) {
    owner.RigidBody.angularVelocity = 0;
    return;
  }

  ///////////////
  // Spawning //
  ///////////////
  if (this.state == BallStates.Traveling) {
    if (this.grounded)
      this.EnterCombat(owner);
    return;
  }

  ////////////
  // COMBAT //
  ////////////

  if (!this.hovering) {
    if (owner.Animator.animationDone) {
      this.hovering = true;
      owner.Animator.Set("ball_enemy_hover");
      owner.ParticleEmitter.active = true;
    }
    return;
  }

  this.hesitateTimer += updateEvent.dt;

  if (this.hesitateTimer >= 3.5)
  {
    if (this.hesitate)
      this.hesitate = false;

    var randInt = Random.RandomRangeInt(0, 4);

    if (!randInt) {
      this.hesitate = true;
      this.toBob = true;
    }
      
    this.hesitateTimer = 0;
  }

  // Check for ground
  this.groundCheckTimer += updateEvent.dt;

  if (this.groundCheckTimer >= 0.5)
  {
    var collisions = Omni.CastRay(owner.transform.translation, {x:0, y:-1}, 10);
    for (var i = 0; i < collisions.length; ++i) {
      if (collisions[i].name == "Wall" || collisions[i].name == "Ground" || collisions[i].name == "terrain") {
        this.groundY = collisions[i].transform.translation.y + (collisions[i].transform.scale.y/2);
      }
    }

    this.groundCheckTimer = 0;
  }

  this.Bob(owner);

  if (owner.GetComponent("Enemy").state == EnemyStates.Idle)
    this.CheckAggro(owner);

  if (this.aggroed && !this.hesitate) {
    this.cooldownTimer += updateEvent.dt;
  
    if (!this.CheckAttack(owner, updateEvent.dt))
      this.MoveTowardsPoint(owner);
  }
  else {
    // Break
    owner.RigidBody.velocity.x *= 0.5;
    owner.RigidBody.angularVelocity = 0;
    owner.hopTimer = 0;
  }

  this.RotateWithVelocity(owner);
};

BallEnemy.prototype.CheckAggro = function(owner) {
  var player = Omni.FindEntityByID(this.playerID);
  var vecToTarget = new Vec2(owner.transform.translation.x, owner.transform.translation.y);
  vecToTarget.sub(vecToTarget, player.transform.translation);
  var distanceToTarget = vecToTarget.length();

  if (distanceToTarget <= this.aggroRange) {
    this.aggroed = true;
    var stateEvent = new StateEvent(EnemyStates.Hostile);
    owner.DispatchEvent("changeState", stateEvent);
    return true;
  }

  return false;
};

BallEnemy.prototype.CheckAttack = function(owner, dt) {
  var player = Omni.FindEntityByID(this.playerID);
  var vecToTarget = new Vec2(owner.transform.translation.x, owner.transform.translation.y);
  vecToTarget.sub(vecToTarget, player.transform.translation);
  var distanceToTarget = vecToTarget.length();

  if (distanceToTarget <= this.attackRange) {
    // Update facing direction
    var targetPoint = owner.space.FindEntityByName("Player").transform.translation;

    // Find facing direction
    direction =  targetPoint.x - owner.transform.translation.x;
    
    if((this.facing > 0 && direction < 0) || (this.facing < 0 && direction > 0)) {
      this.Turn(owner);
    }
    
    // Attack
    if (this.cooldownTimer > this.attackCooldown) {
      owner.Sprite.brightness = Ease.Easy(owner.Sprite.brightness, 5, 0.05);
      owner.transform.translation.y = Ease.Easy(owner.transform.translation.y, player.transform.translation.y - 0.5 + this.hoverHeight, 0.1);
      owner.RigidBody.Clear();
      this.attackTimer += dt;
      this.toBob = false;
    }
    else
      owner.Sprite.brightness = Ease.Easy(owner.Sprite.brightness, 0.5, 0.9);
      
    if (this.attackTimer >= this.attackChargeTime) {
      var a = new Vec2(owner.transform.translation.x + (this.facing * 0.5), owner.transform.translation.y);
      owner.space.CreateEntity({name : "Zap", position : a, archetype : "ZapParticle"});

      player.DispatchEvent("damageEvent", new DamageEvent(this.damage));
      player.DispatchEvent("knockbackEvent", new KnockbackEvent(owner));
      this.attackTimer = 0;
      this.cooldownTimer = 0;
      this.toBob = true;
    }
    return true;
  }
  else
  {
    owner.Sprite.brightness = Ease.Easy(owner.Sprite.brightness, 0.5, 0.1);
    this.attackTimer = 0;
  }

  return false;
};

BallEnemy.prototype.MoveTowardsPoint = function(owner) {
  // Clamp X
  if (owner.RigidBody.velocity.x > this.maxSpeed)
  owner.RigidBody.velocity.x = this.maxSpeed;
  if (owner.RigidBody.velocity.x < -this.maxSpeed)
    owner.RigidBody.velocity.x = -this.maxSpeed;


  // Clamp Y
  if (owner.RigidBody.velocity.y > this.maxSpeed * 2)
  owner.RigidBody.velocity.y = this.maxSpeed * 2;
  if (owner.RigidBody.velocity.y < -this.maxSpeed * 2)
    owner.RigidBody.velocity.y = -this.maxSpeed * 2;

  
  var targetPoint = owner.space.FindEntityByName("Player").transform.translation;

  // Find facing direction
  direction =  targetPoint.x - owner.transform.translation.x;

  // Update facing direction
  if((this.facing > 0 && direction < 0) || (this.facing < 0 && direction > 0))
    this.Turn(owner);

  if (direction <= -0.3 || direction >= 0.3) {
    //owner.RigidBody.angularVelocity += this.accelSpeed * -this.facing;
    owner.RigidBody.ApplyLinearForce(this.accelSpeed * this.facing, 0);
  }
};

BallEnemy.prototype.Bob = function(owner) {
  var distanceFromGround = owner.transform.translation.y - this.groundY;
  distanceFromGround = Math.abs(distanceFromGround);

  if (!this.toBob)
    owner.RigidBody.velocity.y = 0;
  
  else {
    if (distanceFromGround > this.hoverHeight)
      owner.RigidBody.velocity.y -= 0.03;
    if (distanceFromGround < this.hoverHeight)
      owner.RigidBody.velocity.y += 0.03;
  }
}

BallEnemy.prototype.Turn = function(owner) {
  this.facing = this.facing == Facing.Left ? Facing.Right : Facing.Left;
  owner.Sprite.flipX = !owner.Sprite.flipX;
};

BallEnemy.prototype.changeState = function() {
  var owner = arguments[0];
  var stateEvent = arguments[1];

  if (stateEvent.newState == EnemyStates.Idle) {
  }

  if (stateEvent.newState == EnemyStates.Hostile) {
  }

  if (stateEvent.newState == EnemyStates.Defeated) {
    this.active = false;
  }
};

BallEnemy.prototype.onCollisionStart = function () {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  if (hitEvent.otherEntity == undefined)
    return;

  if (hitEvent.otherEntity.name == "Wall" || hitEvent.otherEntity.name == "Ground" || hitEvent.otherEntity.name == "terrain") {
    this.grounded = true;
    this.groundY = owner.transform.translation.y;
    if(owner.Sprite.flipX && !this.accountedForFlip) {
      owner.Sprite.flipX = false;
      this.accountedForFlip = true;
    }
  }
};

BallEnemy.prototype.onCollisionPersist = function () {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  if (hitEvent.otherEntity == undefined)
    return;

  if (hitEvent.otherEntity.name == "Wall" || hitEvent.otherEntity.name == "Ground" || hitEvent.otherEntity.name == "terrain") {
    this.grounded = true;
    this.groundY = owner.transform.translation.y;
  }
};

BallEnemy.prototype.onCollisionEnd = function () {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  if (hitEvent.otherEntity == undefined)
    return;

  if (hitEvent.otherEntity.name == "Wall" || hitEvent.otherEntity.name == "Ground" || hitEvent.otherEntity.name == "terrain") {
    this.grounded = false;
    this.groundY = owner.transform.translation.y;
  }
};

BallEnemy.prototype.EnterCombat = function (owner) {
  this.state = BallStates.Combat;
  owner.RigidBody.gravityScale = 0;

  // "hop"
  owner.Animator.Set("ball_enemy_lift");

};

BallEnemy.prototype.RotateWithVelocity = function(owner) {
  // Rotate relative to velocity
  var speedPercent = owner.RigidBody.velocity.x / this.maxSpeed;
  owner.transform.rotation = -0.35 * speedPercent;
};
