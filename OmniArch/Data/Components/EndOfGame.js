/******************************************************************************
Filename: EndOfGame.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var EndOfGame = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables

};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

EndOfGame.Create = function() {
  // required function; do not remove
  return new EndOfGame(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

EndOfGame.properties = {
  
};

//////////////////
//  INITIALIZE  //
//////////////////

EndOfGame.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "entity", "CollisionTriggered", owner, this.onEndGame);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
EndOfGame.prototype.onEndGame = function() {
  var owner = arguments[0];
  var triggerEvent = arguments[1];
  
  var camera = owner.space.camera;
  
  // stop the input from the player
  player.GetComponent("PlayerController").active = false;
  
  // stop the player itself
  player.Animator.Set("plr_idle");
  player.RigidBody.velocity.x = 1;
};
