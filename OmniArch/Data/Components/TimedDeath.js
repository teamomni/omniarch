/******************************************************************************
Filename: TimedDeath.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var TimedDeath = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.active = true;
  this.animationBased = false;
  this.lifespan = 1;

  this.timer = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

TimedDeath.Create = function() {
  // required function; do not remove
  return new TimedDeath(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

TimedDeath.properties = {
  "active":         Property(Type.Bool,  true),
  "lifespan":       Property(Type.Float, 1),
  "animationBased": Property(Type.Bool,  false)
};

//////////////////
//  INITIALIZE  //
//////////////////

TimedDeath.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
TimedDeath.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  if (!this.active)
    return;

  if (this.animationBased) {
    if (owner.Animator.animationDone) {
      // TODO: Switch back to regular destroy
      this.active = false;
      owner.transform.translation.y -= 100;
    }
  }
  
  else {
    this.timer += updateEvent.dt;

    if (this.timer >= this.lifespan) {
      // TODO: Switch back to regular destroy
      owner.Destroy();
    }
  }
};
