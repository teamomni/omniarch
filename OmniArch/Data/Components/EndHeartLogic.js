/******************************************************************************
Filename: EndHeartLogic.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var EndHeartLogic = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables
  this.timer = 0;
  this.player;
  
  this.health = 40;
  this.dead = false;
  
  this.exploding = false;

  this.explosionTimer = 0;
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

EndHeartLogic.Create = function() {
  // required function; do not remove
  return new EndHeartLogic(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

EndHeartLogic.properties = {
  
};

//////////////////
//  INITIALIZE  //
//////////////////

EndHeartLogic.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "damageEvent", owner, this.onDamage);
  
  owner.ParticleEmitter.depth = owner.Sprite.depth - 1;
  
  // make the background
  // this.background.Sprite.image = "wallLight";
  // this.background.Sprite.brightness = owner.Sprite.brightness;
  // this.background.Sprite.depth = owner.Sprite.depth + 1;
  // this.background.Sprite.glowEnabled = true;
  // 
  // this.background.Sprite.glowColor.r = owner.Sprite.glowColor.r;
  // this.background.Sprite.glowColor.g = owner.Sprite.glowColor.g;
  // this.background.Sprite.glowColor.b = owner.Sprite.glowColor.b;
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
EndHeartLogic.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  var player = owner.space.FindEntityByName("Player");
  
  if (!this.dead) {
    owner.Sprite.glowColor.g = Ease.Easy(owner.Sprite.glowColor.g, 0, 0.1);
    owner.Sprite.glowColor.b = Ease.Easy(owner.Sprite.glowColor.b, 0, 0.1);

    this.background = owner.space.FindEntityByName("WallLight");
    var targetBrightness = 0.8 - 0.3 * Math.cos(2 * this.timer);
    owner.Sprite.brightness = Ease.Easy(owner.Sprite.brightness, targetBrightness, 0.1);
    this.background.Sprite.brightness = owner.Sprite.brightness;
    player.Sprite.brightness = owner.Sprite.brightness - 0.3;
    
    this.timer += updateEvent.dt;
  }
  
  else if (this.exploding) {
    this.timer += updateEvent.dt;
    this.explosionLight.transform.scale.x += 10;
    this.explosionLight.transform.scale.y += 10;

    // Add final explosion sound
    
    if (this.timer >= 0.1)
      Omni.LoadLevel("Credits");
  }
  
  else {
    var targetBrightness = 0.8 - 0.3 * Math.cos(5 * this.timer * this.timer);
    var targetR = 0.8 - 0.3 * Math.cos(5 * this.timer * this.timer);
    var targetG = 0.8 - 0.3 * Math.cos(5 * this.timer * this.timer);
    var targetB = 0.8 - 0.3 * Math.cos(5 * this.timer * this.timer);
    owner.space.camera.Shake(0.1, 1, 0, 0.1);
    
    if (this.timer >= 2) {
      owner.Sprite.glowColor.r = targetR;
      owner.Sprite.glowColor.g = targetG;
      owner.Sprite.glowColor.b = targetB;
      this.background.Sprite.glowColor.r = targetR - 0.2;
      this.background.Sprite.glowColor.g = targetG - 0.2;
      this.background.Sprite.glowColor.b = targetB - 0.2;
    }
    this.explosionTimer += updateEvent.dt;

    if (this.explosionTimer >= 0.25)
    {
      var enemyDefeat1 = owner.space.CreateEntity({"name" : "EnemyDefeatSound", "archetype" : "SoundEffect", "position" : {"x" : 0, "y" : 0}});
      enemyDefeat1.SoundEmitter.soundCue = "event:/enemy-defeat";
      enemyDefeat1.SoundEmitter.startPlaying = true;
      this.explosionTimer = 0;
    }
    
    owner.Sprite.brightness = targetBrightness;
    // owner.Sprite.color.r = targetR;
    // owner.Sprite.color.g = targetG;
    // owner.Sprite.color.b = targetB;
    
    this.background.Sprite.brightness = targetBrightness - 0.2;
    // this.background.Sprite.glowColor.r = targetR - 0.2;
    // this.background.Sprite.glowColor.g = targetG - 0.2;
    // this.background.Sprite.glowColor.b = targetB - 0.2;
    
    this.timer += updateEvent.dt;
    
    if (this.timer >= 2.1) {
      this.exploding = true;
      // owner.Sprite.visible = false;
      owner.ParticleEmitter.active = true;
      var trans = owner.transform.translation;
      this.explosionLight = owner.space.CreateEntity({"name" : "explosionLight", "archetype" : "GenericSprite", "position" : { "x" : trans.x - 0.5, "y" : trans.y - 0.7}});
      this.explosionLight.Sprite.image = "_explosion_light_full";
      this.explosionLight.Sprite.color.r = 0.95,
      this.explosionLight.Sprite.color.g = 0.95,
      this.explosionLight.Sprite.color.b = 0.8,
      this.explosionLight.Sprite.depth = -100;
      
      this.timer = 0;

      owner.space.camera.Shake(0.4, 1, 0, 0.4);
    }
  }
  
};

EndHeartLogic.prototype.onDamage = function() {
  var owner = arguments[0];
  var damageEvent = arguments[1];
  var sprite = owner.Sprite;
  
  this.health -= damageEvent.damage;

  var enemyDefeat1 = owner.space.CreateEntity({"name" : "EnemyDefeatSound", "archetype" : "SoundEffect", "position" : {"x" : 0, "y" : 0}});
  enemyDefeat1.SoundEmitter.soundCue = "event:/brain-damage";
  enemyDefeat1.SoundEmitter.startPlaying = true;
  
  if (this.health <= 0) {
    if (!this.dead) {
      this.timer = 0;
      this.dead = true;
    }
    
  }
  
  if (!this.dead) {
    sprite.glowColor.g = 1;
    sprite.glowColor.b = 1;
    sprite.brightness = 1.25;
    this.background.Sprite.brightness = owner.Sprite.brightness;
  }
  
  
};