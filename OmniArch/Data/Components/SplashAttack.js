/******************************************************************************
Filename: SplashAttack.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var SplashAttack = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
  this.timer = 0;
  this.lifetime = 0.85;
  this.attackedPlayer = false;
  // Member Variables
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

SplashAttack.Create = function() {
  // required function; do not remove
  return new SplashAttack(arguments[0]);
};

//////////////////
//  INITIALIZE  //
//////////////////

SplashAttack.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
  Omni.Connect(this, "entity", "onHitStart", owner, this.onHitStart);
  owner.Animator.Set("splash_attack");
};

SplashAttack.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  this.timer += updateEvent.dt;
  if (this.timer > this.lifetime)
    owner.Destroy();
};

SplashAttack.prototype.onHitStart = function() {
  var owner = arguments[0];
  var hitEvent = arguments[1];

  var damageEvent = new DamageEvent(20);

  if (!this.attackedPlayer && hitEvent.myType == HitBoxType.Damage && hitEvent.otherType == HitBoxType.Body && hitEvent.otherEntity.HasComponent("PlayerLogic")) {
    hitEvent.otherEntity.DispatchEvent("damageEvent", damageEvent);
    //hitEvent.otherEntity.DispatchEvent("knockbackEvent", new KnockbackEvent(owner));
    owner.space.camera.Shake(0.1, 0.35, 0, 1);
    this.attackedPlayer = true;
  }

};