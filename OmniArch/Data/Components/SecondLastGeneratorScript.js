/******************************************************************************
Filename: SecondLastGeneratorScript.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var SecondLastGeneratorScript = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

SecondLastGeneratorScript.Create = function() {
  // required function; do not remove
  return new SecondLastGeneratorScript(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

SecondLastGeneratorScript.properties = {
  "objectLinkID": Property(Type.Int, 0),
  "pipe1LinkID": Property(Type.Int, 0),
  "pipe2LinkID": Property(Type.Int, 0),
  "pipe3LinkID": Property(Type.Int, 0)
};

//////////////////
//  INITIALIZE  //
//////////////////

SecondLastGeneratorScript.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "entity", "Death", owner, this.onDeath);
};

SecondLastGeneratorScript.prototype.onDeath = function() {
  owner = arguments[0];

  var object = Omni.FindEntityByLinkID(this.objectLinkID);
  //Console.WriteLine(object.name);
  object.DispatchEvent("EndEncounter", {});
  
  object = Omni.FindEntityByLinkID(this.pipe1LinkID);
  //Console.WriteLine(object.name);
  object.DispatchEvent("endLastEncounter", {});
  
  object = Omni.FindEntityByLinkID(this.pipe2LinkID);
  //Console.WriteLine(object.name);
  object.DispatchEvent("endLastEncounter", {});
  
  object = Omni.FindEntityByLinkID(this.pipe3LinkID);
  //Console.WriteLine(object.name);
  object.DispatchEvent("endLastEncounter", {});

}
