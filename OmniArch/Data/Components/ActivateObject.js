/******************************************************************************
Filename: ActivateObject.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var ActivateObject = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables

};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

ActivateObject.Create = function() {
  // required function; do not remove
  return new ActivateObject(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

ActivateObject.properties = {
  "objectLinkID":     Property(Type.Int, 1)
};

//////////////////
//  INITIALIZE  //
//////////////////

ActivateObject.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "entity", "activate", owner, this.Activate);
  Omni.Connect(this, "entity", "deactivate", owner, this.Activate);
};


ActivateObject.prototype.Activate = function() {
  var owner = arguments[0];

  var object = Omni.FindEntityByLinkID(this.objectLinkID);
  object.DispatchEvent("activate", {});
};
