/******************************************************************************
Filename: Random.lib.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Eduardo Gorinstein

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

var Random = {
  RandomRange : function(min, max) {
    return Math.random() * (max - min) + min;
  },

  RandomRangeInt : function(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }
};
