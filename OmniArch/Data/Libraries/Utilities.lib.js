/******************************************************************************
Filename: Utilities.lib.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

var Utilities = {
  slowTimer: 0,

  CreateNutsAndBolts: function(numNuts, numBolts, position) {
    var main = Omni.FindSpaceByName("main");
    for (var i = 0; i < numNuts; ++i) {
      main.CreateEntity({"name" : "nut", "archetype" : "Nut", "position" : position });
    }
  },

  UpdateSlow: function(owner, updateEvent) {
    if (this.slowTimer > 0) {
      Omni.timeScale = Ease.Easy(Omni.timeScale, 0.1, 0.4);
      this.slowTimer = Math.max(this.slowTimer - updateEvent.dt, 0);

      // // BEGIN TRAILS
      // 
      // var all = owner.space.GetAllEntities();
      // for (var i = 0; i < all.length; ++i) {
      //   var obj = all[i];
      //   if (!obj.OnScreen() || !obj.HasComponent("Sprite") || !obj.HasComponent("RigidBody") || obj.RigidBody.static || !obj.Sprite.visible)
      //     continue;
      // 
      //   var trail = owner.space.CreateEntity({ name: "GhostTrail", archetype: "GhostTrail" });
      //   trail.transform.translation.x = obj.transform.translation.x;
      //   trail.transform.translation.y = obj.transform.translation.y;
      //   trail.transform.scale.x = obj.transform.scale.x;
      //   trail.transform.scale.y = obj.transform.scale.y;
      //   trail.transform.rotation = obj.transform.rotation;
      //   trail.Sprite.image = obj.Sprite.image;
      //   trail.Sprite.flipX = obj.Sprite.flipX;
      //   trail.Sprite.flipY = obj.Sprite.flipY;
      //   trail.Sprite.depth = obj.Sprite.depth + 1;
      //   trail.Sprite.color.r = obj.Sprite.color.r; //(obj.Sprite.color.r + this.ghostTrailColor.r) / 2;
      //   trail.Sprite.color.g = obj.Sprite.color.g; //(obj.Sprite.color.g + this.ghostTrailColor.g) / 2;
      //   trail.Sprite.color.b = obj.Sprite.color.b; //(obj.Sprite.color.b + this.ghostTrailColor.b) / 2;
      //   trail.Sprite.color.a = obj.Sprite.color.a;
      //   trail.glowEnabled = false;
      // }
      // 
      // // END TRAILS
    }
    else
      Omni.timeScale = Ease.Easy(Omni.timeScale, 1, 0.1);
  },

  Slow: function(amount) {
    this.slowTimer = Math.max(this.slowTimer, amount);
  },

  ghostTrailColor: { r: 4, g: 19, b: 27, a: 1 }
};
