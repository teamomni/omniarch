/******************************************************************************
Filename: Constants.lib.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: All team members

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/
var Type = {
  Undefined: 0,
  Bool: 1,
  Unsigned: 2,
  Int: 3,
  Float: 4,
  String_: 5,
  Vec2: 6,
  Vec2List: 7,
  Color: 8,
  Entity: 9
};

var InputMethod = {
  Keyboard: 0,
  Gamepad: 1,
  MindControl: 2
};

var MouseButton = {
  Left: 0,
  Right: 1,
  Middle: 2,
  ScrollUp: 3,
  ScrollDown: 4,
  _Last: 5
};

var MouseState = {
  Up: 0,
  Pressed: 1,
  Down: 2,
  Released: 3,
  ReleasedOut: 4
};

var HitBoxType = {
  Body: 1,
  Damage: 2
};

var Facing = {
  Left: -1,
  Right: 1
};

var Status = {
  Hostile: 0,
  Friendly: 1,
  Neutral: 2
};

var CollisionGroup = {
  Default : 0,
  Player : 1,
  Enemy : 2,
  Evade : 3,
  AvoidPlayer : 4,
  AvoidAllButDefault : 5,
  CollideWithPlayer : 6,
  DetectAllButDefault : 7,
  DetectPlayer : 8,
  GrabedObject : 9
};

var CollisionState = {
  Resolve : 0,
  Ignore : 1,
  SkipResolution : 2
};

var DebugMessageSetting = {
  None: 0,
  All : 1,
  Destruction : 2,
  EventDispatch : 3
};

// you can add to this, but DO NOT CHANGE any of the existing numbers
var GeneralDepth = {
  Parallax : 300,
  EmptyRooms : 120,
  BackgroundEffects : 110,
  Bombpile : 65,
  Throwable : 60,
  ShieldEnemy : 50,
  PatrolDrone : 15,
  BombDrone : 14,
  SlamDrone : 13,
  BallEnemy : 12,
  Bombs : 11,
  Enemy : 10,
  Terrain : 9.8,
  Buttons : 9.6,
  Checkpoints : 9.4,
  Turrets : 9.2,
  Rooms : 9,
  Doors : 8,
  Player : 0,
  Grafiti : -1,
  UI : -200,
  Menu : -300
}

var ConsoleColors = {
  Black : 0,
  DarkBlue : 1,
  DarkGreen : 2,
  DarkCyan : 3,
  DarkRed : 4,
  DarkMagenta : 5,
  DarkYellow : 6,
  DarkWhite : 7,
  Gray : 8,
  Blue : 9,
  Green : 10,
  Cyan : 11,
  Red : 12,
  Magenta : 13,
  Yellow : 14,
  White : 15
}
