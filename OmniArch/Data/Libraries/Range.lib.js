/******************************************************************************
Filename: Range.lib.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

var Range = function () {
  switch (arguments.length) {
    case 2:
      this.base = 0;
      this.min = arguments[0];
      this.max = arguments[1];
      this.baseMin = 0;
      this.baseMax = 0;
      break;
    case 4:
      this.base = Math.random() * (arguments[1] - arguments[0]) + arguments[0];
      this.min = arguments[2];
      this.max = arguments[3];
      this.baseMin = arguments[0];
      this.baseMax = arguments[1];
      break;
    default:
      Console.Warning("Invalid range!");
  }
};

Range.prototype = {
  roll: function() {
    return Math.random() * (this.max - this.min) + this.min + this.base;
  }
};
