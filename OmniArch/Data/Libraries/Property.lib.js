/******************************************************************************
Filename: Property.lib.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

function Property(type, defaultValue) {
  if (!type)
  {
    Console.Error("Someone attempted to define a property with an invalid type!", "Attempted to set it to " + defaultValue + ", but that's about all I got. Find it yourself.");
  }
  return { type: type, defaultValue: defaultValue };
}