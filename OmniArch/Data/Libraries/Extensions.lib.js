/******************************************************************************
Filename: Extensions.lib.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// var x = 12;
// x.clamp(0, 10); // x = 10
Number.prototype.clamp = function(min, max) {
  return Math.min(Math.max(this, min), max);
};
