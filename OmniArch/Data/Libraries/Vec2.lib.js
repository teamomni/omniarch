/******************************************************************************
Filename: Vec2.lib.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Adam Rezich

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// var b = new Vec2(3, 5);                 // (3, 5)
// var c = new Vec2(b);                    // (3, 5)
// var d = new Vec2(3);                    // (0, 0) (bad but handled)
// var e = new Vec2(3, { x: 14, y: 39 });  // why would you do this
// var f = new Vec2({ foo: "bar" }, 83);   // stop it

var Vec2 = function(x, y) {
  /*if (y === undefined) {
    if (x === undefined) {
      this.x = 0;
      this.y = 0;
    }
    else {
      if (x.x !== undefined && x.y !== undefined) {
        this.x = x.x;
        this.y = x.y;
      }
      else {
        // error
        this.x = 0;
        this.y = 0;
      }
    }
  }*/
  this.x = x || 0;
  this.y = y || 0;
};

Vec2._create = function() {
  return new Vec2(arguments[0], arguments[1]);
};

Vec2.prototype = {

  set: function(x, y) {
    this.x = x;
    this.y = y;
    return this;
  },

  copy: function(vec) {
    this.x = vec.x;
    this.y = vec.y;
    return this;
  },

  clone: function(vec) {
    return new Vec2(this.x, this.y);
  },

  add: function(vec1, vec2) {
    if (vec2) {
      this.x = vec1.x + vec2.x;
      this.y = vec1.y + vec2.y;
    }
    else {
      this.x += vec1.x;
      this.y += vec1.y;
    }
    return this;
  },

  sub: function(vec1, vec2) {
    if (vec2) {
      this.x = vec1.x - vec2.x;
      this.y = vec1.y - vec2.y;
    }
    else {
      this.x -= vec1.x;
      this.y -= vec1.y;
    }
    return this;
  },

  mul: function(scalar) {
    this.x *= scalar;
    this.y *= scalar;
    return this;
  },

  div: function(scalar) {
    this.x /= scalar;
    this.y /= scalar;
    return this;
  },

  negate: function() {
    return this.mul(-1);
  },

  dot: function(vec) {
    return this.x * vec.x + this.y * vec.y;
  },

  lengthSquared: function() {
    return this.x * this.x + this.y * this.y;
  },

  length: function() {
    return Math.sqrt(this.lengthSquared());
  },

  normalize: function() {
    return this.div(this.length());
  },

  normalized: function() {
    return this.clone().normalize();
  },

  distanceToSquared: function(vec) {
    var dx = this.x - v.x;
    var dy = this.y - v.y;
    return dx * dx + dy * dy;
  },

  distanceTo: function(vec) {
    return Math.sqrt(this.distanceToSquared(vec));
  },

  setLength: function(length) {
    return this.normalize().mul(length);
  },

  equals: function(vec) {
    return ( ( vec.x === this.x ) && ( vec.y === this.y ) );
  }

};
