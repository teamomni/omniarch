/******************************************************************************
Filename: Inspiration.lib.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// Thank you one and all, you're the best

var Inspiration = {
  messages : [
    function (color) {
      Console.WriteLine("  ________          _____.___.               ._.", color);
      Console.WriteLine(" /  _____/   ____   \\__  |   |  ____   __ __ | |", color);
      Console.WriteLine("/   \\  ___  /  _ \\   /   |   | /  _ \\ |  |  \\| |", color);
      Console.WriteLine("\\    \\_\\  \\(  <_> )  \\____   |(  <_> )|  |  / \\|", color);
      Console.WriteLine(" \\______  / \\____/   / ______| \\____/ |____/  __", color);
      Console.WriteLine("        \\/           \\/                       \\/", color);
    },
    
    function (color) {
      Console.WriteLine("Thanks for working so hard!", color);
    },
    
    function (color) {
      Console.WriteLine("Only a week more!", color);
    },
    
    function (color) {
      Console.WriteLine("Let's make this game!", color);
    },
    
    function (color) {
      Console.WriteLine("Keep going!", color);
    },
    
    function (color) {
      Console.WriteLine("OmniArch best Arch!", color);
    },
    
    function (color) {
      Console.WriteLine("You're the best, around!", color);
    },
    
    function (color) {
      Console.WriteLine("Blueberries!", color);
    },

    function (color) {
      Console.WriteLine("_____.___.           /\\                 _______               ____._.", color);
      Console.WriteLine("\\__  |   | ____  __ _)/______   ____    \\      \\   ____      /_   | |", color);
      Console.WriteLine(" /   |   |/  _ \\|  |  \\_  __ \\_/ __ \\   /   |   \\ /  _ \\      |   | |", color);
      Console.WriteLine(" \\____   (  <_> )  |  /|  | \\/\\  ___/  /    |    (  <_> )     |   |\\|", color);
      Console.WriteLine(" / ______|\\____/|____/ |__|    \\___  > \\____|__  /\\____/ /\\   |___|__", color);
      Console.WriteLine(" \\/                                \\/          \\/        \\/        \\/", color);
    }
  ],

  PrintMessage : function() {
    var color = ConsoleColors.Green;
    var choice = Math.floor((Math.random() * this.messages.length));
    this.messages[choice](color);
    
  }
}; 