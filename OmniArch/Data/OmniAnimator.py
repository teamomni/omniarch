from tkinter import *
import json
import os
import os.path

running = True

class HitBox():
    def __init__(self, x, y, hw, hh, type_, imgWidth, imgHeight, canvas):
        self.x = x
        self.y = y
        self.hw = hw
        self.hh = hh
        self.imgWidth = imgWidth
        self.imgHeight = imgHeight
        self.canvas = canvas
        self.type = type_

        if type_ == 'damage':
            self.shapeID = canvas.create_rectangle([self.x - self.hw, self.y - self.hh, self.x + self.hw, self.y + self.hh], outline="firebrick")
        elif type_ == 'body':
            self.shapeID = canvas.create_rectangle([self.x - self.hw, self.y - self.hh, self.x + self.hw, self.y + self.hh], outline="sea green")

    def onSelect(self) :
        if self.type == 'damage':
            self.canvas.itemconfig(self.shapeID, outline = "tomato")
        elif self.type == 'body':
            self.canvas.itemconfig(self.shapeID, outline = "chartreuse")

    def onDeselect(self):
        if self.type == 'damage':
            self.canvas.itemconfig(self.shapeID, outline = 'firebrick')
        elif self.type == 'body':
            self.canvas.itemconfig(self.shapeID, outline = "sea green")

    def Scale(self, x, y):
        self.hw += x
        self.hh += y
        self.canvas.coords(self.shapeID, self.x - self.hw, self.y - self.hh, self.x + self.hw, self.y + self.hh)
        self.canvas.pack()
                                
    def Move(self, x, y):
        self.x += x
        self.y += y
        self.canvas.coords(self.shapeID, self.x - self.hw, self.y - self.hh, self.x + self.hw, self.y + self.hh)
        self.canvas.pack()

    def Serialize(self):
        xOffset = -self.imgWidth / 2
        yOffset = -self.imgHeight / 2
        
        positionX = (self.x + xOffset) / self.imgWidth
        positionY = -(self.y + yOffset) / self.imgHeight
        return {'center' : {'x' : positionX, 'y' : positionY}, 'scale' : {'x' : 2 * self.hw / self.imgWidth, 'y' : 2 * self.hh / self.imgHeight}, 'type' : self.type}

    def Destroy(self):
        self.canvas.delete(self.shapeID)

class MainFrame(Frame):
    def __init__(self, parent, textureName):
        Frame.__init__(self, parent, background="white")   
         
        self.parent = parent
        self.imgScale = 0
        self.selectedHitBox = 0
        self.hitBoxes = 0
        self.textureName = textureName
        self.mouseX = 0
        self.mouseY = 0
        self.currentWidth = 0
        self.currentHeight = 0
        self.scaledHeight = 512
        self.currentImgID = 0
        self.textureChooserText = 0

        # movement / scaling amounts
        self.moveAmount = 3
        self.scaleAmount = 1

        # movement / scaling bools
        self.moveUp = False
        self.moveDown = False
        self.moveLeft = False
        self.moveRight = False

        self.scaleUp = False
        self.scaleDown = False
        self.scaleLeft = False
        self.scaleRight = False

        self.moveUpFine = False
        self.moveDownFine = False
        self.moveLeftFine = False
        self.moveRightFine = False
        
        self.initUI()
        self.PrintReadyMessage()

    def Update(self):
        # if there is a currently selected hitbox
        if self.selectedHitBox != 0:
            # handle movement / scaling
            if self.moveUp:
                self.selectedHitBox.Move(0, -self.moveAmount)
            if self.moveDown:
                self.selectedHitBox.Move(0, self.moveAmount)
            if self.moveRight:
                self.selectedHitBox.Move(self.moveAmount, 0)
            if self.moveLeft:
                self.selectedHitBox.Move(-self.moveAmount, 0)

            #handle fine tuning
            if self.moveUpFine:
                self.selectedHitBox.Move(0, -1)
                self.moveUpFine = False
            if self.moveDownFine:
                self.selectedHitBox.Move(0, 1)
                self.moveDownFine = False
            if self.moveRightFine:
                self.selectedHitBox.Move(1, 0)
                self.moveRightFine = False
            if self.moveLeftFine:
                self.selectedHitBox.Move(-1, 0)
                self.moveLeftFine = False


            if self.scaleUp:
                self.selectedHitBox.Scale(0, self.scaleAmount)
            if self.scaleDown:
                self.selectedHitBox.Scale(0, -self.scaleAmount)
            if self.scaleRight:
                self.selectedHitBox.Scale(self.scaleAmount, 0)
            if self.scaleLeft:
                self.selectedHitBox.Scale(-self.scaleAmount, 0)
                

        # calls update after 16 milliseconds
        self.parent.after(16, self.Update)

    
    def PrintReadyMessage(self):
        print("Ready!\n")
        print("Click on a hitbox to select it. Right click to deselect.")
        print("Arrow keys to move boxes around.")
        print("Ctrl + Arrow keys to scale.\n")

    def InitializeNewTexture(self) :
        if self.textureChooserText.get() == "Choose a file" :
            print("Whoops! Please select a texture!")

        else:
            self.ClearHitBoxes()
            
            self.textureName = self.textureChooserText.get()
        
            if self.currentImgID != 0:
                print("Clearing Last Texture...")
                self.canvas.delete(self.currentImgID)
            
            print('Loading Texture...')
            
            imgRaw = PhotoImage(file = self.textureName)
            
            print('Scaling Texture...')
        
            self.imageScale = 512 / imgRaw.height()

            # if we need to scale up, use zoom
            if self.imageScale >= 1:
                self.img = imgRaw.zoom(int(self.imageScale))

            # if we need to scale down, use subsample
            if self.imageScale < 1:
                self.img = imgRaw.subsample(int(1 / self.imageScale))

            self.currentImgID = self.canvas.create_image((0, 0), anchor = NW, image = self.img)
            
            self.currentWidth = self.img.width()
            self.currentHeight = self.img.height()

    def SelectNewBox(self, newHitBox):
        # deselect the old hitbox, if there is one selected
        if self.selectedHitBox != 0:
            self.selectedHitBox.onDeselect()
            
        self.selectedHitBox = newHitBox
        self.selectedHitBox.onSelect()
    
    def CreateRedHitBox(self):
        newBox = HitBox(256, 256, 100, 100, 'damage', self.currentWidth, self.currentHeight, self.canvas)
        self.SelectNewBox(newBox)
        
        if self.hitBoxes == 0:
            self.hitBoxes = [self.selectedHitBox]
            
        else:
            self.hitBoxes.append(self.selectedHitBox)

    def CreateGreenHitBox(self):
        newBox = HitBox(256, 256, 100, 100, 'body', self.currentWidth, self.currentHeight, self.canvas)
        self.SelectNewBox(newBox)
        
        if self.hitBoxes == 0:
            self.hitBoxes = [self.selectedHitBox]
            
        else:
            self.hitBoxes.append(self.selectedHitBox)

    def MouseMovement(self, event):
        self.mouseX = event.x
        self.mouseY = event.y

    def Serialize(self):
        self.serialized = []
        for box in self.hitBoxes:
            self.serialized.append(box.Serialize())

        outputName = self.textureName + "_hitboxFrames.json"
        file = open(outputName, mode = 'w')
        file.write(json.dumps({'hitboxes' : self.serialized}, sort_keys=True, indent=2, separators=(',', ': ')))
        file.close()

        print("Serialized to", outputName)

    def DeleteHitBox(self):
        if self.selectedHitBox != 0:
            self.canvas.delete(self.selectedHitBox.shapeID)
            self.hitBoxes.remove(self.selectedHitBox)

    def ReadInHitBoxData(self):

        # if there isn't a texture currently loaded, print error message and return
        if self.currentImgID == 0:
            print("Oh no! You must load a texture before loading hitbox data!")
            return

        # gotta choose a file name
        if self.textureChooserText.get() == "Choose a file" :
            print("Whoops! Please select a json file!")
        
        infile = open(self.textureChooserText.get(), mode = 'r');
        jsObject = json.load(infile)

        for hitbox in jsObject['hitboxes']:
            scaleX = hitbox['scale']['x']
            scaleY =  hitbox['scale']['y']
            centerX = hitbox['center']['x']
            centerY = hitbox['center']['y']
            type_ = hitbox['type']

            if type_ == 'body':
                self.CreateGreenHitBox()
            elif type_ == 'damage':
                self.CreateRedHitBox()
                
            self.selectedHitBox.hw = self.currentWidth * scaleX / 2
            self.selectedHitBox.hh = self.currentHeight * scaleY / 2

            self.selectedHitBox.x = self.currentWidth * centerX + (self.currentWidth / 2)
            self.selectedHitBox.y = (self.currentHeight / 2) - self.currentHeight * centerY

            self.selectedHitBox.Move(0,0)

        infile.close();

    def ClearHitBoxes(self):
        if self.hitBoxes == 0:
            return
        
        for hitBox in self.hitBoxes:
            hitBox.Destroy()

        self.hitBoxes.clear()
        self.hitBoxes = 0
        
    def initUI(self):

        self.canvas = Canvas(self)
      
        self.parent.title("Omni Animator")        
        self.pack(fill=BOTH, expand=1)

        self.redHitBoxButton = Button(root, text = "Create Damage Hit Box", command = self.CreateRedHitBox)
        self.redHitBoxButton.pack(side = RIGHT)

        self.greenHitBoxButton = Button(root, text = "Create Body Hit Box", command = self.CreateGreenHitBox)
        self.greenHitBoxButton.pack(side = RIGHT)

        self.DeleteHitBoxButton = Button(root, text = "Delete Current Hit Box", command = self.DeleteHitBox)
        self.DeleteHitBoxButton.pack(side = RIGHT)

        self.serializeButton = Button(root, text = "Serialize", command = self.Serialize)
        self.serializeButton.pack(side = RIGHT)

        self.ClearHitBoxButton = Button(root, text = "Clear All Hit Boxes", command = self.ClearHitBoxes)
        self.ClearHitBoxButton.pack(side = RIGHT)

        self.readInHitBoxesButton = Button(root, text = "Read in HitBox Data", command = self.ReadInHitBoxData)
        self.readInHitBoxesButton.pack(side = RIGHT)

        self.textureChooserText = StringVar(self.parent)
        self.textureChooserText.set("Choose a file")

        myPath = os.path.dirname(os.path.abspath(__file__))
        onlyfiles = [ f for f in os.listdir(myPath) if os.path.isfile(os.path.join(myPath,f)) ]

        self.textureChooser = OptionMenu(self.parent, self.textureChooserText, *onlyfiles)
        self.textureChooser.pack()

        self.initializeNewTextureButton = Button(root, text = "Initialize New Texture", command = self.InitializeNewTexture)
        self.initializeNewTextureButton.pack(side = RIGHT)
        
        self.canvas.pack(fill=BOTH, expand=1)
        
    def upPressed(self, event):
        self.moveUp = True
        
    def upReleased(self, event):
        self.moveUp = False
        self.scaleUp = False

    def downPressed(self, event):
        self.moveDown = True
        
    def downReleased(self, event):
        self.moveDown = False
        self.scaleDown = False

    def leftPressed(self, event):
        self.moveLeft = True
        
    def leftReleased(self, event):
        self.moveLeft = False
        self.scaleLeft = False

    def rightPressed(self, event):
        self.moveRight = True
        
    def rightReleased(self, event):
        self.moveRight = False
        self.scaleRight = False

    def upPressed_shift(self, event):
        self.scaleUp = True
        
    def upReleased_shift(self, event):
        self.moveUp = False
        self.scaleUp = False

    def downPressed_shift(self, event):
        self.scaleDown = True
        
    def downReleased_shift(self, event):
        self.moveDown = False
        self.scaleDown = False

    def leftPressed_shift(self, event):
        self.scaleLeft = True
        
    def leftReleased_shift(self, event):
        self.moveLeft = False
        self.scaleLeft = False

    def rightPressed_shift(self, event):
        self.scaleRight = True
        
    def rightReleased_shift(self, event):
        self.moveRight = False
        self.scaleRight = False

    def leftPressed_FineTuning(self, event):
        self.moveLeftFine = True
        
    def rightPressed_FineTuning(self, event):
        self.moveRightFine = True
        
    def upPressed_FineTuning(self, event):
        self.moveUpFine = True
        
    def downPressed_FineTuning(self, event):
        self.moveDownFine = True
        

    # right click deselects hitboxes
    def RightClick(self, event):
        if self.selectedHitBox != 0:
                self.selectedHitBox.onDeselect()
                self.selectedHitBox = 0

    def LeftClick(self, event):
        if self.hitBoxes != 0:
            for box in self.hitBoxes:
                # selects the first hitbox that the mouse collides with
                if self.mouseX > box.x - box.hw and self.mouseX < box.x + box.hw and self.mouseY > box.y- box.hh and self.mouseY < box.y + box.hh:
                    self.SelectNewBox(box)
                    return

fileName = 'test.png'
root = Tk()
print("Initializing...")
root.geometry("1000x700")
print("Creating MainFrame...")
app = MainFrame(root, fileName)

# keyboard events
root.bind("<Up>", app.upPressed)
root.bind("<KeyRelease-Up>", app.upReleased)
root.bind("<Down>", app.downPressed)
root.bind("<KeyRelease-Down>", app.downReleased)
root.bind("<Left>", app.leftPressed)
root.bind("<KeyRelease-Left>", app.leftReleased)
root.bind("<Right>", app.rightPressed)
root.bind("<KeyRelease-Right>", app.rightReleased)

# fine tuning
root.bind("<Shift-Up>", app.upPressed_FineTuning)
root.bind("<Shift-Down>", app.downPressed_FineTuning)
root.bind("<Shift-Left>", app.leftPressed_FineTuning)
root.bind("<Shift-Right>", app.rightPressed_FineTuning)

root.bind("<Control-Up>", app.upPressed_shift)
root.bind("<Control-KeyRelease-Up>", app.upReleased_shift)
root.bind("<Control-Down>", app.downPressed_shift)
root.bind("<Control-KeyRelease-Down>", app.downReleased_shift)
root.bind("<Control-Left>", app.leftPressed_shift)
root.bind("<Control-KeyRelease-Left>", app.leftReleased_shift)
root.bind("<Control-Right>", app.rightPressed_shift)
root.bind("<Control-KeyRelease-Right>", app.rightReleased_shift)

# mouse events
root.bind("<Button-1>", app.LeftClick)
root.bind("<Button-3>", app.RightClick)
root.bind('<Motion>', app.MouseMovement)

# update event
root.after(16, app.Update)

# main loop
root.mainloop()


