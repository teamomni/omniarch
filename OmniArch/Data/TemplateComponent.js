///////////////////
//  CONSTRUCTOR  //
///////////////////

var ComponentName = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // Member Variables

};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

ComponentName.Create = function() {
  // required function; do not remove
  return new ComponentName(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

ComponentName.properties = {
  
};

//////////////////
//  INITIALIZE  //
//////////////////

ComponentName.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
ComponentName.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];
  
  

};
