/******************************************************************************
Filename: OmniArch.cpp

Project Name: OmniArch

Author: Adam Rezich

All content � 2014 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

// OmniArch.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include "Game.h"
#include "resource.h"

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
  _In_opt_ HINSTANCE hPrevInstance,
  _In_ LPTSTR    lpCmdLine,
  _In_ int       nCmdShow)
{
  UNREFERENCED_PARAMETER(hPrevInstance);
  UNREFERENCED_PARAMETER(lpCmdLine);
#if defined(_EDITOR)
  Omni::Game::Instance().Start(hInstance, IDI_ICON1, Omni::Game::GetMyDocumentsDirectory().append("\\Data\\").c_str(), false);
#else
#if !defined(_EDITOR) && (defined(_DEBUG) || defined(_RELEASE))
  Omni::Game::Instance().Start(hInstance, IDI_ICON1, "\\..\\..\\..\\OmniArch\\Data\\", false);
#else
  Omni::Game::Instance().Start(hInstance, IDI_ICON1, "\\Data\\", true);
#endif
#endif

  return 0;
}
