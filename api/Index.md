OmniEngine API docs
===================

This is the documentation for OmniEngine's JavaScript (ECMAScript if you're being a snob) Application Programming Interface (API).

Objects
-------

This is the list of all objects exposed to JavaScript.

Anything tagged [TODO] has yet to be exposed to JavaScript, and anything tagged [FUTURE] hasn't even been implemented in C++.


### Global objects

* [Omni](Objects/Omni.md)
* [Console](Objects/Console.md)
* [FX](Objects/FX.md)


### Entity objects

* [Entity](Objects/Entity.md) (owner)
* [Space](Objects/Space.md)


### System objects

* [Sprite](Objects/Sprite.md)
* [RigidBody](Objects/RigidBody.md)
* [Animator](Objects/Animator.md)
* [SoundEmitter](Objects/SoundEmitter.md)


### Other objects

* [Vec2](Objects/Vec2.md)
* [Type](Objects/Type.md)
