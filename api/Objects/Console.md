Console
=======

**The "Console" object is global and its members can be accessed from anywhere.**

    var Console = {
      Write: function(text) {},
      WriteLine: function(text) {},
      Info: function(header, body) {},
      Warning: function(header, body) {},
      Error: function(header, body) {}
    };


Write(text)
-----------

Output a text to the console.


WriteLine(text)
---------------

Output a line of text to the console. Appends a newline to the end.


Info(header, body)
------------------

Output an "info" message to the console, either just a single line of `header`, or a `header` with a `body`. **This should be used for notifications of things that aren't necessarily bad, but nonetheless require a notification in the console.**


Warning(header, body)
---------------------

Output a "warning" message to the console, either just a single line of `header`, or a `header` with a `body`. **This should be used for notifications of things that aren't necessarily going to cause the game to explode, but nonetheless require a debugger's attention.**


Error(header, body)
-------------------

Output an "error" message to the console, either just a single line of `header`, or a `header` with a `body`. **This should be used for notifications of things that are super bad and really should be paid attention to. However, this is mostly a thing for the engine to use (JavaScript errors, asset loading errors, etc.).**
