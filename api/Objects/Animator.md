Animator
========

    var Animator = {
      Set: function(animation) {},
      Queue: function(animation) {},
      Clear: function() {},
      IsDone: function() {},
      Count: function() {},
      points: [
        {                             // example points,
          x: 0,                       // empty by default
          y: 0
        },
        {
          x: 0.5,
          y: 1
        }
      ],
      flags: [
        "foo",                        // example flags,
        "bar"                         // empty by default
      ]
    };


Set(animation)
--------------

Clears the animation queue and sets the animation immediately.


Queue(animation)
----------------

Appends an animation to the animation queue.


Clear()
-------

Clears the animation queue. This is rarely necessary.


IsDone()
--------

Returns `true` if the current animation has finished its last frame. Never true if the current animation is looping.


Count()
-------

Returns the number of animations currently in the animation queue.


points
------

Array of arbitrary points for the current frame of animation. Access points using indices; for example, the first arbitrary point can be accessed with `points[0]`.


flags
-----

Array of arbitrary flags for the current frame of animation. Access flags using either indices or point notation. For example, access the "skippable" flag using either `flags["skippable"]` or `flags.skippable`.
