Entity
-----

In Events, you have access to `this`, which is "this Component", but you also have access to `owner`, which is "the Entity that 'owns' this Component."

    var Entity = {
      DispatchEvent: function(name, data) {},
      Space: {},
      Sprite: {},
      RigidBody: {},
      Animator: {},
      SoundEmitter: {},
      transform: {
        translation: {
          x: 0,
          y: 0
        },
        scale: {
          x: 1,
          y: 1
        },
        rotation: 0
      }
    };


DispatchEvent(name, data) [TODO] [FUTURE]
-------------------------


Space
-----

Gets the [Space](Space.md) object of the owner.


Sprite
------

Gets the [Sprite](Sprite.md) object of the owner (if there is one).


RigidBody
---------

Gets the [RigidBody](RigidBody.md) object of the owner (if there is one).


Animator
--------

Gets the [Animator](Animator.md) object of the owner (if there is one).


SoundEmitter
------------

Gets the [SoundEmitter](SoundEmitter.md) object of the owner (if there is one).


transform
---------

Gives the Transform of the  Contains the following properties:

|         Property  |  Description                                  |
|------------------:|-----------------------------------------------|
|  **translation**  |  (Vec2) Position relative to center of world  |
|  **scale**        |  (Vec2) Scaling multiplier on each axis       |
|  **rotation**     |  (number) Angle of orientation, in radians    |
