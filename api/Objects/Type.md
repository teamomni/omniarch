Type
====

    var Type = {
      Undefined: 0,
      Bool: 1,
      Unsigned: 2,
      Int: 3,
      Float: 4,
      String_: 5,
      Vec2: 6,
      Color: 7
    };


Remarks
-------

"String_" is as such because "String" is the JavaScript string library.
