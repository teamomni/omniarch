Omni
====

**The "Omni" object is global and its members can be accessed from anywhere.**

    var Omni = {
      Connect: function(listener, name, func) {},
      CheckInput: function(input) {},
      GetEntityByID: function(id) {},
      ChangeLevel : function(level) {},
      ReloadLevel: function() {},
      PauseAllSpacesExcept: function(space) {},
      UnpauseAllSpaces: function() {}
    };


Connect(listener, name, func) [TODO] [FUTURE]
-----------------------------


CheckInput(input)
-----------------

Returns true/false whether or not the given input action is currently occurring. Input actions are strings, and the only valid input actions are listed below.

### Menu

* "menu_up"
* "menu_down"
* "menu_left"
* "menu_right"
* "menu_confirm"
* "menu_cancel"

### Gameplay

* "jump"
* "jump_hold"
* "attack_hold"
* "move_left"
* "move_right"


GetEntityByID(id) [TODO] [FUTURE]
-----------------

Get the [Entity](Entity.md) with the given unique ID, if it exists. Returns `null` if it doesn't exist.


GetSpaceByName(name) [TODO]
--------------------

Get the [Space](Space.md) with the given name, if it exists. Returns `null` if it doesn't exist.


ChangeLevel(level) [TODO] [FUTURE]
------------------


ReloadLevel() [TODO] [FUTURE]
-------------


PauseAllSpacesExcept(space) [TODO] [FUTURE]
---------------------------


UnpauseAllSpaces() [TODO] [FUTURE]
------------------
