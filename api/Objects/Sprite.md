Sprite
======

    var Sprite = {
      image: "",
      glowImage: "",
      color: {
        r: 0,
        g: 0,
        b: 0,
        a: 0
      },
      glowColor: {
        r: 0,
        g: 0,
        b: 0,
        a: 0
      },
      glowAmount: 0,
      flip: {
        x: false,
        y: false
      },
      visible: true,
      depth: 0
    };


image [TODO]
-----

Sets the current image of the sprite to the provided image string. Note that if this entity has an Animator, it will probably be overriding this, constantly.


glowImage [TODO] [FUTURE]
---------


color [TODO]
-----


glowColor [TODO] [FUTURE]
---------


glowAmount [TODO] [FUTURE]
----------


flip [TODO]
----

Flips this sprite's image about the x- and/or y-axis.


visible [TODO]
-------

Sets whether or not this sprite should be drawn to the screen.


depth [TODO]
-----

Determines how far "back into the screen" the image of this sprite should be drawn. **Sprites with lower depths will be drawn on top of those with higher depths.**
