RigidBody
=========

    var RigidBody = {
      ApplyLinearImpulse: function(vec) {},
      ApplyLinearForce: function(vec) {},
      IsOnGround: function() {},
      gravityScale: 1,
      ghost: false,
      fixed: false,
      rotationLocked: false,
      velocity: {
        x: 0,
        y: 0
      },
      angularVelocity: 0
    };


ApplyLinearImpulse(vec) [TODO]
-----------------------


ApplyLinearImpulse(x, y)
------------------------

Applies a linear impulse to the rigid body, either with a Vec2 or with separate x and y components.


ApplyLinearForce(vec) [TODO]
---------------------


ApplyLinearForce(x, y)
----------------------

Applies a linear force to the rigid body, either with a Vec2 or with separate x and y components.


IsOnGround()
------------

Returns `true` if the object has a solid object beneath it from a side-scrolling perspective.


gravityScale [TODO]
------------

Adjusts how much gravity affects this rigid body.


ghost [TODO]
-----


fixed [TODO]
-----


rotationLocked [TODO]
--------------

If set to true, physics will not affect the rotation of this entity.


velocity
--------

Gets or sets the raw velocity of the rigid body.


angularVelocity [TODO]
---------------

Gets or sets the raw velocity of the rigid body.
