Camera
======

    var Camera = {
      Shake: function(magnitude, duration, zoom, rotation) {},
      Punch: function(vec) {},
      Follow: function(entity) {},
      StopFollowing: function() {},
      Zoom: function(scale, duration) {},
      Pan: function(vec, duration) {},
      translation: {
        x: 0,
        y: 0
      },
      rotation: 0,
      size: 10
    };


Shake(magnitude, duration, zoom, rotation) [TODO]
------------------------------------------


Punch(vec) [TODO] [FUTURE]
----------


Punch(angle, magnitude) [TODO] [FUTURE]
-----------------------


Follow(entity) [TODO] [FUTURE]
--------------


StopFollowing() [TODO] [FUTURE]
---------------


Zoom(scale, duration) [TODO] [FUTURE]
---------------------


Pan(vec, duration) [TODO] [FUTURE]
------------------


translation [TODO]
-----------


rotation [TODO]
--------


size [TODO]
----
