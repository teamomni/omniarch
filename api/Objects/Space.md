Space
=====

    var Space = {
      DispatchEvent: function(name, data) {},
      GetEntityByName: function(name) {},
      GetEntitiesByName: function(name) {},
      GetEntitiesByArchetype: function(archetype) {},
      CreateEntity: function(data) {},
      name: "",
      paused: false,
      timeScale: 1
    };


DispatchEvent(name, data) [TODO] [FUTURE]
-------------------------


GetEntityByName(name) [TODO] [FUTURE]
---------------------

Gets the [Entity](Entity.md) with the given name in the space, if it exists. If multiple Entities with this name exist in this space, it returns just the first one. Returns `null` if no entities with the given name exist.


GetEntitiesByName(name) [TODO] [FUTURE]
-----------------------

Returns an array of all [Entities](Entity.md) with the given name in the space, if any exist. Returns an empty array (`[]`) if none exist.


GetEntitiesByArchetype(archetype) [TODO] [FUTURE]
---------------------------------

Returns an array of all [Entities](Entity.md) with the given Archetype name, if any exist. Returns an empty array (`[]`) if none exist.


CreateEntity(data)
------------------

Creates an Entity in the space based on the properties of the `data` object passed in:

|  Property  |  Description  |
|-----------:|---------------|
|  **name**  |  Name of the entity. If not provided, the entity will be unnamed  |
|  **x**  |  x-coordinate of the entity. If not provided, it will be 0  |
|  **y**  |  y-coordinate of the entity. If not provided, it will be 0  |
|  **archetype**  |  Name of the archetype. If not provided, no archetype will be used (obviously)  |


name
----

Gets the name of the space.


paused
------

Sets whether or not the space is "paused". If it is paused, it will not onLogicUpdate, but it *will* onFrameUpdate.


timeScale
---------
