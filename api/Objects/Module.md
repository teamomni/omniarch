Module
======

    var Module = {
      AddProperty: function(name)
    };


AddProperty(name, type)
-----------------------

Makes the given property of the module an accessible-by-C++ "Property". By doing so, you're basically promising C++ that you won't ever change the type to anything other than the provided type. The type is really a number, but you should use one of the [Type variables](Type.md).
