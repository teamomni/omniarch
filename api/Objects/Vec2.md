Vec2
====

**"Vec2" is an object that is basically prototyped, and can be instantiated (`new Vec2(x, y)`). Some other methods return Vec2s.**

    var Vec2 = {
      Set: function(x, y) {},
      Normalize: function() {},
      Normalized: function() {},
      Length: function() {},
      LengthSquared: function() {},
      GetAngle: function() {},
      Cross: function(vec) {},
      Midpoint: function(vec) {},
      Add: function(vec) {},
      Sub: function(vec) {},
      Mul: function(scalar) {},
      Div: function(scalar) {},
      x: 0,
      y: 0
    };


Set(x, y) [TODO]
---------


Normalize() [TODO]
-----------


Normalized() [TODO]
------------


Length() [TODO]
--------


LengthSquared() [TODO]
---------------


GetAngle() [TODO]
----------


Cross(vec) [TODO]
----------


Midpoint(vec) [TODO]
-------------


Add(vec) [TODO]
--------


Sub(vec) [TODO]
--------


Mul(vec) [TODO]
--------


Mul(scalar) [TODO]
-----------


Div(scalar) [TODO]
-----------
