OmniEngine API docs - JavaScript library functions
==================================================

Math
----

* Math.E
* Math.LN10
* Math.LN2
* Math.LOG10E
* Math.LOG2E
* Math.PI
* Math.SQRT1_2
* Math.SQRT2

* **Math.abs()**
* Math.acos()
* Math.acosh()
* Math.asin()
* Math.asinh()
* Math.atan()
* **Math.atan2()**
* Math.atanh()
* Math.cbrt()
* **Math.ceil()**
* Math.clz32()
* **Math.cos()**
* Math.cosh()
* Math.exp()
* **Math.floor()**
* Math.expm1()
* Math.fround()
* Math.imul()
* Math.hypot()
* **Math.log()**
* Math.log10()
* Math.log1p()
* Math.log2()
* **Math.max()**
* **Math.min()**
* **Math.pow()**
* **Math.random()** - Returns random number between 0 and 1. May return 0, will never return 1.
* **Math.round()**
* **Math.sin()**
* **Math.sign()**
* Math.sinh()
* **Math.sqrt()**
* Math.tanh()
* **Math.tan()**
* Math.trunc() - truncate any fractional digits
