###############################################################################

# Filename: commentstuff.py

# Project Name: OmniArch

# Author: Tai Der Hui

# Purpose: Adds the file header to all .cpp and .h and .js and .hlsl files in a folder

# All content © 2015 DigiPen (USA) Corporation, all rights reserved.

###############################################################################

import os, sys

adamFiles = []
edFiles = []
juliFiles = []
dhFiles = []
mickeyFiles = []
authors = ["Adam Rezich", "Eduardo Gorinstein", "Juli Gregg", "Tai Der Hui", "Michael Tyler"]

here = os.path.abspath(os.path.dirname(sys.argv[0]))
filesToCheck = []

for base, dirs, files in os.walk(here):

    # run through every filename in the directory
    for fileName in files:
        # get the file extension
        ext = (fileName[fileName.rindex('.'):]).lower()

        # check if the extension is one of the listed extensions
        if ext == '.cpp' or ext == '.h' or ext == '.js' or ext == '.hlsl':
            #noComment should be true by default
            noComment = True
            foundComment = False
            
            # open file and read in all lines
            fileNameAndPath = (base + '/' + fileName)
            file = open(fileNameAndPath)
            content = file.readlines()
            
            # if there's already a comment, prompt to skip
            numLinesOfComments = 0
            if len(content[0]) >= 2:
                if content[0][0] == '/' and content[0][1] == '*':
                    foundComment = False
                    noComment = False
                    currentFileLines = []
                    for line in content:
                        numLinesOfComments += 1
                        currentFileLines.append(line)
                        lineLength = len(line)
                        if line[lineLength - 2] == '/' and line[lineLength - 3] == '*':
                            foundComment = True
                            break
                    if foundComment == False :
                        print("ERROR: Could not find comment for " + fileName)
                        print("Please check error.txt")
                        filesToCheck.append(fileName)
                        continue
                    else :
                        print("Comment for this file is : ")
                        for currLine in currentFileLines:
                            try:
                                print(currLine)
                            except:
                                print("-------LINE COULD NOT BE PRINTED (probably the CopyRight symbol)------")
                
            # if we have comments, prompt to skip
            if foundComment == True:
                toReplaceComment = input("Do you want to replace this commment? y/n")

            if noComment == True or toReplaceComment == 'y' or toReplaceComment == 'Y':
            
                # get author
                print(fileName)
                author = input("Who is Author? 0 = Adam, 1 = Ed, 2 = Juli, 3 = DH, 4 = Mickey: ")
                if int(author) == 0 :
                    adamFiles.append(fileName)
                if int(author) == 1 :
                    edFiles.append(fileName)
                if int(author) == 2 :
                    juliFiles.append(fileName)
                if int(author) == 3 :
                    dhFiles.append(fileName)
                if int(author) == 4 :
                    mickeyFiles.append(fileName)
                

                # make new array with the new header
                firstLine = "/******************************************************************************\n"
                fileLine = "Filename: " + fileName + "\n\n"
                projectLine = "Project Name: OmniArch\n\n"
                teamLine = "Team Name : Casual Baby Ducks\n\n"
                authorLine = "Author: " + authors[int(author)] + "\n\n"
                copyrightLine = "All content © 2015 DigiPen (USA) Corporation, all rights reserved.\n\n"
                lastLine = "******************************************************************************/\n\n"
                newContent = [firstLine, fileLine, projectLine, teamLine, authorLine, copyrightLine, lastLine]

                # remove comments
                while numLinesOfComments >= 0:
                    content.pop(0)
                    numLinesOfComments -= 1

                # now add the content
                newContent.extend(content)

                # close the file for reading
                file.close()

                # open the file for writing and write the new stuff
                file = open(fileNameAndPath, 'wt')
                for newLine in newContent:
                    file.write(newLine)

            # close the file for writing
            file.close()

# write all the errors to a file
if len(filesToCheck) > 0:
    file = open("errors.txt", 'wt')
    for name in filesToCheck:
        file.write(name)
    file.close()
    print("Please check error.txt to find the files with errors!")

# print to screen the total number of files each person wrote
print("Adam: " + str(len(adamFiles)) + '\n')
print("Ed: " + str(len(edFiles)) + '\n')
print("Juli: " + str(len(juliFiles)) + '\n')
print("DH: " + str(len(dhFiles)) + '\n')
print("Mickey: " + str(len(mickeyFiles)) + '\n')

# write the filenames to different files
file = open("adam.txt", 'wt')
for item in adamFiles:
    file.write(item)
file.close()

file = open("ed.txt", 'wt')
for item in edFiles:
    file.write(item)
file.close()

file = open("juli.txt", 'wt')
for item in juliFiles:
    file.write(item)
file.close()

file = open("dh.txt", 'wt')
for item in dhFiles:
    file.write(item)
file.close()

file = open("mickey.txt", 'wt')
for item in dhFiles:
    file.write(item)
file.close()
