/******************************************************************************
Filename: Example.component.js

Project Name: OmniArch

Team Name : Casual Baby Ducks

Author: Tai Der Hui

All content � 2015 DigiPen (USA) Corporation, all rights reserved.

******************************************************************************/

//  CONSTRUCTOR  //
///////////////////

var ComponentName = function(owner_id) {
  this.owner_id = owner_id; // boilerplate, do not remove

  // initialize member variables here
  this.memberVariable = "value";

  // run any other code that should be executed when this Component
  // is created here (note that this is executed right when the
  // Component is created, but BEFORE it has been attached to its
  // owner Entity
};

///////////////////////
//  CREATE FUNCTION  //
///////////////////////

ComponentName.Create = function() {
  // required function; do not remove
  return new ComponentName(arguments[0]);
};

//////////////////
//  PROPERTIES  //
//////////////////

ComponentName.properties = {
  "memberVariable": Type.String
};

//////////////////
//  INITIALIZE  //
//////////////////

ComponentName.prototype.Initialize = function() {
  var owner = arguments[0];
  var component = arguments[1];

  // this is where you connect events
  Omni.Connect(this, "space", "onLogicUpdate", owner, 0, this.onUpdate);
};

//---------------------------//
//  example helper function  //
//---------------------------//

ComponentName.prototype.SomeHelperFunction = function(owner) {
  // you can access the owner object exactly like you'd expect
  return 108; // you can return whatever you'd want
};

//-----------------------------------//
//  example helper function "array"  //
//-----------------------------------//

ComponentName.prototype.functionsThatDoThings = {};

ComponentName.prototype.functionsThatDoThings.doThingOne = function(owner, dt) {
  // in here you'd then be able to access owner and dt
};

ComponentName.prototype.functionsThatDoThings.doThingTwo = function(owner, dt) {
  // in here you'd then be able to access owner and dt
};

///////////////////////
//  EVENT: OnUpdate  //
///////////////////////
ComponentName.prototype.onUpdate = function() {
  var owner = arguments[0];
  var updateEvent = arguments[1];

  var body = owner.RigidBody;
  var sprite = owner.Sprite;

  if (this.StateCount() > 0)
    this.CurrentState().call(this, owner, updateEvent.dt);

  owner.space.camera.transform.translation.x = owner.transform.translation.x;
  owner.space.camera.transform.translation.y = owner.transform.translation.y;
};
