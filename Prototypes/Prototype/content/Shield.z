class Shield : ZilchComponent
{
    [Property]
    var Integrity : Integer = 50;
    [Property]
    var RechargeTime : Real = 5.0;
    var Timer : Real = 0.0;
    
    [Property]
    var ShieldColor : Real4 = Real4(1,1,1,1);
    var DefaultColor : Real4 = Real4(1,1,1,1);
    
    var Active : Boolean = true;
    
    var CurrentIntegrity : Integer = 50;
    
    var Flashing : Boolean = false;
    var FlashTimer : Real = 0.0;
    
    var BlinkTimer : Real = 0.0;
    
    function Initialize(init : CogInitializer)
    {
        Zero.Connect(this.Space, Events.LogicUpdate, this.OnLogicUpdate);
        Zero.Connect(this.Owner, "TakeHit", this.TakeHit);
        this.CurrentIntegrity = this.Integrity;
        
        this.DefaultColor = this.Owner.Sprite.Color;
    }

    function OnLogicUpdate(event : UpdateEvent)
    {
        // Update the recharge timer
        if(this.Active == false)
        {
            this.UpdateTimer(event.Dt);
            return;
        }
        
        // Continue flashing
        if(this.Flashing)
        {
            this.UpdateFlash(event.Dt);
        }
        
        // Disable self if below 0
        if(this.CurrentIntegrity <= 0)
        {
            this.DisableShield();
        }
    }
    
    function UpdateTimer(dt : Real)
    {
        this.Timer += dt;
        
        // Blink when almost reactivated
        if(this.RechargeTime - this.Timer <= 1)
        {
            this.UpdateBlink(dt);
        }
        
        // Activate shield when recharge time is complete
        if(this.Timer >= this.RechargeTime)
        {
            this.Timer = 0;
            this.ActivateShield();
            this.Owner.Sprite.Visible = true;
        }
    }
    
    function UpdateBlink(dt : Real)
    {
        // Create a random number generator
        var randNumGen = new Random();

        var randNum = randNumGen.DieRoll(3);
        
        if(randNum == 1)
        {
            this.Owner.Sprite.Visible = true;
        }
        else
        {
            this.Owner.Sprite.Visible = false;
        }
    }
    
    function UpdateFlash(dt : Real)
    {
        this.FlashTimer += dt;
        
        // End flash
        if(this.FlashTimer > 0.05)
        {
            this.EndFlash();
        }
    }
    
    function DisableShield()
    {
        this.Active = false;
        this.Owner.EnemyLogic.HasShield = false;
        //this.Owner.Sprite.Visible = false;
        this.Space.CreateAtPosition(Archetype.Find("ShieldDestroy"), this.Owner.Transform.WorldTranslation);
        
        // Display K.O button
        this.Owner.FindChildByName("B_Button").Sprite.Visible = true;
        
        this.Owner.Sprite.Color = this.DefaultColor;
    }
    
    function ActivateShield()
    {
        this.Active = true;
        this.Owner.EnemyLogic.HasShield = true;
        //this.Owner.Sprite.Visible = true;
        this.CurrentIntegrity = this.Integrity;
        
        // Play Sound
        this.Owner.SoundEmitter.PlayCue(SoundCue.Find("ShieldPowerup"));
        
        this.Owner.FindChildByName("B_Button").Sprite.Visible = false;
        this.Owner.Sprite.Color = this.ShieldColor;
    }
    
    function TakeHit(event : AttackEvent)
    {
        if(this.Active && event.Damage > 0)
        {
            this.CurrentIntegrity -= event.Damage;
            //this.Flash();
            this.Space.CreateAtPosition(Archetype.Find("ShieldDamage"), this.Owner.Transform.WorldTranslation);
            
            // Play Sound
            this.Owner.SoundEmitter.PlayCue(SoundCue.Find("ShieldHit"));
        }
    }
    
    function Flash()
    {
        this.Flashing = true;
        // Animation
        this.Owner.Sprite.Color = Real4(this.Owner.Sprite.Color.X, this.Owner.Sprite.Color.Y, this.Owner.Sprite.Color.Z, 0.5);
        this.FlashTimer = 0;
    }
    
    function EndFlash()
    {
        this.Flashing = false;
        //Animation
        this.Owner.Sprite.Color = Real4(this.Owner.Sprite.Color.X, this.Owner.Sprite.Color.Y, this.Owner.Sprite.Color.Z, 0.25);
    }
}
