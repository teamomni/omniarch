class ActionCamera : ZilchComponent
{
    [Property]
    var Duration : Real = 0.0;
    [Property]
    var ZoomScale : Real = 15;
    [Property]
    var SlowTimeScale : Real = 0.3;
    [Property]
    var ZoomSpeed : Real = 0.5;
    
    var Timer : Real = 0.0;
    var Active : Boolean = false;
    
    var ZoomedIn : Boolean = false;
    var ZoomedOut : Boolean = true;
    var ZoomEndY : Real = 0.0;
    var ZoomOutEndY : Real = 0.0;
    
    var TimeSlowed : Boolean = false;
    var TimeRestored : Boolean = true;
    
    function Initialize(init : CogInitializer)
    {
        Zero.Connect(this.Space, Events.LogicUpdate, this.OnLogicUpdate);
        this.Space.TimeSpace.TimeScale = 1.0;
    }

    function OnLogicUpdate(event : UpdateEvent)
    {
        if(this.Active)
        {
            this.Timer += event.Dt / (this.Space.TimeSpace.TimeScale);
            
            if(this.ZoomedIn == false)
            {
                this.UpdateZoomIn();
            }
            
            if(this.Timer >= this.Duration)
            {
                this.Deactivate();
            }
        }
        else
        {
            if(this.ZoomedOut == false)
            {
                this.UpdateZoomOut();
            }
        }
    }
    
    // Activate Action Camera
    function Activate(duration : Real)
    {
        this.Active = true;
        this.Duration = duration;
        this.SlowTime();
        this.ZoomIn();
        
        // Reset values
        this.Timer = 0.0;
    }
    
    function Deactivate()
    {
        this.Active = false;
        this.RestoreTime();
        this.ZoomOut();
    }
    
    function SlowTime()
    {
        this.Space.TimeSpace.TimeScale = this.SlowTimeScale;
    }
    function RestoreTime()
    {
        this.TimeRestored = false;
        this.Space.TimeSpace.TimeScale = 1;
    }
    
    function ZoomIn()
    {
        this.ZoomedIn = false;
        this.ZoomEndY = this.Space.FindObjectByName("Player").Transform.Translation.Y;
    }
    
    function UpdateZoomIn()
    {
        this.Owner.Camera.Size = Math.Lerp(this.Owner.Camera.Size, this.ZoomScale, this.ZoomSpeed);
        this.Owner.Transform.Translation -= Real3(0, Math.Lerp(this.Owner.Transform.Translation.Y, this.ZoomEndY, 0.95), 0);
        
        if(this.Owner.Camera.Size == this.ZoomScale -0.01)
        {
            this.ZoomedIn = true;
        }
    }
    
    function ZoomOut()
    {
        this.ZoomedOut = false;
        this.ZoomOutEndY = this.Owner.CameraLogic.YDistance;
    }
    
    function UpdateZoomOut()
    {
        this.Owner.Camera.Size = Math.Lerp(this.Owner.Camera.Size, 20, this.ZoomSpeed);
        //this.Owner.Transform.Translation += Real3(0, Math.Lerp(this.Owner.Transform.Translation.Y, this.ZoomOutEndY, 0.95), 0);
        
        if(this.Owner.Camera.Size >= 20 - 0.1)
        {
            this.ZoomedOut = true;
        }
    }
}
